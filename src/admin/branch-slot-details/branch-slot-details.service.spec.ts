import { Test, TestingModule } from '@nestjs/testing';
import { BranchSlotDetailsService } from './branch-slot-details.service';

describe('BranchSlotDetailsService', () => {
  let service: BranchSlotDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BranchSlotDetailsService],
    }).compile();

    service = module.get<BranchSlotDetailsService>(BranchSlotDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
