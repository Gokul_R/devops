import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { BranchSlotDetailsService } from './branch-slot-details.service';
import { CreateBranchSlotDetailDto } from './dto/create-branch-slot-detail.dto';
import { UpdateBranchSlotDetailDto } from './dto/update-branch-slot-detail.dto';

// @Controller('branch-slot-details')
export class BranchSlotDetailsController {
  // constructor(private readonly branchSlotDetailsService: BranchSlotDetailsService) {}

  // @Post()
  // create(@Body() createBranchSlotDetailDto: CreateBranchSlotDetailDto) {
  //   return this.branchSlotDetailsService.create(createBranchSlotDetailDto);
  // }

  // @Get()
  // findAll() {
  //   return this.branchSlotDetailsService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.branchSlotDetailsService.findOne(id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateBranchSlotDetailDto: UpdateBranchSlotDetailDto) {
  //   return this.branchSlotDetailsService.update(id, updateBranchSlotDetailDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.branchSlotDetailsService.remove(id);
  // }
}
