import { Test, TestingModule } from '@nestjs/testing';
import { BranchSlotDetailsController } from './branch-slot-details.controller';
import { BranchSlotDetailsService } from './branch-slot-details.service';

describe('BranchSlotDetailsController', () => {
  let controller: BranchSlotDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchSlotDetailsController],
      providers: [BranchSlotDetailsService],
    }).compile();

    controller = module.get<BranchSlotDetailsController>(BranchSlotDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
