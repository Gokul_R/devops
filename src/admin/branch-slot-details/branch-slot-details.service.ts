import { Inject, Injectable } from '@nestjs/common';
import { BaseService } from '../../base.service';
import SlotDetail from 'src/core/database/models/SlotDetail';
import { ModelCtor } from 'sequelize-typescript';
import { SLOT_DETAILS_REPOSITORY } from 'src/core/constants';

@Injectable()
export class BranchSlotDetailsService extends BaseService<SlotDetail>{
  protected model: ModelCtor<SlotDetail>;
  constructor(
    @Inject(SLOT_DETAILS_REPOSITORY)
    private readonly branchServiceTypeRepo: typeof SlotDetail,
  ) {
    super();
    this.model = this.branchServiceTypeRepo;
  }
}
