import { PartialType } from '@nestjs/mapped-types';
import { CreateBranchSlotDetailDto } from './create-branch-slot-detail.dto';

export class UpdateBranchSlotDetailDto extends PartialType(CreateBranchSlotDetailDto) {}
