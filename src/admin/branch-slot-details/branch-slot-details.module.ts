import { Module } from '@nestjs/common';
import { BranchSlotDetailsService } from './branch-slot-details.service';
import { BranchSlotDetailsController } from './branch-slot-details.controller';
import { branchSlotDetailsProviders } from './branch-slot-details.providers';

@Module({
  // controllers: [BranchSlotDetailsController],
  providers: [BranchSlotDetailsService,...branchSlotDetailsProviders],
  exports:[BranchSlotDetailsService]
})
export class BranchSlotDetailsModule {}
