import { commonProvider } from 'src/core/interfaces/common-providers';

export const branchSlotDetailsProviders = [
  // {
  //   provide: BRANCH_REPOSITORY,
  //   useValue: Branch,
  // },
  // {
  //   provide: SLOT_DETAILS_REPOSITORY,
  //   useValue: SlotDetail,
  // },
  commonProvider.slot_details_repository,
];
