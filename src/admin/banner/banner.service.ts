import { Inject, Injectable, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateBannerDto, types } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import { BaseService } from '../../base.service';
import Banner from 'src/core/database/models/Banner';
import { ModelCtor } from 'sequelize-typescript';
import { BANNER_REPOSITORY } from 'src/core/constants';
import { ParamsBannerDto } from './dto/params.banner.dto';
import { Op } from 'sequelize';


@Injectable()
export class BannerService extends BaseService<Banner> {
  protected model: ModelCtor<Banner>;
  constructor(
    @Inject(BANNER_REPOSITORY) private readonly bannerRepo: typeof Banner,
  ) {
    super();
    this.model = this.bannerRepo;
  }
  // create(createBannerDto: CreateBannerDto) {
  //   return 'This action adds a new banner';
  // }

  // findAll() {
  //   return `This action returns all banner`;
  // }

  async getFilterList(req_data: any) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { banner_name: { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await this.findAndCountAll({
      where: {
        ...search,
        is_deleted: false
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']]
    });
  }

  async findAllType() {
    let result = this.bannerRepo.findAll({});
    return result;
  }

  // update(id: number, updateBannerDto: UpdateBannerDto) {
  //   return `This action updates a #${id} banner`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} banner`;
  // }
}
