import { ApiProperty } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';

export enum types {
  HOME = 0,
  FLASH_SALE = 1,
}
export class ParamsBannerDto {
  @ApiProperty({example:1})
  @IsEnum(types)
  banner_type: types;
}
