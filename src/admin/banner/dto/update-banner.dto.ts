import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateBannerDto } from './create-banner.dto';
import { IsOptional, IsString } from 'class-validator';
import { ParamsBannerDto } from './params.banner.dto';

export class UpdateBannerDto extends PartialType(CreateBannerDto) {


}
