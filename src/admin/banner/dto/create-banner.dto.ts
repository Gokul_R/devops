import { IsNotEmpty, IsString, IsDateString, IsOptional, IsBoolean, IsInt, IsEnum, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export enum types {
  HOME = 0,
  FLASH_SALE = 1,
}
export class CreateBannerDto {
  @IsString()
  @IsOptional()
  id: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ default: 'https//172.54.23/image/f4343.jpg' })
  @IsOptional()
  @IsString()
  image: string;

  @ApiProperty({ default: 'banner' })
  @IsOptional()
  @IsString()
  @Length(4, 100, {
    message: 'banner_name must be between 4 and 100 characters',
  })
  banner_name: string;

  // @ApiProperty({ default: 0 })
  // @IsOptional()
  // @IsEnum(types, {
  //   message: 'Invalid banner type . it must be 0 or 1',
  // })
  // banner_type: types;

  // banner_type: number;

  // @ApiProperty({default:'vendor'})
  // @IsOptional()
  // @IsString()
  // add_vendor: string;

  @ApiProperty({ default: '2023-07-13' })
  @IsOptional()
  @IsDateString()
  start_date: Date;

  @ApiProperty({ default: '2023-07-13' })
  @IsOptional()
  @IsDateString()
  end_date: Date;

  @ApiProperty({ default: false })
  @IsOptional()
  @IsBoolean()
  is_mobile: boolean;

  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;
}
