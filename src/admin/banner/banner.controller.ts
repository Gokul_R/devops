import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { BannerService } from './banner.service';
import { CreateBannerDto, types } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../base.controller';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116, EM127 } from 'src/core/constants';
import { ParamsBannerDto } from './dto/params.banner.dto';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@ApiTags('banner')
@Controller('banner')
export class BannerController extends BaseController<CreateBannerDto> {
  constructor(private readonly bannerService: BannerService) {
    super(bannerService);
  }

  @Post()
  async create(@Body() createBannerDto: CreateBannerDto) {
    try {
      let data = await this.bannerService.create(createBannerDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
    // return this.bannerService.create(createBannerDto);
  }

  // @Get()
  // findAll() {
  //   return this.bannerService.findAll();
  // }

  @Get('/by-banner-type')
  async findAllType() {
    try {
      let data = await this.bannerService.findAllType();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
    // return this.bannerService.findOne(banner_type);
  }

  @Post('search')
  async getFilterList(@Body() getbannerServiceListedto: PaginationDto) {
    try {
      let data = await this.bannerService.getFilterList(getbannerServiceListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBannerDto: UpdateBannerDto,
  ) {
    try {
      let data = await this.bannerService.update(id, updateBannerDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }

    // return this.bannerService.update(+id, updateBannerDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let data = await this.bannerService.destroy(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
    //  return this.bannerService.destroy(banner_type);

  }
}
