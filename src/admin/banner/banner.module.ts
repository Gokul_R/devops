import { Module } from '@nestjs/common';
import { BannerService } from './banner.service';
import { BannerController } from './banner.controller';
import { bannerProviders } from './banner.providers';

@Module({
  controllers: [BannerController],
  providers: [BannerService,...bannerProviders]
})
export class BannerModule {}
