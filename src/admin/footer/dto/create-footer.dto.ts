import { IsBoolean, IsOptional, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateFooterDto {
  @IsString()
  @IsOptional()
  id: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: 'lorem ipsem' })
  @IsString()
  @IsOptional()
  footer_text: string;

  @ApiProperty({ example: '@copyright12345' })
  @IsString()
  @IsOptional()
  copyright_text: string;

  // @ApiProperty({default:false})
  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;
}
