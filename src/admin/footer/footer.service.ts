import { Inject, Injectable } from '@nestjs/common';
import Currency from 'src/core/database/models/Currency';
import { ModelCtor } from 'sequelize-typescript';
import { CURRENCY_REPOSITORY, FOOTER_REPOSITORY } from 'src/core/constants';
import { BaseService } from '../../base.service';

@Injectable()
export class FooterService extends BaseService<Currency> {
  protected model: ModelCtor<Currency>;
  constructor(
    @Inject(FOOTER_REPOSITORY) private readonly currenctRepo: typeof Currency,
  ) {
    super();
    this.model = this.currenctRepo;
  }
  }
