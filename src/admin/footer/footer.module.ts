import { Module } from '@nestjs/common';
import { FooterService } from './footer.service';
import { FooterController } from './footer.controller';
import { footerProviders } from './footer.providers';

@Module({
  controllers: [FooterController],
  providers: [FooterService, ...footerProviders],
})
export class FooterModule {}
