import { Inject, Injectable } from '@nestjs/common';
import AdminUser from 'src/core/database/models/AdminUser';
import { ModelCtor } from 'sequelize-typescript';
import {
  ADMIN_USER_REPOSITORY,
  USER_ROLE_REPOSITORY,
} from 'src/core/constants';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import Role from 'src/core/database/models/Role';
import UserRole from 'src/core/database/models/UserRole';
import Permission from 'src/core/database/models/Permission';
import RolePermission from 'src/core/database/models/RolePermission';
import { CreateUserRoleDto } from '../rbac/dto/create-rbac.dto';
import Helpers from 'src/core/utils/helpers';
import { BaseService } from '../../base.service';
import { CreateAdminUserDto } from './dto/create-admin-user.dto';
import Encryption from 'src/core/utils/encryption';
import EmailTemService from 'src/core/utils/emailTemplate';
import { MailOptions, MailUtils } from 'src/core/utils/mailUtils';
import { Model, ModelStatic, Op, col, fn } from 'sequelize';
import { GetRoleUsers } from './dto/get-admin-user.dto';
import VendorRole from 'src/core/database/models/VendorRoles';

@Injectable()
export class AdminUserService extends BaseService<AdminUser> {
  protected model: ModelCtor<AdminUser>;
  admin_user_qry: SequelizeFilter<AdminUser>;
  constructor(
    @Inject(ADMIN_USER_REPOSITORY)
    private readonly adminUserRepo: typeof AdminUser,
    @Inject(USER_ROLE_REPOSITORY)
    private readonly userRolesRepo: typeof UserRole,
    private readonly MailUtilsService: MailUtils,
  ) {
    super();
    this.model = this.adminUserRepo;
  }
  init() {
    this.admin_user_qry = {
      include: [
        {
          model: UserRole,
          include: [
            {
              model: Role,
              include: [
                {
                  model: RolePermission,
                  include: [{ model: Permission }],
                },
              ],
            },
          ],
        },
      ],
    };
  }
  async create(createAdminUserDto: any) {
    let user: any;
    let password = await Helpers.generateRandomPassword();
    let hashPassword = await Encryption.hashPassword(password);
    createAdminUserDto.password = hashPassword;
    if (createAdminUserDto.vendor_id) {
      user = await VendorRole.create(createAdminUserDto);
    } else {
      user = await super.create(createAdminUserDto);
    }
    // let password = await Encryption.decryptValue(user?.password);
    let htmlContent = EmailTemService.emailPasswordVerification(password);
    let mailOptions: MailOptions = {
      to: createAdminUserDto.email_id,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.OTP_VERIFY,
      html: htmlContent,
    };
    this.MailUtilsService.sendEmail(mailOptions);
    if (!createAdminUserDto.vendor_id) {
      delete user?.password;
      let user_role: UserRole = await this.createUserRole({
        user_id: user.id,
        role_id: createAdminUserDto.role_id,
      });
    }

    return user;
  }

  async createUserRole(createPermissionDto: CreateUserRoleDto) {
    let user_role_qry: SequelizeFilter<UserRole> = {
      where: {
        user_id: createPermissionDto.user_id,
        role_id: createPermissionDto.role_id,
      },
      defaults: createPermissionDto,
    };
    let permission = await Helpers.findOrCreate(
      this.userRolesRepo,
      user_role_qry,
    );
    return permission;
  }
  async findAll(): Promise<AdminUser> {
    this.init();
    let users = await super.findAll(this.admin_user_qry);
    users = JSON.stringify(users);
    users = JSON.parse(users);
    users?.user_role?.forEach((user) => {
      user.role.permissions = user?.role?.permissions?.map(
        (permit: any) => permit?.permission,
      );
    });
    return users;
  }

  async updateUsers(id: string, data: any) {
    let user:any;
    let vendor = await VendorRole.findOne({ where: { id: id } });
    if (vendor) {
      user = await Helpers.update(VendorRole, { where: { id: id } }, data);
    }
    else {
      let u_role = await Helpers.update(UserRole, { where: { user_id: id } }, data);
      user = await super.update(id, data);

    }
    return user;

  }

  async removeUsers(id: string) {
    let deletedUsers: any;
    let vendor = await VendorRole.findOne({ where: { id: id } });
    if (vendor) {
      await VendorRole.update({ is_deleted: true }, { where: { id: id } });
       deletedUsers = await VendorRole.destroy({ where: { id: id }});
    }
    else {
      deletedUsers = await super.destroy(id);
    }
    return deletedUsers;

  }

  async getFilterList(req_data: GetRoleUsers) {
    // this.init();
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        //  where: {
        [Op.or]: [
          { '$roles.role_name$': { [Op.iLike]: `%${searchText}%` } },
          { name: { [Op.iLike]: `%${searchText}%` } },
          { employee_id: { [Op.iLike]: `%${searchText}%` } },
          { mobile_no: { [Op.iLike]: `%${searchText}%` } },
          { email_id: { [Op.iLike]: `%${searchText}%` } },
          { role_id: { [Op.iLike]: `%${searchText}%` } },
        ],
        //   },
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let vendorId: {};
    if (req_data.vendor_id) {
      vendorId = { vendor_id: req_data.vendor_id };
    }
    let checkTable: ModelStatic<Model<any, any>> = req_data.vendor_id ? VendorRole : AdminUser;
    let users = await checkTable.findAndCountAll({
      attributes: ['name', 'employee_id', 'mobile_no', 'email_id', 'role_id', 'is_active', 'id'],
      include: [{
        attributes: ['role_name', 'id'],
        model: Role
      }],
      where: { ...search, ...vendorId },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']]
    });
    return users;
  }

  async findvendoruser(vendor_role_user_id: string) {
    return await VendorRole.findOne({ where: { id: vendor_role_user_id } });

  }

  // update(id: number, updateAdminUserDto: UpdateAdminUserDto) {
  //   return `This action updates a #${id} adminUser`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} adminUser`;
  // }
}
