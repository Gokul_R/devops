import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { AdminUserService } from './admin-user.service';
import {
  CreateAdminUserDto,
} from './dto/create-admin-user.dto';
import { UpdateAdminUserDto } from './dto/update-admin-user.dto';
import { BaseController } from '../../base.controller';
import { ApiTags } from '@nestjs/swagger';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EC409, EM100, EM102, EM104, EM106, EM116, EM127 } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import AdminUser from 'src/core/database/models/AdminUser';
import { GetRoleUsers } from './dto/get-admin-user.dto';
import VendorRole from 'src/core/database/models/VendorRoles';
import Vendor from 'src/core/database/models/Vendor';
@ApiTags('Admin User')
@Controller('admin-user')
export class AdminUserController extends BaseController<CreateAdminUserDto> {
  constructor(private readonly adminUserService: AdminUserService) {
    super(adminUserService);
  }

  @Post()
  async create(@Body() createAdminUserDto: CreateAdminUserDto) {
    let existUser: any;
    try {
      if (createAdminUserDto.vendor_id) {
        existUser = await VendorRole.findOne({ attributes: ['email_id'], where: { email_id: createAdminUserDto.email_id } });
        if (!existUser) {
          existUser = await Vendor.findOne({ attributes: ['email_id'], where: { email_id: createAdminUserDto.email_id } });
        }
      } else {
        existUser = await AdminUser.findOne({ attributes: ['email_id'], where: { email_id: createAdminUserDto.email_id } });
      }
      if (existUser && existUser.dataValues) return HandleResponse.buildErrObj(EC409, EM102, null);
      let data = await this.adminUserService.create(createAdminUserDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get()
  async findAll() {
    try {
      let data = await this.adminUserService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get('vendor/:vendor_role_user_id')
  async findvendoruser(@Param('vendor_role_user_id') vendor_role_user_id: string) {
    try {
      let vendorUsers = await this.adminUserService.findvendoruser(vendor_role_user_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, vendorUsers);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);

    }

  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateAdminUserDto: UpdateAdminUserDto,
  ) {
    try {
      let data = await this.adminUserService.updateUsers(id, updateAdminUserDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete(':id')
  async removeUsers(@Param('id') id: string) {
    try {
      let data = await this.adminUserService.removeUsers(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('search')
  async getFilterList(@Body() getadminUserListedto: GetRoleUsers) {
    try {
      let data = await this.adminUserService.getFilterList(getadminUserListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

}
