import { Module } from '@nestjs/common';
import { AdminUserService } from './admin-user.service';
import { AdminUserController } from './admin-user.controller';
import { adminUserProviders } from './admin-user.providers';
import { MailUtils } from 'src/core/utils/mailUtils';

@Module({
  controllers: [AdminUserController],
  providers: [AdminUserService, ...adminUserProviders, MailUtils]
})
export class AdminUserModule { }
