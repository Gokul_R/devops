import { commonProvider } from "src/core/interfaces/common-providers";



export const adminUserProviders = [
commonProvider.admin_user_repository,
commonProvider.user_role_repository,
];
