import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsLowercase } from 'class-validator';
import { IsArray, IsEmail, IsNotEmpty, IsOptional, IsPhoneNumber, IsString, IsUUID, Matches, MAX, ValidateIf } from 'class-validator';
import { Max, Min } from 'sequelize-typescript';
import { BRANCH_ID } from 'src/core/constants';
import { SharedDto } from 'src/core/interfaces/shared.dto';

export class CreateAdminUserDto extends PickType(SharedDto, ['password', 'is_active', 'image']) {
  @IsString()
  @IsOptional()
  id?: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Matches(/.*\S.*/, { message: 'name should contain at least one non-whitespace character' })
  name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Matches(/.*\S.*/, { message: 'mobile_no should contain at least one non-whitespace character' })
  mobile_no: string;

  @ApiProperty()
  @IsLowercase()
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  @Matches(/.*\S.*/, { message: 'email_id should contain at least one non-whitespace character' })
  email_id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Matches(/.*\S.*/, { message: 'employee_id should contain at least one non-whitespace character' })
  employee_id: string;

  @ApiProperty()
  @IsString()
  @Matches(/.*\S.*/, { message: 'role_id should contain at least one non-whitespace character' })
  @IsNotEmpty()
  role_id: string;

  
  @ApiProperty({ example: BRANCH_ID })
  @IsString()
  @IsOptional()
  @Matches(/.*\S.*/, { message: 'vendor_id should contain at least one non-whitespace character' })
  vendor_id: string;

  @ApiProperty({ example: [BRANCH_ID] })
  @IsArray()
  @ValidateIf((o) => o.vendor_id !== undefined && o.vendor_id !== null) // Validate branch_ids only if vendor_id is provided
  @IsNotEmpty({ each: true })
  branch_ids: string[];
}
