import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";
import { BRANCH_ID } from "src/core/constants";
import { PaginationDto } from "src/core/interfaces/shared.dto";

export class GetRoleUsers extends PaginationDto{
    @ApiProperty({example:BRANCH_ID})
    @IsOptional()
    @IsString()
    vendor_id?:string
}