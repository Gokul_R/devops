import { Test, TestingModule } from '@nestjs/testing';
import { BranchGalleryService } from './branch-gallery.service';

describe('BranchGalleryService', () => {
  let service: BranchGalleryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BranchGalleryService],
    }).compile();

    service = module.get<BranchGalleryService>(BranchGalleryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
