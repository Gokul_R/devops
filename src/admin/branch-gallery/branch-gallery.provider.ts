import { commonProvider } from "src/core/interfaces/common-providers";


export const branchGalleryProviders = [
    // {
    //   provide: BRANCH_REPOSITORY,
    //   useValue: Branch,
    // },
    // {
    //   provide: BRANCH_GALLERY_REPOSITORY,
    //   useValue: BranchGallery,
    // },
    // {
    //   provide: BRANCH_GALLERY_FOLDER_REPOSITORY,
    //   useValue: BranchGalleryFolder,
    // },
    commonProvider.branch_repository,
    commonProvider.branch_gallary_repository,
    commonProvider.branch_gallery_folder_repository

  ];