import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param
} from '@nestjs/common';
import { BranchGalleryService } from './branch-gallery.service';
import { CreateBranchGalleryDto } from './dto/create-branch-gallery.dto';
import { UpdateBranchGalleryDto } from './dto/update-branch-gallery.dto';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../base.controller';
import { EC200, EM104, EM100, EM106, EM116 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import BranchGallery from 'src/core/database/models/BranchGallery';

@ApiTags('Branch Gallery')
@Controller('branch-gallery')
export class BranchGalleryController extends BaseController<CreateBranchGalleryDto> {
  constructor(private readonly branchGalleryService: BranchGalleryService) {
    super(branchGalleryService);
  }

  @Post()
  async create(@Body() createBranchGalleryDto: CreateBranchGalleryDto) {
    try {
      let data: BranchGallery = await this.branchGalleryService.create(
        createBranchGalleryDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get('by-branch/:branch_id')
  async findAllByBranchId(@Param('branch_id') branch_id: string) {
    try {
      let data: BranchGallery[] =
        await this.branchGalleryService.findAllByBranchId(branch_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBranchGalleryDto: UpdateBranchGalleryDto,
  ) {
    try {
      let data = await this.branchGalleryService.update(
        id,
        updateBranchGalleryDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
