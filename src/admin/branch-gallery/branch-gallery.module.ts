import { Module } from '@nestjs/common';
import { BranchGalleryService } from './branch-gallery.service';
import { BranchGalleryController } from './branch-gallery.controller';
import { branchGalleryProviders } from './branch-gallery.provider';

@Module({
  controllers: [BranchGalleryController],
  providers: [BranchGalleryService,...branchGalleryProviders]
})
export class BranchGalleryModule {}
