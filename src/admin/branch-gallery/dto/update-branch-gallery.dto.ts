import { OmitType } from '@nestjs/swagger';
import { CreateBranchGalleryDto } from './create-branch-gallery.dto';

export class UpdateBranchGalleryDto extends OmitType(CreateBranchGalleryDto, [
  'branch_id'
]) {}
