import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsString, IsOptional, IsBoolean, IsUUID, IsArray, IsNotEmpty } from "class-validator";
import { BRANCH_ID, VENDOR_ID } from "src/core/constants";

export class CreateBranchGalleryDto {
  @IsString()
  @IsOptional()
  id: string;

  @ApiProperty({ example: "" })
  @IsString()
  @IsOptional()
  folder_id: string;

  @ApiProperty({ example: BRANCH_ID })
  // @IsUUID()
  @IsNotEmpty()
  branch_id: string;

  // @ApiProperty({example:VENDOR_ID})
  // @IsUUID()
  // @IsNotEmpty()
  // vendor_id: string;

  @ApiProperty({ example: [] })
  @IsArray()
  @IsOptional()
  @Transform(({ value }) => value || [])
  gallery_list: any[];
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;
}
