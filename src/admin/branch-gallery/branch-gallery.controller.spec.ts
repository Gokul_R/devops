import { Test, TestingModule } from '@nestjs/testing';
import { BranchGalleryController } from './branch-gallery.controller';
import { BranchGalleryService } from './branch-gallery.service';

describe('BranchGalleryController', () => {
  let controller: BranchGalleryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchGalleryController],
      providers: [BranchGalleryService],
    }).compile();

    controller = module.get<BranchGalleryController>(BranchGalleryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
