import { Inject, Injectable } from '@nestjs/common';
import { CreateBranchGalleryDto } from './dto/create-branch-gallery.dto';
import { UpdateBranchGalleryDto } from './dto/update-branch-gallery.dto';
import { BaseService } from '../../base.service';
import BranchGallery from 'src/core/database/models/BranchGallery';
import { ModelCtor } from 'sequelize-typescript';
import { BRANCH_GALLERY_FOLDER_REPOSITORY, BRANCH_GALLERY_REPOSITORY } from 'src/core/constants';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import BranchGalleryFolder from 'src/core/database/models/BranchGalleryFolder';

@Injectable()
export class BranchGalleryService extends BaseService<BranchGallery> {
  protected model: ModelCtor<BranchGallery>;
  gallery_branch_qry: SequelizeFilter<BranchGallery>;
  constructor(
    @Inject(BRANCH_GALLERY_REPOSITORY)
    private readonly branchGalleryRepo: typeof BranchGallery,
    @Inject(BRANCH_GALLERY_FOLDER_REPOSITORY)
    private readonly branchGalleryFolderRepo: typeof BranchGalleryFolder,
  ) {
    super();
    this.model = this.branchGalleryRepo;
    this.init();
  }
  init() {
    this.gallery_branch_qry={
      include:[
        {
          attributes:['folder_name'],
          model:this.branchGalleryFolderRepo
        }
      ]
    }
  }

  async findAllByBranchId(branch_id: string): Promise<Array<BranchGallery>> {
    this.gallery_branch_qry.where = {
      branch_id: branch_id,
    };
    return super.findAll(this.gallery_branch_qry);
  }
}
