import { Module } from '@nestjs/common';
import { HelpAndSupportService } from './help-and-support.service';
import { HelpAndSupportController } from './help-and-support.controller';
import { MailUtils } from 'src/core/utils/mailUtils';

@Module({
  controllers: [HelpAndSupportController],
  providers: [HelpAndSupportService, MailUtils]
})
export class HelpAndSupportModule { }
