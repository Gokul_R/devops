import { Injectable } from '@nestjs/common';
import { CreateHelpAndSupportDto } from './dto/create-help-and-support.dto';
import { MailOptions, MailUtils } from 'src/core/utils/mailUtils';
import ContactUs from 'src/core/database/models/HelpAndSupport';
import Helpers from 'src/core/utils/helpers';
import { logger } from 'src/core/utils/logger';
import HelpAndSupport from 'src/core/database/models/HelpAndSupport';
import { GetHelpAndSupport } from './dto/get-help-and-support';
import { Op } from 'sequelize';
import { Provided_By } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import SupportCenter from 'src/core/database/models/SupportCenter';

@Injectable()
export class HelpAndSupportService {
  constructor(private readonly MailUtilsService: MailUtils) { }

  async helpAndSupport(createHelpAndSupportDto: CreateHelpAndSupportDto) {
    logger.info(`Help_And_Support_Entry: ${createHelpAndSupportDto}`);
    let data = await Helpers.create(HelpAndSupport, createHelpAndSupportDto);
    this.MailUtilsService.helpAndSupportContent(createHelpAndSupportDto);
    logger.info(`Help_And_Support_Exit:`);
    return data;
  }

  async getVendorAndAdminHelpAndSupport(req: GetHelpAndSupport) {
    logger.info(`Get_Help_And_Support_Entry: ${req}`);
    let search;
    if (req.searchText) {
      search = {
        [Op.or]: [
          {
            user_type: { [Op.iLike]: `%${req.searchText.toUpperCase()}%` },
          },
          {
            name: { [Op.iLike]: `%${req.searchText.toLowerCase()}%` },
          },
          {
            phone_number: { [Op.iLike]: `%${req.searchText.toLowerCase()}%` },
          },
          {
            from_email: { [Op.iLike]: `%${req.searchText.toLowerCase()}%` },
          },
        ],
      };
    }

    let pageNo = req.pagNo || 1;
    let limit = req.limit || 10;
    let offset = (pageNo - 1) * req.limit;

    let userType;
    if (req.user_type && req.user_type != Provided_By.SUPER_ADMIN) {
      userType = {
        user_type: req.user_type,
      };

    }
    let getDate = await HelpAndSupport.findAndCountAll({
      where: {
        is_active: true,
        is_deleted: false,
        deleted_at: null,
        ...search,
        ...userType,
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']],
    });
    logger.info(`Get_Help_And_Support_Exit:`);
    return getDate;
  }

  async getUserSupportList(getList: PaginationDto) {
    logger.info(`Get_Users_Support_Entry:`);
    let search;
    if (getList.searchText) {
      search = {
        [Op.or]: [
          {
            user_name: { [Op.iLike]: `%${getList.searchText.toUpperCase()}%` },
          },
          {
            user_ph: { [Op.iLike]: `%${getList.searchText.toLowerCase()}%` },
          },
          {
            user_email: { [Op.iLike]: `%${getList.searchText.toLowerCase()}%` },
          },
          {
            order_id: { [Op.iLike]: `%${getList.searchText.toLowerCase()}%` },
          },
        ],
      };
    }
    let pageNo = getList.pagNo || 1;
    let limit = getList.limit || 10;
    let offset = (pageNo - 1) * getList.limit;
    return await SupportCenter.findAndCountAll({
      where: {
        is_active: true,
        is_deleted: false,
        deleted_at: null,
        ...search,
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']],
    });
  }
}
