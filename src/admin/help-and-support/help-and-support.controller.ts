import { Controller, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { HelpAndSupportService } from './help-and-support.service';
import { CreateHelpAndSupportDto } from './dto/create-help-and-support.dto';
import { ApiTags } from '@nestjs/swagger';
import { EC200, EM104, EM100, EC500, EM141, EM106 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { GetHelpAndSupport } from './dto/get-help-and-support';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@ApiTags('Help And Support')
@Controller('help-and-support')
export class HelpAndSupportController {
  constructor(private readonly helpAndSupportService: HelpAndSupportService) { }

  @Post()
  async helpAndSupport(@Body() createHelpAndSupportDto: CreateHelpAndSupportDto) {
    try {
      let data = await this.helpAndSupportService.helpAndSupport(createHelpAndSupportDto);
      return HandleResponse.buildSuccessObj(EC200, EM141, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Post('search')
  async getHelpAndSupport(@Body() getHelpAndSupport: GetHelpAndSupport) {
    try {
      let data = await this.helpAndSupportService.getVendorAndAdminHelpAndSupport(getHelpAndSupport);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Post('user')
  async getUserSupportList(@Body() getList: PaginationDto) {
    try {
      let data = await this.helpAndSupportService.getUserSupportList(getList);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }
}
