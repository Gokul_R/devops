import { PartialType } from '@nestjs/swagger';
import { CreateHelpAndSupportDto } from './create-help-and-support.dto';

export class UpdateHelpAndSupportDto extends PartialType(CreateHelpAndSupportDto) {}
