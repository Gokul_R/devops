import { ApiProperty, PartialType } from "@nestjs/swagger";
import { IsNotEmpty, IsString, IsEnum, IsOptional } from "class-validator";
import { Provided_By } from "src/core/constants";
import { PaginationDto } from "src/core/interfaces/shared.dto";

export class GetHelpAndSupport extends PartialType(PaginationDto){
    @ApiProperty({ example: Provided_By.USER + ' (or) ' + Provided_By.BRANCH + ' (or) ' + Provided_By.VENDOR  + ' (or) ' + Provided_By.SUPER_ADMIN })
    @IsOptional()
    @IsString()
   // @IsEnum(Provided_By, { message: 'user_type must be USER (or) VENDOR (or) BRANCH (or) SUPER_ADMIN' })
    user_type: string;
  
}