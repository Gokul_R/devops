import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsEmail, IsEnum, IsLowercase, IsNotEmpty, IsOptional, IsString, Length } from 'class-validator';
import { BRANCH_ID, Provided_By } from 'src/core/constants';

export class CreateHelpAndSupportDto {
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: 'manoj@mailinator.com' })
  @IsNotEmpty()
  @IsEmail()
  @IsLowercase()
  from_email: string;

  @ApiProperty({ example: 'manoj' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: BRANCH_ID })
  @IsString()
  @IsNotEmpty()
  user_id: string;

  @ApiProperty({ example: Provided_By.USER + ' (or) ' + Provided_By.BRANCH + ' (or) ' + Provided_By.VENDOR + ' (or) ' + Provided_By.SUPER_ADMIN })
  @IsNotEmpty()
  @IsString()
  @IsEnum(Provided_By, { message: 'user_type must be USER (or) VENDOR (or) BRANCH (or) SUPER_ADMIN' })
  user_type: string;

  @ApiProperty({ example: '8056743256' })
  @IsString()
  @IsOptional()
  @Length(5, 15, { message: 'phone_number should be min 5 max 15 number only!' })
  phone_number: string;

  @ApiProperty({ example: 'feedback from users or vendors' })
  @IsString()
  @IsNotEmpty()
  message: string;
}
