import { IsString, IsInt, Length, IsBoolean, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

enum Alignment {
  SYMBOL_AMOUNT = 0,
  AMOUNT_SYMBOL = 1,
}

export class CreateCurrencyDto {
  @IsString()
  @IsOptional()
  id: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: 'Lorem Ipsum' })
  @IsString()
  currency_name: string;

  @ApiProperty({ example: '$' })
  @IsString()
  @Length(1, 1)
  currency_symbol: string;

  @ApiProperty({ example: "ind" })
  @IsString()
  currency_code: string;

  // @ApiProperty({ example: '1000 ' })
  // @IsString()
  // rate: string;

  @ApiProperty({ example: 0 })
  @IsInt()
  alignment: Alignment;

  // @ApiProperty({default:false})
  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;


}
