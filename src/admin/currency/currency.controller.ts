import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import { CurrencyService } from './currency.service';
import { CreateCurrencyDto } from './dto/create-currency.dto';
import { UpdateCurrencyDto } from './dto/update-currency.dto';
import { BaseController } from '../../base.controller';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@ApiTags('currency')
@Controller('currency')
export class CurrencyController extends BaseController<CreateCurrencyDto>{
  constructor(private readonly currencyService: CurrencyService) {
    super(currencyService);
  }

  @Post()
  // @UsePipes(new ValidationPipe())
  async create(@Body() body: CreateCurrencyDto) {
    try {
      let data = await this.currencyService.create(body);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }


  @Post('search')
  async getFilterList(@Body() getcurrencyServiceListedto: PaginationDto) {
    try {
      let data = await this.currencyService.getFilterList(getcurrencyServiceListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get()
  // async findAll() {
  //   try {
  //     let data = await this.currencyService.findAll();
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  // @Get(':id')
  // async findOne(@Param('id') id: string) {
  //   try {
  //     let data = await this.currencyService.findOne(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() body: UpdateCurrencyDto) {
    try {
      let data = await this.currencyService.update(id, body);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
