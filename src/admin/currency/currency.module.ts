import { Module } from '@nestjs/common';
import { CurrencyService } from './currency.service';
import { CurrencyController } from './currency.controller';
import { currencyProviders } from './currency.providers';

@Module({
  controllers: [CurrencyController],
  providers: [CurrencyService,...currencyProviders],
})
export class CurrencyModule {}
