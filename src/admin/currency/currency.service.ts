import { Inject, Injectable } from '@nestjs/common';
import { CreateCurrencyDto } from './dto/create-currency.dto';
import { UpdateCurrencyDto } from './dto/update-currency.dto';
import Currency from 'src/core/database/models/Currency';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import { CURRENCY_REPOSITORY } from 'src/core/constants';
import { Op } from 'sequelize';

@Injectable()
export class CurrencyService extends BaseService<Currency> {
  protected model: ModelCtor<Currency>;
  constructor(@Inject(CURRENCY_REPOSITORY) private readonly currencyrepo: typeof Currency) {
    super();
    this.model = this.currencyrepo;

  }
  // create(Body: CreateCurrencyDto) {
  //   const result = Currency.create(Body);
  //   return result;
  // }

  // findAll() {
  //   const result = Currency.findAll();
  //   return result;
  // }

  // findOne(Id: string) {
  //   const result = Currency.findOne({
  //     where: { id: Id },
  //   });
  //   return result;
  // }

  // update(Id: string, Body: UpdateCurrencyDto) {
  //   const result = Currency.update(Body, {
  //     where: { id: Id },
  //   });
  //   return result;
  //  }


  async getFilterList(req_data: any) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { currency_name: { [Op.iLike]: `%${searchText}%` } },
          { currency_symbol: { [Op.iLike]: `%${searchText}%` } },
          { currency_code: { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await this.findAndCountAll({
      where: {
        ...search,
        is_deleted: false
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']]
    });
  }

}
