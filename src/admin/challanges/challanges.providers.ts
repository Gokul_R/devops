import { commonProvider } from 'src/core/interfaces/common-providers';

export const challangesProviders = [commonProvider.challanges_repository];
