import { Controller, Get, Post, Body, Patch, Param, Delete, ParseEnumPipe, ParseIntPipe } from '@nestjs/common';
import { ChallangesService } from './challanges.service';
import { CreateChallangeDto, GetChallengeDto } from './dto/create-challange.dto';
import { UpdateChallangeDto } from './dto/update-challange.dto';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from 'src/base.controller';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@ApiTags('Challanges')
@Controller('challanges')
export class ChallangesController extends BaseController<CreateChallangeDto> {
  constructor(private readonly challangesService: ChallangesService) {
    super(challangesService);
  }

  @Post()
  async create(@Body() createChallangeDto: CreateChallangeDto) {
    try {
      let data = await this.challangesService.create(createChallangeDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }

  }

  // @Get()
  // findAll() {
  //   return this.challangesService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.challangesService.findOne(+id);
  // }

  @Get('/challange_type/:challange_type')
  async findChallangeType(@Param('challange_type') challange_type: string) {
    try {
      let data = await this.challangesService.findChallangeType(challange_type);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('/challange_type/search')
  async getFilterList(@Body() getchallangesServiceListedto: GetChallengeDto) {
    try {
      let data = await this.challangesService.getFilterList(getchallangesServiceListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateChallangeDto: UpdateChallangeDto) {
    try {
      let data = await this.challangesService.update(id, updateChallangeDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }

  }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.challangesService.remove(+id);
  // }
}
