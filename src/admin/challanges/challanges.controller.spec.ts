import { Test, TestingModule } from '@nestjs/testing';
import { ChallangesController } from './challanges.controller';
import { ChallangesService } from './challanges.service';

describe('ChallangesController', () => {
  let controller: ChallangesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChallangesController],
      providers: [ChallangesService],
    }).compile();

    controller = module.get<ChallangesController>(ChallangesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
