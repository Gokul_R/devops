import { Test, TestingModule } from '@nestjs/testing';
import { ChallangesService } from './challanges.service';

describe('ChallangesService', () => {
  let service: ChallangesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChallangesService],
    }).compile();

    service = module.get<ChallangesService>(ChallangesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
