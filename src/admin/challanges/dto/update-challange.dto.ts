import { PartialType } from '@nestjs/swagger';
import { CreateChallangeDto } from './create-challange.dto';

export class UpdateChallangeDto extends PartialType(CreateChallangeDto) {}
