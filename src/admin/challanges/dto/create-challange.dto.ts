import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsDate, IsDateString, IsEnum, IsInt, IsNotEmpty, IsOptional, IsString, isInt } from 'class-validator';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

enum types {
  DAILY = 'daily',
  WEEKLY = 'weekly',
}

export class CreateChallangeDto {
  @IsString()
  @IsOptional()
  id: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: 'daily' })
  @IsNotEmpty()
  @IsEnum(types)
  challange_type?: types;

  @ApiProperty({ example: 'drink water' })
  @IsString()
  title?: string;

  @ApiProperty({ example: ' 3' })
  @IsInt()
  schedule_count?: number;

  @ApiProperty({ example: 1 })
  @IsInt()
  schedule_time?: number;


  // @ApiProperty({ example: '2' })
  // @IsInt()
  // clicking_count?: number;

  @ApiProperty({ example: ' https//234.54.54/photo.jpg' })
  @IsString()
  image?: string;

  @ApiProperty({ example: ' 30' })
  @IsInt()
  reward_coins?: number;

  @ApiProperty({ example: new Date() })
  @IsDateString()
  // @IsString()
  start_date?: Date;

  @ApiProperty({ example: new Date() })
  @IsDateString()
  // @IsString()
  end_date?: Date;

  @ApiProperty({ example: 'how to play this games...' })
  @IsString()
  description?: string;
}


export class GetChallengeDto extends PaginationDto {
  @ApiProperty({ example: 'daily' })
  @IsNotEmpty()
  @IsEnum(types)
  challange_type?: types;
}