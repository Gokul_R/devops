import { Inject, Injectable } from '@nestjs/common';
import { CreateChallangeDto } from './dto/create-challange.dto';
import { UpdateChallangeDto } from './dto/update-challange.dto';
import { BaseService } from 'src/base.service';
import { ModelCtor } from 'sequelize-typescript';
import Challenges from 'src/core/database/models/Challenges';
import { CHALLANGES_REPOSITORY } from 'src/core/constants';
import { Op } from 'sequelize';

@Injectable()
export class ChallangesService extends BaseService<Challenges> {
  protected model: ModelCtor<Challenges>;
  constructor(
    @Inject(CHALLANGES_REPOSITORY)
    private readonly challangesRepo: typeof Challenges,
  ) {
    super();
    this.model = this.challangesRepo;
  }
  // create(createChallangeDto: CreateChallangeDto) {
  //   return 'This action adds a new challange';
  // }

  // findAll() {
  //   return `This action returns all challanges`;
  // }

  async findChallangeType(challange_type: string) {
    let result = await this.challangesRepo.findAll({
      where: { challange_type: challange_type },
    });
    return result;
  }


  async getFilterList(req_data: any) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { title: { [Op.iLike]: `%${searchText}%` } },
          { description: { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await this.findAndCountAll({
      where: {
        ...search,
        is_deleted: false,
        challange_type: req_data.challange_type
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']]
    });
  }

  // update(id: number, updateChallangeDto: UpdateChallangeDto) {
  //   return `This action updates a #${id} challange`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} challange`;
  // }
}
