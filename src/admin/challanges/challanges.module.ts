import { Module } from '@nestjs/common';
import { ChallangesService } from './challanges.service';
import { ChallangesController } from './challanges.controller';
import { challangesProviders } from './challanges.providers';

@Module({
  controllers: [ChallangesController],
  providers: [ChallangesService,...challangesProviders]
})
export class ChallangesModule {}
