import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString, Matches } from 'class-validator';
import { BRANCH_ID, Provided_By } from 'src/core/constants';

export class LogoutDto {
  @ApiProperty({ example: BRANCH_ID })
  @IsString()
  @IsNotEmpty()
  user_id: string;

  @ApiProperty({ example: `${Provided_By.SUPER_ADMIN} (or) ${Provided_By.VENDOR} (or) ${Provided_By.BRANCH}` })
  @IsString()
  @IsNotEmpty()
  @IsEnum(Provided_By)
  user_type: string;
}

export class logsDto {
  @ApiProperty({ example: 'info' })
  @IsString()
  level?: string;

  @ApiProperty({ example: 'razorpay' })
  @IsString()
  type?: string;

  @ApiProperty({ example: '2024-05-01' })
  @IsString()
  // @IsOptional()
  // @Matches(/^\d{4}-\d{2}-\d{2}$/, { message: 'toDate must be in the format YYYY-MM-DD' })
  fromDate?: Date;

  @ApiProperty({ example: '2024-05-01' })
  @IsString()
  // @IsOptional()
  // @Matches(/^\d{4}-\d{2}-\d{2}$/, { message: 'toDate must be in the format YYYY-MM-DD' })
  toDate?: Date;

  @ApiProperty({ example: 1 })
  @IsNumber()
  page?: number;

  @ApiProperty({ example: 50 })
  @IsNumber()
  limit?: number;
}
