import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsString } from "class-validator";

enum DEVICE_TYPE {
    ANDROID = 'Android',
    IOS = 'iOS'
}
export class GetForceUpdate {

    @ApiProperty({ example: 'Android (or) iOS' })
    @IsEnum(DEVICE_TYPE)
    @IsString()
    device_type: string;
}
export class UpdateForceUpdate extends GetForceUpdate {

    @ApiProperty({ example: '0.0.2' })
    @IsString()
    version: string;
}