/* eslint-disable prettier/prettier */
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { DatabaseModule } from '../core/database/database.module';
import { MasterServiceTypesModule } from './master-service-types/master-service-types.module';
import { ServicesModule } from './services/services.module';
import { MasterCustomerTypesModule } from './master-customer-types/master-category.module';
import { MasterServiceCategoriesModule } from './master-service-categories/master-service-categories.module';
import { FlashSaleModule } from './flash-sale/flash-sale.module';
import { VendorModule } from './vendor/vendor.module';
import { SiteMaintenanceModule } from './site-maintenance/site-maintenance.module';
import { SmsSettingsModule } from './sms-settings/sms-settings.module';
import { PaymentConfigModule } from './payment-config/payment-config.module';
import { SmtpSettingsModule } from './smtpsettings/smtpsettings.module';
import { CurrencyModule } from './currency/currency.module';
import { BranchModule } from './branch/branch.module';
import { BranchCategoryModule } from './branch-category/branch-category.module';
import { BranchGalleryModule } from './branch-gallery/branch-gallery.module';
import { BranchServiceCategoryModule } from './branch-service-category/branch-service-category.module';
import { BranchServiceTypeModule } from './branch-service-type/branch-service-type.module';
import { BranchSlotDetailsModule } from './branch-slot-details/branch-slot-details.module';
import { CouponModule } from './coupon/coupon.module';
import { AppInformationModule } from './app-information/app-information.module';
import { AdminNotificationModule } from './admin-notification/admin-notification.module';
import { FooterModule } from './footer/footer.module';
import { LogoModule } from './logo/logo.module';
import { BannerModule } from './banner/banner.module';
import { AdminUserModule } from './admin-user/admin-user.module';
import { AuthModule } from './auth/auth.module';
import { BranchGalleryFolderModule } from './branch-gallery-folder/branch-gallery-folder.module';
import { LanguagesModule } from './languages/languages.module';
import { MarketingGroupModule } from './marketing-group/marketing-group.module';
import { NotificationConfigModule } from './notificationConfig/notification.module.config';
import { ReviewModule } from './review/review.module';
import { RoleModule } from './rbac/role.module';
import { CustomerDetailsModule } from './customer-details/customer-details.module';
import { ChallangesModule } from './challanges/challanges.module';
import { OrdersReportModule } from './orders-report/orders-report.module';
import { SEQUELIZE } from 'src/core/constants';
import { Sequelize } from 'sequelize';
import { ServicesReportModule } from './services-report/services-report.module';
import { CategoryReportModule } from './category-report/category-report.module';
import { BranchReportModule } from './branch-report/branch-report.module';
import { RequestPendingReportModule } from './request-pending-report/request-pending-report.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ReferalManageModule } from './referal-manage/referal-manage.module';
import { CommissionsModule } from './commissions/commissions.module';
import { PayoutModule } from './payout/payout.module';
import { JwtMiddleware } from 'src/middleware/jwt.middleware';
import { SlotManagementModule } from './slot-management/slot-management.module';
import { HelpAndSupportModule } from './help-and-support/help-and-support.module';
import { AwsS3Module } from './aws-s3/aws-s3.module';
import { MulterModule } from '@nestjs/platform-express';
import { FreelancerModule } from './freelancer/freelancer.module';


@Module({
  imports: [
    DatabaseModule,
    DashboardModule,
    ServicesModule,
    BranchModule,
    MasterCustomerTypesModule,
    MasterServiceTypesModule,
    MasterServiceCategoriesModule,
    FlashSaleModule,
    VendorModule,
    BranchServiceCategoryModule,
    BranchCategoryModule,
    BranchServiceTypeModule,
    BranchSlotDetailsModule,
    CouponModule,
    SiteMaintenanceModule,
    SmsSettingsModule,
    AdminNotificationModule,
    PaymentConfigModule,
    CurrencyModule,
    SmtpSettingsModule,
    BranchGalleryModule,
    AuthModule,
    BranchGalleryFolderModule,
    AdminUserModule,
    MarketingGroupModule,
    NotificationConfigModule,
    LanguagesModule,
    AppInformationModule,
    AdminNotificationModule,
    FooterModule,
    LogoModule,
    BannerModule,
    ReviewModule,
    RoleModule,
    CustomerDetailsModule,
    ChallangesModule,
    OrdersReportModule,
    ServicesReportModule,
    CategoryReportModule,
    BranchReportModule,
    RequestPendingReportModule,
    ReferalManageModule,
    CommissionsModule,
    PayoutModule,
    SlotManagementModule,
    HelpAndSupportModule,
    AwsS3Module,
    MulterModule.register({
      dest: './uploads',
    }),
    FreelancerModule,
  ],
  controllers: [AdminController],
  providers: [AdminService, { provide: SEQUELIZE, useValue: Sequelize }],
})
export class AdminModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(JwtMiddleware).forRoutes('auth/change-password');
    // consumer.apply(JwtMiddleware).forRoutes('*'); // TODO enable the JWT token authorize 
  }
}