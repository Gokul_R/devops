import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDateString,
  IsEnum,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { VendorType } from 'src/core/utils/enum';

export class CreateCategoryReportDto {
  @IsString()
  @IsOptional()
  id?: string;

  @ApiProperty({ example: null })
  @IsDateString()
  @IsOptional()
  from_date?: string;

  @ApiProperty({ example: null })
  @IsOptional()
  @IsDateString()
  to_date?: string;

  @ApiProperty({ example: null })
  @IsOptional()
  @IsString()
  vendor_id?: string;

  @ApiProperty({ example: null })
  @IsOptional()
  @IsString()
  branch_id?: string;

  @ApiProperty({ example: '' })
  @IsOptional()
  @IsString()
  searchText?: string;
  @ApiProperty({ example: null })
  @IsOptional()
  @IsEnum(APPOINTMENT_STATUS, {
    message:
      'status must be one of the following values: Pending, Confirmed, Completed, Cancelled, Rejected',
  })
  status?: string;

  @ApiProperty({ example: `${VendorType.BUSINESS} '(or)' ${VendorType.FREELANCER}` })
  @IsOptional()
  @IsString()
  vendor_type: string;
}
