import { Injectable } from '@nestjs/common';
import { CreateCategoryReportDto } from './dto/create-category-report.dto';

import { BaseService } from 'src/base.service';
import { ModelCtor } from 'sequelize-typescript';
import Order from 'src/core/database/models/Order';
import { Op, QueryTypes } from 'sequelize';
import MasterCategory from 'src/core/database/models/MasterCategory';
import sequelize from 'sequelize';
import Service from 'src/core/database/models/Services';
import Branch from 'src/core/database/models/Branch';
import OrdersService from 'src/core/database/models/OrderServices';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import VendorRole from 'src/core/database/models/VendorRoles';

@Injectable()
export class CategoryReportService {
  // protected model: ModelCtor<Order>;
  async getCategoryDetails(createCategoryReportDto: CreateCategoryReportDto) {
    let { from_date, to_date, branch_id, searchText, vendor_id, status, vendor_type } = createCategoryReportDto;

    // const pageSize = limit || 10;
    // const currentPage = pagNo || 1;
    // const offset = (currentPage - 1) * pageSize;

    let searchData;
    if (searchText) {
      searchData = {
        $category$: { [Op.iLike]: `%${searchText}%` },
      };
    }
    let branchId: any;
    let vendorRoleuser = await VendorRole.findOne({ attributes: ['branch_ids'], where: { id: vendor_id } });
    if (vendorRoleuser) {
      branchId = { '$services->branch.id$': vendorRoleuser?.branch_ids };

    } else {
      branchId =
        branch_id && vendor_id
          ? {
            '$services->branch.id$': branch_id,
            '$services->branch.vendor_id$': vendor_id,
          }
          : branch_id && !vendor_id
            ? { '$services->branch.id$': branch_id }
            : !branch_id && vendor_id
              ? { '$services->branch.vendor_id$': vendor_id }
              : {};

    }


    let dateStatus;
    if (from_date && to_date) {
      dateStatus = {
        [Op.and]: [
          {
            created_at: {
              [Op.gte]: `${from_date} 00:00:00.000`,
              [Op.lte]: `${to_date} 23:59:59.999`,
            },
          },
          {
            '$services->ordersService->order.created_at$': {
              [Op.gte]: `${from_date} 00:00:00.000`,
              [Op.lte]: `${to_date} 23:59:59.999`,
            },
          },
        ],
        // [Op.and]: [
        //   {
        //     '$services->ordersService->order.created_at$': {
        //       [Op.gte]: `${from_date} 00:00:00.000`,
        //       [Op.lte]: `${to_date} 23:59:59.999`,
        //     },
        //   },
        // ],
      };
    }
    let vendorType: any = vendor_type ? { where: { vendor_type: vendor_type } } : {};
    let orderStatus = status ? { order_status: status } : {};
    let orderStatus1 = status ? { '$services->ordersService->order.order_status$': status } : {};
    const data = await MasterCategory.findAll({
      attributes: [
        'category',
        [sequelize.col('services.category_id'), 'category_id'],
        [sequelize.col('services->branch.id'), 'branch_id'],
        [sequelize.col('services->branch.vendor_id'), 'vendor_id'],
        [
          sequelize.fn(
            'COUNT',
            sequelize.col('services->ordersService.service_id'),
          ),
          'total_sales',
        ],
      ],
      include: [
        {
          model: Service,
          as: 'services',
          attributes: [],
          paranoid: false,
          include: [
            {
              model: Branch,
              as: 'branch',
              attributes: [],
              ...vendorType,
              paranoid: false,
              required: false,
            },
            {
              model: OrdersService,
              as: 'ordersService',
              attributes: [],
              required: true,
              paranoid: false,
              include: [
                {
                  model: Order,
                  as: 'order',
                  attributes: [],
                  required: true,
                  paranoid: false,
                  where: {
                    ...dateStatus,
                    ...orderStatus,
                  },
                },
              ],
            },
          ],
        },
      ],
      paranoid: false,
      where: {
        ...orderStatus1,
        ...branchId,
        ...dateStatus,
        ...searchData,
      },
      group: [
        'MasterCategory.id',
        'services->branch.id',
        'services.category_id',
      ],
    });
    return data;
  }
}

