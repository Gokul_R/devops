import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CategoryReportService } from './category-report.service';
import { CreateCategoryReportDto } from './dto/create-category-report.dto';
import { UpdateCategoryReportDto } from './dto/update-category-report.dto';
import { BaseController } from 'src/base.controller';
import { EC200, EM106, EM100 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Category Report')
@Controller('category-report')
export class CategoryReportController {
  constructor(private readonly categoryReportService: CategoryReportService) {
    // super(categoryReportService);
  }


  @Post()
  async create(@Body() createCategoryReportDto: CreateCategoryReportDto) {
    try {
      let data = await this.categoryReportService.getCategoryDetails(createCategoryReportDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
