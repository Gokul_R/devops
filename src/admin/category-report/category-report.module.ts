import { Module } from '@nestjs/common';
import { CategoryReportService } from './category-report.service';
import { CategoryReportController } from './category-report.controller';

@Module({
  controllers: [CategoryReportController],
  providers: [CategoryReportService]
})
export class CategoryReportModule {}
