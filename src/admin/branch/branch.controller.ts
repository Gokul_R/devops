import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { BranchService } from './branch.service';
import { CreateBranchDto, GetBranchDto, GetServicategoryDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import {
  EC200,
  EC409,
  EM100,
  EM102,
  EM104,
  EM106,
  EM116,
  EM126,
  EM127,
  VENDOR_ID,
} from 'src/core/constants';
import { getBranchListedto } from './dto/get-service-dto';
import { BaseController } from 'src/base.controller';
import { RejectBranchOrderDto } from './dto/rejectOrder.dto';
import Branch from 'src/core/database/models/Branch';

@ApiTags('Branch')
@Controller('branch')
export class BranchController extends BaseController<CreateBranchDto> {
  constructor(private readonly branchService: BranchService) {
    super(branchService);
  }
  @Post()
  async create(@Body() createBranchDto: CreateBranchDto) {
    try {
      const existingBranch = await Branch.findOne({ where: { branch_email: createBranchDto.general_details.branch_email } });
      if (existingBranch && existingBranch?.dataValues) {
        return HandleResponse.buildErrObj(EC409, EM102, null);
      }
      let data = await this.branchService.create(createBranchDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @ApiQuery({
    name: 'status',
    type: String,
    description: 'Approval Status',
    required: false,
  })
  @Get()
  async findAll(@Query('status') status?: number) {
    try {
      let data = await this.branchService.findAll(status);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Get('by-service-category/:service_category_id')
  async findByServiceCat(@Param('service_category_id') service_category_id: string) {
    try {
      let data = await this.branchService.findByServiceCat(service_category_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @ApiParam({ name: 'vendor_id', example: VENDOR_ID })
  @Get('by-vendor/:vendor_id')
  async findAllByVendorId(@Param('vendor_id') vendor_id: string) {
    try {
      let data = await this.branchService.findAllByVendorId(vendor_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @ApiParam({ name: 'vendor_id', example: VENDOR_ID })
  @Get('child-vendor/:vendor_id')
  async findAllByChildVendorId(@Param('vendor_id') vendor_id: string) {
    try {
      let data = await this.branchService.findAllChildByVendorId(vendor_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('by-vendor')
  async findAllByVendorIdNew(@Body() req_body: GetBranchDto) {
    try {
      let data = await this.branchService.findAllByVendorIdNew(req_body);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  async findBranch(@Param('id') id: string) {
    try {
      let data = await this.branchService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBranchDto: UpdateBranchDto,
  ) {
    try {
      let data = await this.branchService.update(id, updateBranchDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, error?.message || EM100, error?.original?.detail || error);
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let data = await this.branchService.destroy(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('search')
  async getFilterList(@Body() branchDetails: getBranchListedto) {
    try {
      let data = await this.branchService.getFilterList(branchDetails);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('reject-order')
  async cancelOrder(
    @Body() rejectOrderDto: RejectBranchOrderDto) {
    try {
      let data = await this.branchService.rejectOrder(rejectOrderDto);
      return HandleResponse.buildSuccessObj(EC200, EM126, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @ApiQuery({
    name: 'vendor_type',
    type: String,
    description: 'Type of vendor freelancer or business',
    required: true,
  })
  @Get('slot-availabilitys/:date/:branch_id')
  async getSlotAvailability(@Param('date') date: string, @Param('branch_id') branch_id: string, @Query('vendor_type') vendor_type: string) {
    try {
      let data = await this.branchService.getSlotAvailability(date, branch_id, vendor_type);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      // if (error?.name === 'NotFoundException')
      //   return HandleResponse.buildErrObj(EC204, error.message, error.message);
      // else 
      return HandleResponse.buildErrObj(error.status, error.message, error);
    }
  }


  @Post('by-branchId-categoryId')
  async orderinAppointment(@Body() getServicategoryDto: GetServicategoryDto) {
    try {
      let data = await this.branchService.branchCategoryByserviceCategory(getServicategoryDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Post('order-appointment')
  // async orderinAppointment(
  //   @Body() orderinAppointments: OrderinAppointments) {
  //   try {
  //     let data = await this.branchService.orderinAppointment(orderinAppointments);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }
}
