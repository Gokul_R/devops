import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CreateBranchDto, GetServicategoryDto } from './dto/create-branch.dto';
import Helpers from 'src/core/utils/helpers';

import {
  Approval,
  BRANCH_CATEGORY_REPOSITORY,
  BRANCH_REPOSITORY,
  BRANCH_SERVICE_CATEGORY_REPOSITORY,
  BRANCH_SERVICE_TYPE_REPOSITORY,
  EM143,
  EM144,
  Payment_Status,
  Provided_By,
  SLOT_DETAILS_REPOSITORY,
} from 'src/core/constants';
//import { CreateReviewDto } from './dto/create-review.dto';
import Branch from 'src/core/database/models/Branch';
import Location from 'src/core/database/models/Location';
//import { UsersService } from '../users/users.service';
//import { GetServicesDto } from './dto/get-services-dto';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import SlotDetail from 'src/core/database/models/SlotDetail';
import { BranchServiceCategoryService } from '../branch-service-category/branch-service-category.service';
import { BranchCategoryService } from '../branch-category/branch-category.service';
import { BranchServiceTypeService } from '../branch-service-type/branch-service-type.service';
import { BranchSlotDetailsService } from '../branch-slot-details/branch-slot-details.service';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { UpdateBranchDto } from './dto/update-branch.dto';
import BranchServiceCategoryMapping from 'src/core/database/models/BranchServiceCategoryMapping.ts';
import BranchCategoryMapping from 'src/core/database/models/BranchCategoryMapping';
import BranchServiceTypeMapping from 'src/core/database/models/BranchServiceTypeMapping';
import Vendor from 'src/core/database/models/Vendor';
import { VENDOR_ATTRIBUTES } from 'src/core/attributes/vendor';
import BranchBankDetails from 'src/core/database/models/BranchBankDetails';
import { Op, Sequelize, col, fn, where } from 'sequelize';
import sequelize from 'sequelize';
import { LOCATION_ATTRIBUTES } from 'src/core/attributes/location';
import { SLOT_DETAIL_ATTRIBUTES } from 'src/core/attributes/slot_detail';
import Appointment, { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import Order from 'src/core/database/models/Order';
import { OrderinAppointments } from './dto/create-slot';
import moment from 'moment';
import { retry } from 'rxjs';
import { DateUtils } from 'src/core/utils/dateUtils';
import { Errors } from 'src/core/constants/error_enums';
import { MailUtils } from 'src/core/utils/mailUtils';
import { PayoutPaymentService } from '../payout/payout.payment.service';
import { RejectBranchOrderDto } from './dto/rejectOrder.dto';
import PaymentConfig from 'src/core/database/models/PaymentConfig';
import { logger } from 'src/core/utils/logger';
import Payment_Txn from 'src/core/database/models/Payment-Tnx';

import User from 'src/core/database/models/User';
import UserAddress from 'src/core/database/models/UserAddress';
import Service from 'src/core/database/models/Services';
import OrderService from 'src/core/database/models/OrderServices';
import { getBranchListedto } from './dto/get-service-dto';
import OrderRemainder from 'src/core/database/models/OrderRemainder';
import SlotManagement from 'src/core/database/models/SlotManagement';
import { ORDER_ATTRIBUTES } from 'src/core/attributes/order';
import { SendPushNotification } from 'src/core/utils/sendPushNotification';
import CategoryServiceCategoryMappings from 'src/core/database/models/CategoryServiceCatMapping';
import MasterCategory from 'src/core/database/models/MasterCategory';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import { FreelancerSlot, VendorType } from 'src/core/utils/enum';
import { CreateFreelancerDto } from '../freelancer/dto/create-freelancer.dto';
import { VendorService } from '../vendor/vendor.service';
import VendorRole from 'src/core/database/models/VendorRoles';

@Injectable()
export class BranchService extends BaseService<Branch> {
  protected model: ModelCtor<Branch>;
  branchQry: SequelizeFilter<any>;
  excluded: string[];
  constructor(
    private readonly branchServiceCatService: BranchServiceCategoryService,
    private readonly branchCatService: BranchCategoryService,
    private readonly branchServiceTypeService: BranchServiceTypeService,
    private readonly slotDetailsService: BranchSlotDetailsService,
    @Inject(BRANCH_REPOSITORY)
    private readonly branchRepo: typeof Branch,
    @Inject(BRANCH_SERVICE_CATEGORY_REPOSITORY)
    private readonly branchServiceCatRepo: typeof BranchServiceCategoryMapping,
    @Inject(BRANCH_CATEGORY_REPOSITORY)
    private readonly branchCatRepo: typeof BranchCategoryMapping,
    @Inject(BRANCH_SERVICE_TYPE_REPOSITORY)
    private readonly branchServiceTypeRepo: typeof BranchServiceTypeMapping,
    @Inject(SLOT_DETAILS_REPOSITORY)
    private readonly slotRepo: typeof SlotDetail,
  ) {
    super();
    this.model = this.branchRepo;
  }
  init() {
    this.excluded = [
      // 'is_deleted',
      // 'is_active',
      // 'createdAt',
      'updatedAt',
      // 'deletedAt',
    ];
    this.branchQry = {
      attributes: {
        exclude: this.excluded,
      },
      include: [
        {
          attributes: ['vendor_name'],
          model: Vendor,
        },
        {
          attributes: {
            exclude: this.excluded,
          },
          model: Location,
        },
        {
          attributes: {
            exclude: this.excluded,
          },
          model: this.slotRepo,
        },
      ],
      where: { approval_status: Approval.ACCEPTED, is_deleted: false },
    };
  }
  async create(createBranchDto: CreateBranchDto) {
    this.init();
    let { general_details, slot_timings, bank_details } = createBranchDto;
    //insert branch data
    let branch = await super.create({ ...general_details, ...slot_timings });
    let {area} = general_details;
    delete general_details?.area;

    //mapping categories,serviceCategories,slot details,
    await this.mapBranchData(createBranchDto, branch.id, false);

    //insert Bank details
    let bank_details_res: BranchBankDetails = await Helpers.create(BranchBankDetails, {
      ...bank_details,
      branch_id: branch.id,
    });

    //insert shop location
    let location = await Helpers.create(Location, {
      ...general_details,
      area:area.toLowerCase(),
      branch_id: branch.id,
    });

    //send response
    let res = {
      shop: branch,
      // categories: categoriesPromise,
      // service_categories_data: serviceCategoriesPromise,
      // service_types: serviceTypePromise,
      location: location,
      // slots: slotDetailsPromise,
      slots: bank_details_res,
    };
    return res;
  }

  async findAllByVendorId(vendor_id: any): Promise<Branch[]> {
    // this.init();
    // this.branchQry['where'] = {
    //   ...this.branchQry.where,
    //   vendor_id: vendor_id,
    //   // approval_status: req_body.status,
    let condition = {
      where: {
        [Op.and]: [
          { approval_status: Approval.ACCEPTED },
          { is_deleted: false },
          { is_active: true },
          // { deleted_at: null },
          { vendor_id: vendor_id },
        ],
      },
    };
    // };
    let query = {
      include: [
        {
          model: SlotDetail,
          attributes: ['id', 'slot_start_time', 'slot_end_time', 'no_of_employees', 'day'],
          // where: {
          //   deleted_at: null,
          // },
        },
        {
          model: Location,
          attributes: [
            'id',
            'area',
            'city',
            'pincode',
            'state',
            'country',
            'address',
            'latitude',
            'longitude',
            'branch_id',
          ],
        },
      ],
      ...condition,
    };
    // this.branchQry['include'] = this.branchQry?.include[0]; //include only location data
    return await super.findAll(query);
  }

  async findAllChildByVendorId(vendor_id: any) {
    let vendorData = await VendorRole.findOne({ attributes: { exclude: ['password'] }, where: { id: vendor_id } });
    if (!vendorData) return vendorData;
    let branchDeatails = await Branch.findAll({ attributes: ['id', 'branch_name'], where: { id: vendorData.branch_ids } });
    vendorData = { ...vendorData.dataValues, branches: branchDeatails };
    return vendorData;
  }
  async findByServiceCat(service_cat_id: string): Promise<Branch[]> {
    return await this.branchServiceCatService.findAllByBranchOrServiceCat(null, service_cat_id);
  }
  async findAllByVendorIdNew(req_body: any): Promise<Branch[]> {
    // this.init();
    // this.branchQry['where'] = {
    //   ...this.branchQry.where,
    //   vendor_id: req_body.vendor_id,
    //   approval_status: req_body.status,
    // };
    let query = {
      include: [
        {
          model: SlotDetail,
          attributes: ['id', 'slot_start_time', 'slot_end_time', 'no_of_employees', 'day'],
          where: {
            deleted_at: null,
          },
        },
        {
          model: Location,
          attributes: [
            'id',
            'area',
            'city',
            'pincode',
            'state',
            'country',
            'address',
            'latitude',
            'longitude',
            'branch_id',
          ],
        },
      ],
      where: {
        vendor_id: req_body.vendor_id,
        approval_status: req_body.status,
        is_deleted: false,
      },
    };
    // this.branchQry['include'] = this.branchQry?.include[0]; //include only location data
    return await super.findAndCountAll(query);
  }

  async findAll(status: any): Promise<Branch[]> {
    // this.init();
    // this.branchQry['include'] = this.branchQry?.include[0]; //include only location data
    // this.branchQry.where = {
    //   ...this.branchQry.where,
    //   approval_status: status ? status : Approval.ACCEPTED,
    // };
    let query = {
      include: [
        {
          model: SlotDetail,
          attributes: ['id', 'slot_start_time', 'slot_end_time', 'no_of_employees', 'day'],
          where: {
            deleted_at: null,
          },
        },
        {
          model: Location,
          attributes: [
            'id',
            'area',
            'city',
            'pincode',
            'state',
            'country',
            'address',
            'latitude',
            'longitude',
            'branch_id',
          ],
        },
      ],
      where: {
        approval_status: Approval.ACCEPTED,
        is_deleted: false,
      },
    };
    return await super.findAll(query);
  }
  async findOne(id: string): Promise<Branch> {
    this.init();
    this.branchQry['where'] = {
      id: id,
    };
    this.branchQry.include = [
      ...this.branchQry.include,
      {
        attributes: {
          exclude: this.excluded,
        },
        model: Vendor,
      },
      {
        attributes: {
          exclude: this.excluded,
        },
        model: BranchBankDetails,
      },
    ];
    let where = {
      attributes: { exclude: this.excluded },
      where: { branch_id: id },
    };
    let [categories, service_categories, service_type] = await Promise.all([
      this.branchCatService.findAll(where),
      this.branchServiceCatService.findAll(where),
      this.branchServiceTypeService.findAll(where),
    ]);
    let branch = await super.findOne(id, this.branchQry);
    branch = JSON.stringify(branch);
    branch = JSON.parse(branch);
    if (branch) {
      branch.categories = categories;
      branch.service_categories = service_categories;
      branch.service_types = service_type;
    }
    return branch;
  }

  async update(id: string, updateBranchDto: UpdateBranchDto): Promise<any> {
    this.init();
    let general_details = updateBranchDto?.general_details;
    let slot_timings = updateBranchDto?.slot_timings;
    let updateData = await this.mapBranchData(updateBranchDto, id, true);
    let {area} = general_details;
    delete general_details?.area;
    const getBankDetails = await BranchBankDetails.findOne({ attributes: ['account_no', 'ifsc_code'], where: { branch_id: id } });
    let vendorId = await Branch.findOne({ attributes: ['vendor_id', 'vendor_type'], where: { id: id } });
    if (vendorId?.vendor_type == VendorType.FREELANCER) {
      await Vendor.update({ ...general_details, vendor_name: general_details.branch_name }, { where: { id: vendorId?.vendor_id } });
    }
    if (updateBranchDto?.bank_details) {
      let bank_details_update = await Helpers.update(
        BranchBankDetails,
        { where: { branch_id: id } },
        updateBranchDto.bank_details,
      );
      if (bank_details_update?.length == 0) bank_details_update = await Helpers.create(BranchBankDetails, { ...updateBranchDto?.bank_details, branch_id: id });
    }
    let branch = await super.update(id, {
      ...general_details,
      ...slot_timings,
      ...updateBranchDto,
    });

    if (updateBranchDto?.business_verified && updateBranchDto?.approval_status) {
      await Vendor.update({ approval_status: updateBranchDto?.approval_status, is_approved: true }, { where: { id: vendorId?.vendor_id } })
      let vendor = await Vendor.findOne({ where: { id: vendorId?.vendor_id, password_sent: false } });
      if (vendor) {
        new VendorService(null).sendPwdToVendor(vendor?.email_id, vendorId?.vendor_id);
        await Vendor.update({ password_sent: true }, { where: { id: vendorId?.vendor_id } });
      }

    }
    //update shop location
    let location = await Helpers.update(
      Location,
      { where: { branch_id: id } },
      {
         area:area.toLowerCase(),
        ...general_details,
      },
    );
    if (location?.length == 0) location = await Helpers.create(Location, { ...general_details, branch_id: id });

    //create bank account in razorpay
    if (updateBranchDto?.approval_status && updateBranchDto?.business_verified) {//while approve 
      const createBankAccountToRazorpay = await new PayoutPaymentService().bankAccountRegisterToRazorpay({
        branch_id: id,
      });
    } else if (updateBranchDto?.bank_details) { //exiting branch update
      const getOldFunId = await Branch.findOne({ attributes: ['account_verified', 'fund_account_id'], where: { id: id } });
      if (getBankDetails?.account_no != updateBranchDto?.bank_details?.account_no || getBankDetails?.ifsc_code != updateBranchDto?.bank_details?.ifsc_code || !getOldFunId?.fund_account_id || !getOldFunId.account_verified) {
        const createBankAccountToRazorpay = await new PayoutPaymentService().bankAccountRegisterToRazorpay({
          branch_id: id,
        });
      }
    }
    return branch;
  }

  async mapBranchData(createBranchDto: CreateBranchDto | CreateFreelancerDto, branch_id: string, is_update: boolean) {
    if (createBranchDto.general_details && createBranchDto.slot_timings) {
      let { general_details, slot_timings } = createBranchDto;
      const { service_categories, categories, service_type } = general_details;
      let { slot_details } = slot_timings;
      // const promises: Promise<any>[] = [];
      // let serviceCategoriesPromise: Promise<any>,
      //   categoriesPromise: Promise<any>,
      //   slotDetailsPromise: Promise<any>,
      //   serviceTypePromise: Promise<any>;

      //for update delete old records and create new records
      let branch_whr = { where: { branch_id: branch_id }, force: true };
      if (is_update) {
        // await this.branchCatService.destroy(null, branch_whr);
        // await this.branchServiceCatService.destroy(null, branch_whr);
        // await this.branchServiceTypeService.destroy(null, branch_whr);
        // await this.slotDetailsService.destroy(null, branch_whr);
        await Service.destroy({
          where: {
            [Op.or]: [
              {
                category_id: {
                  [Op.notIn]: categories
                }
              },
              {
                service_category_id: {
                  [Op.notIn]: service_categories
                }
              }
            ],
            [Op.and]: [{ branch_id: branch_id }]
          }
        });
      }

      // service categroy mapping
      if (service_categories.length > 0) {
        is_update && await this.branchServiceCatService.destroy(null, branch_whr);
        const branchServiceCategories = service_categories.map((servCategoryId) => {
          return {
            service_category_id: servCategoryId,
            branch_id: branch_id,
          };
        });

        let serviceCategoriesPromise = await this.branchServiceCatService.bulkCreate(branchServiceCategories);
        // promises.push(serviceCategoriesPromise);
      }

      // service categroy mapping
      if (categories.length > 0) {
        is_update && await this.branchCatService.destroy(null, branch_whr);
        const branchCategories = categories.map((categoryId) => {
          return {
            category_id: categoryId,
            branch_id: branch_id,
          };
        });
        let categoriesPromise = await this.branchCatService.bulkCreate(branchCategories);
        // promises.push(categoriesPromise);
      }

      // service type mapping
      if (service_type.length > 0) {
        is_update && await this.branchServiceTypeService.destroy(null, branch_whr);
        const branchServiceTypes = service_type.map((typeId) => {
          return {
            service_type_id: typeId,
            branch_id: branch_id,
          };
        });
        let serviceTypePromise = await this.branchServiceTypeService.bulkCreate(branchServiceTypes);
        // promises.push(serviceTypePromise);
      }

      // slot details mapping
      if (slot_details.length >= 0) {
        is_update && await this.slotDetailsService.destroy(null, branch_whr);
        const branchSlotDetails = slot_details.map((slots) => {
          return {
            ...slots,
            branch_id: branch_id,
          };
        });
        let slotDetailsPromise = await this.slotDetailsService.bulkCreate(branchSlotDetails);
        // promises.push(slotDetailsPromise);
      }
      // await Promise.all(promises);
    }
  }

  async getFilterList(req_data: getBranchListedto): Promise<any> {
    this.init();
    let search: any;
    let ac_verify_status: any =
      req_data?.ac_verify_status != null || req_data?.ac_verify_status != undefined
        ? { account_verified: req_data?.ac_verify_status }
        : '';
    if (req_data.searchText) {
      search = {
        // [Op.or]: [
        //   fn('LOWER', col('branch_name')), // Convert column value to lowercase
        //   fn('LOWER', col('branch_email')), // Convert column value to lowercase
        //   fn('LOWER', col('branch_contact_no')), // Convert column value to lowercase
        //   fn('LOWER', col('vendor_type')), // Convert column value to lowercase
        // ],
        [Op.or]: [
          {
            branch_name: {
              [Op.iLike]: `%${req_data.searchText.toLowerCase()}%`,
            },
          }, // Convert search value to lowercase
          {
            branch_email: {
              [Op.iLike]: `%${req_data.searchText.toLowerCase()}%`,
            },
          }, // Convert search value to lowercase
          {
            branch_contact_no: {
              [Op.iLike]: `%${req_data.searchText.toLowerCase()}%`,
            },
          }, // Convert search value to lowercase
          {
            vendor_type: {
              [Op.iLike]: `%${req_data.searchText.toLowerCase()}%`,
            },
          }, // Convert search value to lowercase
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let vendor_id = req_data.vendor_id ? { vendor_id: req_data.vendor_id } : {};
    let vendorType = req_data.vendor_type ? { vendor_type: req_data.vendor_type } : {};
    let where = {
      ...vendor_id,
      approval_status: req_data.approval_status,
      is_deleted: false,
      ...ac_verify_status,
      ...vendorType,
      ...search,
    };

    this.branchQry.where = where;
    this.branchQry.offset = offset;
    this.branchQry.limit = limit;
    this.branchQry.order = [['created_at', 'DESC']];
    const data = await super.findAll(this.branchQry);
    const count = await Branch.count({
      where: where,
    });
    return { rows: data, count: count };
  }

  async rejectOrder(rejectOrderDto: RejectBranchOrderDto) {
    logger.info('reject_order_entry: ' + JSON.stringify(rejectOrderDto));
    let cancel_query = {
      where: {
        order_id: rejectOrderDto.order_id,
      },
      paranoid: false,
    };
    let order: Order[] = await Helpers.update(Order, cancel_query, {
      ...rejectOrderDto,
      order_status: APPOINTMENT_STATUS.REJECTED,
      cancelled_by: rejectOrderDto.rejected_by,
      cancelled_user_id: rejectOrderDto.rejected_user_id,
      is_cancelled: true,
    });
    if (order.length == 0) throw Error(Errors.INVALID_ORDER_DETAILS);
    await Helpers.update(Appointment, cancel_query, {
      ...rejectOrderDto,
      appointment_status: APPOINTMENT_STATUS.REJECTED,
      is_cancelled: true,
    });
    let { customer_email } = order[0];
    let data: Order[] = await this.rejectorderPrepareContent(rejectOrderDto.order_id);
    await new MailUtils().OrderRejectedMailVendorUser(data, rejectOrderDto.remarks); //Mail to vendor
    await new MailUtils().OrderRejectedMailVendorUser(data, rejectOrderDto.remarks, customer_email); //Mail to user
    // new CronUtils(new SchedulerRegistry()).deleteCron(rejectOrderDto.order_id);
    await this.initiateRefund(order[0]);
    this.sendNotifyToUser(rejectOrderDto?.order_id, order[0]?.user_id, order[0]?.customer_name, order[0]?.appointment_start_date_time);
    this.deleteOrderRemainder(rejectOrderDto.order_id);
    logger.info('reject_order_exit: ' + JSON.stringify(order));
    return order;
  }

  public deleteOrderRemainder(order_id: string) {
    OrderRemainder.destroy({ force: true, where: { order_id: order_id } });
  }
  async initiateRefund(order: Order): Promise<void> {
    logger.info('reject_order_initiate_entry: ' + JSON.stringify(order));
    let { appointment_start_date_time, final_price, cancellation_fee, cancelled_by } = order;
    // The branch rejected the order so the full amount will be refunded
    let full_refund: boolean = true;
    if (cancellation_fee && cancelled_by == Provided_By.SUPER_ADMIN) {
      let diff: number = DateUtils.findDiffInHrs(new Date(appointment_start_date_time), new Date());
      if (diff < 24 && diff > 0) {
        logger.info('reject_order_with_in_24h_entry: ');
        const getPaymentConfig = await PaymentConfig.findOne({
          attributes: ['within_24h_pct', 'before_24h_pct', 'rejectedby_admin_pct'],
        });
        if (!getPaymentConfig) throw new Error('The cancellation deduction percentage is not defined');
        let deductPct = Number((getPaymentConfig.within_24h_pct / 100).toFixed(2));
        final_price = deductPct > 0 ? final_price - final_price * deductPct : final_price;
        full_refund = false;
      } else if (diff < 0) {
        logger.info('reject_order_without_24h_entry: ');
        final_price = 0;
      }
    }
    final_price = Number(final_price) * 100;
    logger.info('reject_order_final_price: ' + JSON.stringify(final_price));
    if (final_price <= 100) {
      // update refunded status
      const updatePaymentTnx = await Payment_Txn.update(
        { refund_status: 'processed', amount_refunded: final_price },
        { where: { order_id: order.order_id, user_id: order.user_id } },
      );

      const updateOrderData = await Order.update(
        { refund_status: 'processed', refund_amount: final_price, refunded: true },
        { where: { order_id: order.order_id, user_id: order.user_id } },
      );
      logger.info('reject_order_success_low_amount_exit: ' + JSON.stringify(final_price));
      return;
      // throw new Error(EM144);//
    } else {
      if (order?.payment_status == Payment_Status?.PaymentSuccess && order?.payment_id) {
        let req = {
          amount: final_price, //// TODO Check whether there is cancellation fee or not
          user_id: order.user_id,
          order_id: order.order_id,
          payment_id: order.payment_id, // TODO change the pay_id
          full_refund: full_refund, //full_refund  //// TODO Check whether there is cancellation fee or not
        };
        logger.info('reject_order_instantRefundAmountToUser_entry: ' + JSON.stringify(req));
        const sendRefundAmount = await new PayoutPaymentService().instantRefundAmountToUser(req);
        logger.info('reject_order_instantRefundAmountToUser_exit: ' + JSON.stringify(sendRefundAmount));
        return;
      } else {
        logger.info('reject_order_user_not_paid_exit: ' + JSON.stringify(order));
        throw new Error(EM143);
      }
    }
  }
  async rejectorderPrepareContent(orderId: string) {
    let ContentData = await Order.findAll({
      attributes: [
        'order_id',
        'service_id',
        'user_id',
        'service_type',
        'payment_id',
        'final_price',
        'payment_method',
        'created_at',
        'customer_name',
        'customer_email',
        'customer_mobile',
      ],
      include: [
        {
          model: OrderService,
          attributes: ['service_id'],
          required: true,
          paranoid: false,
          include: [
            {
              model: Service,
              attributes: ['service_name'],
              paranoid: false,
            },
          ],
        },

        {
          model: Appointment,
          attributes: ['appointment_start_date_time'],
          include: [],
          paranoid: false,
        },
        {
          model: Branch,
          attributes: ['branch_name', 'branch_email'],
          paranoid: false,
        },
        {
          model: UserAddress,
          attributes: ['flat_no', 'area', 'city', 'zipcode', 'state', 'country', 'customer_name', 'customer_mobile'],
          paranoid: false,
        },
      ],
      where: {
        order_id: orderId,
      },
      paranoid: false
    });
    return ContentData;
  }

  async getSlotAvailability(date: string, branch_id: string, vendor_type: any) {
    // date = moment(date).format("YYYY/MM/DD")
    logger.info(
      `Shop_GetSlotAvailability_Entry_BranchId: ` + JSON.stringify(branch_id) + 'data:' + JSON.stringify(date),
    );
    let orders = await this.getOrdersByDate(date, vendor_type);
    const d = new Date(date);
    let day = d.getDay();
    let slotsDetails: SlotDetail | SlotManagement = await this.getSlotDetails(branch_id, d, day);
    if ('is_leave' in slotsDetails && slotsDetails.is_leave) {
      logger.info(`Shop_GetSlotAvailability_Leave_Exit: `);
      return [];
    }
    // else if ('is_leave' in slotsDetails && !slotsDetails.is_leave && vendor_type === VendorType.FREELANCER) { // sasi added this else if condition
    //   let slots = await this.getSlots(branch_id, day);
    //   if (slots && slots.length > 0) {
    //     slotsDetails = slots[0];
    //   } else {
    //     throw new NotFoundException(Errors.NO_SLOTS_AVAILABLE);
    //   }
    // }

    //TODO send freelancer flow slot timing - // sasi added this freelancer function
    if (vendor_type === VendorType.FREELANCER) {
      return this.freelancerSlotDetails(orders, slotsDetails);
    }
    //--------------
    let slots = await this.splitSlotDetails(slotsDetails);
    orders = JSON.stringify(orders);
    orders = JSON.parse(orders);
    return this.decreaseStaffCount(orders, slots);
  }

  freelancerSlotDetails(orders: any, slot_detail: any) {
    let data: any = { morning: slot_detail?.morning, afternoon: slot_detail?.afternoon, evening: slot_detail?.evening };
    if (orders && orders.length > 0) {
      orders?.map((order: any) => {
        if (order?.booking_slot == FreelancerSlot.MORNING) data.morning = false
        else if (order?.booking_slot == FreelancerSlot.AFTERNOON) data.afternoon = false
        else if (order?.booking_slot == FreelancerSlot.EVENING) data.evening = false
      });
    } else {
      data.morning = slot_detail?.morning ? true : false;
      data.afternoon = slot_detail?.afternoon ? true : false;
      data.evening = slot_detail?.evening ? true : false;
    }
    return [data];
  }

  async getSlotDetails(branch_id: string, date: Date, day: number): Promise<SlotDetail | SlotManagement> {
    let slotsDetails: SlotDetail | SlotManagement;
    slotsDetails = await this.findSlotManagementByBranch(branch_id, date);
    if (!slotsDetails) {
      let slots = await this.getSlots(branch_id, day);

      if (slots && slots.length > 0) {
        slotsDetails = slots[0];
      } else {
        throw new NotFoundException(Errors.NO_SLOTS_AVAILABLE);
      }
    }
    return slotsDetails;
  }
  async findSlotManagementByBranch(branch_id: string, date: Date): Promise<SlotDetail | SlotManagement> {
    let filter = {
      branch_id: branch_id,
      date: {
        [Op.eq]: date,
      },
    };
    let slot_management: SlotManagement = await SlotManagement.findOne({ where: filter });
    return slot_management;
  }
  private decreaseStaffCount(orders: any, slots: any[]) {
    logger.info(`Decrease_Staff_Count_Entry_order: ` + JSON.stringify(orders) + 'slots' + JSON.stringify(slots));
    for (const appointment of orders) {
      for (const slot of slots) {
        let start_time = moment(slot.slot_start_time, 'HH:mm a');
        slot.slot_end_time = start_time.add('60', 'minutes').format('HH:mm a');
        const { slot_start_time, slot_end_time } = slot;
        const { appointment_start_date_time } = appointment;
        const appointmentDateTime = new Date(appointment_start_date_time);
        const isBetweenSlotTimes = DateUtils.isBetweenSlotTimes(slot_start_time, slot_end_time, appointmentDateTime);
        if (isBetweenSlotTimes) {
          slot.no_of_employees = slot.no_of_employees - 1;
        }
        slot.slot_start_time = moment(slot.slot_start_time, 'HH:mm a').format('hh:mm A');
        slot.slot_end_time = moment(slot.slot_end_time, 'HH:mm a').format('hh:mm A');
      }
    }
    logger.info(`Decrease_Staff_Count_Exit: ` + JSON.stringify(slots));
    return slots;
  }

  async splitSlotDetails(slots: SlotDetail | SlotManagement) {
    logger.info(`splitSlotDetails_Entry: ` + JSON.stringify(slots));
    let availableSlots = [];
    let start_time = moment(slots.slot_start_time, 'HH:mm a');
    let end_time = moment(slots.slot_end_time, 'HH:mm a');
    availableSlots.push({
      slot_start_time: start_time.format('HH:mm a'),
      no_of_employees: slots.no_of_employees,
    });
    let diff = moment.duration(end_time.diff(start_time));
    let hours = Math.abs(diff.asHours());
    for (let index = 0; index < Math.floor(hours); index++) {
      start_time = start_time.add('60', 'minutes');
      availableSlots.push({
        slot_start_time: start_time.format('hh:mm a'),
        no_of_employees: slots.no_of_employees,
      });
    }
    //insert slot end time at last position
    if (availableSlots.length > 0) {
      console.log('test' + end_time);
      // availableSlots[availableSlots.length - 1].slot_start_time = end_time.format('hh:mm a');
      // end_time.isSame(moment('11:59 PM', 'HH:mm a')) && availableSlots.pop();
    }
    logger.info(`splitSlotDetails_Exit: ` + JSON.stringify(availableSlots));
    return availableSlots;
  }

  async getOrdersByDate(date: string, vendor_type: string) {
    let appointmentStatus = vendor_type == VendorType.FREELANCER ? [APPOINTMENT_STATUS.CONFIRMED, APPOINTMENT_STATUS.AWAITING_APPROVAL] : [APPOINTMENT_STATUS.CONFIRMED]
    const startOfDay = new Date(date);
    startOfDay.setUTCHours(0, 0, 0, 0);

    const endOfDay = new Date(date);
    endOfDay.setUTCHours(23, 59, 59, 999);
    let order_qry: SequelizeFilter<Order> = {
      attributes: [ORDER_ATTRIBUTES.appointment_start_date_time, ORDER_ATTRIBUTES.booking_slot],
      where: {
        appointment_start_date_time: {
          [Op.gte]: startOfDay, // Greater Than or Equal to the start of the day
          [Op.lt]: endOfDay, // Less Than the end of the day
        },
        order_status: appointmentStatus,
      },
    };
    let orders = await Helpers.findAll(Order, order_qry);
    return orders;
  }
  async getSlots(branch_id: string, day?: number) {
    logger.info(`Get_Shop_Slots_Entry_branchId: ` + JSON.stringify(branch_id) + 'day:' + JSON.stringify(day));
    if (day != undefined && day != null) {
      if (day == 0) day = 7;
      day--;
    }
    let slotQry: SequelizeFilter<SlotDetail> = {
      // attributes: ['open_at', 'close_at', 'day'],
      where: {
        branch_id: branch_id,
      },
    };
    if (day != null || day != undefined) slotQry.where.day = day;
    let slots = await Helpers.findAll(SlotDetail, slotQry);
    logger.info(`Get_Shop_Slots_Exit: ` + JSON.stringify(slots));
    return slots;
  }
  async sendNotifyToUser(orderId: string, usersId: string, userName: string, appointment: any) {
    let reqObj = {
      user_id: usersId,
      title: `Hello ${userName || 'Slaylewks users'} 💇 Your order has been rejected!`,
      body: `We regret to inform you that your recent order with ID (${orderId}) has been rejected. Our team has reviewed the order, and unfortunately, we are unable to process it at this time.`,
      data: { order_id: orderId, Time: appointment, user_id: usersId },
      dataModels: User
    };
    await new SendPushNotification().sendNotifyToUserPhone(reqObj);
  }




  async branchCategoryByserviceCategory(getServicategoryDto: GetServicategoryDto) {
    let data = await CategoryServiceCategoryMappings.findAll({
      attributes: ['category_id', 'service_category_id'],
      where: { category_id: getServicategoryDto?.category_ids },
      include: [{
        attributes: ['id', 'category', 'code'],
        model: MasterCategory,
        include: [{
          attributes: [],
          model: BranchCategoryMapping,
          where: { branch_id: getServicategoryDto?.branch_id }
        }]
      },
      {
        attributes: ['id', 'service_category_name', 'code'],
        model: MasterServiceCategory,
        include: [{
          attributes: [],
          model: BranchServiceCategoryMapping,
          where: { branch_id: getServicategoryDto?.branch_id }
        }]
      }],

    });
    return data;
  }

}


