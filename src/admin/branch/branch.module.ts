import { Module } from '@nestjs/common';
import { BranchService } from './branch.service';
import { BranchController } from './branch.controller';
import { branchProviders } from './branch.provider';
import { BranchServiceCategoryModule } from '../branch-service-category/branch-service-category.module';
import { BranchCategoryModule } from '../branch-category/branch-category.module';
import { BranchServiceTypeModule } from '../branch-service-type/branch-service-type.module';
import { BranchSlotDetailsModule } from '../branch-slot-details/branch-slot-details.module';

@Module({
  imports: [
    BranchServiceCategoryModule,
    BranchCategoryModule,
    BranchServiceTypeModule,
    BranchSlotDetailsModule
  ],
  controllers: [BranchController],
  providers: [BranchService, ...branchProviders],
  exports:[BranchService]
})
export class BranchModule {}
