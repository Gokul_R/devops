import { BRANCH_CATEGORY_REPOSITORY, BRANCH_REPOSITORY, BRANCH_SERVICE_CATEGORY_REPOSITORY, BRANCH_SERVICE_TYPE_REPOSITORY, SLOT_DETAILS_REPOSITORY, VENDOR_REPOSITORY } from 'src/core/constants';
import Branch from 'src/core/database/models/Branch';
import BranchCategoryMapping from 'src/core/database/models/BranchCategoryMapping';
import BranchServiceCategoryMapping from 'src/core/database/models/BranchServiceCategoryMapping.ts';
import BranchServiceTypeMapping from 'src/core/database/models/BranchServiceTypeMapping';
import SlotDetail from 'src/core/database/models/SlotDetail';
import Vendor from 'src/core/database/models/Vendor';
import { commonProvider } from 'src/core/interfaces/common-providers';

export const branchProviders = [
  // {
  //   provide: BRANCH_REPOSITORY,
  //   useValue: Branch,
  // },
  // {
  //   provide: BRANCH_SERVICE_CATEGORY_REPOSITORY,
  //   useValue: BranchServiceCategoryMapping,
  // },
  // {
  //   provide: BRANCH_CATEGORY_REPOSITORY,
  //   useValue: BranchCategoryMapping,
  // },
  // {
  //   provide: BRANCH_SERVICE_TYPE_REPOSITORY,
  //   useValue: BranchServiceTypeMapping,
  // },
  // {
  //   provide: SLOT_DETAILS_REPOSITORY,
  //   useValue: SlotDetail,
  // },
  // {
  //   provide: VENDOR_REPOSITORY,
  //   useValue: Vendor,
  // }
  commonProvider.branch_repository,
  commonProvider.branch_service_category_repository,
  commonProvider.branch_catagory_repository,
  commonProvider.branch_service_type_repository,
  commonProvider.slot_details_repository,
  commonProvider.vendor_repository

];
