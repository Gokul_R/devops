import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString } from 'class-validator';
import { Day, VendorType } from 'src/core/utils/enum';

export class OrderinAppointments {
  @ApiProperty()
  @IsString()
  branch_id: string;

  //   @ApiProperty()
  //   @IsString()
  //   appointment_start_date_time: string;

  @ApiProperty()
  @IsEnum(Day)
  dayofWeek: Day;
}


export class VendorTypeDtoForFreelancer {
  @ApiProperty()
  @IsEnum(VendorType)
  vendor_type: string;
}