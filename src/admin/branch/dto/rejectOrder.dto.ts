import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsOptional, IsString } from "class-validator";
import { BRANCH_ID, ORDER_ID, Provided_By } from "src/core/constants";
import { APPOINTMENT_STATUS } from "src/core/database/models/Appointment";

export class RejectBranchOrderDto {
    @ApiProperty({ example: "Test comments" })
    @IsString()
    @IsNotEmpty()
    remarks: string;

    @ApiProperty({ example: APPOINTMENT_STATUS.REJECTED })
    @IsString()
    @IsNotEmpty()
    order_status?: string;

    @ApiProperty({ example: ORDER_ID })
    @IsString()
    @IsNotEmpty()
    order_id: string;

    @ApiProperty({ example: BRANCH_ID })
    @IsString()
    @IsOptional()
    rejected_user_id: string;

    @ApiProperty({ example: `${Provided_By.SUPER_ADMIN} 'or' ${Provided_By.VENDOR} or ${Provided_By.BRANCH}` })
    @IsString()
    @IsOptional()
    rejected_by: string;

    @ApiProperty({ example: true })
    @IsOptional()
    @IsBoolean()
    cancellation_fee: string;
}