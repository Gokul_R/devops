import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { VENDOR_ID } from "src/core/constants";
import { PaginationDto } from "src/core/interfaces/shared.dto";
import { VendorType } from "src/core/utils/enum";

export class getServicedto { }
export class getBranchListedto extends PaginationDto {
    @ApiProperty({ example: '0 or 1 or 2' })
    @IsNumber()
    @IsNotEmpty()
    approval_status?: number;

    @ApiProperty({ example: VENDOR_ID })
    @IsString()
    @IsOptional()
    vendor_id?: string;

    @ApiProperty({ example: true })
    @IsBoolean()
    @IsOptional()
    ac_verify_status?: boolean;

    @ApiProperty({example:VendorType.FREELANCER})
    @IsEnum(VendorType)
    @IsOptional()
    vendor_type:string
}
