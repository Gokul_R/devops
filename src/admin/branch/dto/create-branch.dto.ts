import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsEmail,
  IsArray,
  IsBoolean,
  IsString,
  IsOptional,
  IsNumber,
  IsEnum,
  Min,
  ValidateNested,
  Length,
  MaxLength,
  MinLength,
  ValidateIf
} from 'class-validator';
import { Approval, BRANCH_ID, VENDOR_ID, slaylewks_commission } from 'src/core/constants';
import { DEFAULT_VALUE } from 'src/core/swaggerDefaultValue/swaggerValue';
import { VendorType } from 'src/core/utils/enum';

export enum CommissionType {
  PERCENTAGE = 'percentage',
  AMOUNT = 'amount',
}
enum ServiceType {
  SHOP = '439c6e27-6e4e-4549-ad6e-99d0390ea277',
  HOME = 'df299029-3fa6-4273-b5ee-78f97dd844f6',
}

export class SlotDetailDto {
  @ApiProperty({ example: 0 })
  @Min(0)
  @IsNotEmpty()
  @IsNumber()
  day: number;

  @ApiProperty({ example: '01:00 PM' })
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @IsNotEmpty()
  @IsString()
  slot_start_time: string;

  @ApiProperty({ example: '04:00 PM' })
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @IsNotEmpty()
  @IsString()
  slot_end_time: string;

  @ApiProperty({ example: 5 })
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @IsNotEmpty()
  @Min(1)
  @IsNumber()
  //@Matches(/^[0-9]+$/)
  no_of_employees: number;

  @ApiProperty({ example: true })
  @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  @IsNotEmpty()
  @IsBoolean()
  morning: string;

  @ApiProperty({ example: true })
  @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  @IsNotEmpty()
  @IsBoolean()
  afternoon: string;

  @ApiProperty({ example: true })
  @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  @IsNotEmpty()
  @IsBoolean()
  evening: number;
}

export class SlotTimingsDto {
  @ApiProperty({ example: '01:00 PM' })
  @ValidateIf((o => o.vendorType == VendorType.BUSINESS))
  @IsNotEmpty()
  @IsString()
  open_time: string;

  @ApiProperty({ example: '04:00 PM' })
  @ValidateIf((o => o.vendorType == VendorType.BUSINESS))
  @IsNotEmpty()
  @IsString()
  close_time: string;

  @ApiProperty({ example: [0, 1, 2, 3, 4, 5, 6] })
  @IsArray()
  @IsOptional()
  leave_days: number[];

  @ApiProperty({ type: [SlotDetailDto] })
  @IsArray()
  slot_details: SlotDetailDto[];
}

export class GeneralDetailsDto {
  @ApiProperty({ example: 'https://firebasestorage.googleapis.com/...' })
  @IsOptional()
  @IsString()
  branch_image: string | null;

  @ApiProperty({ example: DEFAULT_VALUE.BRANCH_NAME })
  @IsString()
  @Length(4, 100, {
    message: 'branch_name must be between 4 and 100 characters',
  })
  @IsNotEmpty()
  branch_name: string;

  @ApiProperty({ example: [DEFAULT_VALUE.SERVICE_TYPE_ID] })
  @IsString({ each: true })
  @IsNotEmpty()
  service_type: string[];

  @ApiProperty({ example: [DEFAULT_VALUE.CATEGORIES] })
  @IsNotEmpty()
  @IsString({ each: true })
  categories: string[];

  @ApiProperty({ example: [DEFAULT_VALUE.SERVICE_CATEGORIES] })
  @IsNotEmpty()
  @IsString({ each: true })
  service_categories: string[];

  @ApiProperty({ example: 'Address' })
  @IsString()
  @MinLength(3, {
    message: 'Address must be at least 4 characters',
  })
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (typeof value !== 'string') return value;
    return value.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
  })
  address: string;

  @ApiProperty({ example: 'Area' })
  @IsString()
  @MinLength(4, {
    message: 'Area must be at least 4 characters',
  })
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (typeof value !== 'string') return value;
    return value.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
  })
  area: string;

  @ApiProperty({ example: 'City' })
  @IsString()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (typeof value !== 'string') return value;
    return value.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
  })
  // @Matches(/^[a-zA-Z]+$/)
  city: string;

  @ApiProperty({ example: 'State' })
  @IsString()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (typeof value !== 'string') return value;
    return value.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
  })
  state: string;

  @ApiProperty({ example: 'Country' })
  @IsString()
  @IsNotEmpty()
  @Transform(({ value }) => {
    if (typeof value !== 'string') return value;
    return value.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
  })
  country: string;

  @ApiProperty({ example: '600034' })
  @IsString()
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @IsNotEmpty()
  @MaxLength(6)
  @MinLength(6)
  pincode: string;

  @ApiProperty({ example: 13.0452745 })
  @IsNumber()
  @IsNotEmpty()
  latitude: number;

  @ApiProperty({ example: 80.1351977 })
  @IsNumber()
  @IsNotEmpty()
  longitude: number;

  @ApiProperty({ example: null })
  @IsOptional()
  website_url: string | null;

  @ApiProperty({ example: true })
  @IsBoolean()
  @IsOptional()
  is_active: boolean;

  @ApiProperty({ example: 'business' })
  @IsNotEmpty()
  @IsString()
  vendor_type: string;

  @ApiProperty({ example: DEFAULT_VALUE.VENDOR_ID })
  @IsString()
  vendor_id: string;

  @ApiProperty({ example: 'Branch Owner' })
  @IsString()
  @IsNotEmpty()
  @Length(3, 100, {
    message: 'contact_person must be between 3 and 100 characters',
  })
  // @Matches(/^[A-Za-z\s]+$/, {
  //   message: 'Only letters are allowed',
  // })
  contact_person: string;

  @ApiProperty({ example: '9087789008' })
  @IsString()
  // @IsPhoneNumber()
  // @IsNotEmpty()
  branch_contact_no: string;

  @ApiProperty({ example: 'newbranch@nainatech.com' })
  @IsEmail()
  @IsNotEmpty()
  branch_email: string;

  @ApiProperty({ example: null })
  @IsOptional()
  // @IsPhoneNumber()
  @IsString()
  secondary_mobile?: string | null;

  @ApiProperty({ example: null })
  @IsOptional()
  @IsString()
  branch_landline: string | null;

  @ApiProperty({ example: false })
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @IsBoolean()
  is_tax_checked: boolean;

  @ApiProperty({ example: '24AAACC1206D1ZM' })
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @ValidateIf((o) => o.is_tax_checked === true)
  @IsNotEmpty()
  @IsString()
  gst_number: string;

  @ApiProperty()
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @ValidateIf((o) => o.is_tax_checked === true)
  @IsNotEmpty()
  @IsString()
  gst_document_url: string;

  @ApiProperty({ example: 0, nullable: true })
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @ValidateIf((o) => o.is_tax_checked === true)
  @IsNumber()
  @IsNotEmpty()
  tax_percentage: number | null;

  @ApiProperty({ example: null })
  @IsNotEmpty()
  @IsEnum(slaylewks_commission)
  @IsString()
  commission_type: string | null;

  @ApiProperty({ example: null })
  //@Matches(/^[0-9]+$/)
  @IsNotEmpty()
  @IsNumber()
  payment_commission: number;


  // @ApiProperty({ example: slaylewks_commission.percentage })
  // @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  // @IsNotEmpty()
  // @IsEnum(slaylewks_commission)
  // @IsString()
  // advance_payment_type: string | null;

  // @ApiProperty({ example: 10 })
  // @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  // @IsNotEmpty()
  // @IsNumber()
  // advance_payment: number;
}
export class BankDetailsDto {
  @ApiProperty({ example: 'Sasuki Uchiha' })
  @IsString()
  @Length(3, 100, {
    message: 'account_holder must be between 3 and 100 characters',
  })
  // @Matches(/^[A-Za-z\s]+$/, {
  //   message: 'Only letters are allowed',
  // })
  // @Matches(/^[a-zA-Z\s]+$/)
  account_holder: string;

  @ApiProperty({ example: '50100049107292' })
  @IsString()
  account_no: string;
  @ApiProperty({ example: 'HDFC0000323' })
  @IsString()
  ifsc_code: string;
  @ApiProperty({ example: 'Mylapore' })
  @IsString()
  account_branch: string;
  @ApiProperty({ example: 'saving' })
  @IsString()
  account_type: string;
}

export class CreateBranchDto {
  @ApiProperty({ type: GeneralDetailsDto })
  @ValidateNested({ each: true })
  @Type(() => GeneralDetailsDto)
  general_details?: GeneralDetailsDto;

  @ApiProperty({ type: SlotTimingsDto })
  @ValidateNested({ each: true })
  @Type(() => SlotTimingsDto)
  slot_timings?: SlotTimingsDto;

  @ApiProperty({ type: BankDetailsDto })
  @ValidateNested({ each: true })
  @Type(() => BankDetailsDto)
  bank_details?: BankDetailsDto;
}
export class GetBranchDto {
  @ApiProperty({ example: VENDOR_ID })
  @IsString()
  @IsNotEmpty()
  vendor_id?: string;

  @ApiProperty({ example: `${Approval.ACCEPTED} or ${Approval.PENDING} or ${Approval.REJECTED}` })
  @IsNumber()
  @IsEnum(Approval)
  @IsNotEmpty()
  status?: number;

  @ApiProperty({ example: 1 })
  @IsNotEmpty()
  @IsNumber()
  pagNo?: number;

  @ApiProperty({ example: 10 })
  @IsNotEmpty()
  @IsNumber()
  limit?: number;
}

export class GetServicategoryDto {
  @ApiProperty({ example: [BRANCH_ID] })
  @IsArray()
  @IsNotEmpty()
  category_ids: string[];

  @ApiProperty({ example: BRANCH_ID })
  @IsString()
  @IsNotEmpty()
  branch_id: string;
}
