import { ApiProperty, PartialType, PickType } from '@nestjs/swagger';
import { CreateBranchDto } from './create-branch.dto';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateBranchDto extends PartialType(CreateBranchDto) {
  @ApiProperty({ example: false })
  @IsBoolean()
  @IsOptional()
  business_verified: boolean;

  @ApiProperty({ example: 0 })
  @IsNumber()
  @IsOptional()
  approval_status: number;

  @ApiProperty({ example: 'Some remarks text' })
  @IsString()
  @IsOptional()
  remarks: string;
}
