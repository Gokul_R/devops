import { Test, TestingModule } from '@nestjs/testing';
import { SlotManagementController } from './slot-management.controller';
import { SlotManagementService } from './slot-management.service';

describe('SlotManagementController', () => {
  let controller: SlotManagementController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SlotManagementController],
      providers: [SlotManagementService],
    }).compile();

    controller = module.get<SlotManagementController>(SlotManagementController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
