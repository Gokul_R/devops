import { Injectable, NotAcceptableException } from '@nestjs/common';
import { CreateSlotManagementDto } from './dto/create-slot-management.dto';
import { UpdateSlotManagementDto } from './dto/update-slot-management.dto';
import { ModelCtor, Sequelize } from 'sequelize-typescript';
import SlotManagement from 'src/core/database/models/SlotManagement';
import { BaseService } from 'src/base.service';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { BulkCreateOptions, Op, UpsertOptions, where } from 'sequelize';
import Order from 'src/core/database/models/Order';
import { Errors } from 'src/core/constants/error_enums';

@Injectable()
export class SlotManagementService extends BaseService<SlotManagement> {
  slot_management_qry: SequelizeFilter<SlotManagement>;
  protected model: ModelCtor<SlotManagement>;
  constructor() {
    super();
    this.model = SlotManagement;
  }

  init() {
    this.slot_management_qry = { where: {} };
  }
  async create(createSlotManagementDto: CreateSlotManagementDto): Promise<SlotManagement> {
    return await super.create(createSlotManagementDto);
  }
  async bulkCreate(createSlotManagementDto: CreateSlotManagementDto[]): Promise<any> {
    await this.checkIsOrderExist(createSlotManagementDto);
    createSlotManagementDto.forEach((slot) => slot.id == null && delete slot.id);
    let bulkCreateOptions: BulkCreateOptions = {
      updateOnDuplicate: ['date', 'no_of_employees', 'is_leave', 'slot_start_time', 'slot_end_time'],
    };
    return await super.bulkCreate(createSlotManagementDto, bulkCreateOptions);
  }
  async findAllSlots(branch_id: string) {
    this.init();
    this.slot_management_qry.where.branch_id = branch_id;
    let data = await super.findAll(this.slot_management_qry);
    return data;
  }

  async findOneByBranch(branch_id: string, date?: any): Promise<SlotManagement> {
    this.init();
    let filter = {
      ...this.slot_management_qry.where,
      branch_id: branch_id,
      end_date: {
        [Op.gte]: date,
      },
      start_date: {
        [Op.lte]: date,
      },
    };
    if (!date) {
      delete filter.end_date;
      delete filter.start_date;
    }
    this.slot_management_qry.where = filter;
    let data = await super.findOne(null, this.slot_management_qry);
    return data;
  }

  async update(id: string, updateSlotManagementDto: UpdateSlotManagementDto): Promise<SlotManagement> {
    await this.checkIsOrderExist([updateSlotManagementDto]);
    return await super.update(id, updateSlotManagementDto);
  }

  async checkIsOrderExist(createSlotManagementDto: CreateSlotManagementDto[] | UpdateSlotManagementDto[]) {
    for (const slot of createSlotManagementDto) {
      let { is_leave, branch_id } = slot;
      let date = new Date(slot?.date);
      if (is_leave) {
        let orders: Order[] = await Order.findAll({
          attributes: ['id'],
          where: {
            [Op.and]: [
              this.model.sequelize.literal(
                `DATE("appointment_start_date_time") = (${`DATE('${date.toISOString()}')`})`,
              ),
              { branch_id: branch_id },
            ],
          },
        });
        if (orders.length > 0) throw new NotAcceptableException(Errors.NOT_PERMITTED_TO_UPDATE_LEAVE + date);
      }
    }
  }
}
