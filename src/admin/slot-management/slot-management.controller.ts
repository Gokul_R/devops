import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { SlotManagementService } from './slot-management.service';
import { CreateSlotManagementDto } from './dto/create-slot-management.dto';
import { UpdateSlotManagementDto } from './dto/update-slot-management.dto';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116, EM127 } from 'src/core/constants';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('slot-management')
@ApiTags('Slot Management')
export class SlotManagementController {
  constructor(private readonly slotManagementService: SlotManagementService) { }

  @Post()
  @ApiBody({ type: [CreateSlotManagementDto] })
  async create(@Body() createSlotManagementDto: CreateSlotManagementDto[]) {
    try {
      let data = await this.slotManagementService.bulkCreate(createSlotManagementDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      let msg = EM100;
      let code = null;
      if (error?.name === 'NotAcceptableException') {
        (code = error.status), (msg = error.message);
      }
      return HandleResponse.buildErrObj(code, msg, error);
    }
  }

  @Get('all-slots/:id')
  async findAll(@Param('id') id: string) {
    try {
      let data = await this.slotManagementService.findAllSlots(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.slotManagementService.findOneByBranch(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateSlotManagementDto: UpdateSlotManagementDto) {
    try {
      let data = await this.slotManagementService.update(id, updateSlotManagementDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      let msg = EM100;
      let code = null;
      if (error?.name === 'NotAcceptableException') {
        (code = error.status), (msg = error.message);
      }
      return HandleResponse.buildErrObj(code, msg, error);
    }
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    try {
      let data = this.slotManagementService.destroy(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
