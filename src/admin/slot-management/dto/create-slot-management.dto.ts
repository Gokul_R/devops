import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsString, IsDate, IsInt, IsBoolean, IsOptional, ValidateIf, IsNotEmpty } from 'class-validator';
import { VendorType } from 'src/core/utils/enum';

export class CreateSlotManagementDto {
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty()
  @IsString()
  @IsOptional()
  id: string;

  @ApiProperty()
  @IsString()
  branch_id: string;

  @ApiProperty()
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsDate()
  date: Date | null;

  @ApiProperty({ example: `${VendorType.BUSINESS} '(or)' ${VendorType.FREELANCER}` })
  @IsOptional()
  @IsString()
  vendor_type: string;

  @ApiProperty()
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @IsInt()
  no_of_employees: number;

  @ApiProperty()
  @IsBoolean()
  is_leave: boolean;

  @ApiProperty()
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @IsString()
  slot_start_time: string;

  @ApiProperty()
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @IsString()
  slot_end_time: string;

  @ApiProperty({ example: true })
  @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  @IsNotEmpty()
  @IsBoolean()
  morning: string;

  @ApiProperty({ example: true })
  @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  @IsNotEmpty()
  @IsBoolean()
  afternoon: string;

  @ApiProperty({ example: true })
  @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  @IsNotEmpty()
  @IsBoolean()
  evening: number;
}
