import { Inject, Injectable } from '@nestjs/common';
import { SignInAdminUserDto, verifyOtpDto } from './dto/create-admin-user.dto';
import { BaseService } from '../../base.service';
import { ModelCtor, Sequelize } from 'sequelize-typescript';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import {
  ADMIN_USER_REPOSITORY,
  EC200,
  EC204,
  EC400,
  EC410,
  EM103,
  EM105,
  EM109,
  EM116,
  EM119,
  EM130,
  EM137,
  EM138,
  PRODUCTION,
  Slaylewks_details,
  VENDOR_REPOSITORY,
} from 'src/core/constants';
import Helpers from 'src/core/utils/helpers';
import moment from 'moment';

import { UsersService } from 'src/modules/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { MailOptions, MailUtils } from 'src/core/utils/mailUtils';
import AdminUser from 'src/core/database/models/AdminUser';
import { CreateAdminUserDto } from '../admin-user/dto/create-admin-user.dto';
import EmailTemService from 'src/core/utils/emailTemplate';
import Vendor from 'src/core/database/models/Vendor';
import { UserTypes } from 'src/core/utils/enum';
import Permission from 'src/core/database/models/Permission';
import Role from 'src/core/database/models/Role';
import RolePermission from 'src/core/database/models/RolePermission';
import UserRole from 'src/core/database/models/UserRole';
import Screen from 'src/core/database/models/Screen';
import { ResetPasswordBothDto, ResetPasswordDto } from './dto/reset-password-admin-dto';
import { UpdateAdminUsersDto } from 'src/admin/auth/dto/update-admin-user.dto';
import { ForgotpasswordDto } from './dto/forgot-password-dto';
import { newpasswordDto } from './dto/newPassword.dto';
import { FRONTEND_BASE_URL } from 'src/core/database/database.providers';
import { logger } from 'src/core/utils/logger';
import Encryption from 'src/core/utils/encryption';
import Otp from 'src/core/database/models/Otp';
import VendorRole from 'src/core/database/models/VendorRoles';
import Branch from 'src/core/database/models/Branch';
import { Op } from 'sequelize';

@Injectable()
export class AuthService extends BaseService<AdminUser> {
  protected model: ModelCtor<AdminUser>;
  adminUserQry: SequelizeFilter<any>;
  admin_user_role_qry: SequelizeFilter<any>;

  constructor(
    @Inject(ADMIN_USER_REPOSITORY)
    private readonly adminUserRepo: typeof AdminUser,
    @Inject(VENDOR_REPOSITORY)
    private readonly vendorRepo: typeof Vendor,

    private readonly jwtService: JwtService,
    private readonly MailUtilsService: MailUtils,
    private readonly userService: UsersService,
  ) {
    super();
    this.model = this.adminUserRepo;
  }
  init() {
    this.admin_user_role_qry = {
      include: [
        {
          model: UserRole,
          include: [
            {
              model: Role,
              include: [
                {
                  model: RolePermission,
                  include: [
                    {
                      model: Permission,
                      include: [{ attributes: ['code', 'screen_name'], model: Screen }],
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    };
  }
  async create(createAdminUserDto: CreateAdminUserDto) {
    return super.create(createAdminUserDto);
  }

  async sendOtp(adminUserData: any) {
    logger.info(`SentOtp_Entry: ` + JSON.stringify(adminUserData));
    let otp = await Helpers.generateOTP();
    let otprequest = {
      userId: adminUserData.id,
      otp: otp,
      created_at: moment().utc().format(),
      expires_at: moment().add(5, 'minutes').utc().format(),
    };
    let result = await this.userService.createOtp(otprequest);
    if (result) {
      this.mailSentFunction(otp, adminUserData.email_id);
    }
    logger.info(`SentOtp_Exit: ` + JSON.stringify(result));
    return result;
  }

  async sendOtpWithEmail(adminUserData: any) {
    logger.info(`Sent_Otp_Email_Entry: ` + JSON.stringify(adminUserData));
    this.adminUserQry = {
      where: {
        email_id: adminUserData.userEntry,
      },
    };
    let userResult: any = await super.findOne(null, this.adminUserQry);
    await Otp.destroy({ force: true, where: { user_id: userResult.id } });
    await this.sendOtp(userResult);
    logger.info(`Sent_Otp_Email_EXit: ` + JSON.stringify(userResult));
    return {
      id: userResult.id,
      email: userResult.email_id,
    };
  }

  async mailSentFunction(otp: any, email: string) {
    logger.info(`Mail_Sent_Otp_Entry_emailId:${JSON.stringify(email)} ,OTP: ${otp}`);
    let htmlContent = EmailTemService.emailOtpVerification(otp);
    let mailOptions: MailOptions = {
      to: email,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.OTP_VERIFY,
      html: htmlContent,
    };
    this.MailUtilsService.sendEmail(mailOptions);
  }
  async verifyOtp(request: verifyOtpDto) {
    logger.info(`Verify_Otp_Entry: ` + JSON.stringify(request));
    this.init();
    this.admin_user_role_qry.where = {
      email_id: request.email_id,
      // attributes: {
      //   exclude: ['is_active', 'createdAt', 'updatedAt', 'password'],
      // },
    };
    let user: any = null;
    let checkTable;
    if (request.user_type === UserTypes.VENDOR) {
      user = await this.vendorRepo.findOne(this.admin_user_role_qry);
      user = user?.dataValues || user;
      checkTable = this.vendorRepo;
    } else {
      user = await super.findOne(null, this.admin_user_role_qry);
      checkTable = this.adminUserRepo;
    }
    if (user && user.id) {
      user['user_type'] = request.user_type;
      let requestObjet = {
        user_id: user.id,
        otp: request.otp,
      };
      let result = await this.userService.findOtpById(requestObjet);
      if (result) {
        let now = moment().utc();
        let expires = result.expires_at;

        let difference = now.diff(moment.utc(expires), 'seconds');
        if (difference > 0) {
          logger.info(`Verify_Otp_Expired_Exit: ` + JSON.stringify(request));
          return {
            code: EC410,
            message: EM130,
          };
        } else {
          this.adminUserQry = {
            where: {
              id: requestObjet.user_id,
            },
            attributes: {
              exclude: ['is_active', 'createdAt', 'updatedAt', 'password'],
            },
          };
          // let user = await super.findOne(null, this.adminUserQry);
          let token = null;
          if (user) {
            token = await this.generateToken(
              {
                user_id: user.id,
                user_type: user.user_type,
              },
              '1h',
            );
          }
          await this.userService.deleteOtp(requestObjet.user_id);
          //update the last login time
          checkTable.update({ last_login: new Date().toISOString() }, { where: { email_id: request.email_id } });
          logger.info(`Verify_Otp_Exit: ` + JSON.stringify(request));
          return {
            code: EC200,
            message: EM103,
            data: {
              token: token,
              ...user,
            },
          };
        }
      } else {
        logger.info(`Verify_Otp_IncorrectOtp: ` + JSON.stringify(request));
        return {
          code: EC410,
          message: EM109,
        };
      }
    } else {
      logger.info(`Verify_Otp_invalid_credentials: ` + JSON.stringify(request));
      return {
        code: EC204,
        message: EM105,
      };
    }
  }

  async signIn(signInAdminUserDto: SignInAdminUserDto) {
    logger.info(`SignIn_Entry: ` + JSON.stringify(signInAdminUserDto));
    this.adminUserQry = {
      where: {
        email_id: signInAdminUserDto.email_id,
      },
    };

    if (signInAdminUserDto.user_type === UserTypes.VENDOR) {
      logger.info(`Vendor_SignIn_Exit: `);
      this.adminUserQry = {
        include: [
          //{
          //    model: UserRole,
          //    include: [
          {
            model: Role,
            include: [
              {
                model: RolePermission,
                include: [
                  {
                    model: Permission,
                    include: [{ attributes: ['code', 'screen_name'], model: Screen }],
                  },
                ],
              },
            ],
          },
        ],
        where: {
          email_id: signInAdminUserDto.email_id,
          is_deleted: false
        },

      };
      let vendor: any;
      vendor = await this.vendorRepo.findOne({ where: { email_id: signInAdminUserDto.email_id, is_deleted: false } });
      let time: any = { last_login: new Date().toISOString(), device_token: signInAdminUserDto.device_token ? signInAdminUserDto.device_token : null };
      if (!vendor) {
        VendorRole.update(time, { where: { email_id: signInAdminUserDto.email_id } });
        vendor = await VendorRole.findOne(this.adminUserQry);
        if (!vendor) return null;
        if (vendor && vendor?.branch_ids) {
          let branch_names = await Branch.findAll({
            attributes: ['id', 'branch_name'],
            where: {
              id: {
                [Op.in]: vendor.branch_ids
              }
            }
          });
          let vendorType = await Vendor.findOne({ attributes: ['vendor_type'], where: { id: vendor?.vendor_id } })
          vendor.dataValues.branch = branch_names;
          vendor.dataValues.vendor_type =vendorType?.vendor_type;
        } else {
          vendor.dataValues.branch = null;
        }

      }
      else {
        this.vendorRepo.update(time, { where: { email_id: signInAdminUserDto.email_id } });
      }
      return vendor;
    } else {
      logger.info(`Admin_SignIn_Exit: `);
      return await super.findOne(null, this.adminUserQry);
    }
  }

  async generateToken(user: any, expryTime: string) {
    const token = await this.jwtService.signAsync(user, {
      secret: process.env.JWTKEY,
      expiresIn: expryTime,
    });
    return token;
  }

  async resetPassword(req_data: ResetPasswordBothDto) {
    logger.info(`Reset_Password_Entry: ` + JSON.stringify(req_data));
    let checkUser: any = req_data.user_type === UserTypes.ADMIN ? AdminUser : Vendor;
    let result = await checkUser.findByPk(req_data.id);
    if (!result)
      result = await VendorRole.findByPk(req_data.id);
    // let old_pwd = Encryption.encryptValue(req_data?.old_password);
    // let new_pwd = Encryption.encryptValue(req_data?.new_password);
    if (!Encryption.comparePassword(req_data?.old_password, result.password)) {
      logger.info(`Reset_Password_Invalid_Exit: ` + JSON.stringify(req_data));
      return { code: EC204, message: EM105 };
    }
    if (Encryption.comparePassword(req_data?.new_password, result.password)) {
      logger.info(`Reset_Password_OldPwdMatch_Exit: ` + JSON.stringify(req_data));
      return { code: EC204, message: EM138 };
    }
    let hashPassword = await Encryption.hashPassword(req_data.new_password);
    let updateNewPwd = await checkUser.update({ password: hashPassword }, { where: { id: req_data.id } });
    if (updateNewPwd[0] == 0)
      updateNewPwd = await Helpers.update(VendorRole, { where: { id: req_data.id } }, { password: hashPassword });
    let htmlContent = await EmailTemService.emailPasswordVerification(req_data.new_password);
    let mailOptions: MailOptions = {
      to: result.email_id,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.RESET_PASSWORD_SENT,
      html: htmlContent,
    };
    logger.info(`Reset_Password_Email_Entry_Email_id: ${result.email_id}`);
    var ans = await this.MailUtilsService.sendEmail(mailOptions);
    logger.info(`Reset_Password_Exit: ` + JSON.stringify({ id: req_data.id }));
    return { id: req_data.id };
  }
  async resetPasswordVendor(data: ResetPasswordDto) {
    logger.info(`Reset_Password_Vendor_Entry: ` + JSON.stringify(data));
    let { id } = data;
    let result: Vendor = await Vendor.findOne({ where: { id: id } });
    if (result) {
      const newPwd = await Helpers.generateRandomPassword();
      let hashPassword = await Encryption.hashPassword(newPwd);
      const updateNewPwd = await Vendor.update({ password: hashPassword }, { where: { id: id } });
      // let password = await Encryption.decryptValue(result.password);
      let htmlContent = await EmailTemService.emailPasswordVerification(newPwd);
      let mailOptions: MailOptions = {
        to: result.email_id,
        subject: EmailTemService.EMAIL_DATA.SUBJECT,
        text: EmailTemService.EMAIL_DATA.RESET_PASSWORD_SENT,
        html: htmlContent,
        cc: process.env.NODE_ENV == PRODUCTION ?  Slaylewks_details.cc_mail_live : Slaylewks_details.cc_mail_dev
      };
      logger.info(`Reset_Password_Vendor_Entry_Email_id: ${result.email_id}`);
      var ans = await this.MailUtilsService.sendEmail(mailOptions);
      logger.info(`Reset_Password_Vendor_Exit: ` + JSON.stringify({ id: id, pwd: newPwd }));
      return { id: id, pwd: newPwd };
    } else {
      logger.info(`Reset_Password_Vendor_Invalid: ` + JSON.stringify(data));
      return {
        code: EC204,
        message: EM105,
      };
    }
  }

  async forgotPassword(data: ForgotpasswordDto) {
    logger.info(`Forgot_Password_Entry_EmailId: ${JSON.stringify(data)}`);
    let checkTable: any = data.user_type == 'A' ? this.adminUserRepo : this.vendorRepo;
    if (data.email_id) {
      let email = data.email_id;
      let emailVerification = await checkTable.findOne({
        where: { email_id: email },
      });
      if (!emailVerification) {
        emailVerification = await VendorRole.findOne({ where: { email_id: email } });
      }
      if (!emailVerification) {
        logger.info(`Forgot_Password_DoesNot_Exist: ${JSON.stringify(data)}`);
        return null;
      }

      let token = null;
      if (data.email_id) {
        token = await this.generateToken(
          {
            email_id: email,
            id: emailVerification.id,
            userType: data.user_type,
          },
          '10m',
        );
      }
      const link = `${FRONTEND_BASE_URL}?token=${token}&&&type=${data.user_type}`;
      logger.info(`Forgot_Password_Link :` + JSON.stringify(link));
      let htmlContent = await EmailTemService.forgotPassword(link);
      let mailOptions: MailOptions = {
        to: data.email_id,
        subject: EmailTemService.EMAIL_DATA.SUBJECT,
        text: EmailTemService.EMAIL_DATA.FORGOT_PASSWORD,
        html: htmlContent,
      };
      logger.info(`Forgot_Password_Send_Email_Entry_Email_: ${data.email_id}`);
      await this.MailUtilsService.sendEmail(mailOptions);
      logger.info(`Forgot_Password_Exit: ${data.email_id}`);
      return {
        code: EC200,
        message: EM137,
        //  data: [],
      };
    } else {
      logger.info(`Forgot_Password_Invalid_Credential: ${JSON.stringify(data)}`);
      return {
        code: EC204,
        message: EM105,
      };
    }
  }
  async newPassword(request: any, newPassword: newpasswordDto) {
    logger.info(`Change_Password_Entry_NewPassword: ${JSON.stringify(newPassword)} Data: ${request} `);
    let updated: any;
    // let email = request.user.email_id;
    let user_id = request.user.id;
    let user_Type = request.user.userType;
    let checkTable: any = user_Type == 'A' ? this.adminUserRepo : this.vendorRepo;
    let hashPassword = await Encryption.hashPassword(newPassword.new_password);
    let adminvendorUser = await checkTable.findByPk(user_id);
    if (!adminvendorUser) {
      adminvendorUser = await VendorRole.findByPk(user_id);
      updated = await VendorRole.update({ password: hashPassword }, { where: { id: user_id } });

    }
    if (adminvendorUser) {
      updated = await checkTable.update({ password: hashPassword }, { where: { id: user_id } });
      logger.info(`Change_Password_Exit: ${updated}`);
      return {
        code: EC200,
        message: EM116,
        // data: [],
      };
    } else {
      logger.info(`Change_Password_Record_Not_Found_Exit: ${request.user.email_id}`);
      return {
        code: EC400,
        message: EM119,
        //  data: null,
      };
    }
  }

  // async updateData(updateAdminUserDto: UpdateAdminUsersDto) {
  //   let { user_type, id } = updateAdminUserDto;
  //   let checkTable: any = user_type === 'A' ? AdminUser : Vendor;

  //   const recordToUpdate = await checkTable.findByPk(id);
  //   if (recordToUpdate) {
  //     let updatedData = await recordToUpdate.update(updateAdminUserDto);
  //     return updatedData;
  //   }

  //   return null;
  // }
}
