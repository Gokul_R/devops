import { Controller, Get, Post, Body, UseGuards, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignInAdminUserDto, SendOtpDto } from './dto/create-admin-user.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import {
  EC200,
  EC204, EC401,
  EC410,
  EC411,
  EC500,
  EM100,
  EM101,
  EM103,
  EM107,
  EM116, EM128,
  EM129,
  EM134,
  EM150
} from 'src/core/constants';
import { verifyOtpDto } from 'src/admin/auth/dto/create-admin-user.dto';
import { ResetPasswordBothDto, ResetPasswordDto } from './dto/reset-password-admin-dto';
import { ForgotpasswordDto } from './dto/forgot-password-dto';
import { newpasswordDto } from './dto/newPassword.dto';
import Encryption from 'src/core/utils/encryption';
import { JwtMiddleware } from 'src/middleware/jwt.middleware';
import { logger } from 'src/core/utils/logger';
import Otp from 'src/core/database/models/Otp';
import { UserTypes } from 'src/core/utils/enum';
import Vendor from 'src/core/database/models/Vendor';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private readonly adminUserService: AuthService) { }

  // @Post()
  // create(@Body() createAdminUserDto: CreateAdminUserDto) {
  //   return this.adminUserService.create(createAdminUserDto);
  // }
  @Post('signin')
  async signIn(@Body() signInAdminUserDto: SignInAdminUserDto) {
    try {
      let result = await this.adminUserService.signIn(signInAdminUserDto);
      if (result && result?.is_active == false) {
        // if the user is disabled then
        logger.info(`SignIn_Account_Disabled: ` + JSON.stringify(signInAdminUserDto));
        return { code: EC401, status: false, message: EM150 };
      }
      if (result && result.email_id) {
        // let decryptPwd = Helpers.encryptValue(signInAdminUserDto.password);

        // if (decryptPwd === result.password) {
        if (Encryption.comparePassword(signInAdminUserDto.password, result.password)) {
          // sent data to vendor login without otp scenario said by client 2023-12-15
          if (signInAdminUserDto.user_type === UserTypes.VENDOR) return await this.sendVendorResWithoutOtpCheck(result, signInAdminUserDto);
          //-------------------------------------------------------------------------------
          await Otp.destroy({ force: true, where: { user_id: result.id } });
          this.adminUserService.sendOtp(result);
          logger.info(`SignIn_Exit: ` + JSON.stringify(signInAdminUserDto));
          return HandleResponse.buildSuccessObj(EC200, EM129, {
            // id: result.id,
            email_id: result.email_id,
          });
        } else {
          // return HandleResponse.buildSuccessObj(EC200, EM128, null);
          logger.info(`SignIn_Password_Incorrect: ` + JSON.stringify(signInAdminUserDto));
          return { code: EC411, status: false, message: EM128, data: null };
        }
      } else {
        // return HandleResponse.buildSuccessObj(EC200, EM101, null);
        logger.info(`SignIn_User_not_exists: ` + JSON.stringify(signInAdminUserDto));
        return { code: EC204, status: false, message: EM101, data: null };
      }
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  async sendVendorResWithoutOtpCheck(user: any, signInAdminUserDto: SignInAdminUserDto) {
    let token = await this.adminUserService.generateToken(
      {
        user_id: user.id,
        user_type: UserTypes.VENDOR,
      },
      '1h',
    );
    return {
      code: EC200,
      message: EM103,
      status: true,
      data: {
        token: token,
        ...user?.dataValues,
      },
    };
  }
  @Post('verify-otp')
  async verifyOtp(@Body() body: verifyOtpDto) {
    try {
      let result = await this.adminUserService.verifyOtp(body);
      if (result?.code != EC200) return HandleResponse.buildErrObj(result?.code, result?.message, result?.data || null);
      else return HandleResponse.buildSuccessObj(result?.code, result?.message, result?.data || null);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('send-otp')
  async sendOtp(@Body() body: SendOtpDto) {
    try {
      let result = await this.adminUserService.sendOtpWithEmail(body);
      return HandleResponse.buildSuccessObj(EC200, EM107, null);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('reset-password') //both login reset our old pwd
  async resetPassword(@Body() req_data: ResetPasswordBothDto) {
    try {
      let result: any = await this.adminUserService.resetPassword(req_data);
      if (result?.code == EC204) {
        return HandleResponse.buildErrObj(result?.code, result?.message, null);
      }
      return HandleResponse.buildSuccessObj(EC200, EM116, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }
  @Post('vendor/reset-password') // by admin to vendor reset pwd
  async resetPasswordVendor(@Body() data: ResetPasswordDto) {
    try {
      let result: any = await this.adminUserService.resetPasswordVendor(data);
      if (result?.code == EC204) {
        return HandleResponse.buildErrObj(result?.code, result?.message, null);
      }
      return HandleResponse.buildSuccessObj(EC200, EM116, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Post('forgot-password')
  async forgotpassword(@Body() data: ForgotpasswordDto) {
    try {
      let result: any = await this.adminUserService.forgotPassword(data);
      if (result == null) {
        return HandleResponse.buildSuccessObj(EC401, EM101, null);
      }
      return HandleResponse.buildSuccessObj(result.code, result.message, result.data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Post('change-password')
  // @UseGuards(JwtMiddleware)
  async forgotpasswordreset(@Req() request: any, @Body() password: newpasswordDto) {
    try {
      let result: any = await this.adminUserService.newPassword(request, password);
      if (result == null) {
        return HandleResponse.buildSuccessObj(EC401, EM101, null);
      }
      return HandleResponse.buildSuccessObj(result?.code, result?.message, result?.data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  // @Get()
  // findAll() {
  //   return this.adminUserService.findAll();
  // }

  // @Patch('update-profile')
  // async update(@Body() updateAdminUserDto: UpdateAdminUsersDto) {
  //   try {
  //     let result = await this.adminUserService.updateData(updateAdminUserDto);
  //     if (result === null) {
  //       return HandleResponse.buildSuccessObj(EC400, EM119, null);
  //     }
  //     return HandleResponse.buildSuccessObj(EC200, EM116, result);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }
}
