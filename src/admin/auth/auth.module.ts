import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { authProviders } from './auth.provider';
import { UsersModule } from 'src/modules/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { TextLocalSMS } from 'src/core/utils/textlocal';
import { HttpModule, HttpService } from '@nestjs/axios';
import { MailUtils } from 'src/core/utils/mailUtils';
import { VendorModule } from '../vendor/vendor.module';

@Module({
  imports: [
    VendorModule,
    UsersModule,
    HttpModule,
    JwtModule.register({
      secret: process.env.JWTKEY,
      signOptions: { expiresIn: process.env.TOKEN_EXPIRATION },
    }),],
  controllers: [AuthController],
  providers: [AuthService, ...authProviders, MailUtils]
})
export class AuthModule { }

