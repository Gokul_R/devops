import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { UserTypes } from 'src/core/utils/enum';

export class ForgotpasswordDto {
  @ApiProperty()
  @IsEmail()
  email_id: string;

  @ApiProperty({ example: UserTypes.VENDOR })
  @IsEnum(UserTypes)
  user_type: string;


  // @ApiProperty()
  // @IsOptional()
  // id: string;
  
}
