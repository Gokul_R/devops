import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString, IsUUID, Matches, MaxLength, MinLength } from 'class-validator';
import { BRANCH_ID } from 'src/core/constants';
import { UserTypes } from 'src/core/utils/enum';

export class ResetPasswordDto {
  @ApiProperty({ example: BRANCH_ID })
  // @IsUUID()
  @IsString()
  id?: string;
}
export class ResetPasswordBothDto {
  @ApiProperty({ example: BRANCH_ID })
  // @IsUUID()
  @IsNotEmpty()
  id?: string;

  @ApiProperty({ example: UserTypes.ADMIN })
  @IsEnum(UserTypes)
  @IsNotEmpty()
  user_type: string;

  @ApiProperty({ example: 'Need strong password' })
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(20)
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_])[A-Za-z\d\W_]+$/, {
    message: 'Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character.',
  })
  new_password: string;

  @ApiProperty({ example: 'Old password' })
  @IsString()
  @IsNotEmpty()
  @MaxLength(20)
  old_password: string;
}
