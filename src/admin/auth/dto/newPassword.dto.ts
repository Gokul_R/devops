import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString, IsUUID, Matches, MaxLength, MinLength } from 'class-validator';
import { UserTypes } from 'src/core/utils/enum';

export class newpasswordDto {
  @ApiProperty({ example: 'Need strong password' })
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(20)
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_])[A-Za-z\d\W_]+$/, {
    message: 'Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character.',
  })
  new_password: string;

  // @ApiProperty({ example: UserTypes.VENDOR })
  // @IsEnum(UserTypes)
  // user_type: string;

}
