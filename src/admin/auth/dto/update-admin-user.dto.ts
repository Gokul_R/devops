import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { UserTypes } from 'src/core/utils/enum';

export class UpdateAdminUsersDto {
  @ApiProperty()
  @IsNotEmpty()
  id?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  image?: string | null;

  // @ApiProperty()
  // @IsString()
  // email_id?: string;

  // @ApiProperty()
  // @IsString()
  // password?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  name?: string;

  // @ApiProperty()
  // @IsBoolean()
  // is_active?: boolean;

  @ApiProperty()
  @IsString()
  @IsOptional()
  mobile_no?: string;

  @ApiProperty({ example: UserTypes.VENDOR }) // This should show in Swagger
  @IsEnum(UserTypes)
  user_type?: string;

  // @ApiProperty()
  // @IsString()
  // employee_id?: string;
}
