import { ApiProperty, OmitType, PartialType, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString, IsOptional, IsString, Length } from 'class-validator';
import { CreateAdminUserDto } from 'src/admin/admin-user/dto/create-admin-user.dto';
import { SharedDto } from 'src/core/interfaces/shared.dto';
import { UserTypes } from 'src/core/utils/enum';

export class SignInAdminUserDto extends PickType(CreateAdminUserDto, [
  'email_id',
  'password',
]) {
  @ApiProperty({ example: UserTypes.VENDOR })
  @IsString()
  user_type: string;

  @ApiProperty({ example: "Need device token only for vendor app" })
  @IsOptional()
  device_token: string | null;
}

export class SendOtpDto {
  @ApiProperty({ example: 'rmuniasamy392@gmail.com' })
  @IsString()
  userEntry: string;


}

export class verifyOtpDto {
  @ApiProperty({ example: "rmuniasamy392@gmail.com" })
  @IsNotEmpty()
  email_id: string;

  @ApiProperty({ example: UserTypes.VENDOR })
  @IsString()
  user_type: string;


  @IsString()
  @IsOptional()
  last_login?: string;

  @ApiProperty({ example: 1234 })
  @IsNotEmpty()
  @IsNumberString()
  @Length(4, 4)
  otp: string;
}