import { Test, TestingModule } from '@nestjs/testing';
import { BranchServiceCategoryService } from './branch-service-category.service';

describe('BranchServiceCategoryService', () => {
  let service: BranchServiceCategoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BranchServiceCategoryService],
    }).compile();

    service = module.get<BranchServiceCategoryService>(BranchServiceCategoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
