import { commonProvider } from 'src/core/interfaces/common-providers';

export const branchServiceCatProviders = [
  // {
  //   provide: BRANCH_REPOSITORY,
  //   useValue: Branch,
  // },
  // {
  //   provide: BRANCH_SERVICE_CATEGORY_REPOSITORY,
  //   useValue: BranchServiceCategoryMapping,
  // }
  // {
  //   provide: BRANCH_CATEGORY_REPOSITORY,
  //   useValue: BranchCategoryMapping,
  // },
  // {
  //   provide: BRANCH_SERVICE_TYPE_REPOSITORY,
  //   useValue: BranchServiceTypeMapping,
  // },
  // {
  //   provide: SLOT_DETAILS,
  //   useValue: SlotDetail,
  // },
  // {
  //   provide: BRANCH_REPOSITORY,
  //   useValue: Branch,
  // },
  commonProvider.branch_service_category_repository,
];
