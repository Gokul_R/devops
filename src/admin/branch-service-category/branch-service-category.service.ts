import { Inject, Injectable } from '@nestjs/common';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import BranchServiceCategoryMapping from 'src/core/database/models/BranchServiceCategoryMapping.ts';
import { BRANCH_SERVICE_CATEGORY_REPOSITORY } from 'src/core/constants';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import Branch from 'src/core/database/models/Branch';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import { MASTER_SERVICE_CATEGORY_ATTRIBUTES } from 'src/core/attributes/master_service_category';
import { Op } from 'sequelize';

@Injectable()
export class BranchServiceCategoryService extends BaseService<BranchServiceCategoryMapping> {
  protected model: ModelCtor<BranchServiceCategoryMapping>;
  constructor(
    @Inject(BRANCH_SERVICE_CATEGORY_REPOSITORY)
    private readonly branchServiceCatRepo: typeof BranchServiceCategoryMapping,
  ) {
    super();
    this.model = this.branchServiceCatRepo;
  }
  async findAllByBranchOrServiceCat(
    branch_id: string,
    servCategoryId?: string,
  ) {
    let serviceCatQry: SequelizeFilter<BranchServiceCategoryMapping> = {
      where: {
        [Op.and]: [
          { branch_id: branch_id },
          // { is_active: true },
          // { is_deleted: false },
          // { deleted_at: null },
        ],
      },
      include: [
        {
          attributes: [
            MASTER_SERVICE_CATEGORY_ATTRIBUTES.service_category_name,
          ],
          model: MasterServiceCategory,
          where: {
            [Op.and]: [
              { is_active: true },
              { is_deleted: false },
              { deleted_at: null },
            ],
          },
        },
      ],
    };
    if (servCategoryId) {
      delete serviceCatQry.where.branch_id;
      serviceCatQry.where.service_category_id = servCategoryId;
      serviceCatQry.include = [
        { attributes: [BRANCH_ATTRIBUTES.BRANCH_NAME], model: Branch },
      ];
    }
    return await super.findAll(serviceCatQry);
  }
}
