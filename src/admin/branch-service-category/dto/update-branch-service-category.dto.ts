import { PartialType } from '@nestjs/mapped-types';
import { CreateBranchServiceCategoryDto } from './create-branch-service-category.dto';

export class UpdateBranchServiceCategoryDto extends PartialType(CreateBranchServiceCategoryDto) {}
