import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { BranchServiceCategoryService } from './branch-service-category.service';
import { CreateBranchServiceCategoryDto } from './dto/create-branch-service-category.dto';
import { UpdateBranchServiceCategoryDto } from './dto/update-branch-service-category.dto';
import { BaseService } from '../../base.service';
import BranchServiceCategoryMapping from 'src/core/database/models/BranchServiceCategoryMapping.ts';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { BRANCH_ID, EC100, EC200, EM104, EM106 } from 'src/core/constants';

@ApiTags('Branch Service Category')
@Controller('branch-service-category')
export class BranchServiceCategoryController {
  constructor(
    private readonly branchServiceCategoryService: BranchServiceCategoryService,
  ) {}

  // @Post()
  // create(@Body() createBranchServiceCategoryDto: CreateBranchServiceCategoryDto) {
  //   return this.branchServiceCategoryService.create(createBranchServiceCategoryDto);
  // }

  // @Get()
  // findAll() {
  //   return this.branchServiceCategoryService.findAll();
  // }
  @ApiParam({
    example: BRANCH_ID,
    name: 'branch_id',
  })
  @Get('by-branch/:branch_id')
  async findAllByBranch(@Param('branch_id') branch_id: string) {
    try {
      let data: BranchServiceCategoryMapping =
        await this.branchServiceCategoryService.findAllByBranchOrServiceCat(branch_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EC100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data: BranchServiceCategoryMapping =
        await this.branchServiceCategoryService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EC100, error);
    }
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateBranchServiceCategoryDto: UpdateBranchServiceCategoryDto) {
  //   return this.branchServiceCategoryService.update(+id, updateBranchServiceCategoryDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.branchServiceCategoryService.remove(+id);
  // }
}
