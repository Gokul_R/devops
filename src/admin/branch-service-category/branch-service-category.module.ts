import { Module } from '@nestjs/common';
import { BranchServiceCategoryService } from './branch-service-category.service';
import { BranchServiceCategoryController } from './branch-service-category.controller';
import { branchServiceCatProviders } from './branch-service-category.provider';

@Module({
  controllers: [BranchServiceCategoryController],
  providers: [BranchServiceCategoryService,...branchServiceCatProviders],
  exports:[BranchServiceCategoryService]
})
export class BranchServiceCategoryModule {}
