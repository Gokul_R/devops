import { Test, TestingModule } from '@nestjs/testing';
import { BranchServiceCategoryController } from './branch-service-category.controller';
import { BranchServiceCategoryService } from './branch-service-category.service';

describe('BranchServiceCategoryController', () => {
  let controller: BranchServiceCategoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchServiceCategoryController],
      providers: [BranchServiceCategoryService],
    }).compile();

    controller = module.get<BranchServiceCategoryController>(BranchServiceCategoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
