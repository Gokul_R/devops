import { Module } from '@nestjs/common';
import { SmtpSettingsService } from './smtpsettings.service';
import { SmtpSettingsController } from './smtpsettings.controller';
import { smtpsettingsProviders } from './smtpsettings.providers';

@Module({
  controllers: [SmtpSettingsController],
  providers: [SmtpSettingsService, ...smtpsettingsProviders],
})
export class SmtpSettingsModule {}
