import { Test, TestingModule } from '@nestjs/testing';
import { SmtpSettingsController } from './smtpsettings.controller';
import { SmtpSettingsService } from './smtpsettings.service';

describe('SmtpsettingsController', () => {
  let controller: SmtpSettingsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SmtpSettingsController],
      providers: [SmtpSettingsService],
    }).compile();

    controller = module.get<SmtpSettingsController>(SmtpSettingsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
