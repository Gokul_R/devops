import { Controller, Get, Post, Body, Patch, Param } from '@nestjs/common';
import { SmtpSettingsService } from './smtpsettings.service';
import { CreateSmtpSettingsDto } from './dto/create-smtpsetting.dto';
import { UpdateSmtpSettingsDto } from './dto/update-smtpsetting.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116, EM127 } from 'src/core/constants';
import { BaseController } from '../../base.controller';
import { logger } from 'src/core/utils/logger';

@ApiTags('smtpsettings')
@Controller('smtpsettings')
export class SmtpSettingsController extends BaseController<CreateSmtpSettingsDto> {
  constructor(private readonly smtpSettingsService: SmtpSettingsService) {
    super(smtpSettingsService);
  }

  @Post()
  async create(@Body() body: CreateSmtpSettingsDto) {
    try {
      logger.info(`Create_SmtpSettings_Entry: ${JSON.stringify(body)}`);
      let data = await this.smtpSettingsService.create(body);
      logger.info(`Create_SmtpSettings_Exit: ${JSON.stringify(data)}`);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get()
  // async findAll() {
  //   try {
  //     let data = await this.smtpSettingsService.findAll();

  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  // @Get(':id')
  // async findOne(@Param('id') id: string) {
  //   try {
  //     let data = await this.smtpSettingsService.findOne(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() body: UpdateSmtpSettingsDto) {
    try {
      logger.info(`Update_SmtpSettings_Entry_Id: ${JSON.stringify(id)} Data:${JSON.stringify(body)}`);
      let data = await this.smtpSettingsService.update(id, body);
      logger.info(`Update_SmtpSettings_Exit: ${JSON.stringify(data)}`);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @delete(':id')
  async remove(@Param('id') id: string) {
    try {
      logger.info(`Delete_SmtpSettings_Entry: ${JSON.stringify(id)}`);
      let data = await this.smtpSettingsService.destroy(id);
      logger.info(`Delete_SmtpSettings_Exit: ${JSON.stringify(data)}`);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
