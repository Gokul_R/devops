import { Inject, Injectable } from '@nestjs/common';
import { CreateSmtpSettingsDto } from './dto/create-smtpsetting.dto';
import { UpdateSmtpSettingsDto } from './dto/update-smtpsetting.dto';
import SmtpSettings from 'src/core/database/models/SmtpSettings';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import { SMTP_SETTINGS_REPOSITORY } from 'src/core/constants';

@Injectable()
export class SmtpSettingsService  extends BaseService<SmtpSettings>{
  
  protected model: ModelCtor<SmtpSettings>;
  constructor(@Inject(SMTP_SETTINGS_REPOSITORY) private readonly smtpSettingsRepo:typeof SmtpSettings){
    super();
    this.model=this.smtpSettingsRepo;
  }
  // async create(Body: CreateSmtpSettingsDto) {
  //   const result = await SmtpSettings.create(Body);
  //   return result;
  // }

  // async findAll() {
  //   const result = await SmtpSettings.findAll();
  //   return result.indexOf[0];
  // }

  // async findOne(Id: string) {
  //   const result = await SmtpSettings.findOne({
  //     where: { id: Id },
  //   });
  //   return result;
  // }

  // async update(Id: string, Body: UpdateSmtpSettingsDto) {
  //   const result = await SmtpSettings.update(Body, {
  //     where: { id: Id },
  //   });
  //   return result;
  // }
}
