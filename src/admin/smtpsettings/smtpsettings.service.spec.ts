import { Test, TestingModule } from '@nestjs/testing';
import { SmtpSettingsService } from './smtpsettings.service';

describe('SmtpsettingsService', () => {
  let service: SmtpSettingsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SmtpSettingsService],
    }).compile();

    service = module.get<SmtpSettingsService>(SmtpSettingsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
