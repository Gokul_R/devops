import { PartialType } from '@nestjs/swagger';
import { CreateSmtpSettingsDto } from './create-smtpsetting.dto';

export class UpdateSmtpSettingsDto extends PartialType(CreateSmtpSettingsDto) {}
