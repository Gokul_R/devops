import { IsString, IsInt, IsOptional, IsBoolean } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateSmtpSettingsDto {
  @IsString()
  @IsOptional()
  id: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: 'localhost' })
  @IsString()
  @IsOptional()
  mail_host: string;

  @ApiProperty({ example: 5432 })
  @IsInt()
  @IsOptional()
  mail_port: number;

  @ApiProperty({ example: 'manoj' })
  @IsString()
  @IsOptional()
  mail_username: string;

  @ApiProperty({ example: '12345' })
  @IsString()
  @IsOptional()
  mail_password: string;

  @ApiProperty({ example: 'ccimpweavpo' })
  @IsString()
  @IsOptional()
  mail_encryption: string;

  @ApiProperty({ example: 'from address' })
  @IsString()
  @IsOptional()
  mail_from_address: string;

  @ApiProperty({ example: 'from name' })
  @IsString()
  @IsOptional()
  mail_from_name: string;

  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;
}
