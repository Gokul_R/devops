import { commonProvider } from 'src/core/interfaces/common-providers';

export const smtpsettingsProviders = [
  // {
  //   provide: SMTP_SETTINGS_REPOSITORY,
  //   useValue: SmtpSettings,
  // },
  commonProvider.smtp_settings_repository,
];
