import { Injectable } from '@nestjs/common';
import { CreateFlashSaleDto } from './dto/create-flash-sale.dto';
import { BaseService } from '../../base.service';
import FlashSale from 'src/core/database/models/FlashSale';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import { ModelCtor } from 'sequelize-typescript';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import Branch from 'src/core/database/models/Branch';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { SERVICE_ATTRIBUTES } from 'src/core/attributes/services';
import Service from 'src/core/database/models/Services';
import { MASTER_CATEGORIES_ATTRIBUTES } from 'src/core/attributes/master_category';
import { MASTER_SERVICE_CATEGORY_ATTRIBUTES } from 'src/core/attributes/master_service_category';
import MasterCategory from 'src/core/database/models/MasterCategory';
import Vendor from 'src/core/database/models/Vendor';
import { VENDOR_ATTRIBUTES } from 'src/core/attributes/vendor';
import { col, fn } from 'sequelize';
import { Op } from 'sequelize';

@Injectable()
export class FlashSaleService extends BaseService<FlashSale> {
  protected model: ModelCtor<FlashSale>;
  flash_sale_qry: SequelizeFilter<FlashSale>;
  constructor() {
    super();
    this.model = FlashSale as typeof FlashSale;
  }
  create(createFlashSaleDto: CreateFlashSaleDto) {
    return super.create(createFlashSaleDto);
  }
  init() {
    this.flash_sale_qry = {
      where: {},
      include: [
        {
          attributes: [BRANCH_ATTRIBUTES.BRANCH_NAME, BRANCH_ATTRIBUTES.VENDOR_TYPE],
          model: Branch,
          include: [
            {
              attributes: [VENDOR_ATTRIBUTES.VENDOR_NAME, VENDOR_ATTRIBUTES.ID],
              model: Vendor,
            },
          ],
        },
        {
          attributes: [SERVICE_ATTRIBUTES.ID, SERVICE_ATTRIBUTES.SERVICE_NAME],
          model: Service,
          include: [
            {
              attributes: [MASTER_CATEGORIES_ATTRIBUTES.id, MASTER_CATEGORIES_ATTRIBUTES.category],
              model: MasterCategory,
            },
            {
              attributes: [
                MASTER_SERVICE_CATEGORY_ATTRIBUTES.id,
                MASTER_SERVICE_CATEGORY_ATTRIBUTES.service_category_name,
              ],
              model: MasterServiceCategory,
            },
          ],
        },
      ],
    };
  }
  async findAll() {
    this.init();
    return await super.findAll(this.flash_sale_qry);
  }

  async findOne(id: string) {
    this.init();
    this.flash_sale_qry.where.id = id;
    let data = await super.findOne(id, this.flash_sale_qry);
    data.vendor = data?.branch?.vendor;
    data.category = data?.service?.category;
    data.service_category = data?.service?.service_category;
    data = JSON.stringify(data);
    data = JSON.parse(data);
    delete data?.branch?.vendor;
    delete data?.service;
    return data;
  }

  // update(id: number, updateFlashSaleDto: UpdateFlashSaleDto) {
  //   return `This action updates a #${id} flashSale`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} flashSale`;
  // }

  async getFilterList(req_data: any): Promise<any> {
    let search;
    if (req_data.searchText) {
      search = {
        [Op.or]: [
          {
            '$branch.branch_name$': { [Op.iLike]: `%${req_data.searchText.toLowerCase()}%` },
          },
          { '$service.service_name$': { [Op.iLike]: `%${req_data.searchText.toLowerCase()}%` } },
          {
            discount_type: { [Op.iLike]: `%${req_data.searchText.toLowerCase()}%` },
          }, // Convert search value to lowercase
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let where = {
      where: {
        is_deleted: false,
        // ...search
      },
      offset: offset,
      limit: limit,
      order: [['created_at', 'DESC']],
    };
    search = search ? { where: search } : {};
    let query = {
      ...where,
      ...search,
      include: [
        {
          attributes: [BRANCH_ATTRIBUTES.BRANCH_NAME, BRANCH_ATTRIBUTES.VENDOR_TYPE],
          model: Branch,

          include: [
            {
              attributes: [VENDOR_ATTRIBUTES.VENDOR_NAME, VENDOR_ATTRIBUTES.ID],
              model: Vendor,
            },
          ],
        },
        {
          attributes: [SERVICE_ATTRIBUTES.SERVICE_NAME],
          model: Service,
          include: [
            {
              attributes: [MASTER_CATEGORIES_ATTRIBUTES.id, MASTER_CATEGORIES_ATTRIBUTES.category],
              model: MasterCategory,
            },
            {
              attributes: [
                MASTER_SERVICE_CATEGORY_ATTRIBUTES.id,
                MASTER_SERVICE_CATEGORY_ATTRIBUTES.service_category_name,
              ],
              model: MasterServiceCategory,
            },
          ],
        },
      ],
    };
    return await this.findAndCountAll(query);
  }
}
