import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { FlashSaleService } from './flash-sale.service';
import { CreateFlashSaleDto } from './dto/create-flash-sale.dto';
import { UpdateFlashSaleDto } from './dto/update-flash-sale.dto';
import { ApiTags } from '@nestjs/swagger';
import { EC200, EM106, EM100, EM116, EM127, EM104, EC409, EM151 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { Op } from 'sequelize';
import FlashSale from 'src/core/database/models/FlashSale';
import User from 'src/core/database/models/User';
import { SendPushNotification } from 'src/core/utils/sendPushNotification';
import moment from 'moment';
@ApiTags('Flash Sale')
@Controller('flash-sale')
export class FlashSaleController {
  constructor(private readonly flashSaleService: FlashSaleService) { }

  @Post()
  async create(@Body() createFlashSaleDto: CreateFlashSaleDto) {
    try {
      const checkIfExitData = await FlashSale.findAll({
        where: {
          end_date: {
            [Op.gte]: createFlashSaleDto.start_date,
          },
          is_active: true,
          service_id: createFlashSaleDto.service_id,
          branch_id: createFlashSaleDto.branch_id
        },
      });
      if (checkIfExitData && checkIfExitData.length > 0) return HandleResponse.buildErrObj(EC409, EM151, null);
      let data = await this.flashSaleService.create(createFlashSaleDto);
      this.sendNotifyToUser(data.id, createFlashSaleDto?.image, createFlashSaleDto?.start_date, createFlashSaleDto?.end_date);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  async sendNotifyToUser(flashId: string, image: string, start_date: any, end_date: any) {
    let local_date_time = moment(start_date).tz('Asia/Kolkata');
    start_date = local_date_time.format('DD-MMM-YYYY');
    let local_date_time1 = moment(end_date).tz('Asia/Kolkata');
    end_date = local_date_time1.format('DD-MMM-YYYY');
    let reqObj = {
      title: `Hello Slaylewks users! Flash Sale is here for a limited time! 🚀`,
      image: image || null,
      body: `Exciting news! Our exclusive Flash Sale is here for a limited time! 🚀 Don't miss out on incredible deals and discounts. Flash Sale Period: ${start_date} - ${end_date}`,
      data: { flash_id: flashId },
      dataModels: User
    };
    await new SendPushNotification().sendNotifyToUserPhone(reqObj);
  }

  @Get()
  async findAll() {
    try {
      let data = await this.flashSaleService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.flashSaleService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateFlashSaleDto: UpdateFlashSaleDto,
  ) {
    try {
      let data = await this.flashSaleService.update(id, updateFlashSaleDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let data = await this.flashSaleService.destroy(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('search')
  async getFilterList(@Body() flashSaleSDetails: PaginationDto) {
    try {
      let data = await this.flashSaleService.getFilterList(flashSaleSDetails);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
