import { Module } from '@nestjs/common';
import { FlashSaleService } from './flash-sale.service';
import { FlashSaleController } from './flash-sale.controller';

@Module({
  controllers: [FlashSaleController],
  providers: [FlashSaleService]
})
export class FlashSaleModule {}
