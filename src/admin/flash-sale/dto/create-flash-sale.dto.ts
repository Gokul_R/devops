import {
  IsNumber,
  IsDate,
  Min,
  Max,
  IsNotEmpty,
  IsString,
  Matches,
  IsBoolean,
  IsOptional,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export class CreateFlashSaleDto {
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  service_id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  branch_id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  image: string;

  @ApiProperty()
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsDate()
  @IsNotEmpty()
  start_date: Date | null;

  @ApiProperty()
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsDate()
  @IsNotEmpty()
  end_date: Date | null;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  discount_type: string;
  @ApiProperty()
  @IsNumber()
  @Min(0)
  @IsNotEmpty()
  discount_value: number;

  // @ApiProperty()
  // @IsNumber()
  // @IsNotEmpty()
  // price_before_sale: number;

  // @ApiProperty()
  // @IsNumber()
  // @IsNotEmpty()
  // price_after_sale: number;

  // @ApiProperty()
  // @IsNumber()
  // @Min(0)
  // @IsNotEmpty()
  // quantity_available: number;

  // @ApiProperty()
  // @IsNumber()
  // @Min(0)
  // sold_quantity: number;
}
