import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateFlashSaleDto } from './create-flash-sale.dto';

export class UpdateFlashSaleDto extends OmitType(CreateFlashSaleDto,['service_id','branch_id']) {}
