import { Body, Controller, Get, Patch, Post } from '@nestjs/common';
import { AdminService } from './admin.service';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM106, EM149 } from 'src/core/constants';
import { LogoutDto, logsDto } from './dto/logOut.dto';

@Controller()
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @Get()
  getHello(): string {
    return this.adminService.getHello();
  }

  // @ApiTags('Force Update')
  // @Post('force-update')
  // async updateForceUpdateVersion(@Body() updateForceUpdate: UpdateForceUpdate) {
  //   try {
  //     let data = await this.adminService.updateForceUpdateVersion(updateForceUpdate);;
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @ApiTags('Logout')
  @Post('logout')
  async logOut(@Body() logout: LogoutDto) {
    try {
      let data = await this.adminService.logOut(logout);
      return HandleResponse.buildSuccessObj(EC200, EM149, null);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @ApiTags('Logo-Footer')
  @Get('logo-footer')
  async logoFooter() {
    try {
      let data = await this.adminService.logoFooter();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('logs')
  async getLogs(@Body() logsDto: logsDto) {
    try {
      const data = await this.adminService.showLogs(logsDto);
      return {
        code: 200,
        status: true,
        message: 'Data Fetched Successfully',
        data: data,
      };
    } catch (error) {
      console.log(error);
    }
  }
}
