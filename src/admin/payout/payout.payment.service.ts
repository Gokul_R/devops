import { Body, Injectable } from '@nestjs/common';
import PaymentConfig from 'src/core/database/models/PaymentConfig';
import { RAZORPAY, Slaylewks_details } from 'src/core/constants';
import { razorPayAPIs } from 'src/core/constants/appConfig';
import axios from 'axios';
import { Contact, FundAccount, InstantRefund, SendPayoutAmount, ValidateBankAccount } from './payout.interfaces';
import Branch from 'src/core/database/models/Branch';
import Order from 'src/core/database/models/Order';
import PayoutRefundTnx, { PayOutRefund } from 'src/core/database/models/PayoutRefundTnx';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import BranchBankDetails from 'src/core/database/models/BranchBankDetails';
const Razorpay = require('razorpay');
import crypto from 'crypto';
import moment from 'moment';
import Payment_Txn from 'src/core/database/models/Payment-Tnx';
import { FundAccountTryAgain, PayoutTryAgainAmount, RefundAmountTryAgain } from './dto/view-payout.dto';
import { logger } from 'src/core/utils/logger';
import { newLogger } from 'src/core/utils/loggerNew';
@Injectable()
export class PayoutPaymentService {
  private razor_instance;
  private payment_config: PaymentConfig;

  constructor() {}

  init(config: PaymentConfig) {
    this.razor_instance = new Razorpay({
      key_id: config.razorpay_key,
      key_secret: config.razorpay_secret,
    });
  }

  private async setupPayment() {
    this.payment_config = await PaymentConfig.findOne();
    logger.info('payment_config_entry: ' + JSON.stringify(this.payment_config));
    // newLogger.info({
    //   type: 'payment',
    //   url: '/api/admin/v1/payout',
    //   response: { payment_config_entry: this.payment_config },
    // });
    if (!this.payment_config) throw Error('No payment method configured');
    if (this.payment_config.razorpay_key && this.payment_config.razorpay_secret) return this.init(this.payment_config);
    else throw Error('Razorpay credentials not configured!!');
  }

  // api call to razorpay
  private async apiCallFromAxios(url: string, body: any) {
    const result = await axios.post(url, body, {
      auth: {
        username: this.payment_config.razorpay_key,
        password: this.payment_config.razorpay_secret,
      },
      // maxContentLength: 100 * 1024 * 1024,
      headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
      maxContentLength: Infinity,
      maxBodyLength: Infinity,
    });
    return result;
  }

  // branch contacts register to razorpay.x
  private async contactRegistration(req_body: any) {
    logger.info('Branch_create_contact_entry: ' + JSON.stringify(req_body));

    const contact_body: Contact = {
      name: req_body?.branch_name ? req_body?.branch_name?.slice(0, 50) : req_body?.branch_name, // branch name
      email: req_body?.branch_email, // branch email
      contact: req_body?.branch_contact_no, // branch contact
      // type: "employee",
      type: 'vendor',
      reference_id: req_body.branch_id, //send branh id for refrence
    };
    const creatContact = await this.apiCallFromAxios(`${razorPayAPIs.contact}`, contact_body);
    const updateContactId = await Branch.update(
      { contact_id: creatContact?.data?.id },
      { where: { id: req_body.branch_id } },
    );
    logger.info('Branch_create_contact_exit: ' + JSON.stringify(creatContact?.data));
    newLogger.info({
      type: 'razorPay',
      url: '/api/admin/v1/payout',
      body: req_body,
      response: { Branch_create_contact: creatContact?.data },
    });
    // newLogger.info({
    //   type: 'razorPay',
    //   url: '/api/admin/v1/payout',
    //   response: { Branch_create_contact_exit: creatContact?.data },
    // });
    return creatContact?.data;
  }

  // register branch account to razorpay.x
  private async fundAccountRegistration(req_body: any) {
    logger.info('Branch_create_fundAc_entry: ' + JSON.stringify(req_body));
    // newLogger.info({
    //   type: 'razorPay',
    //   url: '/api/admin/v1/payout/create-fund-account',
    //   body: { Branch_create_fundAc_entry: req_body },
    // });
    const fund_ac_body: FundAccount = {
      contact_id: req_body.contact_id, //"cont_MhGmbrB0Fjnrz5",
      account_type: 'bank_account',
      bank_account: {
        name: req_body?.account_holder ? req_body?.account_holder?.slice(0, 50) : req_body?.account_holder, // "Gaurav Kumar",
        ifsc: req_body?.ifsc_code, // "HDFC0009107",
        account_number: req_body?.account_no, // "50100102283912"
      },
    };
    const fundAccount = await this.apiCallFromAxios(`${razorPayAPIs.fundAccount}`, fund_ac_body);
    const updateFundAccount = await Branch.update(
      { fund_account_id: fundAccount?.data?.id },
      { where: { id: req_body.branch_id } },
    );
    logger.info('Branch_create_fundAc_exit: ' + JSON.stringify(fundAccount?.data));
    newLogger.info({
      type: 'razorPay',
      url: '/api/admin/v1/payout/create-fund-account',
      body: req_body,
      response: { Branch_create_fundAc: fundAccount?.data },
    });
    return fundAccount?.data;
  }

  // check registered bank account original or fake from razorpay.x
  async accountValidationSendOneRupee(req_body: FundAccountTryAgain, checkConfig: boolean) {
    checkConfig && (await this.setupPayment());
    // logger.info('Branch_AcValidate_sendOneRupee_entry: ' + JSON.stringify(req_body));
    // newLogger.info({
    //   type: 'razorPay',
    //   url: '/api/admin/v1/payout',
    //   body: { Branch_AcValidate_sendOneRupee_entry: req_body },
    // });
    const validate_body: ValidateBankAccount = {
      account_number: this.payment_config?.account_number, //dev slayleks account
      fund_account: {
        id: req_body.fund_account_id, // 'fa_MhH019DwX2DtLN'
      },
      amount: 100, // send one repuee for testing if account is fake or valid account
      currency: 'INR',
      notes: {
        branch_id: req_body.branch_id,
      },
    };
    const checkBankAccount = await this.apiCallFromAxios(`${razorPayAPIs.bankAccountValidation}`, validate_body);
    let accountVerify: boolean = checkBankAccount?.data?.status == 'completed' ? true : false;
    const updateFundAccount = await Branch.update(
      {
        validate_account_id: checkBankAccount?.data?.id,
        account_verified: accountVerify,
        account_verify_status: checkBankAccount?.data?.status,
      },
      { where: { id: req_body.branch_id } },
    );
    logger.info('Branch_AcValidate_sendOneRupee_exit: ' + JSON.stringify(checkBankAccount?.data));
    // newLogger.info({
    //   type: 'razorPay',
    //   url: '/api/admin/v1/payout',
    //   response: { Branch_AcValidate_sendOneRupee_exit: checkBankAccount?.data },
    // });
    newLogger.info({
      type: 'razorPay',
      url: '/api/admin/v1/payout',
      body: validate_body,
      response: { Branch_AcValidate_sendOneRupee: checkBankAccount?.data },
    });
    return checkBankAccount?.data;
  }

  async bankAccountRegisterToRazorpay(req_body: any) {
    try {
      await this.setupPayment();
      logger.info('Branch_create_Ac_allproces_entry: ' + JSON.stringify(req_body));
      // newLogger.info({
      //   type: 'razorPay',
      //   url: '/api/admin/v1/payout',
      //   body: { Branch_create_Ac_allproces_entry: req_body },
      // });
      const getBranchDetails = await Branch.findOne({
        attributes: [
          BRANCH_ATTRIBUTES.BRANCH_NAME,
          BRANCH_ATTRIBUTES.ID,
          BRANCH_ATTRIBUTES.BRANCH_EMAIL,
          BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO,
          BRANCH_ATTRIBUTES.CONTACT_ID,
          BRANCH_ATTRIBUTES.FUND_AC_ID,
          BRANCH_ATTRIBUTES.VALIDATE_AC_ID,
          BRANCH_ATTRIBUTES.ACCOUNT_VERFIED,
        ],
        include: [
          {
            attributes: ['account_holder', 'account_no', 'ifsc_code'],
            model: BranchBankDetails,
          },
        ],
        where: { id: req_body.branch_id },
      });
      if (!getBranchDetails || !getBranchDetails?.bank_details) throw new Error('Branch bank details is empty!!');
      let branchObj = { ...getBranchDetails?.dataValues, ...getBranchDetails?.bank_details?.dataValues };
      // console.log(branchObj);
      branchObj.branch_id = req_body.branch_id;
      const createContactInfo = await this.contactRegistration(branchObj);
      if (!createContactInfo || !createContactInfo?.id) throw new Error('Contact Registration faield!!!');
      // console.log(createContactInfo);
      branchObj.contact_id = createContactInfo?.id;
      const createFundAccount = await this.fundAccountRegistration(branchObj);
      if (!createFundAccount || !createFundAccount?.id) throw new Error('createFundAccount Registration faield!!!');
      // console.log(createFundAccount);
      branchObj.fund_account_id = createFundAccount?.id;
      const checkBankAccount = await this.accountValidationSendOneRupee(branchObj, false);
      if (!checkBankAccount || !checkBankAccount?.id) throw new Error('Check Account number valid faield!!!');
      // console.log(checkBankAccount);
      branchObj.validate_account_id = checkBankAccount?.id;
      logger.info('Branch_create_Ac_allproces_exit: ' + JSON.stringify(branchObj));
      // newLogger.info({
      //   type: 'razorPay',
      //   url: '/api/admin/v1/payout',
      //   response: { Branch_create_Ac_allproces_exit: branchObj },
      // });/
      newLogger.info({
        type: 'razorPay',
        url: '/api/admin/v1/payout',
        body: req_body,
        response: { Branch_create_Ac_allproces: branchObj },
      });
      return branchObj;
    } catch (error) {
      logger.error('Branch_create_Ac_allproces_error: ' + JSON.stringify(error?.stack || error?.message || error));
      newLogger.error({
        type: 'razorPay',
        url: '/api/admin/v1/payout',
        body: req_body,
        response: { Branch_create_Ac_allproces_error: error?.stack || error?.message || error },
      });
      throw new Error(error?.response?.data?.error?.description || error);
    }
  }

  //send payout amount to branch bank from razorpay.x
  async sendPayoutAmount(req_body: PayoutTryAgainAmount) {
    await this.setupPayment();
    logger.info('PayoutAmount_entry: ' + JSON.stringify(req_body));
    // newLogger.info({
    //   type: 'payout',
    //   url: '/api/admin/v1/payout',
    //   body: { PayoutAmount_entry: req_body },
    // });
    let receipt_id = `ref#${req_body.order_id + '_' + Date.now()}`;
    const payout_body: SendPayoutAmount = {
      account_number: this.payment_config?.account_number,
      fund_account_id: req_body.fund_account_id, //  "fa_MhH019DwX2DtLN",
      amount: req_body.amount, //100
      currency: 'INR',
      mode: 'IMPS',
      purpose: 'payout',
      queue_if_low_balance: true,
      reference_id: receipt_id.slice(0, 40), // 'YBBC_1695118681088',
      narration: 'Slaylewks Fund Transfer',
      notes: {
        branch_id: req_body.branch_id, // 'cfde2373-38be-4e6e-a5f9-5f74bc5c065c',
        order_id: req_body.order_id, // 'YBBC_1695118681088'
      },
    };
    const sendPayoutAmount = await this.apiCallFromAxios(`${razorPayAPIs.payout}`, payout_body);
    logger.info('PayoutAmount_razorpay_api_exit: ' + JSON.stringify(sendPayoutAmount?.data));
    // newLogger.info({
    //   type: 'payout',
    //   url: '/api/admin/v1/payout',
    //   response: { PayoutAmount_razorpay_api_exit: sendPayoutAmount?.data },
    // });
    newLogger.info({
      type: 'payout',
      url: '/api/admin/v1/payout',
      body: payout_body,
      response: { PayoutAmount_razorpay_api: sendPayoutAmount?.data },
    });
    const updateOrderTable = await Order.update(
      { payout_id: sendPayoutAmount?.data?.id, payout_status: sendPayoutAmount?.data?.status },
      { where: { order_id: req_body.order_id } },
    );
    // create payout Tansaction records
    let obj = {
      branch_id: req_body.branch_id,
      order_id: req_body.order_id,
      tnx_type: PayOutRefund.PayOut,
      payout_id: sendPayoutAmount?.data?.id,
      payout_amount: sendPayoutAmount?.data?.amount,
      status: sendPayoutAmount?.data?.status,
      request_response: sendPayoutAmount?.data,
      merchant_id: sendPayoutAmount?.data?.merchant_id,
      utr: sendPayoutAmount?.data?.utr,
      reference_id: sendPayoutAmount?.data?.reference_id,
    };
    let payoutTnxTableData: any;
    if (req_body?.payout_id)
      payoutTnxTableData = await PayoutRefundTnx.update(obj, {
        where: { payout_id: req_body.payout_id, reference_id: req_body.reference_id },
      });
    else payoutTnxTableData = await PayoutRefundTnx.create(obj);
    logger.info('PayoutAmount_exit: ' + JSON.stringify(sendPayoutAmount?.data));
    newLogger.info({
      type: 'payout',
      url: '/api/admin/v1/payout',
      body: obj,
      response: { PayoutAmount_exit: sendPayoutAmount?.data },
    });
    return sendPayoutAmount?.data;
  }

  // //send refund amount to user account from razorpay
  async instantRefundAmountToUser(req_body: RefundAmountTryAgain) {
    try {
      await this.setupPayment();
      logger.info('Reject_RefundAmount_entry: ' + JSON.stringify(req_body));
      // newLogger.info({
      //   type: 'refund',
      //   url: '/api/admin/v1/payout',
      //   body: { Reject_RefundAmount_entry: req_body },
      // });
      let receipt_id = `ref#${req_body.order_id + '_' + Date.now()}`;
      let req_obj: InstantRefund = {
        receipt: receipt_id.slice(0, 40), // send order id for our reference
        speed: 'normal', // 'normal', // optimum
        notes: {
          order_id: req_body.order_id,
          user_id: req_body.user_id,
        },
      };
      // If the amount parameter is passed, send the incoming amount. Otherwise full amount will be refunded
      if (req_body?.amount && req_body?.amount >= 100 && !req_body?.full_refund) req_obj['amount'] = req_body.amount;

      const instantRefund = await this.apiCallFromAxios(
        `${razorPayAPIs.instantRefund}/${req_body.payment_id}/refund`,
        req_obj,
      );
      logger.info('Reject_RefundAmount_razorpay_exit: ' + JSON.stringify(instantRefund?.data));
      newLogger.info({
        type: 'refund',
        url: '/api/admin/v1/payout',
        body: req_obj,
        response: { Reject_RefundAmount_razorpay: instantRefund?.data },
      });
      const updateRefundStatus = await this.updateRefundDataToTables(req_body, instantRefund);
      return instantRefund?.data;
    } catch (error) {
      logger.error('Reject_RefundAmount_error: ' + JSON.stringify(error?.stack || error?.message || error));
      newLogger.error({
        type: 'refund',
        url: '/api/admin/v1/payout',
        body: req_body,
        response: { Reject_RefundAmount_error: error?.stack || error?.message || error },
      });
      throw new Error(error?.response?.data?.error?.description || error);
    }
  }
  // refund status update the tables
  private async updateRefundDataToTables(req_body: RefundAmountTryAgain, instantRefund: any) {
    logger.info('Reject_RefundAmount_updatedTable_entry: ' + JSON.stringify(req_body));
    // newLogger.info({
    //   type: 'refund',
    //   url: '/api/admin/v1/payout',
    //   body: { Reject_RefundAmount_updatedTable_entry: req_body },
    // });
    let obj = {
      user_id: req_body.user_id,
      order_id: req_body.order_id,
      tnx_type: PayOutRefund.Refund,
      payout_id: instantRefund?.data?.id,
      payout_amount: instantRefund?.data?.amount,
      status: instantRefund?.data?.status,
      request_response: instantRefund?.data,
      merchant_id: instantRefund?.data?.merchant_id,
      utr: instantRefund?.data?.utr,
      reference_id: instantRefund?.data?.receipt,
    };
    let payoutTnxTableData: any;
    if (req_body?.payout_id)
      payoutTnxTableData = await PayoutRefundTnx.update(obj, {
        where: { payout_id: req_body.payout_id, reference_id: req_body.reference_id },
      });
    else payoutTnxTableData = await PayoutRefundTnx.create(obj);
    const updatePaymentTnx = await Payment_Txn.update(
      { refund_status: instantRefund?.data?.status, amount_refunded: req_body.amount },
      { where: { order_id: req_body.order_id, user_id: req_body.user_id } },
    );

    const updateOrderData = await Order.update(
      { refund_status: instantRefund?.data?.status, refund_amount: instantRefund?.data?.amount, refunded: true },
      { where: { order_id: req_body.order_id, user_id: req_body.user_id } },
    );
    logger.info('Reject_RefundAmount_updatedTable_exit: ' + JSON.stringify(req_body));
    // newLogger.info({
    //   type: 'refund',
    //   url: '/api/admin/v1/payout',
    //   response: { Reject_RefundAmount_updatedTable_exit: req_body },
    // });
    return;
  }

  /**
   *     Below function are the webhook handler only.
   *     These api's trigger from razorpay to slaylewks server.
   *     written by sasi - 2023-10-01
   **/

  /*check razorpay incoming request signature is wright or not*/
  verifyWebhooks(body: any, receivedSignature: any) {
    logger.info('Razorpay_webhook_signature_entry_body: ' + JSON.stringify(body));
    logger.info('Razorpay_webhook_signature_entry_signature: ' + JSON.stringify(receivedSignature));
    // newLogger.info({
    //   type: 'webhook',
    //   url: '/api/admin/v1/payout',
    //   body: {
    //     Razorpay_webhook_signature_entry_signature: receivedSignature,
    //     Razorpay_webhook_signature_entry_body: body,
    //   },
    // });
    const expectedSignature = crypto
      .createHmac('sha256', process.env.RAZORPAY_WH_SECRET)
      .update(JSON.stringify(body))
      .digest('hex');
    console.log('verifyWebhooks signature ====>>>>', receivedSignature === expectedSignature);
    logger.info('Razorpay_webhook_signature_exit: ' + receivedSignature === expectedSignature);
    // newLogger.info({
    //   type: 'webhook',
    //   url: '/api/admin/v1/payout',
    //   response: { Razorpay_webhook_signature_exit: receivedSignature === expectedSignature },
    // });
    return receivedSignature === expectedSignature;
  }

  //TODO payout webwook handler work
  async checkAndUpdateIfPayoutCompleteOrNot(headers: any, req_body: any) {
    try {
      //Signature Verification
      console.log('Razorpay payout webhook handler called request headers ======>>>>', JSON.stringify(headers));
      console.log('Razorpay payout webhook handler called request body ======>>>>', JSON.stringify(req_body));
      logger.info('Razorpay_Payout_webhook_entry_header: ' + JSON.stringify(headers));
      logger.info('Razorpay_Payout_webhook_entry_body: ' + JSON.stringify(req_body));
      // newLogger.info({
      //   type: 'webhook',
      //   url: '/api/admin/v1/payout',
      //   body: {
      //     Razorpay_Payout_webhook_entry_header: headers,
      //     Razorpay_Payout_webhook_entry_body: req_body,
      //   },
      // });

      if (!this.verifyWebhooks(req_body, headers['x-razorpay-signature']))
        throw new Error('Webhook Signature Mismatch - ' + headers['x-razorpay-signature']);
      //Idempotency handling
      const alreadyProcessedEvent = await PayoutRefundTnx.findOne({
        where: { event_id: headers['x-razorpay-event-id'] },
      });
      if (alreadyProcessedEvent) throw new Error('Already processed webhook event: ' + headers['x-razorpay-event-id']);
      const body = req_body;
      logger.info(`Payout_webhook_event_entry_${body?.event}`);
      // newLogger.info({
      //   type: 'webhook',
      //   url: '/api/admin/v1/payout',
      //   body: { Payout_webhook_event_entry: body?.event },
      // });

      // payout payment complated then update the incoming status
      if (body.event === 'payout.processed') {
        const payout = body.payload['payout'].entity;
        await PayoutRefundTnx.update(
          { event_id: headers['x-razorpay-event-id'], utr: payout.utr, status: payout.status, update_response: payout },
          { where: { payout_id: payout.id } },
        );
        await Order.update({ payout: true, payout_status: payout.status }, { where: { payout_id: payout.id } });
        logger.info(`Payout_webhook_event_exit_${body?.event}`);
        newLogger.info({
          type: 'webhook',
          url: '/api/admin/v1/payout',
          response: { Payout_webhook_event_exit: body?.event },
        });
      }
      // payout payment any error throug then update the incoming status
      else if (
        body.event === 'payout.failure' ||
        body.event === 'payout.pending' ||
        body.event === 'payout.rejected' ||
        body.event === 'payout.queued' ||
        body.event === 'payout.reversed' ||
        // || body.event === "payout.initiated"
        // || body.event === "payout.updated"
        body.event === 'payout.failed'
      ) {
        const payout = body.payload['payout'].entity;
        // update PayoutRefundTnx table details
        await PayoutRefundTnx.update(
          { event_id: headers['x-razorpay-event-id'], status: payout.status, utr: payout.utr, update_response: payout },
          { where: { payout_id: payout.id } },
        );
        await Order.update({ payout_status: payout.status }, { where: { payout_id: payout.id } });
        logger.info(`Payout_webhook_event_exit_${body?.event}`);
        newLogger.info({
          type: 'webhook',
          url: '/api/admin/v1/payout',
          response: { Payout_webhook_event_exit: body?.event },
        });
      }
      // bank account validation payment any error then update the incoming status
      else if (body.event === 'fund_account.validation.completed' || body.event === 'fund_account.validation.failed') {
        const fundAccount = body.payload['fund_account.validation'].entity;
        let accountVerify: boolean = fundAccount.status == 'completed' ? true : false;
        // update branch table details
        await Branch.update(
          {
            account_verify_status: fundAccount.status,
            account_verified: accountVerify,
            validate_account_id: fundAccount.id,
          },
          { where: { fund_account_id: fundAccount.fund_account.id } },
        );
        logger.info(`Payout_webhook_event_exit_${body?.event}`);
        // newLogger.info({
        //   type: 'webhook',
        //   url: '/api/admin/v1/payout',
        //   response: { Payout_webhook_event_exit: body?.event },
        // });
        newLogger.info({
          type: 'webhook',
          url: '/api/admin/v1/payout',
          body: {
            Razorpay_Payout_webhook_entry_header: headers,
            Razorpay_Payout_webhook_entry_body: req_body,
          },
          response: { Payout_webhook_event_entry: body?.event },
        });
      }
      return;
    } catch (error) {
      console.log('Razorpay payout webhook handler internal error ====>>>', JSON.stringify(error));
      logger.error('Razorpay_Payout_webhook_error: ' + JSON.stringify(error?.stack || error?.message || error));
      // newLogger.error({
      //   type: 'webhook',
      //   url: '/api/admin/v1/payout',
      //   response: { Razorpay_Payout_webhook_error: error?.stack || error?.message || error },
      // });
      newLogger.error({
        type: 'webhook',
        url: '/api/admin/v1/payout',
        body: {
          Razorpay_Payout_webhook_entry_header: headers,
          Razorpay_Payout_webhook_entry_body: req_body,
        },
        response: { Razorpay_Payout_webhook_error: error?.stack || error?.message || error },
      });
      throw new Error(error);
    }
  }
  //TODO Refund webwook handler work
  async checkAndUpdateIfRefundCompleteOrNot(headers: any, req_body: any) {
    try {
      //Signature Verification
      console.log('Razorpay Refund webhook handler called request headers ======>>>>', JSON.stringify(headers));
      console.log('Razorpay Refund webhook handler called request body ======>>>>', JSON.stringify(req_body));
      logger.info('Razorpay_Refund_webhook_entry_header: ' + JSON.stringify(headers));
      logger.info('Razorpay_Refund_webhook_entry_body: ' + JSON.stringify(req_body));
      newLogger.info({
        type: 'webhook',
        url: '/api/admin/v1/payout',
        body: {
          Razorpay_Refund_webhook_entry_header: headers,
          Razorpay_Refund_webhook_entry_body: req_body,
        },
      });
      if (!this.verifyWebhooks(req_body, headers['x-razorpay-signature']))
        throw new Error('Webhook Signature Mismatch - ' + headers['x-razorpay-signature']);
      //Idempotency handling
      const alreadyProcessedEvent = await PayoutRefundTnx.findOne({
        where: { event_id: headers['x-razorpay-event-id'] },
      });
      if (alreadyProcessedEvent) throw new Error('Already processed webhook event: ' + headers['x-razorpay-event-id']);

      const refund = req_body.payload['refund'].entity;
      logger.info(`Refund_webhook_event_entry_${req_body?.event}`);
      // newLogger.info({
      //   type: 'webhook',
      //   url: '/api/admin/v1/payout',
      //   body: { Refund_webhook_event_entry: req_body?.event },
      // });
      // payout payment complated then update the incoming status
      if (req_body.event === 'refund.processed' || req_body.event === 'refund.failed') {
        // or check 'refund.created'
        const updateRefundTnx = await PayoutRefundTnx.update(
          { event_id: headers['x-razorpay-event-id'], status: refund?.status, update_response: refund },
          { where: { reference_id: refund?.receipt } },
        );
        const updatePaymentTnx = await Payment_Txn.update(
          { refund_status: refund?.status, amount_refunded: refund?.amount },
          { where: { payment_id: refund?.payment_id } },
        );
        const updateOrderData = await Order.update(
          { refund_status: refund?.status },
          { where: { payment_id: refund?.payment_id } },
        );
        logger.info(`Refund_webhook_event_exit_${req_body?.event}`);
        // newLogger.info({
        //   type: 'webhook',
        //   url: '/api/admin/v1/payout',
        //   response: { Refund_webhook_event_exit: req_body?.event },
        // });
        newLogger.info({
          type: 'webhook',
          url: '/api/admin/v1/payout',
          body: { Refund_webhook_event_entry: req_body?.event },
          response: { Refund_webhook_event_exit: req_body?.event },
        });
      }
      return;
    } catch (error) {
      console.log('Razorpay refund webhook handler internal error ====>>>', JSON.stringify(error));
      logger.error('Razorpay_Refund_webhook_error: ' + JSON.stringify(error?.stack || error?.message || error));
      newLogger.error({
        type: 'webhook',
        url: '/api/admin/v1/payout',
        body: req_body,
        response: { Razorpay_Refund_webhook_error: error?.stack || error?.message || error },
      });
      throw new Error(error);
    }
  }
}
