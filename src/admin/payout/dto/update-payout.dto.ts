import { PartialType } from '@nestjs/swagger';
import { GetPayoutDto } from './get-payout.dto';

export class UpdatePayoutDto extends PartialType(GetPayoutDto) { }
