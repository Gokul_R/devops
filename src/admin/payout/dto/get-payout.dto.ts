import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsOptional, IsString, ValidateIf } from "class-validator";
import { BRANCH_ID, VENDOR_ID } from "src/core/constants";
import { APPOINTMENT_STATUS } from "src/core/database/models/Appointment";
import { PayOutRefund } from "src/core/database/models/PayoutRefundTnx";
import { PaginationDto } from "src/core/interfaces/shared.dto";
import { VendorType } from "src/core/utils/enum";

export class GetPayoutDto extends PaginationDto {
    @ApiProperty({ example: '2023-08-01' })
    @IsOptional()
    @IsString()
    from_date?: string;

    @ApiProperty({ example: '2023-08-31' })
    @IsOptional()
    @IsString()
    to_date?: string;

    @ApiProperty({ example: VENDOR_ID })
    @IsOptional()
    @IsString()
    vendor_id?: string;

    @ApiProperty({ example: BRANCH_ID })
    @IsOptional()
    @IsString()
    branch_id?: string;


    @ApiProperty({ example: APPOINTMENT_STATUS.COMPLETED })
    @IsOptional()
    @IsEnum(APPOINTMENT_STATUS)
    status: string;

    @ApiProperty({ example: `${VendorType.BUSINESS} '(or)' ${VendorType.FREELANCER}` })
    @IsOptional()
    @IsString()
    vendor_type: string;
}

export class PayoutTnxDto extends PaginationDto {
    @ApiProperty({ example: '2023-08-01' })
    @IsOptional()
    @ValidateIf((o) => o.to_date != null)
    @IsString()
    from_date?: string;

    @ApiProperty({ example: '2023-08-31' })
    @IsOptional()
    @ValidateIf((o) => o.from_date != null)
    @IsString()
    to_date?: string;

    @ApiProperty({ example: `${PayOutRefund.PayOut} (or) ${PayOutRefund.Refund} (or) ${PayOutRefund.FundAccount}` })
    @IsNotEmpty()
    @IsString()
    @IsEnum(PayOutRefund)
    screen_type?: string;
}
