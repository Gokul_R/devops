import { ApiProperty, PickType } from "@nestjs/swagger";
import { IsBoolean, IsEnum, IsInt, IsNotEmpty, IsNumber, IsOptional, IsString, Min, ValidateIf } from "class-validator";
import { BRANCH_ID, FUND_AC_ID, ORDER_ID, PAYMENT_ID, PAYOUT_ID, REF_ID } from "src/core/constants";
import { APPOINTMENT_STATUS } from "src/core/database/models/Appointment";
import { PaginationDto } from "src/core/interfaces/shared.dto";

export class ViewPayOutDto extends PickType(PaginationDto, ['limit', 'pagNo']) {

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    branch_id?: string;

    @ApiProperty({ example: APPOINTMENT_STATUS.COMPLETED })
    @IsOptional()
    @IsEnum(APPOINTMENT_STATUS)
    status: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    from_date: string

    @ApiProperty()
    @IsOptional()
    @IsString()
    to_date: string
}
export class CreateBankToRazorPay {

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    branch_id?: string;

}

export class PayoutTryAgainAmount {

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    branch_id?: string;

    @ApiProperty({ example: ORDER_ID })
    @IsNotEmpty()
    order_id?: string;

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    fund_account_id?: string;

    @ApiProperty({ example: PAYOUT_ID })
    @IsNotEmpty()
    payout_id?: string;

    @ApiProperty({ example: REF_ID })
    @IsNotEmpty()
    reference_id?: string;

    @ApiProperty({ example: 100 })
    @IsNotEmpty()
    @IsNumber()
    @IsInt()
    @Min(100, { message: 'The amount should be greater than 100 paisa.' })
    amount?: number;

}

export class FundAccountTryAgain {

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    branch_id?: string;

    @ApiProperty({ example: FUND_AC_ID })
    @IsNotEmpty()
    fund_account_id?: string;

}

export class RefundAmountTryAgain {

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    user_id?: string;

    @ApiProperty({ example: ORDER_ID })
    @IsNotEmpty()
    order_id?: string;

    @ApiProperty({ example: PAYOUT_ID })
    @IsNotEmpty()
    payout_id?: string;

    @ApiProperty({ example: PAYMENT_ID })
    @IsNotEmpty()
    payment_id?: string;

    @ApiProperty({ example: REF_ID })
    @IsNotEmpty()
    reference_id?: string;
    @IsBoolean()
    full_refund?: boolean;

    @ApiProperty({ example: 100 })
    @IsNotEmpty()
    @IsNumber()
    @IsInt()
    @Min(100, { message: 'The amount should be greater than 100 paisa.' })
    amount?: number;
}