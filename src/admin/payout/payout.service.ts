import { Injectable } from '@nestjs/common';
import { ORDER_ATTRIBUTES } from 'src/core/attributes/order';
import Order from 'src/core/database/models/Order';
import sequelize from 'sequelize';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import Branch from 'src/core/database/models/Branch';
import { Op } from 'sequelize';
import { ViewPayOutDto } from './dto/view-payout.dto';
import { GetPayoutDto, PayoutTnxDto } from './dto/get-payout.dto';
import Service from 'src/core/database/models/Services';
import OrderService from 'src/core/database/models/OrderServices';
import PayoutRefundTnx, { PayOutRefund } from 'src/core/database/models/PayoutRefundTnx';
import Vendor from 'src/core/database/models/Vendor';

@Injectable()
export class PayoutService {
  async getPayOutFilterList(req_data: GetPayoutDto) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { branch_name: { [Op.iLike]: `%${searchText}%` } },
          { id: { [Op.iLike]: `%${searchText}%` } },
          // { order_id: { [Op.iLike]: `%${searchText}%` } },
          { branch_email: { [Op.iLike]: `%${searchText}%` } },
          { branch_contact_no: { [Op.iLike]: `%${searchText}%` } },
          { vendor_type: { [Op.iLike]: `%${searchText}%` } },
          { reg_id: { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let dateStatus;
    if (req_data.from_date && req_data.to_date) {
      dateStatus = {
        created_at: {
          [Op.gt]: `${req_data.from_date} 00:00:00.000`,
          [Op.lte]: `${req_data.to_date} 23:59:59.999`,
        },
      };
    }
    let orderStatus: any = req_data.status ? { order_status: req_data.status } : '';
    let vendorId: any = req_data.vendor_id ? { vendor_id: req_data.vendor_id } : '';
    let branchId: any = req_data.branch_id ? { id: req_data.branch_id } : '';
    let vendorType: any = req_data.vendor_type ? { vendor_type: req_data.vendor_type } : '';
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let getPayout: any = await Order.findAndCountAll({
      attributes: [
        'branch_id',
        [
          sequelize.literal('ROUND(CAST(SUM(COALESCE("payout_amount", 0)) AS NUMERIC), 2)'),
          'total_payout_amount'
        ],   // [sequelize.fn('SUM', sequelize.literal('CASE WHEN "payout" = false THEN COALESCE("payout_amount", 0) ELSE 0 END')), 'total_payout_amount'], //this condition ignore the payout amount
        [sequelize.literal('(SELECT "branch_name" FROM "branches" WHERE "branches"."id" = "Order"."branch_id")'), 'branch_name']],
      include: [
        {
          attributes: [
            BRANCH_ATTRIBUTES.ID, BRANCH_ATTRIBUTES.BRANCH_NAME, BRANCH_ATTRIBUTES.BRANCH_EMAIL,
            BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO, BRANCH_ATTRIBUTES.REG_ID, BRANCH_ATTRIBUTES.BRANCH_IMAGE,
            BRANCH_ATTRIBUTES.COMMISSION_TYPE, BRANCH_ATTRIBUTES.PAYMENT_COMMISSION, BRANCH_ATTRIBUTES.IS_TAX_CHECKED,
            BRANCH_ATTRIBUTES.TAX_PERCENTAGE, BRANCH_ATTRIBUTES.VENDOR_TYPE, BRANCH_ATTRIBUTES.CREATED_AT, BRANCH_ATTRIBUTES.VENDOR_ID
          ],
          model: Branch,
          as: 'branch',
          paranoid: false,
          where: {
            is_deleted: false,
            is_active: true,
            ...vendorId,
            ...branchId,
            ...search,
            ...vendorType
          }
        }
      ],
      paranoid: false,
      where: {
        is_deleted: false,
        is_active: true,
        // payout: false,
        ...orderStatus,
        ...dateStatus
      },
      limit: limit,
      offset: offset,
      group: ['branch_id', 'branch.id'],
      order: [[sequelize.literal('total_payout_amount'), 'DESC']]
    });
    getPayout.count = getPayout?.count?.length || 0;
    return getPayout;
  }

  async getPayoutView(req_data: ViewPayOutDto) {
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let orderStatus: any = req_data.status ? { order_status: req_data.status } : '';
    let dateStatus: {};
    if (req_data.from_date && req_data.to_date) {
      dateStatus = {
        [Op.and]: [
          {
            '$Order.created_at$': {
              [Op.gte]: `${req_data.from_date} 00:00:00.000`,
              [Op.lte]: `${req_data.to_date} 23:59:59.999`,
            },
          },
        ],
      };
    }
    let where = {
      where: {
        branch_id: req_data.branch_id,
        is_active: true,
        is_deleted: false,
        ...orderStatus,
        ...dateStatus
        // payout: false,
      },
    };
    let payout_count = await Order.count({
      ...where
    });
    let payout_orders = await Order.findAll({
      attributes: [ORDER_ATTRIBUTES.id, ORDER_ATTRIBUTES.PAYOUT_AMOUNT, ORDER_ATTRIBUTES.customer_name,
      ORDER_ATTRIBUTES.order_id, ORDER_ATTRIBUTES.PAYOUT, ORDER_ATTRIBUTES.payment_status,
      ORDER_ATTRIBUTES.final_price, ORDER_ATTRIBUTES.order_status, ORDER_ATTRIBUTES.payment_status,
      ORDER_ATTRIBUTES.is_payment_completed, ORDER_ATTRIBUTES.customer_email, ORDER_ATTRIBUTES.created_at,
      ORDER_ATTRIBUTES.customer_mobile, ORDER_ATTRIBUTES.pay_tnx_id, ORDER_ATTRIBUTES.gst, ORDER_ATTRIBUTES.discount],
      // required: true,
      include: [{
        attributes: ['id'],
        model: OrderService,
        as: 'ordersService',
        required: true,
        include: [{
          attributes: ['service_name', 'id'],
          model: Service,
          // required: true
        }]
      }],
      ...where,
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']]
    });
    return { count: payout_count, rows: payout_orders };
  }

  async getTnxList(req_body: PayoutTnxDto) {
    let getTnxData: any;
    let pagNo = req_body.pagNo || 1;
    let limit = req_body.limit || 10;
    let offset = (pagNo - 1) * req_body.limit;
    let dateStatus: any;
    if (req_body.from_date && req_body.to_date) {
      dateStatus = {
        created_at: {
          [Op.gte]: `${req_body.from_date} 00:00:00.000`,
          [Op.lte]: `${req_body.to_date} 23:59:59.999`,
        },
      };
    }
    let search: any;
    //Payout
    if (req_body.screen_type == PayOutRefund.PayOut) {
      if (req_body.searchText) {
        const searchText = req_body.searchText.toLowerCase();
        search = {
          [Op.or]: [
            { order_id: { [Op.iLike]: `%${searchText}%` } },
            { reference_id: { [Op.iLike]: `%${searchText}%` } },
            { payout_id: { [Op.iLike]: `%${searchText}%` } },
            // //order table
            // { '$order.customer_name$': { [Op.iLike]: `%${searchText}%` } },
            // { '$order.customer_mobile$': { [Op.iLike]: `%${searchText}%` } },
            // { '$order.customer_email$': { [Op.iLike]: `%${searchText}%` } },
            //branch and vendor table
            { '$branch.branch_name$': { [Op.iLike]: `%${searchText}%` } },
            { '$branch->vendor.vendor_name$': { [Op.iLike]: `%${searchText}%` } },
            { '$branch.branch_email$': { [Op.iLike]: `%${searchText}%` } },
            { '$branch.branch_contact_no$': { [Op.iLike]: `%${searchText}%` } },
            { '$branch.reg_id$': { [Op.iLike]: `%${searchText}%` } },
          ],
        };
      }
      getTnxData = await PayoutRefundTnx.findAndCountAll({
        attributes: { exclude: ['request_response', 'update_response'] },
        include: [{
          attributes: [
            BRANCH_ATTRIBUTES.ID, BRANCH_ATTRIBUTES.BRANCH_NAME, BRANCH_ATTRIBUTES.BRANCH_EMAIL,
            BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO, BRANCH_ATTRIBUTES.REG_ID, BRANCH_ATTRIBUTES.BRANCH_IMAGE,
            BRANCH_ATTRIBUTES.COMMISSION_TYPE, BRANCH_ATTRIBUTES.PAYMENT_COMMISSION, BRANCH_ATTRIBUTES.IS_TAX_CHECKED,
            BRANCH_ATTRIBUTES.TAX_PERCENTAGE, BRANCH_ATTRIBUTES.VENDOR_TYPE, BRANCH_ATTRIBUTES.CREATED_AT, BRANCH_ATTRIBUTES.VENDOR_ID,
            BRANCH_ATTRIBUTES.CONTACT_ID, BRANCH_ATTRIBUTES.FUND_AC_ID, BRANCH_ATTRIBUTES.VALIDATE_AC_ID, BRANCH_ATTRIBUTES.ACCOUNT_VERFIED
          ],
          model: Branch,
          include: [{
            attributes: ['id', 'vendor_name'],
            model: Vendor
          }],
        },
        {
          attributes: ['id', 'order_id', 'refunded', 'refund_status', 'refund_amount',
            'cancellation_fee', 'customer_name', 'customer_mobile', 'customer_email', 'cancellation_reason',
            'comment', 'remarks', 'cancelled_by', 'cancelled_user_id', 'user_id', 'payout_amount', 'payout'],
          model: Order
        }],
        where: {
          tnx_type: PayOutRefund.PayOut,
          ...search,
          ...dateStatus
        },
        limit: limit,
        offset: offset,
        order: [['updated_at', 'DESC']]
      });
    } else if (req_body.screen_type == PayOutRefund.Refund) {  // Refund
      if (req_body.searchText) {
        const searchText = req_body.searchText.toLowerCase();
        search = {
          [Op.or]: [
            { order_id: { [Op.iLike]: `%${searchText}%` } },
            { reference_id: { [Op.iLike]: `%${searchText}%` } },
            { payout_id: { [Op.iLike]: `%${searchText}%` } },
            { '$order.customer_name$': { [Op.iLike]: `%${searchText}%` } },
            { '$order.customer_mobile$': { [Op.iLike]: `%${searchText}%` } },
            { '$order.customer_email$': { [Op.iLike]: `%${searchText}%` } },
          ],
        };
      }
      getTnxData = await PayoutRefundTnx.findAndCountAll({
        attributes: { exclude: ['request_response', 'update_response'] },
        include: [
          {
            attributes: ['id', 'order_id', 'refunded', 'refund_status', 'refund_amount',
              'cancellation_fee', 'customer_name', 'customer_mobile', 'customer_email', 'cancellation_reason',
              'comment', 'remarks', 'cancelled_by', 'cancelled_user_id', 'user_id', 'payout_amount', 'payout'],
            model: Order
          }],
        where: {
          tnx_type: PayOutRefund.Refund,
          ...search,
          ...dateStatus
        },
        limit: limit,
        offset: offset,
        order: [['updated_at', 'DESC']]
      });
    } else {  // Fund Account
      if (req_body.searchText) {
        const searchText = req_body.searchText.toLowerCase();
        search = {
          [Op.or]: [
            { branch_name: { [Op.iLike]: `%${searchText}%` } },
            { '$vendor.vendor_name$': { [Op.iLike]: `%${searchText}%` } },
            { branch_email: { [Op.iLike]: `%${searchText}%` } },
            { branch_contact_no: { [Op.iLike]: `%${searchText}%` } },
            { reg_id: { [Op.iLike]: `%${searchText}%` } },
          ],
        };
      }
      getTnxData = await Branch.findAndCountAll({
        attributes: [
          BRANCH_ATTRIBUTES.ID, BRANCH_ATTRIBUTES.BRANCH_NAME, BRANCH_ATTRIBUTES.BRANCH_EMAIL,
          BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO, BRANCH_ATTRIBUTES.REG_ID, BRANCH_ATTRIBUTES.BRANCH_IMAGE,
          BRANCH_ATTRIBUTES.COMMISSION_TYPE, BRANCH_ATTRIBUTES.PAYMENT_COMMISSION, BRANCH_ATTRIBUTES.IS_TAX_CHECKED,
          BRANCH_ATTRIBUTES.TAX_PERCENTAGE, BRANCH_ATTRIBUTES.VENDOR_TYPE, BRANCH_ATTRIBUTES.CREATED_AT, BRANCH_ATTRIBUTES.VENDOR_ID,
          BRANCH_ATTRIBUTES.CONTACT_ID, BRANCH_ATTRIBUTES.FUND_AC_ID, BRANCH_ATTRIBUTES.VALIDATE_AC_ID, BRANCH_ATTRIBUTES.ACCOUNT_VERFIED
        ],
        include: [{
          attributes: ['id', 'vendor_name'],
          model: Vendor
        }],
        where: {
          // account_verified: false,
          ...search,
          ...dateStatus
        },
        limit: limit,
        offset: offset,
        order: [['updated_at', 'DESC']]
      });
    }
    return getTnxData;
  }
}
