import { Controller, Get, Post, Body, Patch, Param, Delete, Headers } from '@nestjs/common';
import { PayoutService } from './payout.service';
import { GetPayoutDto, PayoutTnxDto } from './dto/get-payout.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EC404, EM100, EM106, EM119, EM140, EM142, EM146, EM147 } from 'src/core/constants';
import { CreateBankToRazorPay, FundAccountTryAgain, PayoutTryAgainAmount, RefundAmountTryAgain, ViewPayOutDto } from './dto/view-payout.dto';
import { PayoutPaymentService } from './payout.payment.service';
import PayoutRefundTnx from 'src/core/database/models/PayoutRefundTnx';
import Branch from 'src/core/database/models/Branch';
import Order from 'src/core/database/models/Order';

@Controller('payout')
@ApiTags('Payout')
export class PayoutController {
  constructor(private readonly payoutService: PayoutService) { }

  @Post('payout-list')
  async getPayoutList(@Body() getpayoutService: GetPayoutDto) {
    try {
      let data = await this.payoutService.getPayOutFilterList(getpayoutService);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('payout-view')
  async getPayoutView(@Body() getpayoutService: ViewPayOutDto) {
    try {
      let data = await this.payoutService.getPayoutView(getpayoutService);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('transactions')
  async getTnxList(@Body() getpayoutTnxService: PayoutTnxDto) {
    try {
      let data = await this.payoutService.getTnxList(getpayoutTnxService);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  //--------------------------------------------------------------------------------------

  /**
   * The Below router amount and create bank account reassign to razorpay only don't edit the below router function 
   * Written by sasi - 2023-10-10
   */

  @Post('create-fund-account')
  async creteFundAccount(@Body() createAccount: CreateBankToRazorPay) {
    try {
      let data = await new PayoutPaymentService().bankAccountRegisterToRazorpay(createAccount);
      return HandleResponse.buildSuccessObj(EC200, EM140, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('pout-try-again')
  async PayoutTryAgain(@Body() failedPayoutTryAgain: PayoutTryAgainAmount) {
    try {
      let checkIfthere = await PayoutRefundTnx.count({ where: { payout_id: failedPayoutTryAgain.payout_id, reference_id: failedPayoutTryAgain.reference_id } });
      if (checkIfthere) {
        let data = await new PayoutPaymentService().sendPayoutAmount(failedPayoutTryAgain);
        return HandleResponse.buildSuccessObj(EC200, EM140, data);
      } else
        return HandleResponse.buildErrObj(EC404, EM119);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('fav-try-again')
  async FundAccountTryAgain(@Body() failedFundAccountTryAgain: FundAccountTryAgain) {
    try {
      let checkIfthere = await Branch.count({ where: { id: failedFundAccountTryAgain.branch_id, fund_account_id: failedFundAccountTryAgain.fund_account_id } });
      if (checkIfthere) {
        let data = await new PayoutPaymentService().accountValidationSendOneRupee(failedFundAccountTryAgain, true);
        return HandleResponse.buildSuccessObj(EC200, EM147, data);
      } else
        return HandleResponse.buildErrObj(EC404, EM119);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('rfund-try-again')
  async RefundTryAgain(@Body() rFundTryAgain: RefundAmountTryAgain) {
    try {
      let checkIfthere = await Order.count({ where: { payment_id: rFundTryAgain.payment_id, order_id: rFundTryAgain.order_id } });
      if (checkIfthere) {
        let data = await new PayoutPaymentService().instantRefundAmountToUser(rFundTryAgain);
        return HandleResponse.buildSuccessObj(EC200, EM146, data);
      } else
        return HandleResponse.buildErrObj(EC404, EM119);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  //-------------------------------------------------------------------------------

  /**
   * The Below router razorpay webhook only don't edit the below router function 
   * Written by sasi - 2023-10-10
   */


  @Post('webhookHandler') // payout and account validation handler
  async PayoutWebHookHandler(@Headers() req_header: any, @Body() req_body: any) {
    try {
      let data = await new PayoutPaymentService().checkAndUpdateIfPayoutCompleteOrNot(req_header, req_body);
      return HandleResponse.buildSuccessObj(EC200, EM142);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('refund-webhookHandler') // refund handler
  async RefundWebHookHandler(@Headers() req_header: any, @Body() req_body: any) {
    try {
      let data = await new PayoutPaymentService().checkAndUpdateIfRefundCompleteOrNot(req_header, req_body);
      return HandleResponse.buildSuccessObj(EC200, EM142);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  //------------------------------------------------------------------------------------

}
