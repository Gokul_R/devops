export interface Contact {
    name: string,
    contact: string,
    email: string,
    type: string,
    reference_id: string,
    order_id?: string | null,
    branch_id?: string | null;
}

export interface FundAccount {
    contact_id: string,
    account_type: string,
    bank_account: {
        name: string,
        ifsc: string,
        account_number: string;
    };
    order_id?: string | null,
    branch_id?: string | null;

}
export interface ValidateBankAccount {
    account_number: string,
    fund_account: {
        id: string;
    },
    amount: number,
    currency: string,
    notes: {
        branch_id: string,
    },
    order_id?: string | null,
    branch_id?: string | null;
}
export interface SendPayoutAmount {
    account_number: string,
    fund_account_id: string,
    amount: number,
    currency: string,
    mode: string,
    purpose: string,  // payout or refund
    queue_if_low_balance: boolean,
    reference_id: string,  // send orderid or user id for reference
    narration: string,
    notes: {
        order_id: string,
        branch_id: string;
    };
    order_id?: string | null,
    branch_id?: string | null;
}
export interface InstantRefund {
    amount?: number,
    receipt: string,
    speed: string,
    notes?: {
        order_id?: string,
        user_id?: string;
    };

}