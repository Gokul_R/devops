import { PartialType } from '@nestjs/swagger';
import { CreateRoleDto } from './create-rbac.dto';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class UpdateRbacDto extends PartialType(CreateRoleDto) {}
export class UpdateRoleDto extends PartialType(CreateRoleDto) {}
