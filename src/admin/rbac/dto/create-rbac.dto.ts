import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { BRANCH_ID } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

export class CreateRbacDto { }
export class CreateUserRoleDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  user_id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  role_id: string;
}
export class CreateRolePermissionDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  role_id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  permission_id: string;
}

export class CreatePermissionDto {
  @ApiProperty()
  // @IsNotEmpty()
  @IsString()
  @IsOptional()
  permission_name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  screen_id: string;

  @ApiProperty({ example: false })
  @IsBoolean()
  @IsOptional()
  read: boolean;

  @ApiProperty({ example: false })
  @IsBoolean()
  @IsOptional()
  write: boolean;

  @ApiProperty({ example: false })
  @IsBoolean()
  @IsOptional()
  edit: boolean;

  @IsString()
  @IsOptional()
  id: string;

  @ApiProperty({ example: false })
  @IsBoolean()
  @IsOptional()
  delete: boolean;
}
export class CreateRoleDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  role_name: string;

  @ApiProperty({example:BRANCH_ID})
  @IsOptional()
  @IsString()
  vendor_id: string;

  @ApiProperty({ type: [CreatePermissionDto] })
  @ValidateNested({ each: true })
  @Type(() => CreatePermissionDto)
  permission: CreatePermissionDto[];

  //   @ApiProperty({ type: CreateRolePermissionDto })
  //   @ValidateNested({ each: true })
  //   @Type(() => CreateRolePermissionDto)
  //   role_permission: CreateRolePermissionDto;

  //   @ApiProperty({ type: CreateUserRoleDto })
  //   @ValidateNested({ each: true })
  //   @Type(() => CreateUserRoleDto)
  //   user_role: CreateUserRoleDto;
}
export class SearchRolesDto extends PaginationDto{
  @ApiProperty({example:BRANCH_ID})
  @IsOptional()
  @IsString()
  vendor_id: string;
    
}
