import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class CreatePageDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  screen_name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  code: string;

  @ApiProperty()
  @IsBoolean()
  read!: boolean;

  @ApiProperty()
  @IsBoolean()
  write!: boolean;

  @ApiProperty()
  @IsBoolean()
  edit!: boolean;

  @ApiProperty()
  @IsBoolean()
  delete!: boolean;
}
