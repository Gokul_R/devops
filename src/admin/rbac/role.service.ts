import { Inject, Injectable } from '@nestjs/common';
import {
  CreatePermissionDto,
  CreateRbacDto,
  CreateRoleDto,
  CreateRolePermissionDto,
  CreateUserRoleDto,
  SearchRolesDto,
} from './dto/create-rbac.dto';
import { UpdateRbacDto, UpdateRoleDto } from './dto/update-rbac.dto';
import { CreatePageDto } from './dto/page-dto';
import Permission from 'src/core/database/models/Permission';
import { DataType, ModelCtor } from 'sequelize-typescript';
import Helpers from 'src/core/utils/helpers';
import {
  PAGES_REPOSITORY,
  PERMISSION_REPOSITORY,
  ROLE_PERMISSION_REPOSITORY,
  ROLE_REPOSITORY,
  USER_ROLE_REPOSITORY,
} from 'src/core/constants';
import Screen from 'src/core/database/models/Screen';
import { BaseService } from '../../base.service';
import Role from 'src/core/database/models/Role';
import RolePermission from 'src/core/database/models/RolePermission';
import UserRole from 'src/core/database/models/UserRole';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { uuid } from 'uuidv4';
import { Op } from 'sequelize';
@Injectable()
export class RoleService extends BaseService<Role> {
  protected model: ModelCtor<Role>;
  role_qry: SequelizeFilter<Role>;
  constructor(
    @Inject(PAGES_REPOSITORY)
    private readonly screensRepo: typeof Screen,
    @Inject(PERMISSION_REPOSITORY)
    private readonly permissionsRepo: typeof Permission,
    @Inject(ROLE_REPOSITORY)
    private readonly rolesRepo: typeof Role,
    @Inject(ROLE_PERMISSION_REPOSITORY)
    private readonly rolesPermissionRepo: typeof RolePermission,
    @Inject(USER_ROLE_REPOSITORY)
    private readonly userRolesRepo: typeof UserRole,
  ) {
    super();
    this.model = this.rolesRepo;
  }
  init() {
    this.role_qry = {
      include: [
        {
          attributes: ['id'],
          model: this.rolesPermissionRepo,
          include: [
            {
              model: this.permissionsRepo,
              include: [
                { attributes: ['screen_name', 'code'], model: this.screensRepo },
              ],
            },
          ],
        },
      ],
    };
  }
  async create(createRbacDto: CreateRbacDto) {
    return 'This action adds a new rbac';
  }
  async createPage(createPageDto: CreatePageDto[]) {
    let page = await Helpers.bulkCreate(this.screensRepo, createPageDto);
    return page;
  }
  async createPermission(createPermissionDto: CreatePermissionDto[]) {
    let permission = await Helpers.bulkCreate(
      this.permissionsRepo,
      createPermissionDto,
    );
    return permission;
  }
  async createRolePermission(createPermissionDto: CreateRolePermissionDto) {
    let permission = await Helpers.create(
      this.rolesPermissionRepo,
      createPermissionDto,
    );
    return permission;
  }

  async createRole(createRoleDto: CreateRoleDto) {
    let role_qry = {
      where: {
        role_name: createRoleDto.role_name,
      },
      defaults: createRoleDto,
    };
    let role: Role = await Helpers.findOrCreate(this.rolesRepo, role_qry);
    let role_id = role[0].id;
    let permission: Permission[] = await this.createPermission(
      createRoleDto.permission,
    );
    permission?.map(async (data) => {
      let role_permission: RolePermission = await this.createRolePermission({
        role_id: role_id,
        permission_id: data.id,
      });
    });

    return role;
  }

  async findAll() {
    this.init();
    this.role_qry.where = { vendor_id: null };
    return super.findAll(this.role_qry);
  }

  async findVendorRoles(vendor_id: string) {
    this.init();
    this.role_qry.where = { vendor_id: vendor_id };
    return super.findAll(this.role_qry);

  }

  async getFilterList(req_data: SearchRolesDto) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        //  where: {
        [Op.or]: [
          { role_name: { [Op.iLike]: `%${searchText}%` } },
        ],
        //   }
      };
    }
    let vendorId: {};
    if (req_data.vendor_id) {
      vendorId = { vendor_id: req_data.vendor_id };
    }
    else {
      vendorId = { vendor_id: null };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await Role.findAndCountAll({
      where: { ...search, ...vendorId },
      include: [
        {
          attributes: ['id'],
          model: this.rolesPermissionRepo,
          include: [
            {
              model: this.permissionsRepo,
              include: [
                {
                  attributes: ['screen_name', 'code'],
                  model: this.screensRepo
                },
              ],
            },
          ],
        },
      ],
      offset: offset,
      limit: limit,
      order: [['created_at', 'DESC']]
      // group: ['Role.id'],
    });
  }

  async findAllPages(is_vendor: boolean) {
    return Helpers.findAll(this.screensRepo, { where: { is_vendor: is_vendor } });
  }

  async findOne(id: string) {
    this.init();
    this.role_qry.where = { id: id };
    return super.findOne(id, this.role_qry);
  }

  async update(id: string, updateRoleDto: UpdateRoleDto): Promise<any> {
    let role = await super.update(id, updateRoleDto);
    // const transaction = await this.permissionsRepo.sequelize.transaction();
    for (const updatedRecord of updateRoleDto.permission) {
      let reqObj: any = updatedRecord;
      let updateRoleQry = {
        where: {
          id: reqObj?.id ? reqObj?.id : uuid()
        },
        defaults: updatedRecord,
      };
      let updateRole = await Helpers.findOrCreate(
        this.permissionsRepo,
        updateRoleQry,
      );
      if (!updateRole[1]) {
        delete updatedRecord?.id;
        let updatePermission = await this.permissionsRepo.update(updatedRecord, {
          where: { id: updateRole[0].dataValues.id }, // Use a unique identifier for the update condition
        });
      } else {
        let role_permission: RolePermission = await this.createRolePermission({
          role_id: id,
          permission_id: updateRole[0].dataValues.id,
        });
      }
    }
    // await transaction.commit();
    // let permission = await super.bulkUpdateRecords(
    //   updateRoleDto.permission,
    //   this.permissionsRepo,
    // );
    // return { role, permission };
    return { role };
  }

  // async remove(id: string) {
  //   return `This action removes a #${id} rbac`;
  // }
}
