import { Module } from '@nestjs/common';
import { RoleService } from './role.service';
import { RoleController } from './role.controller';
import { rbacProviders } from './role.providers';

@Module({
  controllers: [RoleController],
  providers: [RoleService,...rbacProviders]
})
export class RoleModule {}
