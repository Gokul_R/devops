import { commonProvider } from 'src/core/interfaces/common-providers';

export const rbacProviders = [
  // {
  //   provide: PAGES_REPOSITORY,
  //   useValue: Screen,
  // },
  // {
  //   provide: PERMISSION_REPOSITORY,
  //   useValue: Permission,
  // },
  // {
  //   provide: ROLE_REPOSITORY,
  //   useValue: Role,
  // },
  // {
  //   provide: ROLE_PERMISSION_REPOSITORY,
  //   useValue: RolePermission,
  // },
  // {
  //   provide:USER_ROLE_REPOSITORY,
  //   useValue: UserRole,
  // },
  commonProvider.page_repository,
  commonProvider.permission_repository,
  commonProvider.role_repository,
  commonProvider.role_permission_repository,
  commonProvider.user_role_repository,
];
