import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { RoleService } from './role.service';
import { CreateRoleDto, SearchRolesDto } from './dto/create-rbac.dto';
import { UpdateRbacDto, UpdateRoleDto } from './dto/update-rbac.dto';
import { CreatePageDto } from './dto/page-dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116, EM127 } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
@ApiTags('Roles Controller')
@Controller('role')
export class RoleController {
  constructor(private readonly rbacService: RoleService) { }

  @Post()
  async create(@Body() createRoleDto: CreateRoleDto) {
    try {
      let data = await this.rbacService.createRole(createRoleDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('screen')
  async createPage(@Body() createPageDto: CreatePageDto[]) {
    try {
      let data = await this.rbacService.createPage(createPageDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Get('screen')
  async findAllPage(@Query('is_vendor') is_vendor:boolean) {
    try {
      let data = await this.rbacService.findAllPages(is_vendor);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  // @Post('permission')
  // async createPermission(@Body() createPermissionDto: CreatePermissionDto[]) {
  //   try {
  //     let data = await this.rbacService.createPermission(createPermissionDto);
  //     return HandleResponse.buildSuccessObj(EC200, EM104, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Get()
  async findAll() {
    try {
      let data = await this.rbacService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get('vendor/:vendor_id')
  async findVendorRoles(@Param('vendor_id') vendor_id:string) {
    try {
      let data = await this.rbacService.findVendorRoles(vendor_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('search')
  async getFilterList(@Body() getRbacListedto: SearchRolesDto) {
    try {
      let data = await this.rbacService.getFilterList(getRbacListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.rbacService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateRbacDto: UpdateRoleDto) {
    try {
      let data = await this.rbacService.update(id, updateRbacDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let data = await this.rbacService.destroy(id, { where: { id: id } });
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
