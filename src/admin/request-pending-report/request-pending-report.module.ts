import { Module } from '@nestjs/common';
import { RequestPendingReportService } from './request-pending-report.service';
import { RequestPendingReportController } from './request-pending-report.controller';

@Module({
  controllers: [RequestPendingReportController],
  providers: [RequestPendingReportService]
})
export class RequestPendingReportModule {}
