import { Injectable } from '@nestjs/common';
import { CreateRequestPendingReportDto } from './dto/create-request-pending-report.dto';
import OrdersService from 'src/core/database/models/OrderServices';
import { Op } from 'sequelize';
import Order from 'src/core/database/models/Order';
import Branch from 'src/core/database/models/Branch';
import Service from 'src/core/database/models/Services';
import { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import OrderService from 'src/core/database/models/OrderServices';
import MasterCategory from 'src/core/database/models/MasterCategory';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import VendorRole from 'src/core/database/models/VendorRoles';

@Injectable()
export class RequestPendingReportService {
  // protected model: ModelCtor<OrdersService>;
  async getPendingReports(
    createRequestPendingReportDto: CreateRequestPendingReportDto,
  ) {
    let { from_date, to_date, branch_id, vendor_id, searchText, limit, pagNo, vendor_type } =
      createRequestPendingReportDto;

    const pageSize = limit || 10;
    const currentPage = pagNo || 1;
    const offset = (currentPage - 1) * pageSize;

    let searchData;
    if (searchText) {
      searchData = {
        //  where: {
        [Op.or]: [
          { '$order.order_id$': { [Op.iLike]: `%${searchText}%` } },
          { '$order.customer_name$': { [Op.iLike]: `%${searchText}%` } },
          { '$order.order_status$': { [Op.iLike]: `%${searchText}%` } },
          { '$service.service_name$': { [Op.iLike]: `%${searchText}%` } },
          { '$order.branch.branch_name$': { [Op.iLike]: `%${searchText}%` } },
        ],
        // },
      };
    }
    let branchId: any;
    let vendorRoleUser = await VendorRole.findOne({ attributes: ['branch_ids'], where: { id: vendor_id } });
    if(vendorRoleUser){
      branchId = { '$order->branch.id$': vendorRoleUser?.branch_ids };

    }else{
      branchId = branch_id ? { '$order->branch.id$': branch_id } : {};

    }

    let vendorId = vendor_id ? { '$order->branch.vendor_id$': vendor_id } : {};
    let vendorType = vendor_type ? { '$order->branch.vendor_type$': vendor_type } : {};

    let dateStatus;
    if (from_date && to_date) {
      dateStatus = {
        // where: {
        [Op.and]: [
          {
            '$order.created_at$': {
              [Op.gte]: `${from_date} 00:00:00.000`,
              [Op.lte]: `${to_date} 23:59:59.999`,
            },
          },
          {
            '$order.order_status$': APPOINTMENT_STATUS.PENDING,
          },
        ],
        // },
      };
    }
    let data = await OrderService.findAndCountAll({
      attributes: [],

      include: [
        {
          attributes: [
            'id',
            'order_id',
            'customer_name',
            'order_status',
            'original_price',
            'discount',
            'final_price',
            'payment_status',
            'created_at',
            'appointment_start_date_time',
            'booking_slot'
          ],

          //   ...dateStatus,
          model: Order,
          as: 'order',
          paranoid: false,
          //  where: { ...dateStatus, order_status: APPOINTMENT_STATUS.PENDING },

          include: [
            {
              attributes: ['id', 'branch_name', 'vendor_id', 'vendor_type'],
              model: Branch,
              as: 'branch',
              paranoid: false
              //  ...branchId,
            },
          ],
        },
        {
          attributes: ['id', 'service_name'],
          model: Service,
          as: 'service',
          paranoid: false,
          include: [{
            attributes: ['category'],
            model: MasterCategory,
            paranoid: false
          }, {
            attributes: ['service_category_name'],
            model: MasterServiceCategory,
            paranoid: false
          }]
        },
      ],
      paranoid: false,
      where: {
        ...searchData,
        ...branchId,
        ...vendorId,
        ...vendorType,
        ...dateStatus,
        '$order.order_status$': APPOINTMENT_STATUS.PENDING
      },
      offset: offset,
      limit: pageSize,
      order: [['created_at', 'DESC']]
    });
    return data;
  }
}
