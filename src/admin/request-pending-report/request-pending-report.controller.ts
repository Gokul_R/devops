import { Controller, Post, Body } from '@nestjs/common';
import { RequestPendingReportService } from './request-pending-report.service';
import { CreateRequestPendingReportDto } from './dto/create-request-pending-report.dto';
import { EC200, EM106, EM100 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Request Pending Report')
@Controller('request-pending-report')
export class RequestPendingReportController {
  constructor(private readonly requestPendingReportService: RequestPendingReportService) {
    // super(requestPendingReportService);
  }

  @Post()
  async create(@Body() createRequestPendingReportDto: CreateRequestPendingReportDto) {
    try {
      let data = await this.requestPendingReportService.getPendingReports(createRequestPendingReportDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

}
