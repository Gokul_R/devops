import { PartialType } from '@nestjs/swagger';
import { CreateRequestPendingReportDto } from './create-request-pending-report.dto';

export class UpdateRequestPendingReportDto extends PartialType(CreateRequestPendingReportDto) {}
