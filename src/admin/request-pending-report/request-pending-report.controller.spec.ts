import { Test, TestingModule } from '@nestjs/testing';
import { RequestPendingReportController } from './request-pending-report.controller';
import { RequestPendingReportService } from './request-pending-report.service';

describe('RequestPendingReportController', () => {
  let controller: RequestPendingReportController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RequestPendingReportController],
      providers: [RequestPendingReportService],
    }).compile();

    controller = module.get<RequestPendingReportController>(RequestPendingReportController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
