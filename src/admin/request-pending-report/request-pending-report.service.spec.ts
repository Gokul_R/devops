import { Test, TestingModule } from '@nestjs/testing';
import { RequestPendingReportService } from './request-pending-report.service';

describe('RequestPendingReportService', () => {
  let service: RequestPendingReportService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RequestPendingReportService],
    }).compile();

    service = module.get<RequestPendingReportService>(RequestPendingReportService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
