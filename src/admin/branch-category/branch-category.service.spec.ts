import { Test, TestingModule } from '@nestjs/testing';
import { BranchCategoryService } from './branch-category.service';

describe('BranchCategoryService', () => {
  let service: BranchCategoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BranchCategoryService],
    }).compile();

    service = module.get<BranchCategoryService>(BranchCategoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
