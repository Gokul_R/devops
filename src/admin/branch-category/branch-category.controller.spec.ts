import { Test, TestingModule } from '@nestjs/testing';
import { BranchCategoryController } from './branch-category.controller';
import { BranchCategoryService } from './branch-category.service';

describe('BranchCategoryController', () => {
  let controller: BranchCategoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchCategoryController],
      providers: [BranchCategoryService],
    }).compile();

    controller = module.get<BranchCategoryController>(BranchCategoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
