import { Inject, Injectable } from '@nestjs/common';
import { CreateBranchCategoryDto } from './dto/create-branch-category.dto';
import { UpdateBranchCategoryDto } from './dto/update-branch-category.dto';
import { BaseService } from '../../base.service';
import BranchCategoryMapping from 'src/core/database/models/BranchCategoryMapping';
import { ModelCtor } from 'sequelize-typescript';
import { BRANCH_CATEGORY_REPOSITORY } from 'src/core/constants';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import MasterCategory from 'src/core/database/models/MasterCategory';
import { MASTER_CATEGORIES_ATTRIBUTES } from 'src/core/attributes/master_category';
import { Op } from 'sequelize';

@Injectable()
export class BranchCategoryService extends BaseService<BranchCategoryMapping> {
  protected model: ModelCtor<BranchCategoryMapping>;
  constructor(
    @Inject(BRANCH_CATEGORY_REPOSITORY)
    readonly branchCatRepo: typeof BranchCategoryMapping,
  ) {
    super();
    this.model = this.branchCatRepo;
  }
  async findAllByBranch(branch_id: string) {
    let condition;
    if (branch_id) {
      condition = {
        where: {
          [Op.and]: [
            { is_active: true },
            { is_deleted: false },
            { deleted_at: null },
          ],
        },
      };
    }
    let catQry: SequelizeFilter<BranchCategoryMapping> = {
      where: {
        branch_id: branch_id,
        // is_active: true,
        // is_deleted: false,
        // deleted_at: null,
      },
      include: [
        {
          attributes: [MASTER_CATEGORIES_ATTRIBUTES.category],
          model: MasterCategory,
          ...condition,
        },
      ],
    };
    return await super.findAll(catQry);
  }
}
