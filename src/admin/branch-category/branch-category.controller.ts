import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { BranchCategoryService } from './branch-category.service';
import { CreateBranchCategoryDto } from './dto/create-branch-category.dto';
import { UpdateBranchCategoryDto } from './dto/update-branch-category.dto';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../base.controller';
import BranchCategoryMapping from 'src/core/database/models/BranchCategoryMapping';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM106, EC100, EM100, EM116, EM104 } from 'src/core/constants';
@ApiTags('Branch Category')
@Controller('branch-category')
export class BranchCategoryController {
  constructor(private readonly branchCategoryService: BranchCategoryService) {
    // super(branchCategoryService);
  }

  @Post()
  async create(@Body() createBranchCategoryDto: CreateBranchCategoryDto) {
    try {
      let data = await this.branchCategoryService.create(createBranchCategoryDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get('by-branch/:branch_id')
  async findAllByBranch(@Param('branch_id') branch_id: string) {
    try {
      let data: BranchCategoryMapping =
        await this.branchCategoryService.findAllByBranch(branch_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  // @Get()
  // findAll() {
  //   return this.branchCategoryService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.branchCategoryService.findOne(+id);
  // }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateBranchCategoryDto: UpdateBranchCategoryDto) {
    try {
      let data = await this.branchCategoryService.update(id, updateBranchCategoryDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.branchCategoryService.remove(+id);
  // }
}
