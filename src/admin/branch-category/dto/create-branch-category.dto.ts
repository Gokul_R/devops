import { PickType } from "@nestjs/swagger";
import { SharedDto } from "src/core/interfaces/shared.dto";

export class CreateBranchCategoryDto extends PickType(SharedDto,['branch_id','vendor_id','category_id']) {}
