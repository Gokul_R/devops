import { Module } from '@nestjs/common';
import { BranchCategoryService } from './branch-category.service';
import { branchCatProviders } from './branch-category.providers';
import { BranchCategoryController } from './branch-category.controller';

@Module({
  controllers: [BranchCategoryController],
  providers: [BranchCategoryService,...branchCatProviders],
  exports:[BranchCategoryService]
})
export class BranchCategoryModule {}
