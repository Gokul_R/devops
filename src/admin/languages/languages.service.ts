import { Inject, Injectable } from '@nestjs/common';
import { CreateLanguageDto } from './dto/create-language.dto';
import { UpdateLanguageDto } from './dto/update-language.dto';
import Language from 'src/core/database/models/Langauge';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import { LANGUAGE_REPOSITORY } from 'src/core/constants';
import { Op } from 'sequelize';


@Injectable()
export class LanguagesService extends BaseService<Language> {
  protected model: ModelCtor<Language>;
  constructor(@Inject(LANGUAGE_REPOSITORY) private readonly languageRepo: typeof Language) {
    super();
    this.model = this.languageRepo;
  }
  async getFilterList(req_data: any) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { language_name: { [Op.iLike]: `%${searchText}%` } },
          { language_code: { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await this.findAndCountAll({
      where: {
        ...search,
        is_deleted: false
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']]
    });
  }
  // create(createAddLanguageDto: CreateLanguageDto) {
  //   const result = Language.create(createAddLanguageDto);
  //   return result;
  // }

  // findAll() {
  //   const result = Language.findAll();
  //   return result;
  // }

  // findOne(id: string) {
  //   const result = Language.findOne({
  //     where: { id: id },
  //   });
  //   return result;
  // }

  // update(id: string, updateAddLanguageDto: UpdateLanguageDto) {
  //   const result = Language.update(updateAddLanguageDto, {
  //     where: { id: id },
  //   });
  //   return result;
  // }
}
