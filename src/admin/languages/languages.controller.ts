import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { LanguagesService } from './languages.service';
import { CreateLanguageDto } from './dto/create-language.dto';
import { UpdateLanguageDto } from './dto/update-language.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import { BaseController } from '../../base.controller';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@ApiTags('languages')
@Controller('languages')
export class LanguagesController extends BaseController<CreateLanguageDto> {

  constructor(private readonly LanguagesService: LanguagesService) {
    super(LanguagesService);
  }

  @Post()
  async create(@Body() createLanguageDto: CreateLanguageDto) {
    try {
      const data = await this.LanguagesService.create(createLanguageDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('search')
  async getFilterList(@Body() getLanguagesServiceListedto: PaginationDto) {
    try {
      let data = await this.LanguagesService.getFilterList(getLanguagesServiceListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get()
  // async findAll() {
  //   try {
  //     const data = await this.LanguagesService.findAll();
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  // @Get(':id')
  // async findOne(@Param('id') id: string) {
  //   try {
  //     const data = await this.LanguagesService.findOne(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateLanguageDto: UpdateLanguageDto,
  ) {
    try {
      const data = await this.LanguagesService.update(
        id,
        updateLanguageDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
