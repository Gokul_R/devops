import { Module } from '@nestjs/common';
import { LanguagesService } from './languages.service';
import { LanguagesController } from './languages.controller';
import { languageProviders } from './language.providers';

@Module({
  controllers: [LanguagesController],
  providers: [LanguagesService,...languageProviders]
})
export class LanguagesModule {}
