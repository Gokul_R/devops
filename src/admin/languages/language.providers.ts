import { commonProvider } from 'src/core/interfaces/common-providers';

export const languageProviders = [
  // {
  //   provide: LANGUAGE_REPOSITORY,
  //   useValue: Language,
  // },
  commonProvider.language_repository,
];
