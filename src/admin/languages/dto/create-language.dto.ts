import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsNumber,
  IsNotEmpty,
  IsBoolean,
  IsOptional,
} from 'class-validator';

export class CreateLanguageDto {
  @IsString()
  @IsOptional()
  id: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  language_name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  flag: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  language_code: string;

  @ApiProperty({ default: false })
  @IsBoolean()
  make_default: boolean;

  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;
}
