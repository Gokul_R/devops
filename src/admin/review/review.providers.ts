import { commonProvider } from 'src/core/interfaces/common-providers';

export const reviewProviders = [
  // {
  //   provide: COMMENT_REPOSITORY,
  //   useValue: Comments,
  // },
  commonProvider.review_repository,
  commonProvider.review_reply_repository
];
