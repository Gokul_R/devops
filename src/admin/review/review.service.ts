import { Inject, Injectable } from '@nestjs/common';
import { ModelCtor } from 'sequelize-typescript';

import Review from 'src/core/database/models/Reviews';
import { REVIEWS_REPLY_REPOSITORY, REVIEW_REPOSITORY } from 'src/core/constants';
import { BaseService } from 'src/base.service';
import AdminUser from 'src/core/database/models/AdminUser';
import User from 'src/core/database/models/User';
import Branch from 'src/core/database/models/Branch';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { Op, col, fn } from 'sequelize';
import ReviewReply from 'src/core/database/models/ReviewReply';
import { UpdateReplayReviewDto, UpdateReviewDto } from './dto/update-review.dto';

@Injectable()
export class ReviewService extends BaseService<Review> {
  protected model: ModelCtor<Review>;
  //user_qry: SequelizeFilter<User>;
  //branch_qry: SequelizeFilter<Branch>;
  constructor(
    @Inject(REVIEW_REPOSITORY) private readonly reviewRepo: typeof Review,
    @Inject(REVIEWS_REPLY_REPOSITORY) private readonly reviewReplyrepo: typeof ReviewReply,
  ) {
    super();
    this.model = this.reviewRepo;
  }
  // create(createCommentDto: CreateCommentDto) {
  //   return 'This action adds a new comment';
  // }

  async findAll() {
    // let data = await super.findAll();

    //  let { user_id, branch_id } = data;
    let data = await super.findAll({
      include: [
        {
          attributes: ['user_name'],
          model: User,
        },
        {
          attributes: ['branch_name'],
          model: Branch,
        },
      ],
      where: {
        is_deleted: false,
      },
    });
    return data;
  }

  async findOne(id: string) {
    let data = await super.findAll({
      where: { id: id },
      include: [
        {
          attributes: ['user_name'],
          model: User,
        },
        {
          attributes: ['branch_name'],
          model: Branch,
        },
      ],
    });
    return data;

    // update(id: number, updateCommentDto: UpdateCommentDto) {
    //   return `This action updates a #${id} comment`;
    // }

    // remove(id: number) {
    //   return `This action removes a #${id} comment`;
    // }
  }
  async getFilterList(req_data: any) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { '$user.user_name$': { [Op.iLike]: `%${searchText}%` } },
          { '$branch.branch_name$': { [Op.iLike]: `%${searchText}%` } },
          { review: { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }

    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let branch_id = req_data.branch_id ? { branch_id: req_data.branch_id } : {};
    let query: any = {
      where: {
        ...branch_id,
        is_deleted: false,
        ...search,
      },
      include: [
        {
          attributes: ['user_name', 'profile_url'],
          model: User,
          as: 'user', // Specify the alias for the User model if it's defined in the association
          required: true,
          paranoid: false
        },
        {
          attributes: ['branch_name', 'branch_image'],
          model: Branch,
          as: 'branch', // Specify the alias for the Branch model if it's defined in the association
          required: true,
          paranoid: false
        },
      ],
    };
    let count = await Review.count(query);
    query.include.push({
      attributes: ['id', 'reply_review_id', 'reply_review', 'user_type', 'reply_user_id', 'created_at'],
      model: ReviewReply,
      as: 'reply',
      // required: true,
    });
    let data = await Review.findAll({
      ...query,
      offset: offset,
      limit: limit,
      order: [['created_at', 'DESC']],
    });
    return { count: count ? count : 0, rows: data ? data : [] };
  }

  async replayReviewUpdate(id: string, updateReplayReviewDto: UpdateReplayReviewDto) {
    let data = { reply_review: updateReplayReviewDto.review };
    let replayUpdate = await this.reviewReplyrepo.update(data, { where: { id: id } });
    return replayUpdate;
  }

  async replayReviewDelete(id: string) {
    let replayDelete = await this.reviewReplyrepo.destroy({ force: true, where: { id: id } });
    return replayDelete;
  }
}
