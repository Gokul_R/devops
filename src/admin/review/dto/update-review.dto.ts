import { PartialType, PickType } from '@nestjs/swagger';
import { CreateReviewDto } from './create-review.dto';

export class UpdateReviewDto extends PartialType(CreateReviewDto) {}
export class UpdateReplayReviewDto extends PickType (CreateReviewDto,['review']){}
