import {
  IsArray,
  IsBoolean,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateIf,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { BRANCH_ID } from 'src/core/constants';
import { UserTypes } from 'src/core/utils/enum';
export class CreateReviewDto {
  @IsString()
  @IsOptional()
  id?: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  // @ApiProperty({ example: 'manoj' })
  // @IsString()
  // name: string;

  // @ApiProperty({ example: 'fomc 2435otmo5bom' })
  // @IsString()
  // branch_id: string;

  // @ApiProperty({ example: 'fbogfksbogbpob' })
  // @IsString()
  // comments: string;

  // @ApiProperty({ example: '3' })
  // @IsInt()
  // rating: number;

  @ApiProperty({ example: '3' })
  @IsInt()
  rating?: number;

  @ApiProperty({ example: 'some message' })
  @IsString()
  review?: string;

  @ApiProperty({ example: ['image.jpg', 'image.png'] })
  @IsArray()
  review_images?: string[];

  @ApiProperty({ example: BRANCH_ID })
  user_id?: string;

  @ApiProperty({ example: BRANCH_ID })
  branch_id?: string;

  @ApiProperty({ example: BRANCH_ID })
  @IsOptional()
  reply_review_id?: string;

  @ApiProperty({ example: 'User Name' })
  @IsOptional()
  user_name?: string;

  @ApiProperty({ example: `true 'or' false` })
  @ValidateIf((object) => object.reply_review_id !== null)
  @IsOptional()
  @IsBoolean()
  is_reply?: boolean;

  @ApiProperty({ example: `${UserTypes.ADMIN} or ${UserTypes.BRANCH} or ${UserTypes.VENDOR} or ${UserTypes.USER}` })
  @IsOptional()
  @IsEnum(UserTypes)
  user_type?: string;

  //  @ApiProperty({example:"fdvvcfvijfvf"})
  // branch_id?:string

  // @ApiProperty({ default: false })
  @IsBoolean()
  @IsOptional()
  is_deleted?: boolean;
}

export class GetBranchReviews extends PaginationDto {
  @IsString()
  @IsOptional()
  @ApiProperty({ example: BRANCH_ID })
  branch_id?: string;
}