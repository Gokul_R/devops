import { Controller, Post, Body, Patch, Param, Get, Delete } from '@nestjs/common';
import { ReviewService as ReviewService } from './review.service';
import { CreateReviewDto, GetBranchReviews } from './dto/create-review.dto';
import { UpdateReplayReviewDto, UpdateReviewDto } from './dto/update-review.dto';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116, EM127 } from 'src/core/constants';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from 'src/base.controller';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import Review from 'src/core/database/models/Reviews';
import User from 'src/core/database/models/User';
import Branch from 'src/core/database/models/Branch';
import ReviewReply from 'src/core/database/models/ReviewReply';

@ApiTags('Review')
@Controller('review')
export class ReviewController extends BaseController<CreateReviewDto> {
  constructor(private readonly reviewService: ReviewService) {
    super(reviewService);
  }

  @Post()
  async create(@Body() createReviewDto: CreateReviewDto) {
    try {
      if (createReviewDto?.reply_review_id && createReviewDto?.is_reply) {
        let reviewReply = {
          reply_review_id: createReviewDto.reply_review_id,
          reply_review: createReviewDto.review,
          user_type: createReviewDto.user_type,
          reply_user_id: createReviewDto.user_id,
          user_name: createReviewDto.user_name,
        };
        let data = await ReviewReply.create(reviewReply);
        return HandleResponse.buildSuccessObj(EC200, EM104, data);
      } else {
        let data = await this.reviewService.create(createReviewDto);
        return HandleResponse.buildSuccessObj(EC200, EM104, data);
      }
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get()
  async findAll() {
    try {
      let data = await this.reviewService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }

  }

  @Post('search')
  async getFilterList(@Body() getReviewListedto: GetBranchReviews) {
    try {
      let data = await this.reviewService.getFilterList(getReviewListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  // @Post('by-branch')
  // async getBranchReviewList(@Body() getBranchReviews: GetBranchReviews) {
  //   try {
  //     let data = await Review.findAll({
  //       include: [
  //         {
  //           attributes: ['user_name'],
  //           model: User,
  //           as: 'user', // Specify the alias for the User model if it's defined in the association
  //         },
  //       ], where: { branch_id: getBranchReviews.branch_id }
  //     });
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.reviewService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }

  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateReviewDto: UpdateReviewDto) {
    try {
      let data = await this.reviewService.update(id, updateReviewDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch('replay-update/:id')
  async replayReviewUpdate(@Param('id') id: string, @Body() updateReplayReviewDto: UpdateReplayReviewDto) {
    try {
      let data = await this.reviewService.replayReviewUpdate(id, updateReplayReviewDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete('replay-delete/:id')
  async replayReviewDelete(@Param('id') id: string) {
    try {
      let data = await this.reviewService.replayReviewDelete(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, null);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.commentsService.remove(+id);
  // }
}
