import { Module } from '@nestjs/common';
import { ReviewService as ReviewService } from './review.service';
import { ReviewController } from './review.controller';
import { reviewProviders } from './review.providers';

@Module({
  controllers: [ReviewController],
  providers: [ReviewService,...reviewProviders]
})
export class ReviewModule {}
