import { Controller, Get, Post, Body, Patch, Param, Delete, ParseUUIDPipe } from '@nestjs/common';
import { LogoService } from './logo.service';
import { CreateLogoDto } from './dto/create-logo.dto';
import { UpdateLogoDto } from './dto/update-logo.dto';
import { BaseController } from '../../base.controller';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import { ApiTags } from '@nestjs/swagger';
import Logo from 'src/core/database/models/Logo';

@ApiTags('logo')
@Controller('logo')
export class LogoController extends BaseController<CreateLogoDto> {
  constructor(private readonly logoService: LogoService) {
    super(logoService);
  }

  @Post()
  async create(@Body() createLogoDto: CreateLogoDto) {
    try {
      let data = await this.logoService.create(createLogoDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
    //  return this.logoService.create(createLogoDto);
  }

  @Get()
  async findAll() {
    return HandleResponse.buildSuccessObj(EC200, EM106, await Logo.findOne({ attributes: ['header_logo', 'footer_logo', 'favicon', 'id'], order: [['updated_at', 'DESC']], raw: true, where: { is_active: true, is_deleted: false } }));
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.logoService.findOne(+id);
  // }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateLogoDto: UpdateLogoDto) {
    try {
      let data = await this.logoService.update(id, updateLogoDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
    //  return this.logoService.update(+id, updateLogoDto);
  }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.logoService.remove(+id);
  // }
}
