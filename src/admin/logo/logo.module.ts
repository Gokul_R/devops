import { Module } from '@nestjs/common';
import { LogoService } from './logo.service';
import { LogoController } from './logo.controller';
import { logoProviders } from './logo.providers';

@Module({
  controllers: [LogoController],
  providers: [LogoService,...logoProviders]
})
export class LogoModule {}
