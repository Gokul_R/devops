import { IsOptional, IsString, IsBoolean, Matches } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';


export class CreateLogoDto {
  @IsString()
  @IsOptional()
  id: string;
  
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: 'https://172.45.34/headerlogo' })
  @IsString()
  @IsOptional()
  @Matches(/.*\S.*/, { message: 'Header logo should contain at least one non-whitespace character' })
  header_logo: string;

  @ApiProperty({ example: 'https://172.45.34/headerlogo' })
  @IsString()
  @IsOptional()
  @Matches(/.*\S.*/, { message: 'footer logo should contain at least one non-whitespace character' })
  footer_logo: string;

  @ApiProperty({ example: 'https://172.45.34/headerlogo' })
  @IsString()
  @IsOptional()
  @Matches(/.*\S.*/, { message: 'favicon logo should contain at least one non-whitespace character' })
  favicon: string;

  //@ApiProperty({default:false})
  @IsBoolean()
  @IsOptional()
  is_deleted: string;


}