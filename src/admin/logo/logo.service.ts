import { Inject, Injectable } from '@nestjs/common';
import { CreateLogoDto } from './dto/create-logo.dto';
import { UpdateLogoDto } from './dto/update-logo.dto';
import { BaseService } from '../../base.service';
import Logo from 'src/core/database/models/Logo';
import { ModelCtor } from 'sequelize-typescript';
import { LOGO_REPOSITORY } from 'src/core/constants';

@Injectable()
export class LogoService extends BaseService<Logo> {
  protected model: ModelCtor<Logo>;
  constructor(@Inject(LOGO_REPOSITORY) private readonly logoServiceRepo:typeof Logo){
    super();
   this.model= this.logoServiceRepo
  }
  // create(createLogoDto: CreateLogoDto) {
  //   return 'This action adds a new logo';
  // }

  // findAll() {
  //   return `This action returns all logo`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} logo`;
  // }

  // update(id: number, updateLogoDto: UpdateLogoDto) {
  //   return `This action updates a #${id} logo`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} logo`;
  // }
}
