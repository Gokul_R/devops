import { Inject, Injectable } from '@nestjs/common';
//import { CreateNotificationDto } from './dto/create-notification.dto';

import { ModelCtor } from 'sequelize-typescript';
import { BaseService } from 'src/base.service';
import {  NOTIFICATION_CONFIG_REPOSITORY } from 'src/core/constants';
import NotificationConfig from 'src/core/database/models/NotificationConfig';

@Injectable()
export class NotificationConfigService extends BaseService<NotificationConfig> {
  protected model: ModelCtor<NotificationConfig>;
  constructor(
    @Inject(NOTIFICATION_CONFIG_REPOSITORY)
    private readonly NotificationConfigRepo: typeof NotificationConfig,
  ) {
    super();
    this.model = this.NotificationConfigRepo;
  }
  // async create(body: CreatePushNotificationDto) {
  //   const result = await PushNotification.create(body);
  //   return result;
  // }

  // findAll() {
  //   const result = PushNotification.findAll();
  //   return result;
  // }

  // findOne(id: string) {
  //   const result = PushNotification.findAll({
  //     where: { id: id },
  //   });
  //   return result;
  // }

  // update(id: string, body: CreatePushNotificationDto) {
  //   const result = PushNotification.update(body, {
  //     where: { id: id },
  //   });
  //   return result;
  // }
}
