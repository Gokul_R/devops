import { commonProvider } from 'src/core/interfaces/common-providers';

export const NotificationConfigProviders = [
  // {
  //     provide:NOTIFICATION_REPOSITORY,
  //     useValue:Notification
  // },
  commonProvider.notification_config_repository,
];
