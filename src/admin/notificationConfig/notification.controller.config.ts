import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { NotificationConfigService } from './notification.service.config';
//import { CreateNotificationDto } from './dto/create-notification.dto';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import { ApiTags } from '@nestjs/swagger';
import { CreateNotificationConfigDto } from './dto/create-notification_config.dto';
import { BaseController } from 'src/base.controller';
import NotificationConfig from 'src/core/database/models/NotificationConfig';
import { logger } from 'src/core/utils/logger';


@ApiTags('Notification Config')
@Controller('notification-config')
export class NotificationConfigController extends BaseController<CreateNotificationConfigDto> {
  constructor(
    private readonly NotificationService: NotificationConfigService,
  ) {
    super(NotificationService);

  }

  @Post()
  async create(@Body() createNotificationDto: CreateNotificationConfigDto) {
    try {
      logger.info(`Create_NotificationConfig_Entry: ` + JSON.stringify(createNotificationDto));
      const findExitingData = await NotificationConfig.findAll();
      let data: any;
      if (findExitingData && findExitingData.length > 0) {
        data = await NotificationConfig.update({ server_key: createNotificationDto.server_key }, { returning: true, where: {} });
      } else {
        data = await this.NotificationService.create(
          createNotificationDto,
        );
      }
      logger.info(`Create_Notification_Config_Exit: ` + JSON.stringify(data));
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get()
  // async findAll() {
  //   try {
  //     const data = await this.pushNotificationService.findAll();
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  // @Get(':id')
  // async findOne(@Param('id') id: string) {
  //   try {
  //     const data = await this.pushNotificationService.findOne(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateNotificationDto: CreateNotificationConfigDto,
  ) {
    try {
      logger.info(`Update_NotificationConfig_Entry_Id: ${JSON.stringify(id)}, Data: ${JSON.stringify(updateNotificationDto)}`);
      const data = await this.NotificationService.update(
        id,
        updateNotificationDto,
      );
      logger.info(`Update_NotificationConfig_Exit_Data: ${JSON.stringify(data)}`);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
