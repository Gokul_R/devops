import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsInt, IsBoolean, IsOptional } from 'class-validator';
import { NotEmpty } from 'sequelize-typescript';
export class CreateNotificationConfigDto {
    @IsString()
    @IsOptional()
    id: string;
    @IsBoolean()
    @IsOptional()
    is_active?: boolean;

    // @ApiProperty()
    // @IsString()
    // app_id:string

    // @ApiProperty()
    // @IsString()
    // rest_api_key:string

    // @ApiProperty()
    // @IsString()
    // user_auth_key:string
    @ApiProperty()
    @IsString()
    server_key: string;

    // @ApiProperty()
    // @IsString()
    // rest_api_key:string

    // @ApiProperty()
    // @IsString()
    // user_auth_key:string

    // @ApiProperty()
    // @IsInt()
    // project_number:number

    // @ApiProperty({default:false})
    @IsBoolean()
    @IsOptional()
    is_deleted: boolean;

    // @ApiProperty({default:false})
    // @IsBoolean()
    // is_deleted: boolean;

}
