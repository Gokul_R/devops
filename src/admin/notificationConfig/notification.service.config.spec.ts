import { Test, TestingModule } from '@nestjs/testing';
import { NotificationConfigService } from './notification.service.config';

describe('PushNotificationService', () => {
  let service: NotificationConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NotificationConfigService],
    }).compile();

    service = module.get<NotificationConfigService>(NotificationConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
