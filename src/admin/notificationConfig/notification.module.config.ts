import { Module } from '@nestjs/common';
import { NotificationConfigService } from './notification.service.config';
import { NotificationConfigController } from './notification.controller.config';
import { NotificationConfigProviders } from './notification.providers.config';

@Module({
  controllers: [NotificationConfigController],
  providers: [NotificationConfigService,...NotificationConfigProviders]
})
export class NotificationConfigModule {}
