import { Test, TestingModule } from '@nestjs/testing';

import { NotificationConfigService } from './notification.service.config';
import { NotificationController } from 'src/modules/notification/notification.controller';

describe('PushNotificationController', () => {
  let controller: NotificationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NotificationController],
      providers: [NotificationConfigService],
    }).compile();

    controller = module.get<NotificationController>(NotificationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
