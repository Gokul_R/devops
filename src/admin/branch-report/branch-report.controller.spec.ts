import { Test, TestingModule } from '@nestjs/testing';
import { BranchReportController } from './branch-report.controller';
import { BranchReportService } from './branch-report.service';

describe('BranchReportController', () => {
  let controller: BranchReportController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchReportController],
      providers: [BranchReportService],
    }).compile();

    controller = module.get<BranchReportController>(BranchReportController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
