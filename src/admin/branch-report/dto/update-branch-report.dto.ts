import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateBranchReportDto } from './create-branch-report.dto';
import { BRANCH_ID } from 'src/core/constants';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateBranchReportDto extends PartialType(CreateBranchReportDto) { }
export class GetAppointmentDto {

    @ApiProperty({ example: '2023-08-01' })
    @IsString()
    @IsNotEmpty()
    startDate: string;

    @ApiProperty({ example: '2023-08-31' })
    @IsString()
    @IsNotEmpty()
    endDate: string;

    @ApiProperty({ example: BRANCH_ID })
    @IsString()
    @IsNotEmpty()
    branch_id: string;
}