import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { BranchReportService } from './branch-report.service';
import { CreateBranchReportDto } from './dto/create-branch-report.dto';
import { GetAppointmentDto, UpdateBranchReportDto } from './dto/update-branch-report.dto';
import { BaseController } from 'src/base.controller';
import { EC200, EM106, EM100 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Branch Report')
@Controller('branch-report')
export class BranchReportController {
  constructor(private readonly branchReportService: BranchReportService) {
    //  super(branchReportService);
  }

  @Post()
  async create(@Body() createBranchReportDto: CreateBranchReportDto) {
    try {
      let data = await this.branchReportService.getBranchDetails(createBranchReportDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('getAppointments')
  async getAppointments(@Body() getAppointmentDto: GetAppointmentDto) {
    try {
      let data = await this.branchReportService.getAppointments(getAppointmentDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

}
