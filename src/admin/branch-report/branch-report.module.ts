import { Module } from '@nestjs/common';
import { BranchReportService } from './branch-report.service';
import { BranchReportController } from './branch-report.controller';

@Module({
  controllers: [BranchReportController],
  providers: [BranchReportService]
})
export class BranchReportModule {}
