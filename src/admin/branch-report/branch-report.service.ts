import { Injectable } from '@nestjs/common';
import { CreateBranchReportDto } from './dto/create-branch-report.dto';
import { GetAppointmentDto } from './dto/update-branch-report.dto';
import Branch from 'src/core/database/models/Branch';
import { Op, Sequelize } from 'sequelize';
import Vendor from 'src/core/database/models/Vendor';
import Appointment, {
  APPOINTMENT_STATUS,
} from 'src/core/database/models/Appointment';
import User from 'src/core/database/models/User';
import { APPOINTMENT_ATTRIBUTES } from 'src/core/attributes/appointment';
import Order from 'src/core/database/models/Order';

@Injectable()
export class BranchReportService {
  // protected model: ModelCtor<Branch>;
  async getBranchDetails(createBranchReportDto: CreateBranchReportDto) {
    let { from_date, to_date, vendor_id, branch_id, limit, status, pagNo, searchText, vendor_type } =
      createBranchReportDto;

    const pageSize = limit || 10;
    const currentPage = pagNo || 1;
    const offset = (currentPage - 1) * pageSize;

    let searchData;
    if (searchText) {
      searchData = {
        [Op.or]: [
          { $branch_name$: { [Op.iLike]: `%${searchText}%` } },
          { '$branch->vendor.vendor_name$': { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }

    //let vendorId = vendor_id ? { where: { id: vendor_id } } : {};
    let branchId =
      branch_id && vendor_id
        ? { '$branch.id$': branch_id, '$branch->vendor.id$': vendor_id }
        : branch_id && !vendor_id
          ? { '$branch.id$': branch_id }
          : !branch_id && vendor_id
            ? { '$branch->vendor.id$': vendor_id }
            : {};

    let dateStatus;
    if (from_date && to_date) {
      dateStatus = {
        [Op.and]: [
          {
            '$branch.created_at$': {
              [Op.gte]: `${from_date} 00:00:00.000`,
              [Op.lte]: `${to_date} 23:59:59.999`,
            },
          },
        ],
      };
    }
    let vendorType: any = vendor_type ? { where: { vendor_type: vendor_type } } : {};
    let orderStatus = status ? { order_status: status } : {};
    let data: any = await Order.findAndCountAll({
      attributes: [
        [Sequelize.fn('COUNT', Sequelize.col('Order.id')), 'service_count'],
      ],

      include: [
        {
          attributes: ['id', 'branch_name', 'vendor_type'],
          model: Branch,
          as: 'branch',
          ...vendorType,
          paranoid: false,

          include: [
            {
              attributes: ['id', 'vendor_name'],
              model: Vendor,
              as: 'vendor',
              paranoid: false
            },
          ],
        },
      ],
      paranoid: false,
      where: {
        ...orderStatus,
        ...searchData,
        ...dateStatus,
        ...branchId,
      },
      group: [
        'Order.order_status',
        'branch.branch_name',
        'branch.id',
        'branch->vendor.id',
      ],
      offset: offset,
      limit: pageSize,
    });
    data.count = data?.count.length;

    return data;
  }
  async getAppointments(req_data: GetAppointmentDto) {

    const appointments = await Appointment.findAll({
      // attributes: [
      //   'Appointment.id', // Specify the table alias here
      //   'Appointment.user_id',
      //   'Appointment.appointment_start_date_time',
      //   [Sequelize.fn('DATE', Sequelize.col('Appointment.appointment_start_date_time')), 'appointment_start_date'],
      //   [Sequelize.fn('COUNT', Sequelize.literal('*')), 'appointment_count'],
      //   [Sequelize.literal(`(SELECT "user_name" FROM "users" WHERE "users"."id" = "Appointment"."user_id")`), 'user_name'],
      //   [Sequelize.literal(`(SELECT "profile_url" FROM "users" WHERE "users"."id" = "Appointment"."user_id")`), 'profile_url'],
      // ],
      attributes: [
        APPOINTMENT_ATTRIBUTES.ID,
        APPOINTMENT_ATTRIBUTES.USER_ID,
        APPOINTMENT_ATTRIBUTES.APPOINTMENT_START_DATE_TIME,
        APPOINTMENT_ATTRIBUTES.ORDER_ID,
        APPOINTMENT_ATTRIBUTES.BOOKING_SLOT,
        [
          Sequelize.fn('DATE', Sequelize.col('appointment_start_date_time')),
          'appointment_start_date',
        ],
        [Sequelize.fn('COUNT', Sequelize.col('*')), 'appointment_count'],
        [
          Sequelize.literal(
            '(SELECT "user_name" FROM "users" WHERE "users"."id" = "user_id")',
          ),
          'user_name',
        ],
        [
          Sequelize.literal(
            '(SELECT "profile_url" FROM "users" WHERE "users"."id" = "user_id")',
          ),
          'profile_url',
        ],
      ],
      include: [
        {
          model: User,
          as: 'user',
          attributes: [], // You can specify any required attributes for the user model here
          paranoid: false
          // where: {
          //   deleted_at: null,
          // },
        },
      ],
      paranoid: false,
      where: {
        // deleted_at: null,
        branch_id: req_data.branch_id,
        appointment_status: APPOINTMENT_STATUS.CONFIRMED,
        is_cancelled: false,
        is_active: true,
        appointment_start_date_time: {
          [Op.gte]: `${req_data.startDate} 00:00:00.000`,
          [Op.lte]: `${req_data.endDate} 23:59:59.999`,
        },
      },
      group: [
        Sequelize.col('Appointment.id'), // Specify the table alias here
        'Appointment.user_id',
        'Appointment.appointment_start_date_time',
      ],
    });
    return appointments;

    // const appointments = await Appointment.findAll({
    //   attributes: [
    //     APPOINTMENT_ATTRIBUTES.ID,
    //     APPOINTMENT_ATTRIBUTES.USER_ID,
    //     APPOINTMENT_ATTRIBUTES.APPOINTMENT_START_DATE_TIME,
    //     [
    //       Sequelize.fn('DATE', Sequelize.col('appointment_start_date_time')),
    //       'appointment_start_date',
    //     ],
    //     [Sequelize.fn('COUNT', Sequelize.col('*')), 'appointment_count'],
    //     [
    //       Sequelize.literal(
    //         '(SELECT "user_name" FROM "users" WHERE "users"."id" = "user_id")',
    //       ),
    //       'user_name',
    //     ],
    //     [
    //       Sequelize.literal(
    //         '(SELECT "profile_url" FROM "users" WHERE "users"."id" = "user_id")',
    //       ),
    //       'profile_url',
    //     ],
    //   ],
    //   where: {
    //     branch_id: req_data.branch_id,
    //     appointment_status: APPOINTMENT_STATUS.CONFIRMED,
    //     is_cancelled: false,
    //     is_active: true,
    //     appointment_start_date_time: {
    //       [Op.gte]: req_data.startOfMonth || startOfMonth,
    //       [Op.lte]: req_data.endOfMonth || endOfMonth,
    //     },
    //   },
    //   group: [
    //     Sequelize.fn('DATE', Sequelize.col('appointment_start_date_time')),
    //     'user_id',
    //     'Appointment.id',
    //   ],
    //   include: [
    //     {
    //       attributes: [],
    //       model: User,
    //       required: false,
    //     },
    //   ],
    //   raw: true,
    // });
    // return appointments;
    //--------------------------
    // const groupedAppointments = {};
    // appointments.forEach((appointment: any) => {
    //   const {
    //     appointment_start_date,
    //     appointment_count,
    //     user_name,
    //     profile_url,
    //     appointment_start_date_time,
    //     id,
    //   } = appointment;
    //   if (!groupedAppointments[appointment_start_date]) {
    //     groupedAppointments[appointment_start_date] = [];
    //   }
    //   groupedAppointments[appointment_start_date].push({
    //     appointment_start_date_time,
    //     id,
    //     appointment_count,
    //     user_name,
    //     profile_url,
    //   });
    // });
    // const finalData = Object.keys(groupedAppointments).map((date) => {
    //   return {
    //     appointment_start_date: date,
    //     users: groupedAppointments[date],
    //   };
    // });
    // return finalData;
  }
}
