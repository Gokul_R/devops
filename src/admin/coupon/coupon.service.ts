import { Inject, Injectable } from '@nestjs/common';
import { CreateCouponDto } from './dto/create-coupon.dto';
import { GetCouponDto, UpdateCouponDto } from './dto/update-coupon.dto';
import Helpers from 'src/core/utils/helpers';
import {
  COUPON_BRANCH_REPOSITORY,
  COUPON_CATEGORY_MAPPING_REPOSITORY,
  COUPON_REPOSITORY,
  COUPON_SERVICE_CATEGORY_MAPPING_REPOSITORY,
  COUPON_SERVICE_REPOSITORY,
  Provided_By,
} from 'src/core/constants';
import Coupon from 'src/core/database/models/Coupons';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import Branch from 'src/core/database/models/Branch';
import Service from 'src/core/database/models/Services';
import CouponBranchMapping from 'src/core/database/models/CouponBranchMapping';
import CouponServiceMapping from 'src/core/database/models/CouponServiceMapping';
import { Op } from 'sequelize';
import CouponCategoryMapping from 'src/core/database/models/CouponCategoryMapping';
import CouponServiceCategoryMapping from 'src/core/database/models/CouponServiceCategoryMapping';
import MasterCategory from 'src/core/database/models/MasterCategory';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import MarketingGroup from 'src/core/database/models/MarketingGroup';
import ReferalManage from 'src/core/database/models/ReferalManage';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { COUPON_ATTRIBUTES } from 'src/core/attributes/coupon';
import { ORDER_ATTRIBUTES } from 'src/core/attributes/order';
import { logger } from 'src/core/utils/logger';
import { CouponUsedDto } from './dto/coupon-used-count.dto';
import AppliedCoupon from 'src/core/database/models/AppliedCoupons';
import { APPLIED_COUPON_ATTRIBUTES } from 'src/core/attributes/applied_coupon';
import User from 'src/core/database/models/User';
import { SendPushNotification } from 'src/core/utils/sendPushNotification';
import VendorRole from 'src/core/database/models/VendorRoles';

@Injectable()
export class CouponService extends BaseService<Coupon> {
  protected model: ModelCtor<Coupon>;
  constructor(
    @Inject(COUPON_REPOSITORY)
    private readonly couponRepo: typeof Coupon,
    @Inject(COUPON_BRANCH_REPOSITORY)
    private readonly couponBranchRepo: typeof CouponBranchMapping,
    @Inject(COUPON_SERVICE_REPOSITORY)
    private readonly couponServiceRepo: typeof CouponServiceMapping,
    @Inject(COUPON_CATEGORY_MAPPING_REPOSITORY)
    private readonly couponCategoryMapping: typeof CouponCategoryMapping,
    @Inject(COUPON_SERVICE_CATEGORY_MAPPING_REPOSITORY)
    private readonly couponServiceCategoryMapping: typeof CouponServiceCategoryMapping,
  ) {
    super();
    this.model = this.couponRepo;
  }

  // async findByShop(shop_id: string) {
  //   let coupon_qry: SequelizeFilter<Coupon> = {
  //     attributes: [
  //       COUPON_ATTRIBUTES.ID,
  //       COUPON_ATTRIBUTES.COUPON_CODE,
  //       COUPON_ATTRIBUTES.DESCRIPTION,
  //       COUPON_ATTRIBUTES.DISCOUNT_TYPE,
  //       COUPON_ATTRIBUTES.DISCOUNT_VALUE,
  //       COUPON_ATTRIBUTES.START_DATE,
  //       COUPON_ATTRIBUTES.END_DATE,
  //       COUPON_ATTRIBUTES.MINIMUM_SPEND,
  //     ],
  //     where: {
  //       branch_id: shop_id,
  //     },
  //   };

  //   let coupons: any = await Helpers.collectionFind(Coupon, coupon_qry);

  //   return coupons;
  // }
  async create(createCouponDto: CreateCouponDto): Promise<any> {
    let { branch_ids, service_ids } = createCouponDto;
    let result: any;
    let coupon: Coupon = await super.create(createCouponDto);
    // if (branch_ids?.length > 0) {
    await this.mappings(coupon.id, createCouponDto);
    this.sendNotifyToUser(coupon.id, createCouponDto?.image, createCouponDto?.coupon_name, createCouponDto?.coupon_code, createCouponDto?.marketing_ids);
    // }
    //  else {
    //   createCouponDto.branch_ids = null;
    //   result = await super.create(createCouponDto);
    // }
    return coupon;
  }
  async sendNotifyToUser(couponId: string, image: string, couponName: string, couponCode: string, margetingId: any) {
    let usersId;
    if (margetingId) {
      let getUsers = await MarketingGroup.findOne({ attributes: ['users_mobile', 'is_referel', 'referal_code', 'referal_id'], where: { id: margetingId } });
      if (getUsers && getUsers?.is_referel) {
        usersId = await User.findAll({ attributes: ['id'], where: { invited_ref_code: getUsers?.referal_code } });
      } else if (getUsers && !getUsers?.is_referel) {
        usersId = await User.findAll({ attributes: ['id'], where: { mobile_no: { [Op.in]: getUsers?.users_mobile } } });
      }
    }
    if (usersId && usersId?.length > 0) usersId = usersId.map(userId => userId.id);
    let id: any = usersId ? { user_id: usersId } : '';
    let reqObj = {
      ...id,
      title: `Hello Slaylewks users new coupon available now! 🏷️ ${couponName}`,
      image: image || null,
      body: `We're delighted to share the joy with you! A new coupon is now available just for you. Use code 🏷️ ${couponCode}.`,
      data: { coupon_id: couponId, coupon_name: couponName, coupon_code: couponCode },
      dataModels: User
    };
    await new SendPushNotification().sendNotifyToUserPhone(reqObj);
  }
  async getFilterList(req_data: GetCouponDto) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { coupon_code: { [Op.iLike]: `%${searchText}%` } },
          { description: { [Op.iLike]: `%${searchText}%` } },
          { coupon_name: { [Op.iLike]: `%${searchText}%` } },
          //{'$marketingGroup.group_name$': { [Op.iLike]: `%${searchText}%` }},
          // {'$marketingGroup.description$': { [Op.iLike]: `%${searchText}%` }},
          //{'$coupon_service_category_mapping->service_cat_list.service_catego$': {[Op.iLike]: `%${searchText}%`}}
        ],
      };
    }

    let branchId = req_data?.branch_ids?.length > 0 ? { branch_ids: { [Op.overlap]: req_data.branch_ids } } : {};
    let vendorId = req_data?.vendor_ids?.length > 0 ? { vendor_id: { [Op.in]: req_data.vendor_ids } } : {};
    let providedBy = req_data?.provided_by === Provided_By.VENDOR ? Provided_By.VENDOR : Provided_By.SUPER_ADMIN;
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;

    let childVendor = await VendorRole.findAll({ attributes: ['branch_ids'], where: { id: req_data?.vendor_ids } });
    let branchIds = childVendor.map((data) => { return data.branch_ids })
    let vendId = childVendor.length > 0 ? { branch_ids: { [Op.overlap]: branchIds } } : branchId;


    let count = await Coupon.count({
      where: {
        provided_by: providedBy,
        ...vendId,
        ...branchId,
        ...search,
        is_deleted: false,
      },
    });

    let result = await Coupon.findAll({
      include: [
        {
          attributes: ['id', 'group_name', 'description'],
          model: MarketingGroup,
          as: 'marketingGroup',
          include: [
            {
              attributes: ['id', 'referal_name', 'description', 'referal_code'],
              model: ReferalManage,
              as: 'referal',
            },
          ],
        },
        {
          attributes: ['id'],
          model: CouponServiceCategoryMapping,
          as: 'coupon_service_category_mapping',
          include: [
            {
              attributes: ['id', ['service_category_name', 'service_catego']],
              model: MasterServiceCategory,
              as: 'service_cat_list',
            },
          ],
        },
      ],
      where: {
        provided_by: providedBy,
        ...vendId,
        ...branchId,
        ...search,
        is_deleted: false,
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']],
    });
    return { count: count, rows: result };
  }
  async findOne(coupon_id?: any): Promise<any> {
    // let qry: SequelizeFilter<Coupon> = coupon_id
    //   ? {
    //     where: { id: coupon_id },
    //     include: [
    //       {
    //         attributes: [BRANCH_ATTRIBUTES.ID, BRANCH_ATTRIBUTES.BRANCH_NAME],
    //         model: Branch,
    //         include: [
    //           {
    //             attributes: [
    //               SERVICE_ATTRIBUTES.ID,
    //               SERVICE_ATTRIBUTES.SERVICE_NAME,
    //               SERVICE_ATTRIBUTES.branch_id,
    //             ],
    //             model: Service,
    //           },
    //         ],
    //       }]
    //   }
    //   : {};
    // let couponData: any = await Coupon.findOne({
    //   where: { id: coupon_id },
    //   include: [
    //     {
    //       attributes: ['branch_id'],
    //       model: CouponBranchMapping,
    //       as: 'coupon_branch_mapping',
    //       // where: { coupon_id: coupon_id },
    //       // required: true,
    //       include: [
    //         {
    //           attributes: ['id', 'branch_name'],
    //           model: Branch,
    //           as: 'branch',
    //           include: [
    //             {
    //               attributes: ['id', 'service_name', 'branch_id'],
    //               model: Service,
    //               as: 'services',
    //               include: [
    //                 {
    //                   attributes: [],
    //                   model: CouponServiceMapping,
    //                   required: true,
    //                   where: { coupon_id: coupon_id }
    //                 },
    //               ],
    //             },
    //           ],
    //         },
    //       ],
    //     },
    //   ],
    // });
    let where = {
      where: {
        is_deleted: false,
        is_active: true,
        coupon_id: coupon_id
      }
    };
    let couponData: any = await Coupon.findOne({
      where: {
        is_deleted: false,
        // is_active: true,
        id: coupon_id
      }
    });
    let couponBranchData: any = await CouponBranchMapping.findAll({
      attributes: ['branch_id'],
      include: [{
        attributes: ['id', 'branch_name'],
        model: Branch,
        as: 'branch',
      }],
      ...where
    });
    let couponServiceData: any = await CouponServiceMapping.findAll({
      attributes: [],
      include: [{
        attributes: ['id', 'service_name', 'branch_id'],
        model: Service,
        as: 'services',
      }],
      ...where
    });

    const categories = await CouponCategoryMapping.findAll({
      attributes: [],
      ...where,
      include: [
        {
          model: MasterCategory,
          attributes: ['category', 'id'],
        },
      ],
    });
    const service_category = await CouponServiceCategoryMapping.findAll({
      attributes: [],
      ...where,
      include: [
        {
          model: MasterServiceCategory,
          attributes: ['service_category_name', 'id'],
        },
      ],
    });
    // const selected_services = await CouponServiceMapping.findAll({
    //   attributes: [],
    //   where: { coupon_id: coupon_id },
    //   include: {
    //     attributes: [
    //       SERVICE_ATTRIBUTES.ID,
    //       SERVICE_ATTRIBUTES.SERVICE_NAME,
    //       SERVICE_ATTRIBUTES.branch_id,
    //     ],
    //     model: Service,
    //   },
    // });
    // let couponData = await super.findOne(null, qry);
    return { ...couponData?.dataValues, couponBranchData, couponServiceData, categories, service_category };
  }
  async update(id: string, data: UpdateCouponDto): Promise<Coupon> {
    let coupon = await super.update(id, data);
    let qry = { where: { coupon_id: id } };
    // const deleteExitData = await Promise.all([
    await Helpers.collectionDelete(this.couponBranchRepo, qry),
      await Helpers.collectionDelete(this.couponServiceRepo, qry),
      await Helpers.collectionDelete(this.couponCategoryMapping, qry),
      await Helpers.collectionDelete(this.couponServiceCategoryMapping, qry),
      // ]);
      // if (data.branch_ids?.length > 0)
      this.mappings(id, data);
    return coupon;
  };
  //branch and services mapping with coupon
  async mappings(coupon_id: string, data: UpdateCouponDto) {
    try {
      let { branch_ids, service_ids, category_id, service_category_id } = data;
      if (branch_ids?.length > 0) {
        let couponBranchObj = branch_ids?.map((branch_id) => {
          return { coupon_id: coupon_id, branch_id: branch_id };
        });
        let result = await this.couponBranchRepo.bulkCreate(couponBranchObj);
      }
      if (service_ids?.length > 0) {
        let couponServiceObj = service_ids?.map((service_id) => {
          return { coupon_id: coupon_id, service_id: service_id };
        });
        let data = await this.couponServiceRepo.bulkCreate(couponServiceObj);
      }
      if (category_id && category_id?.length > 0) {
        let coupenCategoryMapping = category_id?.map((cat_id) => {
          return { coupon_id: coupon_id, category_id: cat_id };
        });
        let coupenCategoryMappingdata = await this.couponCategoryMapping.bulkCreate(coupenCategoryMapping);
      }
      if (service_category_id && service_category_id?.length > 0) {
        let coupenCategoryServiceMapping: any = service_category_id?.map((ser_cat_id) => {
          return { coupon_id: coupon_id, service_category_id: ser_cat_id };
        });
        let coupenCategoryServiceMappingdata = await this.couponServiceCategoryMapping.bulkCreate(
          coupenCategoryServiceMapping,
        );
      }
    } catch (err: any) {
      // console.log(err);
      logger.error('Coupon_mapping_error: ' + JSON.stringify(err));
      throw new Error(err);
    }
  }

  async delete(id: string) {
    let qry = { where: { coupon_id: id }, force: true };
    let coupon = await Helpers.collectionDelete(this.couponRepo, {
      where: { id: id },
    });
    await Promise.all([
      Helpers.collectionDelete(this.couponBranchRepo, qry),
      Helpers.collectionDelete(this.couponServiceRepo, qry),
      Helpers.collectionDelete(this.couponCategoryMapping, qry),
      Helpers.collectionDelete(this.couponServiceCategoryMapping, qry),
    ]);
    return coupon;
  }

  async couponusedCounts(req_data: CouponUsedDto) {
    let search;
    if (req_data.searchText) {
      search = {
        [Op.or]: [
          { coupon_code: { [Op.iLike]: `%${req_data.searchText}%` } },
          { description: { [Op.iLike]: `%${req_data.searchText}%` } },
        ],
      };
    }

    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let providedBy = req_data?.provided_by === Provided_By.VENDOR ? Provided_By.VENDOR : Provided_By.SUPER_ADMIN;

    const count = await Coupon.count({
      paranoid: false,
      where: {
        ...search,
        is_active: true,
        is_deleted: false,
        //   deleted_at: null,
        provided_by: providedBy,
      },
    });
    let searchqry = req_data.searchText
      ? `AND c.${COUPON_ATTRIBUTES.COUPON_CODE} ilike '%${req_data.searchText}%' OR c.${COUPON_ATTRIBUTES.DESCRIPTION} ilike '%${req_data.searchText}%'`
      : '';

    const results = await AppliedCoupon.sequelize.query(`WITH coupon_sum AS (
      SELECT "${APPLIED_COUPON_ATTRIBUTES.COUPON_ID}", CAST(SUM("${APPLIED_COUPON_ATTRIBUTES.USED_COUNT}")AS INTEGER) AS "user_count"
      FROM ${APPLIED_COUPON_ATTRIBUTES.TABLE_NAME}
      GROUP BY "${APPLIED_COUPON_ATTRIBUTES.COUPON_ID}" 
    )
    
    SELECT "cs"."${APPLIED_COUPON_ATTRIBUTES.COUPON_ID}", 
    CASE WHEN "cs"."user_count" IS null THEN 0 else "cs"."user_count" END,
     "c".*
    FROM coupon_sum  AS "cs"
    RIGHT JOIN ${COUPON_ATTRIBUTES.TABLE_NAME}  AS "c" ON "cs"."${APPLIED_COUPON_ATTRIBUTES.COUPON_ID}" = "c"."${COUPON_ATTRIBUTES.ID}"
    WHERE "c"."${COUPON_ATTRIBUTES.PROVIDED_BY}" = '${providedBy}'  ${searchqry} limit ${limit} offset ${offset};
    `);

    return { count: count, rows: results[0] };
    //     const results = await Coupon.sequelize.query(`SELECT
    // c.${COUPON_ATTRIBUTES.ID},
    // c.${COUPON_ATTRIBUTES.IS_ACTIVE},
    // c.${COUPON_ATTRIBUTES.IS_DELETED},
    // c.${COUPON_ATTRIBUTES.COUPON_CODE},
    // c.${COUPON_ATTRIBUTES.IMAGE},
    // c.${COUPON_ATTRIBUTES.VENDOR_ID},
    // c.${COUPON_ATTRIBUTES.DISCOUNT_TYPE},
    // c.${COUPON_ATTRIBUTES.DISCOUNT_VALUE},
    // c.${COUPON_ATTRIBUTES.QUANTITY_TYPE},
    // c.${COUPON_ATTRIBUTES.QUANTITY_VALUE},
    // c.${COUPON_ATTRIBUTES.PER_USER_LIMIT},
    // c.${COUPON_ATTRIBUTES.MINIMUM_SPEND},
    // c.${COUPON_ATTRIBUTES.MAX_DISCOUNT_AMOUNT},
    // c.${COUPON_ATTRIBUTES.START_DATE},
    // c.${COUPON_ATTRIBUTES.END_DATE},
    // c.${COUPON_ATTRIBUTES.DESCRIPTION},
    // c.${COUPON_ATTRIBUTES.CREATED_AT},
    // c.${COUPON_ATTRIBUTES.PROVIDED_BY},
    // c.${COUPON_ATTRIBUTES.PROVIDED_ID},
    // c.${COUPON_ATTRIBUTES.BRANCH_ID},
    // CAST(COUNT(DISTINCT o.${ORDER_ATTRIBUTES.user_id}) AS INTEGER )  AS user_count
    // FROM coupons c
    // left JOIN orders o ON c.id = o.${ORDER_ATTRIBUTES.applied_coupon_id}
    // WHERE c.${COUPON_ATTRIBUTES.IS_ACTIVE}= true
    // AND c.${COUPON_ATTRIBUTES.IS_DELETED} = false
    // AND c.${COUPON_ATTRIBUTES.PROVIDED_BY} = '${providedBy}'
    // AND c.${COUPON_ATTRIBUTES.DELETED_AT} IS NULL ${searchqry}
    // GROUP BY c.${COUPON_ATTRIBUTES.ID}, c.${COUPON_ATTRIBUTES.COUPON_CODE}, c.${COUPON_ATTRIBUTES.PROVIDED_BY}
    //  limit ${limit} offset ${offset}`);

    // return { count: count, rows: results[0] };
  }

  async getrandomCouponcode() {
    let randomCode = await this.generateCode();
    return randomCode;
  }

  private async generateCode() {
    let couponCode = await Helpers.generaterandomCouponcode();
    let count = await Coupon.count({ where: { coupon_code: couponCode } });
    if (!count) return couponCode;
    else return this.generateCode();
  }

  // let ifCheck = true;
  // let couponCode: string;
  // while (ifCheck) {
  //   couponCode = await Helpers.generaterandomCouponcode();
  //   let getCouponCodes = await Coupon.count({ where: { coupon_code: couponCode } });
  //   if (!getCouponCodes) {
  //     ifCheck = false;
  //     return couponCode;
  //   } else continue;
  // }
  // return couponCode;
}
