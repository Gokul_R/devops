import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsOptional } from "class-validator";
import { Provided_By } from "src/core/constants";
import { PaginationDto } from "src/core/interfaces/shared.dto";

export class CouponUsedDto extends PaginationDto {
    @ApiProperty({ example: `${Provided_By.SUPER_ADMIN} (or) ${Provided_By.VENDOR}` })
    @IsNotEmpty()
    @IsEnum(Provided_By)
    provided_by: string;
}