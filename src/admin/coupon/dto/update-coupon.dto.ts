import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { CreateCouponDto } from './create-coupon.dto';
import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { IsArray, IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { BRANCH_ID, Provided_By } from 'src/core/constants';

export class UpdateCouponDto extends PartialType(CreateCouponDto) { }
export class GetCouponDto extends PaginationDto {
    @ApiProperty({ example: [BRANCH_ID] })
    @IsOptional()
    @IsArray()
    branch_ids?: string[];

    @ApiProperty({ example: [BRANCH_ID] })
    @IsOptional()
    @IsArray()
    vendor_ids?: string[];

    @ApiProperty({ example: `${Provided_By.SUPER_ADMIN} (or) ${Provided_By.VENDOR}` })
    @IsOptional()
    @IsEnum(Provided_By)
    provided_by:string
}
