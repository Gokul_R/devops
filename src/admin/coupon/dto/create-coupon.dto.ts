import { ApiProperty, PickType } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsString,
  IsArray,
  IsEnum,
  IsNumber,
  IsDate,
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsUUID,
  ValidateIf,
} from 'class-validator';
import { BRANCH_ID, Provided_By } from 'src/core/constants';
import { SharedDto } from 'src/core/interfaces/shared.dto';

export enum CouponDiscountType {
  AMOUNT = 'amount',
  PERCENTAGE = 'percentage',
}
export enum LimitType {
  LIMITED = 'limited',
  UNLIMITED = 'unlimited',
}
// export enum Provided_By {
//   SUPER_ADMIN = 'SUPER_ADMIN',
//   VENDOR = 'VENDOR',
//   BRANCH = 'BRANCH'
// }

export class CreateCouponDto extends PickType(SharedDto, [
  'branch_ids',
  'image',
  // 'description',
  'service_ids',
  'vendor_id',
]) {
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: 'Best mega offer' })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty({ example: 'SAVE100' })
  @IsNotEmpty()
  @IsString()
  coupon_code?: string;

  @ApiProperty({ example: 'SUPER_ADMIN (or) VENDOR' })
  @IsNotEmpty()
  @IsString()
  @IsEnum(Provided_By)
  provided_by?: string;

  @ApiProperty({ example: BRANCH_ID })
  @IsNotEmpty()
  @IsString()
  provided_id?: string;

  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional()
  @IsArray()
  category_id: string[];

  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional()
  @IsArray()
  service_category_id: string[];

  @ApiProperty({ example: 'amount' })
  @IsNotEmpty()
  @IsEnum(CouponDiscountType)
  discount_type?: CouponDiscountType;

  @ApiProperty({ example: 10 })
  @IsNotEmpty()
  @IsNumber()
  discount_value?: number;

  @ApiProperty({ example: 'limited' })
  @IsEnum(LimitType)
  @IsString()
  @IsNotEmpty()
  quantity_type?: LimitType;

  @ApiProperty({ example: 100 })
  @IsNumber()
  @ValidateIf((o) => o.quantity_type == LimitType.LIMITED)
  quantity_value?: number;

  @ApiProperty({ example: 3 })
  @IsNotEmpty()
  @IsNumber()
  per_user_limit?: number;

  @ApiProperty({ example: 100 })
  @IsNotEmpty()
  @IsNumber()
  minimum_spend?: number;

  @ApiProperty({ example: 500 })
  @IsNotEmpty()
  @IsNumber()
  max_discount_amount?: number;

  @ApiProperty({ example: new Date() })
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsOptional()
  @IsNotEmpty({ message: 'Custom message for not empty validation' })
  @IsDate({ message: 'Custom message for date validation' })
  start_date?: string | null;

  @ApiProperty({ example: new Date() })
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsOptional()
  @IsNotEmpty({ message: 'Custom message for not empty validation' })
  @IsDate({ message: 'Custom message for date validation' })
  end_date?: string | null;

  // @ApiProperty({ example: true })
  // @IsBoolean()
  // @IsOptional()
  // grouped_coupon_type?: boolean;

  @ApiProperty({ example: BRANCH_ID })
  @IsOptional()
  @IsString()
  // @IsUUID()
  marketing_ids?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  coupon_name: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  lifetime_coupon: boolean;
}
