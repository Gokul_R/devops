import { Module } from '@nestjs/common';
import { CouponService } from './coupon.service';
import { CouponController } from './coupon.controller';
import { couponProviders } from './coupon.provider';

@Module({
  controllers: [CouponController],
  providers: [CouponService,...couponProviders],
  exports: [CouponService],
})
export class CouponModule {}
