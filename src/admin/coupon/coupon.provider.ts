import { commonProvider } from 'src/core/interfaces/common-providers';

export const couponProviders = [
  // {
  //   provide: COUPON_REPOSITORY,
  //   useValue: Coupon,
  // },
  // {
  //   provide: COUPON_BRANCH_REPOSITORY,
  //   useValue: CouponBranchMapping,
  // },
  // {
  //   provide: COUPON_SERVICE_REPOSITORY,
  //   useValue: CouponServiceMapping,
  // },
  commonProvider.coupon_repository,
  commonProvider.coupon_branch_repository,
  commonProvider.coupon_service_repository,
  commonProvider.coupon_category_mapping_repository,
  commonProvider.coupon_service_category_mapping_repository,
];
