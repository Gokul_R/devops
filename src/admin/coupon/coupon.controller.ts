import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { CouponService } from './coupon.service';
import { CreateCouponDto } from './dto/create-coupon.dto';
import { GetCouponDto, UpdateCouponDto } from './dto/update-coupon.dto';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import {
  EC200,
  EM106,
  EC500,
  EM100,
  EM127,
  EM116,
  EM104,
} from 'src/core/constants';
import Coupon from 'src/core/database/models/Coupons';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { CouponUsedDto } from './dto/coupon-used-count.dto';
@ApiTags('Coupons')
@Controller('coupon')
export class CouponController {
  constructor(private readonly couponService: CouponService) { }

  @Post()
  async create(@Body() createCouponDto: CreateCouponDto) {
    try {
      let result: Coupon = await this.couponService.create(createCouponDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Post('search')
  async getFilterList(@Body() getCoupenList: GetCouponDto) {
    try {
      let data = await this.couponService.getFilterList(getCoupenList);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @ApiQuery({
  //   name: "coupon_code",
  //   type: String,
  //   description: "Coupon code",
  //   required: false
  // })
  @Get()
  async findAll() {
    try {
      let result = await this.couponService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Get('random-code')
  async getrandomCouponcode() {
    try {
      let result = await this.couponService.getrandomCouponcode();
      return HandleResponse.buildSuccessObj(EC200, EM106, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Post('used-counts')
  async couponusedCounts(@Body() pagination: CouponUsedDto) {
    try {
      let result = await this.couponService.couponusedCounts(pagination);
      return HandleResponse.buildSuccessObj(EC200, EM106, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let result = await this.couponService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateCouponDto: UpdateCouponDto,
  ) {
    try {
      let result = await this.couponService.update(id, updateCouponDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let result = await this.couponService.delete(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }
}
