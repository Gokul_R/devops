import { Injectable } from '@nestjs/common';
import { CreateReferalManageDto } from './dto/create-referal-manage.dto';
import { UpdateReferalManageDto } from './dto/update-referal-manage.dto';
import ReferalManage from 'src/core/database/models/ReferalManage';
import { Op } from 'sequelize';
import MarketingGroup from 'src/core/database/models/MarketingGroup';

@Injectable()
export class ReferalManageService {
  async create(createReferalManageDto: CreateReferalManageDto) {
    return await ReferalManage.create({ ...createReferalManageDto });
  }

  async findAll() {
    return await ReferalManage.findAll({
    });
  }

  async findOne(id: string) {
    return await ReferalManage.findOne({
      where: { id: id }
    });
  }

  async update(id: string, updateReferalManageDto: UpdateReferalManageDto) {
    return await ReferalManage.update(updateReferalManageDto, { where: { id: id } });
  }

  async remove(id: string) {
    return await ReferalManage.destroy({ where: { id: id } });
  }

  async getFilterList(req_data: any) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { referal_name: { [Op.iLike]: `%${searchText}%` } },
          { description: { [Op.iLike]: `%${searchText}%` } },
          { referal_code: { [Op.iLike]: `%${searchText}%` } },
          // { '$marketing_group.group_name$': { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await ReferalManage.findAndCountAll({
      where: {
        ...search,
        is_deleted: false,
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']],
    });
  }
}
