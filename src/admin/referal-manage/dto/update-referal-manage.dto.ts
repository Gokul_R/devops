import { PartialType } from '@nestjs/swagger';
import { CreateReferalManageDto } from './create-referal-manage.dto';

export class UpdateReferalManageDto extends PartialType(CreateReferalManageDto) {}
