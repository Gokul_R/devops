import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsOptional, IsString, Length } from "class-validator";

export class CreateReferalManageDto {
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: "Referal Name" })
  @IsString()
  @IsNotEmpty()
  @Length(3, 50, {
    message: 'referal_name must be between 3 and 50 characters',
  })
  referal_name?: string;

  @ApiProperty({ example: "Some description" })
  @IsString()
  @IsNotEmpty()
  @Length(4, 250, {
    message: 'Description must be between 4 and 250 characters',
  })
  description?: string;

  @ApiProperty({ example: "BWYOSMWU" })
  @IsString()
  @IsNotEmpty()
  @Length(5, 100, {
    message: 'referal code must be between 5 and 100 characters',
  })
  referal_code?: string;

  // @ApiProperty({ example: 'cec5ca18-0a51-4e80-96a0-0ea1b1d9bd62' })
  // @IsNotEmpty()
  // @IsString()
  // marketing_ids?: string;
}
