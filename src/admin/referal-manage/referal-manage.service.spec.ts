import { Test, TestingModule } from '@nestjs/testing';
import { ReferalManageService } from './referal-manage.service';

describe('ReferalManageService', () => {
  let service: ReferalManageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReferalManageService],
    }).compile();

    service = module.get<ReferalManageService>(ReferalManageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
