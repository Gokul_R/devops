import { Module } from '@nestjs/common';
import { ReferalManageService } from './referal-manage.service';
import { ReferalManageController } from './referal-manage.controller';

@Module({
  controllers: [ReferalManageController],
  providers: [ReferalManageService]
})
export class ReferalManageModule {}
