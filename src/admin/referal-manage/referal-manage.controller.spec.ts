import { Test, TestingModule } from '@nestjs/testing';
import { ReferalManageController } from './referal-manage.controller';
import { ReferalManageService } from './referal-manage.service';

describe('ReferalManageController', () => {
  let controller: ReferalManageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReferalManageController],
      providers: [ReferalManageService],
    }).compile();

    controller = module.get<ReferalManageController>(ReferalManageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
