import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ReferalManageService } from './referal-manage.service';
import { CreateReferalManageDto } from './dto/create-referal-manage.dto';
import { UpdateReferalManageDto } from './dto/update-referal-manage.dto';
import { EC200, EC500, EM100, EM104, EM106, EM116, EM127 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { ApiTags } from '@nestjs/swagger';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@Controller('referal-manage')
@ApiTags('Referal Manage')
export class ReferalManageController {
  constructor(private readonly referalManageService: ReferalManageService) { }

  @Post()
  async create(@Body() createReferalManageDto: CreateReferalManageDto) {
    try {
      const result = await this.referalManageService.create(createReferalManageDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, result);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Post('search')
  async getFilterList(@Body() getCoupenList: PaginationDto) {
    try {
      let data = await this.referalManageService.getFilterList(getCoupenList);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get()
  async findAll() {
    try {
      let data = await this.referalManageService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.referalManageService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateReferalManageDto: UpdateReferalManageDto) {
    try {
      let data = this.referalManageService.update(id, updateReferalManageDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    try {
      let data = this.referalManageService.remove(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
