import { Test, TestingModule } from '@nestjs/testing';
import { BranchServiceTypeService } from './branch-service-type.service';

describe('BranchServiceTypeService', () => {
  let service: BranchServiceTypeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BranchServiceTypeService],
    }).compile();

    service = module.get<BranchServiceTypeService>(BranchServiceTypeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
