import { Inject, Injectable } from '@nestjs/common';
import { CreateBranchServiceTypeDto } from './dto/create-branch-service-type.dto';
import { UpdateBranchServiceTypeDto } from './dto/update-branch-service-type.dto';
import { BaseService } from '../../base.service';
import BranchServiceTypeMapping from 'src/core/database/models/BranchServiceTypeMapping';
import { ModelCtor } from 'sequelize-typescript';
import { BRANCH_SERVICE_TYPE_REPOSITORY } from 'src/core/constants';

@Injectable()
export class BranchServiceTypeService extends BaseService<BranchServiceTypeMapping> {
  protected model: ModelCtor<BranchServiceTypeMapping>;
  constructor(
    @Inject(BRANCH_SERVICE_TYPE_REPOSITORY)
    private readonly branchServiceTypeRepo: typeof BranchServiceTypeMapping,
  ) {
    super();
    this.model = this.branchServiceTypeRepo;
  }
  // create(createBranchServiceTypeDto: CreateBranchServiceTypeDto) {
  //   return 'This action adds a new branchServiceType';
  // }

  // findAll() {
  //   return `This action returns all branchServiceType`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} branchServiceType`;
  // }

  // update(id: number, updateBranchServiceTypeDto: UpdateBranchServiceTypeDto) {
  //   return `This action updates a #${id} branchServiceType`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} branchServiceType`;
  // }
}
