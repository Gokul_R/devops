import { PartialType } from '@nestjs/mapped-types';
import { CreateBranchServiceTypeDto } from './create-branch-service-type.dto';

export class UpdateBranchServiceTypeDto extends PartialType(CreateBranchServiceTypeDto) {}
