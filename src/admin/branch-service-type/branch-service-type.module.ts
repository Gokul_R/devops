import { Module } from '@nestjs/common';
import { BranchServiceTypeService } from './branch-service-type.service';
import { BranchServiceTypeController } from './branch-service-type.controller';
import { branchServiceTypeProviders } from './branch-service-type.providers';

@Module({
  // controllers: [BranchServiceTypeController],
  providers: [BranchServiceTypeService, ...branchServiceTypeProviders],
  exports:[BranchServiceTypeService]
})
export class BranchServiceTypeModule {}
