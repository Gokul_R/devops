import { Test, TestingModule } from '@nestjs/testing';
import { BranchServiceTypeController } from './branch-service-type.controller';
import { BranchServiceTypeService } from './branch-service-type.service';

describe('BranchServiceTypeController', () => {
  let controller: BranchServiceTypeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchServiceTypeController],
      providers: [BranchServiceTypeService],
    }).compile();

    controller = module.get<BranchServiceTypeController>(BranchServiceTypeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
