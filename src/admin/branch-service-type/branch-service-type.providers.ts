import { commonProvider } from 'src/core/interfaces/common-providers';

export const branchServiceTypeProviders = [
  // {
  //   provide: BRANCH_REPOSITORY,
  //   useValue: Branch,
  // },
  // {
  //   provide: BRANCH_SERVICE_TYPE_REPOSITORY,
  //   useValue: BranchServiceTypeMapping,
  // },
  commonProvider.branch_service_type_repository,
];
