import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { BranchServiceTypeService } from './branch-service-type.service';
import { CreateBranchServiceTypeDto } from './dto/create-branch-service-type.dto';
import { UpdateBranchServiceTypeDto } from './dto/update-branch-service-type.dto';

// // @Controller('branch-service-type')
export class BranchServiceTypeController {
//   constructor(private readonly branchServiceTypeService: BranchServiceTypeService) {}

//   @Post()
//   create(@Body() createBranchServiceTypeDto: CreateBranchServiceTypeDto) {
//     return this.branchServiceTypeService.create(createBranchServiceTypeDto);
//   }

//   @Get()
//   findAll() {
//     return this.branchServiceTypeService.findAll();
//   }

//   @Get(':id')
//   findOne(@Param('id') id: string) {
//     return this.branchServiceTypeService.findOne(id);
//   }

//   @Patch(':id')
//   update(@Param('id') id: string, @Body() updateBranchServiceTypeDto: UpdateBranchServiceTypeDto) {
//     return this.branchServiceTypeService.update(id, updateBranchServiceTypeDto);
//   }

//   @Delete(':id')
//   remove(@Param('id') id: string) {
//     return this.branchServiceTypeService.remove(id);
//   }
}
