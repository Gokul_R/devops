import {
  Controller,
  Post,
  Body,
  Patch,
  Param,
  Get,
  Delete
} from '@nestjs/common';
import { MasterServiceCategoriesService } from './master-service-categories.service';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../base.controller';
import { EC200, EM100, EM116, EM104, EM106, EM127 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import {
  CreateMasterServiceCategoryDto,
  GetFilterWiseDataDto,
  UpdateMasterServiceCategoryDto,
} from './dto/master-service-category.dto';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@Controller('master-service-categories')
@ApiTags('master-service-categories')
export class MasterServiceCategoriesController extends BaseController<CreateMasterServiceCategoryDto> {
  constructor(
    private readonly masterServiceCategoriesService: MasterServiceCategoriesService,
  ) {
    super(masterServiceCategoriesService);
  }

  @Post()
  async create(
    @Body() createMasterServiceCategoryDto: CreateMasterServiceCategoryDto,
  ) {
    try {
      let data: MasterServiceCategory =
        await this.masterServiceCategoriesService.create(
          createMasterServiceCategoryDto,
        );
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateMasterServiceCategoryDto: UpdateMasterServiceCategoryDto,
  ) {
    try {
      let data = await this.masterServiceCategoriesService.update(
        id,
        updateMasterServiceCategoryDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('search')
  async getFilterList(@Body() MasterServiceCategoryDto: PaginationDto) {
    try {
      let data = await this.masterServiceCategoriesService.getFilterList(MasterServiceCategoryDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  // @Get('by-category/:category_id')
  // async serviceCategoryByCategory(@Param('category_id') cat_id: string) {
  //   try {
  //     let data = await this.masterServiceCategoriesService.serviceCategoryByCategory(cat_id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }


  @Get()
  async findAll() {
    try {
      let data = await this.masterServiceCategoriesService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('by-filter')
  async findAllServicategoryByCategoriesWise(@Body() getFilterWiseDataDto: GetFilterWiseDataDto) {
    try {
      let data = await this.masterServiceCategoriesService.findAllServicategoryByCategoriesWise(getFilterWiseDataDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }


  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let data = await this.masterServiceCategoriesService.deleteData(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
