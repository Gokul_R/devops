import { ApiProperty, OmitType } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsNotEmpty, IsOptional, IsUrl, Length } from 'class-validator';
import { BRANCH_ID } from 'src/core/constants';

export class CreateMasterServiceCategoryDto {
  @ApiProperty()
  @Length(3, 100, {
    message: 'service category name must be between 3 and 100 characters',
  })
  @IsNotEmpty()
  service_category_name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  category_ids: Array<string>;

  @ApiProperty({ example: '' })
  @IsOptional()
  image: string;

  @ApiProperty({ example: '' })
  @Length(4, 250, {
    message: 'Description must be between 4 and 250 characters',
  })
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsBoolean()
  is_active: boolean;
}
export class UpdateMasterServiceCategoryDto extends OmitType(
  CreateMasterServiceCategoryDto,
  [],
) { }
export class GetFilterWiseDataDto {
  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional({ each: true })
  @IsArray()
  branch_ids: Array<string>;

  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional({ each: true })
  @IsArray()
  category_ids: Array<string>;


  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional({ each: true })
  @IsArray()
  service_category_ids: Array<string>;
}
