import { Injectable } from '@nestjs/common';
import { BaseService } from '../../base.service';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import { ModelCtor } from 'sequelize-typescript';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import CategoryServiceCategoryMappings from 'src/core/database/models/CategoryServiceCatMapping';
import MasterCategory from 'src/core/database/models/MasterCategory';
import { MASTER_CATEGORIES_ATTRIBUTES } from 'src/core/attributes/master_category';
import {
  CreateMasterServiceCategoryDto,
  GetFilterWiseDataDto,
  UpdateMasterServiceCategoryDto,
} from './dto/master-service-category.dto';
import { CategoryServiceCategoryService } from './category-service-category/category-service-category.service';
import { Op, Sequelize, col, fn } from 'sequelize';
import Helpers from 'src/core/utils/helpers';
import Service from 'src/core/database/models/Services';

@Injectable()
export class MasterServiceCategoriesService extends BaseService<MasterServiceCategory> {
  protected model: ModelCtor<MasterServiceCategory> = MasterServiceCategory;
  masterCatQry: SequelizeFilter<MasterServiceCategory>;
  constructor(
    private readonly categoryServiceCategoryService: CategoryServiceCategoryService,
  ) {
    super();
  }
  init() {
    this.masterCatQry = {
      include: [
        {
          attributes: ['category_id'],
          model: CategoryServiceCategoryMappings,
          where: { is_active: true },
          include: {
            attributes: [MASTER_CATEGORIES_ATTRIBUTES.category, MASTER_CATEGORIES_ATTRIBUTES.id],
            model: MasterCategory,
            where: { is_active: true },
          },
        },
      ],
    };
  }

  async findMasterServiceCategriesByCode(masterServiceCategory: any) {
    return await super.findOne(null, {
      where: {
        code: masterServiceCategory.code,
      },
    });
    //category service category mapping
  }
  async create(createMasterServiceCategoryDto: CreateMasterServiceCategoryDto): Promise<any> {
    let service_cat = await super.create(createMasterServiceCategoryDto);
    //category service category mapping
    let cat_service_cat_res = this.catServiceCatMapping(createMasterServiceCategoryDto, service_cat.id);

    return (service_cat['category_service_category'] = cat_service_cat_res);
  }
  async catServiceCatMapping(createMasterServiceCategoryDto, id) {
    let cat_service_cat: Array<any> = createMasterServiceCategoryDto.category_ids.map((cat_id) => {
      return { category_id: cat_id, service_category_id: id };
    });
    return await this.categoryServiceCategoryService.bulkCreate(cat_service_cat);
  }
  async findAll(): Promise<any> {
    this.init();
    return await MasterServiceCategory.findAll(this.masterCatQry);
  }
  async findOne(id: string): Promise<any> {
    this.init();
    this.masterCatQry.where = {
      id: id,
    };
    return super.findOne(null, this.masterCatQry);
  }
  async update(
    id: string,
    updateMasterServiceCategoryDto: UpdateMasterServiceCategoryDto,
  ): Promise<MasterServiceCategory> {
    //remove old data before update
    const deleteData = await CategoryServiceCategoryMappings.destroy({
      where: { service_category_id: id },
    });
    //after removed create new updated records
    let catServiceCatRes = await this.catServiceCatMapping(updateMasterServiceCategoryDto, id);
    let data = await super.update(id, updateMasterServiceCategoryDto);
    return (data['category'] = catServiceCatRes);
  }

  async getFilterList(req_data: any): Promise<any> {
    // this.init();
    let search;
    if (req_data.searchText) {
      search = {
        // [Op.or]: [
        //   fn('LOWER', col('service_category_name')), // Convert column value to lowercase
        //   fn('LOWER', col('description')), // Convert column value to lowercase
        //   // Sequelize.fn(
        //   //   'LOWER',
        //   //   Sequelize.col('categories->category.category'),
        //   // ),
        // ],
        [Op.or]: [
          {
            service_category_name: {
              [Op.iLike]: `%${req_data.searchText.toLowerCase()}%`,
            },
          }, // Convert search value to lowercase
          {
            description: {
              [Op.iLike]: `%${req_data.searchText.toLowerCase()}%`,
            },
          },
        ],
      };
    }

    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;

    let whereSearch = req_data.searchText
      ? ` AND ("categories"."service_category_name" ILIKE '%${req_data.searchText}%'
  OR "categories"."description" ILIKE '%${req_data.searchText}%')`
      : '';
    let masterCatQry = await MasterServiceCategory.findAll({
      attributes: [
        'id',
        'service_category_name',
        'created_at',
        'is_active',
        'description',
        'image',
        'code',
        [
          Sequelize.literal(
            `(SELECT CAST(COUNT("categories"."id") AS INTEGER) from "master_service_categories" AS "categories" WHERE ("categories".is_deleted = false AND "categories".deleted_at IS null ${whereSearch}))`,
          ),
          'count',
        ],
      ],
      include: [
        {
          attributes: ['category_id'],
          model: CategoryServiceCategoryMappings,
          include: [
            {
              attributes: ['id', 'category'],
              model: MasterCategory,
              // where:{...masterCatSearch},
            },
          ],
        },
      ],
      where: {
        is_deleted: false,
        deleted_at: null,
        ...search,
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']],
    });
    return {
      count: masterCatQry[0]?.dataValues?.count || 0,
      rows: masterCatQry,
    };
  }

  // async serviceCategoryByCategory(cat_id: string) {
  //   return await this.categoryServiceCategoryService.serviceCategoryByCategory(
  //     cat_id,
  //   );
  // }
  async findAllServicategoryByCategoriesWise(req_data: GetFilterWiseDataDto) {
    return await this.categoryServiceCategoryService.serviceCategoryByCategoryNew(req_data);
  }

  async deleteData(id: string) {
    await Helpers.softDelete(MasterServiceCategory, { where: { id: id } });
    await Helpers.softDelete(CategoryServiceCategoryMappings, { where: { service_category_id: id } });
    await Helpers.softDelete(Service, { where: { service_category_id: id } });
    return true;
  }
}
