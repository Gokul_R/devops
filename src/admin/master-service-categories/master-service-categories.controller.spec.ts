import { Test, TestingModule } from '@nestjs/testing';
import { MasterServiceCategoriesController } from './master-service-categories.controller';
import { MasterServiceCategoriesService } from './master-service-categories.service';

describe('MasterServiceCategoriesController', () => {
  let controller: MasterServiceCategoriesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MasterServiceCategoriesController],
      providers: [MasterServiceCategoriesService],
    }).compile();

    controller = module.get<MasterServiceCategoriesController>(MasterServiceCategoriesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
