import { Module } from '@nestjs/common';
import { MasterServiceCategoriesService } from './master-service-categories.service';
import { MasterServiceCategoriesController } from './master-service-categories.controller';
import { CategoryServiceCategoryService } from './category-service-category/category-service-category.service';
import { masterServiceCategoriesProviders } from './master-service-categories.providers';

@Module({
  controllers: [MasterServiceCategoriesController],
  providers: [MasterServiceCategoriesService,CategoryServiceCategoryService,...masterServiceCategoriesProviders],
  exports:[MasterServiceCategoriesService]
})
export class MasterServiceCategoriesModule {}
