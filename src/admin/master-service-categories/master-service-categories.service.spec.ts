import { Test, TestingModule } from '@nestjs/testing';
import { MasterServiceCategoriesService } from './master-service-categories.service';

describe('MasterServiceCategoriesService', () => {
  let service: MasterServiceCategoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MasterServiceCategoriesService],
    }).compile();

    service = module.get<MasterServiceCategoriesService>(MasterServiceCategoriesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
