import { commonProvider } from 'src/core/interfaces/common-providers';

export const masterServiceCategoriesProviders = [
  // {
  //   provide: BRANCH_REPOSITORY,
  //   useValue: Branch,
  // },
  // {
  //   provide: BRANCH_SERVICE_CATEGORY_REPOSITORY,
  //   useValue: BranchServiceCategoryMapping,
  // },
  // {
  //   provide: BRANCH_CATEGORY_REPOSITORY,
  //   useValue: BranchCategoryMapping,
  // },
  // {
  //   provide: BRANCH_SERVICE_TYPE_REPOSITORY,
  //   useValue: BranchServiceTypeMapping,
  // },
  // {
  //   provide: MASTER_SERVICE_CATEGORY_REPOSITORY,
  //   useValue: MasterServiceCategory,
  // },
  // {
  //   provide: MASTER_CATEGORY_REPOSITORY,
  //   useValue: MasterCategory,
  // },
  commonProvider.branch_repository,
  commonProvider.branch_service_category_repository,
  commonProvider.branch_catagory_repository,
  commonProvider.branch_service_type_repository,
  commonProvider.master_service_category_repository,
  commonProvider.master_category_repository,
  commonProvider.category_service_cat_repo,
];
