import { Injectable } from '@nestjs/common';
import { Sequelize, where } from 'sequelize';
import { ModelCtor } from 'sequelize-typescript';
import { BaseService } from 'src/base.service';
import BranchServiceCategoryMapping from 'src/core/database/models/BranchServiceCategoryMapping.ts';
import CategoryServiceCategoryMappings from 'src/core/database/models/CategoryServiceCatMapping';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { GetFilterWiseDataDto } from '../dto/master-service-category.dto';
import Service from 'src/core/database/models/Services';
import BranchCategoryMapping from 'src/core/database/models/BranchCategoryMapping';
import MasterCategory from 'src/core/database/models/MasterCategory';

@Injectable()
export class CategoryServiceCategoryService extends BaseService<CategoryServiceCategoryMappings> {
  protected model: ModelCtor<CategoryServiceCategoryMappings> = CategoryServiceCategoryMappings;
  constructor() {
    // private readonly categoryServiceCategoryRepo: typeof CategoryServiceCategoryMappings,
    super();
    // this.model = categoryServiceCategoryRepo;
  }
  // async serviceCategoryByCategory(cat_id: string) { // old written by anand
  //   let qry: SequelizeFilter<CategoryServiceCategoryMappings> = {
  //     attributes: ['id'],
  //     where: { category_id: cat_id },
  //     include: [{ model: MasterServiceCategory }],
  //   };
  //   let service_cats = await super.findAll(qry);
  //   return service_cats;
  // }

  async serviceCategoryByCategoryNew(req_data?: GetFilterWiseDataDto) {
    // written by sasi 20230911
    if (!req_data?.category_ids && req_data?.branch_ids && !req_data?.service_category_ids) {
      //category filter with (branch id)
      let service_cats = await BranchCategoryMapping.findAll({
        attributes: [
          [Sequelize.col('master_category.category'), 'category_name'],
          [Sequelize.col('master_category.id'), 'category_id'],
          [Sequelize.col('master_category.code'), 'code'],
        ],
        raw: true,
        where: { branch_id: req_data?.branch_ids },
        include: [
          {
            attributes: ['category', 'id', 'code'],
            model: MasterCategory,
            where: { is_active: true, is_deleted: false, deleted_at: null },
          },
        ],
      });
      return service_cats;
    } else if (req_data?.category_ids && !req_data?.branch_ids && !req_data?.service_category_ids) {
      // service category filter with (category id)
      let service_cats = await CategoryServiceCategoryMappings.findAll({
        attributes: [
          [Sequelize.col('masterServiceCategory.service_category_name'), 'service_cat_name'],
          [Sequelize.col('masterServiceCategory.id'), 'service_cat_id'],
          [Sequelize.col('masterServiceCategory.code'), 'code'],
        ],
        where: { category_id: req_data?.category_ids },
        include: [
          {
            attributes: ['service_category_name', 'id', 'code'],
            model: MasterServiceCategory,
            where: { is_active: true, is_deleted: false, deleted_at: null },
          },
        ],
        group: ['"masterServiceCategory"."service_category_name"', 'masterServiceCategory.id'],
        raw: true,
      });
      return service_cats;
    } else if (req_data?.category_ids && req_data?.branch_ids && !req_data?.service_category_ids) {
      // service category filter with (brand id and category id)
      let branchID: any = req_data?.branch_ids ? { where: { branch_id: req_data?.branch_ids } } : '';
      let service_cats = await BranchServiceCategoryMapping.findAll({
        attributes: ['id', 'service_category_id'],
        ...branchID,
        include: [
          {
            required: true,
            model: MasterServiceCategory,
            where: { is_active: true, is_deleted: false, deleted_at: null },
            include: [
              {
                attributes: ['id', 'category_id'],
                where: { category_id: req_data?.category_ids },
                model: CategoryServiceCategoryMappings,
              },
            ],
          },
        ],
      });
      return service_cats;
    } else if (req_data?.category_ids && req_data?.branch_ids && req_data?.service_category_ids) {
      // service list filter with (brand id and category id and serviceCategorId)
      let where = {
        branch_id: req_data?.branch_ids,
        category_id: req_data?.category_ids,
        service_category_id: req_data?.service_category_ids,
      };
      let service_cats = await Service.findAll({
        where: {
          is_active: true,
          is_deleted: false,
          deleted_at: null,
          ...where,
        },
      });
      return service_cats;
    } else return [];
  }
}
