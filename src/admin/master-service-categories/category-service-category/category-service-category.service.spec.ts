import { Test, TestingModule } from '@nestjs/testing';
import { CategoryServiceCategoryService } from './category-service-category.service';

describe('CategoryServiceCategoryService', () => {
  let service: CategoryServiceCategoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CategoryServiceCategoryService],
    }).compile();

    service = module.get<CategoryServiceCategoryService>(CategoryServiceCategoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
