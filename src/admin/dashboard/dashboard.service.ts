import { Inject, Injectable } from '@nestjs/common';
import Order from 'src/core/database/models/Order';
import User from 'src/core/database/models/User';
import { USER_ATTRIBUTES } from 'src/core/attributes/user';
import { ORDER_ATTRIBUTES } from 'src/core/attributes/order';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import { Op, QueryTypes } from 'sequelize';
import { SEQUELIZE } from 'src/core/constants';
import { Sequelize } from 'sequelize-typescript';
import Branch from 'src/core/database/models/Branch';
import sequelize from 'sequelize';
import Vendor from 'src/core/database/models/Vendor';
import AdminUser from 'src/core/database/models/AdminUser';
import { VendorType } from 'src/core/utils/enum';
import { CreateDashboardDto, GetDashboardDto } from './dto/create-dashboard.dto';
import { GetDashBoardDto } from './dto/get-orders-dashboard.dto';
import moment from 'moment';
import VendorRole from 'src/core/database/models/VendorRoles';

@Injectable()
export class DashboardService {
  constructor(@Inject(SEQUELIZE) private readonly sequelize: Sequelize) { }
  async getDashBoard(req_data: GetDashboardDto) {
    let { admin_id, vendor_id, from_date, to_date } = req_data;
    let branchID = [];


    if (vendor_id) {
      let childVendor = await VendorRole.findOne({ attributes: ['vendor_id'], where: { id: vendor_id } });
      if (childVendor) vendor_id = childVendor?.vendor_id;
      let branchids = await Branch.findAll({
        attributes: ['id'],
        where: { vendor_id: vendor_id },
      });
      branchids.length > 0
        ? branchids.forEach((branchId) => {
          return branchID.push(`'${branchId.id}'`);
        })
        : [];
    }

    let freelanchfilter = `${from_date && to_date
      ? `(${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.CREATED_AT} BETWEEN '${from_date} 00:00:00.000' AND '${to_date} 23:59:59.999' ) AND ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null AND `
      : ` ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null AND`
      }`;
    let branchfilter = `${from_date && to_date
      ? `WHERE  (${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.CREATED_AT} BETWEEN '${from_date} 00:00:00.000' AND '${to_date} 23:59:59.999') AND ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null`
      : `WHERE  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null`
      }`;
    let admindateQry = `${from_date && to_date
      ? `WHERE (${ORDER_ATTRIBUTES.TABLE_NAME}.${ORDER_ATTRIBUTES.created_at} BETWEEN '${from_date} 00:00:00.000' AND '${to_date} 23:59:59.999') AND (${USER_ATTRIBUTES.TABLE_NAME}.${USER_ATTRIBUTES.CREATED_AT} BETWEEN   '${from_date}  00:00:00.000' AND   '${to_date} 23:59:59.999' )`
      : ''
      } `;
    let newCustomerfilter = `${from_date && to_date
      ? `CASE WHEN ${USER_ATTRIBUTES.TABLE_NAME}."created_at" BETWEEN '${from_date} 00:00:00.000' AND '${to_date} 23:59:59.999' THEN ${USER_ATTRIBUTES.TABLE_NAME}.id END) AS "last30DaysUserCount",`
      : `CASE WHEN DATE(${USER_ATTRIBUTES.TABLE_NAME}."created_at") =  CURRENT_DATE - INTERVAL '30 days' THEN ${USER_ATTRIBUTES.TABLE_NAME}.id END) AS "last30DaysUserCount" ,`
      }`;
    let branchQry =
      vendor_id && branchID.length > 0
        ? ` LEFT JOIN
    ${BRANCH_ATTRIBUTES.TABLE_NAME} AS branch
  ON  
    ${ORDER_ATTRIBUTES.TABLE_NAME}.branch_id = branch.id 
  WHERE
   branch.vendor_id   IN ('${vendor_id}') ${from_date && to_date
          ? ` AND (branch.${BRANCH_ATTRIBUTES.CREATED_AT} BETWEEN   '${from_date} 00:00:00.000' AND   '${to_date} 23:59:59.999' AND ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null  )`
          : `AND ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null`
        }`
        : ` LEFT JOIN
   ${BRANCH_ATTRIBUTES.TABLE_NAME} AS branch
 ON
   ${ORDER_ATTRIBUTES.TABLE_NAME}.branch_id = branch.id 
 WHERE
  branch.vendor_id   IN ('${vendor_id}')  ${from_date && to_date
          ? `AND (branch.${BRANCH_ATTRIBUTES.CREATED_AT} BETWEEN   '${from_date} 00:00:00.000' AND '${to_date} 23:59:59.999' AND ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null  )`
          : `AND ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null`
        }`;
    let branQry =
      vendor_id && branchID.length > 0
        ? `WHERE ${BRANCH_ATTRIBUTES.TABLE_NAME}.vendor_id = '${vendor_id}'  ${from_date && to_date
          ? `AND (${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.CREATED_AT} BETWEEN  '${from_date} 00:00:00.000' AND '${to_date} 23:59:59.999' AND  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null )`
          : `AND  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null`
        }`
        : ` ${from_date && to_date
          ? `WHERE (${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.CREATED_AT} BETWEEN  '${from_date} 00:00:00.000' AND '${to_date} 23:59:59.999') AND  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null  `
          : `WHERE  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null`
        } `;
    let freelaunchQry =
      vendor_id && branchID.length > 0
        ? ` ${BRANCH_ATTRIBUTES.TABLE_NAME}.vendor_id = '${vendor_id}' ${from_date && to_date
          ? `AND (${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.CREATED_AT} BETWEEN '${from_date} 00:00:00.000' AND '${to_date} 23:59:59.999' ) AND ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null AND`
          : `AND  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null AND`
        }`
        : `${from_date && to_date
          ? ` (${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.CREATED_AT} BETWEEN '${from_date} 00:00:00.000' AND '${to_date} 23:59:59.999' ) AND ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null AND `
          : `  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null AND `
        } `;
    let branchTovendorCount =
      vendor_id && branchID.length > 0
        ? ` AND orders."${ORDER_ATTRIBUTES.branch_id}" IN (${branchID})`
        : ` AND orders."${ORDER_ATTRIBUTES.branch_id}" IN ('${vendor_id}')`;

    let OrderQry = admin_id
      ? from_date && to_date
        ? `WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}.${ORDER_ATTRIBUTES.created_at} >= '${from_date} 00:00:00.000' AND  ${ORDER_ATTRIBUTES.TABLE_NAME}.${ORDER_ATTRIBUTES.created_at} <= '${to_date} 23:59:59.999' AND ${ORDER_ATTRIBUTES.deleted_at} IS null`
        : `WHERE ${ORDER_ATTRIBUTES.deleted_at} IS null`
      : from_date && to_date
        ? ` LEFT JOIN "branches" ON "orders"."branch_id" = "branches"."id"  WHERE  "branches"."vendor_id" = '${vendor_id}' AND ${ORDER_ATTRIBUTES.TABLE_NAME}.${ORDER_ATTRIBUTES.created_at} >= '${from_date} 00:00:00.000' AND  ${ORDER_ATTRIBUTES.TABLE_NAME}.${ORDER_ATTRIBUTES.created_at} <= '${to_date} 23:59:59.999' AND ${ORDER_ATTRIBUTES.TABLE_NAME}.${ORDER_ATTRIBUTES.deleted_at} IS null AND  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null`
        : ` LEFT JOIN "branches" ON "orders"."branch_id" = "branches"."id"  WHERE  "branches"."vendor_id" = '${vendor_id}' AND ${ORDER_ATTRIBUTES.TABLE_NAME}.${ORDER_ATTRIBUTES.deleted_at} IS null AND  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null`;

    let userQry = admin_id
      ? from_date && to_date
        ? `WHERE ${USER_ATTRIBUTES.TABLE_NAME}.${USER_ATTRIBUTES.CREATED_AT} >= '${from_date} 00:00:00.000' AND  ${USER_ATTRIBUTES.TABLE_NAME}.${USER_ATTRIBUTES.CREATED_AT} <= '${to_date} 23:59:59.999' AND ${USER_ATTRIBUTES.TABLE_NAME}.${USER_ATTRIBUTES.DELETED_AT} IS null`
        : `WHERE ${USER_ATTRIBUTES.TABLE_NAME}.${USER_ATTRIBUTES.DELETED_AT} IS null`
      : from_date && to_date
        ? ` LEFT  JOIN "orders" ON "users"."id" = "orders"."user_id" LEFT JOIN "branches" ON "orders"."branch_id" = "branches"."id"  WHERE  "branches"."vendor_id" = ('${vendor_id}') AND   ${USER_ATTRIBUTES.TABLE_NAME}.${USER_ATTRIBUTES.CREATED_AT} >= '${from_date} 00:00:00.000' AND  ${USER_ATTRIBUTES.TABLE_NAME}.${USER_ATTRIBUTES.CREATED_AT} <= '${to_date} 23:59:59.999' AND ${USER_ATTRIBUTES.TABLE_NAME}.${USER_ATTRIBUTES.DELETED_AT} IS null AND  ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null `
        : `LEFT  JOIN "orders" ON "users"."id" = "orders"."user_id" LEFT JOIN "branches" ON "orders"."branch_id" = "branches"."id"  WHERE  "branches"."vendor_id" =  ('${vendor_id}') AND  ${USER_ATTRIBUTES.TABLE_NAME}.${USER_ATTRIBUTES.DELETED_AT} IS null AND ${BRANCH_ATTRIBUTES.TABLE_NAME}.${BRANCH_ATTRIBUTES.DELETED_AT} IS null `;

    const [getTotalCount, getTodayCount] = await Promise.all([
      this.getTotalCounts(
        admin_id,
        //   branchQry,
        branQry,
        freelaunchQry,
        //   admindateQry,
        freelanchfilter,
        branchfilter,
        newCustomerfilter,
        OrderQry,
        userQry,
      ),
      this.getTodayCounts(req_data, branchTovendorCount),
    ]);
    return { dataCount: getTotalCount[0], todayCount: getTodayCount[0] };
  }
  getTotalCounts = async (
    admin_id: string,
    //   branchQry: string,
    branQry: string,
    freelaunchQry: string,
    //  admindateQry: string,
    freelanchfilter: string,
    branchfilter: string,
    newCustomerfilter: string,
    OrderQry: string,
    userQry: string,
  ) => {
    const getCount = await User.sequelize.query(`SELECT 
  COUNT(DISTINCT ${USER_ATTRIBUTES.TABLE_NAME}.id) AS "userCount",
  COUNT(DISTINCT ${newCustomerfilter}
 (SELECT COUNT(DISTINCT CASE WHEN ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.PENDING
      }' THEN ${ORDER_ATTRIBUTES.TABLE_NAME}.id END) AS "pendingOrderCount" from ${ORDER_ATTRIBUTES.TABLE_NAME
      } ${OrderQry}) ,
 (SELECT COUNT(DISTINCT CASE WHEN ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.COMPLETED
      }' THEN ${ORDER_ATTRIBUTES.TABLE_NAME}.id END) AS "completedOrderCount" from ${ORDER_ATTRIBUTES.TABLE_NAME
      } ${OrderQry}) ,
 (SELECT COUNT(DISTINCT CASE WHEN ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CONFIRMED
      }' THEN ${ORDER_ATTRIBUTES.TABLE_NAME}.id END) AS "confirmedOrderCount" from ${ORDER_ATTRIBUTES.TABLE_NAME
      } ${OrderQry}) ,
 (SELECT COUNT(DISTINCT CASE WHEN ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CANCELLED
      }' THEN ${ORDER_ATTRIBUTES.TABLE_NAME}.id END) AS "cancelledOrderCount" from ${ORDER_ATTRIBUTES.TABLE_NAME
      } ${OrderQry}) ,
(SELECT COUNT(DISTINCT ${BRANCH_ATTRIBUTES.ID}) FROM ${BRANCH_ATTRIBUTES.TABLE_NAME} ${!admin_id ? branQry : branchfilter
      } ) AS "branchCount",
  (SELECT COUNT(DISTINCT ${BRANCH_ATTRIBUTES.ID}) FROM ${BRANCH_ATTRIBUTES.TABLE_NAME} WHERE ${!admin_id ? freelaunchQry : freelanchfilter
      }   "${BRANCH_ATTRIBUTES.VENDOR_TYPE}" = 'freelancer') AS "freeLaunchCount"
FROM ${USER_ATTRIBUTES.TABLE_NAME} ${userQry}`);
    return getCount[0];
  };
  getTodayCounts = async (req_data: GetDashboardDto, branchID: any) => {
    let getTodayCounts:any;
    if (req_data.vendor_id && !req_data.admin_id) {
       getTodayCounts = await Order.sequelize.query(`
      WITH
      yesterday_pending_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayPendingOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.PENDING}' ${branchID}
      ),
      today_pending_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayPendingOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND ${ORDER_ATTRIBUTES.TABLE_NAME}. "created_at" < CURRENT_DATE + INTERVAL '1 day'
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.PENDING}' ${branchID}
      ),
      yesterday_completed_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayCompletedOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.COMPLETED}' ${branchID}
      ),
      today_completed_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayCompletedOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.COMPLETED}' ${branchID}
      ),
      yesterday_request_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayRequestOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CONFIRMED}' ${branchID}
      ),
      today_request_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayRequestOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CONFIRMED}' ${branchID}
      ),
      yesterday_cancelled_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayCancelledOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CANCELLED}'${branchID}
      ),
      today_cancelled_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayCancelledOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CANCELLED}' ${branchID}
      )
      SELECT
      today_pending_orders."todayPendingOrderCount",
      yesterday_pending_orders."yesterdayPendingOrderCount",
      today_pending_orders."todayPendingOrderCount" - yesterday_pending_orders."yesterdayPendingOrderCount" AS "pendingOrderCountChange",
      today_completed_orders."todayCompletedOrderCount",
      yesterday_completed_orders."yesterdayCompletedOrderCount",
      today_completed_orders."todayCompletedOrderCount" - yesterday_completed_orders."yesterdayCompletedOrderCount" AS "completedOrderCountChange",
      today_request_orders."todayRequestOrderCount",
      yesterday_request_orders."yesterdayRequestOrderCount",
      today_request_orders."todayRequestOrderCount" - yesterday_request_orders."yesterdayRequestOrderCount" AS "requestOrderCountChange",
      today_cancelled_orders."todayCancelledOrderCount",
      yesterday_cancelled_orders."yesterdayCancelledOrderCount",
      today_cancelled_orders."todayCancelledOrderCount" - yesterday_cancelled_orders."yesterdayCancelledOrderCount" AS "cancelledOrderCountChange"

    FROM
      today_pending_orders
    CROSS JOIN
      yesterday_pending_orders
    CROSS JOIN
      today_completed_orders
    CROSS JOIN
      yesterday_completed_orders
    CROSS JOIN
      today_request_orders
    CROSS JOIN
      yesterday_request_orders
    CROSS JOIN
    yesterday_cancelled_orders
    CROSS JOIN
    today_cancelled_orders;
    `);
      return getTodayCounts[0];
    } else {
       getTodayCounts = await User.sequelize.query(`
      WITH
      yesterday_users AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayUserCount"
        FROM ${USER_ATTRIBUTES.TABLE_NAME}
        WHERE ${USER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${USER_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
      ),
       today_users AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayUserCount"
        FROM ${USER_ATTRIBUTES.TABLE_NAME}
        WHERE ${USER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
      ),
      yesterday_pending_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayPendingOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.PENDING}'
      ),
      today_pending_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayPendingOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.PENDING}'
      ),
      yesterday_completed_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayCompletedOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.COMPLETED}'
      ),
      today_completed_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayCompletedOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.COMPLETED}'
      ),
      yesterday_request_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayRequestOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CONFIRMED}'
      ),
      today_request_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayRequestOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CONFIRMED}'
      ),
      yesterday_branches AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayBranchCount"
        FROM ${BRANCH_ATTRIBUTES.TABLE_NAME}
        WHERE "vendor_type" = '${VendorType.BUSINESS}' AND ${BRANCH_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${BRANCH_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
      ),
      today_branches AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayBranchCount"
        FROM ${BRANCH_ATTRIBUTES.TABLE_NAME}
        WHERE "vendor_type" = '${VendorType.BUSINESS}' AND ${BRANCH_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
      ),
      yesterday_free_launches AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayFreeLaunchCount"
        FROM ${BRANCH_ATTRIBUTES.TABLE_NAME}
        WHERE "vendor_type" = '${VendorType.FREELANCER}' AND ${BRANCH_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${BRANCH_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
      ),
      today_free_launches AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayFreeLaunchCount"
        FROM ${BRANCH_ATTRIBUTES.TABLE_NAME}
        WHERE "vendor_type" = '${VendorType.FREELANCER}' AND ${BRANCH_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
      ),
      yesterday_cancelled_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "yesterdayCancelledOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE - INTERVAL '1 day' AND ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" < CURRENT_DATE
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CANCELLED}'
      ),
      today_cancelled_orders AS (
        SELECT CAST(COUNT(*) AS INTEGER) AS "todayCancelledOrderCount"
        FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
        WHERE ${ORDER_ATTRIBUTES.TABLE_NAME}."created_at" >= CURRENT_DATE AND "created_at" < CURRENT_DATE + INTERVAL '1 day'
          AND ${ORDER_ATTRIBUTES.TABLE_NAME}."order_status" = '${APPOINTMENT_STATUS.CANCELLED}' 
      )
      SELECT
      today_users."todayUserCount",
      yesterday_users."yesterdayUserCount",
      today_users."todayUserCount" - yesterday_users."yesterdayUserCount" AS "userCountChange",
      today_pending_orders."todayPendingOrderCount",
      yesterday_pending_orders."yesterdayPendingOrderCount",
      today_pending_orders."todayPendingOrderCount" - yesterday_pending_orders."yesterdayPendingOrderCount" AS "pendingOrderCountChange",
      today_completed_orders."todayCompletedOrderCount",
      yesterday_completed_orders."yesterdayCompletedOrderCount",
      today_completed_orders."todayCompletedOrderCount" - yesterday_completed_orders."yesterdayCompletedOrderCount" AS "completedOrderCountChange",
      today_request_orders."todayRequestOrderCount",
      yesterday_request_orders."yesterdayRequestOrderCount",
      today_request_orders."todayRequestOrderCount" - yesterday_request_orders."yesterdayRequestOrderCount" AS "requestOrderCountChange",
      today_branches."todayBranchCount",
      yesterday_branches."yesterdayBranchCount",
      today_branches."todayBranchCount" - yesterday_branches."yesterdayBranchCount" AS "branchCountChange",
      today_free_launches."todayFreeLaunchCount",
      yesterday_free_launches."yesterdayFreeLaunchCount",
      today_free_launches."todayFreeLaunchCount" - yesterday_free_launches."yesterdayFreeLaunchCount" AS "freeLaunchCountChange",
      today_cancelled_orders."todayCancelledOrderCount",
      yesterday_cancelled_orders."yesterdayCancelledOrderCount",
      today_cancelled_orders."todayCancelledOrderCount" - yesterday_cancelled_orders."yesterdayCancelledOrderCount" AS "cancelledOrderCountChange"
    FROM
      today_users
    CROSS JOIN
      yesterday_users
    CROSS JOIN
      today_pending_orders
    CROSS JOIN
      yesterday_pending_orders
    CROSS JOIN
      today_completed_orders
    CROSS JOIN
      yesterday_completed_orders
    CROSS JOIN
      today_request_orders
    CROSS JOIN
      yesterday_request_orders
    CROSS JOIN
      today_branches
    CROSS JOIN
      yesterday_branches
    CROSS JOIN
      today_free_launches
    CROSS JOIN
      yesterday_free_launches
    CROSS JOIN
      yesterday_cancelled_orders
    CROSS JOIN
      today_cancelled_orders;
    `);
      return getTodayCounts[0];
    }
  };
  getOrderData = async (req_data: GetDashBoardDto) => {
    let { admin_id, vendor_id } = req_data;
    if (vendor_id) {
      let childVendor = await VendorRole.findOne({ attributes: ['vendor_id'], where: { id: vendor_id } });
      if (childVendor) vendor_id = childVendor?.vendor_id;
    }
    let vendorId = vendor_id ? { where: { vendor_id: vendor_id } } : {};
    let where = req_data.orderType == 'All' ? {} : req_data.orderType == APPOINTMENT_STATUS.PENDING ? { where: { order_status: { [Op.in]: [APPOINTMENT_STATUS.PENDING, APPOINTMENT_STATUS.AWAITING_APPROVAL] } } } : { where: { order_status: req_data.orderType } };

    const getOdersData = await Order.findAll({
      attributes: [
        'order_id',
        'id',
        'user_id',
        'service_type',
        'payment_status',
        'final_price',
        'original_price',
        'discount',
        'rescheduled',
        'customer_name',
        'customer_mobile',
        'customer_email',
        'pay_tnx_id',
        'order_status',
        'appointment_start_date_time',
      ],
      include: [
        {
          attributes: ['reg_id', 'id', 'branch_name'],
          model: Branch,
          ...vendorId,
        },
      ],
      ...where,
      limit: req_data.limit || 10,
      order: [['created_at', 'DESC']],
    });
    return getOdersData;
  };
  getRevenueAmount = async (req_data: GetDashboardDto) => {
    let { admin_id, vendor_id, from_date, to_date } = req_data;
    if (vendor_id) {
      let childVendor = await VendorRole.findOne({ attributes: ['vendor_id'], where: { id: vendor_id } });
      if (childVendor) vendor_id = childVendor?.vendor_id;
    }
    let vendorId = vendor_id ? { 'b.vendor_id': vendor_id } : '';
    let currentDate = to_date ? to_date : new Date().toISOString().split('T')[0];
    let oneYearAgoDate: any = new Date();
    oneYearAgoDate = from_date
      ? from_date
      : new Date(new Date().getFullYear() - 1, new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0];
    // const orders: any = await Order.findAll({
    //   attributes: [
    //     [this.sequelize.fn('date_trunc', 'month', this.sequelize.col('Order.created_at')), 'month'],
    //     [
    //       this.sequelize.literal(`SUM(
    //         CASE
    //           WHEN "branch"."commission_type" = 'percentage' THEN (("final_price" * "branch"."payment_commission") / 100)
    //           WHEN "branch"."commission_type" = 'amount' THEN "branch"."payment_commission"
    //           ELSE 0
    //         END
    //       )`),
    //       'revenueAmount',
    //     ],
    //   ],
    //   include: [
    //     {
    //       model: Branch,
    //       as: 'branch',
    //       attributes: [],
    //     },
    //   ],
    //   where: {
    //     [Op.and]: [
    //       {
    //         order_status: APPOINTMENT_STATUS.COMPLETED,
    //       },
    //       {
    //         created_at: {
    //           [Op.gt]: `${oneYearAgoDate} 00:00:00.000`,
    //           [Op.lte]: `${currentDate} 23:59:59.999`,
    //         }
    //       },
    //       // {
    //       //   created_at: {
    //       //     [Op.between]: [oneYearAgoDate, currentDate],
    //       //   },
    //       // },
    //       {
    //         deletedAt: null,
    //       },
    //       vendorId,
    //     ],
    //   },
    //   group: [
    //     this.sequelize.fn('date_trunc', 'month', this.sequelize.col('Order.created_at')),
    //     // this.sequelize.col('branch.id') // Include the branch.id column in the GROUP BY clause
    //   ],
    //   raw: true,
    // });
    // const revenueByMonth = orders.map((order: { month: string; revenueAmount: string; }) => ({
    //   month: order.month, // The month value will be in the format 'YYYY-MM-DD'
    //   revenueAmount: Number(parseFloat(order.revenueAmount).toFixed(2)) || 0, // Convert revenue amount to a number
    // }));
    //return revenueByMonth;
    let query = admin_id
      ? `  with order_result AS ( SELECT
        (date_trunc('month', "o"."created_at")) AS "month",
          "o"."final_price",
          "o"."payout_amount",
          "c"."discount_type",
           "c"."discount_value",
      "o"."applied_coupon_id",
        "c"."provided_by" AS "provider",
             
          CAST(
          CASE WHEN "c"."provided_by" = 'SUPER_ADMIN' THEN
            (CASE
              WHEN "c"."discount_type" = 'percentage'
              THEN ("o"."final_price"::numeric(10,2) * "c"."discount_value"::numeric(10,2)  / 100)
           ELSE "c"."discount_value"
            END
           )
           WHEN "c"."provided_by" = 'VENDOR'
          THEN ("o"."payout_amount"::numeric(10,2)  - "o"."final_price"::numeric(10,2) )
           END
     
             AS INTEGER
         
          ) AS "discount_amount"
        FROM
          orders AS "o"
          left JOIN
          coupons AS "c"
        ON
          "o"."applied_coupon_id" = "c"."id"
        WHERE
          "o"."order_status" = '${APPOINTMENT_STATUS.COMPLETED}' AND "o"."deleted_at" IS null AND ("o"."created_at" >='${oneYearAgoDate} 00:00:00+05:30' AND "o"."created_at" <='${currentDate} 00:00:00+05:30')
           group by "month",
          "o"."final_price",
          "o"."payout_amount",
          "c"."discount_type",
           "c"."discount_value",
        "c"."provided_by",
    "o"."applied_coupon_id")
         
   SELECT
    "month",
    COALESCE(
      SUM(
        CASE
          WHEN "provider" = 'SUPER_ADMIN' AND "applied_coupon_id" IS NOT null
          THEN "payout_amount"::numeric(10,2)  - "discount_amount"::numeric(10,2)
          WHEN "provider" = 'VENDOR'AND "applied_coupon_id" IS NOT null
          THEN "discount_amount"::numeric(10,2)
      WHEN applied_coupon_id IS null THEN "payout_amount"::numeric(10,2)  - "final_price"::numeric(10,2)
        END
      )::NUMERIC(10,2), 0) AS "revenueAmount"
  FROM order_result
  GROUP BY "month" order by "month"`
      : `with branch_result AS(select "b"."branch_name","o"."payout_amount",(date_trunc('month',"o"."created_at")) AS "month" from "orders" AS "o"  left join branches As "b" ON "o"."branch_id" = "b"."id" where "b"."vendor_id" IN('${vendor_id}') AND "o"."order_status" = '${APPOINTMENT_STATUS.COMPLETED}' AND "o"."created_at" >= '${oneYearAgoDate} 00:00:00.000' AND  
      "o"."created_at" <= '${currentDate} 23:59:59.999')
select "month",sum(payout_amount)::NUMERIC(10,2) AS "revenueAmount" from branch_result group by "month" order by "month"`;

    let result = await Order.sequelize.query(query, { type: QueryTypes.SELECT });
    let data: any = result.length > 0 ? result : [{ month: `${oneYearAgoDate} 00:00:00`, revenueAmount: 0 }];
    // let revenueResult = await result.map(
    //   (order) => ({
    //     month: order.month,
    //     revenueAmount:
    //      Number (order.total_amount) -
    //      Number((order.slaylewks_total_comission + order.slaylewks_total_gst_amount + order.trasaction_commission_amounts) +
    //       order.total_coupon)
    //     }),

    // );
    let revenueResult = data.map((order) => ({
      month: order.month,
      revenueAmount: parseFloat(order.revenueAmount),
    }));
    return revenueResult;
  };
  async getRecentActivity(req_data: CreateDashboardDto) {
    let { admin_id, vendor_id, pagNo, limit } = req_data;
    const pageSize = limit || 2;
    const currentPage = pagNo || 1;
    const offset = (currentPage - 1) * pageSize;
    let branchID = [];
    let ids: any;
    const thirtyDaysAgo = moment().subtract(30, 'days').toDate(); // Calculate the date 30 days ago
    if (vendor_id) {
      let childVendor = await VendorRole.findOne({ attributes: ['vendor_id'], where: { id: vendor_id } });
      if (childVendor) vendor_id = childVendor?.vendor_id;
      let branchids = await Branch.findAll({
        attributes: ['id'],
        where: { vendor_id: vendor_id },
      });
      branchids.length > 0
        ? branchids.forEach((branchId) => {
          return branchId != null && branchID.push(`${branchId.id}`);
        })
        : [];
      ids = branchID && branchID.length > 0 ? { branch_id: branchID } : '';
      if (branchids.length == 0) {
        return {
          count: 0,
          rows: [],
        };
      }
      // branchID && branchID.length > 0
      //   ? `AND ${ORDER_ATTRIBUTES.branch_id} IN (${branchID})`
      //   : '';
    }
    //     const recentOrder = await Order.sequelize.query(
    //       ` SELECT
    //       ${ORDER_ATTRIBUTES.id},
    //     ${ORDER_ATTRIBUTES.order_id},
    //     ${ORDER_ATTRIBUTES.created_at},
    //     ${ORDER_ATTRIBUTES.updated_at},
    //   ${ORDER_ATTRIBUTES.customer_name} AS user_name,
    //   CASE
    //     WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.COMPLETED}' THEN
    //       'Completed recent order ' || "customer_name"
    //     WHEN  ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.CONFIRMED}' THEN
    //       'Confirmed recent order ' || "customer_name"
    //     WHEN  ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.PENDING}' THEN
    //       'Pending recent order ' || "customer_name"
    //       WHEN  ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.CANCELLED}' THEN
    //       'Cancelled recent order ' || "customer_name"
    //   END AS type,
    //   CASE
    //   WHEN  ${ORDER_ATTRIBUTES.order_status} =  '${APPOINTMENT_STATUS.COMPLETED}' THEN
    //     'Thank you for your purchase!'
    //   WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.CONFIRMED}' THEN
    //     'Your order is confirmed and will be processed soon.'
    //   WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.PENDING}' THEN
    //     'Your order is pending. Please wait for further updates.'
    //   WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.CANCELLED}' THEN
    //     'Unfortunately, your order has been cancelled.'
    // END AS message
    // FROM (
    //   SELECT
    //     *,
    //     ROW_NUMBER() OVER (PARTITION BY ${ORDER_ATTRIBUTES.order_status} ORDER BY  ${ORDER_ATTRIBUTES.created_at} DESC) AS rownum
    //   FROM ${ORDER_ATTRIBUTES.TABLE_NAME}
    //   WHERE
    //   (${ORDER_ATTRIBUTES.order_status} IN ('${APPOINTMENT_STATUS.COMPLETED}', '${APPOINTMENT_STATUS.CANCELLED}', '${APPOINTMENT_STATUS.CONFIRMED}', '${APPOINTMENT_STATUS.PENDING}')
    //  AND deleted_at IS NULL ${ids})
    // ) AS RankedOrders
    // WHERE
    //   "rownum" <= ${pageSize}  OFFSET ${offset}  LIMIT ${pageSize}`,
    //     );
    const recentOrder = await Order.findAndCountAll({
      attributes: [
        'id',
        'order_id',
        'created_at',
        'updated_at',
        [sequelize.literal(`${ORDER_ATTRIBUTES.customer_name}`), 'user_name'],
        [
          sequelize.literal(`CASE 
        WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.COMPLETED}' THEN 
          'Completed recent order ' || "customer_name"  
        WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.CONFIRMED}' THEN 
          'Confirmed recent order ' || "customer_name"
        WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.PENDING}' THEN 
          'Pending recent order ' || "customer_name"
        WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.CANCELLED}' THEN 
          'Cancelled recent order ' || "customer_name"
      END`),
          'type',
        ],
        [
          sequelize.literal(`CASE 
        WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.COMPLETED}' THEN 
          'Thank you for your purchase!'
        WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.CONFIRMED}' THEN 
          'Your order is confirmed and will be processed soon.'
        WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.PENDING}' THEN 
          'Your order is pending. Please wait for further updates.'
        WHEN ${ORDER_ATTRIBUTES.order_status} = '${APPOINTMENT_STATUS.CANCELLED}' THEN 
          'Unfortunately, your order has been cancelled.'
      END`),
          'message',
        ],
      ],
      where: {
        [Op.and]: [
          ids,
          {
            [ORDER_ATTRIBUTES.order_status]: [
              APPOINTMENT_STATUS.COMPLETED,
              APPOINTMENT_STATUS.CONFIRMED,
              APPOINTMENT_STATUS.PENDING,
              APPOINTMENT_STATUS.CANCELLED,
            ],
          },
          { deleted_at: null },
          { created_at: { [Op.gte]: thirtyDaysAgo } },
          // Add any other conditions you need
        ],
      },
      order: [[ORDER_ATTRIBUTES.created_at, 'DESC']],
      limit: pageSize,
      offset: offset,
    });
    let newUser: any;
    if (!vendor_id) {
      newUser = await User.findAndCountAll({
        attributes: [
          'id',
          'updated_at',
          'created_at',
          [Sequelize.literal(`CASE WHEN user_name IS NULL THEN 'New User' ELSE user_name END`), 'user_name'],
          [Sequelize.literal("'New User ' || user_name"), 'message'],
          [Sequelize.literal("'New Customers'"), 'type'],
        ],
        where: {
          [Op.and]: [
            { deleted_at: null },
            { created_at: { [Op.gte]: thirtyDaysAgo } },
            // Add any other conditions you need
          ],
        },
        order: [['created_at', 'DESC']],
        offset: offset,
        limit: pageSize,
      });
    }
    let defineModel: any = admin_id ? AdminUser : Vendor;
    let vendorName = vendor_id ? 'vendor_name' : 'name';
    let adminName = vendor_id ? ['vendor_name', 'user_name'] : ['name', 'user_name'];
    let vendorId: any = vendor_id ? { id: vendor_id } : '';
    let lastlogin = await defineModel.findAndCountAll({
      attributes: [
        'id',
        adminName,
        'last_login',
        'updated_at',
        'created_at',
        [Sequelize.literal(`'last logined ' || ${vendorName}`), 'message'],
        [Sequelize.literal("'Role Bassed User'"), `type`],
      ],
      where: {
        [Op.and]: [
          vendorId,
          { deleted_at: null },
          { last_login: { [Op.gte]: thirtyDaysAgo } },
          // Add any other conditions you need
        ],
      },
      order: [
        ['created_at', 'DESC'],
        ['last_login', 'DESC'],
      ],
      offset: offset,
      limit: pageSize,
    });
    return {
      count: (recentOrder?.count || 0) + (newUser?.count || 0) + (lastlogin?.count || 0),
      rows: [...(recentOrder?.rows || []), ...(newUser?.rows || []), ...(lastlogin?.rows || [])],
    };
  }
}
