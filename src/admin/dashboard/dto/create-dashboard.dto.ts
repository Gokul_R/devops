import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString, ValidateIf } from 'class-validator';
import { BRANCH_ID } from 'src/core/constants';
import { PaginationDto, SharedDto } from 'src/core/interfaces/shared.dto';

export class CreateDashboardDto extends PickType(PaginationDto, ['limit', 'pagNo']) {
  @ApiProperty({ example: BRANCH_ID })
  @ValidateIf((object) => !object.vendor_id) // Validate if vendor_id is not provided
  @IsNotEmpty({ message: 'Either admin_id or vendor_id is required' })
  admin_id: string;

  @ApiProperty({ example: BRANCH_ID })
  @ValidateIf((object) => !object.admin_id) // Validate if admin_id is not provided
  @IsNotEmpty({ message: 'Either admin_id or vendor_id is required' })
  vendor_id: string;
}
export class GetDashboardDto {
  @ApiProperty({ example: BRANCH_ID })
  @ValidateIf((object) => !object.vendor_id) // Validate if vendor_id is not provided
  @IsNotEmpty({ message: 'Either admin_id or vendor_id is required' })
  admin_id: string;

  @ApiProperty({ example: BRANCH_ID })
  @ValidateIf((object) => !object.admin_id) // Validate if admin_id is not provided
  @IsNotEmpty({ message: 'Either admin_id or vendor_id is required' })
  vendor_id: string;

  @ApiProperty({ example: '2023-08-01' })
  @ValidateIf((object) => object.to_date)
  @IsNotEmpty({ message: 'from_date is required' })
  @IsString()
  from_date: string;

  @ApiProperty({ example: '2023-08-30' })
  @ValidateIf((object) => object.from_date)
  @IsNotEmpty({ message: 'to_date is required' })
  @IsString()
  to_date: string;
}
