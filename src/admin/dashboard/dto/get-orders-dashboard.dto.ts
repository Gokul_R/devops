import { ApiProperty, PickType } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { APPOINTMENT_STATUS } from "src/core/database/models/Appointment";
import { CreateDashboardDto } from "./create-dashboard.dto";

export class GetDashBoardDto extends PickType(CreateDashboardDto, ['admin_id', 'vendor_id', 'pagNo']) {
    @IsString()
    @IsOptional()
    id: string;

    @ApiProperty({ example: 'All or Pending or Completed or Confirmed or Cancelled' })
    @IsString()
    @IsNotEmpty()
    @IsEnum({ ...APPOINTMENT_STATUS, ALL: "All" })
    orderType: string;

    @ApiProperty({ example: 5 })
    @IsNotEmpty()
    @IsNumber()
    limit: number;

}