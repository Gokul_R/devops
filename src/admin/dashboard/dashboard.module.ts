import { Module } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardController } from './dashboard.controller';
import { SEQUELIZE } from 'src/core/constants';
import { Sequelize } from 'sequelize';

@Module({
  controllers: [DashboardController],
  providers: [DashboardService, { provide: SEQUELIZE, useValue: Sequelize }]
})
export class DashboardModule { }
