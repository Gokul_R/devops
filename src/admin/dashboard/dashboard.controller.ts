import { Controller, Post, Body, Injectable } from '@nestjs/common';
import { DashboardService } from './dashboard.service';

import { ApiTags } from '@nestjs/swagger';
import { EC200, EM100, EM106 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { CreateDashboardDto, GetDashboardDto } from './dto/create-dashboard.dto';
import { GetDashBoardDto } from './dto/get-orders-dashboard.dto';

@Injectable()
@ApiTags('DashBoard')
@Controller('dashboard')
export class DashboardController {
  constructor(private readonly dashboardService: DashboardService) { }

  @Post()
  async getDashBoard(@Body() getDashBoard: GetDashboardDto) {
    try {
      let data = await this.dashboardService.getDashBoard(getDashBoard);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('orders')
  async getDashBoardOrders(@Body() getDashBoard: GetDashBoardDto) {
    try {
      let data = await this.dashboardService.getOrderData(getDashBoard);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('revenue')
  async getRevenueAmount(@Body() getDashBoard: GetDashboardDto) {
    try {
      let data = await this.dashboardService.getRevenueAmount(getDashBoard);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('/recent-activity/')
  async findAll(@Body() createDashboardDto: CreateDashboardDto) {
    try {
      let data = await this.dashboardService.getRecentActivity(createDashboardDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
