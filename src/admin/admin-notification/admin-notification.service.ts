import { Inject, Injectable, Logger } from '@nestjs/common';
import { CreateAdminNotificationDto, ScheduleTime } from './dto/create-admin-notification.dto';
import { UpdateAdminNotificationDto } from './dto/update-admin-notification.dto';
import AdminNotification from 'src/core/database/models/AdminNotification';
import { ModelCtor } from 'sequelize-typescript';
import {
  ADMIN_NOTIFICATION_REPOSITORY,
  NOTIFICATION_MARKETING_MAPPING,
  PRODUCTION,
  Slaylewks_details,
} from 'src/core/constants';
import NotificationMarketingMapping from 'src/core/database/models/NotificationMarketingMapping';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import MarketingGroup from 'src/core/database/models/MarketingGroup';
import { MARKETING_GROUP_ATTRIBUTES } from 'src/core/attributes/marketing_group';
import Helpers from 'src/core/utils/helpers';
import { BaseService } from 'src/base.service';
import { Op } from 'sequelize';
import NotificationConfig from 'src/core/database/models/NotificationConfig';
import { SendPushNotification } from 'src/core/utils/sendPushNotification';
import User from 'src/core/database/models/User';
import { logger } from 'src/core/utils/logger';
import UserNotification from 'src/core/database/models/UserNotification';
import ReferalManage from 'src/core/database/models/ReferalManage';

@Injectable()
export class AdminNotificationService extends BaseService<AdminNotification> {
  protected model: ModelCtor<AdminNotification>;
  constructor(
    @Inject(ADMIN_NOTIFICATION_REPOSITORY)
    private readonly adminNotificationRepo: typeof AdminNotification,
    @Inject(NOTIFICATION_MARKETING_MAPPING)
    private readonly NotificationMarketingRepo: typeof NotificationMarketingMapping,
  ) {
    super();
    this.model = this.adminNotificationRepo;
    // this.sendNotificationEvent();
  }
  // create(createAdminNotificationDto: CreateAdminNotificationDto) {
  //   return 'This action adds a new adminNotification';
  // }

  async create(
    createAdminNotificationDto: CreateAdminNotificationDto,
  ): Promise<any> {
    logger.info(`create_pushNotification_Entry: ` + JSON.stringify(createAdminNotificationDto));
    let { marketing_ids } = createAdminNotificationDto;
    let result: any;
    let adminNotification: AdminNotification = await super.create(
      createAdminNotificationDto,
    );
    if (marketing_ids?.length > 0) {
      await this.mappings(adminNotification.id, createAdminNotificationDto);
    }
    //  else {
    //   createAdminNotificationDto.marketing_ids = null;
    //   result = await super.create(createAdminNotificationDto);
    // }
    if (createAdminNotificationDto.scheduleTime == ScheduleTime.Now) {
      await this.sendNowNotification(createAdminNotificationDto);
      // super.remove(adminNotification.id);
    }
    logger.info(`create_pushNotification_Exit: ` + JSON.stringify(adminNotification));
    return adminNotification;
  }
  async sendNowNotification(AdminNotificationDto: CreateAdminNotificationDto) {
    if (AdminNotificationDto?.marketing_ids && AdminNotificationDto?.marketing_ids?.length > 0) {
      const getMarketingListUsersMobileOnly: any = await MarketingGroup.findAll({ attributes: ['users_mobile', 'is_referel', 'referal_code', 'referal_id'], where: { id: { [Op.in]: AdminNotificationDto?.marketing_ids } } });
      if (getMarketingListUsersMobileOnly && getMarketingListUsersMobileOnly?.length > 0) {
        let userReferralIds = getMarketingListUsersMobileOnly.filter(element => element?.is_referel).map((ele: any) => ele?.referal_id);
        let userMobile = [...getMarketingListUsersMobileOnly.map((element: any) => element?.users_mobile)].flat();
        if (userMobile && userMobile?.length > 0) this.sendPushNotificationToUserMobileNoVia(AdminNotificationDto, userMobile);
        if (userReferralIds && userReferralIds?.length > 0) this.sendPushNotificationToUserReferralVia(AdminNotificationDto, userReferralIds);
      }
    } else {
      this.sendPushNotificationToUserMobileNoVia(AdminNotificationDto, null);
    }
  }

  async sendPushNotificationToUserReferralVia(createAdminNotificationDto: CreateAdminNotificationDto, referralCode: any) {
    logger.info(`Send_Push_notification_referral_entry: ${JSON.stringify(createAdminNotificationDto)}, phones: ${referralCode}`);
    let getReferralIds = await ReferalManage.findAll({ attributes: ['referal_code'], where: { id: { [Op.in]: referralCode } } });
    let getRefCode: any = [...getReferralIds.map((element: any) => element?.referal_code)].flat();
    getRefCode = getRefCode && getRefCode?.length > 0 ? {
      where: { invited_ref_code: { [Op.in]: getRefCode }, [Op.and]: { device_token: { [Op.not]: null } } }
    } : {};
    let getReferredUser = await User.findAll({
      attributes: ['device_token', 'id'], ...getRefCode
    });
    this.preparePushNotificationContent(createAdminNotificationDto, getReferredUser);
  }

  async sendPushNotificationToUserMobileNoVia(createAdminNotificationDto: CreateAdminNotificationDto, ifMarketing: any) {
    logger.info(`Send_Push_notification_userMobile_entry: ${JSON.stringify(createAdminNotificationDto)}, phones: ${ifMarketing}`);
    ifMarketing = ifMarketing && ifMarketing?.length > 0 ? {
      where: { mobile_no: { [Op.in]: ifMarketing }, [Op.and]: { device_token: { [Op.not]: null } } }
    } : {};
    const getUsersList = await User.findAll({
      attributes: ['device_token', 'id'], ...ifMarketing
    });
    this.preparePushNotificationContent(createAdminNotificationDto, getUsersList);
  }

  async preparePushNotificationContent(adminNotificationDto: any, usersTokenList: any) {
    if (usersTokenList && usersTokenList?.length > 0) {
      this.putUserNotification(adminNotificationDto, usersTokenList); //create users notification records
      let userToken = usersTokenList.reduce((tokens: any, res: any) => {
        if (res?.dataValues?.device_token !== undefined && res?.dataValues?.device_token !== null && res?.dataValues?.device_token !== '') {
          tokens.push(res?.dataValues?.device_token);
        }
        return tokens;
      }, []);
      if (userToken?.length <= 0) return;
      logger.info(`Send_Push_notification_users_count_entry: ${userToken?.length}`);
      let requestObj = {
        // to: userToken,
        registration_ids: userToken,
        notification: {
          title: adminNotificationDto?.title || 'Hi slaylewks users!',
          body: adminNotificationDto?.description || 'Hi slaylewks users!',
          // image: adminNotificationDto.image
          image: adminNotificationDto?.image ? `${Slaylewks_details?.s3base_url}${process.env.NODE_ENV != PRODUCTION ? 'dev/' : 'prod/'}${adminNotificationDto?.image}` : `${Slaylewks_details?.logo}`,
        },
      };
      if (userToken?.length > 1000) {
        let origionalLength = Number((userToken?.length / 1000).toFixed());
        for (let i = 0; i <= origionalLength; i++) {
          let splited = userToken.splice(0, 1000);
          if (splited.length <= 0) return;
          requestObj.registration_ids = splited;
          await new SendPushNotification().sendNotification(requestObj);
        }
      } else {
        await new SendPushNotification().sendNotification(requestObj);
      }
    }
  }
  async putUserNotification(AdminNotificationDto: CreateAdminNotificationDto, usersList: any) {
    // let dataDto = [];
    // const usersData = usersList?.filter((data: any) => {
    //   return dataDto.push({
    //     title: AdminNotificationDto?.title,
    //     description: AdminNotificationDto?.description,
    //     image: AdminNotificationDto?.image,
    //     user_id: data?.dataValues?.id
    //   });
    // });
    // const createUserNotification = Helpers.bulkCreate(UserNotification, dataDto);
    let dataDto = {
      title: AdminNotificationDto?.title,
      description: AdminNotificationDto?.description,
      image: AdminNotificationDto?.image,
      user_id: usersList?.map((data: any) => { return data?.dataValues?.id; })
    };
    // console.log(dataDto);
    const createUserNotification = Helpers.create(UserNotification, dataDto);
  }

  async findOne(admin_notification_id?: any): Promise<any> {
    let qry: SequelizeFilter<AdminNotification> = admin_notification_id
      ? {
        where: { id: admin_notification_id },
        include: [
          {
            attributes: [
              MARKETING_GROUP_ATTRIBUTES.ID,
              MARKETING_GROUP_ATTRIBUTES.GROUP_NAME,
            ],
            model: MarketingGroup,
          },
        ],
      }
      : {};
    return await super.findOne(null, qry);
  }
  async getFilterList(req_data: any) {
    let search: any;
    if (req_data?.searchText) {
      const searchText = req_data?.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { title: { [Op.iLike]: `%${searchText}%` } },
          { description: { [Op.iLike]: `%${searchText}%` } },
          // { '$marketing.group_name$': { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await this.findAndCountAll({
      where: {
        ...search,
        is_deleted: false
      },
      include: [{
        attributes: [
          MARKETING_GROUP_ATTRIBUTES.ID,
          MARKETING_GROUP_ATTRIBUTES.GROUP_NAME,
        ],
        model: MarketingGroup,
        // required: true
      }],
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']]
    });
  }

  async findAllData(): Promise<any> {
    let data = super.findAll({
      include: [
        {
          // attributes: [
          //   MARKETING_GROUP_ATTRIBUTES.ID,
          //   MARKETING_GROUP_ATTRIBUTES.GROUP_NAME,
          // ],
          model: MarketingGroup,
        },
      ],
    });
    return data;
  }

  async update(id: string, data: UpdateAdminNotificationDto): Promise<any> {
    let adminNotification: AdminNotification = await super.update(id, data);
    let qry = { where: { admin_notification_id: id } };
    let deleteExitMap = await this.NotificationMarketingRepo.destroy({ force: true, ...qry });
    if (data.marketing_ids?.length > 0) await this.mappings(id, data);
    return adminNotification;
  };
  //marketing ids mapping.....
  async mappings(
    adminNotification_id: string,
    data: UpdateAdminNotificationDto,
  ) {
    let { marketing_ids } = data;
    let notificationMarketingObj = marketing_ids.map((marketing_id) => {
      return {
        admin_notification_id: adminNotification_id,
        marketing_id: marketing_id,
      };
    });
    await this.NotificationMarketingRepo.bulkCreate(notificationMarketingObj);
  }

  async laterNotification() {
    logger.info('later_notification_triggered:_');
    const getLaterData = await AdminNotification.findAll({
      where: {
        trigger_time: {
          [Op.gte]: new Date().getTime() - 120000, //-2m // Find records with timestamps before 2 minutes ago
          [Op.lte]: new Date().getTime() + 120000, //+2m // Find records with timestamps after 2 minutes from now
        },
        is_active: true,
        scheduleTime: ScheduleTime.Later,
        later_sent: false
      }
    });
    if (!getLaterData || getLaterData.length == 0) return;
    logger.info('later_notification_send_length:_' + JSON.stringify(getLaterData.length));
    for (let index = 0; index < getLaterData.length; index++) {
      let element = getLaterData[index];
      let sendNotify = await this.sendNowNotification(element);
      let updateSentStatus = AdminNotification.update({ later_sent: true }, { where: { id: element.id } });
    }
    return 'executed';
  }


}
