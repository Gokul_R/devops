import { Module } from '@nestjs/common';
import { AdminNotificationService } from './admin-notification.service';
import { AdminNotificationController } from './admin-notification.controller';
import { adminNotificationProviders } from './admin-notification.providers.';

@Module({
  controllers: [AdminNotificationController],
  providers: [AdminNotificationService,...adminNotificationProviders]
})
export class AdminNotificationModule {}
