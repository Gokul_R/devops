import { commonProvider } from "src/core/interfaces/common-providers";



export const adminNotificationProviders = [
  // {
  //   provide: ADMIN_NOTIFICATION_REPOSITORY,
  //   useValue: AdminNotification,
  // },
 
  commonProvider.admin_notification_repository,
  commonProvider.notification_marketing_mapping
];
