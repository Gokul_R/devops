import { ApiProperty, PickType } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsBoolean,
  IsDate,
  IsDateString,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsTimeZone,
  Length,
  Matches,
  ValidateIf,
  isTimeZone,
} from 'class-validator';
import { SharedDto } from 'src/core/interfaces/shared.dto';
export enum ScheduleTime {
  Now = 'Now',
  Later = 'Later',
}
export class CreateAdminNotificationDto extends PickType(SharedDto, ['marketing_ids']) {
  @IsString()
  @IsOptional()
  id?: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: 'Lorem Ipsum' })
  @IsString()
  @Length(3, 100, {
    message: 'title must be between 3 and 100 characters',
  })
  // @Matches(/^[A-Za-z0-9]+$/, {
  //   message: ' title Only alphabets and numbers are allowed',
  // })
  title?: string;

  // @ApiProperty({ example: 'Lorem Ipsum' })
  // @IsString()
  // marketing_group: string;

  @ApiProperty({ example: 'Lorem Ipsum' })
  @IsString()
  @Length(4, 250, {
    message: 'Description must be between 4 and 250 characters',
  })
  @IsOptional()
  description?: string;

  // @ApiProperty({ example: new Date() })
  // @Transform(({ value }) => new Date(value))
  // @IsNotEmpty()
  // @IsDate()
  // date?: Date;

  @ApiProperty({ example: 'Now (or) Later' })
  @IsNotEmpty()
  @IsString()
  @IsEnum(ScheduleTime, {
    message: 'scheduleTime must be Now or Later',
  })
  scheduleTime?: string;

  @ApiProperty({ example: '1705041579741' })
  @ValidateIf((o) => o.scheduleTime === ScheduleTime.Later)
  @IsNotEmpty()
  @IsNumber()
  trigger_time: number;

  // @ApiProperty({ example: '10:00 AM' })
  // @ValidateIf((o) => o.scheduleTime === ScheduleTime.Later) // Validate 'time' only if 'scheduleTime' is 'Later'
  // @Matches(/^((0?[1-9]|1[0-2]):[0-5]\d\s([APap][Mm]))$/, {
  //   message: 'Invalid time format. Please use HH:mm AM/PM with 12 hours format.',
  // })
  // @IsString()
  // time?: string;

  // @ApiProperty({ example: new Date() })
  // @ValidateIf((o) => o.scheduleTime === ScheduleTime.Later) // Validate 'date' only if 'scheduleTime' is 'Later'
  // @Transform(({ value }) => value ? new Date(value) : null)
  // @IsNotEmpty()
  // @IsDate()
  // date?: Date;

  @ApiProperty({ example: ' https//17.54.23:4000/235.img' })
  @IsString()
  @IsOptional()
  image?: string | null;

  // @ApiProperty({ default:false })
  @IsBoolean()
  @IsOptional()
  is_deleted?: boolean;

  //   const dateStr = '2023-07-05';   // Date in format: YYYY-MM-DD
  // const timeStr = '12:30 AM';    // Time in format: HH:mm AM/PM

  // // Convert timeStr to 24-hour format
  // const time24Hr = new Date(`2000-01-01 ${timeStr}`).toISOString().slice(11, 19);

  // // Concatenate date and time strings
  // const dateTimeStr = `${dateStr}T${time24Hr}.000Z`;

  // // Create a Date object from the concatenated string
  // const timestamp = new Date(dateTimeStr).getTime();

  // console.log(timestamp);
}
