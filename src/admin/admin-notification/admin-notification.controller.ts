import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';
import { AdminNotificationService } from './admin-notification.service';
import { CreateAdminNotificationDto } from './dto/create-admin-notification.dto';
import { UpdateAdminNotificationDto } from './dto/update-admin-notification.dto';
import { BaseController } from '../../base.controller';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { ApiTags } from '@nestjs/swagger';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@ApiTags('Notification')
@Controller('admin-notification')
export class AdminNotificationController extends BaseController<CreateAdminNotificationDto> {
  constructor(
    private readonly adminNotificationService: AdminNotificationService,
  ) {
    super(adminNotificationService);
  }

  @Post()
  async create(@Body() createAdminNotificationDto: CreateAdminNotificationDto) {
    try {
      let data = await this.adminNotificationService.create(
        createAdminNotificationDto
      );
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get('later-notify')
  triggerLaterNotification() {
    try {
      let data = this.adminNotificationService.laterNotification();
      return HandleResponse.buildSuccessObj(EC200, EM106, null);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }

  }

  @Get()
  async findAll() {
    try {
      let data = await this.adminNotificationService.findAllData();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }

  }
  @Post('search')
  async getFilterList(@Body() getadminNotificationListedto: PaginationDto) {
    try {
      let data = await this.adminNotificationService.getFilterList(getadminNotificationListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Get(':id')
  async findOne(@Param('id') id: string) {
    {
      try {
        let data = await this.adminNotificationService.findOne(id);
        return HandleResponse.buildSuccessObj(EC200, EM106, data);
      } catch (error) {
        return HandleResponse.buildErrObj(null, EM100, error);
      }
      //   return this.adminNotificationService.findOne(+id);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateAdminNotificationDto: UpdateAdminNotificationDto,
  ) {
    try {
      let data = await this.adminNotificationService.update(
        id,
        updateAdminNotificationDto
      );
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.adminNotificationService.remove(+id);
  // }
}
