import { Module } from '@nestjs/common';
import { BranchGalleryFolderService } from './branch-gallery-folder.service';
import { BranchGalleryFolderController } from './branch-gallery-folder.controller';
import { branchGalleryFolderProviders } from './branch-gallery-folder.provider';

@Module({
  controllers: [BranchGalleryFolderController],
  providers: [BranchGalleryFolderService,...branchGalleryFolderProviders]
})
export class BranchGalleryFolderModule {}
