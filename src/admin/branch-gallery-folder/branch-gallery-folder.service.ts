import { Inject, Injectable } from '@nestjs/common';
import BranchGalleryFolder from 'src/core/database/models/BranchGalleryFolder';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import {
  BRANCH_GALLERY_REPOSITORY,
  BRANCH_GALLERY_FOLDER_REPOSITORY,
} from 'src/core/constants';
import BranchGallery from 'src/core/database/models/BranchGallery';

@Injectable()
export class BranchGalleryFolderService extends BaseService<BranchGalleryFolder> {
  protected model: ModelCtor<BranchGalleryFolder>;
  folder_qry: SequelizeFilter<BranchGalleryFolder>;
  constructor(
    @Inject(BRANCH_GALLERY_REPOSITORY)
    private readonly branchGalleryRepo: typeof BranchGallery,
    @Inject(BRANCH_GALLERY_FOLDER_REPOSITORY)
    private readonly branchGalleryFolderRepo: typeof BranchGalleryFolder,
  ) {
    super();
    this.model = BranchGalleryFolder;
    this.folder_qry = {
      include: [
        {
          attribute: ['gallery_list'],
          model: this.branchGalleryRepo,
        },
      ],
      where: { is_deleted: false },
    };
  }
  // create(createBranchGalleryFolderDto: CreateBranchGalleryFolderDto) {
  //   return 'This action adds a new branchGalleryFolder';
  // }

  // update(id: number, updateBranchGalleryFolderDto: UpdateBranchGalleryFolderDto) {
  //   return `This action updates a #${id} branchGalleryFolder`;
  // }

  async findAllByBranch(branch_id: string): Promise<any> {
    this.folder_qry.where = {
      ...this.folder_qry.where,
      branch_id: branch_id,
    };
    return await super.findAll(this.folder_qry);
  }
}
