import { Test, TestingModule } from '@nestjs/testing';
import { BranchGalleryFolderController } from './branch-gallery-folder.controller';
import { BranchGalleryFolderService } from './branch-gallery-folder.service';

describe('BranchGalleryFolderController', () => {
  let controller: BranchGalleryFolderController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchGalleryFolderController],
      providers: [BranchGalleryFolderService],
    }).compile();

    controller = module.get<BranchGalleryFolderController>(BranchGalleryFolderController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
