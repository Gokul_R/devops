import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { SharedDto } from 'src/core/interfaces/shared.dto';

export class CreateBranchGalleryFolderDto extends PickType(SharedDto, [
  'branch_id',

]) {
  @ApiProperty()
  @IsString()
  folder_name: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

}
