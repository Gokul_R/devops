import { PartialType } from '@nestjs/mapped-types';
import { CreateBranchGalleryFolderDto } from './create-branch-gallery-folder.dto';
import { OmitType } from '@nestjs/swagger';

export class UpdateBranchGalleryFolderDto extends OmitType(CreateBranchGalleryFolderDto,['branch_id']) {}
