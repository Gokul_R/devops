import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { BranchGalleryFolderService } from './branch-gallery-folder.service';
import { CreateBranchGalleryFolderDto } from './dto/create-branch-gallery-folder.dto';
import { UpdateBranchGalleryFolderDto } from './dto/update-branch-gallery-folder.dto';
import { CreateBranchGalleryDto } from '../branch-gallery/dto/create-branch-gallery.dto';
import { BaseController } from '../../base.controller';
import { ApiTags } from '@nestjs/swagger';
import { EC200, EM106, EM100, EM104, EM116 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';

@ApiTags('Branch Gallery Folder')
@Controller('branch-gallery-folder')
export class BranchGalleryFolderController extends BaseController<CreateBranchGalleryFolderDto> {
  constructor(
    private readonly branchGalleryFolderService: BranchGalleryFolderService,
  ) {
    super(branchGalleryFolderService);
  }

  @Post()
  async create(
    @Body() createBranchGalleryFolderDto: CreateBranchGalleryFolderDto,
  ) {
    try {
      let data = await this.branchGalleryFolderService.create(
        createBranchGalleryFolderDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get()
  // findAll() {
  //   return this.branchGalleryFolderService.findAll();
  // }
  @Get('by-branch/:branch_id')
  async findAllByBranch(@Param('branch_id') branch_id: string) {
    try {
      let data = await this.branchGalleryFolderService.findAllByBranch(
        branch_id,
      );
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.branchGalleryFolderService.findOne(+id);
  // }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBranchGalleryFolderDto: UpdateBranchGalleryFolderDto,
  ) {
    try {
      let data = await this.branchGalleryFolderService.update(
        id,
        updateBranchGalleryFolderDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.branchGalleryFolderService.remove(+id);
  // }
}
