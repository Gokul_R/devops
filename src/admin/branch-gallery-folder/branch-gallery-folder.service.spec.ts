import { Test, TestingModule } from '@nestjs/testing';
import { BranchGalleryFolderService } from './branch-gallery-folder.service';

describe('BranchGalleryFolderService', () => {
  let service: BranchGalleryFolderService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BranchGalleryFolderService],
    }).compile();

    service = module.get<BranchGalleryFolderService>(BranchGalleryFolderService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
