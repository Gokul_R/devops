import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsString } from "class-validator";
import { BRANCH_ID, ORDER_ID, Provided_By } from "src/core/constants";

export class OrderCompletedDto {
    @ApiProperty({ example: ORDER_ID })
    @IsNotEmpty()
    @IsString()
    order_id: string;

    @ApiProperty({ example: `${Provided_By.SUPER_ADMIN} 'or' ${Provided_By.VENDOR} or ${Provided_By.BRANCH}` })
    @IsNotEmpty()
    @IsEnum(Provided_By)
    order_approved_by: string;

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    @IsString()
    order_approved_id: string;

    @ApiProperty({ example: '5270' })
    @IsNotEmpty()
    @IsString()
    order_otp: string;

}

export class OrderApprovalDto {
    @ApiProperty({ example: ORDER_ID })
    @IsNotEmpty()
    @IsString()
    order_id: string;

    @ApiProperty({ example: `${Provided_By.SUPER_ADMIN} 'or' ${Provided_By.VENDOR} or ${Provided_By.BRANCH}` })
    @IsNotEmpty()
    @IsEnum(Provided_By)
    order_approved_by: string;

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    @IsString()
    order_approved_id: string;

    order_status: string;
}