import {
  IsString,
  IsEmail,
  IsOptional,
  IsBoolean,
  IsNotEmpty,
  Min,
  Max,
  IsMobilePhone,
  Matches,
  IsUppercase,
  Length,
  MaxLength,
  MinLength,
  IsPhoneNumber,
  IsUrl,
  IsNumber,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { DEFAULT_VALUE } from 'src/core/swaggerDefaultValue/swaggerValue';
import { ValidateIf } from 'nestjs-class-validator';
import { Approval } from 'src/core/constants';
import { IsLowercase } from 'class-validator';
import { VendorType } from 'src/core/utils/enum';

export class CreateVendorDto {
  // @IsBoolean()
  // @IsOptional()
  // is_active?: boolean;

  @ApiProperty({ example: DEFAULT_VALUE.NAME })
  @IsString()
  @Length(3, 100, {
    message: 'vendor_name must be between 3 and 100 characters',
  })
  // @Matches(/^[A-Za-z\s]+$/, {
  //   message: 'Only letters and allowed',
  // })
  @IsNotEmpty()
  vendor_name: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  manual_register: boolean;

  @ApiProperty({ example: DEFAULT_VALUE.EMAIL })
  @IsEmail()
  @IsLowercase()
  @IsNotEmpty()
  email_id: string;

  @ApiProperty({ example: DEFAULT_VALUE.MOBILE_NO })
  @IsString()
  // @IsPhoneNumber() 
  @IsNotEmpty()
  mobile_no: string;

  @ApiProperty({ example: DEFAULT_VALUE.ADDRESS })
  @IsString()
  @IsNotEmpty()
  // @Matches(/^[a-zA-Z0-9\s-]+$/)
  address: string;

  @ApiProperty()
  @IsNotEmpty()
  // @Matches(/^[a-zA-Z]+$/)
  @IsString()
  city: string;

  @ApiProperty()
  //@IsOptional()
  @IsString()
  @IsNotEmpty()
  // @Matches(/^[a-zA-Z0-9]+$/)
  area: string;

  @ApiProperty()
  @IsNotEmpty()
  // @Matches(/^[a-zA-Z]+$/)
  @IsString()
  state: string;

  @ApiProperty()
  @MaxLength(6)
  @MinLength(6)
  @Matches(/^[0-9]+$/)
  @IsString()
  pincode: string;

  @ApiProperty()
  @IsNotEmpty()
  // @Matches(/^[a-zA-Z]+$/)
  @IsString()
  country: string;

  @IsBoolean()
  @IsOptional()
  is_approved: boolean;

  @IsNumber()
  @IsOptional()
  approval_status: number;

  @ApiProperty({ example: DEFAULT_VALUE.VENDOR_TYPE })
  @IsNotEmpty()
  @IsString()
  vendor_type: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  profile_url: string;

  @ApiProperty()
  @IsUppercase()
  @Length(10)
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @Matches(/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/, {
    message: 'Invalid PAN card number format.',
  })
  @ValidateIf((o) => !o?.manual_register)
  @IsString()
  pancard_num: string;

  @ApiProperty()
  @IsNotEmpty()
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @ValidateIf((o) => !o?.manual_register)
  @IsString()
  pancard_url: string;

  @ApiProperty()
  @IsNotEmpty()
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @ValidateIf((o) => !o?.manual_register)
  @IsString()
  @MaxLength(12)
  @MinLength(12)
  aadharcard_num: string;

  @ApiProperty()
  @IsNotEmpty()
  @ValidateIf((o) => o.vendor_type == VendorType.BUSINESS)
  @ValidateIf((o) => !o?.manual_register)
  @IsString()
  aadharcard_url: string;

}
