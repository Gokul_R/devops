import { ApiProperty, OmitType, PartialType, PickType } from '@nestjs/swagger';
import { CreateVendorDto } from './create-vendor.dto';
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { BRANCH_ID } from 'src/core/constants';
import { CreateFreelancerDto } from 'src/admin/freelancer/dto/create-freelancer.dto';

export class UpdateVendorDto extends OmitType(CreateVendorDto,['is_approved','approval_status']) {
    // @ApiProperty({ example: 0 })
    // @IsNumber()
    // @IsOptional()
    // approval_status: number;

    @ApiProperty({ example: 'Some remarks text' })
    @IsString()
    @IsOptional()
    remarks: string;

    @ApiProperty({ example: true })
    @IsBoolean()
    @IsOptional()
    is_active: boolean;
}


export class UpdateVendorRolesDto {
    @ApiProperty()
    user_type: string;

    @ApiProperty()
    name: string;

    @ApiProperty()
    employee_id: string;

    @ApiProperty()
    email_id: string;

    @ApiProperty()
    image: string;

    @ApiProperty()
    mobile_no: string;

    @ApiProperty()
    branch_ids: string[];

    @ApiProperty()
    role_id: string;

    @ApiProperty()
    vendor_id: string;



}

export class logoutDto {
    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    @IsString()
    user_id: string;
}
