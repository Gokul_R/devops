import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class VendorEnquiryDto {
  @ApiProperty({ example: 'manoj' })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({ example: 'manoj@mailinator.com' })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty({ example: '9024564345' })
  @IsNotEmpty()
  @IsString()
  phone_number: string;
}
