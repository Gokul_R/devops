import { Controller, Get, Post, Body, Patch, Param, Delete, ValidationPipe, UsePipes, Query } from '@nestjs/common';
import { VendorService } from './vendor.service';
import { CreateVendorDto } from './dto/create-vendor.dto';
import { UpdateVendorDto, UpdateVendorRolesDto, logoutDto } from './dto/update-vendor.dto';
import { ApiBody, ApiQuery, ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import {
  EC200,
  EC409,
  EM100,
  EM102,
  EM104,
  EM106,
  EM116,
  EM127,
  EM126,
  EM136,
  EM141,
  EC204,
  EM101,
  EM149,
  EC500,
} from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { getBranchListedto } from '../branch/dto/get-service-dto';
import { OrderApprovalDto, OrderCompletedDto } from './dto/order-completed.dto';
import { MailUtils } from 'src/core/utils/mailUtils';
import { VendorEnquiryDto } from './dto/enquiry-vendor.dto';
import Vendor from 'src/core/database/models/Vendor';
import VendorRole from 'src/core/database/models/VendorRoles';
import { VendorType } from 'src/core/utils/enum';
@ApiTags('Vendor')
@Controller('vendor')
export class VendorController {
  constructor(private readonly vendorService: VendorService, private readonly MailUtilsService: MailUtils) { }

  @Post()
  async createVendor(@Body() vendorDetails: CreateVendorDto) {
    try {
      let existVendor = await Vendor.count({ where: { email_id: vendorDetails?.email_id } });
      if (!existVendor) existVendor = await VendorRole.count({ where: { email_id: vendorDetails?.email_id } });
      if (existVendor) return { code: EC409, status: false, message: EM102, data: null };

      vendorDetails.approval_status = 1;
      vendorDetails.is_approved = true;
      let data = await this.vendorService.create(vendorDetails);
      // this.vendorService.sendPwdToVendor(data?.email_id);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get()
  @ApiQuery({
    name: 'status',
    type: String,
    description: 'Approval Status(0 or 1 or 2)',
    required: false,
  })
  @ApiQuery({
    name: 'vendor_type',
    type: String,
    description: `${VendorType.BUSINESS} (or) ${VendorType.FREELANCER}`,
    required: true,
  })
  async getVendorList(@Query('status') status?: number, @Query('vendor_type') vendor_type?: string) {
    try {
      let data = await this.vendorService.findAllVendor(status, vendor_type);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.vendorService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':email_id')
  async getCartItems(@Param('email_id') id: string) {
    try {
      let data = await this.vendorService.findByEmail(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async updatevendor(@Param('id') id: string, @Body() UpdateVendorDto: UpdateVendorDto) {
    try {
      // let data = await this.vendorService.update(id, UpdateVendorDto);
      let data = await this.vendorService.updatevendor(id, UpdateVendorDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let checkIfUserExit: any = await Vendor.findOne({ where: { id: id } });
      checkIfUserExit = checkIfUserExit ? checkIfUserExit : await VendorRole.findOne({ where: { id: id } });
      if (!checkIfUserExit) return { code: EC204, status: false, message: EM101, data: null };
      let data = await this.vendorService.deletedVendor(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  // @Delete('hard-delete/:id')
  // async hardDelete(@Param('id') id: string) {
  //   try {
  //     let checkIfUserExit = await Vendor.findOne({ where: { id: id } });
  //     if (!checkIfUserExit) return { code: EC204, status: false, message: EM101, data: null };
  //     let data = await this.vendorService.hardDeletedVendor(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM127, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Post('search')
  async getFilterList(@Body() getBranchListedto: getBranchListedto) {
    try {
      let data = await this.vendorService.getFilterList(getBranchListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('order-completed')
  async orderCompleted(@Body() orderCompleteddto: OrderCompletedDto) {
    try {
      let data = await this.vendorService.orderCompleted(orderCompleteddto);
      if (data && data?.code) return HandleResponse.buildErrObj(data.code, data.message, data.message);
      else return HandleResponse.buildSuccessObj(EC200, EM136, data);
    } catch (error) {
      return HandleResponse.buildErrObj(
        error?.response?.status || null,
        EM100,
        error?.response?.data?.error?.description || error,
      );
    }
  }

  @Post('enquiry')
  async vendorEnquiry(@Body() vendorEnquiryDto: VendorEnquiryDto) {
    try {
      let data = await this.vendorService.vendorEnquiry(vendorEnquiryDto);
      return HandleResponse.buildSuccessObj(EC200, EM141, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }


  @Post('get-request-vendor')
  async requestVendor(@Body() paginationDto: PaginationDto) {
    try {
      let data = await this.vendorService.requestVendor(paginationDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('logout')
  async logout(@Body() logoutDto: logoutDto) {
    try {
      let data = await this.vendorService.logout(logoutDto);
      return HandleResponse.buildSuccessObj(EC200, EM149, null);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500 || error.status, error?.message || EM100, error);
    }
  }

  @Post('order-approval')
  async orderApproval(@Body() req_body: OrderApprovalDto) {
    try {
      let data = await this.vendorService.orderApproval(req_body);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500 || error.status, error?.message || EM100, error);
    }
  }




  @Patch('vendor_roles/:id')
  async updatevendorRole(@Param('id') id: string, @Body() UpdateVendorDto: any) {
    try {
      let data = await this.vendorService.updatevendorRole(id, UpdateVendorDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

}
