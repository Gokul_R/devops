import { Injectable, NotFoundException } from '@nestjs/common';
import Helpers from 'src/core/utils/helpers';
import { Approval, EC200, EC400, EC410, EM106, EM109, EM119, EM152, NotificationType, PRODUCTION, Slaylewks_details, TriggerType, notificationTypeConfig } from 'src/core/constants';
import Vendor from 'src/core/database/models/Vendor';
import { BaseService } from '../../base.service';
import { ModelCtor, Sequelize } from 'sequelize-typescript';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import EmailTemService from 'src/core/utils/emailTemplate';
import { MailOptions, MailUtils } from 'src/core/utils/mailUtils';
import { col, fn } from 'sequelize';
import { Op } from 'sequelize';
import { OrderApprovalDto, OrderCompletedDto } from './dto/order-completed.dto';
import Order from 'src/core/database/models/Order';
import Appointment, { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import OrderService from 'src/core/database/models/OrderServices';
import Service from 'src/core/database/models/Services';
import Branch from 'src/core/database/models/Branch';
import Location from 'src/core/database/models/Location';
import Coupon from 'src/core/database/models/Coupons';
import { PayoutPaymentService } from '../payout/payout.payment.service';
import FlashSale from 'src/core/database/models/FlashSale';
import { FlashSaleConstants } from 'src/modules/flash-sale/flashsale-constants';
import { ORDER_ATTRIBUTES } from 'src/core/attributes/order';
import { logger } from 'src/core/utils/logger';
import OrderRemainder from 'src/core/database/models/OrderRemainder';
import BranchCategoryMapping from 'src/core/database/models/BranchCategoryMapping';
import BranchServiceCategoryMapping from 'src/core/database/models/BranchServiceCategoryMapping.ts';
import SlotDetail from 'src/core/database/models/SlotDetail';
import Favourites from 'src/core/database/models/Favourites';
import CouponBranchMapping from 'src/core/database/models/CouponBranchMapping';
import BranchBankDetails from 'src/core/database/models/BranchBankDetails';
import BranchGallery from 'src/core/database/models/BranchGallery';
import BranchGalleryFolder from 'src/core/database/models/BranchGalleryFolder';
import BranchServiceTypeMapping from 'src/core/database/models/BranchServiceTypeMapping';
import Cart from 'src/core/database/models/Cart';
import SlotManagement from 'src/core/database/models/SlotManagement';
import { VendorEnquiryDto } from './dto/enquiry-vendor.dto';
import VendorEnquiry from 'src/core/database/models/VendorEnquiry';
import { logoutDto } from './dto/update-vendor.dto';
import { DateUtils } from 'src/core/utils/dateUtils';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import User from 'src/core/database/models/User';
import { SendPushNotification } from 'src/core/utils/sendPushNotification';
import VendorRole from 'src/core/database/models/VendorRoles';
import { VendorType } from 'src/core/utils/enum';
import { getBranchListedto } from '../branch/dto/get-service-dto';
import Encryption from 'src/core/utils/encryption';
import AppliedCoupon from 'src/core/database/models/AppliedCoupons';
import { Errors } from 'src/core/constants/error_enums';
import { MASTER_CATEGORIES_ATTRIBUTES } from 'src/core/attributes/master_category';
import MasterCategory from 'src/core/database/models/MasterCategory';
import UserAddress from 'src/core/database/models/UserAddress';
import Payment_Txn from 'src/core/database/models/Payment-Tnx';
import { LOCATION_ATTRIBUTES } from 'src/core/attributes/location';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { CreateNotificationDto } from 'src/modules/notification/dto/create-notification.dto';
import { NotificationService } from 'src/modules/notification/notification.service';
import { APPOINTMENT_ATTRIBUTES } from 'src/core/attributes/appointment';
import { SERVICE_ATTRIBUTES } from 'src/core/attributes/services';

@Injectable()
export class VendorService extends BaseService<Vendor> {
  protected model: ModelCtor<Vendor> = Vendor;
  vendors_qry: SequelizeFilter<Vendor>;
  order: Order;

  constructor(private readonly MailUtilsService: MailUtils) {
    super();
  }
  init() {
    this.vendors_qry = {
      where: {
        approval_status: Approval.ACCEPTED,
      },
    };
  }

  async findByEmail(email: string) {
    this.init();
    this.vendors_qry.where = {
      email_id: email,
      deleted_at: null,
      is_active: true,
      is_deleted: false,
      approval_status: Approval.ACCEPTED,
    };
    return await super.findAll(this.vendors_qry);
  }
  async findAllVendor(status: any, vendor_type: string): Promise<any> {
    this.init();
    let vendorType = vendor_type ? { vendor_type: vendor_type } : {};
    this.vendors_qry.where = {
      [Op.and]: [{ approval_status: status ? status : Approval.ACCEPTED, ...vendorType }, { is_active: true }],
    };
    return await super.findAll(this.vendors_qry);
  }

  async updatevendor(id: string, req_data: any) {
    let updatedVendor: any;
    if (req_data?.vendor_type == VendorType.FREELANCER) {
      updatedVendor = await Vendor.update({ ...req_data }, { where: { approval_status: Approval.ACCEPTED, id: id } });
      await Branch.update({ ...req_data }, { where: { vendor_id: id } });
    } else {
      this.init();
      let where = {
        where: { id: id },
      };
      updatedVendor = await Helpers.update(Vendor, where, req_data);
    }
    return updatedVendor;
  }
  async sendPwdToVendor(email_id: string, vendor_id?: string) {
    let pwd = Helpers.generateRandomPassword();
    let hashPassword = await Encryption.hashPassword(pwd);
    await Vendor.update({ password: hashPassword }, { where: { id: vendor_id } });
    let htmlContent = EmailTemService.emailPasswordVerification(pwd);
    let mailOptions: MailOptions = {
      to: email_id,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.PASSWORD_SENT,
      html: htmlContent,
    };
    new MailUtils().sendEmail(mailOptions);
  }
  async getFilterList(req_data: getBranchListedto): Promise<any> {
    let search;
    if (req_data.searchText) {

      search = req_data.vendor_type == VendorType.BUSINESS ? {
        [Op.or]: [
          { vendor_name: { [Op.iLike]: `%${req_data.searchText}%` } },
          { mobile_no: { [Op.iLike]: `%${req_data.searchText}%` }, },
          { email_id: { [Op.iLike]: `%${req_data.searchText}%` } },
          { id: { [Op.iLike]: `%${req_data.searchText}%` } },
          { vendor_type: { [Op.iLike]: `%${req_data.searchText}%` } },
        ],
      } : {
        [Op.or]: [
          { branch_name: { [Op.iLike]: `%${req_data.searchText}%` } },
          { branch_contact_no: { [Op.iLike]: `%${req_data.searchText}%` }, },
          { branch_email: { [Op.iLike]: `%${req_data.searchText}%` } },
          { id: { [Op.iLike]: `%${req_data.searchText}%` } },
          { vendor_type: { [Op.iLike]: `%${req_data.searchText}%` } },
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let accountVerifyStatus = req_data?.ac_verify_status ? { account_verified: req_data?.ac_verify_status } : { account_verified: false };
    const branchqry = req_data.vendor_type == VendorType.FREELANCER ? {
      include: [{
        model: Branch, attributes: ['id', 'branch_name', 'fund_account_id'],
        where: {
          is_deleted: false,
          approval_status: req_data.approval_status,
          vendor_type: req_data?.vendor_type ? req_data?.vendor_type : VendorType.BUSINESS,
          ...accountVerifyStatus,
          ...search,
        }
      }]
    } : {
      where: {
        is_deleted: false,
        approval_status: req_data.approval_status,
        vendor_type: req_data?.vendor_type ? req_data?.vendor_type : VendorType.BUSINESS,
        ...search,
      },
    };
    // if (req_data.vendor_type == VendorType.FREELANCER) {
    //   branchqry = [{
    //     model: Branch, attributes: ['id', 'branch_name'],
    //     where: {
    //       is_deleted: false,
    //       approval_status: req_data.approval_status,
    //       vendor_type: req_data?.vendor_type,
    //       ...accountVerifyStatus,
    //     }
    //   }]
    // }else{
    //   where: {
    //     is_deleted: false,
    //     approval_status: req_data.approval_status,
    //     vendor_type: req_data?.vendor_type,
    //     ...search,
    //   },
    // }


    return await this.findAndCountAll({
      // include: branchqry ? branchqry : [],
      // where: {
      //   is_deleted: false,
      //   approval_status: req_data.approval_status,
      //   vendor_type: req_data?.vendor_type,
      //   ...search,
      // },
      ...branchqry,
      offset: offset,
      limit: limit,
      order: [['created_at', 'DESC']],
    });
  }

  async orderCompleted(req_body: OrderCompletedDto) {
    logger.info('Order_Complete_entry: ' + JSON.stringify(req_body));
    const checkOrderData = await Helpers.findOne(Order, {
      where: { order_id: req_body.order_id, order_status: { [Op.ne]: APPOINTMENT_STATUS.COMPLETED } },
    });
    if (checkOrderData) {
      //TODO check appointment time crashed then close the orders otherwise should not allow the order
      if (!(await new DateUtils().isTimestampInThePast(checkOrderData?.appointment_start_date_time)))
        return { code: EC400, message: EM152 };
      if (req_body.order_otp === checkOrderData.order_otp) {
        //TODO The payout amount sent to the branches.
        const sendPayout = await this.preparePayoutAmount(req_body.order_id);
        logger.info('Payout_amount_initiated_to_razorpay_successfully ' + JSON.stringify(req_body?.order_id));
        // update database
        const updateAppointmentComplet = await Helpers.update(
          Appointment,
          { where: { order_id: req_body.order_id } },
          { appointment_status: APPOINTMENT_STATUS.COMPLETED },
        );
        const updatedBy = await Helpers.update(
          Order,
          { where: { order_id: req_body.order_id } },
          {
            order_status: APPOINTMENT_STATUS.COMPLETED,
            otp_verified: true,
            order_approved_by: req_body.order_approved_by,
            order_approved_id: req_body.order_approved_id,
          },
        );
        await this.mailContents(req_body.order_id);
        this.deleteOrderRemainder(req_body.order_id); // incase data is there then delete the remainder records
        this.sendNotifyToUser(req_body?.order_id, checkOrderData?.user_id, checkOrderData?.customer_name, checkOrderData?.appointment_start_date_time);
        logger.info('Order_complete_Order_exit: ');
      } else {
        logger.info('Order_complete_Order_OTP_mismatch_exit: ' + JSON.stringify(req_body?.order_id));
        return { code: EC410, message: EM109 };
      }
    } else {
      logger.info('Order_complete_order_not_found_exit:_' + JSON.stringify(req_body?.order_id));
      return { code: EC400, message: EM119 };
    }
    return;
  }

  async sendNotifyToUser(orderId: string, usersId: string, userName: string, appointment: any) {
    let reqObj = {
      user_id: usersId,
      title: `Hello ${userName || 'Slaylewks users'} 💇 Your order has been completed!`,
      body: `We are thrilled to inform you that your recent order with ID (${orderId}) has been successfully completed. Your items have been processed and shipped, and they should arrive shortly. Thank you for choosing Slaylewks for your beauty needs. If you have any questions or concerns, feel free to reach out to our customer support team. We hope you enjoy your service!`,
      data: { order_id: orderId, Time: appointment, user_id: usersId },
      dataModels: User
    };
    await new SendPushNotification().sendNotifyToUserPhone(reqObj);
  }

  public deleteOrderRemainder(order_id: string) {
    OrderRemainder.destroy({ force: true, where: { order_id: order_id } });
  }

  async mailContents(order_id: string) {
    let { orderDetails, payoutContent, invoice_pdf: invoice_pdf } = await this.prepareContent(order_id);
    await this.sendMail(orderDetails[0], invoice_pdf, payoutContent);
  }

  async sendMail(order: Order, invoice_pdf: Buffer, payoutContent: string) {
    // let { customer_email } = order;
    let mailOptions: MailOptions = {
      to: order?.branch?.branch_email,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_CONFIRMED,
      html: payoutContent,
      attachments: [{ content: invoice_pdf.toString('base64'), filename: 'invoice.pdf' }],
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live_1 : Slaylewks_details.cc_mail_dev,
    };
    logger.info('Order_complete_invoice_entry: ' + JSON.stringify(order?.branch?.branch_email));
    await this.MailUtilsService.sendEmail(mailOptions);
    logger.info('Order_complete_invoice_exit: ' + JSON.stringify(order?.branch?.branch_email));
  }

  async prepareContent(order_id: string) {
    let orderDetails = await Order.findAll({
      attributes: [
        'id',
        'order_id',
        'created_at',
        'appointment_start_date_time',
        'coupon_discount',
        'final_price',
        'applied_coupon_id',
        'payout_amount',
        'initial_amount',
        ORDER_ATTRIBUTES.customer_email,
        ORDER_ATTRIBUTES.customer_mobile,
        ORDER_ATTRIBUTES.customer_name,
        ORDER_ATTRIBUTES.gst,
        ORDER_ATTRIBUTES.vendor_type,
        ORDER_ATTRIBUTES.pay_by_cash
      ],
      include: [
        {
          attributes: ['id'],
          model: OrderService,
          paranoid: false,
          include: [
            {
              attributes: ['id', 'service_name', 'discount', 'discounted_price', 'amount'],
              model: Service,
              paranoid: false,
              // required: true,
              include: [
                {
                  attributes: { exclude: ['createdAt', 'updatedAt'] },
                  model: FlashSale,
                  where: FlashSaleConstants.getFlashSaleQry(),
                  required: false,
                  paranoid: false
                },
              ],
            },
          ],
        },
        {
          attributes: [
            'id',
            'branch_name',
            'branch_email',
            'branch_contact_no',
            'is_tax_checked',
            'gst_number',
            'tax_percentage',
            'commission_type',
            'payment_commission',
          ],
          model: Branch,
          paranoid: false,

          include: [
            {
              attributes: ['address', 'city', 'area'],
              model: Location,
              paranoid: false
            },
            {
              attributes: ['id', 'vendor_name'],
              model: Vendor,
              paranoid: false
            },
          ],
        },
      ],
      where: { order_id: order_id },
      paranoid: false
      // raw: true,
    });
    let coupon: Coupon;
    let { applied_coupon_id } = orderDetails[0];
    if (applied_coupon_id) coupon = await Coupon.findByPk(applied_coupon_id);
    let invoiceContent: string = EmailTemService.payoutInvoiceForBranch(orderDetails, coupon);
    let invoice: Buffer = await MailUtils.convertHtmlToPdf(invoiceContent);
    let payoutContent = EmailTemService.payoutEmailTemplate(orderDetails);
    return {
      orderDetails: orderDetails,
      payoutContent: payoutContent,
      invoice_pdf: invoice,
    };
  }

  //send payout amount to assign razorpay
  async preparePayoutAmount(orderId: string) {
    logger.info('preparePayoutAmount_entry: ' + JSON.stringify(orderId));
    const getOrderAndBranchDetails = await Order.findOne({
      attributes: ['id', 'order_id', 'user_id', 'payout_amount'],
      raw: true,
      paranoid: false,
      include: [
        {
          attributes: ['id', 'fund_account_id', 'account_verified', 'account_verify_status'],
          model: Branch,
          paranoid: false,
        },
      ],
      where: { order_id: orderId },
    });
    // if (getOrderAndBranchDetails && getOrderAndBranchDetails['branch.account_verified'] && getOrderAndBranchDetails['branch.account_verify_status'] == 'completed') { //TODO CHNAGE THE CONDION ==
    let finalObj = {
      amount: Number((getOrderAndBranchDetails?.payout_amount * 100).toFixed()),
      fund_account_id: getOrderAndBranchDetails['branch.fund_account_id'],
      branch_id: getOrderAndBranchDetails['branch.id'],
      order_id: getOrderAndBranchDetails.order_id,
    };
    logger.info('complete_order_payout_amount: ' + JSON.stringify(finalObj.amount));
    if (finalObj.amount >= 100) {
      // check payout amount above 1 rupees
      logger.info('sendOrderPayoutAmount_entry: ' + JSON.stringify(finalObj));
      const sendPayoutAmount = await new PayoutPaymentService().sendPayoutAmount(finalObj);
      logger.info('sendOrderPayoutAmountSuccess: ' + JSON.stringify(sendPayoutAmount));
      return true;
    } else {
      const getOrderAndBranchDetails = await Order.update(
        { payout: true, payout_status: 'payout amount less than 1 rupees' },
        { where: { order_id: orderId } },
      );
      logger.info('sendOrderPayoutAmount_low: ' + JSON.stringify(finalObj));
      return true;
      // throw new Error("Payout amount should be above 99 paisa");
    }
    // } else {
    //   logger.info('Branch_Ac_notverifyed_exit: ' + JSON.stringify(getOrderAndBranchDetails));
    //   throw new Error('Your branch account verification is incomplete.');
    // }
  }

  async deletedVendor(id: string) {
    const branch_ids = await Branch.findAll({ attributes: ['id'], where: { vendor_id: id } });
    const modelsToDelete = [
      Branch,
      BranchCategoryMapping,
      BranchServiceCategoryMapping,
      Service,
      SlotDetail,
      Favourites,
      FlashSale,
      CouponBranchMapping,
      Appointment,
      BranchBankDetails,
      BranchGallery,
      BranchGalleryFolder,
      BranchServiceTypeMapping,
      Cart,
      Location,
      SlotManagement,
      Vendor,
      VendorRole
    ];

    const deletePromises = modelsToDelete.map(async (Model) => {
      if (Model === VendorRole) {
        return Helpers.softDelete(Model, { where: { id: id } });
      }
      if (Model === Vendor) {
        return Helpers.softDelete(Model, { where: { id: id } });
      } else if (Model === Branch) {
        return Helpers.softDelete(Model, { where: { vendor_id: id } });
      } else {
        return Helpers.softDelete(Model, {
          where: { branch_id: { [Op.in]: branch_ids?.map((data) => data.id) } },
        });
      }
    });
    let deleteVendor = await Promise.all(deletePromises);
    return [];

  }

  async updatevendorRole(id: string, req: any) {
    let payload = {
      name: req?.vendor_name,
      image: req?.profile_url,
      ...req
    };
    let updatevendorRoles = await VendorRole.update({ ...payload }, { where: { id: id } });
    return updatevendorRoles;
  }
  // async hardDeletedVendor(id: string) {
  //   const branch_ids = await Branch.findAll({ attributes: ['id'], where: { vendor_id: id } });
  //   const modelsToDelete = [
  //     Review,
  //     Branch,
  //     BranchCategoryMapping,
  //     BranchServiceCategoryMapping,
  //     Service,
  //     SlotDetail,
  //     Favourites,
  //     FlashSale,
  //     CouponBranchMapping,
  //     Appointment,
  //     BranchBankDetails,
  //     BranchGallery,
  //     BranchGalleryFolder,
  //     BranchServiceTypeMapping,
  //     Cart,
  //     Location,
  //     SlotManagement,
  //     Vendor,
  //   ];
  //   const deletePromises = modelsToDelete.map(async (Model:any) => {
  //     if (Model === Vendor) {
  //       return Model.destroy({ where: { id: id },force:true });
  //     }
  //     else if (Model === Branch) {
  //       return Model.destroy({ where: { vendor_id: id },force:true });
  //     } else {
  //       return Model.destroy({
  //         where: { branch_id: { [Op.in]: branch_ids?.map((data) => data.id) }},force:true
  //       });
  //     }
  //   });
  //   let deleteVendor = await Promise.all(deletePromises);
  //   return;
  // }
  async vendorEnquiry(vendorEnquiryDto: VendorEnquiryDto) {
    logger.info(`Vendor_enquiry_entry ` + JSON.stringify(vendorEnquiryDto));
    let requestVendor = await Helpers.create(VendorEnquiry, vendorEnquiryDto);
    this.MailUtilsService.sendVendorRequestMail(requestVendor);
    logger.info(`Vendor_enquiry_exit_email ` + JSON.stringify(vendorEnquiryDto.email));
    return requestVendor;
  }

  async requestVendor(req: PaginationDto) {
    let page_no = (req.pagNo - 1) * req.limit;
    let limit = req.limit;
    let offset = page_no;
    let search: {};
    if (req.searchText) {
      search = {
        [Op.or]: [
          { name: { [Op.iLike]: `%${req.searchText}%` } },
          { email: { [Op.iLike]: `%${req.searchText}%` } },
          { phone_number: { [Op.iLike]: `%${req.searchText}%` } },
        ],
      };
    }
    let vendor_qry = {
      where: { ...search },
      limit: limit,
      offset: offset,
    };
    let reqVendor = await Helpers.findAndCountAll(VendorEnquiry, vendor_qry);
    return reqVendor;
  }

  async logout(req: logoutDto) {
    let userLogout = await Vendor.update({ device_token: null }, { where: { id: req.user_id } });
    return userLogout;
  }

  async orderApproval(req_body: OrderApprovalDto) {
    req_body.order_status = APPOINTMENT_STATUS.CONFIRMED;
    let updateOrderData: any = await Order.update(req_body, { returning: true, where: { order_id: req_body?.order_id } });
    let updateAppointment = await Appointment.update({ appointment_status: req_body?.order_status }, { where: { order_id: req_body?.order_id }, returning: true })
    //Todo Send to order confirmation mail to user for freelancer
    let { user, confirmationContent, invoice_pdf, appointments } = await this.prepareContentForFreelancer(updateOrderData[1][0].user_id, req_body.order_id);
    let { customer_email, applied_coupon_id, branch } = this.order[0];
    let { branch_email } = branch;

    await this.increaseCouponUsedCount(updateOrderData[1][0].user_id, applied_coupon_id);
    this.MailUtilsService.sendOrderConfirmationMail(
      customer_email || user.email_id,
      confirmationContent,
      invoice_pdf
    );
    this.notifyVendor(branch.vendor_id, NotificationType.CONFIRMED);
    this.notifyUser(updateOrderData[1][0].user_id, NotificationType.CONFIRMED);
    this.MailUtilsService.sendOrderPlacedMailToVendor(branch_email, appointments, invoice_pdf, this.order);
    this.createRemaiderRecords(customer_email || user.email_id, branch_email, appointments, this.order[0]);
    return;
  }

  async increaseCouponUsedCount(user_id: string, coupon_id: string) {
    let where_qry = { where: { user_id: user_id, coupon_id: coupon_id } };
    let updated_coupon = await Helpers.update(AppliedCoupon, where_qry, {
      used_count: Sequelize.literal('used_count + 1'),
      is_applied: false,
    });
  }

  async notifyVendor(vendor_id: string, type: NotificationType) {
    const typeConfig = notificationTypeConfig[type];
    const vendor: Vendor = await Vendor.findByPk<Vendor>(vendor_id);
    const notificationData = new CreateNotificationDto();

    notificationData.device_token = vendor.device_token || "eQNzky7-SUmu1SnTM2-Ed6:APA91bHTcGmahV707KPx16jhKCzKlcyOXcjjy_zdmTX_vMgxnPKVXUrEVycS576quAXXKqi8tdklwdnlPuYDTjLF9VJfDnowOXJOqP4mHbFg8cHDtLHvhq6nmB8iqGQXGWUB7o-0ynlS";
    notificationData.description = typeConfig.description;
    notificationData.title = typeConfig.title;
    await new NotificationService().sendPushNotification(notificationData);
  }
  async notifyUser(user_id: string, type: NotificationType) {
    const typeConfig = notificationTypeConfig[type];
    const user: User = await User.findByPk<User>(user_id);
    const notificationData = new CreateNotificationDto();

    notificationData.device_token = user.device_token || "eQNzky7-SUmu1SnTM2-Ed6:APA91bHTcGmahV707KPx16jhKCzKlcyOXcjjy_zdmTX_vMgxnPKVXUrEVycS576quAXXKqi8tdklwdnlPuYDTjLF9VJfDnowOXJOqP4mHbFg8cHDtLHvhq6nmB8iqGQXGWUB7o-0ynlS";
    notificationData.description = typeConfig.description;
    notificationData.title = typeConfig.title;
    await new NotificationService().sendPushNotification(notificationData);
  }

  async createRemaiderRecords(user_email: string, branch_email: string, appointments: any, order: any) {
    const timestamp = new Date(order?.appointment_start_date_time).getTime();
    const threeHoursBehindTimestamp = timestamp - 3 * 60 * 60 * 1000; //half hour - 30 * 60 * 1000;
    let update_body = {
      order_id: order?.order_id,
      user_email: user_email,
      branch_email: branch_email,
      trigger_type: TriggerType.OrderRemain,
      trigger_time: threeHoursBehindTimestamp,
      appointment_data: appointments,
      order_data: order,
    };
    let where = { where: { order_id: order?.order_id }, defaults: update_body };
    const createRemainder = await Helpers.findOrCreate(OrderRemainder, where);
    if (!createRemainder[1]) {
      delete where?.defaults;
      let updateRemainder = await Helpers.update(OrderRemainder, where, update_body);
    }
  }

  async prepareContentForFreelancer(user_id: string, order_id: string) {
    let confirmationContent: any;
    let vendortemp: any;
    let user: User = await User.findOne({ where: { id: user_id } });
    let { data: appointments } = await this.getAppointments(user_id, 1, order_id);
    this.order = await this.getOrderDetails(order_id);

    confirmationContent = EmailTemService.confirmationTemp(appointments, this.order);
    let invoiceContent: string = EmailTemService.orderInovoice(appointments, this.order);
    let invoice: Buffer = await MailUtils.convertHtmlToPdf(invoiceContent);
    return {
      user: user,
      confirmationContent: confirmationContent,
      inovoiceContent: invoiceContent,
      invoice_pdf: invoice,
      appointments: appointments,
      vendortemp: vendortemp,
    };
  }

  async getOrderDetails(order_id: string) {
    let order_qry = {
      attributes: [
        'order_otp',
        'original_price',
        'user_id',
        'createdAt',
        'coupon_discount',
        'applied_coupon_id',
        'gst',
        'service_type',
        'order_status',
        'is_payment_completed',
        'payment_status',
        'payment_method',
        'payment_id',
        'total_services',
        'customer_email',
        'customer_mobile',
        'customer_name',
        'final_price',
        'discount',
        'original_price',
        'order_id',
        'appointment_start_date_time',
        'created_at',
        'rescheduled',
        'rescheduled_at',
        ORDER_ATTRIBUTES.booking_slot,
        ORDER_ATTRIBUTES.updated_at,
        ORDER_ATTRIBUTES.address_id,
        ORDER_ATTRIBUTES.vendor_type
      ],
      where: {
        order_id: order_id,
      },
      paranoid: false,
      include: [
        {
          model: Branch,
          attributes: [
            BRANCH_ATTRIBUTES.ID,
            BRANCH_ATTRIBUTES.BRANCH_IMAGE,
            BRANCH_ATTRIBUTES.BRANCH_NAME,
            BRANCH_ATTRIBUTES.BRANCH_EMAIL,
            BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO,
            BRANCH_ATTRIBUTES.IS_TAX_CHECKED,
            BRANCH_ATTRIBUTES.TAX_PERCENTAGE,
            BRANCH_ATTRIBUTES.GST_NUMBER,
            BRANCH_ATTRIBUTES.VENDOR_ID,
          ],
          paranoid: false,
          include: {
            attributes: [
              LOCATION_ATTRIBUTES.latitude,
              LOCATION_ATTRIBUTES.longitude,
              LOCATION_ATTRIBUTES.address,
              LOCATION_ATTRIBUTES.area,
              LOCATION_ATTRIBUTES.city,
              LOCATION_ATTRIBUTES.state,
              LOCATION_ATTRIBUTES.pincode,
            ],
            model: Location,
            paranoid: false,
          },
        },
        {
          attributes: ['id', 'receipt', 'status'],
          model: Payment_Txn,
          paranoid: false,
        },
        {
          model: UserAddress,
          attributes: ['flat_no', 'area', 'city', 'zipcode', 'state', 'country', 'customer_name', 'customer_mobile'],
          paranoid: false,
        },
        {
          attributes: ['appointment_start_date_time', 'appointment_status'],
          model: Appointment,
          paranoid: false,
          include: {
            model: Service,
            attributes: { exclude: ['is_active', 'createdAt', 'updatedAt'] },
            paranoid: false,
            include: [
              {
                attributes: { exclude: ['createdAt', 'updatedAt'] },
                model: FlashSale,
                where: FlashSaleConstants.getFlashSaleQry(),
                required: false,
                paranoid: false,
              },
              { attributes: [MASTER_CATEGORIES_ATTRIBUTES.category], model: MasterCategory, paranoid: false },
            ],
          },
        },
      ],
    };
    let order = await Helpers.findAll(Order, order_qry);
    if (order.length == 0) throw new NotFoundException(Errors.INVALID_ORDER_DETAILS);
    return order;
  }

  async getAppointments(id: string, status: number, order_id?: string) {
    let statusArr = {
      1: [APPOINTMENT_STATUS.PENDING, APPOINTMENT_STATUS.CONFIRMED],
      2: APPOINTMENT_STATUS.COMPLETED,
      3: [APPOINTMENT_STATUS.CANCELLED, APPOINTMENT_STATUS.REJECTED, APPOINTMENT_STATUS.EXPIRED],
    };
    let appointment_qry = {
      attributes: [
        APPOINTMENT_ATTRIBUTES.IS_ACCEPTED_BY_BRANCH,
        APPOINTMENT_ATTRIBUTES.IS_CANCELLED,
        APPOINTMENT_ATTRIBUTES.APPOINTMENT_STATUS,
        APPOINTMENT_ATTRIBUTES.ORDER_ID,
        APPOINTMENT_ATTRIBUTES.APPOINTMENT_START_DATE_TIME,
        APPOINTMENT_ATTRIBUTES.BOOKING_SLOT,
      ],
      where: { user_id: id, appointment_status: statusArr[status] },
      include: [
        {
          model: Service,
          attributes: [
            SERVICE_ATTRIBUTES.SERVICE_NAME,
            SERVICE_ATTRIBUTES.SERVICE_DESCRIPTION,
            SERVICE_ATTRIBUTES.AMOUNT,
            SERVICE_ATTRIBUTES.DISCOUNT,
            SERVICE_ATTRIBUTES.DISCOUNTED_PRICE,
          ],
          include: [
            {
              attributes: { exclude: ['createdAt', 'updatedAt'] },
              model: FlashSale,
              where: FlashSaleConstants.getFlashSaleQry(),
              required: false,
            },
            {
              attributes: [MASTER_CATEGORIES_ATTRIBUTES.category],
              model: MasterCategory,
              required: false,
            },
          ],
        },
        {
          model: Order,
          attributes: ['order_otp', ORDER_ATTRIBUTES.service_type, ORDER_ATTRIBUTES.payment_status, ORDER_ATTRIBUTES.booking_slot],
        },
        {
          model: Branch,
          attributes: [BRANCH_ATTRIBUTES.ID, BRANCH_ATTRIBUTES.BRANCH_IMAGE, BRANCH_ATTRIBUTES.BRANCH_NAME],
          include: {
            attributes: ['area', 'city'],
            model: Location,
          },
        },
      ],
      order: [[APPOINTMENT_ATTRIBUTES.APPOINTMENT_START_DATE_TIME, 'ASC']],
    };
    if (order_id) appointment_qry.where['order_id'] = order_id;
    let appointments = await Helpers.findAll(Appointment, appointment_qry);
    appointments = JSON.stringify(appointments);
    appointments = JSON.parse(appointments);
    appointments = appointments.map((appts) => {
      let apt = {
        ...appts.order,
        ...appts,
      };
      delete apt.order;
      return apt;
    });
    // console.log('======>', appointments);
    return { code: EC200, msg: EM106, data: appointments };
  }
}
