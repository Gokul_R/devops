import { Module } from '@nestjs/common';
import { VendorService } from './vendor.service';
import { VendorController } from './vendor.controller';
import { MailUtils } from 'src/core/utils/mailUtils';

@Module({
  controllers: [VendorController],
  providers: [VendorService, MailUtils],
  
})
export class VendorModule { }
