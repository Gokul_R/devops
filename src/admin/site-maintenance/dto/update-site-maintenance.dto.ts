import {  PartialType } from '@nestjs/swagger';
import { CreateSiteMaintenanceDto } from './create-site-maintenance.dto';

export class UpdateSiteMaintenanceDto  extends PartialType(CreateSiteMaintenanceDto){
 
}
