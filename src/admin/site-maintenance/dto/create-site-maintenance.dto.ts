import { IsOptional, IsString, IsBoolean, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateSiteMaintenanceDto {
  @IsString()
  @IsOptional()
  id: string;

  @ApiProperty({ example: 'server is in progress' })
  @IsString()
  @IsOptional()
  @Length(3, 100, {
    message: 'maintenance_text must be between 3 and 100 characters',
  })
  maintenance_text: string;

  @ApiProperty({ example: 'new updates are there' })
  @IsString()
  @IsOptional()
  sub_text: string;

  @ApiProperty({ example: 'https//example.com/photo.jpg' })
  @IsString()
  @IsOptional()
  image: string;

  @ApiProperty({ example: 'true' })
  @IsBoolean()
  @IsOptional()
  is_active: boolean;

  //@ApiProperty({default:false})
  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;
}
