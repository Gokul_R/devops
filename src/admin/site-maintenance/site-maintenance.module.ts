import { Module } from '@nestjs/common';
import { SiteMaintenanceService } from './site-maintenance.service';
import { SiteMaintenanceController } from './site-maintenance.controller';
import { siteMaintenanceProviders } from './site-maintenance.providers';

@Module({
  controllers: [SiteMaintenanceController],
  providers: [SiteMaintenanceService,...siteMaintenanceProviders],
})
export class SiteMaintenanceModule {}
