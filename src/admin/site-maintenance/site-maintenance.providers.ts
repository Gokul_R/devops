
import { commonProvider } from 'src/core/interfaces/common-providers';

export const siteMaintenanceProviders = [
  // {
  //   provide: SITE_MAINTENANCE_REPOSITORY,
  //   useValue: SiteMaintenance,
  // },
  commonProvider.site_maintenance_repository
];
