import { Test, TestingModule } from '@nestjs/testing';
import { SiteMaintenanceController } from './site-maintenance.controller';
import { SiteMaintenanceService } from './site-maintenance.service';

describe('SiteMaintenanceController', () => {
  let controller: SiteMaintenanceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SiteMaintenanceController],
      providers: [SiteMaintenanceService],
    }).compile();

    controller = module.get<SiteMaintenanceController>(SiteMaintenanceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
