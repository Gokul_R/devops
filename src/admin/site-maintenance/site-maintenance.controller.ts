import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { SiteMaintenanceService } from './site-maintenance.service';
import { CreateSiteMaintenanceDto } from './dto/create-site-maintenance.dto';
import { UpdateSiteMaintenanceDto } from './dto/update-site-maintenance.dto';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../base.controller';

@ApiTags('site-maintenance')
@Controller('site-maintenance')
export class SiteMaintenanceController extends BaseController<CreateSiteMaintenanceDto> {
  constructor(
    private readonly siteMaintenanceService: SiteMaintenanceService,
  ) {
    super(siteMaintenanceService);
  }


  @Post()
  // @UsePipes(new ValidationPipe())
  async create(@Body() body: CreateSiteMaintenanceDto) {
    try {
      let data = await this.siteMaintenanceService.create(body);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get()
  // async findAll() {
  //   try {
  //     let data = await this.siteMaintenanceService.findAll();
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  // @Get(':id')
  // async findOne(@Param('id') id: string) {
  //   try {
  //     let data = await this.siteMaintenanceService.findOne(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() body: UpdateSiteMaintenanceDto,
  ) {
    try {
      let data = await this.siteMaintenanceService.update(id, body);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
