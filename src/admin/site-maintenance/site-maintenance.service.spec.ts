import { Test, TestingModule } from '@nestjs/testing';
import { SiteMaintenanceService } from './site-maintenance.service';

describe('SiteMaintenanceService', () => {
  let service: SiteMaintenanceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SiteMaintenanceService],
    }).compile();

    service = module.get<SiteMaintenanceService>(SiteMaintenanceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
