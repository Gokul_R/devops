import { Inject, Injectable } from '@nestjs/common';
import { CreateSiteMaintenanceDto } from './dto/create-site-maintenance.dto';
import SiteMaintenance from 'src/core/database/models/SiteMaintenance';
import { UpdateSiteMaintenanceDto } from './dto/update-site-maintenance.dto';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import { SITE_MAINTENANCE_REPOSITORY } from 'src/core/constants';

@Injectable()
export class SiteMaintenanceService  extends BaseService<SiteMaintenance>{
  protected model: ModelCtor<SiteMaintenance>;
  constructor(@Inject(SITE_MAINTENANCE_REPOSITORY) private readonly siteMaintenanceRepo:typeof SiteMaintenance ){
    super();
    this.model= this.siteMaintenanceRepo;
  }

  //  async create(body: CreateSiteMaintenanceDto){
  //    const result =  await SiteMaintenance.create(body);
  //    return result;
  // }

  // findAll() {
  //   return SiteMaintenance.findAll();
  // }

  // findOne(Id: string) {
  //   return SiteMaintenance.findOne({
  //     where: { id: Id },
  //   });
  // }

  // update(Id: string, body: UpdateSiteMaintenanceDto) {
  //   return SiteMaintenance.update(body, {
  //     where: { id: Id },
  //   });
  // }
}
