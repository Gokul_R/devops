import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ServicesReportService } from './services-report.service';
import { CreateServicesReportDto } from './dto/create-services-report.dto';
import { UpdateServicesReportDto } from './dto/update-services-report.dto';
import { BaseController } from 'src/base.controller';
import { EC200, EM106, EM100, EM104 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Service Report')
@Controller('services-report')
export class ServicesReportController {
  constructor(private readonly servicesReportService: ServicesReportService) {
    // super(servicesReportService);
  }

  @Post()
  async create(@Body() createServicesReportDto: CreateServicesReportDto) {
    try {
      let data = await this.servicesReportService.getServiceDetails(createServicesReportDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
    //  return this.servicesReportService.create(createServicesReportDto);
  }
}
