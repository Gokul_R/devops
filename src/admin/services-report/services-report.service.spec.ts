import { Test, TestingModule } from '@nestjs/testing';
import { ServicesReportService } from './services-report.service';

describe('ServicesReportService', () => {
  let service: ServicesReportService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ServicesReportService],
    }).compile();

    service = module.get<ServicesReportService>(ServicesReportService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
