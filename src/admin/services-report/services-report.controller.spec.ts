import { Test, TestingModule } from '@nestjs/testing';
import { ServicesReportController } from './services-report.controller';
import { ServicesReportService } from './services-report.service';

describe('ServicesReportController', () => {
  let controller: ServicesReportController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ServicesReportController],
      providers: [ServicesReportService],
    }).compile();

    controller = module.get<ServicesReportController>(ServicesReportController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
