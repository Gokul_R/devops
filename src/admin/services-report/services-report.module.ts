import { Module } from '@nestjs/common';
import { ServicesReportService } from './services-report.service';
import { ServicesReportController } from './services-report.controller';

@Module({
  controllers: [ServicesReportController],
  providers: [ServicesReportService]
})
export class ServicesReportModule {}
