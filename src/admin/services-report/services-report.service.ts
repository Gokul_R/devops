import { Injectable } from '@nestjs/common';
import { CreateServicesReportDto } from './dto/create-services-report.dto';
import { UpdateServicesReportDto } from './dto/update-services-report.dto';
import { BaseService } from 'src/base.service';
import Services from 'src/core/database/models/Services';
import { ModelCtor } from 'sequelize-typescript';
import { Op, QueryTypes, Sequelize } from 'sequelize';
import MasterCategory from 'src/core/database/models/MasterCategory';
import Branch from 'src/core/database/models/Branch';
import OrderService from 'src/core/database/models/OrderServices';
import Order from 'src/core/database/models/Order';
import { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import VendorRole from 'src/core/database/models/VendorRoles';

@Injectable()
export class ServicesReportService extends PaginationDto {
  //protected model: ModelCtor<Services>;

  async getServiceDetails(createServicesReportDto: CreateServicesReportDto) {
    let { from_date, to_date, branch_id, vendor_id, searchText, status, limit, pagNo, vendor_type } =
      createServicesReportDto;

    const pageSize = limit || 10;
    const currentPage = pagNo || 1;
    const offset = (currentPage - 1) * pageSize;

    let searchData;
    if (searchText) {
      searchData = {
        //   where: {
        [Op.or]: [
          { '$branch.branch_name$': { [Op.iLike]: `%${searchText}%` } },
          { '$category.category$': { [Op.iLike]: `%${searchText}%` } },
          { '$Service.service_name$': { [Op.iLike]: `%${searchText}%` } },
        ],
        // },
      };
    }
    let branchId: any;
    let vendorId: any
    let vendorRoleUser = await VendorRole.findOne({ attributes: ['branch_ids'], where: { id: vendor_id } });
    if (vendorRoleUser) {
      branchId = { where: { '$ordersService->order.branch_id$': vendorRoleUser?.branch_ids } }

    } else {
      branchId = branch_id
        ? { where: { '$ordersService->order.branch_id$': branch_id } }
        : '';

      vendorId = vendor_id && vendor_type
        ? { where: { '$branch.vendor_id$': vendor_id, '$branch.vendor_type$': vendor_type } }
        : vendor_id && !vendor_type ? { where: { '$branch.vendor_id$': vendor_id } } : !vendor_id && vendor_type ? { where: { '$branch.vendor_type$': vendor_type } } : '';

    }

    let dateStatus;
    if (from_date && to_date) {
      dateStatus = {
        '$ordersService->order.created_at$': {
          [Op.gte]: `${from_date} 00:00:00.000`,
          [Op.lte]: `${to_date} 23:59:59.999`,
        },
      };
    }
    let orderStatus = status ? { '$ordersService->order.order_status$': status } : {};
    let query: any = {
      attributes: [
        'service_name',
        'id',
        [Sequelize.literal('MAX("ordersService->order"."order_id")'), 'order_id'],
        [
          Sequelize.literal('MAX("ordersService->order"."created_at")'),
          'order_created_at',
        ],
        [
          Sequelize.fn('COUNT', Sequelize.col('ordersService.service_id')),
          'service_count',
        ],
      ],

      include: [
        {
          attributes: [],
          model: OrderService,
          as: 'ordersService',
          required: true,
          paranoid: false,
          include: [
            {
              attributes: [],
              model: Order,
              ...branchId,
              as: 'order',
              required: true,
              paranoid: false
            },
          ],
        },
        {
          attributes: ['id', 'branch_name', 'vendor_id', 'vendor_type'],
          as: 'branch',
          model: Branch,
          ...vendorId,
          paranoid: false
          //required:true,
        },
        {
          attributes: ['category'],
          as: 'category',
          model: MasterCategory,
          paranoid: false
        },
      ],
      paranoid: false,
      where: {
        ...orderStatus,
        ...dateStatus,
        ...searchData,
      },
      group: ['Service.id', 'Service.service_name', 'branch.id', 'category.id'],
    };
    // console.log(query);

    let data: any = await Services.findAndCountAll({
      ...query,
      limit: pageSize,
      offset: offset,
      subQuery: false,
    });
      data.count = data?.count.length;
    return data;
  }
}
