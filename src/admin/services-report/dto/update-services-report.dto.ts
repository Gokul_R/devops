import { PartialType } from '@nestjs/swagger';
import { CreateServicesReportDto } from './create-services-report.dto';

export class UpdateServicesReportDto extends PartialType(CreateServicesReportDto) {}
