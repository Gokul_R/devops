import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDateString,
  IsEnum,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { VendorType } from 'src/core/utils/enum';

export class CreateServicesReportDto extends PaginationDto {
  @IsString()
  @IsOptional()
  id?: string;

  @ApiProperty({ example: '2023-08-01' })
  @IsDateString()
  @IsOptional()
  from_date?: string;

  @ApiProperty({ example: '2023-08-01' })
  @IsOptional()
  @IsDateString()
  to_date?: string;

  @ApiProperty({ example: null })
  @IsOptional()
  // @IsUUID()
  @IsString()
  vendor_id?: string;

  @ApiProperty({ example: null })
  @IsOptional()
  // @IsUUID()
  @IsString()
  branch_id?: string;
  @ApiProperty({ example: null })
  @IsOptional()
  @IsEnum(APPOINTMENT_STATUS, {
    message:
      'status must be one of the following values: Pending, Confirmed, Completed, Cancelled, Rejected',
  })
  status?: string;

  @ApiProperty({ example: `${VendorType.BUSINESS} '(or)' ${VendorType.FREELANCER}` })
  @IsOptional()
  @IsString()
  vendor_type: string;
}
