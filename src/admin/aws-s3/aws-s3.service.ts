import { Injectable } from '@nestjs/common';
import { FileUploadDto } from './dto/create-aws-s3.dto';
import { S3 } from 'aws-sdk';
import { ConfigService } from '@nestjs/config';
import { s3Credentials } from 'src/core/constants/appConfig';
import { logger } from 'src/core/utils/logger';

@Injectable()
export class AwsS3Service {
  private readonly s3: S3;

  constructor() {
    this.s3 = new S3(s3Credentials);
  }

  // async uploadFile(buffer: Buffer, fileName: string, path: string): Promise<string> {
  async uploadFile(file: Express.Multer.File, fileUploadDto: FileUploadDto): Promise<string> {
    logger.info('Admin_side_single_image_upload_entry: ' + JSON.stringify(fileUploadDto));
    const { buffer, originalname, mimetype } = file;
    let path = originalname.split('_');
    let full_path = path[0];
    if (path.length > 2) full_path = path[0] + '/' + path[1];
    const uploadParams: S3.Types.PutObjectRequest = {
      Bucket: s3Credentials.bucket_name + '/' + full_path,
      Key: originalname,
      ContentType: mimetype,
      Body: buffer,
      ACL: 'public-read', // or 'private' based on your requirements
    };

    const uploadResponse = await this.s3
      .upload(uploadParams)
      .promise()
      .catch((err) => {
        console.log('Admin_side_single_image_upload_error: ', err);
        logger.error('Admin_side_single_image_upload_error: ' + JSON.stringify(err));
        throw new Error(err);
      });
    let uploaded_image_path = full_path + '/' + uploadParams.Key;
    let res;
    if (fileUploadDto?.field_name) {
      res = { [fileUploadDto.field_name]: uploaded_image_path };
    } else {
      res = uploaded_image_path;
    }
    logger.info('Admin_side_single_image_upload_exit_200: ' + JSON.stringify(res));
    return res;
  }

  async uploadMultipleImages(files: Array<Express.Multer.File>): Promise<string[]> {
    const uploadPromises = [];
    logger.info('Admin_side_multiple_image_upload_entry: ');
    for (const file of files) {
      const { originalname } = file;
      let path = originalname.split('_');
      let full_path = path[0];
      if (path.length > 2) full_path = path[0] + '/' + path[1];
      const uploadParams: AWS.S3.Types.PutObjectRequest = {
        Bucket: s3Credentials.bucket_name + '/' + full_path,
        Key: file.originalname,
        ContentType: file.mimetype,
        Body: file.buffer,
        ACL: 'public-read', // or 'private' based on your requirements
      };
      const uploadResponse = await this.s3
        .upload(uploadParams)
        .promise()
        .catch((err) => {
          console.log('Admin_side_multiple_image_upload_error: ', err);
          logger.error('Admin_side_multiple_image_upload_error: ' + JSON.stringify(err));
          throw new Error(err);
        });
      uploadPromises.push(full_path + '/' + uploadParams.Key);
    }
    const uploadLocations = await Promise.all(uploadPromises);
    logger.info('Admin_side_multiple_image_upload_exit_200: ' + JSON.stringify(uploadLocations));
    return uploadLocations;
  }
}
