import { Controller, Post, UseInterceptors, UploadedFile, Param, UploadedFiles, Body } from '@nestjs/common';
import { AwsS3Service } from './aws-s3.service';
import { FileUploadDto } from './dto/create-aws-s3.dto';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104 } from 'src/core/constants';
import { log } from 'console';
@Controller('aws-s3')
@ApiTags('AwsS3 Service')
export class AwsS3Controller {
  constructor(private readonly awsS3Service: AwsS3Service) { }

  @Post('image')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    required: false,
    description: 'Image file',
    type: FileUploadDto,
    // isArray:true  
  })
  @UseInterceptors(FileInterceptor('image'))
  async uploadImage(@UploadedFile() file: Express.Multer.File, @Body() data: any): Promise<any> {
    try {
      log(file);
      const imageUrl = await this.awsS3Service.uploadFile(file, data);
      return HandleResponse.buildSuccessObj(EC200, EM104, imageUrl);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('multiple-images/')
  @ApiConsumes('multipart/form-data')
  @ApiBody(
    {
      description: 'Image file',
      type: FileUploadDto,
      // isArray: true,
    }
  )
  @UseInterceptors(FilesInterceptor('image'))
  async uploadMultipleImage(@UploadedFiles() files: Array<Express.Multer.File>): Promise<any> {
    try {
      const imageUrl = await this.awsS3Service.uploadMultipleImages(files);
      return HandleResponse.buildSuccessObj(EC200, EM104, imageUrl);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

}
