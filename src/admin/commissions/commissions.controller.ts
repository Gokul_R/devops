import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CommissionsService } from './commissions.service';
import { UpdateCommissionDto } from './dto/update-commission.dto';
import { ApiTags } from '@nestjs/swagger';
import { EC200, EM100, EM106, EM116 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { GetPayoutDto } from '../payout/dto/get-payout.dto';

@Controller('commissions')
@ApiTags('Commissions')
export class CommissionsController {
  constructor(private readonly commissionsService: CommissionsService) { }

  @Post('search')
  async getFilterList(@Body() getcommissionsService: GetPayoutDto) {
    try {
      let data = await this.commissionsService.getFilterList(getcommissionsService);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('update-commissions')
  async updateCommissions(@Body() updatecommissionsService: UpdateCommissionDto) {
    try {
      let data = await this.commissionsService.updateCommissions(updatecommissionsService);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
