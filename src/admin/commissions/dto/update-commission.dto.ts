import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateCommissionDto } from './create-commission.dto';
import { IsBoolean, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateIf } from 'class-validator';
import { CommissionType } from 'src/admin/branch/dto/create-branch.dto';
import { BRANCH_ID } from 'src/core/constants';

export class UpdateCommissionDto extends PartialType(CreateCommissionDto) {

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    branch_id?: string;

    @ApiProperty({ example: false })
    @IsBoolean()
    @IsNotEmpty()
    is_tax_checked: boolean;

    // @ApiProperty({ example: 0 })
    // @IsNumber()
    // @IsNotEmpty()
    // tax_percentage: number;

    @ApiProperty({ example: `${CommissionType.AMOUNT} or ${CommissionType.PERCENTAGE}` })
    @IsNotEmpty()
    @IsEnum(CommissionType)
    commission_type: string | null;

    @ApiProperty({ example: 0 })
    @IsNumber()
    @IsNotEmpty()
    payment_commission: number;

    @ApiProperty({ example: '24AAACC1206D1ZM' })
    @ValidateIf((o) => o.is_tax_checked === true)
    @IsNotEmpty()
    @IsString()
    gst_number: string;
  
    @ApiProperty()
    @ValidateIf((o) => o.is_tax_checked === true)
    @IsNotEmpty()
    @IsString()
    gst_document_url: string;
  
    @ApiProperty({ example: 0, nullable: true })
    @ValidateIf((o) => o.is_tax_checked === true)
    @IsNumber()
    @IsNotEmpty()
    tax_percentage: number | null;
}
