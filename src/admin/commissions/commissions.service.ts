import { Injectable } from '@nestjs/common';
import { UpdateCommissionDto } from './dto/update-commission.dto';
import { Op, where } from 'sequelize';
import Branch from 'src/core/database/models/Branch';
import Vendor from 'src/core/database/models/Vendor';
import { Approval } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { GetPayoutDto } from '../payout/dto/get-payout.dto';

@Injectable()
export class CommissionsService {

  async getFilterList(req_data: GetPayoutDto) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { reg_id: { [Op.iLike]: `%${searchText}%` } },
          { branch_name: { [Op.iLike]: `%${searchText}%` } },
          { branch_email: { [Op.iLike]: `%${searchText}%` } },
          { branch_contact_no: { [Op.iLike]: `%${searchText}%` } },
          { vendor_type: { [Op.iLike]: `%${searchText}%` } },
          { secondary_mobile: { [Op.iLike]: `%${searchText}%` } },
          { '$vendor.vendor_name$': { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let dateStatus;
    if (req_data.from_date && req_data.to_date) {
      dateStatus = {
        created_at: {
          [Op.gt]: `${req_data.from_date} 00:00:00.000`,
          [Op.lte]: `${req_data.to_date} 23:59:59.999`,
        },
      };
    }
    let vendorId: any = req_data.vendor_id ? { vendor_id: req_data.vendor_id } : '';
    let branchId: any = req_data.branch_id ? { id: req_data.branch_id } : '';
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await Branch.findAndCountAll({
      attributes: [BRANCH_ATTRIBUTES.ID, BRANCH_ATTRIBUTES.REG_ID, BRANCH_ATTRIBUTES.BRANCH_NAME,
      BRANCH_ATTRIBUTES.BRANCH_EMAIL, BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO, BRANCH_ATTRIBUTES.VENDOR_TYPE,
      BRANCH_ATTRIBUTES.BRANCH_IMAGE, BRANCH_ATTRIBUTES.IS_TAX_CHECKED, BRANCH_ATTRIBUTES.TAX_PERCENTAGE,
      BRANCH_ATTRIBUTES.PAYMENT_COMMISSION, BRANCH_ATTRIBUTES.COMMISSION_TYPE, BRANCH_ATTRIBUTES.VENDOR_ID,
      BRANCH_ATTRIBUTES.CREATED_AT],
      include: [{
        attributes: ['vendor_name', 'id'],
        model: Vendor,
       paranoid:false
      }],
     paranoid:false,
      where: {
        is_deleted: false,
        approval_status: Approval.ACCEPTED,
        ...dateStatus,
        ...vendorId,
        ...branchId,
        ...search,
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']]
    });
  }

  async updateCommissions(req_data: UpdateCommissionDto) {
    const updateCommissions = await Branch.update(req_data, { where: { id: req_data.branch_id }, returning: true });
    return updateCommissions[1][0];
  }

}
