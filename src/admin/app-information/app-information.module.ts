import { Module } from '@nestjs/common';
import { AppInformationService } from './app-information.service';
import { AppInformationController } from './app-information.controller';
import { appInformationProviders } from './app-information.providers';

@Module({
  controllers: [AppInformationController],
  providers: [AppInformationService,...appInformationProviders]
})
export class AppInformationModule {}