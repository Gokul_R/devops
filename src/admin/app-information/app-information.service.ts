import { Inject, Injectable } from '@nestjs/common';
import { CreateAppInformationDto } from './dto/create-app-information.dto';
import { UpdateAppInformationDto } from './dto/update-app-information.dto';
import { BaseService } from '../../base.service';
import AppInformation from 'src/core/database/models/AppInformation';
import { ModelCtor } from 'sequelize-typescript';
import { APP_INFORMATION_REPOSITORY } from 'src/core/constants';

@Injectable()
export class AppInformationService extends BaseService<AppInformation> {
  protected model: ModelCtor<AppInformation>;
  constructor(
    @Inject(APP_INFORMATION_REPOSITORY)
    private readonly appInformationRepo: typeof AppInformation,
  ) {
    super();
    this.model = this.appInformationRepo;
  }
  // create(createAppInformationDto: CreateAppInformationDto) {
  //   return 'This action adds a new appInformation';
  // }

  // findAll() {
  //   return `This action returns all appInformation`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} appInformation`;
  // }

  // update(id: number, updateAppInformationDto: UpdateAppInformationDto) {
  //   return `This action updates a #${id} appInformation`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} appInformation`;
  // }
}
