import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateAppInformationDto {
  @IsString()
  @IsOptional()
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  ios_version: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  ios_app_link: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  android_version: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  android_app_link: string;

  @ApiProperty()
  @IsOptional()//
  @IsString()
  vendor_android_version: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  vendor_android_app_link: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  vendor_ios_version: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  vendor_ios_app_link: string;

  // @ApiProperty({default:false})
  // @IsNotEmpty()
  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;
}
