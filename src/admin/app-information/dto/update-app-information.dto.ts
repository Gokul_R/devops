import { PartialType } from '@nestjs/swagger';
import { CreateAppInformationDto } from './create-app-information.dto';
export class UpdateAppInformationDto extends PartialType(CreateAppInformationDto) {}
