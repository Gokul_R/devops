import { Test, TestingModule } from '@nestjs/testing';
import { AppInformationService } from './app-information.service';

describe('AppInformationService', () => {
  let service: AppInformationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AppInformationService],
    }).compile();

    service = module.get<AppInformationService>(AppInformationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
