import { Test, TestingModule } from '@nestjs/testing';
import { AppInformationController } from './app-information.controller';
import { AppInformationService } from './app-information.service';

describe('AppInformationController', () => {
  let controller: AppInformationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AppInformationController],
      providers: [AppInformationService],
    }).compile();

    controller = module.get<AppInformationController>(AppInformationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
