import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AppInformationService } from './app-information.service';
import { CreateAppInformationDto } from './dto/create-app-information.dto';
import { UpdateAppInformationDto } from './dto/update-app-information.dto';
import { BaseController } from '../../base.controller';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import { ApiTags } from '@nestjs/swagger';
import Helpers from 'src/core/utils/helpers';
import AppInformation from 'src/core/database/models/AppInformation';

@ApiTags('app-information')
@Controller('app-information')
export class AppInformationController extends BaseController<CreateAppInformationDto> {
  constructor(private readonly appInformationService: AppInformationService) {
    super(appInformationService);
  }

  @Post()
  async create(@Body() createAppInformationDto: CreateAppInformationDto) {
    try {
      let data: any;
      let findData = await AppInformation.findOne({ order: [['updated_at', 'DESC']] });
      if (findData && findData?.dataValues) {
        data = await Helpers.update(AppInformation, { where: { id: findData?.dataValues?.id } }, createAppInformationDto);
        return HandleResponse.buildSuccessObj(EC200, EM104, data[0]);
      } else {
        data = await this.appInformationService.create(createAppInformationDto);
        return HandleResponse.buildSuccessObj(EC200, EM104, data);
      }
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }


    //return this.appInformationService.create(createAppInformationDto);
  }

  // @Get()
  // findAll() {
  //   return this.appInformationService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.appInformationService.findOne(+id);
  // }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateAppInformationDto: UpdateAppInformationDto) {
    try {
      let data = await this.appInformationService.update(id, updateAppInformationDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }


    //  return this.appInformationService.update(id, updateAppInformationDto);
  }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.appInformationService.remove(+id);
  // }
}
