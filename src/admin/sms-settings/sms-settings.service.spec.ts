import { Test, TestingModule } from '@nestjs/testing';
import { SmsSettingsService } from './sms-settings.service';

describe('SmsSettingsService', () => {
  let service: SmsSettingsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SmsSettingsService],
    }).compile();

    service = module.get<SmsSettingsService>(SmsSettingsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
