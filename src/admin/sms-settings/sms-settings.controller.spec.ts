import { Test, TestingModule } from '@nestjs/testing';
import { SmsSettingsController } from './sms-settings.controller';
import { SmsSettingsService } from './sms-settings.service';

describe('SmsSettingsController', () => {
  let controller: SmsSettingsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SmsSettingsController],
      providers: [SmsSettingsService],
    }).compile();

    controller = module.get<SmsSettingsController>(SmsSettingsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
