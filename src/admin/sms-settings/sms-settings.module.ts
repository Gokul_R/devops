import { Module } from '@nestjs/common';
import { SmsSettingsService } from './sms-settings.service';
import { SmsSettingsController } from './sms-settings.controller';
import { smsSettingsProviders } from './sms-settings.providers';

@Module({
  controllers: [SmsSettingsController],
  providers: [SmsSettingsService,...smsSettingsProviders],
})
export class SmsSettingsModule {}
