import {
  IsOptional,
  IsString,
  IsEnum,
  IsInt,
  IsBoolean,
  IsNumber,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

enum Sms_type {
  TEXT_LOCAL = 'text-local',
  TWILIO = 'twilio',
}

export class CreateSmsSettingsDto {
  @IsString()
  @IsOptional()
  id: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: false })
  @IsBoolean()
  user_verification: boolean;

  @ApiProperty({ example: '9024564321' })
  @IsString()
  country_code: string;

  @ApiProperty({ example: 'text-local' })
  @IsEnum(Sms_type, { message: 'sms-type must be text-local or twilio' })
  sms_type: Sms_type;

  @ApiProperty({ example: '12345' })
  @IsString()
  twilio_number: string;

  @ApiProperty({ example: '12345' })
  @IsString()
  twilio_sid: string;

  @ApiProperty({ example: 'dsookkfffovomof' })
  @IsString()
  twilio_auth_token: string;

  @ApiProperty({ example: 'https/3000p/textlocal/api' })
  @IsString()
  textlocal_api: string;

  //@ApiProperty({ default: false })
  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;
}
