import { PartialType } from '@nestjs/swagger';
import { CreateSmsSettingsDto } from './create-sms-setting.dto';

export class UpdateSmsSettingsDto extends PartialType(CreateSmsSettingsDto) {}
