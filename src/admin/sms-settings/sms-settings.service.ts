import { Inject, Injectable } from '@nestjs/common';
import { CreateSmsSettingsDto } from './dto/create-sms-setting.dto';
import { UpdateSmsSettingsDto } from './dto/update-sms-setting.dto';
import SmsSettings from 'src/core/database/models/SmsSettings';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import { SMS_SETTINGS_REPOSITORY } from 'src/core/constants';

@Injectable()
export class SmsSettingsService extends BaseService<SmsSettings> {
  protected model: ModelCtor<SmsSettings>;
  constructor(
    @Inject(SMS_SETTINGS_REPOSITORY)
    private readonly smsSettingsRepo: typeof SmsSettings,
  ) {
    super();
    this.model = this.smsSettingsRepo;
  }
  // create(body: CreateSmsSettingsDto) {
  //   return SmsSettings.create(body);
  // }

  // findAll() {
  //   const result = SmsSettings.findAll();
  //   return result;
  // }

  // findOne(Id: string) {
  //   const result = SmsSettings.findOne({
  //     where: { id: Id },
  //   });
  //   return result;
  // }

  // update(Id: string, Body: UpdateSmsSettingsDto) {
  //   const result = SmsSettings.update(Body, {
  //     where: { id: Id },
  //   });
  //   return result;
  // }
}
