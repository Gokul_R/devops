import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { SmsSettingsService } from './sms-settings.service';
import { CreateSmsSettingsDto } from './dto/create-sms-setting.dto';
import { UpdateSmsSettingsDto } from './dto/update-sms-setting.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import { BaseController } from '../../base.controller';
import { logger } from 'src/core/utils/logger';


@ApiTags('sms-settings')
@Controller('sms-settings')
export class SmsSettingsController extends BaseController<CreateSmsSettingsDto> {
  constructor(private readonly smsSettingsService: SmsSettingsService) {
    super(smsSettingsService);
  }

  @Post()
  // @UsePipes(new ValidationPipe())
  async create(@Body() createSmsSettingsDto: CreateSmsSettingsDto) {
    try {
      logger.info(`Create_SmsSettings_Entry: ${JSON.stringify(createSmsSettingsDto)}`);
      let data = await this.smsSettingsService.create(createSmsSettingsDto);
      logger.info(`Create_SmsSettings_Exit: ${JSON.stringify(data)}`);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get()
  // async findAll() {
  //   try {
  //     let data = await this.smsSettingsService.findAll();
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  // @Get(':id')
  // async findOne(@Param('id') id: string) {
  //   try {
  //     let data = await this.smsSettingsService.findOne(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() body: UpdateSmsSettingsDto) {
    try {
      logger.info(`Update_SmsSettings_Entry_Id: ${JSON.stringify(id)}, Data:${JSON.stringify(body)}`);
      let data = await this.smsSettingsService.update(id, body);
      logger.info(`Update_SmsSettings_Exit: ${JSON.stringify(body)}`);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
