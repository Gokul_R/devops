import { Inject, Injectable } from '@nestjs/common';
import { logger } from 'src/core/utils/logger';
import { LogoutDto } from './dto/logOut.dto';
import { Provided_By } from 'src/core/constants';
import AdminUser from 'src/core/database/models/AdminUser';
import Vendor from 'src/core/database/models/Vendor';
import Branch from 'src/core/database/models/Branch';
import { Sequelize } from 'sequelize';
import Logo from 'src/core/database/models/Logo';
import Footer from 'src/core/database/models/Footer';
const fs = require('fs');
const moment = require('moment');
// import data from '../../logs/';
const logFilePath = './logs/CombinedLogs.log';

@Injectable()
export class AdminService {
  getHello(): string {
    return 'Hello World!';
  }

  // async updateForceUpdateVersion(req_body: UpdateForceUpdate) {
  //   logger.info('Force_update_version_entry: ' + JSON.stringify(req_body));
  //   const getVersions = await FroceUpdate.findOne({ where: { device_type: req_body.device_type }, order: [['created_at', 'DESC']] });
  //   if (getVersions && getVersions?.dataValues) {
  //     const data = await FroceUpdate.update({ version: req_body.version }, { where: { device_type: req_body.device_type }, returning: true });
  //     logger.info('Force_update_version_exit: ' + JSON.stringify(data[1][0]));
  //     return data[1][0];
  //   } else {
  //     logger.info('Force_update_version_exit: ' + JSON.stringify(req_body));
  //     return await FroceUpdate.create({ ...req_body });
  //   }
  // }

  async logOut(req_body: LogoutDto) {
    logger.info('Logout_entry: ' + JSON.stringify(req_body));
    const checkModel: any =
      req_body.user_type === Provided_By.SUPER_ADMIN
        ? AdminUser
        : req_body.user_type === Provided_By.VENDOR
        ? Vendor
        : req_body.user_type === Provided_By.BRANCH
        ? Branch
        : '';
    const checUsers = await checkModel.count({ where: { id: req_body.user_id } });
    if (!checUsers) {
      logger.info('Logout_User_Not_Found: ' + JSON.stringify(req_body));
      throw new Error('Logout User Not Found');
    } else {
      const updateTime = await checkModel.update({ last_login: new Date() }, { where: { id: req_body.user_id } });
      logger.info('Logout_exit_200: ' + JSON.stringify(req_body));
      return;
    }
  }

  async logoFooter() {
    const logo = await Logo.findOne({
      attributes: ['header_logo', 'footer_logo', 'favicon'],
      order: [['updated_at', 'DESC']],
      raw: true,
      where: { is_active: true, is_deleted: false },
    });
    const footer = await Footer.findOne({ attributes: ['footer_text', 'copyright_text'] });
    return { ...logo, ...footer?.dataValues };
  }

  // async showLogs(body): Promise<any> {
  //   if (body.date) {
  //     const providedDate = new Date(body.date);
  //     const currentDate = new Date();
  //     const differenceInDays = Math.ceil((currentDate.getTime() - providedDate.getTime()) / (1000 * 60 * 60 * 24));
  //     if (differenceInDays > 20) {
  //       return { message: "Can't find logs more than 30 days in the past" };
  //     }
  //     if (providedDate > currentDate) {
  //       return { message: 'The date should be earlier than todays date.' };
  //     }
  //   }

  //   return new Promise((resolve, reject) => {
  //     fs.readFile(logFilePath, 'utf8', (err, data) => {
  //       if (err) {
  //         console.error('Error reading log file:', err);
  //         reject(err);
  //         return;
  //       }
  //       const lines = data.trim().split('\n');
  //       if (lines.length === 0 || (lines.length === 1 && lines[0] === '')) {
  //         resolve({ message: 'No logs found' });
  //         return;
  //       }
  //       const logs = lines
  //         .map((line, index) => {
  //           try {
  //             return JSON.parse(line);
  //           } catch (e) {
  //             console.error(`Error parsing JSON at line ${index + 1}:`, e);
  //             console.error('Problematic line:', line);
  //             return null;
  //           }
  //         })
  //         .filter((log) => log !== null);

  //       // Filter logs based on provided criteria in the body
  //       const filteredLogs = logs.filter((log) => {
  //         if (
  //           (!body.level || log.level == body.level) &&
  //           (!body.date || log.date == body.date) &&
  //           (!body.type || log.type == body.type)
  //         ) {
  //           return true;
  //         }
  //         return false;
  //       });

  //       filteredLogs.sort((a, b) => {
  //         const dateA = new Date(a.timestamp).getTime();
  //         const dateB = new Date(b.timestamp).getTime();
  //         return dateB - dateA;
  //       });

  //       resolve(filteredLogs);
  //     });
  //   });
  // }

  // async showLogs(body) {
  //   const { level, type, fromDate, toDate } = body;

  //   // Parse fromDate and toDate if provided
  //   const fromDateObj = fromDate ? new Date(fromDate) : null;
  //   const toDateObj = toDate ? new Date(toDate) : null;

  //   // Validate fromDate and toDate
  //   if (fromDateObj && isNaN(fromDateObj.getTime())) {
  //     return Promise.resolve({ message: 'Invalid fromDate format' });
  //   }

  //   if (toDateObj && isNaN(toDateObj.getTime())) {
  //     return Promise.resolve({ message: 'Invalid toDate format' });
  //   }

  //   // Read log file
  //   return new Promise((resolve, reject) => {
  //     fs.readFile(logFilePath, 'utf8', (err, data) => {
  //       if (err) {
  //         console.error('Error reading log file:', err);
  //         reject(err);
  //         return;
  //       }
  //       const lines = data.trim().split('\n');
  //       if (lines.length === 0 || (lines.length === 1 && lines[0] === '')) {
  //         resolve({ message: 'No logs found' });
  //         return;
  //       }
  //       const logs = lines
  //         .map((line, index) => {
  //           try {
  //             return JSON.parse(line);
  //           } catch (e) {
  //             console.error(`Error parsing JSON at line ${index + 1}:`, e);
  //             console.error('Problematic line:', line);
  //             return null;
  //           }
  //         })
  //         .filter((log) => log !== null);

  //       // Filter logs based on provided criteria
  //       let filteredLogs = logs;

  //       if (fromDateObj && toDateObj) {
  //         filteredLogs = logs.filter((log) => {
  //           const logDate = new Date(log.timestamp);
  //           return logDate >= fromDateObj && logDate <= toDateObj;
  //         });
  //       } else if (fromDateObj) {
  //         filteredLogs = logs.filter((log) => {
  //           const logDate = new Date(log.timestamp);
  //           return logDate >= fromDateObj;
  //         });
  //       } else if (toDateObj) {
  //         filteredLogs = logs.filter((log) => {
  //           const logDate = new Date(log.timestamp);
  //           return logDate <= toDateObj;
  //         });
  //       }

  //       if (level) {
  //         filteredLogs = filteredLogs.filter((log) => log.level === level);
  //       }

  //       if (type) {
  //         filteredLogs = filteredLogs.filter((log) => log.type === type);
  //       }

  //       // Sort logs by timestamp descending
  //       filteredLogs.sort((a, b) => new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime());

  //       // Calculate total count
  //       const totalCount = filteredLogs.length;

  //       resolve({
  //         totalCount,
  //         logs: filteredLogs,
  //       });
  //     });
  //   });
  // }

  async showLogs(body) {
    const { level, type, fromDate, toDate, page, limit } = body;
    const pageNum = page ? parseInt(page) : 1;
    const limitNum = limit ? parseInt(limit) : 50;

    // Parse fromDate and toDate if provided
    const fromDateObj = fromDate ? new Date(fromDate) : null;
    const toDateObj = toDate ? new Date(toDate) : null;

    // Validate fromDate and toDate
    if (fromDateObj && isNaN(fromDateObj.getTime())) {
      return Promise.resolve({ message: 'Invalid fromDate format' });
    }

    if (toDateObj && isNaN(toDateObj.getTime())) {
      return Promise.resolve({ message: 'Invalid toDate format' });
    }

    // Read log file
    return new Promise((resolve, reject) => {
      fs.readFile(logFilePath, 'utf8', (err, data) => {
        if (err) {
          console.error('Error reading log file:', err);
          reject(err);
          return;
        }
        const lines = data.trim().split('\n');
        if (lines.length === 0 || (lines.length === 1 && lines[0] === '')) {
          resolve({ message: 'No logs found' });
          return;
        }
        const logs = lines
          .map((line, index) => {
            try {
              return JSON.parse(line);
            } catch (e) {
              console.error(`Error parsing JSON at line ${index + 1}:`, e);
              console.error('Problematic line:', line);
              return null;
            }
          })
          .filter((log) => log !== null);

        // Filter logs based on provided criteria
        let filteredLogs = logs;

        if (fromDateObj && toDateObj) {
          filteredLogs = logs.filter((log) => {
            const logDate = new Date(log.timestamp);
            return logDate >= fromDateObj && logDate <= toDateObj;
          });
        } else if (fromDateObj) {
          filteredLogs = logs.filter((log) => {
            const logDate = new Date(log.timestamp);
            return logDate >= fromDateObj;
          });
        } else if (toDateObj) {
          filteredLogs = logs.filter((log) => {
            const logDate = new Date(log.timestamp);
            return logDate <= toDateObj;
          });
        }

        if (level) {
          filteredLogs = filteredLogs.filter((log) => log.level === level);
        }

        if (type) {
          filteredLogs = filteredLogs.filter((log) => log.message.type === type);
        }

        // Sort logs by timestamp descending
        filteredLogs.sort((a, b) => new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime());

        // Calculate total count
        const totalCount = filteredLogs.length;

        // Implement pagination
        const startIndex = (pageNum - 1) * limitNum;
        const endIndex = startIndex + limitNum;
        const paginatedLogs = filteredLogs.slice(startIndex, endIndex);

        resolve({
          totalCount,
          logs: paginatedLogs,
        });
      });
    });
  }

  deleteOldLogs() {
    const thirtyDaysAgo = moment().subtract(30, 'days').toDate(); // Get the date 20 days ago

    fs.readFile(logFilePath, 'utf8', (err, data) => {
      if (err) {
        console.error('Error reading CombinedLogs.log:', err);
        return; // Return early if there's an error
      }

      if (!data) {
        console.error('No data found in CombinedLogs.log');
        return; // Return early if no data is found
      }

      const lines = data.trim().split('\n');
      const filteredLogs = lines.filter((line) => {
        try {
          const logData = JSON.parse(line);
          const logDate = moment(logData.timestamp, 'YYYY-MM-DD hh:mm:ss.SSS A').toDate();
          return logDate >= thirtyDaysAgo;
        } catch (error) {
          console.error('Error parsing log entry:', error);
          return false;
        }
      });
      const logData = filteredLogs.flat().join('\n');

      fs.writeFile(logFilePath, logData, (err) => {
        if (err) {
          console.error('Error writing CombinedLogs.log:', err);
        } else {
          console.log('Deleted old logs from CombinedLogs.log');
        }
      });
    });
  }
}
