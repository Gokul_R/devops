import { Inject, Injectable } from '@nestjs/common';
import { Multer } from 'multer';

import {
  CreateServiceDto,
  GetBrnachServices,
  ServiceArrayDto,
  UpdateServiceDto,
} from './dto/service.dto';
import Service from 'src/core/database/models/Services';
import {
  DISCOUNT_SERVICE_REPOSITORY,
  EC200,
  EM104,
  EM105,
  EM116,
  EM131,
  FLAT_DISCOUNT_STRATEGY,
  MASTER_CATEGORY_REPOSITORY,
  MASTER_SERVICE_CATEGORY_REPOSITORY,
  PERCENTAGE_DISCOUNT_STRATEGY,
} from 'src/core/constants';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { DiscountService } from 'src/core/utils/discount/discount-service';
import Branch from 'src/core/database/models/Branch';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import MasterCategory from 'src/core/database/models/MasterCategory';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import { MASTER_CATEGORIES_ATTRIBUTES } from 'src/core/attributes/master_category';
import { MASTER_SERVICE_CATEGORY_ATTRIBUTES } from 'src/core/attributes/master_service_category';

import { MasterCustomerTypesService } from '../master-customer-types/master-category.service';
import { MasterServiceCategoriesService } from '../master-service-categories/master-service-categories.service';
import { FileUtils } from 'src/core/utils/fileUtils';
import Vendor from 'src/core/database/models/Vendor';
import { VENDOR_ATTRIBUTES } from 'src/core/attributes/vendor';
import { Op } from 'sequelize';
@Injectable()
export class ServicesService extends BaseService<Service> {
  protected model: ModelCtor<Service>;
  service_qry: SequelizeFilter<Service>;
  constructor(
    @Inject(DISCOUNT_SERVICE_REPOSITORY)
    private readonly discountService: DiscountService,
    @Inject(MASTER_CATEGORY_REPOSITORY)
    private readonly masterCategoryRepo: typeof MasterCategory,
    @Inject(MASTER_SERVICE_CATEGORY_REPOSITORY)
    private readonly masterServiceCategoryRepo: typeof MasterServiceCategory,
  ) {
    super();
    this.model = Service;
    this.init();
  }
  init() {
    this.service_qry = {
      include: [
        {
          attributes: [BRANCH_ATTRIBUTES.BRANCH_NAME],
          model: Branch,
          include: [
            { attributes: [VENDOR_ATTRIBUTES.VENDOR_NAME], model: Vendor },
          ],
        },
        {
          attributes: [MASTER_CATEGORIES_ATTRIBUTES.category],
          model: this.masterCategoryRepo,
        },
        {
          attributes: [
            MASTER_SERVICE_CATEGORY_ATTRIBUTES.service_category_name,
          ],
          model: this.masterServiceCategoryRepo,
        },
      ],
    };
  }
  async create(createServiceDto: CreateServiceDto) {
    let data: any = createServiceDto;
    data.discounted_price = (
      createServiceDto.amount -
      (createServiceDto.amount * createServiceDto.discount) / 100
    ).toFixed(2);
    return super.create(data);
  }

  async bulkCreates(createServiceDto: ServiceArrayDto) {
    let serviceExcelData = createServiceDto.services;
    let serviceList: any[] = [];
    let non_insert_serviceList: any[] = [];
    if (serviceExcelData && Array.isArray(serviceExcelData)) {
      await Promise.all(
        serviceExcelData.map(async (ele: any) => {
          let [masterCategoryId, masterServiceCategoriesId, branchId] =
            await Promise.all([
              this.masterCategoryRepo.findOne({
                attributes: [MASTER_CATEGORIES_ATTRIBUTES.id],
                where: { code: ele.category_code }, //ele.CATEGORY
              }),
              this.masterServiceCategoryRepo.findOne({
                attributes: [MASTER_SERVICE_CATEGORY_ATTRIBUTES.id],
                where: { code: ele.service_category_code }, //ele.SERVICECATEGORY
              }),
              Branch.findOne({
                attributes: [BRANCH_ATTRIBUTES.ID],
                where: { reg_id: ele.reg_id }, //ele.BRANCH
              }),
            ]);
          if (
            masterCategoryId?.id &&
            masterServiceCategoriesId?.id &&
            branchId?.id
          ) {
            serviceList.push({
              branch_id: branchId?.id,
              category_id: masterCategoryId?.id,
              service_category_id: masterServiceCategoriesId?.id,
              description: ele.description,
              amount: ele.amount,
              service_name: ele.service_name,
              duration: ele.duration,
              discount: ele.discount,
              discounted_price: ele.amount - (ele.amount * ele.discount) / 100,
            });
          } else {
            non_insert_serviceList.push({
              error: EM131,
              branch_id: branchId?.id,
              category_id: masterCategoryId?.id,
              service_category_id: masterServiceCategoriesId?.id,
              description: ele.description,
              amount: ele.amount,
              service_name: ele.service_name,
              duration: ele.duration,
              discount: ele.discount,
              discounted_price: ele.amount - (ele.amount * ele.discount) / 100,
              bridal_service:ele?.bridal_service
            });
          }
        }),
      );
    }
    await super.bulkCreate(serviceList);
    return {
      rejected_services: non_insert_serviceList,
    };
  }
  async servicesImport(file: any) {
    let serviceList: any[] = [];
    let serviceExcelData = FileUtils.xlsxToJsonConvertor(file);
    if (serviceExcelData && Array.isArray(serviceExcelData)) {
      await Promise.all(
        serviceExcelData.map(async (ele: any) => {
          let [masterCategoryId, masterServiceCategoriesId, branchId] =
            await Promise.all([
              MasterCategory.findOne({
                attributes: [MASTER_CATEGORIES_ATTRIBUTES.id],
                where: { code: 'MC_1' }, //ele.CATEGORY
              }),
              MasterServiceCategory.findOne({
                attributes: [MASTER_SERVICE_CATEGORY_ATTRIBUTES.id],
                where: { code: 'MSC_1' }, //ele.SERVICECATEGORY
              }),
              Branch.findOne({
                attributes: [BRANCH_ATTRIBUTES.ID],
                where: { reg_id: 'SLAY3521' }, //ele.BRANCH
              }),
            ]);
          if (
            (masterCategoryId?.id && masterServiceCategoriesId?.id,
              branchId?.id)
          ) {
            serviceList.push({
              branch_id: branchId.id,
              // image: ele.IMAGE || '',
              category_id: masterCategoryId.id,
              service_category_id: masterServiceCategoriesId.id,
              description: ele.DESCRIPTION,
              amount: ele.AMOUNT,
              service_name: ele['SERVICE TITLE'],
              duration: ele.DURATION,
              discount: ele['DISCOUNT IN %'],
              service_price:
                ele.AMOUNT - (ele.AMOUNT * ele['DISCOUNT IN %']) / 100,
            });
          }
        }),
      );
    }
    return await super.bulkCreate(serviceList);
  }

  async update(id: string, updateServiceDto: UpdateServiceDto) {
    let reqData: any = updateServiceDto;
    if (updateServiceDto.amount) {
      reqData.discounted_price = (
        updateServiceDto.amount -
        (updateServiceDto.amount * updateServiceDto.discount) / 100
      ).toFixed(2);
    }
    return await super.update(id, reqData);
  }

  async findAllByBranchId(
    branch_id: string[],
    servCategoryId: string,
  ): Promise<Service> {
    // let data =  branch_id && servCategoryId ? {branch_id:branch_id} && {service_category_id:servCategoryId}:'';
    // console.log(data);
    // let condition;

    //     {
    //   condition = {
    //     [Op.and]: [
    //      branch_id ? { branch_id: branch_id }:'',
    //      servCategoryId ? {service_category_id:servCategoryId} :'',

    //       { is_active: true },
    //       { is_deleted: false },
    //       { deleted_at: null },
    //     ],
    //   };
    // }
    let branchservicecatId: any =
      branch_id?.length > 0 && servCategoryId
        ? { service_category_id: servCategoryId, branch_id: branch_id }
        : servCategoryId
          ? { service_category_id: servCategoryId }
          : branch_id?.length > 0
            ? { branch_id: branch_id }
            : '';

    const whereCondition = {
      ...branchservicecatId,
      is_active: true,
      is_deleted: false,
      deleted_at: null,
    };

    this.init();
    this.service_qry.where = whereCondition;
    // if (servCategoryId)
    //   this.service_qry.where.service_category_id = servCategoryId;
    return await super.findAll(this.service_qry);
  }
  async findAllByBranchIdSearch(req_data: GetBrnachServices): Promise<Service> {
    let search;
    let service_category_id: any = req_data.service_category_ids ? { service_category_id: req_data.service_category_ids } : '';
    let category_id: any = req_data.category_ids ? { category_id: req_data.category_ids } : '';
    let branch_id: any = req_data.branch_ids ? { branch_id: req_data.branch_ids } : '';
    if (req_data.searchText) {
      search = {
        [Op.or]: [
          {
            service_name: {
              [Op.iLike]: `%${req_data.searchText.toLowerCase()}%`,
            },
          }, // Convert search value to lowercase
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let where = {
      where: {
        ...branch_id,
        ...service_category_id,
        ...category_id,
        is_deleted: false,
        ...search,
      },
      offset: offset,
      limit: limit,
      order: [['created_at', 'DESC']]
    };
    let query = {
      ...where,
      include: [
        {
          attributes: [BRANCH_ATTRIBUTES.BRANCH_NAME],
          model: Branch,
          include: [
            { attributes: [VENDOR_ATTRIBUTES.VENDOR_NAME], model: Vendor },
          ],
        },
        {
          attributes: [MASTER_CATEGORIES_ATTRIBUTES.category],
          model: this.masterCategoryRepo,
        },
        {
          attributes: [
            MASTER_SERVICE_CATEGORY_ATTRIBUTES.service_category_name,
          ],
          model: this.masterServiceCategoryRepo,
        },
      ],
    };
    return await super.findAndCountAll(query);
  }
}
