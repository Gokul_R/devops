import { ApiProperty, OmitType, PartialType, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  ArrayNotEmpty,
  IsArray,
  IsBoolean,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Length,
  Matches,
  ValidateNested,
  matches,
} from 'class-validator';
import { BRANCH_ID, SERVICE_ID } from 'src/core/constants';
import { PaginationDto, SharedDto } from 'src/core/interfaces/shared.dto';

export class CreateServiceDto extends PickType(SharedDto, [
  'branch_id',
  'image',
  // 'description',
  'category_id',
  'service_category_id',
  // 'amount',
]) {
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({example:false})
  @IsBoolean()
  @IsOptional()
  bridal_service: boolean;

  @ApiProperty()
  @IsNotEmpty()
  @Length(3, 100, { message: 'service name must be 3 to 100' })
  // @Matches(/^[A-Za-z\s]+$/, {
  //   message: ' In service_name Only letters are allowed',
  // })
  service_name: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  duration: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  amount: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  discount: number;
}
export class CreateServiceArrayDto extends PickType(SharedDto, [
  'reg_id',
  'image',
  'category_code',
  'service_category_code',
]) {
  @ApiProperty()
  @IsNotEmpty()
  service_name: string;

  @ApiProperty({example:false})
  @IsBoolean()
  @IsOptional()
  bridal_service: boolean;

  @ApiProperty()
  @IsString()
  @IsOptional()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  duration: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  amount: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  discount: number;
}
export class ServiceArrayDto {
  @ApiProperty({ type: [CreateServiceArrayDto] })
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => CreateServiceArrayDto)
  services: CreateServiceArrayDto[];
}
export class UpdateServiceDto extends PartialType(CreateServiceDto) { }
export class GetServicesByBranchDto extends PickType(SharedDto, ['branch_ids']) { 
  // @ApiProperty({example:false})
  // @IsBoolean()
  // @IsNotEmpty()
  // bridal_service: boolean;
}

export class GetBrnachServices extends PaginationDto {
  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional()
  @IsArray()
  branch_ids?: string[];

  // @ApiProperty({example:false})
  // @IsBoolean()
  // @IsNotEmpty()
  // bridal_service: boolean;

  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional()
  @IsArray()
  category_ids?: string[];

  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional()
  @IsArray()
  service_category_ids?: string[];

}

export class MultipleServiceDelete {
  @ApiProperty({ example: [SERVICE_ID] })
  @IsArray()
  @ArrayNotEmpty() // Ensure the array itself is not empty
  @IsString({ each: true }) // Ensure each element is a string
  @IsNotEmpty({ each: true }) // Ensure each string is not empty
  service_ids?: string[];
}