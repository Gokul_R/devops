import { Module } from '@nestjs/common';
import { ServicesService } from './services.service';
import { ServicesController } from './services.controller';
import { PercentageDiscountStrategy } from 'src/core/utils/discount/percentage-discount-strategy';
import { FlatDiscountStrategy } from 'src/core/utils/discount/flat-discount-strategy';
import { servicesProviders } from './services.providers';

@Module({
  imports:[PercentageDiscountStrategy,FlatDiscountStrategy], 
  controllers: [ServicesController],
  providers: [ServicesService,...servicesProviders]
})
export class ServicesModule {}
