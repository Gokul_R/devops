import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  UsePipes,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { ServicesService } from './services.service';
import { CreateServiceDto, GetBrnachServices, GetServicesByBranchDto, MultipleServiceDelete, ServiceArrayDto, UpdateServiceDto } from './dto/service.dto';
import { ApiBody, ApiConsumes, ApiQuery, ApiTags } from '@nestjs/swagger';
import { EC200, EM106, EM100, EM104, EM127, EM116 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { FileInterceptor } from '@nestjs/platform-express';
import { Multer } from 'multer';
import Service from 'src/core/database/models/Services';


@Controller('services')
@ApiTags('services')
export class ServicesController {
  constructor(private readonly servicesService: ServicesService) { }

  @Post()
  async create(@Body() createServiceDto: CreateServiceDto) {
    try {
      let data = await this.servicesService.create(createServiceDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('services-import')
  // @UsePipes(new ValidationPipe())
  async bulkCreate(@Body() createServiceDto: ServiceArrayDto) {
    try {
      let data = await this.servicesService.bulkCreates(createServiceDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  // @Post('services-import')
  // @ApiConsumes('multipart/form-data')
  // @ApiBody({
  //   schema: {
  //     type: 'object',
  //     properties: {
  //       file: {
  //         type: 'string',
  //         format: 'binary',
  //       },
  //     },
  //   },
  // })
  // @UseInterceptors(FileInterceptor('file'))
  // async servicesImport(@UploadedFile() file:Multer.File): Promise<any> {
  //   try {
  //     let data = await this.servicesService.servicesImport(file);
  //     return HandleResponse.buildSuccessObj(EC200, EM104, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }

  // }

  @Get()
  async findAll() {
    try {
      let data = await this.servicesService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @ApiQuery({
    name: 'service_cat_id',
    type: String,
    description: 'Service Category ID',
    required: false,
  })
  @Post('by-branch')
  async findAllByBranchId(@Body() req_dto: GetServicesByBranchDto, @Query('service_cat_id') service_cat_id?: string) {
    try {
      let data = await this.servicesService.findAllByBranchId(req_dto.branch_ids, service_cat_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('by-branch/search')
  async findAllByBranchIdSearch(@Body() req_dto: GetBrnachServices) {
    try {
      let data = await this.servicesService.findAllByBranchIdSearch(req_dto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.servicesService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateServiceDto: UpdateServiceDto) {
    try {
      let data = await this.servicesService.update(id, updateServiceDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let data = await this.servicesService.destroy(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('destroy')
  async multipleremove(@Body() service_ids: MultipleServiceDelete) {
    try {
      let data1 = await Service.update({ is_deleted: true }, { where: { id: service_ids.service_ids } });
      let data = await Service.destroy({ where: { id: service_ids.service_ids } });
      return HandleResponse.buildSuccessObj(EC200, EM127, null);
    } catch (error) {
      return HandleResponse.buildErrObj(null, error?.message || EM100, error);
    }
  }
}
