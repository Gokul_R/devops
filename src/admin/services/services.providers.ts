import { commonProvider } from "src/core/interfaces/common-providers";


export const servicesProviders = [
    // {
    //   provide: SERVICES_REPOSITORY,
    //   useValue: Service,
    // },
    // {
    //   provide: DISCOUNT_SERVICE_REPOSITORY, 
    //   useValue: DiscountService,
    // },
  commonProvider.service_repository,
  commonProvider.discount_service_repository,
  commonProvider.master_category_repository,
  commonProvider.master_service_category_repository
  ];