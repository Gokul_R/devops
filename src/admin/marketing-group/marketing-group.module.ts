import { Module } from '@nestjs/common';
import { MarketingGroupService } from './marketing-group.service';
import { MarketingGroupController } from './marketing-group.controller';
import { marketingGroupProviders } from './marketing-group.provider';

@Module({
  controllers: [MarketingGroupController],
  providers: [MarketingGroupService,...marketingGroupProviders]
})
export class MarketingGroupModule {}
