import { Inject, Injectable } from '@nestjs/common';
import { BaseService } from '../../base.service';

import MarketingGroup from 'src/core/database/models/MarketingGroup';
import { ModelCtor } from 'sequelize-typescript';
import { MARKETING_REPOSITORY } from 'src/core/constants';
import { Op } from 'sequelize';
import ReferalManage from 'src/core/database/models/ReferalManage';

@Injectable()
export class MarketingGroupService extends BaseService<MarketingGroup> {
  protected model: ModelCtor<MarketingGroup>;
  constructor(
    @Inject(MARKETING_REPOSITORY)
    private readonly marketingGroupRepo: typeof MarketingGroup,
  ) {
    super();
    this.model = this.marketingGroupRepo;

  }

  async getFilterList(req_data: any) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { group_name: { [Op.iLike]: `%${searchText}%` } },
          { description: { [Op.iLike]: `%${searchText}%` } },
          { '$referal.referal_name$': { [Op.iLike]: `%${searchText}%` } },
          { '$referal.referal_code$': { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await this.findAndCountAll({
      include: [{
        attributes: ['id', 'referal_name', 'description', 'referal_code'],
        model: ReferalManage
      }],
      where: {
        ...search,
        is_deleted: false,
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']],
    });
  }
  //   async create(createMarketingGroupDto: CreateMarketingGroupDto) {
  //     return await  super.create(createMarketingGroupDto)

  //   }

  //  async  findAll() {
  //     return await super.findAll()
  //   }

  //   async findOne(id: string) {
  //     return await super.findOne(id)
  //   }

  // async  update(id: string, updateMarketingGroupDto: UpdateMarketingGroupDto) {
  //   return await super.update(id,updateMarketingGroupDto)
  // }

  // async remove(id: string) {
  //   return await super.remove(id)
  // }
}
