import { Test, TestingModule } from '@nestjs/testing';
import { MarketingGroupService } from './marketing-group.service';

describe('MarketingGroupService', () => {
  let service: MarketingGroupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MarketingGroupService],
    }).compile();

    service = module.get<MarketingGroupService>(MarketingGroupService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
