import { ApiProperty, PartialType } from "@nestjs/swagger";
import { IsArray, IsBoolean, IsNotEmpty, IsOptional, IsString, ValidateIf } from "class-validator";
import { BRANCH_ID } from "src/core/constants";

export class CreateMarketingGroupDto {
    @IsBoolean()
    @IsOptional()
    is_active?: boolean;

    @ApiProperty({ example: 'IYYAPANTHAGNAL120' })
    @IsString()
    group_name: string;

    @ApiProperty({ example: 'Only Iyyapan thangal' })
    @IsString()
    description: string;

    // @ApiProperty({ example: false })
    // @IsNotEmpty()
    // @IsBoolean()
    // is_user_mapping: boolean;

    @ApiProperty({ example: true })
    @IsNotEmpty()
    @IsBoolean()
    is_referel: boolean;

    @ApiProperty({ example: ['+9198873290234', '+91982379737'] })
    @IsArray()
    // @ValidateIf((object, value) => object.is_coupen) // Validate only if is_coupen is true
    @IsNotEmpty({ each: true }) // Validate each element of the array is not empty 
    users_mobile: string[];

    @ApiProperty({ example: BRANCH_ID })
    @IsString()
    @ValidateIf((object, value) => object.is_referel) // Validate only if is_referel is true
    referal_id: string;

    @ApiProperty({ example: BRANCH_ID })
    @IsString()
    @ValidateIf((object, value) => object.is_referel) // Validate only if is_referel is true
    referal_code: string;

}

export class UpdateMarketingGroupDto extends PartialType(CreateMarketingGroupDto) { }