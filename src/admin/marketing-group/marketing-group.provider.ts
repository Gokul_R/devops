import { commonProvider } from 'src/core/interfaces/common-providers';

export const marketingGroupProviders = [
  // {
  //   provide: MARKETING_REPOSITORY,
  //   useValue: MarketingGroup,
  // }
  commonProvider.marketing_group_repository,
];
