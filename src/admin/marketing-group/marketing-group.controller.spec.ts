import { Test, TestingModule } from '@nestjs/testing';
import { MarketingGroupController } from './marketing-group.controller';
import { MarketingGroupService } from './marketing-group.service';

describe('MarketingGroupController', () => {
  let controller: MarketingGroupController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MarketingGroupController],
      providers: [MarketingGroupService],
    }).compile();

    controller = module.get<MarketingGroupController>(MarketingGroupController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
