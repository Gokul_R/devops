import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MarketingGroupService } from './marketing-group.service';
import { CreateMarketingGroupDto, UpdateMarketingGroupDto } from './dto/marketing-group.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EC409, EC412, EM100, EM101, EM104, EM106, EM116, EM127, EM135 } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@Controller('marketing-group')
@ApiTags('marketing-group')
export class MarketingGroupController {
  constructor(private readonly marketingGroupService: MarketingGroupService) { }

  @Post()
  async create(@Body() createMarketingGroupDto: CreateMarketingGroupDto) {
    try {
      let ifExites = await this.marketingGroupService.findAll({ where: { group_name: createMarketingGroupDto.group_name } });
      if (ifExites && ifExites.length > 0) {
        return HandleResponse.buildErrObj(EC412, EM135, EM135);
      }
      if (createMarketingGroupDto?.users_mobile.length > 0) {
        createMarketingGroupDto.users_mobile = createMarketingGroupDto?.users_mobile.map(number => {
          if (!number.startsWith('+91')) {
            return '+91' + number;
          }
          return number;
        });
      }
      let result = await this.marketingGroupService.create(createMarketingGroupDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, result);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get()
  async findAll() {
    try {
      let result = await this.marketingGroupService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, result);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);

    }
  }
  @Post('search')
  async getFilterList(@Body() getFilterList: PaginationDto) {
    try {
      let result = await this.marketingGroupService.getFilterList(getFilterList);
      return HandleResponse.buildSuccessObj(EC200, EM106, result);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let result = await this.marketingGroupService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, result);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);

    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateMarketingGroupDto: UpdateMarketingGroupDto) {
    try {
      if (updateMarketingGroupDto?.users_mobile.length > 0) {
        updateMarketingGroupDto.users_mobile = updateMarketingGroupDto?.users_mobile.map(number => {
          if (!number.startsWith('+91')) {
            return '+91' + number;
          }
          return number;
        });
      }
      let result = await this.marketingGroupService.update(id, updateMarketingGroupDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, result);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);

    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let result = await this.marketingGroupService.remove(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, result);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);

    }
  }
}
