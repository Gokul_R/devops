import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsNotEmpty,
  IsEmail,
  IsPhoneNumber,
  IsEnum,
  IsBoolean,
  IsOptional
} from 'class-validator';

enum Gender {
  MALE = 'Male',
  FEMALE = 'Female',
  OTHERS = 'Others',
}

export class CreateCustomerDetailDto {
  @IsString()
  @IsOptional()
  id?: string;

  @ApiProperty({ example: 'manojkumar' })
  @IsNotEmpty()
  @IsString()
  user_name?: string;

  @ApiProperty({ example: 'Male' })
  @IsNotEmpty()
  @IsEnum(Gender)
  gender?: Gender;

  @ApiProperty({ example: '+919803457831' })
  @IsNotEmpty()
  @IsPhoneNumber()
  mobile_no?: string;

  @ApiProperty({ example: 'manoj@gmail.com' })
  @IsNotEmpty()
  @IsEmail()
  email_id?: string;

  @ApiProperty({ default: 'true' })
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @IsBoolean()
  @IsOptional()
  is_deleted?: boolean;
}
