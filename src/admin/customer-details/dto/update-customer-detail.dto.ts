import { PartialType } from '@nestjs/swagger';
import { CreateCustomerDetailDto } from './create-customer-detail.dto';

export class UpdateCustomerDetailDto extends PartialType(CreateCustomerDetailDto) {}
