import { Inject, Injectable } from '@nestjs/common';
import { CreateCustomerDetailDto } from './dto/create-customer-detail.dto';
import { UpdateCustomerDetailDto } from './dto/update-customer-detail.dto';
import { BaseService } from 'src/base.service';
import User from 'src/core/database/models/User';
import { ModelCtor } from 'sequelize-typescript';
import { CUSTOMER_DETAILS_REPOSITORY } from 'src/core/constants';
import { Op, col, fn } from 'sequelize';
import { BlockingDto, BlockingUserType } from 'src/core/interfaces/shared.dto';

@Injectable()
export class CustomerDetailsService extends BaseService<User> {
  protected model: ModelCtor<User>;
  constructor(
    @Inject(CUSTOMER_DETAILS_REPOSITORY)
    private readonly customerDetailsRepo: typeof User,
  ) {
    super();
    this.model = this.customerDetailsRepo;
  }

  async blockUnblock(req_data: BlockingDto) {
    switch (req_data.type) {
      case BlockingUserType.USER:
        const updateUserTable = await this.customerDetailsRepo.update({ is_blocked: req_data.block }, { where: { id: req_data.id }, returning: true });
        return updateUserTable[1][0];
        break;
      default:
        return null;
        break;
    }
  }

  async getFilterList(req_data: any): Promise<any> {
    let search;
    if (req_data.searchText) {
      search = {
        // [Op.or]: [
        //   fn('LOWER', col('user_name')), // Convert column value to lowercase
        //   fn('LOWER', col('email_id')), // Convert column value to lowercase
        //   fn('LOWER', col('mobile_no')), // Convert column value to lowercase
        // ],
        [Op.or]: [
          { user_name: { [Op.iLike]: `%${req_data.searchText.toLowerCase()}%` } }, // Convert search value to lowercase
          { email_id: { [Op.iLike]: `%${req_data.searchText.toLowerCase()}%` } }, // Convert search value to lowercase
          { mobile_no: { [Op.iLike]: `%${req_data.searchText.toLowerCase()}%` } }, // Convert search value to lowercase
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await this.findAndCountAll({
      attributes: ['user_name', 'id', 'profile_url', 'email_id', 'mobile_no', 'earn_coins', 'gender', 'is_active', 'is_blocked'],
      paranoid: false,
      where: {
        is_deleted: false,
        ...search
      },
      offset: offset,
      limit: limit,
      order: [['created_at', 'DESC']]
    });
  }

}
