import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CustomerDetailsService } from './customer-details.service';
import { CreateCustomerDetailDto } from './dto/create-customer-detail.dto';
import { UpdateCustomerDetailDto } from './dto/update-customer-detail.dto';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from 'src/base.controller';
import User from 'src/core/database/models/User';
import { EC200, EM106, EM100, EM104, EM116 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { BlockingDto, PaginationDto } from 'src/core/interfaces/shared.dto';

@ApiTags('Customer Details')
@Controller('customer-details')
export class CustomerDetailsController extends BaseController<CreateCustomerDetailDto> {
  constructor(private readonly customerDetailsService: CustomerDetailsService) {
    super(customerDetailsService);
  }

  // @Post()
  async create(@Body() createCustomerDetailDto: CreateCustomerDetailDto) {
    try {
      let data = await this.customerDetailsService.create(
        createCustomerDetailDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('search')
  async getFilterList(@Body() getCustomerListedto: PaginationDto) {
    try {
      let data = await this.customerDetailsService.getFilterList(getCustomerListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('block')
  async blockUnblock(@Body() getCustomerListedto: BlockingDto) {
    try {
      let data = await this.customerDetailsService.blockUnblock(getCustomerListedto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateCustomerDetailDto: UpdateCustomerDetailDto,
  ) {
    try {
      let data = await this.customerDetailsService.update(
        id,
        updateCustomerDetailDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

}
