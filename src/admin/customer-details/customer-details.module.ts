import { Module } from '@nestjs/common';
import { CustomerDetailsService } from './customer-details.service';
import { CustomerDetailsController } from './customer-details.controller';
import { customerDetailsproviders } from './customer-details.providers';

@Module({
  controllers: [CustomerDetailsController],
  providers: [CustomerDetailsService,...customerDetailsproviders]
})
export class CustomerDetailsModule {}
