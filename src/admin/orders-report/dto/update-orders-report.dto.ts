import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateOrdersReportDto } from './create-orders-report.dto';
import { Transform } from 'class-transformer';
import { IsDate, IsEnum, IsInt, IsNotEmpty, IsString, ValidateIf } from 'class-validator';
import { BRANCH_ID, ORDER_ID, Provided_By } from 'src/core/constants';
import { VendorType } from 'src/core/utils/enum';

export class UpdateOrdersReportDto extends PartialType(CreateOrdersReportDto) { }

export class RescheduleFromAdminDto {
    @ApiProperty()
    @IsDate()
    @Transform(({ value }) => value ? new Date(value) : null)
    appointment_start_date_time: Date | null;

    @ApiProperty({ example: ORDER_ID })
    @IsString()
    @IsNotEmpty()
    order_id: string;

    @ApiProperty({ example: `${Provided_By.SUPER_ADMIN} 'or' ${Provided_By.VENDOR} or ${Provided_By.BRANCH}` })
    @IsNotEmpty()
    @IsEnum(Provided_By)
    reschedule_by: string;

    @ApiProperty({ example: BRANCH_ID })
    @IsNotEmpty()
    @IsString()
    reschedule_user_id: string;

    @ApiProperty({ example: `${VendorType.FREELANCER} 'or' ${VendorType.BUSINESS}` })
    @IsNotEmpty()
    @IsEnum(VendorType)
    vendor_type: string;

    @ApiProperty()
    @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
    @IsInt()
    booking_slot: number;
}