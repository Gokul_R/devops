import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateIf,
} from 'class-validator';
import { BRANCH_ID, ORDER_ID } from 'src/core/constants';
import { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { VendorType } from 'src/core/utils/enum';

export enum GetOrderList {
  Request = 'Request',
  Orders = 'Orders',
  Earnings = 'Earnings',
  Rejected = 'Rejected',
  Confirmed = 'Confirmed',
  Cancelled = 'Cancelled',
  Pending = 'Pending',
  Awaiting_approval = 'Awaiting_approval'
}
export class CreateOrdersReportDto extends PaginationDto {
  @IsString()
  @IsOptional()
  id?: string;

  @ApiProperty({ example: '2023-08-01' })
  // @IsDateString()
  @IsOptional()
  @IsString()
  from_date?: string;

  @ApiProperty({ example: '2023-08-31' })
  @IsOptional()
  @IsString()
  // @IsDateString()
  to_date?: string;

  @ApiProperty({ example: null })
  @IsOptional()
  @IsString()
  vendor_id?: string;

  @ApiProperty({ example: null })
  @IsOptional()
  @IsString()
  branch_id?: string;

  @ApiProperty({ example: null })
  @IsOptional()
  @IsEnum(APPOINTMENT_STATUS, {
    message:
      'status must be one of the following values: Pending, Confirmed, Completed, Cancelled, Rejected',
  })
  status?: string;

  @ApiProperty({ example: `${VendorType.BUSINESS} '(or)' ${VendorType.FREELANCER}` })
  @IsOptional()
  @IsString()
  vendor_type: string;

  // @ApiProperty({ example: '' })
  // @IsOptional()
  // @IsString()
  // search?: string;
}

export class GetOrdersByBranchDto extends PaginationDto {
  @ApiProperty({ example: BRANCH_ID })
  @IsString()
  @IsOptional()
  branch_id?: string;

  @ApiProperty({ example: BRANCH_ID })
  @IsString()
  @IsOptional()
  user_id?: string;

  @ApiProperty({ example: "Request , Orders , Earnings" })
  @IsString()
  @IsNotEmpty()
  @IsEnum(GetOrderList)
  order_status?: string;

  @ApiProperty({ example: '2023-08-01' })
  @IsOptional()
  @ValidateIf((o) => o.to_date != null)
  @IsString()
  from_date?: string;

  @ApiProperty({ example: '2023-08-31' })
  @IsOptional()
  @ValidateIf((o) => o.from_date != null)
  @IsString()
  to_date?: string;
}

export class ViewOrderDto {
  @ApiProperty({ example: ORDER_ID })
  @IsString()
  @IsNotEmpty()
  order_id?: string;
}