import { Module } from '@nestjs/common';
import { OrdersReportService } from './orders-report.service';
import { OrdersReportController } from './orders-report.controller';
//import { orderReportProvider } from './order-report.provider';

@Module({
  controllers: [OrdersReportController],
  providers: [OrdersReportService,]
})
export class OrdersReportModule {}
