import { Injectable, Inject, NotFoundException, NotAcceptableException } from '@nestjs/common';
import {
  CreateOrdersReportDto,
  GetOrderList,
  GetOrdersByBranchDto,
  ViewOrderDto,
} from './dto/create-orders-report.dto';
import { Op, Sequelize } from 'sequelize';
import Branch from 'src/core/database/models/Branch';
import Order from 'src/core/database/models/Order';
import sequelize from 'sequelize';
import Service from 'src/core/database/models/Services';
import OrderService from 'src/core/database/models/OrderServices';
import Appointment, { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import MasterCategory from 'src/core/database/models/MasterCategory';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import User from 'src/core/database/models/User';
import Coupon from 'src/core/database/models/Coupons';
import { Errors } from 'src/core/constants/error_enums';
import Helpers from 'src/core/utils/helpers';
import { TriggerType } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { MailUtils } from 'src/core/utils/mailUtils';
import { RescheduleFromAdminDto } from './dto/update-orders-report.dto';
import { logger } from 'src/core/utils/logger';
import { App } from 'aws-sdk/clients/opsworks';
import OrderRemainder from 'src/core/database/models/OrderRemainder';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { MASTER_CATEGORIES_ATTRIBUTES } from 'src/core/attributes/master_category';
import FlashSale from 'src/core/database/models/FlashSale';
import { FlashSaleConstants } from 'src/modules/flash-sale/flashsale-constants';
import Payment_Txn from 'src/core/database/models/Payment-Tnx';
import { LOCATION_ATTRIBUTES } from 'src/core/attributes/location';
import { DateUtils } from 'src/core/utils/dateUtils';
import Location from 'src/core/database/models/Location';
import UserAddress from 'src/core/database/models/UserAddress';
import EmailTemService from 'src/core/utils/emailTemplate';
import { SendPushNotification } from 'src/core/utils/sendPushNotification';
import moment from 'moment';
import VendorRole from 'src/core/database/models/VendorRoles';
import { FreelancerBookingSlots, VendorType } from 'src/core/utils/enum';

@Injectable()
export class OrdersReportService {
  // protected model: ModelCtor<OrdersService>;

  async getOrderDetails(createOrdersReportDto: CreateOrdersReportDto) {
    let { from_date, to_date, branch_id, vendor_id, status, searchText, pagNo, limit, vendor_type } = createOrdersReportDto;

    const pageSize = limit || 10;
    const currentPage = pagNo || 1;
    const offset = (currentPage - 1) * pageSize;
    let searchData;
    if (searchText) {
      searchData = {
        [Op.or]: [
          { '$order->branch.branch_name$': { [Op.iLike]: `%${searchText}%` } },
          { '$order.customer_name$': { [Op.iLike]: `%${searchText}%` } },
          { '$order.order_id$': { [Op.iLike]: `%${searchText}%` } },
          { '$order.order_status$': { [Op.iLike]: `%${searchText}%` } },
          { '$service.service_name$': { [Op.iLike]: `%${searchText}%` } },
          // { '$branch.branch_name$': { [Op.iLike]: `%${searchText}%` } },
          // { customer_name: { [Op.iLike]: `%${searchText}%` } },
          // { order_id: { [Op.iLike]: `%${searchText}%` } },
          // { order_status: { [Op.iLike]: `%${searchText}%` } },
          // { '$ordersService->service.service_name$': { [Op.iLike]: `%${searchText}%` } },
        ]
      };
    }
    let dateStatus;
    let wherestatus: any = status ? { '$order.order_status$': status } : {};
    if (from_date && to_date) {
      dateStatus = {
        [Op.and]: [
          {
            '$order.created_at$': {
              // created_at: {
              [Op.gte]: `${from_date} 00:00:00.000`,
              [Op.lte]: `${to_date} 23:59:59.999`,
            },
          },
        ],
      };
    }
    let bran_id = branch_id && vendor_id ? { '$order->branch.id$': branch_id, '$order->branch.vendor_id$': vendor_id }
      : branch_id && !vendor_id ? { '$order->branch.id$': branch_id }
        : !branch_id && vendor_id ? { '$order->branch.vendor_id$': vendor_id } : {};
    let vendorRoleBranches = !branch_id && vendor_id && await VendorRole.findOne({ attributes: ['branch_ids'], where: { id: vendor_id } });
    let childvendorBranches: any = branch_id && vendor_id && await VendorRole.findOne({ attributes: ['branch_ids'], where: { branch_ids: { [Op.contains]: [branch_id] }, id: vendor_id } });
    if (childvendorBranches) {
      childvendorBranches = childvendorBranches?.branch_ids;
      childvendorBranches.branch_ids = childvendorBranches.filter(item => item == branch_id);
    }


    vendorRoleBranches = vendorRoleBranches ? vendorRoleBranches : childvendorBranches;
    let branch_ids = vendorRoleBranches ? { '$order->branch.id$': vendorRoleBranches?.branch_ids } : bran_id;
    let whereCondition = {
      where: {
        ...dateStatus,
        ...searchData,
        ...wherestatus,
        ...branch_ids
      }
    };
    // bran_id && Object.keys(bran_id).length > 0 && vendor_type ? bran_id.where['vendor_type'] = vendor_type : vendor_type ? bran_id = { where: { vendor_type: vendor_type } } : {};
    let vendoType = vendor_type ? { where: { vendor_type: vendor_type } } : {};
    let data = await OrderService.findAndCountAll({
      attributes: [],
      include: [
        {
          model: Order,
          as: 'order',
          attributes: [
            'id',
            'order_id',
            'customer_name',
            'payment_status',
            'created_at',
            'order_status',
            'appointment_start_date_time',
            'final_price',
            'booking_slot'
          ],
          required: true,
          paranoid: false,
          include: [
            {
              attributes: ['branch_name', 'vendor_id', 'id', 'vendor_type'],
              model: Branch,
              as: 'branch',
              // ...bran_id,
              // ...branch_ids,
              ...vendoType,
              paranoid: false

            },
            {
              attributes: ['profile_url'],
              model: User
            }
          ],
        },
        {
          attributes: ['service_name', 'id'],
          model: Service,
          as: 'service',
          paranoid: false,
          include: [{
            attributes: ['service_category_name', 'id'],
            model: MasterServiceCategory,
            paranoid: false
          },
          {
            attributes: ['category', 'id'],
            model: MasterCategory,
            paranoid: false
          }]
          // ...searchData,
        },
      ],
      ...whereCondition,
      paranoid: false,
      offset: offset,
      limit: pageSize,
      order: [[{ model: Order, as: 'order' }, 'created_at', 'DESC']]
    });
    return data;

    // let data: any = await Order.findAll({
    //   group: ['Order.order_id', 'branch.branch_name', 'branch.id'],
    //   attributes: ['order_id', 'customer_name', 'payment_status', 'created_at', 'updated_at', 'order_status',
    //     'final_price', 'discount', 'original_price', 'service_type', 'payment_method', 'customer_name', 'appointment_start_date_time',
    //     'customer_mobile', 'payout_amount', 'payout', 'branch_id', 'customer_email',
    //     // [sequelize.fn('COUNT', sequelize.col("Order.order_id")), 'order_count']
    //   ],
    //   include: [{
    //     attributes: ['branch_name', 'id'],
    //     model: Branch,
    //     ...bran_id
    //   },
    //   {
    //     model: OrderService,
    //     include: [{
    //       attributes: ['service_name', 'id'],
    //       model: Service,
    //       include: [{
    //         attributes: ['service_category_name', 'id'],
    //         model: MasterServiceCategory
    //       },
    //       {
    //         attributes: ['category', 'id'],
    //         model: MasterCategory
    //       }]
    //     }]
    //   },
    //   ],
    //   ...whereCondition,
    //   offset: offset,
    //   limit: pageSize,
    // });
    // data.count = data?.count?.length || 0;
    // return data;

  }
  async getByBranchRequestOrdersEarnings(req_data: GetOrdersByBranchDto) {
    let search: any;
    let orderStatus: any =
      req_data.order_status === GetOrderList.Request || req_data.order_status === GetOrderList.Pending ? { order_status: APPOINTMENT_STATUS.PENDING }
        : req_data.order_status === GetOrderList.Earnings ? { order_status: APPOINTMENT_STATUS.COMPLETED }
          : req_data.order_status === GetOrderList.Rejected ? { order_status: APPOINTMENT_STATUS.REJECTED }
            : req_data.order_status === GetOrderList.Confirmed ? { order_status: APPOINTMENT_STATUS.CONFIRMED }
              : req_data.order_status === GetOrderList.Cancelled ? { order_status: APPOINTMENT_STATUS.CANCELLED }
                : req_data.order_status === GetOrderList.Awaiting_approval ? { order_status: [APPOINTMENT_STATUS.AWAITING_APPROVAL, APPOINTMENT_STATUS.CONFIRMED] }
                  : {};
    let whereIds: any = req_data.user_id ? { user_id: req_data.user_id } : { branch_id: req_data.branch_id };
    if (req_data.searchText) {
      const searchText = req_data?.searchText?.toLowerCase();
      search = {
        [Op.or]: [
          { order_id: { [Op.iLike]: `%${searchText}%` } },
          { customer_name: { [Op.iLike]: `%${searchText}%` } },
          { order_status: { [Op.iLike]: `%${searchText}%` } },
          { customer_email: { [Op.iLike]: `%${searchText}%` } },
          { customer_mobile: { [Op.iLike]: `%${searchText}%` } },
          // { '$ordersService.service.service_name$': { [Op.iLike]: `%${searchText}%` } }
        ],
      };
    }
    let dateStatus;
    if (req_data?.from_date && req_data?.to_date) {
      dateStatus = {
        created_at: {
          [Op.gte]: new Date(`${req_data.from_date} 00:00:00.000`),
          [Op.lte]: new Date(`${req_data.to_date} 23:59:59.999`),
        },
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;

    let query: any = {
      where: {
        ...search,
        [Op.and]: [
          whereIds,
          // {
          //   deletedAt: null,
          // },
          dateStatus,
          { ...orderStatus },
        ],
      },
      attributes: ['order_id', 'customer_name', 'payment_status', 'created_at', 'updated_at', 'order_status',
        'final_price', 'discount', 'original_price', 'service_type', 'payment_method', 'customer_name', 'appointment_start_date_time', 'booking_slot',
        'customer_mobile', 'payout_amount', 'payout', 'customer_email', [sequelize.fn('COUNT', sequelize.col("Order.order_id")), 'order_count']],
      include: [
        {
          // attributes: ['service_name', 'id'],
          model: OrderService,
          paranoid: false,
          include: [{
            attributes: ['service_name', 'id'],
            model: Service,
            paranoid: false,
            include: [{
              attributes: ['service_category_name', 'id'],
              model: MasterServiceCategory,
              paranoid: false
            },
            {
              attributes: ['category', 'id'],
              model: MasterCategory,
              paranoid: false
            }]
          }]
        }],
      paranoid: false,
    };
    const getdata: any = await Order.findAndCountAll({
      ...query,
      offset: offset,
      limit: limit,
      order: [['created_at', 'DESC']],
      group: ['Order.order_id']
    });
    getdata.count = getdata?.count?.length || 0;
    let earningAmount: any;
    if (req_data.order_status === GetOrderList.Earnings && !req_data.user_id) {
      [earningAmount] = await Order.findAll({
        attributes: [
          [sequelize.fn('SUM', sequelize.col('payout_amount')), 'totalEarnings'],
        ],
        where: {
          [Op.and]: [
            {
              branch_id: req_data.branch_id,
            },
            {
              deletedAt: null,
            },
            { ...orderStatus },
          ],
        },
      });
      return {
        ...getdata,
        todayEarnings: Number(earningAmount?.dataValues?.totalEarnings) || 0,
      };
    }
    return getdata;
  }

  async viewOrder(req_data: ViewOrderDto) {
    let query: any = {
      where: {
        [Op.and]: [
          { order_id: req_data.order_id },
          // { deletedAt: null }
        ],
      },
      attributes: { exclude: ['order_otp', 'service_id'] },
      // attributes: ['order_id', 'customer_name', 'payment_status', 'created_at', 'updated_at', 'order_status',
      //   'final_price', 'discount', 'original_price', 'service_type', 'payment_method', 'customer_name',
      //   'customer_mobile', 'customer_email','applied_coupon_id','cancellation_reason','comment','gst','coupon_discount',
      // 'order_approved_by'],
      include: [
        {
          attributes: ['user_name', 'profile_url', 'email_id', 'mobile_no', 'gender'],
          model: User,
          paranoid: false
        },
        {
          attributes: { exclude: ['vendor_id', 'branch_ids', 'marketing_ids'] },
          model: Coupon,
          paranoid: false
        },
        {
          // attributes: ['service_name', 'id'],
          model: OrderService,
          include: [{
            attributes: ['service_name', 'id', 'amount', 'discount', 'duration', 'discounted_price'],
            model: Service,
            paranoid: false,
            include: [
              {
                attributes: ['id', 'category', 'image'],
                model: MasterCategory,
                paranoid: false
              },
              {
                attributes: ['id', 'service_category_name', 'image', 'code'],
                model: MasterServiceCategory,
                paranoid: false
              },
              {
                attributes: ['id', 'discount_value', 'discount_type'],
                model: FlashSale,
                paranoid: false,
              }
            ]
          }]
        }]
    };
    const getdata: any = await Order.findOne({
      ...query,
    });

    return getdata;
  }
  async scheduleRemiderMail(user_mail: string, appointments: Appointment[], order: Order) {
    // let { order_id, appointment_start_date_time } = order;
    // let reminding_time: Date = DateUtils.addHours(appointment_start_date_time, -6);
    // let diff = DateUtils.findDiffInHrs(reminding_time, new Date());
    // let sendRemindMail = () => {
    //   new MailUtils().sendOrderReminderMail(user_mail, appointments, order);
    // };
    // if (diff > 0)
    //   new CronUtils(new SchedulerRegistry).addCronJob(order_id, reminding_time, sendRemindMail);
    const createRemainder = await this.createRemaiderRecords(user_mail, order?.branch?.branch_email, appointments, order);

  }
  async checkOrderAlreadyExitThisTime(rescheduleOrderDto: RescheduleFromAdminDto) {
    let GetExiteOrder = await Order.findOne({ attributes: ['appointment_start_date_time', 'booking_slot'], where: { appointment_start_date_time: new Date(rescheduleOrderDto.appointment_start_date_time) } });
    if (GetExiteOrder && GetExiteOrder?.booking_slot == rescheduleOrderDto?.booking_slot) throw new Error(Errors.ALREADY_ORDER_EXIT_THIS_TIME);
  }
  async rescheduleOrder(rescheduleOrderDto: RescheduleFromAdminDto) {
    logger.info(`Order_RescheduleOrder_entry: ` + JSON.stringify(rescheduleOrderDto));
    // try {
    if (await new DateUtils().isTimestampInThePast(rescheduleOrderDto.appointment_start_date_time)) throw new Error(Errors.IFPASTTIME);
    let alreadyRescheduled = await Order.findOne({ attributes: ['rescheduled'], where: { order_id: rescheduleOrderDto?.order_id } });
    if (alreadyRescheduled?.rescheduled) return { rescheduled: true };
    if (rescheduleOrderDto?.vendor_type == VendorType.FREELANCER) {
      await this.checkOrderAlreadyExitThisTime(rescheduleOrderDto);
    }
    let reschedule_query = {
      where: {
        order_id: rescheduleOrderDto.order_id,
      },
    };
    let order = await Helpers.update(Order, reschedule_query, {
      ...rescheduleOrderDto,
      rescheduled: true,
    });
    if (order.length == 0) throw Error(Errors.INVALID_ORDER_DETAILS);
    let appointment = await Helpers.update(Appointment, reschedule_query, rescheduleOrderDto);
    let { customer_email } = order[0];
    logger.info(`Order_RescheduleOrder_Send_Email_entry: ${customer_email}`);
    // await new MailUtils().sendOrderRescheduleMail(customer_email, '');
    // new CronUtils(new SchedulerRegistry).deleteCron(rescheduleOrderDto.order_id);
    //-------------------------------------------
    let orders = await this.getOrderDetailsForRemaind(rescheduleOrderDto.order_id);
    // let { data: appointments } = await this.getAppointments(orders[0]?.user_id, 1, orders[0]?.order_id);
    await this.scheduleRemiderMail(customer_email, orders[0].appointments, orders[0]);
    //----------------- sendOrderRescheduleMail both side user and vendor ---------------------------
    let useraddress = await UserAddress.findOne({ where: { user_id: orders[0]?.user_id } });
    let confirmationContent = EmailTemService.rescheduleTemp(orders[0].appointments, orders, useraddress);
    let vendortemp = EmailTemService.rescheduleVendorTemplete(orders[0].appointments, orders, useraddress);
    await new MailUtils().sendOrderRescheduleMail(customer_email, confirmationContent);
    await new MailUtils().sendOrderRescheduleVendor(orders[0]?.branch?.branch_email, vendortemp);
    this.sendNotifyToUser(rescheduleOrderDto?.order_id, order[0]?.user_id, order[0]?.customer_name, order[0]?.appointment_start_date_time, order[0]?.booking_slot);
    //-------------------------------------------
    logger.info(`Order_RescheduleOrder_Exit: ` + JSON.stringify(order));
    return order;
    // } catch (error) {
    //   return HandleResponse.buildErrObj(null, error.message, error);
    // }
  }

  async sendNotifyToUser(orderId: string, usersId: string, userName: string, appointment: any, booking_slot: number | null) {
    let local_date_time = moment(appointment).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YYYY');
    let timeOnly = local_date_time.format('hh:mm A');
    let reqObj = {
      user_id: usersId,
      title: `Hello ${userName || 'Slaylewks users'} 💇 Your order has been rescheduled!`,
      body: `We hope this message finds you well. We wanted to inform you that there has been a change to the schedule for your recent order with ID (${orderId}). Your order is now rescheduled, and the appointment time is set for [${dateOnly} ${FreelancerBookingSlots[booking_slot] || timeOnly}].`,
      data: { order_id: orderId, Time: appointment, user_id: usersId },
      dataModels: User
    };
    await new SendPushNotification().sendNotifyToUserPhone(reqObj);
  }

  async createRemaiderRecords(user_email: string, branch_email: string, appointments: any, order: any) {
    const timestamp = new Date(order?.appointment_start_date_time).getTime();
    const threeHoursBehindTimestamp = timestamp - 3 * 60 * 60 * 1000; //half hour - 30 * 60 * 1000;
    let update_body = {
      order_id: order?.order_id,
      user_email: user_email,
      branch_email: branch_email,
      trigger_type: TriggerType.OrderRemain,
      trigger_time: threeHoursBehindTimestamp,
      appointment_data: appointments,
      order_data: order
    };
    let where = { where: { order_id: order?.order_id }, defaults: update_body };
    const createRemainder = await Helpers.findOrCreate(OrderRemainder, where);
    if (!createRemainder[1]) {
      delete where?.defaults;
      let updateRemainder = await Helpers.update(OrderRemainder, where, update_body);
    }
  }

  async getOrderDetailsForRemaind(order_id: string) {
    let order_qry = {
      attributes: [
        'order_otp',
        'original_price',
        'user_id',
        'createdAt',
        'coupon_discount',
        'applied_coupon_id',
        'gst',
        'service_type',
        'order_status',
        'is_payment_completed',
        'payment_status',
        'payment_method',
        'payment_id',
        'total_services',
        'customer_email',
        'customer_mobile',
        'customer_name',
        'final_price',
        'discount',
        'original_price',
        'order_id',
        'appointment_start_date_time',
        'created_at',
        'booking_slot'
      ],
      where: {
        order_id: order_id,
      },
      include: [
        {
          model: Branch,
          paranoid: false,
          attributes: [
            BRANCH_ATTRIBUTES.ID,
            BRANCH_ATTRIBUTES.BRANCH_IMAGE,
            BRANCH_ATTRIBUTES.BRANCH_NAME,
            BRANCH_ATTRIBUTES.BRANCH_EMAIL,
            BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO,
            BRANCH_ATTRIBUTES.IS_TAX_CHECKED,
            BRANCH_ATTRIBUTES.TAX_PERCENTAGE,
          ],
          include: {
            attributes: [
              LOCATION_ATTRIBUTES.latitude,
              LOCATION_ATTRIBUTES.longitude,
              LOCATION_ATTRIBUTES.address,
              LOCATION_ATTRIBUTES.area,
              LOCATION_ATTRIBUTES.city,
              LOCATION_ATTRIBUTES.state,
              LOCATION_ATTRIBUTES.pincode,
            ],
            model: Location,
            paranoid: false
          },
        },
        {
          attributes: ['id', 'receipt', 'status'],
          model: Payment_Txn,
          paranoid: false
        },
        {
          attributes: ['appointment_start_date_time', 'appointment_status'],
          model: Appointment,
          paranoid: false,
          include: {
            model: Service,
            paranoid: false,
            attributes: { exclude: ['is_active', 'createdAt', 'updatedAt'] },
            include: [
              {
                attributes: { exclude: ['createdAt', 'updatedAt'] },
                model: FlashSale,
                paranoid: false,
                where: FlashSaleConstants.getFlashSaleQry(),
                required: false,
              },
              { attributes: [MASTER_CATEGORIES_ATTRIBUTES.category], model: MasterCategory, paranoid: false },
            ],
          },
        },
      ],
    };
    let order = await Helpers.findAll(Order, order_qry);
    if (order.length == 0) throw new NotFoundException(Errors.INVALID_ORDER_DETAILS);
    return order;
  }

}
