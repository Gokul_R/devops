import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrdersReportService } from './orders-report.service';
import { CreateOrdersReportDto, GetOrdersByBranchDto, ViewOrderDto } from './dto/create-orders-report.dto';
import { RescheduleFromAdminDto, UpdateOrdersReportDto } from './dto/update-orders-report.dto';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from 'src/base.controller';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM106, EM100, EM104, EM148 } from 'src/core/constants';

@ApiTags('Order Reports')
@Controller('orders-report')
export class OrdersReportController {
  constructor(private readonly ordersReportService: OrdersReportService) {
    // super(ordersReportService);
  }

  @Post()
  async create(@Body() createOrdersReportDto: CreateOrdersReportDto) {
    try {
      let data = await this.ordersReportService.getOrderDetails(
        createOrdersReportDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }

  }
  @Post('get-RequestOrdersEarnings')
  async getByBranchOrders(@Body() getOrdersReportDto: GetOrdersByBranchDto) {
    try {
      let data = await this.ordersReportService.getByBranchRequestOrdersEarnings(getOrdersReportDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('view-order')
  async viewOrder(@Body() viewOrderDtoDto: ViewOrderDto) {
    try {
      let data = await this.ordersReportService.viewOrder(viewOrderDtoDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('reschedule')
  async rescheduleOrder(@Body() rescheduleFromAdminDto: RescheduleFromAdminDto) {
    try {
      let data = await this.ordersReportService.rescheduleOrder(rescheduleFromAdminDto);
      return HandleResponse.buildSuccessObj(EC200, EM148, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, error.message || EM100, error);
    }
  }

}

