import { Test, TestingModule } from '@nestjs/testing';
import { OrdersReportService } from './orders-report.service';

describe('OrdersReportService', () => {
  let service: OrdersReportService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OrdersReportService],
    }).compile();

    service = module.get<OrdersReportService>(OrdersReportService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
