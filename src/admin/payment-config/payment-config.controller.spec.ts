import { Test, TestingModule } from '@nestjs/testing';
import { PaymentConfigController } from './payment-config.controller';
import { PaymentConfigService } from './payment-config.service';

describe('PaymentConfigController', () => {
  let controller: PaymentConfigController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PaymentConfigController],
      providers: [PaymentConfigService],
    }).compile();

    controller = module.get<PaymentConfigController>(PaymentConfigController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
