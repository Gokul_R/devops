import { Controller, Get, Post, Body, Patch, Param } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { CreatePaymentConfigDto } from './dto/create-payment-config.dto';
import { PaymentConfigService } from './payment-config.service';
import { UpdatePaymentConfigDto } from './dto/update-payment-config.dto';
import { BaseController } from '../../base.controller';
import { logger } from 'src/core/utils/logger';
import { newLogger } from 'src/core/utils/loggerNew';

@ApiTags('payment-config')
@Controller('paymentmethod')
export class PaymentConfigController extends BaseController<CreatePaymentConfigDto> {
  constructor(private readonly paymentConfigService: PaymentConfigService) {
    super(paymentConfigService);
  }

  @Post()
  async create(@Body() body: CreatePaymentConfigDto) {
    try {
      logger.info(`Create_PaymentConfig_Entry: ${JSON.stringify(body)}`);
      // newLogger.info({
      //   type: 'payment',
      //   url: '/api/admin/v1/paymentmethod',
      //   body: { Create_PaymentConfig_Entry: body },
      // });
      let data = await this.paymentConfigService.create(body);
      logger.info(`Create_PaymentConfig_Exit: ${JSON.stringify(data)}`);
      // newLogger.info({
      //   type: 'payment',
      //   url: '/api/admin/v1/paymentmethod',
      //   response: { Create_PaymentConfig_Exit: data },
      // });
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get()
  // async findAll() {
  //   try {
  //     let data = await this.paymentConfigService.findAll();
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  // @Get(':id')
  // async findOne(@Param('id') id: string) {
  //   try {
  //     let data = await this.paymentConfigService.findOne(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() body: UpdatePaymentConfigDto) {
    try {
      logger.info(`Update_PaymentConfig_Entry_Id:${JSON.stringify(id)} Data:${body}`);
      // newLogger.info({
      //   type: 'payment',
      //   url: `/api/admin/v1/paymentmethod/${id}`,
      //   body: { Update_PaymentConfig_Entry_Id: id, Data: body },
      // });
      let data = await this.paymentConfigService.update(id, body);
      logger.info(`Update_PaymentConfig_Exit:${JSON.stringify(data)}`);
      // newLogger.info({
      //   type: 'payment',
      //   url: `/api/admin/v1/paymentmethod/${id}`,
      //   body: { Update_PaymentConfig_Exit: data },
      // });
      return HandleResponse.buildSuccessObj(EC200, EM116, data[0]);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
