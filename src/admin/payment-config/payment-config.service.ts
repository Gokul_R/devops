import { Inject, Injectable } from '@nestjs/common';
import { CreatePaymentConfigDto } from './dto/create-payment-config.dto';
import PaymentConfig from 'src/core/database/models/PaymentConfig';
import { UpdatePaymentConfigDto } from './dto/update-payment-config.dto';
import { BaseService } from '../../base.service';
import { ModelCtor } from 'sequelize-typescript';
import { PAYMENT_CONFIG_REPOSITORY } from 'src/core/constants';

@Injectable()
export class PaymentConfigService  extends BaseService<PaymentConfig>{
  protected model: ModelCtor<PaymentConfig>;
  constructor(@Inject(PAYMENT_CONFIG_REPOSITORY) private readonly paymentConfigRepo:typeof PaymentConfig){
    super();
    this.model=this.paymentConfigRepo;
  }
  // create(Body: CreatePaymentConfigDto) {
  //   const result = PaymentConfig.create(Body);
  //   return result;
  // }

  // findAll() {
  //   const result = PaymentConfig.findAll();
  //   return result;
  //

  // findOne(Id: string) {
  //   const result = PaymentConfig.findOne({
  //     where: { id: Id },
  //   });
  //   return result;
  // }

  // update(Id: string, Body: UpdatePaymentConfigDto) {
  //   const result = PaymentConfig.update(Body, {
  //     where: { id: Id },
  //   });
  //   return result;
  // }
}
