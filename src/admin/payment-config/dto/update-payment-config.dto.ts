import { IsString, IsBoolean, IsUUID } from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreatePaymentConfigDto } from './create-payment-config.dto';

export class UpdatePaymentConfigDto extends PartialType(CreatePaymentConfigDto) { }
