import { IsString, IsBoolean, IsNotEmpty, IsOptional, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreatePaymentConfigDto {
  @IsString()
  @IsOptional()
  id: string;
  @IsBoolean()
  @IsOptional()
  is_active?: boolean;

  @ApiProperty({ example: 'OWEPMIEJFPWEFJORJFOERJ' })
  @IsString()
  @IsOptional()
  razorpay_key: string;

  @ApiProperty({ example: 'WEKJNFIFDGINSDJNFIJERFJ' })
  @IsString()
  @IsOptional()
  razorpay_secret: string;

  @ApiProperty({ example: 'POERFJWOERFKNKNFIUHEIRHF' })
  @IsString()
  @IsNotEmpty()
  authentication_key: string;

  @ApiProperty({ example: 'FJNERHFIUWEINFIENRFIERHIN' })
  @IsString()
  @IsNotEmpty()
  authentication_token: string;

  @ApiProperty({ example: 'RUGFJEBJVNWHIDFJO' })
  @IsString()
  @IsNotEmpty()
  secure_hash_key: string;

  @ApiProperty({ example: 'ERJFHUNKJNFKJIRJI' })
  @IsString()
  @IsNotEmpty()
  merchant_key_id: string;

  // @ApiProperty({ example: true })
  // @IsBoolean()
  // is_active?: boolean;

  // @ApiProperty({ default: false })
  @IsBoolean()
  @IsOptional()
  is_deleted: boolean;

  @ApiProperty({ example: 'Razorpay' })
  @IsString()
  active_payment_method: string;

  @ApiProperty({ example: 'Razorpay' })
  @IsString()
  @IsOptional()
  account_number: string;

  @ApiProperty({ example: 10 })
  @IsNumber()
  @IsOptional()
  within_24h_pct: number;

  @ApiProperty({ example: 10 })
  @IsNumber()
  @IsOptional()
  before_24h_pct: number;

  @ApiProperty({ example: 10 })
  @IsNumber()
  @IsOptional()
  rejectedby_admin_pct: number;
}
