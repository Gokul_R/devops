import { PAYMENT_CONFIG_REPOSITORY } from 'src/core/constants';
import PaymentConfig from 'src/core/database/models/PaymentConfig';
import { commonProvider } from 'src/core/interfaces/common-providers';

export const paymentConfigProviders = [
  // {
  //   provide: PAYMENT_CONFIG_REPOSITORY,
  //   useValue: PaymentConfig,
  // },
  commonProvider.payment_config_method
];
