import { Module } from '@nestjs/common';
import { PaymentConfigController } from './payment-config.controller';
import { PaymentConfigService } from './payment-config.service';
import { paymentConfigProviders } from './payment-config.providers';

@Module({
  controllers: [PaymentConfigController],
  providers: [PaymentConfigService,...paymentConfigProviders],
})
export class PaymentConfigModule {}
