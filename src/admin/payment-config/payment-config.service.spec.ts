import { Test, TestingModule } from '@nestjs/testing';
import { PaymentConfigService } from './payment-config.service';

describe('PaymentConfigService', () => {
  let service: PaymentConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PaymentConfigService],
    }).compile();

    service = module.get<PaymentConfigService>(PaymentConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
