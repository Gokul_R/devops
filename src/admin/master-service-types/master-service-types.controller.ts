import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MasterServiceTypesService } from './master-service-types.service';
import { CreateMasterServiceTypeDto } from './dto/create-master-service-type.dto';
import { UpdateMasterServiceTypeDto } from './dto/update-master-service-type.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM127, EM116 } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@Controller('master-service-types')
@ApiTags('master-service-types')
export class MasterServiceTypesController {
  constructor(
    private readonly masterServiceTypesService: MasterServiceTypesService,
  ) { }

  @Post()
  async create(@Body() createMasterServiceTypeDto: CreateMasterServiceTypeDto) {
    try {
      let data = await this.masterServiceTypesService.create(
        createMasterServiceTypeDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get()
  async findAll() {
    try {
      let data = await this.masterServiceTypesService.FindallData();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.masterServiceTypesService.findOne(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateMasterServiceTypeDto: UpdateMasterServiceTypeDto,
  ) {
    try {
      let data = await this.masterServiceTypesService.update(
        id,
        updateMasterServiceTypeDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let data = await this.masterServiceTypesService.deleteData(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('search')
  async getFilterList(@Body() getmasterServiceTypesListedto: PaginationDto) {
    try {
      let data = await this.masterServiceTypesService.getFilterList(getmasterServiceTypesListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

}
