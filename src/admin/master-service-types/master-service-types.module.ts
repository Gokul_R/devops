import { Module } from '@nestjs/common';
import { MasterServiceTypesService } from './master-service-types.service';
import { MasterServiceTypesController } from './master-service-types.controller';

@Module({
  controllers: [MasterServiceTypesController],
  providers: [MasterServiceTypesService]
})
export class MasterServiceTypesModule {}
