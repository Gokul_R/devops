import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsOptional } from "class-validator";

export class CreateMasterServiceTypeDto {
    @IsBoolean()
    @IsOptional()
    is_active?: boolean;

    @ApiProperty()
    @IsNotEmpty()
    service_type_name: string;

    @ApiProperty()
    @IsNotEmpty()
    description: string;
}
