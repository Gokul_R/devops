import { PartialType } from '@nestjs/mapped-types';
import { CreateMasterServiceTypeDto } from './create-master-service-type.dto';

export class UpdateMasterServiceTypeDto extends PartialType(CreateMasterServiceTypeDto) {}
