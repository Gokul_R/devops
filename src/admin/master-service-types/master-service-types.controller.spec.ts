import { Test, TestingModule } from '@nestjs/testing';
import { MasterServiceTypesController } from './master-service-types.controller';
import { MasterServiceTypesService } from './master-service-types.service';

describe('MasterServiceTypesController', () => {
  let controller: MasterServiceTypesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MasterServiceTypesController],
      providers: [MasterServiceTypesService],
    }).compile();

    controller = module.get<MasterServiceTypesController>(MasterServiceTypesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
