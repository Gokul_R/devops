import { Injectable } from '@nestjs/common';
import MasterServiceType from 'src/core/database/models/MasterServiceType';
import { BaseService } from '../../base.service';
import { Op } from 'sequelize';
import BranchServiceTypeMapping from 'src/core/database/models/BranchServiceTypeMapping';
import Helpers from 'src/core/utils/helpers';

@Injectable()
export class MasterServiceTypesService extends BaseService<MasterServiceType> {
  protected model = MasterServiceType;


  async getFilterList(req_data: any) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { service_type_name: { [Op.iLike]: `%${searchText}%` } },
          { description: { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await MasterServiceType.findAndCountAll({
      where: {
        ...search,
        is_deleted: false,
      },
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']],
    });
  }

  async FindallData() {
    let condition = {
      where: {
        [Op.and]: [{ is_active: true }, { deleted_at: null }, { is_deleted: false }],
      },
    };
    return await MasterServiceType.findAll({ ...condition });
  }

  async deleteData(id: string) {
    await Helpers.softDelete(MasterServiceType, { where: { id: id } });
    await Helpers.softDelete(BranchServiceTypeMapping, { where: { service_type_id: id } });

    return true;
  }
}
