import { Test, TestingModule } from '@nestjs/testing';
import { MasterServiceTypesService } from './master-service-types.service';

describe('MasterServiceTypesService', () => {
  let service: MasterServiceTypesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MasterServiceTypesService],
    }).compile();

    service = module.get<MasterServiceTypesService>(MasterServiceTypesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
