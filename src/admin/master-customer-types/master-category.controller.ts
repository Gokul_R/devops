import {
  Controller,
  Post,
  Body,
  Patch,
  Param,
  UsePipes,
  ValidationPipe,
  Delete,
  Get,
} from '@nestjs/common';
import { MasterCustomerTypesService as MasterCategoryService } from './master-category.service';
import {
  CreateMasterCategoryDto as CreateCategoryDto,
  UpdateMasterCustomerTypeDto,
} from './dto/master-customer-type.dto';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../base.controller';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116, EM127 } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';

@Controller('master-category')
@ApiTags('master-category')
export class MasterCategoryController extends BaseController<CreateCategoryDto> {
  constructor(
    private readonly masterCustomerTypesService: MasterCategoryService,
  ) {
    super(masterCustomerTypesService);
  }
  @Post()
  async create(@Body() createMasterCustomerTypeDto: CreateCategoryDto) {
    try {
      let data = await this.masterCustomerTypesService.create(
        createMasterCustomerTypeDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('search')
  async getFilterList(@Body() getMasterCustomerTypeDto: PaginationDto) {
    try {
      let data = await this.masterCustomerTypesService.getFilterList(getMasterCustomerTypeDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMasterCustomerTypeDto: UpdateMasterCustomerTypeDto,
  ) {
    try {
      let data = this.masterCustomerTypesService.update(
        id,
        updateMasterCustomerTypeDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get()
  async findAll() {
    try {
      let data = await this.masterCustomerTypesService.findallData();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let data = await this.masterCustomerTypesService.deleteData(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }


}
