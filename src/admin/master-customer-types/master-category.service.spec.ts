import { Test, TestingModule } from '@nestjs/testing';
import { MasterCustomerTypesService } from './master-category.service';

describe('MasterCustomerTypesService', () => {
  let service: MasterCustomerTypesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MasterCustomerTypesService],
    }).compile();

    service = module.get<MasterCustomerTypesService>(MasterCustomerTypesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
