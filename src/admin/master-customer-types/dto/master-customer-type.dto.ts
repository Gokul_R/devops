import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsOptional, Length } from 'class-validator';

export class CreateMasterCategoryDto {
  @ApiProperty()
  @Length(4, 100, {
    message: 'category must be between 4 and 100 characters',
  })
  @IsNotEmpty()
  category: string;

  @ApiProperty()
  @Length(4, 250, {
    message: 'Description must be between 4 and 250 characters',
  })
  @IsOptional()
  description: string;

  @ApiProperty()
  @IsOptional()
  image: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsBoolean()
  is_active: boolean;

  // @ApiProperty()
  // @IsOptional()
  // branch_id: string;

}
export class UpdateMasterCustomerTypeDto {
  @ApiProperty()
  @IsNotEmpty()
  category: string;
}
