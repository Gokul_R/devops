import { Module } from '@nestjs/common';
import { MasterCustomerTypesService } from './master-category.service';
import { MasterCategoryController } from './master-category.controller';

@Module({
  controllers: [MasterCategoryController],
  providers: [MasterCustomerTypesService]
})
export class MasterCustomerTypesModule {}
