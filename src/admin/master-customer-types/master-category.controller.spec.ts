import { Test, TestingModule } from '@nestjs/testing';
import { MasterCategoryController } from './master-category.controller';
import { MasterCustomerTypesService } from './master-category.service';

describe('MasterCustomerTypesController', () => {
  let controller: MasterCategoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MasterCategoryController],
      providers: [MasterCustomerTypesService],
    }).compile();

    controller = module.get<MasterCategoryController>(MasterCategoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
