import { Inject, Injectable } from '@nestjs/common';
import { BaseService } from '../../base.service';
import MasterCategory from 'src/core/database/models/MasterCategory';
import { ModelCtor } from 'sequelize-typescript';
import { Op, col, fn } from 'sequelize';
import BranchCategoryMapping from 'src/core/database/models/BranchCategoryMapping';
import Helpers from 'src/core/utils/helpers';
import CategoryServiceCategoryMappings from 'src/core/database/models/CategoryServiceCatMapping';
import Service from 'src/core/database/models/Services';

@Injectable()
export class MasterCustomerTypesService extends BaseService<MasterCategory> {
  protected model: ModelCtor<MasterCategory> = MasterCategory;

  async findMasterCategriesByCode(masterCategory: any): Promise<any> {
    return await super.findOne(null, {
      where: {
        code: masterCategory.code,
      },
    });
    //category service category mapping
  }

  async getFilterList(req_data: any): Promise<any> {
    let search;
    if (req_data.searchText) {
      search = {
        // [Op.or]: [
        //   fn('LOWER', col('category')), // Convert column value to lowercase
        //   fn('LOWER', col('description')), // Convert column value to lowercase
        // ],
        [Op.or]: [
          { category: { [Op.iLike]: `%${req_data.searchText.toLowerCase()}%` } }, // Convert search value to lowercase
          { description: { [Op.iLike]: `%${req_data.searchText.toLowerCase()}%` } }, // Convert search value to lowercase
        ],
      };
    }
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await this.findAndCountAll({
      where: {
        is_deleted: false,
        ...search,
      },
      offset: offset,
      limit: limit
    });
  }

  async findallData() {
    let condition = {
      where: {
        [Op.and]: [{ is_active: true }, { is_deleted: false }],
      },
    };
    return await MasterCategory.findAll({ ...condition });
  }

  async deleteData(id: string) {
    await Helpers.softDelete(MasterCategory, { where: { id: id } });
    await Helpers.softDelete(BranchCategoryMapping, { where: { category_id: id } });
    await Helpers.softDelete(CategoryServiceCategoryMappings, { where: { category_id: id } });
    await Helpers.softDelete(Service, { where: { category_id: id } });

    return true;
  }
}
