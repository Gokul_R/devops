import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { FreelancerService } from './freelancer.service';
import { CreateFreelancerDto } from './dto/create-freelancer.dto';
import { UpdateFreelancerDto } from './dto/update-freelancer.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM104, EM100, EM116, EM127, EC409, EM102 } from 'src/core/constants';
import Vendor from 'src/core/database/models/Vendor';
import VendorRole from 'src/core/database/models/VendorRoles';

@ApiTags('freelancer')
@Controller('freelancer')
export class FreelancerController {
  constructor(
    private readonly freelancerService: FreelancerService
  ) {
  }

  @Post()
  async freelancerCreate(@Body() createFreelancerDto: CreateFreelancerDto) {
    try {
      let existFreelancer =await  Vendor.count({ where: { email_id: createFreelancerDto?.general_details?.branch_email } });
      if (!existFreelancer)
        existFreelancer = await VendorRole.count({ where: { email_id: createFreelancerDto?.general_details?.branch_email } });

      if (existFreelancer) return HandleResponse.buildErrObj(EC409, EM102, null);
      let data = await this.freelancerService.freelancerCreate(createFreelancerDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
