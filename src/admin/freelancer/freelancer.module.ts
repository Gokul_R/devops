import { Module } from '@nestjs/common';
import { FreelancerService } from './freelancer.service';
import { FreelancerController } from './freelancer.controller';
import { BranchModule } from 'src/admin/branch/branch.module';

@Module({
  imports:[BranchModule],
  controllers: [FreelancerController],
  providers: [FreelancerService],
})
export class FreelancerModule {}
