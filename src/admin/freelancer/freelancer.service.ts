import { Injectable } from '@nestjs/common';
import { CreateFreelancerDto } from './dto/create-freelancer.dto';
import Vendor from 'src/core/database/models/Vendor';
import Branch from 'src/core/database/models/Branch';
import { BranchService } from '../branch/branch.service';
import { BaseService } from 'src/base.service';
import { ModelCtor } from 'sequelize-typescript';
import Location from 'src/core/database/models/Location';
import Helpers from 'src/core/utils/helpers';
import BranchBankDetails from 'src/core/database/models/BranchBankDetails';

@Injectable()
export class FreelancerService extends BaseService<Vendor> {
  protected model: ModelCtor<Vendor>;
  constructor(
    private readonly branchService: BranchService
  ) {
    super();
  }
  async freelancerCreate(createFreelancerDto: CreateFreelancerDto) {
    let { general_details, bank_details, slot_timings } = createFreelancerDto;
    let vendorPayload = {
      vendor_name: general_details?.branch_name,
      email_id: general_details.branch_email,
      mobile_no: general_details?.branch_contact_no,
      profile_url: general_details?.branch_image,
      ...general_details,
      is_approved: false,
      approved_status: 0
    }
    let freelancerVendorCreate = await Vendor.create(vendorPayload);
    let branchPayload = {
      ...slot_timings,
      ...general_details,
      vendor_id: freelancerVendorCreate?.id,
      no_of_employees: 1,
      business_verified: false,
      approval_status: 0,

    }

    let freelancerBranchCreate = await Branch.create(branchPayload);
    await this.branchService.mapBranchData(createFreelancerDto, freelancerBranchCreate.id, false)


    let location = await Helpers.create(Location, {
      ...general_details,
      branch_id: freelancerBranchCreate?.id,
    });

    let bankDetails = await Helpers.create(BranchBankDetails, {
      ...bank_details,
      branch_id: freelancerBranchCreate?.id,
    });
    return {
      branch: freelancerBranchCreate,
      location: location,
      bankDetails: bankDetails
    }
  }








}
