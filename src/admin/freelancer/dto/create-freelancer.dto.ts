import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import {
    BankDetailsDto,
    CreateBranchDto,
    GeneralDetailsDto,
    SlotDetailDto,
    SlotTimingsDto,
} from 'src/admin/branch/dto/create-branch.dto';
import { slaylewks_commission } from 'src/core/constants';

export class freelancerGeneralDetailsDto extends PickType(GeneralDetailsDto, [
    'branch_image',
    'branch_landline',
    'commission_type',
    'payment_commission',
    'branch_email',
    'secondary_mobile',
    'branch_name',
    'service_type',
    'categories',
    'service_categories',
    'area',
    'city',
    'state',
    'country',
    'latitude',
    'longitude',
    'website_url',
    'is_active',
    'vendor_type',
    'vendor_id',
    'contact_person',
    'branch_contact_no',
]) {
    // @ApiProperty({ example: slaylewks_commission.percentage })
    // @IsNotEmpty()
    // @IsEnum(slaylewks_commission)
    // @IsString()
    // advance_payment_type: string | null;

    // @ApiProperty({ example: 10 })
    // //@Matches(/^[0-9]+$/)
    // @IsNotEmpty()
    // @IsNumber()
    // advance_payment: number;
}

export class freelancerSlotDetailDto extends PickType(SlotDetailDto, [
    'day',
    'no_of_employees',
    'evening',
    'morning',
    'afternoon',
]) { }

export class freelancerSlotTimingsDto extends PickType(SlotTimingsDto, [
    'open_time',
    'close_time',
    'leave_days',
    'slot_details',
]) { }

export class freelancerBankDetailsDto extends PickType(BankDetailsDto, [
    'account_branch',
    'account_holder',
    'account_no',
    'account_type',
    'ifsc_code',
]) { }

export class CreateFreelancerDto extends PickType(CreateBranchDto, [
    'general_details',
    'slot_timings',
    'bank_details',
]) { }
