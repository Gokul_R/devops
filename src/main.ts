/* eslint-disable prettier/prettier */
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { config } from 'dotenv';
import { AdminModule } from './admin/admin.module';
import { setupSwagger } from './swagger';
import { ValidationPipe } from '@nestjs/common';
import { EncryptionPipe } from './encrypt.pipe';
import helmet from 'helmet';
import cookieParser from 'cookie-parser';
import { ALLOWED_CORS_URLS, ALLOWED_METHODS, LOCAL, PRODUCTION } from './core/constants';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { rateLimit } from 'express-rate-limit';
import { ResponseLoggingInterceptor } from './core/utils/ResponseLoggingInterceptor';
import device from 'express-device';
import { logger } from './core/utils/logger';
import * as bodyParser from 'body-parser';
import { GlobalExceptionFilter } from './core/utils/customValidation';
import { GlobalErrorHandler } from './core/utils/globalErrorHandler';
import { SecretManager } from './core/utils/secretDatas';
import { newLogger } from './core/utils/loggerNew';

const limiter = rateLimit({
  windowMs: 1 * 60 * 1000, // one minutes
  max: 100, // 100 requests only allowed per one minutes
  message: 'Hold on their, maybe get a life instead of spamming my api.',
  standardHeaders: true,
  legacyHeaders: true,
  skipFailedRequests: true,
  validate: { xForwardedForHeader: false },
});
const corsOption = Object.freeze({
  origin: ALLOWED_CORS_URLS, // Add your allowed IP addresses and ports
  methods: ALLOWED_METHODS,
  preflightContinue: false,
  optionsSuccessStatus: 204,
  credentials: true,
});

async function bootstrap() {
  require('dotenv').config();
  await new SecretManager().awsSecretManager();
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  /*-------- security headers --------*/
  // app.enableCors({ origin: '*', methods: ALLOWED_METHODS }); //need to change enable allowed cors url only
  app.enableCors(corsOption);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );
  app.getHttpAdapter().getInstance().set('trust proxy', false);
  // process.env.NODE_ENV == PRODUCTION ? app.use(helmet()) : "";
  app.use(helmet());
  app.use(helmet.xssFilter());
  app.use(helmet.hidePoweredBy());
  app.use(helmet.frameguard({ action: 'deny' }));
  app.use(helmet.ieNoOpen());
  app.use(bodyParser.json({ limit: '100mb' }));
  app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }));
  app.use(helmet.noSniff());
  app.use(cookieParser());
  const expressApp = app.getHttpAdapter().getInstance();
  expressApp.disable('x-powered-by');
  expressApp.set('etag', 'strong');
  /*-------- security headers --------*/
  app.setGlobalPrefix('api/v1');
  app.useGlobalPipes(new ValidationPipe());
  app.useStaticAssets(join(__dirname, '..', 'pdfs'));

  setupSwagger(app, 'user');
  app.use(limiter);
  app.useGlobalFilters(new GlobalErrorHandler());
  app.use(device.capture());
  app.useGlobalFilters(new GlobalExceptionFilter());
  app.useGlobalInterceptors(new ResponseLoggingInterceptor());
  await app.listen(Number(process.env.USER_PORT));

  const app1 = await NestFactory.create(AdminModule);
  /*-------- security headers --------*/
  // app1.enableCors({ origin: '*', methods: ALLOWED_METHODS });  //need to change enable allowed cors url only
  app1.enableCors(corsOption);
  app1.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );
  app1.getHttpAdapter().getInstance().set('trust proxy', false);
  // process.env.NODE_ENV == PRODUCTION ? app.use(helmet()) : "";
  app.use(helmet());
  app1.use(helmet.xssFilter());
  app1.use(helmet.hidePoweredBy());
  app1.use(helmet.frameguard({ action: 'deny' }));
  app1.use(helmet.ieNoOpen());
  app1.use(bodyParser.json({ limit: '50mb' }));
  app1.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
  app1.use(helmet.noSniff());
  app1.use(cookieParser());
  const expressApp1 = app1.getHttpAdapter().getInstance();
  expressApp1.disable('x-powered-by');
  expressApp1.set('etag', 'strong');
  /*-------- security headers --------*/
  app1.setGlobalPrefix('api/admin/v1');
  app1.useGlobalPipes(new EncryptionPipe());
  setupSwagger(app1, 'admin');
  app1.use(limiter);
  app1.useGlobalFilters(new GlobalErrorHandler());
  app1.use(device.capture());
  app1.useGlobalFilters(new GlobalExceptionFilter());
  app1.useGlobalInterceptors(new ResponseLoggingInterceptor());
  await app1.listen(Number(process.env.ADMIN_PORT));

  process.on('uncaughtException', (error) => {
    console.error('Uncaught Exception:', error?.message);
    logger.error('Uncaught Exception: ' + JSON.stringify(error?.message || error?.stack || error));
    // newLogger.error({ type: 'main', response: { UncaughtException: error?.message || error?.stack || error } });
  });
  process.on('unhandledRejection', (reason, promise) => {
    console.error('Unhandled Rejection at:', promise, 'reason:', reason);
    logger.error('Unhandled Rejection at: ' + JSON.stringify(promise) + '_reason:' + JSON.stringify(reason));
    // newLogger.error({ type: 'main', response: { UnhandledRejectionat: { promise: promise, reason: reason } } });
  });
  // let pass = ['Siddharth', 'John', 'Amirta', 'Jonathan', 'Anuthama', 'Leander Dsouza'];
  // pass.map((pass) => {
  //   let hashed = Encryption.hashPassword(pass);
  //   logger.info(pass + '--------->' + hashed)
  //   console.log(pass + '--------->' + hashed);
  // });
}
bootstrap();
