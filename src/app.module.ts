import { CacheInterceptor, CacheModule, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';
import { DatabaseModule } from './core/database/database.module';
import { BranchModule } from './modules/shop/branch.module';
import { CartModule } from './modules/cart/cart.module';
import { CouponModule } from './modules/coupon/coupon.module';
import { NotificationModule } from './modules/notification/notification.module';
import { ReferralModule } from './modules/referral/referral.module';
import { RewardsAndChallengesModule } from './modules/rewards-and-challenges/rewards-and-challenges.module';
import { SupportModule } from './modules/support/support.module';
import { OrdersModule } from './modules/orders/orders.module';
import { PaymentModule } from './modules/payment/payment.module';
import { ClientAddressModule } from './modules/client-address/client-address.module';
import { FavouritesModule } from './modules/favourites/favourites.module';
import { ConfigModule } from '@nestjs/config';
import { FlashSaleModule } from './modules/flash-sale/flash-sale.module';
import { ServicesModule } from './modules/services/services.module';
import { ReviewModule } from './modules/review/review.module';
import { ScheduleModule } from 'nestjs-schedule';
import { SlotManagementModule } from './modules/slot-management/slot-management.module';
import { AwsS3Module } from './modules/aws-s3/aws-s3.module';
import { MulterModule } from '@nestjs/platform-express';

//import { Currency } from './core/database/models/Currency'; 
import { HelpAndSupportModule } from './admin/help-and-support/help-and-support.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    // CacheModule.register({ isGlobal: true }),
    ScheduleModule.forRoot(),
    DatabaseModule,
    AuthModule,
    UsersModule,
    BranchModule,
    CartModule,
    CouponModule,
    NotificationModule,
    ReferralModule,
    RewardsAndChallengesModule,
    SupportModule,
    OrdersModule,
    PaymentModule,
    ClientAddressModule,
    FavouritesModule,
    FlashSaleModule,
    ServicesModule,
    ReviewModule,
    SlotManagementModule,
    HelpAndSupportModule,
    AwsS3Module,
    MulterModule.register({
      dest: './uploads',
    }),
  ],
  controllers: [AppController],
  providers: [
    // {
    //   provide: APP_INTERCEPTOR,
    //   useClass: CacheInterceptor
    // },
    AppService
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    // process.env.NODE_ENV != LOCAL ? consumer.apply(JwtMiddleware).forRoutes('*') : '';
  }
}
