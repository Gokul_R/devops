import {
  CanActivate,
  ExecutionContext,
  Injectable,
  ForbiddenException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import Cart from '../database/models/Cart';
import Helpers from '../utils/helpers';

@Injectable()
export class DoesCartExist implements CanActivate {
  constructor() {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const cartExist = await Helpers.findOne(Cart, {
      where: {
        user_id: request.body.user_id,
        branch_id: request.body.branch_id,
      },
    });
    const cartExistForUser = await Helpers.findOne(Cart, {
      where: {
        user_id: request.body.user_id,
      },
    });
    if (!cartExistForUser) {
      return true;
    } else {
      if (cartExist) {
        return true;
      } else throw new ForbiddenException('Please clear your cart');
    }
  }
}
