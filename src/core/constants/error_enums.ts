export enum Errors {
  INVALID_ORDER_DETAILS = 'Invalid Order Details ',
  INVALID_USER_DETAILS = 'Invalid User Details ',
  INCORRECT_USER_PASSWORD = 'Incorrect password',
  INVALID_Coupon_DETAILS = 'Invalid Coupon Details ',
  EMAIL_ID_ALREADY_EXISTS = 'Email id already exists ',
  MOBILE_NO_ALREADY_EXISTS = 'Mobile no already exists ',
  INVALID_REMINDER_TIME = 'Invalid reminder time ',
  NO_SLOTS_AVAILABLE = 'No slots available ',
  NOT_PERMITTED_TO_UPDATE_LEAVE = `Orders Exists On This Date Can't Update Leave  `,
  MAXIMUM_RESCHEDULE_LIMIT_REACHED = `Maximum reschedule limit reached `,
  NO_RECORD_FOUND = `No record found `,
  USER_NOT_EXISTS = 'User not exists',
  SLOT_OCCUPIED = 'Slot has been occupied.Please select different slot time',
  IFPASTTIME = 'The given date is in the past. Please choose a future date.',
  ALREADY_ORDER_EXIT_THIS_TIME = `Rescheduling is not possible as an exit has already been ordered for this time.`
}
