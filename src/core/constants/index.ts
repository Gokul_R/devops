export const SEQUELIZE = 'SEQUELIZE';
export const DEVELOPMENT = 'development';
export const STAGING = 'staging';
export const PRODUCTION = 'production';
export const LOCAL = 'local';
export const TEST = 'test';
export const USER_REPOSITORY = 'USER_REPOSITORY';
export const CART_SERVICE_MAPPINGS = 'CART_SERVICE_MAPPINGS';
export const POST_REPOSITORY = 'POST_REPOSITORY';
export const HOME_REPOSITORY = 'HOME_REPOSITORY';
export const CART_REPOSITORY = 'CART_REPOSITORY';
export const FAVOURITES_REPOSITORY = 'FAVOURITES_REPOSITORY';
export const MASTER_SERVICE_CATEGORY_REPOSITORY = 'MASTER_SERVICE_CATEGORY_REPOSITORY';
export const LOCATION_REPOSITORY = 'LOCATION_REPOSITORY';
export const SERVICES_REPOSITORY = 'SERVICES_REPOSITORY';
export const BRANCH_REPOSITORY = 'BRANCH_REPOSITORY';
export const USER_SERVICE = 'USER_SERVICE';
export const BRANCH_SERVICE_CATEGORY_REPOSITORY = 'BRANCH_SERVICE_CATEGORY_REPOSITORY';
export const BRANCH_CATEGORY_REPOSITORY = 'BRANCH_CATEGORY_REPOSITORY';
export const BRANCH_SERVICE_TYPE_REPOSITORY = 'BRANCH_SERVICE_TYPE_REPOSITORY';
export const MASTER_SERVICE_TYPE_REPO = 'MASTER_SERVICE_TYPE_REPO';
export const COUPON_REPOSITORY = 'COUPON_REPOSITORY';
export const COUPON_BRANCH_REPOSITORY = 'COUPON_BRANCH_REPOSITORY';
export const SLOT_DETAILS_REPOSITORY = 'SLOT_DETAILS_REPOSITORY';
export const MASTER_CATEGORY_REPOSITORY = 'MASTER_CATEGORY_REPOSITORY';
export const BRANCH_GALLERY_REPOSITORY = 'BRANCH_GALLERY_REPOSITORY';
export const BRANCH_GALLERY_FOLDER_REPOSITORY = 'BRANCH_GALLERY_FOLDER_REPOSITORY';
export const COUPON_SERVICE_REPOSITORY = 'COUPON_SERVICE_REPOSITORY';
export const COUPON_CATEGORY_MAPPING_REPOSITORY = 'COUPON_CATEGORY_MAPPING_REPOSITORY';
export const COUPON_SERVICE_CATEGORY_MAPPING_REPOSITORY = 'COUPON_SERVICE_CATEGORY_MAPPING_REPOSITORY';
export const VENDOR_REPOSITORY = 'VENDOR_REPOSITORY';
export const PERCENTAGE_DISCOUNT_STRATEGY = 'PERCENTAGE_DISCOUNT_STRATEGY';
export const FLAT_DISCOUNT_STRATEGY = 'FLAT_DISCOUNT_STRATEGY';
export const DISCOUNT_SERVICE_REPOSITORY = 'DISCOUNT_SERVICE';
export const ADMIN_USER_REPOSITORY = 'ADMIN_USER_REPOSITORY';
export const MARKETING_REPOSITORY = 'MARKETING_REPOSITORY';
export const LOGO_REPOSITORY = 'LOGO_REPOSITORY';
export const NOTIFICATION_CONFIG_REPOSITORY = 'NOTIFICATION_CONFIG_REPOSITORY';
export const APP_INFORMATION_REPOSITORY = 'APP_INFORMATION_REPOSITORY';
export const LANGUAGE_REPOSITORY = 'LANGUAGE_REPOSITORY';
export const CURRENCY_REPOSITORY = 'CURRENCY_REPOSITORY';
export const SITE_MAINTENANCE_REPOSITORY = 'SITE_MAINTENANCE_REPOSITORY';
export const FOOTER_REPOSITORY = 'FOOTER_REPOSITORY';
export const PAYMENT_CONFIG_REPOSITORY = 'PAYMENT_CONFIG_REPOSITORY';
export const SMS_SETTINGS_REPOSITORY = 'SMS_SETTINGS_REPOSITORY';
export const BANNER_REPOSITORY = 'BANNER_REPOSITORY';
export const PAGES_REPOSITORY = 'PAGES_REPOSITORY';
export const PERMISSION_REPOSITORY = 'PERMISSION_REPOSITORY';
export const ROLE_REPOSITORY = 'ROLE_REPOSITORY';
export const ROLE_PERMISSION_REPOSITORY = 'ROLE_PERMISSION_REPOSITORY';
export const USER_ROLE_REPOSITORY = 'USER_ROLE_REPOSITORY';
export const COMMENT_REPOSITORY = 'COMMENT_REPOSITORY';
export const SMTP_SETTINGS_REPOSITORY = 'SMTP_SETTINGS_REPOSITORY';
export const SERVICE_PROVIDER_REPOSITORY = 'SERVICE_PROVIDER_REPOSITORY';
export const ADMIN_NOTIFICATION_REPOSITORY = 'ADMIN_NOTIFICATION_REPOSITORY';
export const OTP_REPOSTIORY = 'OTP_REPOSTIORY';
export const NOTIFICATION_MARKETING_MAPPING = 'NOTIFICATION_MARKETING_MAPPING';
export const REVIEW_REPOSITORY = 'REVIEW_REPOSITORY';
export const REVIEWS_REPLY_REPOSITORY = 'REVIEWS_REPLY_REPOSITORY';
export const CUSTOMER_DETAILS_REPOSITORY = 'CUSTOMER_DETAILS_REPOSITORY';
export const CHALLANGES_REPOSITORY = 'CHALLANGES_REPOSITORY';
export const ORDERS_REPORT_REPOSITORY = 'ORDERS_REPORT_REPOSITORY';
export const ORDER_SERVICES_REPO = 'ORDER_SERVICES_REPO';
export const NOTIFICATION_SERVICES_REPO = 'NOTIFICATION_SERVICES_REPO';
export const CATEGORY_SERIVICE_CAT_REPOSITORY = 'CATEGORY_SERIVICE_CAT_REPOSITORY';

export const TWILIO_ACCOUNT_SID = 'ACe4aa02c62f46ca0c824ae28156723235';
export const TWILIO_AUTH_TOKEN = 'a7f032ef2592f39a41f5dcc84354219a';
export const TEXT_LOCAL_URL = 'https://api.textlocal.in/send/?';
export const PAYG_URL = 'https://uatapi.payg.in/payment/api/order/';
export const CREATE = 'create';
export const DETAIL = 'Detail';
export const RAZORPAY = 'Razorpay';
export const PAYG = 'Payg';
export const SECRET_FILENAME = '.env';
// const twilio = require('twilio')(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

export const EM100 = 'Something went wrong !! please try again later';
export const EM101 = 'User not exists!!! please register';
export const EM102 = 'User already exist!!! please use different credentials';
export const EM103 = 'Successfully logged in';
export const EM104 = 'Record created Successfully';
export const EM105 = 'Please enter valid credentials';
export const EM106 = 'Data Fetched Successfully';
export const EM107 = 'Otp Sent Successfully';
export const EM108 = 'Otp verified successfully';
export const EM109 = 'Incorrect otp';
export const EM110 = 'Phone no already exists';
export const EM112 = 'Session expired!!';
export const EM113 = 'Refresh token is not valid!!';
export const EM114 = 'Token refreshed successfully';
export const EM115 = 'Unauthorized access';
export const EM116 = 'Record updated successfully';
export const EM117 = 'Car number already exist!!';
export const EM118 = 'Employee added successfully!!';
export const EM119 = 'No record found!!';
export const EM120 = 'Employee id required!!';
export const EM121 = 'Employee id or userid required!!';
export const EM122 = 'Referral code matched';
export const EM422 = 'Referral code invalid!!';
export const EM124 = 'Order Successful';
export const EM125 = 'Payment pending';
export const EM126 = 'Order cancelled successfully';
export const EM127 = 'Record deleted successfully';
export const EM128 = 'Password Incorrect ';
export const EM129 = 'Please Enter the Otp ';
export const EM130 = 'Code Expired.Please Request again';
export const EM131 = 'Please check reg_id,service_category_code,category_code';
export const EM132 = 'Code Expired.Please Request again';
export const EM133 = 'Coupon Applied Succesfully';
export const EM134 = 'Your account has been disabled!';
export const EM135 = 'Group name already exist!!! please use different name';
export const EM136 = 'Order completed successfully';
export const EM137 = 'Reset link sent successfully';
export const EM138 = 'New password must be different from the old password.';
export const EM139 = 'Coupon Collected Succesfully';
export const EM140 = 'Bank Account Created Succesfully in RazorPay';
export const EM141 = 'Mail sent successfully';
export const EM142 = 'Tansaction status updated succesfully';
export const EM143 = 'Order rejected succesfully,But payment not completed!!';
export const EM144 = 'Order rejected succesfully, But payment should be grater than 100 paisa';
export const EM145 = 'Payout amount successfully sent to RazorPay';
export const EM146 = 'Refund amount successfully sent to RazorPay';
export const EM147 = 'Fund account validation amount successfully sent to RazorPay';
export const EM148 = 'Order rescheduled successfully';
export const EM149 = 'Logout successfully';
export const EM150 = 'Admin has deactivated your profile. Please contact the administration for assistance.';
export const EM151 = 'The specified flash sale with the given details already exists. Please create a new flash sale with different details.';
export const EM152 = 'Please refrain from closing this order before your scheduled appointment time😔.';
export const EC500 = 500; //Internal server error
export const EC100 = 100; //Continue
export const EC200 = 200; //Success
export const EC401 = 401; //Unauthorized
export const EC409 = 409; //Conflict
export const EC412 = 412; //GroupName Exites
export const EC201 = 201; //Record Created
export const EC400 = 400; //Bad Request
export const EC204 = 204; //No content
export const EC403 = 403; //Forbidden
export const EC422 = 422; //Forbidden
export const EC410 = 410; //OTP Wrong
export const EC411 = 411; //password Wrong
export const EC404 = 404; // page not found

export const E1 = true;
export const E0 = false;

export const RADIUS = 20;

export const Referral_Pionts = {
    _referralPoint: 20,
};
export const Payment_Status = {
    PaymentOrderCreated: 'created',
    PaymentSuccess: 'success',
    PaymentFailed: 'failed',
    PaymentPending: 'pending',
};
export const Service_Type = {
    Home: 'home',
    Shop: 'shop',
};

export enum Approval {
    PENDING = 0,
    ACCEPTED = 1,
    REJECTED = 2,
}

export enum Provided_By {
    SUPER_ADMIN = 'SUPER_ADMIN',
    VENDOR = 'VENDOR',
    BRANCH = 'BRANCH',
    USER = 'USER',
}

export enum slaylewks_commission {
    AMOUNT = 'amount',
    percentage = 'percentage',
}

export enum contactType {
    CONTACTUS = 'contactUs',
    SUPPORT = 'support',
}

export enum complaintType {
    SERVICE = 'Service',
    GENERAL = 'General',
}

export enum ChennaiLocation {
    lat = 13.0827,
    lng = 80.2707
}
export enum NotificationType {
    CONFIRMED = 'CONFIRMED',
    RESCHEDULED = 'RESCHEDULED',
    CANCELLED = 'CANCELLED',
    REMAINDER = 'REMAINDER',
    AWATING_ORDER = 'AWAITING ORDER'
}

export const notificationTypeConfig = {
    [NotificationType.CONFIRMED]: {
        title: 'Order Confirmed',
        description: 'Your order has been confirmed.',
    },
    [NotificationType.RESCHEDULED]: {
        title: 'Order Rescheduled',
        description: 'Your order has been rescheduled.',
    },
    [NotificationType.CANCELLED]: {
        title: 'Order Cancelled',
        description: 'Your order has been Cancelled.',
    },
    [NotificationType.REMAINDER]: {
        title: 'Order Reminder',
        description: 'Today Your Service Available',
    },
    [NotificationType.AWATING_ORDER]: {
        title: 'Order Awating',
        description: 'Your order has been awating for approval.',
    },
};

export const PAYOUT_ID = 'pout_Mhl6zOslBFf9fa';
export const PAYMENT_ID = 'pay_MkYMg7KqK6TORL';
export const REFUND_ID = 'rfnd_MkoRvCkIyKuzmE';
export const REF_ID = 'ref#IIPK_1696588327883_1696595555';
export const CONTACT_ID = 'cont_MkuVD5CBmfG5tE';
export const FUND_AC_ID = 'fa_MkuVIIKxAWT9LG';
export const FAV_ID = 'fav_MlDogJP3GvgmRA';
export const BRANCH_ID = '48e4021a-40d0-4183-b356-2e0d5edf3b10';
export const ORDER_ID = 'TOTZ_1691760106800';
export const VENDOR_ID = 'bf2d7b4d-622d-4199-a31a-66326c901879';
export const SERVICE_ID = '70dabc15-80f1-496b-b2e4-531b0a8e2478';
export const ALLOWED_CORS_URLS: string[] = [
    // add your http cors origin urls
    'http://210.18.187.83:4200',  // hathway static ip
    'http://localhost:4200',  // localhost 
    'https://staging.slaylewks.com', // stagging server
    'https://dev3.slaylewks.com',  // dev server
    'https://slaylewks.com', //live web
    'http://slaylewks.com', //live web
    'https://www.slaylewks.com', //live web
    'http://www.slaylewks.com', //live web
    'http://3.108.242.245', //live web
    'https://3.108.242.245', //live web
    'http://business.slaylewks.com', //live vendor 
    'http://business.slaylewks.com/super_admin', // live admin
    'https://business.slaylewks.com', //live vendor 
    'https://business.slaylewks.com/super_admin' // live admin

];
export const ALLOWED_METHODS: string[] = ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'];

export const Slaylewks_details = Object.freeze({
    logo: 'https://slaylwks.s3.ap-south-1.amazonaws.com/slaylewks_logo.webp',
    comName: 'SLAYLEWKS INDIA PRIVATE LIMITED',
    taxNo: '33AVDPV2796G1Z8',
    email: 'info@slaylewks.com',
    phone: '+91-9445510157',
    address1: 'Villa No.5-G, Lane-5, Voora Villa 96 Bhaktha Veda Samy Road',
    address2: 'Akkarai, Chennai, Kancheepuram. Tamil Nadu 600119',
    gst: 0.18,
    transaction_fees: 0.02, // razorpay percentage
    payout_ac_number_dev: '2323230079191843',
    payout_ac_number_live: '4564567483723837',
    firebase_url: 'https://firebasestorage.googleapis.com/v0/b/slaylewks-a0986.appspot.com/o/',
    s3base_url: 'https://slaylwks.s3.ap-south-1.amazonaws.com/',
    s3base_cdn_url: 'https://cdnimages.slaylewks.com/',
    support_team_to_email_prod: 'info@slaylewks.com',
    support_team_to_email_dev: 'bala.slaylewks@gmail.com',
    web_url: 'https://slaylewks.com/',
    privacy_policy: 'https://slaylewks.com/privacy-policy',
    terms_and_conditions: 'https://slaylewks.com/term-of-use',
    cancelation_and_reschedule: 'https://slaylewks.com/reschedule-cancel-policy',
    cc_mail_live: ['slaylewks.official@gmail.com', 'slaylewksdata@gmail.com'],
    cc_mail_live_1: ['slaylewks.officials@gmail.com'],
    to_vendor_request: ['bala.slaylewks@gmail.com', 'ganesh_br@hotmail.com', 'sambasivam@slaylewks.com'],
    cc_mail_dev: ['developmentslaylewks@gmail.com', 'balamurugesan58@gmail.com'],
    to_mail_dev: ['bala.slaylewks@gmail.com', 'gsoftslaylewks@gmail.com']
});


export enum TriggerType {
    OrderRemain = 'Order_Remaind'
}
