// customerTypeAttributes.ts

export const CUSTOMER_TYPE_ATTRIBUTES = {
    ID: 'id',
    branch_id: 'branch_id',
    CUSTOMER_TYPE_ID: 'customer_type_id',
    IS_ACTIVE: 'is_active',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    IS_DELETED: 'is_deleted',
  };
  