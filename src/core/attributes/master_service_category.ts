export const MASTER_SERVICE_CATEGORY_ATTRIBUTES = {
  id: 'id',
  service_category_name: 'service_category_name',
  icon_url: 'icon_url',
  code: 'code',
  is_active: 'is_active',
  is_deleted: 'is_deleted',

};
