export const BRANCH_GALLERY_ATTRIBUTES = {
    ID: 'id',
    GALLERY_LIST: 'gallery_list',
    IS_ACTIVE: 'is_active',
    IS_DELETED: 'is_deleted',
  };
  