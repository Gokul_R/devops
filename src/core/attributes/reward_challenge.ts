export const REWARD_CHALLENGE_HISTORY_ATTRIBUTES = {
    ID: 'id',
    REWARDS_ID: 'rewards_id',
    CHALLENGE_ID: 'challenge_id',
    USER_ID: 'user_id',
    ATTEMPT_COUNT: 'attempt_count',
    IS_REWARDS: 'is_rewards',
    IS_DELETED: 'is_deleted',
  };
  