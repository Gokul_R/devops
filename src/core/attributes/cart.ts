// cartAttributes.ts

export const CART_ATTRIBUTES = {
    ID: 'id',
    USER_ID: 'user_id',
    branch_id: 'branch_id',
    SERVICE_ID: 'service_id',
    CART_ID: 'cart_id',
    IS_ACTIVE: 'is_active',
    CREATED_BY: 'created_by',
    UPDATED_BY: 'updated_by',
    IS_DELETED: 'is_deleted',
  };

  