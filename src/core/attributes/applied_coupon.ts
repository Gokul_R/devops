// appliedCouponAttributes.ts

export const APPLIED_COUPON_ATTRIBUTES = {
  TABLE_NAME: 'applied_coupons',
  USED_COUNT: 'used_count',
  ID: 'id',
  USER_ID: 'user_id',
  COUPON_ID: 'coupon_id',
  CART_ID: 'cart_id',
  IS_DELETED: 'is_deleted',
  IS_APPLIED: 'is_applied',
};
