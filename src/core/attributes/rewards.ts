export const REWARDS_ATTRIBUTES = {
    ID: 'id',
    TITLE: 'title',
    REWARDS_TYPE: 'rewards_type',
    SPEND_COINS: 'spend_coins',
    START_DATE: 'start_date',
    END_DATE: 'end_date',
    DESCRIPTION: 'description',
    IS_ACTIVE: 'is_active',
    IS_DELETED: 'is_deleted',
};
  
