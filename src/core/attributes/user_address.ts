export const USER_ADDRESS_ATTRIBUTES = {
    ID: 'id',
    CUSTOMER_NAME: 'customer_name',
    CUSTOMER_MOBILE: 'customer_mobile',
    CUSTOMER_EMAIL: 'customer_email',
    ADDRESS_TYPE: 'address_type',
    FLAT_NO: 'flat_no',
    AREA: 'area',
    CITY: 'city',
    ZIPCODE: 'zipcode',
    STATE: 'state',
    COUNTRY: 'country',
    MESSAGE: 'message',
    IS_ACTIVE: 'is_active',
    USER_ID: 'user_id',
    IS_DELETED: 'is_deleted',
  };
  