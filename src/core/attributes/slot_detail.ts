export const SLOT_DETAIL_ATTRIBUTES = {
  ID: 'id',
  DAY: 'day',
  OPEN_AT: 'open_at',
  CLOSE_AT: 'close_at',
  START_TIME: 'start_time',
  SLOT_START_TIME: 'slot_start_time',
  SLOT_END_TIME: 'slot_end_time',
  END_TIME: 'end_time',
  NO_OF_EMPLOYEES: 'no_of_employees',
  branch_id: 'branch_id',
  IS_DELETED: 'is_deleted',
  IS_ACTIVE: 'is_active',
};
