const MASTER_SERVICE_TYPE_ATTRIBUTES = {
    id: 'id',
    service_type_name: 'service_type_name',
    is_active: 'is_active',
    created_at: 'created_at',
    updated_at: 'updated_at',
    is_deleted: 'is_deleted',
  };
  