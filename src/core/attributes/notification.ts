const NOTIFICATIONS_ATTRIBUTES = {
    id: 'id',
    user_id: 'user_id',
    branch_id: 'branch_id',
    description: 'description',
    is_viewed: 'is_viewed',
    is_active: 'is_active',
    is_deleted: 'is_deleted',
  };
  