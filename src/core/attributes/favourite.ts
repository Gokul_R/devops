// favouritesAttributes.ts

export const FAVOURITES_ATTRIBUTES = {
    ID: 'id',
    USER_ID: 'user_id',
    branch_id: 'branch_id',
    IS_ACTIVE: 'is_active',
    IS_DELETED: 'is_deleted',
    CREATED_BY: 'created_by',
    UPDATED_BY: 'updated_by',
  };
  