// challengesAttributes.ts

export const CHALLENGES_ATTRIBUTES = {
    ID: 'id',
    CHALLENGE_TYPE: 'challenge_type',
    TITLE: 'title',
    MARKETING_GROUP: 'marketing_group',
    SCHEDULE_COUNT: 'schedule_count',
    CLICKING_COUNT: 'clicking_count',
    IMAGES: 'images',
    REWARD_COINS: 'reward_coins',
    START_DATE: 'start_date',
    END_DATE: 'end_date',
    DESCRIPTION: 'description',
    IS_ACTIVE: 'is_active',
    IS_DELETED: 'is_deleted',
  };
  