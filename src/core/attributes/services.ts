// serviceAttributes.ts

export const SERVICE_ATTRIBUTES = {
  ID: 'id',
  TABLE_NAME: 'services',
  SERVICE_NAME: 'service_name',
  SERVICE_IMAGE: 'image',
  SERVICE_URL: 'service_url',
  TYPE: 'type',
  DURATION: 'duration',
  branch_id: 'branch_id',
  SERVICE_CATEGORY_ID: 'service_category_id',
  AMOUNT: 'amount',
  DISCOUNT: 'discount',
  GENDER: 'gender',
  SERVICE_PRICE: 'service_price',
  DISCOUNTED_PRICE: 'discounted_price',
  SERVICE_DESCRIPTION: 'description',
  IS_ACTIVE: 'is_active',
  IS_DELETED: 'is_deleted',
};
