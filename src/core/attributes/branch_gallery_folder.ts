export const BRANCH_GALLERY_FOLDER_ATTRIBUTES = {
    ID: 'id',
    FOLDER_NAME: 'folder_name',
    IS_ACTIVE: 'is_active',
    IS_DELETED: 'is_deleted',
  };
  