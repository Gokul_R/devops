export const MARKETING_GROUP_ATTRIBUTES = {
  TABLE_NAME: 'marketing_group',
  ID: 'id',
  GROUP_NAME: 'group_name',
  DESCRIPTION: 'description',
  REFERRAL_CODE: 'referral_code',
  START_DATE: 'start_date',
  END_DATE: 'end_date',
};
