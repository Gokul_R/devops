export const SUPPORT_CENTER_ATTRIBUTES = {
    ID: 'id',
    USER_ID: 'user_id',
    COMPLAINT_TYPE: 'complaint_type',
    ORDER_ID: 'order_id',
    USER_NAME: 'user_name',
    USER_PH: 'user_ph',
    USER_EMAIL: 'user_email',
    MESSAGE: 'message',
    IS_ACTIVE: 'is_active',
    IS_DELETED: 'is_deleted',
  };
  