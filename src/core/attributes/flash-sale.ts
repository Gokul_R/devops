const FLASHSALE_ATTRIBUTES = {
    id: 'id',
    service_id: 'service_id',
    branch_id: 'branch_id',
    start_time: 'start_time',
    end_time: 'end_time',
    discount_type: 'discount_type',
    discount_value: 'discount_percentage',
    // price_before_sale: 'price_before_sale',
    // price_after_sale: 'price_after_sale',
    quantity_available: 'quantity_available',
    sold_quantity: 'sold_quantity',
    is_deleted: 'is_deleted',
  };
  