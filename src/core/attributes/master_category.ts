export const MASTER_CATEGORIES_ATTRIBUTES = {
    id: 'id',
    category: 'category',
    description: 'description',
    image: 'image',
    code: 'code',
    is_deleted: 'is_deleted',
  };
  