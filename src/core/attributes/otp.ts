export const OTP_ATTRIBUTES = {
    ID: 'id',
    USER_ID: 'userId',
    OTP: 'otp',
    EXPIRES_AT: 'expires_at',
    IS_DELETED: 'is_deleted',
  };
  