// assetAttributes.ts

export const ASSET_ATTRIBUTES = {
    ID: 'id',
    DESCRIPTION: 'description',
    ASSET_URL: 'asset_url',
    SCREEN: 'screen',
    IS_DELETED: 'is_deleted',
  };
  