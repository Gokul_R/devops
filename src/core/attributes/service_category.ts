export const SERVICE_CATEGORY_ATTRIBUTES = {
    ID: 'id',
    branch_id: 'branch_id',
    SERVICE_CATEGORY_ID: 'service_category_id',
    IS_DELETED: 'is_deleted',
  };
  