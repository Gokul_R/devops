// appointmentAttributes.ts

export const APPOINTMENT_ATTRIBUTES = {
  ID: 'id',
  USER_ID: 'user_id',
  branch_id: 'branch_id',
  SERVICE_ID: 'service_id',
  ORDER_ID: 'order_id',
  APPOINTMENT_START_DATE_TIME: 'appointment_start_date_time',
  IS_ACCEPTED_BY_BRANCH: 'is_accepted_by_branch',
  APPOINTMENT_STATUS: 'appointment_status',
  BOOKING_SLOT: 'booking_slot',
  IS_CANCELLED: 'is_cancelled',
  IS_ACTIVE: 'is_active',
  IS_DELETED: 'is_deleted',
};
