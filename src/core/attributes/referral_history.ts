export const REFERRAL_HISTORY_ATTRIBUTES = {
    ID: 'id',
    USER_ID: 'user_id',
    REFERRAL_USER_ID: 'referral_user_id',
    EARN_COINS: 'earn_coins',
    IS_ACTIVE: 'is_active',
    IS_DELETED: 'is_deleted',
  };
  