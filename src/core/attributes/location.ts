export const LOCATION_ATTRIBUTES = {
  id: 'id',
  area: 'area',
  city: 'city',
  pincode: 'pincode',
  state: 'state',
  country: 'country',
  is_active: 'is_active',
  address: 'address',
  address_line1: 'address_line1',
  address_line2: 'address_line2',
  address_line3: 'address_line3',
  latitude: 'latitude',
  longitude: 'longitude',
  is_deleted: 'is_deleted',
  branch_id: 'branch_id',

};
