import {
  Model,
  Column,
  Table,
  DataType,
  Default,
  PrimaryKey,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import User from './User';
import Service from './Services';
import Branch from './Branch';
import Order from './Order';
import BaseModel from '../BaseModel';
import moment from 'moment-timezone';

export enum APPOINTMENT_STATUS {
  PENDING = 'Pending',
  CONFIRMED = 'Confirmed',
  COMPLETED = 'Completed',
  CANCELLED = 'Cancelled',
  REJECTED = 'Rejected',
  EXPIRED = 'Expired',
  AWAITING_APPROVAL = 'Awaiting_approval'
}

@Table({ timestamps: true, tableName: 'appointments', underscored: true, paranoid: true })
export default class Appointment extends BaseModel<Appointment> {
  @ForeignKey(() => User)
  @Column(DataType.STRING)
  user_id: string;

  @ForeignKey(() => Branch)
  @Column(DataType.STRING)
  branch_id: string;

  @ForeignKey(() => Service)
  @Column(DataType.STRING)
  service_id: string;

  @ForeignKey(() => Order)
  @Column(DataType.STRING)
  order_id: string;

  @Column({
    type: DataType.DATE,
    get() {
      const rawValue = this.getDataValue('appointment_start_date_time');
      if (rawValue) {
        const formattedDate = moment.utc(rawValue).tz('Asia/kolkata');
        // console.log('------>', formattedDate);
        return formattedDate;
      }
    },
  })
  appointment_start_date_time: Date;

  @Default(false)
  @Column(DataType.BOOLEAN)
  is_accepted_by_branch: boolean;

  @Column(DataType.INTEGER)
  booking_slot: number;

  @Default(APPOINTMENT_STATUS.PENDING)
  @Column(
    // DataType.ENUM(
    //   APPOINTMENT_STATUS.PENDING,
    //   APPOINTMENT_STATUS.CONFIRMED,
    //   APPOINTMENT_STATUS.COMPLETED,
    //   APPOINTMENT_STATUS.CANCELLED,
    // ),
    DataType.STRING,
  )
  appointment_status: string;

  @Default(false)
  @Column(DataType.BOOLEAN)
  is_cancelled: boolean;

  // @BelongsTo(() => User, 'client_id')
  // client: User;

  @BelongsTo(() => Branch, 'branch_id')
  branch: Branch;

  @BelongsTo(() => Service, 'service_id')
  service: Service;

  @BelongsTo(() => Order, { targetKey: 'order_id' })
  order: Order;

  @BelongsTo(() => User)
  user: User[];
}
