import {
  Model,
  Column,
  Table,
  DataType,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
  Default,
  AllowNull,
  HasOne,
} from 'sequelize-typescript';
import Branch from './Branch';
import Vendor from './Vendor';
import BranchGallery from './BranchGallery';
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName: 'branch_gallery_folder', underscored: true, paranoid: true })
export default class BranchGalleryFolder extends BaseModel<BranchGalleryFolder> {
  @AllowNull(true)
  @Column(DataType.STRING(70))
  folder_name: string;

  @ForeignKey(() => Branch)
  @AllowNull(true)
  @Column(DataType.STRING(70))
  branch_id: string;

  // @ForeignKey(() => Vendor)
  // @AllowNull(true)
  // @Column(DataType.STRING(70))
  // vendor_id: string;

  @HasOne(() => BranchGallery)
  gallery: BranchGallery;
}
