import {
  Model,
  Column,
  Table,
  DataType,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
  Default,
  AllowNull,
} from 'sequelize-typescript';
import Branch from './Branch';
import Vendor from './Vendor';
import BranchGalleryFolder from './BranchGalleryFolder';
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName: 'branch_gallery', underscored: true, paranoid: true })
export default class BranchGallery extends BaseModel<BranchGallery> {
  @ForeignKey(() => BranchGalleryFolder)
  @AllowNull(true)
  @Column(DataType.STRING(70))
  folder_id: string;

  @ForeignKey(() => Branch)
  @AllowNull(true)
  @Column(DataType.STRING(70))
  branch_id: string;

  // @ForeignKey(() => Vendor)
  // @AllowNull(true)
  // @Column(DataType.STRING(70))
  // vendor_id: string;

  @AllowNull(true)
  @Column(DataType.ARRAY(DataType.TEXT))
  gallery_list: any;

  @BelongsTo(() => BranchGalleryFolder)
  folder: BranchGalleryFolder;
}
