import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  ForeignKey,
  BelongsTo,
  Table,
} from 'sequelize-typescript';
import MasterServiceType from './MasterServiceType';
import Branch from './Branch';
import BaseModel from '../BaseModel';

@Table({
  timestamps: true,
  tableName: 'branch_service_type_mappings',
  underscored: true,
  paranoid: true,
})
export default class BranchServiceTypeMapping extends BaseModel<BranchServiceTypeMapping> {
  @ForeignKey(() => Branch)
  @Column(DataType.STRING)
  branch_id: string;

  @ForeignKey(() => MasterServiceType)
  @Column(DataType.STRING)
  service_type_id: string;

  @BelongsTo(() => Branch)
  branch: Branch;

  @BelongsTo(() => MasterServiceType)
  master_service_type: MasterServiceType;
}
