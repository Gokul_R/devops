import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  BelongsTo,
  Table,
  ForeignKey,
} from 'sequelize-typescript';
import Branch from './Branch';
import Service from './Services';
import BaseModel from '../BaseModel';
import { normalizeMap } from 'src/core/constants/appConfig';
import { log } from 'console';
// import { ServiceProvider } from './ServiceProvider';

@Table({
  timestamps: true,
  tableName: 'locations',
  underscored: true,
  paranoid: true,
})
export default class Location extends BaseModel<Location> {
  @Column(DataType.STRING)
  area: string;

  @Column(DataType.STRING)
  city: string;

  @Column(DataType.STRING)
  pincode: string;

  @Column(DataType.STRING)
  state: string;

  @Column(DataType.STRING)
  country: string;

  @Column(DataType.TEXT)
  address: string;
  @Column(DataType.DOUBLE)
  latitude: number;
  @Column(DataType.DOUBLE)
  longitude: number;

  @ForeignKey(() => Branch)
  @ForeignKey(() => Service)
  @Column(DataType.STRING)
  branch_id: string;

  @BelongsTo(() => Branch)
  branch: Branch;

  @BelongsTo(() => Service, { targetKey: 'branch_id' })
  service: Service;

  static async updateArea() {
    let locations: Location[] = await Location.findAll({
      // where: { area: 'ADYAR,' },
      order: [['area', 'ASC']],
      // limit: 10,
    });

    let updatedLocations = locations.map((location) => {
      const plainLocation = location.get({ plain: true });
      return {
        ...plainLocation,
        area: this.normalizeArea(location.area),
      };
    });
    let res = await Location.bulkCreate(updatedLocations, { updateOnDuplicate: ['id', 'area'] });
    log(res);
  }

  static normalizeArea(str: string): string {
    // Convert to lowercase and apply mappings
    if (str) {
      const normalized = str.toLowerCase();
      return normalizeMap[normalized] || str;
    }
  }
}

// Location.belongsTo(ServiceProvider, { foreignKey: 'branch_id', targetKey: 'id' });
