import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  BelongsTo,
  Table,
  ForeignKey,
  HasMany,
  HasOne,
  Unique,
  AllowNull,
  BeforeCreate,
} from 'sequelize-typescript';
import Branch from './Branch';
import Appointment, { APPOINTMENT_STATUS } from './Appointment';
import UserAddress from './UserAddress';
import Payment_Txn from './Payment-Tnx';
import BaseModel from '../BaseModel';
import Service from './Services';
import OrderService from './OrderServices';
import Helpers from 'src/core/utils/helpers';
import { Payment_Status } from 'src/core/constants';
import User from './User';
import Coupon from './Coupons';
import PartialPayment from './PartialPayments';

@Table({
  timestamps: true,
  tableName: 'orders',
  underscored: true,
  paranoid: true,
})
export default class Order extends BaseModel<Order> {
  @Default(DataType.UUIDV4)
  @Column(DataType.STRING)
  id!: string;

  @PrimaryKey
  @Unique
  @Column(DataType.STRING)
  order_id: string;

  @Column(DataType.STRING)
  receipt_id: string;

  @PrimaryKey
  @Unique
  @Column(DataType.STRING)
  payment_order_id: string;

  @Default(0)
  @Column(DataType.INTEGER)
  payment_attempt: number;

  @ForeignKey(() => Coupon)
  // @Default(null)
  @Column(DataType.STRING)
  applied_coupon_id: string;

  @ForeignKey(() => User)
  @Column(DataType.STRING(50))
  user_id: string;

  @Column(DataType.STRING)
  service_type: string;

  @Column(DataType.STRING)
  payment_status: string;

  @Column(DataType.STRING)
  order_otp: string;

  @Column(DataType.DOUBLE)
  original_price: number;

  @Column(DataType.DOUBLE)
  discount: number;

  @Column(DataType.DOUBLE)
  final_price: number;

  @Column(DataType.ARRAY(DataType.TEXT))
  service_id: string[];

  @Default(false)
  @Column(DataType.BOOLEAN)
  rescheduled: boolean;

  @Column(DataType.DATE)
  rescheduled_at: Date;

  @Default(false)
  @Column(DataType.BOOLEAN)
  otp_verified: boolean;

  @Column(DataType.STRING)
  customer_name: string;

  @Column(DataType.STRING)
  invoice_no: string;

  @Column(DataType.STRING)
  customer_mobile: string;

  @Column(DataType.STRING)
  customer_email: string;

  @ForeignKey(() => Branch)
  @Column(DataType.STRING)
  branch_id: string;

  @ForeignKey(() => UserAddress)
  @AllowNull(true)
  @Column(DataType.STRING)
  address_id: string;

  @Column(DataType.INTEGER)
  total_services: number;

  @AllowNull(true)
  @Column(DataType.INTEGER)
  pay_tnx_id: number;

  @Column(DataType.STRING)
  payment_method: string;

  @Column(DataType.STRING)
  cancellation_reason: string;

  @Column(DataType.STRING)
  cancelled_by: string;

  @Column(DataType.STRING)
  cancelled_user_id: string;

  @Column(DataType.STRING)
  comment: string;

  @Default('Pending')
  @Column(DataType.STRING)
  order_status: string;

  @Column({ type: DataType.STRING, allowNull: true })
  remarks!: string;

  @Default(false)
  @Column(DataType.BOOLEAN)
  pay_by_cash: boolean;

  @Column(DataType.INTEGER)
  booking_slot: number;

  @Default(0)
  @Column(DataType.DOUBLE)
  initial_amount: number;

  @Default(0)
  @Column(DataType.DOUBLE)
  due_amount: number;

  @Default(false)
  @Column(DataType.BOOLEAN)
  partial_payment_completed: boolean;

  @Default(false)
  @Column(DataType.BOOLEAN)
  is_payment_completed: boolean;

  @Column(DataType.DOUBLE)
  gst: number;

  @Default(0)
  @Column(DataType.DOUBLE)
  coupon_discount: number;

  @Column({ type: DataType.STRING, allowNull: true })
  payment_id: string;

  @Column({ type: DataType.STRING, allowNull: true })
  partial_id: string;

  @Default(false)
  @Column({ type: DataType.BOOLEAN, allowNull: true })
  refunded: boolean;

  @Column({ type: DataType.STRING, allowNull: true })
  refund_status: string;

  @Default(0)
  @Column(DataType.DOUBLE)
  refund_amount: number;

  @Default(0)
  @Column(DataType.DOUBLE)
  payout_amount: number;

  @Default(false)
  @Column(DataType.BOOLEAN)
  payout: boolean;

  @Default(true)
  @Column(DataType.BOOLEAN)
  cancellation_fee: boolean;

  @Column(DataType.STRING)
  device: string;

  @Column(DataType.STRING)
  vendor_type: string;

  @Column({ type: DataType.STRING, allowNull: true })
  payout_id!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  payout_status!: string;

  @Column({ type: DataType.STRING(20), allowNull: true })
  order_approved_by!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  order_approved_id!: string;

  @Column({ type: DataType.STRING(20), allowNull: true })
  reschedule_by!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  reschedule_user_id!: string;

  @Default(0)
  @Column({ type: DataType.DOUBLE, allowNull: true })
  revenue_amount: number;

  @Column({
    type: DataType.DATE,
    // get() {
    // Get the raw value of the column
    // const rawValue = this.getDataValue('appointment_start_date_time');
    // const formattedDate = new Date(rawValue.toLocaleString('en-IN', { timeZone: 'Asia/Kolkata' }));
    // return formattedDate
    // },
  })
  appointment_start_date_time: Date;

  @BelongsTo(() => Branch)
  branch: Branch;

  @HasMany(() => Appointment, { sourceKey: 'order_id' })
  appointments: Appointment;

  @HasMany(() => OrderService)
  ordersService: OrderService[];

  @BelongsTo(() => UserAddress)
  address: UserAddress;

  @BelongsTo(() => User) //sasi imported
  user: User;

  @BelongsTo(() => Coupon) //sasi imported
  coupon: Coupon;

  @HasOne(() => Payment_Txn)
  payment_details: Payment_Txn;

  @HasOne(() => PartialPayment)
  partialPayment: PartialPayment;

  @BeforeCreate
  static beforeOrder(order: Order) {
    order.order_id = this.generatePaymentOrderId();
    order.payment_status = Payment_Status.PaymentOrderCreated;
    order.order_otp = Helpers.generateOTP();
  }

  static generateReceiptId(id: string) {
    var text = '';
    var charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = 0; i < 4; i++) {
      text += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    const random_number = Math.floor(Math.random() * (9999999 - 1000000 + 1) + 1000000);
    let res = `rcpt#${id}_${text}_${random_number.toString()}`;
    return res.slice(0, 40);
  }
  static generatePaymentOrderId() {
    const timestamp = new Date().getTime();
    var charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let text = '';
    for (var i = 0; i < 4; i++) {
      text += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    const paymentOrderId = `${text}_${timestamp}`;
    return paymentOrderId;
  }
}
