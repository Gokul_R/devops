import {
  Table,
  Model,
  Column,
  PrimaryKey,
  Default,
  AllowNull,
  DataType,
  BelongsToMany,
  HasMany,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';
import MarketingGroup from './MarketingGroup';
import NotificationMarketingMapping from './NotificationMarketingMapping';

//const time =Math.floor(new Date().getTime()/1000.0);
@Table({ timestamps: true, tableName: 'admin_notification', underscored: true, paranoid: true })
export default class AdminNotification extends BaseModel<AdminNotification> {
  @AllowNull(false)
  @Column(DataType.STRING)
  title!: string;

  // @AllowNull(false)
  // @Column(DataType.STRING)
  // marketing_group: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  description!: string;

  // @AllowNull(true)
  // @Column(DataType.DATE)
  // date!: Date;

  // @AllowNull(true)
  // @Column({
  //   type: DataType.STRING,
  //   // get() {
  //   //   return this.getDataValue('time') as string;
  //   // },
  //   // set(value: string) {
  //   //   this.setDataValue('time', value);
  //   // },
  // })
  // time!: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  image!: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  user_id!: string;

  @Column({ type: DataType.BIGINT, allowNull: true })
  trigger_time: number;// need to store this format - 1705041579741

  @Default(false)
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  later_sent: boolean;

  @AllowNull(true)
  @Column(DataType.ARRAY(DataType.TEXT))
  marketing_ids: string[];

  @AllowNull(true)
  @Column(DataType.STRING(50))
  scheduleTime!: string;

  @HasMany(() => NotificationMarketingMapping)
  notification_marketting: NotificationMarketingMapping;

  @BelongsToMany(() => MarketingGroup, () => NotificationMarketingMapping)
  marketing!: MarketingGroup;
}
