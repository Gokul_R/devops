import {
  Model,
  Column,
  ForeignKey,
  Table,
  BelongsTo,
  DataType,
  PrimaryKey,
  Default,
  HasMany,
} from 'sequelize-typescript';
import User from './User';
import Cart from './Cart';
import Coupon from './Coupons';
import Order from './Order';
import BaseModel from '../BaseModel';

@Table({ tableName: 'applied_coupons', underscored: true, timestamps: true,paranoid:true })
export default class AppliedCoupon extends BaseModel<AppliedCoupon> {
  @ForeignKey(() => User)
  @Column({ type: DataType.STRING, allowNull: false })
  public user_id!: string;

  @ForeignKey(() => Coupon)
  @Column({ type: DataType.STRING, allowNull: false })
  public coupon_id!: string;

  @Default(true)
  @Column({ type: DataType.BOOLEAN })
  is_applied: boolean;

  @Default(0)
  @Column({ type: DataType.INTEGER })
  used_count: number;

  @BelongsTo(() => User)
  public user!: User;

  @BelongsTo(() => Coupon)
  public coupon!: Coupon;

}
