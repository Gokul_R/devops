import {
  AllowNull,
  BelongsTo,
  Column,
  DataType,
  Default,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';
import Role from './Role';
import Screen from './Screen';

@Table({
  tableName: 'permissions',
  timestamps: true,
  underscored: true,
  paranoid: true,
})
export default class Permission extends BaseModel<Permission> {
  @AllowNull(true)
  @Column(DataType.STRING)
  permission_name!: string;

  @ForeignKey(() => Screen)
  @AllowNull(false)
  @Column(DataType.STRING)
  screen_id!: string;

  @Default(false)
  @Column(DataType.BOOLEAN)
  read!: boolean;

  @Default(false)
  @Column(DataType.BOOLEAN)
  write!: boolean;

  @Default(false)
  @Column(DataType.BOOLEAN)
  edit!: boolean;

  @Default(false)
  @Column(DataType.BOOLEAN)
  delete!: boolean;

  @BelongsTo(() => Screen)
  screen: Screen;
}
