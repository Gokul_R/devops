import {
  Table,
  Model,
  Column,
  PrimaryKey,
  AllowNull,
  Default,
  DataType,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ tableName: 'notification_config', timestamps: true, underscored: true, paranoid: true })
export default class NotificationConfig extends BaseModel<NotificationConfig> {
  // @AllowNull(false)
  // @Column(DataType.STRING)
  // app_id: string;
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @Column(DataType.STRING)
  id!: string;

  @AllowNull(false)
  @Column(DataType.TEXT)
  server_key: string;
  // @AllowNull(false)
  // @Column(DataType.STRING)
  // rest_api_key: string;

  // @AllowNull(false)
  // @Column(DataType.STRING)
  // user_auth_key: string;

  // @AllowNull(false)
  // @Column(DataType.INTEGER)
  // project_number: number;
}
