import {
  Model,
  DataType,
  Default,
  Column,
  PrimaryKey,
  Table,
  Unique,
  HasMany,
  BelongsToMany,
  AllowNull,
  ForeignKey,
  BelongsTo,
  HasOne,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';
import NotificationMarketingMapping from './NotificationMarketingMapping';
import AdminNotification from './AdminNotification';
import ReferalManage from './ReferalManage';
import Coupon from './Coupons';

@Table({
  timestamps: true,
  tableName: 'marketing_group',
  underscored: true,
  paranoid: true,
})
export default class MarketingGroup extends BaseModel<MarketingGroup> {
  @Unique(true)
  @Column(DataType.STRING)
  group_name: string;

  @Column(DataType.STRING)
  description: string;

  // @Default(false)
  // @Column(DataType.BOOLEAN)
  // is_user_mapping: boolean;

  @Default(false)
  @Column(DataType.BOOLEAN)
  is_referel: boolean;

  @ForeignKey(() => ReferalManage)
  @Column(DataType.STRING)
  referal_id: string;

  @Column(DataType.STRING)
  referal_code: string;

  @AllowNull(true)
  @Column(DataType.ARRAY(DataType.TEXT))
  users_mobile: string[];

  // @Column(DataType.DATE)
  // start_date!: Date;

  // @Column(DataType.DATE)
  // end_date!: Date;
  @BelongsTo(() => ReferalManage)
  referal: string;
  @HasOne(() => Coupon)
  coupon: Coupon;

  @BelongsToMany(() => AdminNotification, () => NotificationMarketingMapping)
  adminNotification: AdminNotification;
}
