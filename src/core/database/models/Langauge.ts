import {
  Table,
  Model,
  Column,
  DataType,
  Default,
  PrimaryKey,
  AllowNull,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ tableName: 'language', timestamps: true, underscored: true,paranoid:true })
export default class Language extends BaseModel<Language> {
  @AllowNull(false)
  @Column(DataType.STRING)
  language_name: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  flag: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  language_code: string;

  @AllowNull(false)
  @Default(false)
  @Column(DataType.BOOLEAN)
  make_default: boolean; 
}
