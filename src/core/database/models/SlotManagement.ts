import { Column, Table, DataType, Default } from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ tableName: 'slot_management', timestamps: true, underscored: true, paranoid: true })
class SlotManagement extends BaseModel<SlotManagement> {
  @Column(DataType.STRING)
  branch_id: string;

  @Column(DataType.DATEONLY)
  date: Date;
  @Default(0)
  @Column(DataType.INTEGER)
  no_of_employees: number;

  @Column(DataType.BOOLEAN)
  is_leave: boolean;

  @Column(DataType.STRING)
  slot_start_time: string;

  @Column(DataType.STRING)
  slot_end_time: string;

  @Column(DataType.BOOLEAN)
  morning: boolean;

  @Column(DataType.BOOLEAN)
  afternoon: boolean;

  @Column(DataType.BOOLEAN)
  evening: boolean;
}

export default SlotManagement;
