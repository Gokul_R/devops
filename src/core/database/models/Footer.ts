import {
  Table,
  Column,
  Model,
  AllowNull,
  PrimaryKey,
  DataType,
  Default,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName: 'footer', underscored: true,paranoid: true, })
export default class Footer extends BaseModel<Footer> {
  @AllowNull(true)
  @Column(DataType.STRING)
  footer_text: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  copyright_text: string;
}
