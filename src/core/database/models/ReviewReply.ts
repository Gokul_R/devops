import {
    Table,
    Column,
    Model,
    DataType,
    PrimaryKey,
    Default,
    ForeignKey,
    BelongsTo,
} from 'sequelize-typescript';

import Branch from './Branch';
import User from './User';
import BaseModel from '../BaseModel';
import Review from './Reviews';

@Table({
    tableName: 'reviews_reply',
    underscored: true,
    timestamps: true,
    paranoid: true,
})
export default class ReviewReply extends BaseModel<ReviewReply> {

    @Column(DataType.STRING)
    user_name!: string;

    @ForeignKey(() => Review)
    @Column(DataType.STRING)
    reply_review_id!: string;

    @Column(DataType.STRING)
    reply_review!: string;

    @Column(DataType.STRING(20))
    user_type!: string;

    @ForeignKey(() => User)
    @Column(DataType.STRING)
    reply_user_id!: string;

    @BelongsTo(() => Review)
    reviews!: Review;

    // @BelongsTo(() => User)
    // user!: User;
}

