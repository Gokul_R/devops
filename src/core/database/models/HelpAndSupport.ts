import { AllowNull, Column, DataType, Table } from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName: 'help_and_support', underscored: true, paranoid: true })
export default class HelpAndSupport extends BaseModel<HelpAndSupport> {
  @AllowNull(false)
  @Column(DataType.STRING)
  from_email: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  name: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  user_type: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  user_id: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  phone_number: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  message: string;
}
