import { AllowNull, Column, DataType, Default, Model, PrimaryKey, Table } from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName:'vendor_enquiry', underscored: true, paranoid: true })
export default class VendorEnquiry extends BaseModel<VendorEnquiry> {
  @AllowNull(true)
  @Column(DataType.STRING)
  name: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  email: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  phone_number: string;
}
