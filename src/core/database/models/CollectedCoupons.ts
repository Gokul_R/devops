import {
      Model,
      Column,
      ForeignKey,
      Table,
      BelongsTo,
      DataType,
      PrimaryKey,
      Default,
      HasMany,
    } from 'sequelize-typescript';
    import User from './User';
    import Cart from './Cart';
    import Coupon from './Coupons';
    import Order from './Order';
    import BaseModel from '../BaseModel';
    
    @Table({ tableName: 'collected_coupons', underscored: true, timestamps: true,paranoid:true })
    export default class CollectedCoupons extends BaseModel<CollectedCoupons> {
      @ForeignKey(() => User)
      @Column({ type: DataType.STRING, allowNull: false })
      public user_id!: string;
    
      @ForeignKey(() => Coupon)
      @Column({ type: DataType.STRING, allowNull: false })
      public coupon_id!: string;
    
      @Default(true)
      @Column({ type: DataType.BOOLEAN })
      is_collected: boolean;
        
      @BelongsTo(() => User)
      public user!: User;
    
      @BelongsTo(() => Coupon)
      public coupon!: Coupon;
    
    }
    