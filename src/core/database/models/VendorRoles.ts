import { AllowNull, BelongsTo, Column, DataType, ForeignKey, Table } from 'sequelize-typescript';
import BaseModel from '../BaseModel';
import Role from './Role';
import Vendor from './Vendor';

@Table({ timestamps: true, tableName: 'vendor_roles', underscored: true, paranoid: true })
export default class VendorRole extends BaseModel<VendorRole> {
    @Column(DataType.STRING)
    name: string;

    @Column(DataType.STRING)
    employee_id: string;

    @Column(DataType.STRING)
    email_id: string;

    @Column(DataType.STRING)
    password: string;

    @Column(DataType.STRING)
    image: string;

    @Column(DataType.STRING)
    mobile_no: string;

    @Column(DataType.ARRAY(DataType.STRING))
    branch_ids: string;

    @ForeignKey(() => Role)
    @Column(DataType.STRING)
    role_id: string;

    @ForeignKey(() => Vendor)
    @Column(DataType.STRING)
    vendor_id: string;

    @BelongsTo(() => Role)
    roles: Role;

    @BelongsTo(() => Vendor)
    vendor: Vendor;


}

