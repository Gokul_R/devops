import {
  Table,
  Model,
  Column,
  AutoIncrement,
  PrimaryKey,
  DataType,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ tableName: 'support_center', timestamps: true, underscored: true, paranoid: true })
export default class SupportCenter extends BaseModel<SupportCenter> {
  @Column({
    type: DataType.STRING(50),
    allowNull: false,
  })
  user_id?: string;

  @Column({
    type: DataType.ENUM('Service', 'General'),
    allowNull: false,
  })
  complaint_type?: string;

  @Column({
    type: DataType.STRING(50),
    allowNull: false,
  })
  order_id?: string;

  @Column({
    type: DataType.STRING(50),
    allowNull: false,
  })
  user_name?: string;

  @Column({
    type: DataType.STRING(20),
    allowNull: false,
  })
  user_ph?: string;

  @Column({
    type: DataType.STRING(100),
    allowNull: false,
  })
  user_email?: string;

  @Column({
    type: DataType.TEXT,
    allowNull: true,
  })
  message?: string;
}
