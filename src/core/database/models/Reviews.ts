import {
  Table,
  Column,
  Model,
  DataType,
  PrimaryKey,
  Default,
  ForeignKey,
  BelongsTo,
  HasMany,
  HasOne,
} from 'sequelize-typescript';

import Branch from './Branch';
import BranchServiceCategoryMapping from './BranchServiceCategoryMapping.ts';
import User from './User';
import BaseModel from '../BaseModel';
import ReviewReply from './ReviewReply';

@Table({
  tableName: 'reviews',
  underscored: true,
  timestamps: true,
  paranoid: true,
})
export default class Review extends BaseModel<Review> {
  @Default(0)
  @Column(DataType.INTEGER)
  rating!: number;

  @Column(DataType.TEXT)
  review!: string;

  @Column(DataType.ARRAY(DataType.TEXT))
  review_images: string[];

  @Column(DataType.STRING)
  user_name!: string;

  @ForeignKey(() => User)
  @Column(DataType.STRING)
  user_id!: string;

  @ForeignKey(() => Branch)
  @Column(DataType.STRING)
  branch_id!: string;

  @Column(DataType.STRING(20))
  user_type!: string;

  @BelongsTo(() => Branch)
  branch!: Branch;

  @BelongsTo(() => User)
  user!: User;

  @HasOne(() => ReviewReply)
  reply!: ReviewReply;
}

// Review.associate = function(models) {
//     Review.belongsTo(models.branch, {
//         foreignKey: {
//             name: 'branch_id',
//             allowNull: false
//         }
//     });
//     Review.belongsToMany(models.Service_category, {
//         through: 'review_service_categories',
//         as: 'categories',
//         foreignKey: 'review_id'
//     });
//     Review.belongsToMany(models.User, {
//         through: 'review_users',
//         as: 'users',
//         foreignKey: 'review_id'
//     });
// }
