import {
  Model,
  Column,
  Table,
  DataType,
  PrimaryKey,
  AutoIncrement,
  HasOne,
  AllowNull,
  Default,
} from 'sequelize-typescript';
import RewardChallengeHistroy from './RewardChallengeHistroy';
import BaseModel from '../BaseModel';

@Table({
  tableName: 'challenges',
  timestamps: true,
  underscored: true,
  paranoid: true,
})
export default class Challenges extends BaseModel<Challenges> {
  @AllowNull(false)
  @Column(DataType.ENUM('daily', 'weekly'))
  challange_type: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  title: string;

  // @Column({
  //   type: DataType.STRING(),
  //   allowNull: true,
  // })
  // marketing_group: string;

  @AllowNull(false)
  @Column(DataType.INTEGER)
  schedule_count!: number;
  
  @Default(0)
  @AllowNull(false)
  @Column(DataType.INTEGER)
  schedule_time!: number;

  // @AllowNull(false)
  // @Column(DataType.INTEGER)
  // clicking_count!: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  image!: string;

  @AllowNull(false)
  @Column(DataType.INTEGER)
  reward_coins!: number;

  @AllowNull(false)
  @Column(DataType.DATEONLY)
  start_date!: Date;

  @AllowNull(false)
  @Column(DataType.DATEONLY)
  end_date!: Date;

  @AllowNull(false)
  @Column(DataType.STRING)
  description!: string;

  // @HasOne(() => RewardChallengeHistroy)
  // challengeHistroy!: RewardChallengeHistroy;
}
