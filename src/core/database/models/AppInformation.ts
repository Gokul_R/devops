import {
  Table,
  Model,
  Column,
  DataType,
  Default,
  AllowNull,
  PrimaryKey,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ tableName: 'app_information', timestamps: true, underscored: true, paranoid: true })
export default class AppInformation extends BaseModel<AppInformation> {
  @AllowNull(true)
  @Column(DataType.STRING)
  ios_version: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  ios_app_link: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  android_version: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  android_app_link: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  vendor_android_version: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  vendor_android_app_link: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  vendor_ios_version: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  vendor_ios_app_link: string;
}
