import {
  Table,
  Model,
  PrimaryKey,
  DataType,
  Column,
  Default,
  AllowNull,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName: 'smtp_settings', underscored: true,paranoid:true })
export default class SmtpSettings extends BaseModel<SmtpSettings> {
  @AllowNull(false)
  @Column(DataType.STRING)
  mail_host: string;

  @AllowNull(false)
  @Column(DataType.INTEGER)
  mail_port: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  mail_username: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  mail_password: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  mail_encryption: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  mail_from_address: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  mail_from_name: string;
}
