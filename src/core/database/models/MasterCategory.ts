import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  HasOne,
  Table,
  Unique,
  HasMany,
  BeforeValidate,
  AutoIncrement,
  BeforeCreate,
  ForeignKey,
  BelongsTo,
  BelongsToMany,
} from 'sequelize-typescript';
import BranchCategoryMapping from './BranchCategoryMapping';
import CategoryServiceCategoryMappings from './CategoryServiceCatMapping';
import Service from './Services';
import BaseModel from '../BaseModel';
import Branch from './Branch';
import CouponCategoryMapping from './CouponCategoryMapping';
import Coupon from './Coupons';

@Table({ timestamps: true, tableName: 'master_categories', underscored: true, paranoid: true })
export default class MasterCategory extends BaseModel<MasterCategory> {
  @Column(DataType.STRING)
  category: string;
  @Column(DataType.STRING)
  description: string;
  @Column(DataType.STRING)
  image: string;

  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @Column({
    type: DataType.STRING,
  })
  code: string;

  @HasMany(() => BranchCategoryMapping)
  customer_type: BranchCategoryMapping;

  @HasMany(() => CategoryServiceCategoryMappings)
  service_category: CategoryServiceCategoryMappings;

  // @BelongsToMany(() => Coupon, () => CouponCategoryMapping)
  // coupons!: Coupon;

  @HasMany(() => Service)
  services: Service;

  @BeforeCreate
  static async generateId(instance: MasterCategory) {
    const prefix = 'CA_';
    const lastRecord: any = await MasterCategory.findOne({ order: [['code', 'DESC']] });
    const lastId = lastRecord?.code ? parseInt(lastRecord.code.replace(prefix, '')) : 0;
    const newId = lastId + 1;
    instance.code = `${prefix}${newId}`;
  }

  // @BeforeValidate
  // static addPrefix(instance: MasterCategory) {
  //   const prefix = 'MC';
  //   instance.id = `${prefix}${instance.id}`;
  // }
}
