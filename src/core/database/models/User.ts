import {
  Table,
  Model,
  Column,
  DataType,
  PrimaryKey,
  Unique,
  Default,
  AllowNull,
  BeforeCreate,
  HasOne,
  HasMany,
} from 'sequelize-typescript';
import Review from './Reviews';
import AppliedCoupon from './AppliedCoupons';
import Encryption from 'src/core/utils/encryption';
import Helpers from 'src/core/utils/helpers';
import BaseModel from '../BaseModel';
import Appointment from './Appointment';
import CollectedCoupons from './CollectedCoupons';

@Table({ timestamps: true, tableName: 'users', underscored: true, paranoid: true })
export default class User extends BaseModel<User> {
  @AllowNull(true)
  @Column(DataType.STRING)
  user_name: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  profile_url: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  email_id: string;

  @Column({
    type: DataType.STRING,
    set(value: string) {
      this.setDataValue(
        'password',
        value ? Encryption.hashPassword(value) : null,
      );
    },
  })
  password: string;

  @AllowNull(false)
  @Column({
    type: DataType.STRING,
    // set(value: string) {
    //   this.setDataValue(
    //     'mobile_no',
    //     value ? Helpers.encryptValue(value) : null,
    //   );
    // },
  })
  mobile_no: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  invited_ref_code: string;

  @Unique
  @AllowNull(true)
  @Column(DataType.STRING)
  referral_code: string;

  @AllowNull(false)
  @Column({ type: DataType.INTEGER, defaultValue: 0 })
  earn_coins: number;

  @AllowNull(false)
  @Default(false)
  @Column(DataType.BOOLEAN)
  email_verified: boolean;

  @AllowNull(true)
  // @Column(DataType.ENUM('Male', 'Female', 'Others'))
  @Column(DataType.STRING(15))
  gender: string;

  // @AllowNull(false)
  // @Column(DataType.BOOLEAN)
  // is_active?: boolean;

  @AllowNull(true)
  @Column(DataType.DATEONLY)
  dob: Date;

  // @Column(DataType.DATE)
  // created_at: Date;

  // @Column(DataType.DATE)
  // updated_at: Date;

  @Column(DataType.DATE)
  last_login: Date;

  @AllowNull(true)
  @Column(DataType.TEXT)
  device_token: string;

  @AllowNull(false)
  @Default(false)
  @Column(DataType.BOOLEAN)
  is_blocked: boolean;

  @AllowNull(false)
  @Default(false)
  @Column(DataType.BOOLEAN)
  is_mobile_verified: boolean;

  @AllowNull(true)
  @Column(DataType.STRING)
  created_by: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  updated_by: string;

  //ref code and is ref cod used columns need to add for marketing group

  @HasMany(() => Review)
  reviews: Review;

  @HasMany(() => AppliedCoupon)
  applied_coupons: AppliedCoupon;

  @HasMany(() => CollectedCoupons)
  collected_coupons: CollectedCoupons;

  static sequelize: any;

  private static generateUniqueReferralCode(): string {
    // Generate a random alphanumeric string with the given length
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < 6; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
  }
  @BeforeCreate
  static addRefCode(user: User) {
    user.referral_code = User.generateUniqueReferralCode();
    // user.email_id = await Encryption.encryptValue(
    //   User.sequelize,
    //   'anandroyal147@gmail.com',
    // );
  }
}
