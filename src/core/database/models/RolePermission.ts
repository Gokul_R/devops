import {
  Table,
  PrimaryKey,
  Column,
  ForeignKey,
  Model,
  DataType,
  BelongsTo,
} from 'sequelize-typescript';
import Permission from './Permission';
import Role from './Role';
import BaseModel from '../BaseModel';

@Table({
  timestamps: true,
  tableName: 'role_permission',
  underscored: true,
  paranoid: true,
})
export default class RolePermission extends BaseModel<RolePermission> {
  @ForeignKey(() => Role)
  @Column(DataType.STRING)
  role_id!: string;

  @ForeignKey(() => Permission)
  @Column
  permission_id!: string;

  @BelongsTo(() => Role)
  role: Role;

  @BelongsTo(() => Permission)
  permission: Permission;
}
