import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  Table,
  AutoIncrement,
  HasMany,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';
import Permission from './Permission';
import RolePermission from './RolePermission';
import Vendor from './Vendor';

@Table({
  timestamps: true,
  tableName: 'roles',
  underscored: true,
  paranoid: true,
})
export default class Role extends BaseModel<Role> {
  @Column(DataType.STRING)
  role_name: string;

  @ForeignKey(()=>Vendor)
  @Column(DataType.STRING)
  vendor_id: string;

  @Column(DataType.STRING)
  created_by: string;

  @Column(DataType.STRING)
  updated_by: string;

  @HasMany(() => RolePermission)
  permissions: RolePermission;

  @BelongsTo(() => Vendor)
  vendor: Vendor;
}
