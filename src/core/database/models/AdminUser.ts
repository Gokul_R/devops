import {
  Table,
  Column,
  Default,
  AllowNull,
  DataType,
  HasMany,
  AfterFind,
  HasOne,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Helpers from 'src/core/utils/helpers';
import UserRole from './UserRole';
import BaseModel from '../BaseModel';
import Role from './Role';
import Encryption from 'src/core/utils/encryption';

@Table({ timestamps: true, tableName: 'admin_users', underscored: true, paranoid: true })
export default class AdminUser extends BaseModel<AdminUser> {
  @AllowNull(true)
  @Column(DataType.STRING)
  name: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  employee_id: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  mobile_no: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  image: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  email_id: string;

  @AllowNull(false)
  @Column({
    type: DataType.STRING,
    // defaultValue: () => {
    //   let pass = Encryption.hashPassword(Helpers.generateRandomPassword());
    //   return pass;
    // },
    // set(value: string) {
    //   this.setDataValue('password', value ? Encryption.hashPassword(value) : null);
    // },
  })
  password: string;

  @ForeignKey(() => Role)
  @AllowNull(true)
  @Column(DataType.STRING)
  role_id: string;

  @AllowNull(false)
  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @AllowNull(true)
  @Column(DataType.DATE)
  last_login: Date;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  is_deleted: boolean;

  @BelongsTo(() => Role)
  roles: Role;

  @HasOne(() => UserRole)
  user_role: UserRole;

  // @AfterFind
  static async afterFindData(users: AdminUser[] | any): Promise<void> {
    if (Array.isArray(users)) {
    } else {
    }
  }
}

// users.forEach((user) => {
//   user?.user_role?.forEach((u_role) => {
//     u_role.role.permissions = u_role?.role?.permissions?.map(
//       (permit) => permit?.permission,
//     );
//   });
//   console.log(user)
// });

// users?.user_role?.forEach((user) => {
//   user.role.permissions = user?.role?.permissions?.map(
//     (permit: any) => permit?.permission,
//   );
// });
// }
