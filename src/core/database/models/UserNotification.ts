import {
    Table,
    Column,
    AllowNull,
    DataType,
    Default,

} from 'sequelize-typescript';
import BaseModel from '../BaseModel';


@Table({ timestamps: true, tableName: 'user_notification', underscored: true, paranoid: true })
export default class UserNotification extends BaseModel<UserNotification> {

    @AllowNull(true)
    @Column(DataType.STRING)
    title!: string;

    @AllowNull(true)
    @Column(DataType.TEXT)
    description!: string;

    @AllowNull(true)
    @Column(DataType.STRING)
    image!: string;

    @AllowNull(true)
    @Column(DataType.ARRAY(DataType.TEXT))
    user_id!: string[];

    @AllowNull(true)
    @Column(DataType.ARRAY(DataType.TEXT))
    viewers!: string[];

    @Default(false)
    @Column(DataType.BOOLEAN)
    is_view: boolean;
}
