import {
  Table,
  Column,
  Model,
  DataType,
  IsUUID,
  Default,
  PrimaryKey,
  AllowNull,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName: 'site_maintenance', underscored: true,paranoid:true })
export default class SiteMaintenance extends BaseModel<SiteMaintenance> {
  @AllowNull(true)
  @Column(DataType.STRING)
  maintenance_text: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  sub_text: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  image: string;
  
}
