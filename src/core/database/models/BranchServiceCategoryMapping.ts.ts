import {
  Model,
  DataType,
  Sequelize,
  Column,
  PrimaryKey,
  BelongsTo,
  Table,
  ForeignKey,
  Default,
  AfterFind,
} from 'sequelize-typescript';
import MasterServiceCategory from './MasterServiceCategory';
import Branch from './Branch';
import BaseModel from '../BaseModel';

@Table({
  timestamps: true,
  tableName: 'branch_service_category_mappings',
  underscored: true,
  paranoid: true,
})
export default class BranchServiceCategoryMapping extends BaseModel<BranchServiceCategoryMapping> {
  @ForeignKey(() => Branch)
  @Column(DataType.STRING)
  branch_id: string;

  @ForeignKey(() => MasterServiceCategory)
  @Column(DataType.STRING)
  service_category_id: string;

  @BelongsTo(() => Branch)
  branch: Branch;

  @BelongsTo(() => MasterServiceCategory)
  master_service_category: MasterServiceCategory;

  // @AfterFind
  // public static afterFindHook(
  //   result: BranchServiceCategoryMapping | BranchServiceCategoryMapping[],
  // ): void {
  //   if (Array.isArray(result)) {
  //     // If the result is an array of mapping models
  //     result.forEach((mappingModel: BranchServiceCategoryMapping) => {
  //       mappingModel.includeMasterData();
  //     });
  //   } else {
  //     // If the result is a single mapping model
  //     result.includeMasterData();
  //   }
  // }

  // Include the master model data
  // public async includeMasterData(): Promise<void> {
  //   // await this.reload({
  //   //   include: [MasterServiceCategory],
  //   // });
  // }
}
