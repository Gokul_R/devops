import {
  Table,
  Column,
  Model,
  DataType,
  PrimaryKey,
  Default,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Service from './Services';
import Branch from './Branch';
import BaseModel from '../BaseModel';

@Table({ tableName: 'flash_sales', timestamps: true, underscored: true, paranoid: true }) // Replace with your table name
class FlashSale extends BaseModel<FlashSale> {
  @ForeignKey(() => Service)
  @Column(DataType.STRING)
  service_id: string;

  @ForeignKey(() => Branch)
  @Column(DataType.STRING)
  branch_id: string;

  @Column(DataType.STRING)
  image: string;

  @Column(DataType.DATEONLY)
  start_date: Date;

  @Column(DataType.DATEONLY)
  end_date: Date;

  @Column(DataType.STRING)
  discount_type: string;

  @Column(DataType.INTEGER)
  discount_value: number;

  // @Column(DataType.FLOAT)
  // price_before_sale: number;

  // @Column(DataType.FLOAT)
  // price_after_sale: number;

  @Column(DataType.INTEGER)
  quantity_available: number;

  @Column(DataType.INTEGER)
  sold_quantity: number;

  @BelongsTo(() => Service)
  service: Service;

  @BelongsTo(() => Branch)
  branch: Branch;
}

export default FlashSale;
