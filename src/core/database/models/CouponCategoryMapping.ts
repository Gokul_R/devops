import {
    Table,
    Column,
    DataType,
    ForeignKey,
    BelongsTo,
    HasMany,
} from 'sequelize-typescript';
import Branch from './Branch';
import MasterCategory from './MasterCategory';
import Coupon from './Coupons';
import BaseModel from '../BaseModel';

@Table({
    timestamps: true,
    tableName: 'coupon_category_mappings',
    underscored: true,
    paranoid: false,
})
export default class CouponCategoryMapping extends BaseModel<CouponCategoryMapping> {
    @ForeignKey(() => MasterCategory)
    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    category_id: string;

    @ForeignKey(() => Coupon)
    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    coupon_id: string;

    @BelongsTo(() => MasterCategory)
    category_list: MasterCategory[];

    @BelongsTo(() => Coupon)
    coupons: Coupon;
}
