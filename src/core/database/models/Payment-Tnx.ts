import {
  DataType,
  PrimaryKey,
  Table,
  AutoIncrement,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Order from './Order';
import BaseModel from '../BaseModel';
@Table({
  timestamps: true,
  tableName: 'payment_txn',
  underscored: true,
  paranoid: true
})
export default class Payment_Txn extends BaseModel<Payment_Txn> {
  @AutoIncrement
  @PrimaryKey
  @Column
  pay_tnx_id?: number;
  @Column({
    type: DataType.STRING(50),
    allowNull: true,
    defaultValue: null,
  })
  user_id: string;

  @Column({
    type: DataType.STRING(50),
    allowNull: true,
    defaultValue: null,
  })
  receipt: string;
  @Column({
    type: DataType.STRING(50),
    allowNull: true,
    defaultValue: null,
  })
  payment_id: string;

  @Column({
    type: DataType.STRING(50),
    allowNull: true,
    defaultValue: null,
  })
  entity: string;
  @Column({
    type: DataType.DOUBLE,
    allowNull: false,
    defaultValue: 0,
  })
  amount: number;
  @Column({
    type: DataType.STRING(15),
    allowNull: true,
    defaultValue: null,
  })
  currency: string;
  @Column({
    type: DataType.STRING(25),
    allowNull: true,
    defaultValue: null,
  })
  status: string;

  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  invoice_id: string;
  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  })
  international: boolean;
  @Column({
    type: DataType.STRING(50),
    allowNull: false,
    defaultValue: false,
  })
  method: string;
  @Column({
    type: DataType.REAL,
    allowNull: false,
    defaultValue: 0,
  })
  amount_refunded: number;
  @Column({
    type: DataType.STRING(50),
    allowNull: true,
    defaultValue: null,
  })
  refund_status: string;
  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  })
  captured: boolean;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  description: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  card_id: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  bank: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  wallet: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  vpa: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  email: string;
  @Column({
    type: DataType.STRING(30),
    allowNull: true,
    defaultValue: null,
  })
  contact: string;
  @Column({
    type: DataType.STRING(50),
    allowNull: true,
    defaultValue: null,
  })
  customer_id: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  token_id: string;
  @Column({
    type: DataType.JSON,
    allowNull: true,
    defaultValue: null,
  })
  notes: object;
  @Column({
    type: DataType.INTEGER,
    allowNull: true,
    defaultValue: null,
  })
  fee: number;
  @Column({
    type: DataType.INTEGER,
    allowNull: true,
    defaultValue: null,
  })
  tax: number;
  @Column({
    type: DataType.STRING(50),
    allowNull: true,
    defaultValue: null,
  })
  error_code: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  error_description: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  error_source: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  error_step: string;
  @Column({
    type: DataType.STRING(100),
    allowNull: true,
    defaultValue: null,
  })
  error_reason: string;
  @Column({
    type: DataType.JSON,
    allowNull: true,
    defaultValue: null,
  })
  acquirer_data: object;


  @ForeignKey(() => Order)
  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  order_id: string;

  @BelongsTo(() => Order, { targetKey: 'order_id' })
  order: Order;
}
