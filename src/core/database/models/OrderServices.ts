import {
  Table,
  Column,
  DataType,
  ForeignKey,
  BelongsTo,
  Model,
} from 'sequelize-typescript';
import Branch from './Branch';
import BranchCategoryMapping from './BranchCategoryMapping';
import MasterCategory from './MasterCategory';
import Service from './Services';
import Coupon from './Coupons';
import BaseModel from '../BaseModel';
import Order from './Order';

@Table({
  timestamps: true,
  tableName: 'orders_service',
  underscored: true,
  paranoid: true,
})
export default class OrderService extends BaseModel<OrderService> {
  @ForeignKey(() => Order)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  order_id: string;

  @ForeignKey(() => Service)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  service_id: string;

  @BelongsTo(() => Order)
  order: Order;

  @BelongsTo(() => Service)
  service: Service;
}
