import { AllowNull, BelongsTo, Column, DataType, Default, ForeignKey, Table } from "sequelize-typescript";
import BaseModel from "../BaseModel";
import Order from "./Order";



@Table({ timestamps: true, tableName: 'partial_payments', underscored: true, paranoid: true })
export default class PartialPayment extends BaseModel<PartialPayment>{

    @ForeignKey(() => Order)
    @Column(DataType.STRING)
    order_id: string

    @AllowNull(true)
    @Column(DataType.STRING)
    pay_tnx_id: string;

    @Column({ type: DataType.STRING, allowNull: true })
    partial_payment_id: string;

    @Column({ type: DataType.STRING, allowNull: true })
    partial_payment_order_id: string;

    @Column({ type: DataType.STRING, allowNull: true })
    partial_payment_receipt_id: string

    @Column(DataType.DOUBLE)
    amount: number

    @Column(DataType.DOUBLE)
    payment_attempt: number

    @Default(false)
    @Column(DataType.BOOLEAN)
    is_payment_completed: boolean

    @Column(DataType.STRING)
    status: string

    @BelongsTo(() => Order)
    order: Order
}
