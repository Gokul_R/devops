import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  ForeignKey,
  BelongsTo,
  Table,
} from 'sequelize-typescript';
import MasterServiceType from './MasterServiceType';
import Branch from './Branch';
import MasterCategory from './MasterCategory';
import MasterServiceCategory from './MasterServiceCategory';
import BaseModel from '../BaseModel';

@Table({
  timestamps: true,
  tableName: 'category_service_category_mappings',
  underscored: true,
  paranoid: true
})
export default class CategoryServiceCategoryMappings extends BaseModel<CategoryServiceCategoryMappings> {
  @ForeignKey(() => MasterCategory)
  @Column(DataType.STRING)
  category_id: string;

  @ForeignKey(() => MasterServiceCategory)
  @Column(DataType.STRING)
  service_category_id: string;

  @BelongsTo(() => MasterCategory)
  category: MasterCategory;

  @BelongsTo(() => MasterServiceCategory)
  masterServiceCategory: MasterServiceCategory;
}
