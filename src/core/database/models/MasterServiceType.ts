import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  Table,
  HasOne,
} from 'sequelize-typescript';
import BranchServiceTypeMapping from './BranchServiceTypeMapping';
import BaseModel from '../BaseModel';

@Table({
  timestamps: true,
  tableName: 'master_service_types',
  underscored: true,
  paranoid: true
})
export default class MasterServiceType extends BaseModel<MasterServiceType> {
  @Column(DataType.STRING)
  service_type_name: string;

  @Column(DataType.STRING)
  description: string;

  @Column(DataType.STRING)
  image: string;
  
  @Default(false)
  @Column(DataType.BOOLEAN)
  freelancer: boolean;

  @HasOne(() => BranchServiceTypeMapping)
  service_type: BranchServiceTypeMapping;
}
