import {
  Model,
  Column,
  Table,
  DataType,
  ForeignKey,
  BelongsTo,
  HasMany,
  Default,
  HasOne,
} from 'sequelize-typescript';
import User from './User';
import Location from 'src/core/database/models/Location';

import Branch from './Branch';
import BaseModel from '../BaseModel';
@Table({
  tableName: 'favourites',
  underscored: true,
  timestamps: true,
  paranoid: true,
})
export default class Favourites extends BaseModel<Favourites> {
  @ForeignKey(() => User)
  @Column({ type: DataType.STRING, allowNull: false })
  user_id: string;

  @ForeignKey(() => Branch)
  @Column({ type: DataType.STRING, allowNull: false })
  branch_id: string;

  // @Column({ type: DataType.STRING, allowNull: false })
  // type: string;

  @Column({ type: DataType.STRING, allowNull: true })
  created_by: string;

  @Column({ type: DataType.STRING, allowNull: true })
  updated_by: string;

  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active!: boolean;

  @BelongsTo(() => Branch, 'branch_id')
  branch: Branch;
}
