import {
  Table,
  Column,
  DataType,
  BeforeCreate,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';
import MarketingGroup from './MarketingGroup';
import User from './User';

@Table({
  tableName: 'referal_manage',
  timestamps: true,
  underscored: true,
  paranoid: true,
})
export default class ReferalManage extends BaseModel<ReferalManage> {
  @Column(DataType.STRING)
  referal_name: string;

  @Column(DataType.STRING)
  description: string;
  @Column(DataType.STRING)
  referal_code: string;

  // @ForeignKey(() => MarketingGroup)
  // @Column(DataType.STRING)
  // marketing_ids: string;

  // @BelongsTo(() => MarketingGroup)
  // marketing_group: MarketingGroup;


  @BeforeCreate
  static async validateCouponCode(instance: ReferalManage) {
    if (!instance.is_deleted) {
      const existingCoupon = await ReferalManage.findOne({
        where: {
          referal_code: instance.referal_code,
          is_deleted: false,
        },
      });
      if (existingCoupon) {
        throw new Error('Referal code must be unique.');
      }
    }
  }
}
