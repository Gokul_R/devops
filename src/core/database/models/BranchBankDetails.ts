import { AfterBulkCreate, AfterCreate, AfterFind, BeforeFind, BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import Branch from './Branch';
import Helpers from 'src/core/utils/helpers';
import BaseModel from '../BaseModel';
import Encryption from 'src/core/utils/encryption';

@Table({
  tableName: 'branch_bank_details',
  timestamps: true,
  underscored: true,
  paranoid: true

})
export default class BranchBankDetails extends BaseModel<BranchBankDetails> {


  @ForeignKey(() => Branch)
  @Column({ type: DataType.STRING })
  branch_id: string;

  @Column({
    type: DataType.STRING, set(value: string) {
      this.setDataValue("account_holder", value ?
        Encryption.encryptValue(value) : null);
    }
  })

  account_holder: string;

  @Column({
    type: DataType.STRING, set(value: string) {
      this.setDataValue("account_no", value ?
        Encryption.encryptValue(value) : null);
    }
  })
  account_no: string;

  @Column({
    type: DataType.STRING, set(value: string) {
      this.setDataValue("ifsc_code", value ?
        Encryption.encryptValue(value) : null);
    }
  })
  ifsc_code: string;

  @Column({
    type: DataType.STRING, set(value: string) {
      this.setDataValue("account_branch", value ?
        Encryption.encryptValue(value) : null);
    }
  })
  account_branch: string;
  @Column({
    type: DataType.STRING, set(value: string) {
      this.setDataValue("account_type", value ?
        Encryption.encryptValue(value) : null);
    }
  })
  account_type: string;


  @AfterFind

  static async decryptPassword(auths: BranchBankDetails[] | any): Promise<void> {
    if (Array.isArray(auths)) {
      for (const user of auths) {
        if (user) await this.extracAndDecryptBranchBankDetails(user?.dataValues);
      }
    } else {
      if (auths) await this.extracAndDecryptBranchBankDetails(auths?.dataValues);
    }

  }

  static async extracAndDecryptBranchBankDetails(auth: any) {
    let decryptData = ['account_type', 'account_branch', 'ifsc_code', 'account_no', 'account_holder'];
    let keys = Object.keys(auth);
    for (const key of keys) {
      if (decryptData.includes(key)) {
        // console.log(key);
        auth[key] = await Encryption.decryptValue(auth[key]);
      } else {
        auth[key] = auth[key];
      }
    }
    return auth;


  }
  @BelongsTo(() => Branch)
  branch: Branch;

}
