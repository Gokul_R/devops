import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  BelongsTo,
  Table,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';
import User from './User';
// import { User } from './User';

@Table({ timestamps: true, tableName: 'otp',underscored:true,paranoid:true })
export default class Otp extends BaseModel<Otp> {
  @Column(DataType.STRING)
  userId: string;

  @Column(DataType.STRING(6))
  otp: string;

  @Column(DataType.DATE)
  expires_at: Date;

  @Column(DataType.DATE)
  created_at: Date;

  

    // @BelongsTo(() => User)
    // user: User;
}

// Otp.belongsTo(User, { foreignKey: 'userId', targetKey: 'id' });
