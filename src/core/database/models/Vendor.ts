import {
  Table,
  Model,
  Column,
  DataType,
  PrimaryKey,
  Unique,
  Default,
  AllowNull,
  HasMany,
  AfterFind,
  HasOne,
  ForeignKey
} from 'sequelize-typescript';
import Branch from './Branch';
import Helpers from 'src/core/utils/helpers';
import BaseModel from '../BaseModel';
import UserRole from './UserRole';
import Encryption from 'src/core/utils/encryption';
import VendorRole from './VendorRoles';
import Role from './Role';

@Table({ timestamps: true, tableName: 'vendor', underscored: true, paranoid: true })
export default class Vendor extends Model<Vendor> {
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @Column(DataType.STRING)
  id: string;

  @AllowNull(false)
  @Default(false)
  @Column(DataType.BOOLEAN)
  password_sent: boolean;

  @AllowNull(true)
  @Column(DataType.STRING)
  vendor_name: string;

  @AllowNull(true)
  //@Unique
  @Column(DataType.STRING)
  email_id: string;


  @AllowNull(false)
  //@Unique
  @Column(DataType.STRING)
  mobile_no: string;

  @AllowNull(true)
  @Column(DataType.TEXT)
  address: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  state: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  city: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  area: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  country: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  pincode: string;

  // @Default(false)
  // @Column(DataType.BOOLEAN)
  // manual_register: boolean;

  // @AllowNull(true)
  // @Column(DataType.BOOLEAN)
  // tax: boolean;

  // @AllowNull(true)
  // @Column(DataType.STRING)
  // commision: string;


  // @AllowNull(true)
  // @Column(DataType.STRING)
  // amount: string;

  @AllowNull(true)
  @Column(DataType.DATE)
  last_login: Date;

  @AllowNull(true)
  @Column({
    type: DataType.STRING, set(value: string) {
      this.setDataValue("pancard_num", value ?
        Encryption.encryptValue(value) : null);
    }
  })
  pancard_num: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  role_id: string;

  @HasOne(() => UserRole)
  user_role: UserRole;

  @AllowNull(true)
  @Column({
    type: DataType.STRING, set(value: string) {
      this.setDataValue("pancard_url", value ?
        Encryption.encryptValue(value) : null);
    }
  })
  pancard_url: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  profile_url: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  remarks: string;

  @AllowNull(true)
  @Column({
    type: DataType.STRING, set(value: string) {
      this.setDataValue("aadharcard_num", value ?
        Encryption.encryptValue(value) : null);
    }
  })
  aadharcard_num: string;


  @AllowNull(true)
  @Column(DataType.STRING)
  vendor_type: string;

  @AllowNull(true)
  @Column(DataType.TEXT)
  device_token: string;

  @AllowNull(true)
  @Column({
    type: DataType.STRING,
    // defaultValue: () => {
    //   let pass= Encryption.hashPassword(Helpers.generateRandomPassword());
    //   return pass
    // // },
    // set(value: string) {
    //   // this.setDataValue('password', value ? Helpers.encryptValue(value) : null);
    //   this.setDataValue('password', value ? Encryption.hashPassword(value) : null);
    // },
  })
  password: string;

  @AllowNull(true)
  @Column({
    type: DataType.STRING, set(value: string) {
      this.setDataValue("aadharcard_url", value ?
        Encryption.encryptValue(value) : null);
    }
  })
  aadharcard_url: string;

  @Default(false)
  @Column(DataType.BOOLEAN)
  is_approved: boolean;

  @Default(0)
  @Column({ type: DataType.INTEGER, allowNull: false })
  approval_status!: number;

  @AllowNull(false)
  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @AllowNull(false)
  @Default(false)
  @Column(DataType.BOOLEAN)
  is_deleted: boolean;

  @AllowNull(false)
  @Default(true)
  @Column(DataType.BOOLEAN)
  status: boolean;

  @HasMany(() => Branch)
  branches: Branch[];

  @HasMany(() => VendorRole)
  vendorRole: VendorRole[];

  @HasMany(() => Role)
  roles: Role[];

  @AfterFind
  static async decryptData(auths: Vendor[] | any): Promise<void> {
    if (Array.isArray(auths)) {
      for (const user of auths) {
        if (user) await this.extracAndDecryptVendor(user?.dataValues);
      }
    } else {
      if (auths) await this.extracAndDecryptVendor(auths?.dataValues);
    }

  }

  static async extracAndDecryptVendor(auth: any) {
    let decryptData = ['aadharcard_url', 'aadharcard_num', 'pancard_url', 'pancard_num'];
    let keys = Object.keys(auth);
    for (const key of keys) {
      if (decryptData.includes(key)) {
        auth[key] = await Encryption.decryptValue(auth[key]);
      } else {
        auth[key] = auth[key];
      }
    }
    return auth;


  }
}


