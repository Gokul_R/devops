import {
  Table,
  PrimaryKey,
  Column,
  ForeignKey,
  DataType,
  Default,
  Model,
  BelongsTo,
} from 'sequelize-typescript';
import Role from './Role';
import BaseModel from '../BaseModel';
import AdminUser from './AdminUser';
import Vendor from './Vendor';

@Table({
  timestamps: true,
  tableName: 'user_roles',
  underscored: true,
  paranoid: true,
})

export default class UserRole extends BaseModel<UserRole> {
  @ForeignKey(() => AdminUser)
  @Column(DataType.STRING)
  user_id!: string;
  @ForeignKey(() => Vendor)
  @Column(DataType.STRING)
  vendor_id!: string;

  @ForeignKey(() => Role)
  @Column(DataType.STRING)
  role_id!: string;

  @BelongsTo(() => AdminUser)
  user: AdminUser;

  // @BelongsTo(() => Vendor)
  // vendor: Vendor;

  @BelongsTo(() => Role)
  role: Role;
}
