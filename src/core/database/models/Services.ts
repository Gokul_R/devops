import {
  Model,
  DataType,
  Column,
  PrimaryKey,
  HasMany,
  Table,
  Default,
  ForeignKey,
  BelongsTo,
  HasOne,
  BelongsToMany,
  AfterDestroy,
  AfterUpdate,
  BeforeUpdate,
} from 'sequelize-typescript';
import Appointment from './Appointment';
import Branch from './Branch';
import Cart from './Cart';
import Location from './Location';
import Coupon from './Coupons';
import FlashSale from './FlashSale';
import CouponServiceMapping from './CouponServiceMapping';
import MasterCategory from './MasterCategory';
import MasterServiceCategory from './MasterServiceCategory';
import BaseModel from '../BaseModel';
import CartServiceMapping from './CartServiceMapping';
import Order from './Order';
import OrderService from './OrderServices';

@Table({
  timestamps: true,
  tableName: 'services',
  underscored: true,
  paranoid: true,
})
export default class Service extends BaseModel<Service> {
  @Column(DataType.STRING)
  service_name: string;

  @Column(DataType.STRING)
  image: string;

  @Column(DataType.INTEGER)
  duration: number;

  @Default(false)
  @Column(DataType.BOOLEAN)
  bridal_service: boolean;

  @ForeignKey(() => Branch)
  @Column(DataType.STRING)
  branch_id: string;

  @ForeignKey(() => MasterServiceCategory)
  @Column(DataType.STRING)
  service_category_id: string;

  @Column(DataType.INTEGER)
  amount: number;

  // @Column(DataType.INTEGER)
  @Column(DataType.DOUBLE(10, 2))
  discount: number;

  @ForeignKey(() => MasterCategory)
  @Column(DataType.STRING)
  category_id: string;

  @Column(DataType.DOUBLE(10, 2))
  discounted_price: number;

  @Column(DataType.STRING)
  description: string;

  @HasMany(() => Appointment, 'service_id')
  appointments: Appointment[];

  @HasMany(() => Cart)
  cart: Cart;

  @HasMany(() => FlashSale)
  flash_sales: FlashSale[];

  @HasMany(() => OrderService)
  ordersService: OrderService;

  @BelongsToMany(() => Coupon, () => CouponServiceMapping)
  coupons!: Coupon;

  @HasMany(() => CouponServiceMapping)
  CouponServiceMapping: CouponServiceMapping;

  @BelongsTo(() => Branch)
  branch: Branch;

  @BelongsTo(() => MasterCategory)
  category: MasterCategory;

  @BelongsTo(() => MasterServiceCategory)
  service_category: MasterServiceCategory;

  @HasOne(() => Location, { sourceKey: 'branch_id' })
  location: Location;

  // @HasMany(() => CustomerType, { sourceKey: 'customer_types' })
  // customer_type: CustomerType;

  @BeforeUpdate
  static afterServiceUpdate(instance, options) {
    // console.log('----------->');
  }
}
