import { AllowNull, BelongsTo, Column, DataType, Default, ForeignKey, Table } from "sequelize-typescript";
import BaseModel from "../BaseModel";
import Branch from "./Branch";
import User from "./User";
import Order from "./Order";

export enum PayOutRefund {
    PayOut = 'PayOut',
    Refund = 'Refund',
    FundAccount = 'FundAccount'
}

@Table({ timestamps: true, tableName: 'payout_refund_tnx', underscored: true, paranoid: true })
export default class PayoutRefundTnx extends BaseModel<PayoutRefundTnx> {

    // @ForeignKey(() => User)
    @AllowNull(true)
    @Column(DataType.STRING)
    user_id!: string;

    @ForeignKey(() => Branch)
    @AllowNull(true)
    @Column(DataType.STRING)
    branch_id!: string;

    @ForeignKey(() => Order)
    @AllowNull(true)
    @Column(DataType.STRING)
    order_id!: string;

    @AllowNull(true)
    @Column(DataType.STRING)
    event_id!: string;

    @AllowNull(true)
    @Column(DataType.STRING)
    reference_id!: string;

    // @Column({ type: DataType.ENUM(PayOutRefund.PayOut, PayOutRefund.Refund) })
    @AllowNull(true)
    @Column(DataType.STRING)
    tnx_type: string;

    @AllowNull(true)
    @Column(DataType.STRING)
    payout_id!: string;

    @AllowNull(true)
    @Column(DataType.STRING)
    merchant_id!: string;

    @AllowNull(true)
    @Column(DataType.STRING)
    utr!: string;

    @Default(0)
    @Column(DataType.DOUBLE)
    payout_amount: number;

    @AllowNull(true)
    @Column(DataType.STRING)
    status!: string;

    @AllowNull(true)
    @Column(DataType.JSON)
    request_response!: object;

    @AllowNull(true)
    @Column(DataType.JSON)
    update_response!: object;

    @BelongsTo(() => Branch)
    branch: Branch;
    @BelongsTo(() => Order)
    order: Order;

}