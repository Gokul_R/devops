import {
  Table,
  Model,
  Column,
  AllowNull,
  PrimaryKey,
  DataType,
  Default,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

enum PaymentMethods {
  RAZORPAY = 'Razorpay',
  Payg = 'Payg',
}

@Table({ timestamps: true, tableName: 'payment_config', underscored: true, paranoid: true })
export default class PaymentConfig extends BaseModel<PaymentConfig> {
  // @AllowNull(false)
  // @Column(DataType.BOOLEAN)
  // cash_on_service: boolean;

  @AllowNull(false)
  @Column(DataType.STRING)
  authentication_key: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  authentication_token: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  secure_hash_key: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  merchant_key_id: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  razorpay_secret: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  razorpay_key: string;

  @Default("")
  @AllowNull(false)
  @Column(DataType.STRING)
  active_payment_method: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  account_number: string;

  @AllowNull(false)
  @Default(0)
  @Column(DataType.DOUBLE)
  within_24h_pct!: number;

  @AllowNull(false)
  @Default(0)
  @Column(DataType.DOUBLE)
  before_24h_pct!: number;

  @AllowNull(false)
  @Default(0)
  @Column(DataType.DOUBLE)
  rejectedby_admin_pct!: number;
}
