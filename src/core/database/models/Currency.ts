import {
  Table,
  Model,
  Default,
  DataType,
  Column,
  AllowNull,
  PrimaryKey,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName: 'currency', underscored: true ,paranoid: true})
export default class Currency extends BaseModel<Currency> {
  @AllowNull(false)
  @Column(DataType.STRING)
  currency_name: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  currency_symbol: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  currency_code: string;

  // @AllowNull(false)
  // @Column(DataType.STRING)
  // rate: string;

  @AllowNull(false)
  @Column(DataType.ENUM('0', '1'))
  alignment: number;
}
