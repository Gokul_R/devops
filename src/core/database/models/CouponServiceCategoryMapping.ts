import {
    Table,
    Column,
    DataType,
    ForeignKey,
    BelongsTo,
} from 'sequelize-typescript';
import Branch from './Branch';
import MasterCategory from './MasterCategory';
import Coupon from './Coupons';
import BaseModel from '../BaseModel';
import MasterServiceCategory from './MasterServiceCategory';

@Table({
    timestamps: true,
    tableName: 'coupon_service_category_mappings',
    underscored: true,
    paranoid: false,
})
export default class CouponServiceCategoryMapping extends BaseModel<CouponServiceCategoryMapping> {
    @ForeignKey(() => MasterServiceCategory)
    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    service_category_id: string;

    @ForeignKey(() => Coupon)
    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    coupon_id: string;

    @BelongsTo(() => MasterServiceCategory)
    service_cat_list: MasterServiceCategory[];

    @BelongsTo(() => Coupon)
    coupons: Coupon;
}
