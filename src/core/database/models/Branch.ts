import {
  Table,
  Column,
  Model,
  DataType,
  HasMany,
  HasOne,
  Default,
  AllowNull,
  BelongsTo,
  BelongsToMany,
  AfterFind,
  ForeignKey,
  BeforeCreate,
} from 'sequelize-typescript';
import BranchCategoryMapping from './BranchCategoryMapping';
import BranchServiceCategoryMapping from './BranchServiceCategoryMapping.ts';
import BranchServiceTypeMapping from './BranchServiceTypeMapping';
import SlotDetail from './SlotDetail';
import Location from './Location';
import Review from './Reviews';
import Cart from './Cart';
import Coupon from './Coupons';
import Favourites from './Favourites';
import FlashSale from './FlashSale';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import CouponBranchMapping from './CouponBranchMapping';
import Vendor from './Vendor';
import BranchBankDetails from './BranchBankDetails';
import Helpers from 'src/core/utils/helpers';
import BaseModel from '../BaseModel';
import Service from './Services';
import Order from './Order';
import MasterCategory from './MasterCategory';
import Encryption from 'src/core/utils/encryption';
//import OrdersReport from './OrdersReport';


enum CommissionType {
  PERCENTAGE = 'percentage',
  AMOUNT = 'amount',
}

@Table({
  tableName: BRANCH_ATTRIBUTES.TABLE_NAME,
  timestamps: true,
  underscored: true,
  paranoid: true,
})
export default class Branch extends BaseModel<Branch> {
  @Column({ type: DataType.STRING, allowNull: true })
  reg_id!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  branch_name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  branch_email!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  branch_contact_no!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  branch_landline!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  remarks!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  vendor_type!: string;

  @ForeignKey(() => Vendor)
  @Column({ type: DataType.STRING, allowNull: false })
  vendor_id!: string;

  @AllowNull(true)
  @Column(DataType.TEXT)
  branch_image: string;

  @Column({ type: DataType.STRING, allowNull: true })
  secondary_mobile!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  std_code!: string;

  @Default(false)
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  business_verified!: boolean;

  @Default(0)
  @Column({ type: DataType.INTEGER, allowNull: false })
  approval_status!: number;

  @Column({ type: DataType.STRING, allowNull: true })
  payment_verification!: string;

  // @Column({ type: DataType.STRING, allowNull: true })
  // advance_percentage!: string;

  @Column({ type: DataType.INTEGER, allowNull: true })
  payment_commission!: number;

  // @Column({ type: DataType.INTEGER, allowNull: true })
  // advance_payment!: number;

  // @Column({ type: DataType.ENUM('percentage', 'amount') })
  @Column({ type: DataType.STRING, allowNull: true })
  commission_type: string;

  // @Column({ type: DataType.STRING, allowNull: true })
  // advance_payment_type: string;


  @Default(false)
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  is_tax_checked!: boolean;

  @Column({ type: DataType.INTEGER, allowNull: true })
  tax_percentage: number;

  @Column(DataType.STRING)
  website_url?: string;

  // @Column(DataType.STRING)
  // aadhaar_no?: string;

  // @Column(DataType.STRING)
  // pan_no?: string;

  // @Column(DataType.STRING)
  // pan_url?: string;

  // @Column(DataType.STRING)
  // aadhar_url?: string;

  // @Column(DataType.STRING)
  // business_url?: string;

  // @Column(DataType.STRING)
  // businesslogo_url?: string;

  @Column({
    type: DataType.STRING,
    set(value: string) {
      this.setDataValue(
        'gst_number',
        value ? Encryption.encryptValue(value) : null,
      );
    },
  })
  gst_number?: string;


  @Column({
    type: DataType.STRING,
    set(value: string) {
      this.setDataValue(
        'gst_document_url',
        value ? Encryption.encryptValue(value) : null,
      );
    },
  })
  gst_document_url?: string;

  @Column(DataType.STRING)
  instagram?: string;

  @Column(DataType.STRING)
  facebook?: string;

  @Column(DataType.STRING)
  linkedin?: string;

  @Column(DataType.STRING)
  open_time?: string;

  @Column(DataType.STRING)
  close_time?: string;

  @Column(DataType.ARRAY(DataType.INTEGER))
  leave_days?: Array<number>;

  @Column(DataType.STRING)
  contact_person?: string;

  @Column(DataType.STRING)  //create razorpay contact 
  contact_id?: string;

  @Column(DataType.STRING)   //create razorpay bank account
  fund_account_id?: string;

  @Column(DataType.STRING)  //verify bank account in send 1 rupee from razorpay 
  validate_account_id?: string;

  @Default(false)  //verify bank account in send 1 rupee then true 
  @Column({ type: DataType.BOOLEAN, allowNull: false })
  account_verified?: boolean;

  @Column(DataType.STRING)
  account_verify_status?: string;

  @AllowNull(true)
  @Column(DataType.DATE)
  last_login: Date;

  @HasMany(() => BranchCategoryMapping, { onDelete: 'CASCADE' })
  catagories!: BranchCategoryMapping[];

  @HasMany(() => BranchServiceCategoryMapping, { onDelete: 'CASCADE' })
  service_categories!: BranchServiceCategoryMapping[];

  @HasMany(() => BranchServiceTypeMapping, { onDelete: 'CASCADE' })
  service_types!: BranchServiceTypeMapping[];

  @HasMany(() => SlotDetail, { onDelete: 'CASCADE' })
  slot_details!: SlotDetail[];

  @HasMany(() => Review, { onDelete: 'CASCADE' })
  reviews!: Review[];
  @HasMany(() => Service, { onDelete: 'CASCADE' })
  services!: Service[];

  @HasOne(() => Location, { onDelete: 'CASCADE' })
  location!: Location;

  @HasOne(() => Favourites, { onDelete: 'CASCADE' })
  favorites!: Favourites;


  @BelongsToMany(() => Coupon, () => CouponBranchMapping)
  coupons!: Coupon;

  @BelongsTo(() => Vendor)
  vendor!: Vendor;

  @HasMany(() => Order)
  order!: Order;

  // @HasMany(() => MasterCategory)
  // category!: MasterCategory;

  @HasMany(() => Cart, { onDelete: 'CASCADE' })
  cart: Cart;

  @HasMany(() => FlashSale, { onDelete: 'CASCADE' })
  flash_sales: FlashSale;

  @HasOne(() => BranchBankDetails, { onDelete: 'CASCADE' })
  bank_details: BranchBankDetails;

  @BeforeCreate
  static generateCustomValue(instance: Branch) {
    const customValue = 'SLAY' + Math.floor(Math.random() * 10000);
    instance.reg_id = customValue;
  }
  @AfterFind
  static async decryptPassword(auths: Branch[] | any): Promise<void> {
    if (Array.isArray(auths)) {
      for (const user of auths) {
        if (user) await this.extracAndDecrypt(user?.dataValues);
      }
    } else {
      if (auths) await this.extracAndDecrypt(auths?.dataValues);
    }
  }

  static async extracAndDecrypt(auth: any) {
    let incluedeData = ['bank_details', 'vendor'];
    let deCryptData = ['gst_number', 'gst_document_url'];
    let keys = Object.keys(auth);
    for (const key of keys) {
      if (incluedeData.includes(key)) {
        if (auth[key]) {
          if (key === 'vendor') {
            auth[key].dataValues = await Vendor.extracAndDecryptVendor(
              auth[key].dataValues,
            );
          } else {
            auth[key].dataValues =
              await BranchBankDetails.extracAndDecryptBranchBankDetails(
                auth[key].dataValues,
              );
          }
        } else {
          auth[key] = auth[key];
        }
      } else if (deCryptData.includes(key)) {
        auth[key] = auth[key] ? Encryption.decryptValue(auth[key]) : auth[key];
      } else {
        auth[key] = auth[key];
      }
    }
    return auth;
  }
}
