import {
  AllowNull,
  Column,
  DataType,
  Default,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

enum BannerType {
  HOME = 0,
  FLASH_SALE = 1,
}

@Table({
  timestamps: true,
  tableName: 'banner',
  underscored: true,
  paranoid: true,
})
export default class Banner extends BaseModel<Banner> {
  @AllowNull(false)
  @Column(DataType.STRING)
  image: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  banner_name: string;

  // @AllowNull(false)
  // @Column(DataType.INTEGER)
  // banner_type: number;

  // @AllowNull(false)
  // @Column(DataType.STRING)
  // add_vendor: string;

  @AllowNull(false)
  @Column(DataType.DATEONLY)
  start_date: Date;

  @AllowNull(false)
  @Column(DataType.DATEONLY)
  end_date: Date;

  @Default(false)
  @AllowNull(false)
  @Column(DataType.BOOLEAN)
  is_mobile: boolean;
}
