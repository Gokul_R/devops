import {
  Model,
  Column,
  Table,
  DataType,
  ForeignKey,
  BelongsTo,
  HasMany,
  Default,
  PrimaryKey,
} from 'sequelize-typescript';
import User from './User';
import Branch from './Branch';
import Services from './Services';
import { Col } from 'sequelize/types/utils';
import AppliedCoupon from './AppliedCoupons';
import Coupon from './Coupons';
import BaseModel from '../BaseModel';
import CartServiceMapping from './CartServiceMapping';

@Table({
  tableName: 'cart',
  underscored: true,
  timestamps: true,
  paranoid: true,
})
export default class Cart extends BaseModel<Cart> {
  @ForeignKey(() => User)
  @Column({ type: DataType.STRING, allowNull: false })
  user_id: string;

  @ForeignKey(() => Branch)
  @Column({ type: DataType.STRING, allowNull: false })
  branch_id: string;

  @ForeignKey(() => Services)
  @Column({ type: DataType.STRING, allowNull: false })
  service_id: string;

  // @Column({ type: DataType.STRING, allowNull: false })
  // cart_id: string;

  @Column({ type: DataType.STRING, allowNull: true })
  created_by: string;

  @Column({ type: DataType.STRING, allowNull: true })
  updated_by: string;

  @BelongsTo(() => User)
  user_detail: User;

  @BelongsTo(() => Services)
  services: Services;

  @BelongsTo(() => Branch)
  branch: Branch;


  // @HasMany(() => AppliedCoupon)
  // applied_coupons: AppliedCoupon;

  // @HasMany(()=>Coupon,{sourceKey:'cart_id'})
  // coupon:Coupon
}
