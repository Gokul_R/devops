import {
  Model,
  Table,
  Column,
  DataType,
  Default,
  PrimaryKey,
  HasMany,
  AllowNull,
  BelongsToMany,
  BeforeCreate,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Branch from './Branch';
import Service from './Services';
import AppliedCoupon from './AppliedCoupons';
import { CouponDiscountType } from 'src/admin/coupon/dto/create-coupon.dto';
import CouponBranchMapping from './CouponBranchMapping';
import CouponServiceMapping from './CouponServiceMapping';
import BaseModel from '../BaseModel';
import { ApiProperty } from '@nestjs/swagger';
import MasterCategory from './MasterCategory';
import CouponCategoryMapping from './CouponCategoryMapping';
import MarketingGroup from './MarketingGroup';
import CouponServiceCategoryMapping from './CouponServiceCategoryMapping';
import CollectedCoupons from './CollectedCoupons';
import Order from './Order';

@Table({ tableName: 'coupons', timestamps: true, underscored: true, paranoid: true })
export default class Coupon extends BaseModel<Coupon> {
  @ApiProperty()
  @Column(DataType.STRING)
  coupon_code!: string;

  @ApiProperty()
  @Column(DataType.STRING)
  image!: string;

  // @ForeignKey(()=>Branch)
  @ApiProperty()
  @AllowNull(true)
  @Column(DataType.STRING)
  vendor_id!: string;

  @AllowNull(true)
  @Column(DataType.ARRAY(DataType.TEXT))
  branch_ids: string[];

  @ForeignKey(() => MarketingGroup)
  @Column(DataType.STRING)
  marketing_ids!: string;

  @ApiProperty()
  // @Column(DataType.ENUM(CouponDiscountType.AMOUNT, CouponDiscountType.PERCENTAGE))
  @Column(DataType.STRING(20))
  discount_type!: string;

  @ApiProperty()
  @Column(DataType.INTEGER)
  discount_value!: number;

  @ApiProperty()
  @Column(DataType.STRING)
  quantity_type!: string;

  @ApiProperty()
  @Column(DataType.INTEGER)
  quantity_value!: number;

  @ApiProperty()
  @Column(DataType.INTEGER)
  per_user_limit!: number;

  @ApiProperty()
  @Column(DataType.INTEGER)
  minimum_spend!: number;

  @ApiProperty()
  @Column(DataType.INTEGER)
  max_discount_amount!: number;

  @ApiProperty()
  @Column(DataType.DATEONLY)
  start_date!: Date;

  @ApiProperty()
  @Column(DataType.DATEONLY)
  end_date!: Date;

  @ApiProperty()
  @Column(DataType.STRING)
  grouped_coupon: string;

  @ApiProperty()
  @Column(DataType.STRING(15))
  provided_by: string;

  @ApiProperty()
  @Column(DataType.STRING)
  provided_id: string;

  @ApiProperty()
  @Column(DataType.STRING)
  coupon_name: string;

  @ApiProperty()
  @Column(DataType.BOOLEAN)
  lifetime_coupon: boolean;


  // @ApiProperty()
  // @Column(DataType.STRING)
  // category_id: string;

  // @ApiProperty()
  // @Column(DataType.ARRAY(DataType.STRING))
  // service_category_id: string;

  // @ApiProperty()
  // @Default(null)
  // @Column(DataType.BOOLEAN)
  // grouped_coupon_type!: boolean;

  @ApiProperty()
  @Column(DataType.STRING)
  description!: string;

  @HasMany(() => AppliedCoupon)
  applied_coupons: AppliedCoupon;

  @HasMany(() => CollectedCoupons)
  collected_coupons: CollectedCoupons;

  @HasMany(() => Order)
  order: Order;

  @BelongsToMany(() => Branch, () => CouponBranchMapping)
  branch: Branch;

  @BelongsToMany(() => Service, () => CouponServiceMapping)
  services: CouponServiceMapping;

  @BelongsTo(() => MarketingGroup)
  marketingGroup: MarketingGroup;

  @HasMany(() => CouponBranchMapping)
  coupon_branch_mapping: CouponBranchMapping[];
  @HasMany(() => CouponServiceMapping)
  coupon_service_mapping: CouponServiceMapping[];
  @HasMany(() => CouponCategoryMapping)
  coupon_category_mapping: CouponCategoryMapping[];
  @HasMany(() => CouponServiceCategoryMapping)
  coupon_service_category_mapping: CouponServiceCategoryMapping[];

  // @BelongsToMany(() => MasterCategory, () => CouponCategoryMapping)
  // category_List: CouponCategoryMapping;
  // @BelongsToMany(() => Service, () => CouponServiceMapping)
  // services: CouponServiceMapping;

  @BeforeCreate
  static async validateCouponCode(instance: Coupon) {
    if (!instance.is_deleted) {
      const existingCoupon = await Coupon.findOne({
        where: {
          coupon_code: instance.coupon_code,
          is_deleted: false,
        },
      });

      if (existingCoupon) {
        throw new Error('Coupon code must be unique.');
      }
    }
  }
}
