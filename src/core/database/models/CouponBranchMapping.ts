import {
  Table,
  Column,
  DataType,
  ForeignKey,
  BelongsTo,
  Model,
} from 'sequelize-typescript';
import Branch from './Branch';
import BranchCategoryMapping from './BranchCategoryMapping';
import MasterCategory from './MasterCategory';
import Service from './Services';
import Coupon from './Coupons';
import BaseModel from '../BaseModel';

@Table({
  timestamps: true,
  tableName: 'coupon_branch_mappings',
  underscored: true,
  paranoid: false,
})
export default class CouponBranchMapping extends BaseModel<BranchCategoryMapping> {
  @ForeignKey(() => Branch)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  branch_id: string;

  @ForeignKey(() => Coupon)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  coupon_id: string;

  @BelongsTo(() => Branch)
  branch: Branch;

  @BelongsTo(() => Coupon)
  coupons: Coupon;
}
