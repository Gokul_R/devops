import {
  Model,
  DataType,
  Default,
  Column,
  PrimaryKey,
  BelongsTo,
  Table,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import User from './User';
import Order from './Order'; 
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName: 'user_addresses', underscored: true ,paranoid:true})
export default class UserAddress extends BaseModel<UserAddress> {
  @Column(DataType.STRING)
  customer_name: string;

  @Column(DataType.STRING)
  customer_mobile: string;

  @Column(DataType.STRING)
  customer_email: string;

  @Column(DataType.STRING)
  address_type: string;

  @Column(DataType.STRING)
  flat_no: string;

  @Column(DataType.STRING)
  area: string;

  @Column(DataType.STRING)
  city: string;

  @Column(DataType.STRING)
  zipcode: string;

  @Column(DataType.STRING)
  state: string;

  @Column(DataType.STRING)
  country: string;

  @Column(DataType.STRING)
  message: string;

  // @ForeignKey(() => Order)
  @ForeignKey(() => User)
  @Column(DataType.STRING)
  user_id: string;

  @BelongsTo(() => User)
  user: User;
  @HasMany(() => Order)
  order: Order;
}

// Location.belongsTo(ServiceProvider, { foreignKey: 'branch_id', targetKey: 'id' });
