import {
  Model,
  Column,
  Table,
  DataType,
  PrimaryKey,
  AutoIncrement,
  HasOne,
  AllowNull,
} from 'sequelize-typescript';
import RewardChallengeHistroy from './RewardChallengeHistroy';
import BaseModel from '../BaseModel';

@Table({
  tableName: 'rewards',
  timestamps: true,
  underscored: true,
  paranoid: true,
})
export default class Rewards extends BaseModel<Rewards> {
  @AllowNull(false)
  @Column(DataType.STRING)
  title!: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  reward_type!: string;

  @AllowNull(false)
  @Column(DataType.INTEGER)
  spend_coins!: number;

  @AllowNull(false)
  @Column(DataType.DATE)
  start_date!: Date;

  @AllowNull(false)
  @Column(DataType.DATE)
  end_date!: Date;

  @AllowNull(false)
  @Column(DataType.STRING)
  description!: string;

  // @HasOne(() => RewardChallengeHistroy)
  // rewardHistroy!: RewardChallengeHistroy;
}
