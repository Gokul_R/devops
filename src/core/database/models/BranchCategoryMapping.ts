import {
  Model,
  DataType,
  Column,
  ForeignKey,
  BelongsTo,
  Table,
} from 'sequelize-typescript';
import Branch from './Branch';
import MasterCategory from './MasterCategory';
import Service from './Services';
import BaseModel from '../BaseModel';

@Table({
  timestamps: true,
  tableName: 'branch_category_mappings',
  underscored: true,
  paranoid: true,
})
export default class BranchCategoryMapping extends BaseModel<BranchCategoryMapping> {
  @ForeignKey(() => Branch)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  branch_id: string;

  @ForeignKey(() => MasterCategory)
  @ForeignKey(() => Service)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  category_id: string;

  @BelongsTo(() => Branch)
  branch: Branch;

  @BelongsTo(() => MasterCategory)
  master_category: MasterCategory;
}
