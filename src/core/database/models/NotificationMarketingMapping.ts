import {
  BelongsTo,
  BelongsToMany,
  Column,
  DataType,
  ForeignKey,
  Table,
} from 'sequelize-typescript';
import AdminNotification from './AdminNotification';
import MarketingGroup from './MarketingGroup';
import BaseModel from '../BaseModel';

@Table({
  timestamps: true,
  tableName: 'notification_marketing_mapping',
  underscored: true
  ,paranoid:true
})
export default class NotificationMarketingMapping extends BaseModel<NotificationMarketingMapping> {
  @ForeignKey(() => AdminNotification)
  @Column(DataType.STRING)
  admin_notification_id?: string;

  @ForeignKey(() => MarketingGroup)
  @Column(DataType.STRING)
  marketing_id?: string;

  @BelongsTo(() => AdminNotification)
  adminNotification: AdminNotification;

  @BelongsTo(() => MarketingGroup)
  marketingGroup: MarketingGroup;

 
}
