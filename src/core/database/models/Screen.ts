import {
    Column,
    DataType,
    Default,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Unique,
  } from 'sequelize-typescript';
  import BaseModel from '../BaseModel';
import Permission from './Permission';
  
  @Table({
    timestamps: true,
    tableName: 'screens',
    underscored: true,
    paranoid: true,
  })
  export default class Screen extends BaseModel<Screen> {
    // @Unique
    @Column(DataType.STRING)
    screen_name!: string;

    @Unique
    @Column(DataType.STRING)
    code!: string;

    @Default(false)
    @Column(DataType.BOOLEAN)
    read!: boolean;
  
    @Default(false)
    @Column(DataType.BOOLEAN)
    write!: boolean;
  
    @Default(false)
    @Column(DataType.BOOLEAN)
    edit!: boolean;
  
    @Default(false)
    @Column(DataType.BOOLEAN)
    delete!: boolean;

    @Default(false)
    @Column(DataType.BOOLEAN)
    is_vendor!: boolean;

    @HasMany(()=>Permission)
    permission:Permission

    
  }
  