import { Model, DataType, Default, Column, Table, BelongsTo, ForeignKey } from 'sequelize-typescript';
import User from './User';
import BaseModel from '../BaseModel';
import { timestamp } from 'aws-sdk/clients/cloudfront';

@Table({ tableName: 'reserved_slots', timestamps: true, underscored: true, paranoid: true })
export default class ReservedSlots extends BaseModel<ReservedSlots> {
  @Column(DataType.STRING(60))
  user_id: string;

  @Column(DataType.STRING(60))
  branch_id: string;

  @Column({
    type: DataType.DATE,
  })
  appointment_start_date_time: Date;

  @Column(DataType.BOOLEAN)
  is_reserved: boolean;
}
