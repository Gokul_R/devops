import { Column, DataType, Default, PrimaryKey, Table } from "sequelize-typescript";
import BaseModel from "../BaseModel";
import { TriggerType } from "src/core/constants";

@Table({
    timestamps: true,
    tableName: 'order_remainder',
    underscored: true,
    paranoid: true,
})

export default class OrderRemainder extends BaseModel<OrderRemainder> {

    @Column({ type: DataType.STRING(100), allowNull: true })
    order_id: string;

    @Column({ type: DataType.STRING(100), allowNull: true, })
    user_email: string;

    @Column({ type: DataType.STRING(100), allowNull: true, })
    branch_email: string;

    @Column({ type: DataType.ENUM(TriggerType.OrderRemain), allowNull: true, })
    trigger_type: string;

    @Column({ type: DataType.BIGINT, allowNull: true })
    trigger_time: number;

    @Default(false)
    @Column({ type: DataType.BOOLEAN, allowNull: false })
    is_sent: boolean;

    @Column({ type: DataType.JSON, allowNull: true })
    appointment_data: object;

    @Column({ type: DataType.JSON, allowNull: true })
    order_data: object;

    @Column({ type: DataType.JSON, allowNull: true })
    error_data: object;
}