import {
  Table,
  Column,
  Model,
  AllowNull,
  PrimaryKey,
  Default,
  DataType,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ tableName: 'logo', timestamps: true, underscored: true,paranoid:true })
export default class Logo extends BaseModel<Logo> {
  @AllowNull(true)
  @Column(DataType.STRING)
  header_logo: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  footer_logo: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  favicon: string;
}
