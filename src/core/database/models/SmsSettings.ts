import {
  Table,
  Column,
  Model,
  DataType,
  Default,
  PrimaryKey,
  AllowNull,
} from 'sequelize-typescript';
import BaseModel from '../BaseModel';

@Table({ timestamps: true, tableName: 'sms_settings', underscored: true, paranoid: true })
export default class SmsSettings extends BaseModel<SmsSettings> {
  @AllowNull(false)
  @Column(DataType.BOOLEAN)
  user_verification: boolean;

  @AllowNull(false)
  @Column(DataType.STRING)
  country_code: string;

  @AllowNull(false)
  @Column(DataType.ENUM('text-local', 'twilio'))
  sms_type: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  twilio_number: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  twilio_sid: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  twilio_auth_token: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  textlocal_api: string;
}
