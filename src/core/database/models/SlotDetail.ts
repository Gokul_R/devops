import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  BelongsTo,
  Table,
  ForeignKey,
  Unique,
} from 'sequelize-typescript';
import Branch from './Branch';
import BaseModel from '../BaseModel';
// import { ServiceProvider } from './ServiceProvider';

@Table({
  tableName: 'slot_details',
  timestamps: true,
  underscored: true,
  paranoid: true,
})
export default class SlotDetail extends BaseModel<SlotDetail> {
  @Column(DataType.INTEGER)
  day: number;

  @Column(DataType.STRING)
  slot_start_time: string;

  @Column(DataType.STRING)
  slot_end_time: string;

  @Column(DataType.INTEGER)
  no_of_employees: number;

  @Column(DataType.BOOLEAN)
  morning: boolean;

  @Column(DataType.BOOLEAN)
  afternoon: boolean;

  @Column(DataType.BOOLEAN)
  evening: boolean;

  @ForeignKey(() => Branch)
  @Column(DataType.STRING)
  branch_id: string;

  @BelongsTo(() => Branch)
  branch: Branch;
}

// SlotDetail.belongsTo(ServiceProvider, { foreignKey: 'branch_id', targetKey: 'id' });
