import {
  Model,
  DataType,
  Sequelize,
  Default,
  Column,
  PrimaryKey,
  ForeignKey,
  BelongsTo,
  Table,
} from 'sequelize-typescript';
import MasterServiceType from './MasterServiceType';
import Branch from './Branch';
import BaseModel from '../BaseModel';
import Cart from './Cart';
import Service from './Services';

@Table({
  timestamps: true,
  tableName: 'cart_service_mappings',
  underscored: true,
  paranoid: true,
})
export default class CartServiceMapping extends BaseModel<CartServiceMapping> {
  @ForeignKey(() => Cart)
  @Column(DataType.STRING)
  cart_id: string;

  @ForeignKey(() => Service)
  @Column(DataType.STRING)
  service_id: string;

  @BelongsTo(() => Service)
  service: Service;

  @BelongsTo(() => Cart)
  cart: Cart;
}
