import {
  Table,
  Column,
  DataType,
  ForeignKey,
  BelongsTo,
  Model,
} from 'sequelize-typescript';
import Branch from './Branch';
import BranchCategoryMapping from './BranchCategoryMapping';
import MasterCategory from './MasterCategory';
import Service from './Services';
import Coupon from './Coupons';
import BaseModel from '../BaseModel';

@Table({
  timestamps: true,
  tableName: 'coupon_service_mappings',
  underscored: true,
  paranoid: false,
})
export default class CouponServiceMapping extends BaseModel<CouponServiceMapping> {
  @ForeignKey(() => Service)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  service_id: string;

  @ForeignKey(() => Coupon)
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  coupon_id: string;

  @BelongsTo(() => Service)
  services: Service;

  @BelongsTo(() => Coupon)
  coupons: Coupon;
}
