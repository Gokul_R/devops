import {
  Model,
  DataType,
  Default,
  Column,
  Table,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import User from './User';
import BaseModel from '../BaseModel';

@Table({ tableName: 'referral_histroy', timestamps: true,underscored:true,paranoid:true })
export default class ReferralHistroy extends BaseModel<ReferralHistroy> {
  @Column(DataType.STRING(60))
  user_id: string;

  @ForeignKey(() => User)
  @Column(DataType.STRING(60))
  referral_user_id: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  earn_coins: number;

  @BelongsTo(() => User, 'referral_user_id')
  referral_user: User;
}
