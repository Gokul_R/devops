import {
  Model,
  Column,
  Table,
  DataType,
  PrimaryKey,
  Default,
  HasMany,
  BeforeCreate,
} from 'sequelize-typescript';
import CategoryServiceCategoryMappings from './CategoryServiceCatMapping';
import Service from './Services';
import BaseModel from '../BaseModel';
import CouponServiceCategoryMapping from './CouponServiceCategoryMapping';
import BranchServiceCategoryMapping from './BranchServiceCategoryMapping.ts';

@Table({
  timestamps: true,
  tableName: 'master_service_categories',
  underscored: true,
  paranoid: true
})
export default class MasterServiceCategory extends BaseModel<MasterServiceCategory> {
  @Column(DataType.STRING)
  service_category_name: string;

  @Column(DataType.STRING)
  image: string;

  @Column(DataType.STRING)
  description: string;

  @Column({
    type: DataType.STRING,
  })
  code: string;

  @HasMany(() => CategoryServiceCategoryMappings)
  categories: CategoryServiceCategoryMappings;

  @HasMany(() => CouponServiceCategoryMapping)
  couponServiceCategoryMapping: CouponServiceCategoryMapping;

  @HasMany(() => BranchServiceCategoryMapping)
  branchServiceCategoryMapping: BranchServiceCategoryMapping;

  @HasMany(() => Service)
  services: Service;
  @BeforeCreate
  static async generateId(instance: MasterServiceCategory) {
    const prefix = 'SC_';
    const lastRecord: any = await MasterServiceCategory.findOne({
      order: [['code', 'DESC']],
    });
    const lastId = lastRecord?.code
      ? parseInt(lastRecord.code.replace(prefix, ''))
      : 0;
    const newId = lastId + 1;
    instance.code = `${prefix}${newId}`;
  }
}
