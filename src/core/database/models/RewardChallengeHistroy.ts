import {
  Model,
  Column,
  Table,
  DataType,
  PrimaryKey,
  AutoIncrement,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Challenges from './Challenges';
import Rewards from './Rewards';
import BaseModel from '../BaseModel';

@Table({
  tableName: 'rewardChallenge_histroy',
  timestamps: true,
  underscored: true,
  paranoid: true,
})
export default class RewardChallengeHistroy extends BaseModel<RewardChallengeHistroy> {
  // @ForeignKey(() => Rewards)
  // @Column({
  //   type: DataType.STRING,
  //   allowNull: true,
  // })
  // rewards_id: string;

  // @ForeignKey(() => Rewards)
  // @Column(DataType.STRING)
  // rewards_id: string;

  // @ForeignKey(() => Challenges)
  @Column(DataType.STRING)
  challange_id: string;

  // @ForeignKey(() => Challenges)
  // @Column({
  //   type: DataType.STRING,
  //   allowNull: true,
  // })
  // challenge_id: string;

  // @Column({
  //   type: DataType.STRING(),
  //   allowNull: true,
  // })
  // user_id: string;

  // @Column({
  //   type: DataType.SMALLINT,
  //   allowNull: true,
  //   defaultValue: 0,
  // })
  // attempt_count: string;

  // @Column({
  //   type: DataType.BOOLEAN,
  //   allowNull: true,
  // })
  // is_rewards: boolean;

  // @BelongsTo(() => Rewards)
  // rewards: Rewards;

  // @BelongsTo(() => Challenges)
  // challanges: Challenges;
}
