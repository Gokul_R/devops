import { ModelStatic, Sequelize } from 'sequelize';
import { QueryInterface, ColumnOptions } from 'sequelize/types';
import { ModelCtor } from 'sequelize/types';

// Create Sequelize instance
const sequelize = new Sequelize(/* Sequelize configuration */);

// Import all Sequelize models
// import models from './models'; // Assuming your models are located in the './models' directory
import { ModelType } from 'sequelize-typescript';
import { join } from 'path';
import glob from 'glob';

// (async () => {
//   try {
//     let models= getModels();
//     // Iterate over all models
//     for (const modelName of getModels()) {
//       const Model = models[modelName] as ModelStatic<ModelType>;

//       // Get the table name associated with the model
//       const tableName = Model.getTableName();

//       // Retrieve the columns in the table from the database
//       const existingColumns = await sequelize.getQueryInterface().describeTable(tableName) as Record<string, any>;

//       // Check if each desired column exists in the table
//       for (const [columnName, columnDefinition] of Object.entries(Model.rawAttributes)) {
//         if (!existingColumns[columnName]) {
//           // Add the missing column using the migration method
//           await sequelize.getQueryInterface().addColumn(tableName, columnName, columnDefinition);
//           console.log(`Added column "${columnName}" to table "${tableName}"`);
//         }
//       }
//     }

//     console.log('Migration completed successfully.');
//   } catch (error) {
//     console.error('Migration failed:', error);
//   } finally {
//     // Close the database connection
//     await sequelize.close();
//   }
// })();

function getModels() {
  const modelsPath = join(__dirname, '/models');
  const files = glob.sync(`${modelsPath}/**/*.js`);
  const models = files.map((file) => require(file).default);
  // console.log('---', models);
  return models;
}
