import { Column, DataType, Default, Model, PrimaryKey, Sequelize } from 'sequelize-typescript';


 class BaseModel<T> extends Model {
    
  @Default(DataType.UUIDV4)
  @PrimaryKey
  @Column(DataType.STRING)
  id!: string;
  
  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active!: boolean;
  
  @Default(false)
  @Column(DataType.BOOLEAN)
  is_deleted!: boolean;


}

export default BaseModel;
  