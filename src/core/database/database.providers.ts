import { Sequelize } from 'sequelize-typescript';
import { SEQUELIZE, DEVELOPMENT, TEST, PRODUCTION, STAGING, LOCAL } from '../constants';
import { databaseConfig } from './database.config';
import { join } from 'path';
import * as glob from 'glob';
import { logger } from '../utils/logger';
import { newLogger } from '../utils/loggerNew';
export let FRONTEND_BASE_URL: string;
export const databaseProviders = [
  {
    provide: SEQUELIZE,
    value: Sequelize,
    useFactory: async () => {
      let config;
      switch (process.env.NODE_ENV) {
        case DEVELOPMENT:
          config = databaseConfig.development;
          FRONTEND_BASE_URL = databaseConfig?.development?.frontEndBaseUrl;
          break;
        case LOCAL:
          config = databaseConfig.local;
          FRONTEND_BASE_URL = databaseConfig?.local?.frontEndBaseUrl;
          break;
        case STAGING:
          config = databaseConfig.staging;
          FRONTEND_BASE_URL = databaseConfig?.staging?.frontEndBaseUrl;
          break;
        case PRODUCTION:
          config = databaseConfig.production;
          FRONTEND_BASE_URL = databaseConfig?.production?.frontEndBaseUrl;
          break;
        default:
          config = databaseConfig.development;
          FRONTEND_BASE_URL = databaseConfig?.development?.frontEndBaseUrl;
      }
      console.log('database details--------->', config);
      logger.info('Database_Config: ' + JSON.stringify(config));
      // newLogger.info({
      //   type: 'database',
      //   response: { Database_Config: config },
      // });
      let sequelize = new Sequelize({
        dialectOptions: {
          useUTC: true,
          // dateStrings: true,
          // typeCast: true,
          timezone: 'UTC',
        },
        // timezone: 'Asia/Kolkata',
        ...config,
        sync: {
          alter: true,
          // force: true
        },
        logging: process.env.NODE_ENV != PRODUCTION,
      });

      sequelize.addModels(getModels());
      sequelize
        .authenticate()
        .then(() => {
          console.log('Database connection has been established successfully.');
          logger.info('Database connection has been established successfully.');
          // newLogger.info({ type: 'database', response: 'Database connection has been established successfully.' });
        })
        .catch((error) => {
          console.error('Unable to connect to the database:', error);
          logger.error('Unable to connect to the database:' + JSON.stringify(error));
          // newLogger.error({
          //   type: 'database',
          //   response: { Unable_to_connect_to_the_database: error },
          // });
        });
      sequelize
        .sync()
        .then(() => {
          console.log('All models have been synchronized successfully.');
          logger.info('All models have been synchronized successfully.');
          // newLogger.info({
          //   type: 'database',
          //   response: 'All models have been synchronized successfully.',
          // });
        })
        .catch((error) => {
          console.error('Unable to add models to the database:', error);
          logger.error('Unable to add models to the database:' + JSON.stringify(error));
          newLogger.error('Unable to add models to the database:' + JSON.stringify(error));
          // newLogger.error({
          //   type: 'database',
          //   response: { Unable_to_add_models_to_the_database: error },
          // });
        });
      return sequelize;
    },
  },
];
function getModels() {
  const modelsPath = join(__dirname, '/models');
  const files = glob.sync(`${modelsPath}/**/*.js`);
  const models = files.map((file) => require(file).default);
  logger.info('DatabaseModelImportedCount: ' + JSON.stringify(models?.length || models));
  // newLogger.info({
  //   type: 'database',
  //   response: { DatabaseModelImportedCount: models?.length || models },
  // });
  return models;
}
