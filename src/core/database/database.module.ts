import { Module } from '@nestjs/common';
import { databaseProviders } from './database.providers';
@Module({
  // imports: [
    // SequelizeModule.forRoot({
    //   dialect: 'postgres',
    //   host: '74.50.86.238', //74.50.86.238
    //   port: 5432,
    //   username: 'postgres',
    //   password: '1234',

    //   database: 'slaylewks',
    //   // sync: {
    //   //   force: true,
    //   //   alter: true,
    //   // },
    //   // define:{
    //   //       // Disable foreign key constraints globally
    //   // },
    //   // dialectOptions: {
    //   //   supportBigNumbers: true,
    //   //   bigNumberStrings: true,
    //   //   foreignKeyConstraint: false,
    //   // },
    //   pool: {
    //     max: 100,
    //     min: 0,
    //     idle: 10000,
    //   },
    //   autoLoadModels: true,
    //   synchronize: false,
    //   logging: true,
    // }),
    // SequelizeModule.forFeature(getModels()),
  // ],
  providers: [...databaseProviders],
  exports: [...databaseProviders],
})
export class DatabaseModule {}
