'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.renameTable('service_providers', 'shops');
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.renameTable('shops', 'service_providers');
  }
};