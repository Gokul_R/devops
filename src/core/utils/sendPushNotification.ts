import { Logger } from '@nestjs/common';
import NotificationConfig from '../database/models/NotificationConfig';
import { logger } from './logger';
import { CreateNotificationDto } from 'src/modules/notification/dto/create-notification.dto';
import { Op } from 'sequelize';
import { PRODUCTION, Slaylewks_details } from '../constants';
import UserNotification from '../database/models/UserNotification';
import { newLogger } from './loggerNew';

export class SendPushNotification {
  // constructor(private message: any) {
  //     this.sendNotification(message);
  // }

  public sendNotification = async (request: any) => {
    try {
      const getServerKey = await NotificationConfig.findOne();
      if (!getServerKey || !getServerKey?.dataValues || !request) return;
      const FCM = require('fcm-node');
      var fcm = new FCM(getServerKey?.dataValues?.server_key);
      let send = await fcm.send(request, function (err: any, response: any) {
        if (err) {
          logger.error('Send_pushNotification_Error: ' + JSON.stringify(err));
          newLogger.error({
            type: 'pushNotification',
            body: request,
            response: { Send_pushNotification_Error: err },
          });
          console.log('Something has gone wrong! Push Notification', err);
        } else {
          logger.info('Send_pushNotification_200: ' + JSON.stringify(response));
          newLogger.info({
            type: 'pushNotification',
            body: request,
            response: { Send_pushNotification_200: response },
          });
          console.log('Successfully sent with response: ', response);
        }
      });
      return true;
    } catch (error: any) {
      console.log('Something has gone wrong! Push Notification', error);
      Logger.error(`Something has gone wrong! Push Notification ` + JSON.stringify(error));
      newLogger.error({
        type: 'pushNotification',
        body:request,
        response: { Something_has_gone_wrong_Push_Notification: error },
      });
    }
  };

  public async sendNotifyToUserPhone(req_body: any) {
    try {
      let userId: any = req_body?.user_id
        ? [{ id: req_body?.user_id }, { device_token: { [Op.not]: null } }]
        : [{ device_token: { [Op.not]: null } }];
      let getUsersToken: any = await req_body?.dataModels?.findAll({
        attributes: ['device_token', 'id'],
        where: {
          [Op.and]: userId,
        },
      });
      if (!getUsersToken || getUsersToken?.length == 0) return;
      let usersId = getUsersToken.map((userId) => userId?.id);
      getUsersToken = getUsersToken.map((userToken) => userToken?.device_token);
      let requestObj = {
        registration_ids: getUsersToken,
        notification: {
          title: req_body?.title,
          body: req_body?.body,
          image: req_body?.image
            ? `${Slaylewks_details?.s3base_url}${process.env.NODE_ENV != PRODUCTION ? 'dev/' : 'prod/'}${
                req_body?.image
              }`
            : `${Slaylewks_details?.logo}`,
          data: req_body?.data,
          // image: `${Slaylewks_details?.logo}`,
        },
      };
      const sendNotification = await this.sendNotification(requestObj);
      requestObj['user_id'] = usersId;
      requestObj['description'] = req_body?.body || null;
      requestObj['image'] = req_body?.image || null;
      requestObj['title'] = requestObj?.notification?.title || null;
      await UserNotification.create(requestObj);
    } catch (error) {
      logger.error('Send_Notify_Error_' + JSON.stringify(error));
    }
  }
}
