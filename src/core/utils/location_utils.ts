class GeoUtils {
    private earthRadius = 6371; // Earth's radius in kilometers
  
    private toRadians(degrees: number): number {
      return (degrees * Math.PI) / 180;
    }
   
    public calculateDistance(point1: Point, point2: Point): number {
      const { lat: lat1, lng: lng1 } = point1;
      const { lat: lat2, lng: lng2 } = point2;
  
      const dLat = this.toRadians(lat2 - lat1);
      const dLng = this.toRadians(lng2 - lng1);
  
      const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(this.toRadians(lat1)) * Math.cos(this.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
  
      const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      const distance = this.earthRadius * c;
  
      return distance;
    }
  
    public findNearbyThings(center: Point, things: Point[], radius: number): Point[] {
      const nearbyThings: Point[] = [];
  
      for (const thing of things) {
        const distance = this.calculateDistance(center, thing);
        if (distance <= radius) {
          nearbyThings.push(thing);
        }
      }
  
      return nearbyThings;
    }
  }
  
  // Example usage
  const geoUtils = new GeoUtils();
  
  const center: Point = { lat: 40.7128, lng: -74.006 };
  const things: Point[] = [
    { lat: 40.712, lng: -74.008 },
    { lat: 40.713, lng: -74.007 },
    { lat: 40.711, lng: -74.005 },
  ];
  
  const radius = 1; // in kilometers
  
  const nearbyThings = geoUtils.findNearbyThings(center, things, radius);
  console.log(nearbyThings);
  
  interface Point {
    lat: number;
    lng: number;
  }