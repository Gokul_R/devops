import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosError } from 'axios';
import { catchError } from 'rxjs';
import * as http from 'http';
import * as querystring from 'querystring';
import { TEXT_LOCAL_URL } from '../constants';
import SmsSettings from '../database/models/SmsSettings';
import { logger } from './logger';
const sgMail = require('@sendgrid/mail');
@Injectable()
export class TextLocalSMS {
  constructor(private readonly httpService: HttpService) { }

  async callApi(url: string, body: any) {
    let { apiKey, sender, message, numbers } = body;
    const URL: string = `${TEXT_LOCAL_URL}apikey=${apiKey}&sender=${sender}&numbers=${numbers}&message=${message}`;

    let res = this.httpService.axiosRef
      .get(URL)
      .then((result) => {
        // console.log(`SMS API: ${url} RESULT =`, result);
        logger.info('SmsSended: ' + JSON.stringify(numbers));
        return result;
      })
      .catch(function (err) {
        console.log(`SMS API: ${url} ERROR =`, err.message);
        logger.error('SmsError: ' + JSON.stringify(err || err.message));
        return err;
      });
    // const req: http.ClientRequest = http.request(
    //   url,
    //   options,
    //   (res: http.IncomingMessage) => {
    //     let responseData = '';

    //     res.on('data', (chunk: string) => {
    //       responseData += chunk;
    //     });

    //     res.on('end', () => {
    //       // Handle the response data
    //       console.log(responseData);
    //     });
    //   },
    // );

    // req.on('error', (error: Error) => {
    //   console.error('An error occurred: ${error.message}');
    // });

    // req.write(postData);
    // req.end();
  }

  async sendSMS(toNumbers: string, rawMessage: string) {
    try {
      let smsCredentials: SmsSettings = await SmsSettings.findOne({ attributes: ['textlocal_api'] });
      if (!smsCredentials) throw new Error('sms credentials is empty');
      let url = TEXT_LOCAL_URL;
      let sender = encodeURIComponent('SLYLKS');
      let encoded_message = encodeURIComponent(rawMessage);
      let body = {
        //TODO Move to env
        apiKey: smsCredentials?.textlocal_api,
        //   numbers: toNumbers.join('+917010132357'),
        numbers: toNumbers,
        sender: sender,
        message: encoded_message,
      };
      let result = await this.callApi(url, body);
      return result;
    } catch (error: any) {
      logger.info(`Sent_sms_OTP_error:` + JSON.stringify(error));
      throw Error(error);
    }
  }
  // async sendEmail() {
  //   let SENDGRID_API_KEY = "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";
  //   let SENDER_EMAIL_ADDRESS = "no-reply@slaylewks.com";
  //   sgMail.setApiKey(SENDGRID_API_KEY);
  //   const msg = {
  //     to: 'rmuniasamy392@gmail.com',
  //     from: SENDER_EMAIL_ADDRESS, // Use the email address or domain you verified above
  //     subject: 'Sending with Twilio SendGrid is Fun',
  //     text: 'and easy to do anywhere, even with Node.js',
  //     html: `<P>and easy to do anywhere, even with Node.js</P>`,
  //   };
  //   //ES6
  //   let rsult = await sgMail.send(msg);
  // }
}
