import { Module } from '@nestjs/common';
import { TextLocalSMS } from './textlocal';
import { HttpModule, HttpService } from '@nestjs/axios';
import { PercentageDiscountStrategy } from './discount/percentage-discount-strategy';
import { FlatDiscountStrategy } from './discount/flat-discount-strategy';
import { HttpUtils } from './http-utils';

@Module({
  imports: [HttpModule, HttpService,HttpUtils],
  providers: [TextLocalSMS],
  exports: [
    TextLocalSMS,
    PercentageDiscountStrategy,
    FlatDiscountStrategy,
    HttpUtils,
  ],
})
export class UtilsModule {}
