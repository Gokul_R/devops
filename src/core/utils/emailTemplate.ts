import { CreateHelpAndSupportDto } from 'src/admin/help-and-support/dto/create-help-and-support.dto';
import { CouponDiscountType } from 'src/admin/coupon/dto/create-coupon.dto';
import { Provided_By, slaylewks_commission, Slaylewks_details } from '../constants';
import Appointment from '../database/models/Appointment';
import Order from '../database/models/Order';
import { DateUtils } from './dateUtils';
import moment from 'moment-timezone';
import OrderService from '../database/models/OrderServices';
import Helpers from './helpers';
import { String } from 'aws-sdk/clients/cloudsearch';
import Encryption from './encryption';
import { VendorEnquiryDto } from 'src/admin/vendor/dto/enquiry-vendor.dto';
import UserAddress from '../database/models/UserAddress';
import User from '../database/models/User';
import { Branch } from 'aws-sdk/clients/sagemaker';
import { FreelancerBookingSlots, VendorType } from './enum';

export default class EmailTemService {
  static logo() {
    return `<div class="text-center" style=" text-align: center;">
    <img src="${Slaylewks_details.logo}" alt="" style=" width: 40%;margin-top: 15px;" alt="SLAYLEWKS">
</div>`;
  }
  static reminderTempVendor(appointments: Appointment[], order_detail: Order): string {
    let {
      original_price,
      final_price,
      order_id,
      customer_mobile,
      customer_name,
      customer_email,
      service_type,
      payment_id,
      payment_method,
      appointment_start_date_time,
      branch,
      address,
      createdAt,
      booking_slot
    } = order_detail;
    let { branch_name, branch_contact_no, location } = branch || {};
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YYYY');
    let timeOnly = local_date_time.format('hh:mm A');
    let services: string = '';
    let serviceList = appointments.map((appts, i) => {
      let { service_name } = appts.service || {};
      return (services = services.concat(service_name + ','));
    });
    let paid_on = moment(createdAt).tz('Asia/Kolkata').format('DD/MM/YYYY');

    // let html = `<!DOCTYPE html>
    // <html lang="en">
    // <head>
    //     <meta charset="UTF-8">
    //     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //     <title>Confirmed</title>
    //     <link rel="preconnect" href="https://fonts.googleapis.com">
    //     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    //     <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
    //     <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    // </head>
    // <body>
    //     <style>
    //         body,div,p,h1,h2,h3,h4,span{
    //             font-family: 'Inter', sans-serif;
    //         }
    //         p,h1,h2,h3,h4,span{
    //             margin: 0;
    //         }
    //         ul{
    //             padding-left: 0;
    //         }
    //         .mt-1{
    //             margin-top: 20px;
    //         }
    //         .pt-1{
    //             padding-top: 1rem;
    //         }
    //         .mt-2{
    //             margin-top: 2rem;
    //         }
    //         .mt-3{
    //             margin-top: 3rem;
    //         }
    //         .mt-5{
    //             margin-top: 5rem;
    //         }
    //         .fw-600{
    //             font-weight: 600;
    //         }
    //         .fw-700{
    //             font-weight: 700;
    //         }
    //         .container {
    //           max-width: 700px;
    //           background-color: white;
    //           margin: 0 auto;
    //         }
    //         .content{
    //             padding: 3rem;
    //         }
    //         .content img{
    //             height: 100px;
    //         }
    //         .Confirmed{
    //             font-size: 20px;
    //             font-weight: 600;
    //             letter-spacing: 1px;
    //         }
    //         .text-center{
    //             text-align: center;
    //         }
    //         .mx-auto{
    //             margin-left: auto;
    //             margin-right: auto;
    //         }
    //         .content-text{
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         .order-detail ul li{
    //             list-style: none;
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         a{
    //             text-decoration: none;
    //             color: black;
    //         }
    //         .invoice-pdf{
    //             font-size: 32px;
    //         }
    //         .invoice-pdf img{
    //             height: 20px;
    //         }
    //         .fa-circle-down{
    //             font-size: 18px;
    //         }
    //         .policy a{
    //             color: #ECB390;
    //         }
    //     </style>
    //     <div class="container">
    //         <div class="content">
    //         <div class="logo-img" style="text-align: center;"><img
    //         src="${Slaylewks_details.logo}" alt=""
    //         style=" width: 40%;margin-top: 15px;"></div>
    //             <p class="Confirmed text-center pt-1">Appoinment reminder!</p>
    //             <div>
    //                 <h3 class="Confirmed mt-3">Hey Stylist,</h3>
    //                 <h3 class="Confirmed mt-3">A quick reminder on your appointment with
    //                     ${customer_name}</h3>
    //             </div>
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Order Details</h3>
    //                 <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <li>Order id : ${order_id}</li>
    //                     <li>Appointment Date: ${dateOnly}</li>
    //                     <li>Appointment Time: ${timeOnly}</li>
    //                     <li>Services: ${services}</li>
    //                     <li>Services type: ${service_type}</li>
    //                 </ul>
    //             </div>
    //            ${this.customerDetails(customer_name, customer_email, customer_mobile, address)}
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Payment details</h3>
    //                 <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <li>Total amount : ${final_price}</li>
    //                     <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
    //                     <li>Method: ${payment_method}</li>
    //                     <li>Paid On: ${paid_on}</li>
    //                 </ul>
    //             </div>
    //         ${this.footer()}
    //         </div>
    //     </div>
    // </body>
    // </html>`;
    let html1 = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Confirmed</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body style=" font-family: 'Inter', sans-serif;">
    <!-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- haed -->
        <div class="container" style=" max-width: 700px; background-color: white; margin: 0 auto;">
            <div class="content" style=" padding: 1rem;">
${this.logo()}
                <p class="Confirmed text-center pt-1" style=" text-align: center;padding-top: 1rem;font-size: 20px;font-weight: 600;letter-spacing: 1px;">Appoinment reminder!</p>
    <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ -->
    <!-- body -->
                <div>
                    <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Hey Stylist,</h3>
                    <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">A quick reminder on your appointment with
                        ${customer_name}</h3>
                </div>
                <div class="mt-3 order-detail" style=" margin-top: 3rem;">
                    <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Order Details</h3>
                    <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                        <li>Order id : ${order_id}</li>
                        <li>Appointment Date: ${dateOnly}</li>
                        <li>Appointment Time: ${FreelancerBookingSlots[booking_slot] || timeOnly}</li>
                        <li>Services: ${services}</li>
                        <li>Services type: ${service_type}</li>
                    </ul>
                </div>
                ${this.customerDetails(customer_name, customer_email, customer_mobile, address)}
                <div class="mt-3 order-detail" style=" margin-top: 3rem;">
                    <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Payment details</h3>
                    <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                    <li>Total amount : ${final_price}</li>
                    <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
                    <li>Method: ${payment_method}</li>
                    <li>Paid On: ${paid_on}</li>
                    </ul>
                </div>
    <!-- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- footer -->
    ${this.footer()}
            </div>
        </div>
    </body>
    </html>`;
    return html1;
  }
  static reminderTempUser(appointments: Appointment[], order_detail: Order): string {
    let {
      original_price,
      final_price,
      order_id,
      customer_mobile,
      customer_name,
      service_type,
      payment_id,
      payment_method,
      appointment_start_date_time,
      branch,
      createdAt,
      booking_slot
    } = order_detail;
    let { branch_name, branch_contact_no, location } = branch || {};
    let { address, city, area, state, pincode } = location || {};
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YY');
    let timeOnly = local_date_time.format('hh:mm a');
    let services: string = '';
    let serviceList = appointments.map((appts, i) => {
      let { service_name } = appts.service || {};
      return (services = services.concat(service_name + ','));
    });
    let paid_on = moment(createdAt).tz('Asia/Kolkata').format('DD-MMM-YYYY');

    // let html = `<!DOCTYPE html>
    // <html lang="en">
    // <head>
    //     <meta charset="UTF-8">
    //     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //     <title>Cancellation</title>
    //     <link rel="preconnect" href="https://fonts.googleapis.com">
    //     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    //     <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
    //     <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    // </head>
    // <body>
    //     <style>
    //         body,div,p,h1,h2,h3,h4,span{
    //             font-family: 'Inter', sans-serif;
    //         }
    //         p,h1,h2,h3,h4,span{
    //             margin: 0;
    //         }
    //         ul{
    //             padding-left: 0;
    //         }
    //         .mt-1{
    //             margin-top: 20px;
    //         }
    //         .pt-1{
    //             padding-top: 1rem;
    //         }
    //         .mt-2{
    //             margin-top: 2rem;
    //         }
    //         .mt-3{
    //             margin-top: 3rem;
    //         }
    //         .mt-5{
    //             margin-top: 5rem;
    //         }
    //         .fw-600{
    //             font-weight: 600;
    //         }
    //         .fw-700{
    //             font-weight: 700;
    //         }
    //         .container {
    //           max-width: 700px;
    //           background-color: white;
    //           margin: 0 auto;
    //         }
    //         .content{
    //             padding: 3rem;
    //         }
    //         .content img{
    //             height: 100px;
    //         }
    //         .Confirmed{
    //             font-size: 20px;
    //             font-weight: 700;
    //             letter-spacing: 1px;
    //         }
    //         .Reminder{
    //             font-size: 18px;
    //             font-weight: 700;
    //             letter-spacing: 1px;
    //         }
    //         .text-center{
    //             text-align: center;
    //         }
    //         .mx-auto{
    //             margin-left: auto;
    //             margin-right: auto;
    //         }
    //         .content-text{
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         .order-detail ul li{
    //             list-style: none;
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         a{
    //             text-decoration: none;
    //             color: black;
    //         }
    //         .invoice-pdf{
    //             font-size: 32px;
    //         }
    //         .invoice-pdf img{
    //             height: 20px;
    //         }
    //         .fa-circle-down{
    //             font-size: 18px;
    //         }
    //         .policy a{
    //             color: #ECB390;
    //         }
    //         .ps-5{
    //             padding-left: 5rem;
    //         }
    //         .appointment-details{
    //             display: flex;
    //         }
    //     </style>
    //     <div class="container">
    //         <div class="content">
    //         <div class="logo-img" style="text-align: center;"><img
    //         src="${Slaylewks_details.logo}" alt=""
    //         style=" width: 40%;margin-top: 15px;"></div>
    //             <p class="Reminder text-center pt-1">Your Upcoming Salon Appointment Reminder - Don't Miss Out!</p>
    //             <div>
    //                 <h3 class="Confirmed mt-3">Hi ${customer_name},</h3>
    //                 <p class="mt-1 content-text">We hope this message finds you well and that you're looking
    //                     forward to your upcoming salon appointment with us at ${branch_name}, we value your trust in our services and can't wait
    //                     to provide you with an exceptional experience.</p>
    //             </div>
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Appointment Details:</h3>
    //                 <ul class="appointment-details" style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <div>
    //                     <li>Date: ${dateOnly}</li>
    //                     <li>Time: ${timeOnly}</li>
    //                     <li>Salon name: ${branch_name}</li>
    //                     <li>Paid Amount: ${final_price}</li>
    //                     <li>Service: ${services}</li>
    //                     </div>   
    //                 </ul>
    //             </div>
    //             <div>
    //                 <p class="mt-2 content-text">We recommend arriving 10-15 minutes before your appointment to
    //                     ensure a smooth check-in process. If you need to reschedule or
    //                     have any questions, please contact us at ${Slaylewks_details.email} or
    //                     reply to this email</p>
    //                     <p class="mt-2 content-text">To reschedule or have any questions, please contact us at
    //                         <a href="${Slaylewks_details.email}">${Slaylewks_details.email}</a> or reply to this email. 
    //                         <span class="fw-700">If you need rescheduling,
    //                             it can be done via slaylewks app</span></p>
    //             </div>
    //             ${this.footer()}
    //         </div>
    //     </div>
    // </body>
    // </html>`;
    let html1 = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cancellation</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body style=" font-family: 'Inter', sans-serif;">
    <!-- ---------------------------------------------------------------------------------------------------------------------------------------------------------     -->
    <!-- head -->
        <div class="container" style=" max-width: 700px; background-color: white; margin: 0 auto;">
            <div class="content" style=" padding: 1rem;">
${this.logo()}
                <p class="Reminder text-center pt-1" style="font-size: 18px;font-weight: 700; letter-spacing: 1px;padding-top: 1rem; text-align: center;">Your Upcoming Salon Appointment Reminder - Don't Miss Out!</p>
    <!-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------             -->
    <!-- body -->
                <div>
                    <h3 class="Confirmed mt-3" style=" font-size: 20px; font-weight: 700;letter-spacing: 1px; margin-top: 3rem;">Hi ${customer_name},</h3>
                    <p class="mt-1 content-text" style=" font-size: 16px;line-height: 1.5; letter-spacing: 1px;margin-top: 20px;">We hope this message finds you well and that you're looking
                        forward to your upcoming salon appointment with us at ${branch_name}, we value your trust in our services and can't wait
                        to provide you with an exceptional experience.</p>
                </div>
                <div class="mt-3 order-detail" style="margin-top: 3rem;">
                    <h3 class="Confirmed mt-3" style=" font-size: 20px; font-weight: 700;letter-spacing: 1px; margin-top: 3rem;">Appointment Details:</h3>
                    
                        <table style="width: 100%;">
                        <tr>
                            <td style="font-weight: 300;white-space:nowrap;width: 20%;">Date</td>
                            <td style="font-weight: 300;white-space:nowrap;width: 3%;">:</td>
                            <td style="font-weight: 300;">${dateOnly}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: 300;white-space:nowrap">Time</td>
                            <td style="font-weight: 300;white-space:nowrap">:</td>
                            <td style="font-weight: 300;">${FreelancerBookingSlots[booking_slot] || timeOnly}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: 300;white-space:nowrap">Salon name</td>
                            <td style="font-weight: 300;white-space:nowrap">:</td>
                            <td style="font-weight: 300;">${branch_name}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: 300;white-space:nowrap">Paid Amount</td>
                            <td style="font-weight: 300;white-space:nowrap">:</td>
                            <td style="font-weight: 300;">${final_price}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: 300;white-space:nowrap">Service</td>
                            <td style="font-weight: 300;white-space:nowrap">:</td>
                            <td style="font-weight: 300;">${serviceList}</td>
                        </tr>
                    </table>
                        
                   
                </div>
                <div>
                    <p class="mt-2 content-text" style=" margin-top: 2rem;font-size: 16px;line-height: 1.5; letter-spacing: 1px;">We recommend arriving 10-15 minutes before your appointment to
                        ensure a smooth check-in process. If you need to reschedule or
                        have any questions, please contact us at ${Slaylewks_details.email} or
                        reply to this email.</p>
                        <p class="mt-2 content-text" style=" margin-top: 2rem;font-size: 16px;line-height: 1.5; letter-spacing: 1px;">To reschedule or have any questions, please contact us at
                            <a href="${Slaylewks_details.email}">${Slaylewks_details.email}</a> or reply to this email. 
                            <span class="fw-700" style=" font-weight: 700;">If you need rescheduling,
                                it can be done via slaylewks app.</span></p>
                </div>
    <!-- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------             -->
    <!-- footer -->
${this.footer()}
            </div>
        </div>
    </body>
    </html>`;

    return html1;
  }
  static emailOtpVerification(otp: any) {
    let html = `<body>
      <div style="padding: 3rem">
      <div class="logo-img" style="text-align: center;"><img
      src="${Slaylewks_details.logo}" alt=""
      style=" width: 40%;margin-top: 15px;"></div>
        <div
          style="
            background-color: lightgray;
            border-radius: 2rem;
            padding: 23px;
            margin-top: 10px;
          "
        >
          <h1> slaylewks Email verification</h1>
          <p>Enter the below OTP  to verify your account</p>
          <div style="text-align: center">
            <h1
              style="
                padding-top: 10px;
                padding-bottom: 10px;
                padding-right: 15px;
                padding-left: 15px;
                color: #e01b22;
                font-size: 40px;
                text-align: center;
              "
            >
           ${otp}
            </h1>
          </div>
          <p>
            If you didn't request this, please
            <a href="" style="color: black"> contact us</a>
            for more information.
          </p>
    
          <div style="margin-top: 1rem">
            <p>thank you</p>
            <p>Team SLAYLEWKS</p>
          </div>
        </div>
      </div>
      <div style="text-align: center; margin-top: 1rem">
        <div
          style="
            display: flex;
            flex-direction: row;
            justify-content: center;
            gap: 30px;
          "
        >    
        
        </div>
        <p>Sent with love from SLAYLEWKS</p>
      </div>
    
    </body>`;
    return html;
  }
  static emailPasswordVerification(password: any) {
    let html = `<body>
      <div style="padding: 3rem">
      <div class="logo-img" style="text-align: center;"><img
      src="${Slaylewks_details.logo}" alt=""
      style=" width: 40%;margin-top: 15px;"></div>
        <div
          style="
            background-color: lightgray;
            border-radius: 2rem;
            padding: 23px;
            margin-top: 10px;
          "
        >
          <h1> slaylewks Password verification</h1>
          <p>Enter the below Password  to login  your account</p>
          <div style="text-align: center">
            <h1
              style="
                padding-top: 10px;
                padding-bottom: 10px;
                padding-right: 15px;
                padding-left: 15px;
                color: #e01b22;
                font-size: 40px;
                text-align: center;
              "
            >
           ${password}
            </h1>
          </div>
          <p>
            If you didn't request this, please
            <a href="" style="color: black"> contact us</a>
            for more information.
          </p>
    
          <div style="margin-top: 1rem">
            <p>thank you</p>
            <p>Team SLAYLEWKS</p>
          </div>
        </div>
      </div>
      <div style="text-align: center; margin-top: 1rem">
        <div
          style="
            display: flex;
            flex-direction: row;
            justify-content: center;
            gap: 30px;
          "
        >
        </div>
        <p>Sent with love from SLAYLEWKS</p>
      </div>
    
    </body>`;
    return html;
  }

  static forgotPassword(token: any) {
    let html = `<body>
    <div style="padding: 3rem">
    <div class="logo-img" style="text-align: center;"><img
    src="${Slaylewks_details.logo}" alt=""
    style=" width: 40%;margin-top: 15px;"></div>
      <div
        style="
          background-color: lightgray;
          border-radius: 2rem;
          padding: 23px;
          margin-top: 10px;
        "
      >
        <h1> slaylewks forgot Password verification</h1>
        <p>Your reset password link <a href="${token}">click here.</a></p>
        <p>
          If you didn't request this, please
          <a href="" style="color: black"> contact us</a>
          for more information.
        </p>
  
        <div style="margin-top: 1rem">
          <p>Thank you.</p>
          <p>Team SLAYLEWKS.</p>
        </div>
      </div>
    </div>
    <div style="text-align: center; margin-top: 1rem">
      <div
        style="
          display: flex;
          flex-direction: row;
          justify-content: center;
          gap: 30px;
        "
      >
      </div>
      <p>Sent with love from SLAYLEWKS.</p>
    </div>
  
  </body>`;
    return html;
  }
  static orderInovoice(appoinments: Appointment[], order_detail: Order) {
    let {
      original_price,
      final_price,
      gst,
      order_id,
      customer_mobile,
      customer_name,
      coupon_discount,
      appointment_start_date_time,
      branch,
      address,
      booking_slot
    } = order_detail[0];
    let sub_total: number = 0;
    let gst_number = Encryption.decryptValue(branch.gst_number);
    let serviceList = '';
    appoinments.map((appts, i) => {
      let service_discount: number;
      if (appts?.service?.flash_sales.length == 0) {
        service_discount = appts.service.amount * (appts.service.discount / 100);
      } else {
        if (appts?.service?.flash_sales && appts?.service?.flash_sales.length > 0) {
          service_discount =
            appts.service.flash_sales[0]?.discount_type === 'percentage'
              ? appts.service.amount * (appts.service.flash_sales[0]?.discount_value / 100)
              : appts.service.flash_sales[0]?.discount_value;
        } else {
          service_discount = appts.service.amount * (appts.service.discount / 100);
        }
      }

      let discounted_price = appts.service.amount - service_discount;
      sub_total = sub_total + discounted_price;
      serviceList += `<tr>
      <td>${i + 1}</td>
      <td style="word-break: break-all; overflow-wrap: break-word;">${appts.service.service_name}</td>
      <td>₹${discounted_price}</td>
       </tr>`;
    });
    let tax: number = +(branch.tax_percentage / 2).toFixed(1);
    let tax_amount: number = gst ? gst.toFixed(2) : 0;
    let grand_total: number = final_price.toFixed(2);
    let payment = order_detail[0].vendor_type == VendorType.FREELANCER ?`Initial payment(50%): ₹ ${grand_total}`:`Grand Total: ₹ ${grand_total}`;
    let html = `<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap"
          rel="stylesheet"
        />
      </head>
      <body>
        <div class="invoice-email">
          <style>
            body {
              font-family: 'Inter', sans-serif;
            }
    
            .container {
              max-width: 939px;
              background-color: white;
              border: 1px solid #e1dddd;
              margin: 0 auto;
            }
            .inner-container {
              padding: 0px 50px;
            }
            .main-header {
              width: 100%;
            }
            .privacy {
              display: flex;
              justify-content: center;
            }
    
            .invoice-details-header {
              text-align: start;
            }
            /* .company-details-header{
            float: left;
            width: 40%;
          } */
            .company-details-header {
              text-align: start;
              position: relative;
              bottom: 0px;
            }
            .company-details-header img {
              max-width: 250px;
              background-color: white;
            }
            .invoice-details-header h1 {
              color: #000000;
              font-size: 64px;
              margin: 0;
              font-weight: 700;
            }
            .invoice-details-header h6 {
              font-size: 24px;
              color: #000000;
              margin: 18px 12px;
              font-weight: 700;
            }
            .invoice-details-header h5 {
              color: rgba(0, 0, 0, 0.5);
              font-size: 24px;
              margin: 0px 12px;
              font-weight: 700;
            }
    
            .company-details-header h6 {
              font-size: 24px;
              color: #000000;
              margin: 12px;
              font-weight: 700;
            }
            .company-details-header h5 {
              color: rgba(0, 0, 0, 0.5);
              font-size: 24px;
              margin: 0px 15px;
              font-weight: 700;
            }
            .items-container {
              margin-top: 65px;
              margin-bottom: 70px;
              padding: 0px 0px 20px 0px;
            }
            .items-container tr {
              background-color: #d9d9d9;
            }
    
            .total-amount h4 {
              color: rgba(0, 0, 0, 0.5) !important;
              font-size: 20px;
              font-weight: 400;
            }
    
            .items-container tr th {
              text-align: start;
              height: 20px;
              padding: 10px;
              color: #000000;
              font-size: 24px;
              font-weight: 700;
            }
    
            .items-container tr td {
              text-align: start;
              height: auto;
              padding: 10px;
              font-size: 18px;
              color: #000000;
              background-color: #fbfafa;
            }
    
            .total-amount {
              text-align: end;
              padding: 0px 111px 0px 0px;
              display: flex;
              flex-direction: column;
              justify-content: space-around;
              height: 24vh;
              margin-top: 32px;
            }
            .total-amount p {
              color: rgba(0, 0, 0, 0.38);
              font-size: 20px;
            }
            .total-amount h6 {
              color: #000000;
              font-size: 24px;
            }
            .footer {
              padding: 0 20px;
              font-size: 16px;
              margin-bottom: 20px;
              font-weight: 400;
            }
    
            hr {
              margin: 5rem 23px 0px 23px;
            }
    
            .address-details {
              display: flex;
              justify-content: space-between;
              width: auto;
            }
    
            @media only screen and (max-width: 600px) {
              .inner-container {
                padding: 0px 5px;
              }
              .invoice-details-header h1 {
                color: #000000;
                font-size: 20px;
                margin: 0;
              }
              .invoice-details-header h6 {
                font-size: 12px;
                color: #000000;
                margin: 15px 5px;
              }
              .invoice-details-header h5 {
                color: rgba(0, 0, 0, 0.5);
                font-size: 12px;
                margin: 5px 5px;
              }
              .company-details-header img {
                max-width: 150px;
                background-color: white;
                position: relative;
                bottom: 0px;
                margin-top: 25px;
              }
              .company-details-header h6 {
                font-size: 18px;
                color: #000000;
                margin: 15px 5px;
              }
              .company-details-header h5 {
                color: rgba(0, 0, 0, 0.5);
                font-size: 12px;
                margin: 5px 5px;
              }
    
              .total-amount span {
                color: rgba(0, 0, 0, 0.5) !important;
                font-size: 24px;
                font-weight: 400;
              }
    
              .items-container {
                overflow-x: scroll !important;
                padding: 0px 0px 10px 0px;
              }
              .items-container tr th {
                text-align: start;
                background-color: #d9d9d9;
                height: 20px;
                padding: 10px;
                color: #000000;
                font-size: 12px;
              }
              .items-container tr td {
                text-align: start;
                height: auto;
                padding: 5px;
                font-size: 10px;
                color: #000000;
              }
              .total-amount p {
                color: rgba(0, 0, 0, 0.38);
                font-size: 14px;
              }
              .total-amount h6 {
                color: #000000;
                font-size: 18px;
              }
              .address-details {
                display: flex;
                flex-direction: column;
                justify-content: center;
              }
            }
          </style>
          <div class="container">
            <div class="inner-container">
              <div class="address-details" style="overflow: hidden;word-wrap: break-word;">
                <div class="invoice-details-header" style="width: 50%;">
                  <h1>Invoice</h1>
                  <h5>Invoice Dated on : ${DateUtils.formatDate(new Date())}</h5>
                  <h5>Name:${customer_name}</h5>
                  <h5>Contact #: ${customer_mobile}</h5>
                  <h5>Order #: ${order_id}</h5>
                  <h5>
                  Order Date: ${DateUtils.formatDate(appointment_start_date_time)}
                 </h5>
          
                
                  <h6>Billed on behalf of</h6>
                  <h5>${branch.branch_name}</h5> 
                  <h5>TAX No.# ${gst_number ? gst_number : '-'}</h5>
                  <h5>Address: ${branch?.location?.address ? branch.location.address:''}</h5>
                  <h5>Email: ${branch?.branch_email}</h5>
                  <h5>Phone: +91-${branch?.branch_contact_no}</h5>
                </div>
                <div class="company-details-header" style="width: 50%;">
                  <div class="">
                    <img
                      src="${Slaylewks_details.logo}"
                    />
                    <h6>${Slaylewks_details.comName}</h6>
                    <h5>TAX No.# ${Slaylewks_details.taxNo}</h5>
                    <h5>
                    ${Slaylewks_details.address1}
                    </h5>
                    <h5>${Slaylewks_details.address2}</h5>
                    <h5>${Slaylewks_details.email}</h5>
                    <h5>${Slaylewks_details.phone}</h5>
                  </div>
                </div>
              </div>
              <div class="items-container">
                <table style="width: 100%">
                  <tr>
                    <th>#</th>
                    <th>Service Name</th>
                    <!-- <th>Qty</th> -->
                    <th>Service price</th>
                    <!-- <th>Tax</th>
                        <th>Total</th> -->
                  </tr>
                 ${serviceList}
                </table>
                <hr />
                <div class="total-amount">
                  <div style="font-size: 24px; font-weight: 400">
                    <p></p>
                    SubTotal: ₹${sub_total.toFixed(2)}
                  </div>
                  <div style="font-size: 24px; font-weight: 400">
                    Coupon: ₹${coupon_discount.toFixed(2)}
                  </div>
                  <div style="font-size: 24px; font-weight: 400">
                    CGST @${tax}%: ₹${(tax_amount / 2).toFixed(2)}
                  </div>
                  <div style="font-size: 24px; font-weight: 400">
                    SGST @${tax}%: ₹${(tax_amount / 2).toFixed(2)}
                  </div>
                  <h2>
                    ${payment}
                  </h2>
                </div>
              </div>
              <div class="footer">
                <p>
                  Disclaimer: This is an acknowledgment of Delivery of the Order and
                  not an actual invoice. Details mentioned above including the menu
                  prices and taxes (as applicable) are as provided by the Service
                  provider to Slaylewks. Responsibility of charging (or not
                  charging) taxes lies with the Salons and Slaylewks disclaims any
                  liability that may arise in this respect.
                </p>
                ${this.footer()}
              </div>
            </div>
          </div>
        </div>
      </body>
    </html>`;
    return html;
  }

  static vendorRequestTemplete(data: VendorEnquiryDto) {
    let html = `
<p>Name      : ${data?.name}</p>
<p>Email id  : ${data?.email}</p>
<p>Phone Number:${data?.phone_number}</p>
`;
    return html;
  }

  static confirmationTemp(appointments: Appointment[], order_detail: Order) {
    let {
      final_price,
      order_id,
      customer_name,
      payment_id,
      payment_method,
      order_otp,
      appointment_start_date_time,
      branch,
      created_at,
      booking_slot,
      partial_id,
      partial_payment_completed,
      initial_amount
    } = order_detail[0];
    let { branch_name, branch_contact_no, location } = branch || {};
    let { address, city, area, state, pincode } = location || {};
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YY');
    let timeOnly = local_date_time.format('hh:mm a');
    const services = appointments.map((appts) => appts.service?.service_name || '').join(',');
    let paid_on = moment(created_at).tz('Asia/Kolkata').format('DD-MMM-YYYY');
    let otpVerification = order_detail?.vendor_type == VendorType.FREELANCER ? `` : `<li>Verification Otp: ${order_otp}</li>`;
    let amountDetails = (partial_id && partial_payment_completed) ?
    `<li>Advance amount : ${initial_amount || 0}</li>
     <li>Paid amount : ${initial_amount || 0}</li>
      <li>Total amount : ${final_price || 0}</li>` 
      : `<li>Total amount : ${final_price || 0}</li>`

    // let html = 
    // `<!DOCTYPE html>
    // <html lang="en">
    // <head>
    //     <meta charset="UTF-8">
    //     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //     <title>Confirmed</title>
    //     <link rel="preconnect" href="https://fonts.googleapis.com">
    //     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    //     <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
    //     <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    // </head>
    // <body>
    //     <style>
    //         body,div,p,h1,h2,h3,h4,span{
    //             font-family: 'Inter', sans-serif;
    //         }
    //         p,h1,h2,h3,h4,span{
    //             margin: 0;
    //         }
    //         ul{
    //             padding-left: 0;
    //         }
    //         .mt-1{
    //             margin-top: 20px;
    //         }
    //         .pt-1{
    //             padding-top: 1rem;
    //         }
    //         .mt-2{
    //             margin-top: 2rem;
    //         }
    //         .mt-3{
    //             margin-top: 3rem;
    //         }
    //         .mt-5{
    //             margin-top: 5rem;
    //         }
    //         .fw-600{
    //             font-weight: 600;
    //         }
    //         .fw-700{
    //             font-weight: 700;
    //         }
    //         .container {
    //           max-width: 700px;
    //           background-color: white;
    //           margin: 0 auto;
    //         }
    //         .content{
    //             padding: 3rem;
    //         }
    //         .content img{
    //             height: 100px;
    //         }
    //         .Confirmed{
    //             font-size: 20px;
    //             font-weight: 600;
    //             letter-spacing: 1px;
    //         }
    //         .text-center{
    //             text-align: center;
    //         }
    //         .mx-auto{
    //             margin-left: auto;
    //             margin-right: auto;
    //         }
    //         .content-text{
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         .order-detail ul li{
    //             list-style: none;
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         a{
    //             text-decoration: none;
    //             color: black;
    //         }
    //         .invoice-pdf{
    //             font-size: 32px;
    //         }
    //         .invoice-pdf img{
    //             height: 20px;
    //         }
    //         .fa-circle-down{
    //             font-size: 18px;
    //         }
    //         .policy a{
    //             color: #ECB390;
    //         }
    //     </style>
    //     <div class="container">
    //         <div class="content">
    //         <div class="logo-img" style="text-align: center;"><img
    //         src="${Slaylewks_details.logo}" alt=""
    //         style=" width: 40%;margin-top: 15px;"></div>
    //             <p class="Confirmed text-center pt-1">Order Confirmed!</p>
    //             <div>
    //                 <h3 class="Confirmed mt-3">Dear ${customer_name}</h3>
    //                 <p class="mt-1 content-text">We are excited to confirm your upcoming salon appointment with
    //                     us at ${branch_name} on ${local_date_time.format('DD-MMM-YYYY hh:mm a')}.
    //                     </p>
    //             </div>
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Order Details</h3>
    //                 <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <li>Order id :${order_id}</li>
    //                     <li>Appointment Date: ${dateOnly}</li>
    //                     <li>Appointment Time: ${timeOnly}</li>
    //                     <li>Services:${services}</li>
    //                     <li class="fw-600" style="font-weight:600">Verification OTP: ${order_otp}</li>
    //                 </ul>
    //             </div>
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Salon details</h3>
    //                 <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <li>Salon Name: ${branch_name}</li>
    //                     <li>Salon Address: ${address},${area},${city},${state},${pincode}</li>
    //                     <li>Salon contact number: ${branch_contact_no}</li>
    //                 </ul>
    //             </div>
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Payment details</h3>
    //                 <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <li>Total amount : ${final_price}</li>
    //                     <li>Payment Id:${payment_id ? payment_id : 'NA'}</li>
    //                     <li>Method: ${payment_method}</li>
    //                     <li>Paid On: ${paid_on}</li>
    //                 </ul>
    //             </div>
    //             <div class="mt-3">
    //             <p class="content-text mt-2">Please arrive 10-15 minutes before your scheduled appointment time
    //                 to ensure a smooth check-in process. If you need to reschedule or
    //                 cancel your appointment, please contact us at least 24 hours in
    //                 advance to avoid any cancellation fees.</p>
    //                 <p class="content-text mt-2">We can't wait to make you look and feel your best. Thank you for
    //                     choosing ${branch_name} for your beauty needs.</p>
    //                     <p class="content-text mt-2 pt-1">See you soon!</p>
    //             </div>
    //             <div class="mt-5">
    //                 <p class="content-text fw-700">If you need to reschedule or cancel your appointment, please
    //                     contact us at least 24 hours in advance to avoid any cancellation
    //                     fees.</p>
    //             </div>
    //      ${this.footer()}
    //         </div>
    //     </div>
    // </body>
    // </html>`;
    let html = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Confirmed</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body style="font-family: 'Inter', sans-serif;">
        <!-- head -->
        <div class="container" style=" max-width: 700px; background-color: white; margin: 0 auto;">
        <div class="content" style=" padding: 1rem;">
                            ${this.logo()}
                <p class="Confirmed text-center pt-1" style="font-size: 20px; font-weight: 600;letter-spacing: 1px;text-align: center;padding-top: 1rem;">Order Confirmed!</p>
    <!-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------             -->
    <!-- body -->
                <div>
                    <h3 class="Confirmed mt-3"  style="font-size: 20px; font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Dear ${customer_name}</h3>
                    <p class="mt-1 content-text" style=" font-size: 16px;line-height: 1.5;letter-spacing: 1px; margin-top: 20px;">We are excited to confirm your upcoming salon appointment with
                        us at ${branch_name} on ${dateOnly} at ${FreelancerBookingSlots[booking_slot] || timeOnly}.
                        </p>
                </div>
                <div class="mt-3 order-detail" style="margin-top: 3rem;">
                    <h3 class="Confirmed mt-3" style="font-size: 20px; font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Order Details</h3>
                    <ul style="list-style: none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                        <li>Order id : ${order_id}</li>
                        <li>Appointment Date:  ${dateOnly}</li>
                        <li>Appointment Time:  ${FreelancerBookingSlots[booking_slot] || timeOnly}</li>
                        <li>Services: ${services}</li>
                           ${otpVerification}
                    </ul>
                </div>
                <div class="mt-3 order-detail" style=" margin-top: 3rem;">
                    <h3 class="Confirmed mt-3"  style="font-size: 20px; font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Salon details</h3>
                    <ul style="list-style: none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                        <li>Salon Name: ${branch_name || 'NA'}</li>
                        <li>Salon Address: ${address ? address : ''},${area ? area : ''},${city ? city : ''},${state ? state : ''},${pincode ? pincode : ''}</li>
                        <li>Salon contact number: ${branch_contact_no || 'NA'}</li>
                    </ul>
                </div>
                <div class="mt-3 order-detail" style=" margin-top: 3rem;">
                    <h3 class="Confirmed mt-3"  style="font-size: 20px; font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Payment details</h3>
                    <ul style="list-style: none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                           ${amountDetails} 
                        <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
                        <li>Method: ${payment_method || "NA"}</li>
                        <li>Paid On: ${paid_on || 'NA'}</li>
                    </ul>
                </div>
                <div class="mt-3" style=" margin-top: 3rem;">
                <p class="content-text mt-2" style="font-size: 16px;line-height: 1.5;letter-spacing: 1px; margin-top: 2rem;">Please arrive 10-15 minutes before your scheduled appointment time
                    to ensure a smooth check-in process. If you need to reschedule or
                    cancel your appointment, please contact us at least 24 hours in
                    advance to avoid any cancellation fees.</p>
                    <p class="content-text mt-2" style="font-size: 16px;line-height: 1.5;letter-spacing: 1px; margin-top: 2rem;">We can't wait to make you look and feel your best. Thank you for
                        choosing ${branch_name} for your beauty needs.</p>
                        <p class="content-text mt-2 pt-1" style="font-size: 16px;line-height: 1.5;letter-spacing: 1px; margin-top: 2rem;padding-top: 1rem;">See you soon!</p>
                </div>
                <div class="mt-5 fw-700" style=" margin-top: 3rem;  text-decoration: none;color: #00060f; font-weight: 800; font-size: 16px; line-height: 1.5;letter-spacing: 1px; font-weight: 700;">
                       If you need to reschedule or cancel your appointment, please
                        contact us at least 24 hours in advance to avoid any cancellation fees.
                        
                </div>
    <!-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------             -->
                               ${this.footer()}
        </div>
        </div>
    </body>
    </html>`;
    return html;
  }

  static awaitingApprovalUserTemp(appointments: Appointment[], orderDetails: Order) {
    let {
      final_price,
      order_id,
      customer_name,
      payment_id,
      payment_method,
      appointment_start_date_time,
      branch,
      created_at,
      booking_slot,
      due_amount
    } = orderDetails[0];
    let { branch_name, branch_contact_no, location } = branch || {};
    // let { address, city, area, state, pincode } = location || {};
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YY');
    const services = appointments.map((appts) => appts.service?.service_name || '').join(',');
    let paid_on = moment(created_at).tz('Asia/Kolkata').format('DD-MMM-YYYY');
    let html = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Confirmed</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body style="font-family: 'Inter', sans-serif;">
        <!-- head -->   
        <div class="container" style=" max-width: 700px; background-color: white; margin: 0 auto;">
        <div class="content" style=" padding: 1rem;">
                            ${this.logo()}
                <p class="Confirmed text-center pt-1" style="font-size: 20px; font-weight: 600;letter-spacing: 1px;text-align: center;padding-top: 1rem;">Order Awaiting!</p>
    <!-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------             -->
    <!-- body -->
                <div>
                    <h3 class="Confirmed mt-3"  style="font-size: 20px; font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Dear ${customer_name}</h3>
                    <p class="mt-1 content-text" style=" font-size: 16px;line-height: 1.5;letter-spacing: 1px; margin-top: 20px;">your order is in awaiting for approval.
                        </p>
                </div>
                <div class="mt-3 order-detail" style="margin-top: 3rem;">
                    <h3 class="Confirmed mt-3" style="font-size: 20px; font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Order Details</h3>
                    <ul style="list-style: none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                        <li>Order id : ${order_id}</li>
                        <li>Appointment Date:  ${dateOnly}</li>
                        <li>Appointment Time:  ${FreelancerBookingSlots[booking_slot]}</li>
                        <li>Services: ${services}</li>
                    </ul>
                </div>
                <div class="mt-3 order-detail" style=" margin-top: 3rem;">
                    <h3 class="Confirmed mt-3"  style="font-size: 20px; font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Freelancer details</h3>
                    <ul style="list-style: none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                        <li>Freelancer Name: ${branch_name || 'NA'}</li>
                        <li>Freelancer contact number: ${branch_contact_no || 'NA'}</li>
                    </ul>
                </div>
                <div class="mt-3 order-detail" style=" margin-top: 3rem;">
                    <h3 class="Confirmed mt-3"  style="font-size: 20px; font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Payment details</h3>
                    <ul style="list-style: none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                        <li>Total amount : ${final_price || 0}</li>
                        <li>Due amount : ${due_amount || 0}</li>
                        <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
                        <li>Method: ${payment_method || "NA"}</li>
                        <li>Paid On: ${paid_on || 'NA'}</li>
                    </ul>
                </div>
               
    <!-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------             -->
                               ${this.footer()}
        </div>
        </div>
    </body>
    </html>`;
    return html;
  }

  static awaitingApprovalVendorTemp(appointments: Appointment[], order: Order, useraddress: UserAddress) {
    let {
      order_id,
      customer_name,
      customer_email,
      customer_mobile: user_mobile,
      payment_id,
      payment_method,
      appointment_start_date_time,
      final_price,
      service_type,
      created_at,
      branch,
      booking_slot,
      due_amount,

    } = order[0];
    let { branch_name } = branch;
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YYYY');
    let timeOnly = local_date_time.format('hh:mm a');
    let services: string = '';
    let serviceList = appointments.map((appts, i) => {
      let { service_name } = appts.service || {};
      return (services = services.concat(service_name + ','));
    });
    let paid_on = moment(created_at).tz('Asia/Kolkata').format('DD-MMM-YYYY');

    let html = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Confirmed</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="content">
            ${this.logo()}
                <p class="Confirmed text-center pt-1" style="text-align: center; font-weight: bold;">Order Awating!</p>
                <div>
                    <h3 class="Confirmed mt-3">Hi ${branch_name}</h3>
                    <p class="mt-1 content-text">Your customer
                    ${customer_name} order request is in Awating </p>
                </div>
                <div class="mt-3 order-detail order">
                    <h3 class="Confirmed mt-3">Order Details</h3>
                    <ul>
                        <li>Order id :${order_id}</li>
                        <li class="fw-600" style="font-weight: bold;">Appointment Date: ${dateOnly}</li>
                        <li class="fw-600" style="font-weight: bold;">Appointment Time: ${FreelancerBookingSlots[booking_slot]}</li>
                        <li>Services:${services}</li>
                        <li>Services type: ${service_type}</li>
                    </ul>
                </div>
                <div class="mt-3 order-detail">
                    <h3 class="Confirmed mt-3">Customer details</h3>
                    <ul>
                        <li>Name: ${customer_name}</li>
                        <li>Address: ${useraddress.flat_no},${useraddress.area}${useraddress.city}${useraddress.state},${useraddress.zipcode}</li>
                        <li>Email: ${customer_email}</li> 
                         <li>Contact number: ${useraddress.customer_mobile}</li>
                      
                    </ul>
                </div>
                <div class="mt-3 order-detail">
                    <h3 class="Confirmed mt-3">Payment details</h3>
                    <ul>
                        <li>Total amount : ${final_price}</li>
                        <li>due amount : ${due_amount}</li>
                        <li>Payment Id:${payment_id ? payment_id : 'NA'}</li>
                        <li>Method: ${payment_method}</li>
                        <li>Paid On: ${paid_on}</li>
                    </ul>
                </div>
                ${this.footer()}
            </div>
        </div>
    </body>
    </html>`;
    return html;

  }

  static vendorOrderPlaced(appointments: Appointment[], order_detail: Order) {
    let {
      final_price,
      order_id,
      customer_mobile,
      customer_name,
      customer_email,
      service_type,
      payment_id,
      payment_method,
      appointment_start_date_time,
      branch,
      created_at,
      address,
      booking_slot
    } = order_detail[0];
    let { branch_name, branch_contact_no, location } = branch || {};
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YY');
    let timeOnly = local_date_time.format('hh:mm a');
    let services: string = '';
    let serviceList = appointments.map((appts, i) => {
      let { service_name } = appts.service || {};
      appointments.length > 1 ? service_name += ',' : '';
      return (services = services.concat(service_name));
    });
    let paid_on = moment(created_at).tz('Asia/Kolkata').format('DD-MMM-YYYY');

    // let html = `<!DOCTYPE html>
    // <html lang="en">
    // <head>
    //     <meta charset="UTF-8">
    //     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //     <title>Confirmed</title>
    //     <link rel="preconnect" href="https://fonts.googleapis.com">
    //     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    //     <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
    //     <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    // </head>
    // <body>
    //     <style>
    //         body,div,p,h1,h2,h3,h4,span{
    //             font-family: 'Inter', sans-serif;
    //         }
    //         p,h1,h2,h3,h4,span{
    //             margin: 0;
    //         }
    //         ul{
    //             padding-left: 0;
    //         }
    //         .mt-1{
    //             margin-top: 20px;
    //         }
    //         .pt-1{
    //             padding-top: 1rem;
    //         }
    //         .mt-2{
    //             margin-top: 2rem;
    //         }
    //         .mt-3{
    //             margin-top: 3rem;
    //         }
    //         .mt-5{
    //             margin-top: 5rem;
    //         }
    //         .fw-600{
    //             font-weight: 600;
    //         }
    //         .fw-700{
    //             font-weight: 700;
    //         }
    //         .container {
    //           max-width: 700px;
    //           background-color: white;
    //           margin: 0 auto;
    //         }
    //         .content{
    //             padding: 3rem;
    //         }
    //         .content img{
    //             height: 100px;
    //         }
    //         .Confirmed{
    //             font-size: 20px;
    //             font-weight: 600;
    //             letter-spacing: 1px;
    //         }
    //         .text-center{
    //             text-align: center;
    //         }
    //         .mx-auto{
    //             margin-left: auto;
    //             margin-right: auto;
    //         }
    //         .content-text{
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         .order-detail ul li{
    //             list-style: none;
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         a{
    //             text-decoration: none;
    //             color: black;
    //         }
    //         .invoice-pdf{
    //             font-size: 32px;
    //         }
    //         .invoice-pdf img{
    //             height: 20px;
    //         }
    //         .fa-circle-down{
    //             font-size: 18px;
    //         }
    //         .policy a{
    //             color: #ECB390;
    //         }
    //     </style>
    //     <div class="container">
    //         <div class="content">
    //         <div class="logo-img" style="text-align: center;"><img
    //         src="${Slaylewks_details.logo}" alt=""
    //         style=" width: 40%;margin-top: 15px;"></div>
    //             <p class="Confirmed text-center pt-1">New Order Confirmed</p>
    //             <div>
    //                 <h3 class="Confirmed mt-3">Hi ${branch_name}</h3>
    //                 <p class="mt-1 content-text">We got a super customer closer to you. Our customer
    //                     <span class="fw-700">${customer_name}</span> has booked an appointment on <span class="fw-700">${local_date_time.format(
    //   'DD-MMM-YYYY hh:mm a',
    // )}.</span></p>
    //             </div>
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Order Details</h3>
    //                 <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <li>Order id : ${order_id}</li>
    //                     <li>Appointment Date: ${dateOnly}</li>
    //                     <li>Appointment Time: ${timeOnly}</li>
    //                     <li>Services:${services}</li>
    //                     <li>Services type: ${service_type}</li>
    //                 </ul>
    //             </div>
    //         ${this.customerDetails(customer_name, customer_email, address, customer_mobile)}
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Payment details</h3>
    //                 <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <li>Total amount :${final_price}</li>
    //                     <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
    //                     <li>Method: ${payment_method}</li>
    //                     <li>Paid On:${paid_on}</li>
    //                 </ul>
    //             </div>
    //            ${this.footer()}
    //         </div>
    //     </div>
    // </body>
    // </html>`;
    let html = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Confirmed</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body style=" font-family: 'Inter', sans-serif;">
    <!-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------     -->
    <!-- head -->
        <div class="container" style=" max-width: 700px; background-color: white; margin: 0 auto;">
            <div class="content" style=" padding: 1rem;">
                ${this.logo()}
                <p class="Confirmed text-center pt-1" style=" text-align: center;padding-top: 1rem;font-size: 20px;font-weight: 600;letter-spacing: 1px;padding-top: 1rem;">New Order Confirmed</p>
    <!-- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------             -->
    <!-- body -->
                <div>
                    <h3 class="Confirmed mt-3" style="padding-top: 1rem;font-size: 20px;font-weight: 600;letter-spacing: 1px;">Hi ${branch_name}</h3>
                    <p class="mt-1 content-text" style="font-size: 16px; line-height: 1.5;letter-spacing: 1px;margin-top: 20px;">We got a super customer closer to you. Our customer
                        <span class="fw-700" style="font-weight: 700;">${customer_name}</span> has booked an appointment on <span class="fw-700" style="font-weight: 700;">${dateOnly}  
                        ${FreelancerBookingSlots[booking_slot] || timeOnly}.</span></p>
                </div>
                <div class="mt-3 order-detail" style=" margin-top: 3rem;">
                    <h3 class="Confirmed mt-3" style="padding-top: 1rem;font-size: 20px;font-weight: 600;letter-spacing: 1px;">Order Details</h3>
                    <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                        <li>Order id : ${order_id}</li>
                        <li>Appointment Date: ${dateOnly}</li>
                        <li>Appointment Time: ${FreelancerBookingSlots[booking_slot] || timeOnly}</li>
                        <li>Services: ${services}</li>
                        <li>Services type: ${service_type}</li>
                    </ul>
                </div>
                ${this.customerDetails(customer_name, customer_email, customer_mobile, address)}

                <div class="mt-3 order-detail" style=" margin-top: 3rem;">
                    <h3 class="Confirmed mt-3" style="padding-top: 1rem;font-size: 20px;font-weight: 600;letter-spacing: 1px;">Payment details</h3>
                    <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                        <li>Total amount : ${final_price}</li>
                        <li>Payment Id: ${payment_id}</li>
                        <li>Method: ${payment_method}</li>
                        <li>Paid On: ${paid_on}</li>
                    </ul>
                </div>
    <!-- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <!-- footer -->
                                 ${this.footer()}
            </div>
        </div>
    </body>
    </html>`;
    return html;
  }

  static cancelationTempUser(mailData: any) {
    let {
      customer_name,
      customer_mobile,
      customer_email,
      order_id,
      appointment_start_date_time,
      payment_method,
      service_type,
      branch,
      payment_id,
      final_price,
      createdAt,
      cancellation_reason,
      comment,
      booking_slot
    } = mailData?.order;
    let { area, city, address, pincode, state } = mailData.location || {};
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let services: string = '';
    let serviceList = mailData.appointments.map((appts, i) => {
      let { service_name } = appts.service || {};
      return (services = services.concat(service_name + ','));
    });
    let dateOnly = local_date_time.format('DD-MMM-YYYY');
    let timeOnly = local_date_time.format('hh:mm A');
    //     let html = `<!DOCTYPE html>
    //  <html lang="en">
    //  <head>
    //      <meta charset="UTF-8">
    //      <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //      <title>Cancellation</title>
    //      <link rel="preconnect" href="https://fonts.googleapis.com">
    //      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    //      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
    //      <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    //  </head>
    //  <body>
    //      <style>
    //          body,div,p,h1,h2,h3,h4,span{
    //              font-family: 'Inter', sans-serif;
    //          }
    //          p,h1,h2,h3,h4,span{
    //              margin: 0;
    //          }
    //          ul{
    //              padding-left: 0;
    //          }
    //          .mt-1{
    //              margin-top: 20px;
    //          }
    //          .pt-1{
    //              padding-top: 1rem;
    //          }
    //          .mt-2{
    //              margin-top: 2rem;
    //          }
    //          .mt-3{
    //              margin-top: 3rem;
    //          }
    //          .mt-5{
    //              margin-top: 5rem;
    //          }
    //          .fw-600{
    //              font-weight: 600;
    //          }
    //          .fw-700{
    //              font-weight: 700;
    //          }
    //          .container {
    //            max-width: 700px;
    //            background-color: white;
    //            margin: 0 auto;
    //          }
    //          .content{
    //              padding: 3rem;
    //          }
    //          .content img{
    //              height: 100px;
    //          }
    //          .Confirmed{
    //              font-size: 20px;
    //              font-weight: 600;
    //              letter-spacing: 1px;
    //          }
    //          .text-center{
    //              text-align: center;
    //          }
    //          .mx-auto{
    //              margin-left: auto;
    //              margin-right: auto;
    //          }
    //          .content-text{
    //              font-size: 16px;
    //              line-height: 1.5;
    //              letter-spacing: 1px;
    //          }
    //          .order-detail ul li{
    //              list-style: none;
    //              font-size: 16px;
    //              line-height: 1.5;
    //              letter-spacing: 1px;
    //          }
    //          a{
    //              text-decoration: none;
    //              color: black;
    //          }
    //          .invoice-pdf{
    //              font-size: 32px;
    //          }
    //          .invoice-pdf img{
    //              height: 20px;
    //          }
    //          .fa-circle-down{
    //              font-size: 18px;
    //          }
    //          .policy a{
    //              color: #ECB390;
    //          }
    //          @media (max-width: 576px) {
    //           .custom-width{
    //             width:100% !important;
    //           }
    //          }
    //      </style>
    //      <div class="container">
    //          <div class="content">
    //          <div class="logo-img" style="text-align: center;"><img
    //          src="${Slaylewks_details.logo}" alt=""
    //          style=" width: 40%;margin-top: 15px;"></div>
    //              <p class="Confirmed text-center pt-1">Order cancelled</p>
    //              <div>
    //                  <h3 class="Confirmed mt-3">Hi ${customer_name}</h3>
    //                  <p class="mt-1 content-text">Your order has been cancelled.<br>
    //                      I hope this email finds you well. We regret to inform you that your
    //                      recent order, placed on ${dateOnly}, with the order number
    //                      ${order_id} has been cancelled.
    //                      </p>
    //              </div>
    //              <div class="mt-3 order-detail">
    //                  <h3 class="Confirmed mt-3">Order Details</h3>
    //                  <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                      <li>Order id : ${order_id}</li>
    //                      <li>Appointment Date: ${dateOnly}</li>
    //                      <li>Appointment Time:${timeOnly}</li>
    //                      <li>Services: ${services}</li>
    //                      <li>Services type: ${service_type}</li>
    //                  </ul>
    //              </div>
    //             ${this.customerDetails(customer_name, customer_email, customer_mobile, mailData.address)}
    //              <div class="mt-3 order-detail">
    //                  <h3 class="Confirmed mt-3">Payment details</h3>
    //                  <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                      <li>Total amount : ${final_price}</li>
    //                      <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
    //                      <li>Method: ${payment_method}</li>
    //                      <li>Paid On: ${moment(createdAt).tz('Asia/Kolkata').format('DD-MMM-YYYY')}</li>
    //                  </ul>
    //              </div>
    //              <div class="mt-3">
    //              <div>
    //                  <h3 class="Confirmed mt-3">Reason for cancellation</h3>
    //                  <p class="mt-1 content-text">${cancellation_reason}</p>
    //                      <p class="mt-3 content-text">Check our website to know about the <span class="fw-700"><a href=${Slaylewks_details.cancelation_and_reschedule
    //       }>Refund Policy.</a></span></p>
    //              </div>
    //              </div>
    //              <div class="mt-5">
    //                  <p class="content-text fw-700">If you need to reschedule or cancel your appointment, please
    //                      contact us at least 24 hours in advance to avoid any cancellation
    //                      fees.</p>
    //              </div>
    //       ${this.footer()}
    //          </div>
    //      </div>
    //  </body>
    //  </html>`;

    let html1 = `<!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Cancellation</title>
     <link rel="preconnect" href="https://fonts.googleapis.com">
     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
     <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
     <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
 </head>
 <body style="font-family: 'Inter', sans-serif;">
     <!-- header -->
     <div class="container" style=" max-width: 700px; background-color: white; margin: 0 auto;">
     <div class="content" style=" padding: 1rem;">
                 ${this.logo()}
             <p class="Confirmed text-center pt-1" style=" text-align: center;padding-top: 1rem;font-size: 20px;font-weight: 600;letter-spacing: 1px;">Order cancelled</p>
    <!-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <!-- body -->
             <div>
                 <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Hi ${customer_name}</h3>
                 <p class="mt-1 content-text" style="font-size: 16px; line-height: 1.5;letter-spacing: 1px;margin-top: 20px;">Your order has been cancelled.<br>
                     I hope this email finds you well. We regret to inform you that your
                     recent order placed on ${moment(createdAt).tz('Asia/Kolkata').format('DD-MMM-YYYY')} with the order number
                     ${order_id} has been cancelled.
                     </p>
             </div>
             <div class="mt-3 order-detail" style=" margin-top: 3rem;">
                 <h3 class="Confirmed mt-3" style="font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Order Details</h3>
                 <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                 <li>Order id : ${order_id}</li>
                 <li>Appointment Date: ${dateOnly}</li>
                 <li>Appointment Time: ${FreelancerBookingSlots[booking_slot] || timeOnly}</li>
                 <li>Services: ${services}</li>
                 <li>Services type: ${service_type}</li>
                 </ul>
             </div>
             ${this.customerDetails(customer_name, customer_email, customer_mobile, mailData?.address)}
             <div class="mt-3 order-detail" style="margin-top: 3rem;">
                 <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Payment details</h3>
                 <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                     <li>Total amount : ${final_price}</li>
                     <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
                     <li>Method: ${payment_method || 'NA'}</li>
                     <li>Paid On: ${moment(createdAt).tz('Asia/Kolkata').format('DD-MMM-YYYY')}</li>
                 </ul>
             </div>
             <div class="mt-3" style=" margin-top: 3rem;">
             <div>
                 <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Reason for cancellation</h3>
                 <p class="mt-1 content-text" style="font-size: 16px; line-height: 1.5;letter-spacing: 1px;margin-top: 20px;">${cancellation_reason}</p>
                     <p class="mt-3 content-text"  style="font-size: 16px; line-height: 1.5;letter-spacing: 1px;margin-top: 20px;">Check our website to know about the <a href=${Slaylewks_details.cancelation_and_reschedule} style=" text-decoration: none;color: #00060f;font-weight: 800;">Refund Policy.</a></p>
             </div>
             </div>
 <!-- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------             -->
 <!-- footer -->
             <div class="mt-5" style=" margin-top: 3rem;">
                 <p class="content-text fw-700" style="font-size: 16px; line-height: 1.5;letter-spacing: 1px; font-weight: 700;">If you need to reschedule or cancel your appointment, please
                     contact us at least 24 hours in advance to avoid any cancellation
                     fees.</p>
             </div>
             ${this.footer()}
     </div>
     </div>
 </body>
 </html>`;

    return html1;
  }
  static cancelationTempVendor(mailData: any) {
    let {
      customer_name,
      customer_mobile,
      customer_email,
      order_id,
      appointment_start_date_time,
      payment_method,
      service_type,
      branch,
      payment_id,
      final_price,
      createdAt,
      cancellation_reason,
      comment,
      booking_slot
    } = mailData.order;
    let { branch_name } = mailData.branch || {};
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let services: string = '';
    let serviceList = mailData.appointments.map((appts, i) => {
      let { service_name } = appts.service || {};
      return (services = services.concat(service_name + ','));
    });
    let dateOnly = local_date_time.format('DD-MMM-YY');
    let timeOnly = local_date_time.format('hh:mm a');
    let paid_on = moment(createdAt).tz('Asia/Kolkata').format('DD-MMM-YYYY');
    // let html = `<!DOCTYPE html>
    // <html lang="en">
    // <head>
    //     <meta charset="UTF-8">
    //     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //     <title>Confirmed</title>
    //     <link rel="preconnect" href="https://fonts.googleapis.com">
    //     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    //     <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
    //     <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    // </head>
    // <body>
    //     <style>
    //         body,div,p,h1,h2,h3,h4,span{
    //             font-family: 'Inter', sans-serif;
    //         }
    //         p,h1,h2,h3,h4,span{
    //             margin: 0;
    //         }
    //         ul{
    //             padding-left: 0;
    //         }
    //         .mt-1{
    //             margin-top: 20px;
    //         }
    //         .pt-1{
    //             padding-top: 1rem;
    //         }
    //         .mt-2{
    //             margin-top: 2rem;
    //         }
    //         .mt-3{
    //             margin-top: 3rem;
    //         }
    //         .mt-5{
    //             margin-top: 5rem;
    //         }
    //         .fw-600{
    //             font-weight: 600;
    //         }
    //         .fw-700{
    //             font-weight: 700;
    //         }
    //         .container {
    //           max-width: 700px;
    //           background-color: white;
    //           margin: 0 auto;
    //         }
    //         .content{
    //             padding: 3rem;
    //         }
    //         .content img{
    //             height: 100px;
    //         }
    //         .Confirmed{
    //             font-size: 20px;
    //             font-weight: 600;
    //             letter-spacing: 1px;
    //         }
    //         .text-center{
    //             text-align: center;
    //         }
    //         .mx-auto{
    //             margin-left: auto;
    //             margin-right: auto;
    //         }
    //         .content-text{
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         .order-detail ul li{
    //             list-style: none;
    //             font-size: 16px;
    //             line-height: 1.5;
    //             letter-spacing: 1px;
    //         }
    //         a{
    //             text-decoration: none;
    //             color: black;
    //         }
    //         .invoice-pdf{
    //             font-size: 32px;
    //         }
    //         .invoice-pdf img{
    //             height: 20px;
    //         }
    //         .fa-circle-down{
    //             font-size: 18px;
    //         }
    //         .policy a{
    //             color: #ECB390;
    //         }
    //     </style>
    //     <div class="container">
    //         <div class="content">
    //         <div class="logo-img" style="text-align: center;"><img
    //         src="${Slaylewks_details.logo}" alt=""
    //         style=" width: 40%;margin-top: 15px;"></div>
    //             <p class="Confirmed text-center pt-1">Order cancelled</p>
    //             <div>
    //                 <h3 class="Confirmed mt-3">Hi ${branch_name}</h3>
    //                 <p class="mt-1 content-text">We regret to inform you that our order (Order ${order_id}) has been cancelled by our customer
    //                     <span class="fw-700">${customer_name}</span> on <span class="fw-700">${local_date_time.format(
    //   'DD-MMM-YYYY hh:mm a',
    // )} .</span></p>
    //             </div>
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Order Details</h3>
    //                 <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <li>Order id : ${order_id}</li>
    //                     <li>Appointment Date: ${dateOnly}</li>
    //                     <li>Appointment Time: ${timeOnly}</li>
    //                     <li>Services: ${services}</li>
    //                     <li>Services type: ${service_type}</li>
    //                 </ul>
    //             </div>
    //           ${this.customerDetails(customer_name, customer_email, customer_mobile, mailData.address)}
    //             <div class="mt-3 order-detail">
    //                 <h3 class="Confirmed mt-3">Payment details</h3>
    //                 <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
    //                     <li>Total amount : ${final_price}</li>
    //                     <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
    //                     <li>Method: ${payment_method}</li>
    //                     <li>Paid On: ${paid_on}</li>
    //                 </ul>
    //             </div>
    //             <div class="mt-5">
    //                 <h3 class="Confirmed">Reason for cancellation</h3>
    //                 <p class="content-text mt-1">${cancellation_reason}</p>
    //                     <p class="content-text mt-3">We sincerely apologize for any inconvenience this cancellation may
    //                         have caused, and we appreciate your understanding. We value your
    //                         business and hope to have the opportunity to serve you better in the
    //                         future.</p>
    //             </div>
    //           ${this.footer()}
    //         </div>
    //     </div>
    // </body>
    // </html>`;

    let html1 = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Confirmed</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body style=" font-family: 'Inter', sans-serif;">
    <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- head -->
        <div class="container" style=" max-width: 700px; background-color: white; margin: 0 auto;">
            <div class="content" style=" padding: 1rem;">
${this.logo()}
                <p class="Confirmed text-center pt-1" style=" text-align: center;padding-top: 1rem;font-size: 20px;font-weight: 600;letter-spacing: 1px;">Order cancelled</p>
    <!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
    <!-- body -->
                <div>
                    <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Hi ${branch_name}</h3>
                    <p class="mt-1 content-text" style="font-size: 16px; line-height: 1.5;letter-spacing: 1px;">We regret to inform you that our order (Order ${order_id}) has been cancelled by our customer
                        <span class="fw-700" style="font-weight: 700;">${customer_name}</span> on <span class="fw-700" style="font-weight: 700;">${moment(new Date()).tz('Asia/Kolkata').format('DD-MMM-YYYY')}, ${moment(new Date()).tz('Asia/Kolkata').format('hh:mm A')} .</span></p>
                </div>
                <div class="mt-3 order-detail" style="margin-top: 3rem;">
                    <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Order Details</h3>
                    <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                        <li>Order id : ${order_id}</li>
                        <li>Appointment Date: ${dateOnly}</li>
                        <li>Appointment Time: ${FreelancerBookingSlots[booking_slot] || timeOnly}</li>
                        <li>Services: ${services}</li>
                        <li>Services type: ${service_type}</li>
                    </ul>
                </div>
                ${this.customerDetails(customer_name, customer_email, customer_mobile, mailData.address)}
                <div class="mt-3 order-detail" style="margin-top: 3rem;">
                    <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Payment details</h3>
                    <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
                    <li>Total amount : ${final_price}</li>
                    <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
                    <li>Method: ${payment_method}</li>
                    <li>Paid On: ${paid_on}</li>
                    </ul>
                </div>
                <div class="mt-5" style=" margin-top: 3rem;">
                    <h3 class="Confirmed" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px;">Reason for cancellation</h3>
                    <p class="content-text mt-1" style="font-size: 16px; line-height: 1.5;letter-spacing: 1px;margin-top: 20px;">${cancellation_reason}</p>
                        <p class="content-text mt-3" style="font-size: 16px; line-height: 1.5;letter-spacing: 1px;margin-top: 3rem;">We sincerely apologize for any inconvenience this cancellation may
                            have caused, and we appreciate your understanding. We value your
                            business and hope to have the opportunity to serve you better in the
                            future.</p>
                </div>
    <!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ -->
    <!-- footer -->
                        ${this.footer()}
            </div>
        </div>
    </body>
    </html>`;

    return html1;
  }

  static rejectedTemp(contentData: Order[], remarks: String, customer_email?: string) {
    let {
      branch,
      order_id,
      service_type,
      address,
      appointments,
      created_at,
      final_price,
      payment_id,
      payment_method,
      ordersService,
      customer_name: user_name,
      customer_email: user_email,
      customer_mobile: user_mobile,
      booking_slot
    } = contentData[0]?.dataValues;
    let userOrderDate = new Date(created_at).toLocaleDateString();
    let { branch_name } = branch || {};
    let { appointment_start_date_time } = appointments[0] || {};
    // let { area, state, city, country, customer_name, customer_mobile, zipcode, flat_no } = address.area;
    let date = moment(appointment_start_date_time).tz('Asia/Kolkata');
    const appointmentDate = date.format('DD-MMM-YYYY');
    const appointmentTime = date.format('hh:mm a');

    let serviceName = ordersService.map((services) => {
      return services?.service?.service_name;
    });
    // let name = customer_email ? branch_name : customer_name;
    // let cancelReject  = customer_email ? 'Cancelled': 'Rejected';
    let customerDetails = address
      ? `  <div class="mt-3 order-detail">
    <h3 class="Confirmed mt-3">Customer details</h3>
    <ul style="list-style:none">
        <li>Name: ${address.customer_name}</li>
        <li>Address: ${address.flat_no}, ${address.area}, ${address.city}, ${address.state} ${address.country} ${address.zipcode}</li>
        <li>contact number: ${address.customer_mobile}</li>
    </ul>
</div>`
      : `<div class="mt-3 order-detail">
<h3 class="Confirmed mt-3">Customer details</h3>
<ul style="list-style:none">
    <li>Name: ${user_name}</li>
    <li>Email: ${user_email}</li>
    <li>contact number: ${user_mobile}</li>
</ul>
</div>`;
    let description = customer_email
      ? ` <h1 style="text-align: center;" class="Confirmed text-center pt-1">Order Rejected</h1>
      <div>
          <h3 class="Confirmed mt-3">Hi ${user_name}</h3>
          <p class="mt-1 content-text">Your order has been Rejected.<br>
              I hope this email finds you well. We regret to inform you that your
              recent order, placed on ${userOrderDate}, with the order number
              ${order_id} has been rejected.
              </p>
      </div>`
      : `
    <h1  style="text-align: center;" class="Confirmed text-center pt-1">Order Rejected</h1> 
    <div>
         <h3 class="Confirmed mt-3">${branch_name}</h3>
         <p class="mt-1 content-text">We regret to inform you that our order ${order_id} has been rejected</span></p>
     </div>`;

    let cancelReason = customer_email
      ? ` <div class="mt-3">
     <div>
         <h3 class="Confirmed mt-3">Reason for Rejection</h3>
         <p class="mt-1 content-text">${remarks}</p>
             <p class="mt-3 content-text">Check our website to know about the <span class="fw-700"><a href=${Slaylewks_details.cancelation_and_reschedule}>Refund Policy.</a></span></p>
     </div>
     </div>
     <div class="mt-5">
         <p class="content-text fw-700">If you need to reschedule or cancel your appointment, please
             contact us at least 24 hours in advance to avoid any cancellation
             fees.</p>
     </div>`
      : ` <div class="mt-5">
     <h3 class="Confirmed">Reason for Rejection</h3>
     <p class="content-text mt-1">${remarks}</p>
         <p class="content-text mt-3">We sincerely apologize for any inconvenience this rejection may
             have caused, and we appreciate your understanding. We value your
             business and hope to have the opportunity to serve you better in the
             future.</p>
 </div>`;

    let html = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Confirmed</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <style>
            body,div,p,h1,h2,h3,h4,span{
                font-family: 'Inter', sans-serif;
            }
            p,h1,h2,h3,h4,span{
                margin: 0;
            }
            ul{
                padding-left: 0;
            }
            .mt-1{
                margin-top: 20px;
            }
            .pt-1{
                padding-top: 1rem;
            }
            .mt-2{
                margin-top: 2rem;
            }
            .mt-3{
                margin-top: 3rem;
            }
            .mt-5{
                margin-top: 5rem;
            }
            .fw-600{
                font-weight: 600;
            }
            .fw-700{
                font-weight: 700;
            }
            .container {
              max-width: 700px;
              background-color: white;
              margin: 0 auto;
            }
            .content{
                padding: 3rem;
            }
            .content img{
                height: 100px;
                width:80% !important;
            }
            .Confirmed{
                font-size: 20px;
                font-weight: 600;
                letter-spacing: 1px;
            }
            .text-center{
                text-align: center;
            }
            .mx-auto{
                margin-left: auto;
                margin-right: auto;
            }
            .content-text{
                font-size: 16px;
                line-height: 1.5;
                letter-spacing: 1px;
            }
            .order-detail ul li{
                list-style: none;
                font-size: 16px;
                line-height: 1.5;
                letter-spacing: 1px;
            }
            a{
                text-decoration: none;
                color: black;
            }
            .invoice-pdf{
                font-size: 32px;
            }
            .invoice-pdf img{
                height: 20px;
            }
            .fa-circle-down{
                font-size: 18px;
            }
            .policy a{
                color: #ECB390;
            }
        </style>
        <div class="container">
            <div class="content">
            <div class="logo-img" style="text-align: center;"><img
            src="${Slaylewks_details.logo}" alt=""
            style=" width: 40%;margin-top: 15px;"></div>
               ${description}
                    <h3 class="Confirmed mt-3">Order Details</h3>
                    <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
                        <li>Order id : ${order_id}</li>
                        <li>Appointment Date: ${appointmentDate}</li>
                        <li>Appointment Time: ${FreelancerBookingSlots[booking_slot] || appointmentTime}</li>
                        <li>Services: ${serviceName}</li>
                        <li>Services type: ${service_type}</li>
                    </ul>
                </div>
                          ${customerDetails}
              <div class="mt-3 order-detail">
                    <h3 class="Confirmed mt-3">Payment details</h3>
                    <ul style="list-style:none;font-size: 16px;line-height: 1.5;letter-spacing: 1px;">
                        <li>Total amount : ${final_price}</li>
                        <li>Payment Id: ${payment_id ? payment_id : 'NA'}</li>
                        <li>Method: ${payment_method}</li>
                        <li>Paid On: ${userOrderDate}</li>
                    </ul>
                </div>
               ${cancelReason}
     
               ${this.footer()}
            </div>
        </div>
    </body>
    </html>`;

    return html;
  }
  static rescheduleTemp(appointments: Appointment[], order: Order, useraddress?: UserAddress) {
    let {
      order_id,
      customer_name,
      customer_email,
      customer_mobile: user_mobile,
      payment_id,
      payment_method,
      appointment_start_date_time,
      final_price,
      service_type,
      created_at,
      booking_slot
    } = order[0];
    let { flat_no, area, city, state, zipcode, customer_mobile } = useraddress || {};
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YYYY');
    let timeOnly = local_date_time.format('hh:mm a');
    let services: string = '';
    let serviceList = appointments.map((appts, i) => {
      let { service_name } = appts.service || {};
      return (services = services.concat(service_name + ','));
    });
    let paid_on = moment(created_at).tz('Asia/Kolkata').format('DD-MMM-YYYY');
    let userAddress = useraddress
      ? `<li>Address: ${flat_no},${area}${city}${state},${zipcode}</li>
      <li>Email: ${customer_email}</li> 
       <li>Contact number: ${customer_mobile}</li>`
      : `<li>Email: ${customer_email}</li> 
      <li>Contact number: ${user_mobile}</li>`;

    let html = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Confirmed</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="content">
${this.logo()}
                <p class="Confirmed text-center pt-1">Order Rescheduled</p>
                <div>
                    <h3 class="Confirmed mt-3">Hi ${customer_name}</h3>
                    <p class="mt-1 content-text">Your order has been rescheduled</p>
                </div>
                <div class="mt-3 order-detail order">
                    <h3 class="Confirmed mt-3">Order Details</h3>
                    <ul>
                        <li>Order id :${order_id}</li>
                        <li class="fw-600" style="font-weight: bold;">Appointment Date: ${dateOnly}</li>
                        <li class="fw-600" style="font-weight: bold;">Appointment Time: ${FreelancerBookingSlots[booking_slot] || timeOnly}</li>
                        <li>Services:${services}</li>
                        <li>Services type: ${service_type}</li>
                    </ul>
                </div>
                <div class="mt-3 order-detail">
                    <h3 class="Confirmed mt-3">Customer details</h3>
                    <ul>
                        <li>Name: ${customer_name}</li>
                        ${userAddress}
                      
                    </ul>
                </div>
                <div class="mt-3 order-detail">
                    <h3 class="Confirmed mt-3">Payment details</h3>
                    <ul>
                        <li>Total amount : ${final_price}</li>
                        <li>Payment Id:${payment_id ? payment_id : 'NA'}</li>
                        <li>Method: ${payment_method}</li>
                        <li>Paid On: ${paid_on}</li>
                    </ul>
                </div>
${this.footer()}
            </div>
        </div>
    </body>
    </html>`;

    return html;
  }

  static rescheduleVendorTemplete(appointments: Appointment[], order: Order, useraddress?: UserAddress) {

    let {
      order_id,
      customer_name,
      customer_email,
      customer_mobile: user_mobile,
      payment_id,
      payment_method,
      appointment_start_date_time,
      final_price,
      service_type,
      created_at,
      branch,
      booking_slot
    } = order[0];
    let { branch_name } = branch;
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YYYY');
    let timeOnly = local_date_time.format('hh:mm a');
    let services: string = '';
    let serviceList = appointments.map((appts, i) => {
      let { service_name } = appts.service || {};
      return (services = services.concat(service_name + ','));
    });
    let paid_on = moment(created_at).tz('Asia/Kolkata').format('DD-MMM-YYYY');
    let userAddress = useraddress
      ? `<li>Address: ${useraddress.flat_no},${useraddress.area}${useraddress.city}${useraddress.state},${useraddress.zipcode}</li>
      <li>Email: ${customer_email}</li> 
       <li>Contact number: ${useraddress.customer_mobile}</li>`
      : `<li>Email: ${customer_email}</li> 
      <li>Contact number: ${user_mobile}</li>`;

    let html = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Confirmed</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="content">
            ${this.logo()}
                <p class="Confirmed text-center pt-1" style="text-align: center; font-weight: bold;">Order Rescheduled</p>
                <div>
                    <h3 class="Confirmed mt-3">Hi ${branch_name}</h3>
                    <p class="mt-1 content-text">Our customer
                    ${customer_name} has rescheduled an appointment on ${dateOnly}  ${FreelancerBookingSlots[booking_slot] || timeOnly}</p>
                </div>
                <div class="mt-3 order-detail order">
                    <h3 class="Confirmed mt-3">Order Details</h3>
                    <ul>
                        <li>Order id :${order_id}</li>
                        <li class="fw-600" style="font-weight: bold;">Appointment Date: ${dateOnly}</li>
                        <li class="fw-600" style="font-weight: bold;">Appointment Time: ${FreelancerBookingSlots[booking_slot] || timeOnly}</li>
                        <li>Services:${services}</li>
                        <li>Services type: ${service_type}</li>
                    </ul>
                </div>
                <div class="mt-3 order-detail">
                    <h3 class="Confirmed mt-3">Customer details</h3>
                    <ul>
                        <li>Name: ${customer_name}</li>
                        ${userAddress}
                      
                    </ul>
                </div>
                <div class="mt-3 order-detail">
                    <h3 class="Confirmed mt-3">Payment details</h3>
                    <ul>
                        <li>Total amount : ${final_price}</li>
                        <li>Payment Id:${payment_id ? payment_id : 'NA'}</li>
                        <li>Method: ${payment_method}</li>
                        <li>Paid On: ${paid_on}</li>
                    </ul>
                </div>
                ${this.footer()}
            </div>
        </div>
    </body>
    </html>`;

    return html;
  }

  static payoutInvoiceForBranch(orderDetails: Order[], couponProvided?: any) {
    let { order_id, coupon_discount, final_price, appointment_start_date_time, ordersService, branch, gst, vendor_type, payout_amount, pay_by_cash, initial_amount } =
      orderDetails[0];
    let {
      tax_percentage,
      is_tax_checked,
      branch_name,
      branch_email,
      commission_type,
      payment_commission,
      location,
      branch_contact_no,
    } = branch;
    let { city, address,area } = location;
    let { provided_by, discount_type } = couponProvided || {};
    let sub_total: number = 0;
    let discount_total: number = 0;
    let slCommission: number = 0;
    let CGST: number = gst / 2;
    let SGST: number = gst / 2;
    let transactionPay: number = 0;
    let payoutAmount: number = 0;
    let grandTotal: number = 0;
    let slGST: number = 0;
    let provider: string = '';
    let tax = tax_percentage / 2;

    let servicesList = '';
    ordersService.map((records: OrderService, i: number) => {
      let { service } = records || {};
      let { amount, flash_sales, discount, service_name } = service;
      let { discount_value, discount_type } = flash_sales[0] || {};
      let flashSaleDiscount = discount_type == 'percentage' ? (amount / 100) * discount_value : discount_value;
      let discount_amount = flash_sales && flash_sales.length > 0 ? flashSaleDiscount : (amount / 100) * discount;
      discount_total += discount_amount;
      let discounted_amount = amount - discount_amount;
      sub_total += discounted_amount;

      let row = `<tr>
              <td>${service_name}</td>
              <td>₹${(amount ?? 0).toFixed(2)}</td>
              <td>₹${(discount_amount ?? 0).toFixed(2)}</td>
              <td class="table-data1">${(discounted_amount ?? 0).toFixed(2)}</td>
            </tr>`;
      servicesList += row;
      return;
    });
    let coupon_amount: number = 0;
    if (provided_by && provided_by == Provided_By.SUPER_ADMIN) {
      coupon_amount = coupon_discount;
      provider = '(Slaylewks)';
    }
    // CGST = is_tax_checked ? +(sub_total * (tax / 100)).toFixed(2) : 0;
    // SGST = is_tax_checked ? +(sub_total * (tax / 100)).toFixed(2) : 0;
    pay_by_cash ? grandTotal = Number((initial_amount).toFixed(2)) : grandTotal = Number((final_price).toFixed(2));

    slCommission = commission_type == slaylewks_commission.AMOUNT ? payment_commission : sub_total * (payment_commission / 100);
    slCommission = slCommission > 1000 ? slCommission = 1000 : slCommission = slCommission;
    slGST = slCommission * Slaylewks_details.gst;
    transactionPay = +(grandTotal * Slaylewks_details.transaction_fees);
    payoutAmount = (vendor_type == VendorType.FREELANCER) ? payout_amount : +(grandTotal - (slCommission + slGST + transactionPay) + coupon_amount).toFixed(2);
    let amountDetails = pay_by_cash ?
      ` <div class="order-details">
    <h3 class="table-sum2">Initial Payment</h3>
    <h3 class="table-sum2">${(grandTotal).toFixed(2)}</h3>
</div>
<div class="order-details">
    <h3 class="table-sum2">Final Payment</h3>
    <h3 class="table-sum2">${(grandTotal).toFixed(2)}</h3>
</div>`
      :
      `<div class="order-details">
<h3 class="table-sum2">Total amount</h3>
<h3 class="table-sum2">${grandTotal.toFixed(2)}</h3>
</div>`;


    let html = `<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>invoice-email</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
      </head>
    <body>
        <div class="invoice-email">
          <style>
            body{
                font-family: 'Inter', sans-serif;
            }
            .container {
              max-width: 939px;
              background-color: white;
              border: 1px solid #e1dddd;
              margin: 0 auto;
            }
            .inner-container {
              padding: 0px 50px;
            }
            .inner-second-container{
                padding: 0px 27px;
            }
            .main-header {
              width: 100%;
            }
            .info-details{
              display: flex;
              justify-content: space-between;
            }
            .first-line-left{
                margin-top: 33px;
                font-size: 18px;
                font-weight: 300;
            }
            .third-line-left{
                font-size: 18px;
                font-weight: 300;
            }
            .address{
                font-size: 15px;
                font-weight: 400;
            }
            .address-font1,.address-font2{
                font-size: 15px;
                font-weight: 400;
                display: flex;
            }
            .icon-location{
              margin-right: 12px;
            }
            .fa-phone{
                margin-right: 11px;
            }
            .fa-envelope{
                margin-right: 11px;
            }
            .fa-print{
                margin-right: 11px;
            }
            .right-hading{
                font-size: 40px;
                font-weight:400;
                white-space: nowrap;
            }
            .right-address{
                font-size: 15px;
                font-weight: 400;
            }
            .horizontal-line{
                border-bottom: 1.5px solid #00060f;
            }
            .order-details{
                display: flex;
                justify-content: space-between;
                margin-top: 20px;
            }
            .order-head-1{
                margin: 0;
                font-weight: 300;
                padding-bottom: 10px;
            }
            .order-head-2{
                margin: 0;
            }
            table {
              border-collapse: collapse;
               width: 100%;
               border: 2px solid black;
               margin-top: 15px;
               padding: 15px;
             }
             thead{
                border-bottom: 2px solid black ;
             }
             .table-head{
                text-align:center;
                background-color: #f9dbc7b3;
                padding: 15px;
             }
             .table-head1{
                text-align:start;
                background-color: #f9dbc7b3;
                padding: 10px 29px;
             }
             .table-head2{
                text-align:end;
                background-color: #f9dbc7b3 ;
                padding: 15px;
                padding: 10px 29px;
             }
             tbody{
                text-align:center;
                padding: 15px;
             }
             .table-data{
                text-align:start;
                padding: 10px 29px;
             }
             .table-data1{
                text-align:end;
                padding: 10px 29px;
             }
             .table-sum1{
                margin: 0;
                font-weight: 300;
                padding-bottom: 10px;
                font-size: 16px;
             }
             .table-sum2{
                margin: 0;
             }
             .sub-sum{
                font-size: 12px;
             }
             .sub-sum1{

                font-size: 14px;
                font-weight: 200;
                margin: 0;
             }
             .second-table-head{
                text-align:center;
                padding: 4px;
             }
             .second-table-head1{
                text-align:start;
                padding: 10px 29px;
             }
             .second-table-head2{
                text-align:end;
                padding: 10px 29px;
             }
             .footer{margin-top: 80px;
            }
             .footer1 a{
                text-decoration: none;
                color: #f29b61de;
            }
          </style>
          <div class="container">
            <div class="inner-container">
                <div class="info-details">
                   <div class="left-side">
                  <h3 class="first-line-left">${Helpers.generateInvoiceNumber('INV')}</h3>
                  <h3>BILLED TO <br>${branch_name} </h3>
                  <h4 class="third-line-left">${branch_name}</h4>
                   <h4 class="address"><i class="fa-sharp fa-solid fa-print"></i>220044020204</h4>
                   <h4 class="address"><i class="fa-sharp fa-solid fa-phone"></i>${branch_contact_no}</h4>
                   <div class="address-font1">
                  <div class="icon-location"><i class="fa-sharp fa-solid fa-location-dot" style="color: #00060f;"></i></div>
                  <div><h4 class="address" style="margin: 0;">${address ? address:area}<br>${city} <br> ${branch_contact_no}</h4></div>
                  </div>
                   <h4 class="address"><i class="fa-solid fa-envelope" style="color: #020a17;"></i>${branch_email}</h4>
                   </div>
                   <div class="right-side">
                     <h3 class="right-hading">I N V O I C E</h3>
                     <h3 class="">SLAYLEWKS INDIA PRIVATE <br> LIMITED</h3>
                     <h4 class="right-address">PAN No. ABJCS6521K</h4>
                     <div class="address-font2">
                       <div class="icon-location"><i class="fa-sharp fa-solid fa-location-dot" style="color: #00060f;"></i></div>
                       <div class="address">VILLA NO.5-G, LANE-5,<br>VOORA VILLA 96, BHAKTHA<br>VEDANTHA SAMY ROAD,<br>AKKARAI CHENNAI 600119<br>TAMIL NADU</div>

                     </div>
                     <h4 class="address" style="margin-top: 41px;"><i class="fa-solid fa-envelope" style="color: #020a17;"></i>${Slaylewks_details.email}</h4>
                   </div>
                </div>
                <hr class="horizontal-line">
                <div class="inner-second-container">
                    <h3>ORDER DETAILS</h3>
                    <div class="order-details">
                    <h4 class="order-head-1">Appoinement date & time</h4>
                     <h3 class="order-head-2">${new Date(appointment_start_date_time).toLocaleString('en-IN', {
      timeZone: 'Asia/Kolkata',
    })}</h3>
                    </div>
                    <div class="order-details">
                        <h4 class="order-head-1">Order Id</h4>
                        <h3 class="order-head-2">${order_id}</h3>
                    </div>
                </div>
                <table style="table-layout: fixed;word-wrap: break-word;">
                    <thead>
                        <tr>
                            <th class="table-head1">SERVICE</th>
                            <th class="table-head">ACTUAL PRICE </th>
                            <th class="table-head">DISCOUNT</th>
                            <th class="table-head2">PRICE</th>
                          </tr>
                    </thead>
                <tbody>
                ${servicesList}
                </tbody>
                  </table>
                  <div class="inner-second-container">
                    <div class="order-details" style="margin-top: 10px;">
                    <h4 class="table-sum1">Sub-Total</h4>
                     <h4 class="table-sum1">${sub_total.toFixed(2)}</h4>
                    </div>
                    <div class="order-details">
                        <h4 class="table-sum1">Coupon <span class="sub-sum">${provider}</span></h4>
                        <h4 class="table-sum1">${+coupon_discount.toFixed(2)}</h4>
                    </div>
                    <div class="order-details">
                        <h4 class="table-sum1">GST <span class="sub-sum">(${tax_percentage ?? 0}% of Sub-Total)</span></h4>
                        <h4 class="table-sum1">${gst.toFixed(2)}</h4>
                    </div>
                    ${amountDetails}
                </div>
                <hr class="horizontal-line" style="margin: 16px;">
                <div class="inner-second-container">
                    <div class="order-details" style="margin-top: 10px;">
                    <h4 class="table-sum1">Slaylekws Commission <span class="sub-sum">( ${commission_type === slaylewks_commission.AMOUNT ? payment_commission : payment_commission + ' %'
      }  of Sub-Total amount)</span></h4>
                     <h4 class="table-sum1">${slCommission.toFixed(2)}</h4>
                    </div>
                    <div class="order-details">
                        <h4 class="table-sum1">Slaylewks GST <span class="sub-sum">( 18% GST of Slaylewks commission)</span></h4>
                        <h4 class="table-sum1">${slGST.toFixed(2)}</h4>
                    </div>
                    <div class="order-details">
                        <h4 class="table-sum1">Transaction fee<span class="sub-sum">(2% of Total amount)</span></h4>
                        <h4 class="table-sum1">${transactionPay.toFixed(2)}</h4>
                    </div>
            </div>
            <hr class="horizontal-line" style="margin: 16px;">
            <div class="inner-second-container">
            <div class="order-details">
                <h3 class="table-sum2">PAYOUT AMOUNT</h3>
                <h3 class="table-sum2">${payoutAmount.toFixed(2)}</h3>
            </div>
            <h4 class="sub-sum1" style="margin: 0;">(Total amount - (Slaylewks commission + Slaylewks GST + Transaction fee) + Coupon)</h4>
            <div class="sub-sum1" style="display: flex;justify-content: space-around; margin: 0;">
              <h4 class="sub-sum1" style="margin-left: 5px;">${grandTotal.toFixed(2)}</h4>
              <h4 class="sub-sum1"> -</h4>
              <h4 class="sub-sum1">( ${slCommission.toFixed(2)} </h4>
              <h4 class="sub-sum1">+</h4>
              <h4 class="sub-sum1">${slGST.toFixed(2)}</h4>
              <h4 class="sub-sum1">+</h4>
              <h4 class="sub-sum1">${transactionPay.toFixed(2)} )</h4>
              <h4 class="sub-sum1">+ </h4>
              <h4 class="sub-sum1">${coupon_amount.toFixed(2)}</h4>
            </div>
            </div>
            <table>
                <thead>
                    <tr style="border: 2px solid #00060f;background:  #f9dbc7b3;">
                      <th style="text-align: start;padding: 15px;">TCS BREAKUP</th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                    </tr>
                    <tr>
                        <th class="second-table-head1">PARTICULARS</th>
                        <th class="second-table-head">CGST@ <br>0.5%</th>
                        <th class="second-table-head">SGST/UGST@ <br>0.5%</th>
                        <th class="second-table-head">IGST@ <br>1%</th>
                        <th class="second-table-head2">TOTAL</th>
                      </tr>
                </thead>
            <tbody>
                <tr>
                  <td class="table-data">TCS COLLECTED
                    FOR CUSTOMER PAID</td>
                  <td>${CGST.toFixed(2)}</td>
                  <td>${SGST.toFixed(2)} </td>
                  <td>-</td>
                  <td class="table-data1">${(CGST + CGST).toFixed(2)} </td>
                </tr>
                <tr>
                  <td class="table-data">TCS COLLECTED
                    FOR SLAYLEWKS</td>
                  <td>${(slGST / 2).toFixed(2)} </td>
                  <td>${(slGST / 2).toFixed(2)}</td>
                  <td>-</td>
                  <td class="table-data1">${slGST.toFixed(2)}</td>
                </tr>
            </tbody>
              </table>
              <div class="footer">
              <h3>AUTHORIZED SIGNATORY</h3>
              <h3>SLAYLEWKS INDIA PRIVATE LIMITED</h3>
            ${this.footer()}
              </div>
          </div>
        </div>
        </div>
    </body>
    </html>`;
    return html;
  }

  static payoutEmailTemplate(orderDetails: Order[]) {
    let { appointment_start_date_time, ordersService, branch, customer_name, customer_email, customer_mobile, booking_slot } =
      orderDetails[0];
    let { branch_name, commission_type, location, vendor } = branch;
    let sub_total = 0;
    let discount_total = 0;
    let local_date_time = moment(appointment_start_date_time).tz('Asia/Kolkata');
    let dateOnly = local_date_time.format('DD-MMM-YYYY');
    let timeOnly = local_date_time.format('hh:mm A');
    let servicesList: string = '';
    `${ordersService.map((records: OrderService, i: number) => {
      let { service } = records || {};
      let { amount, flash_sales, discount, service_name } = service;
      let { discount_value, discount_type } = flash_sales[0] || {};
      let flashSaleDiscount = discount_type == 'percentage' ? (amount / 100) * discount_value : discount_value;
      let discount_amount = flash_sales && flash_sales.length > 0 ? flashSaleDiscount : (amount / 100) * discount;
      discount_total += discount_amount;
      let discounted_amount = amount - discount_amount;
      sub_total += discounted_amount;

      //     let row = ` <div class="order-details-1">
      //     <h4 class="order-head-1">${service_name}</h4>
      //     <h4 class="order-head-1">${discounted_amount}</h4>
      // </div>`;
      let row = `<tr>
      <td style="font-size: 15px; width: 50%; font-weight:400;" colspan="2">${service_name}</td>
      <td style="font-size: 15px; width: 50%; font-weight:400;text-align: end;">${(discounted_amount ?? 0).toFixed(2)}</td>
  </tr>`;

      servicesList = servicesList.concat(row);
    })}`;
    let total = Number((sub_total).toFixed(2));

    // let html = `<!DOCTYPE html>
    //  <html lang="en">
    //  <head>
    //      <meta charset="UTF-8">
    //      <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //      <title>Invoice</title>
    //      <link rel="preconnect" href="https://fonts.googleapis.com">
    //      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    //      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
    //      <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
    //  </head>
    //  <body>
    //      <div>
    //          <style>
    //              body{
    //                  font-family: 'Inter', sans-serif;
    //              }
    //              .container {
    //            max-width: 939px;
    //            background-color: white;
    //            border: 1px solid #e1dddd;
    //            margin: 0 auto;
    //          }
    //          .inner-container {
    //            padding: 0px 50px;
    //          }
    //          .inner-second-container{
    //              padding: 0px 27px;
    //          }
    //          img{
    //              width: 24%;
    //          }
    //          .first-line{
    //              font-size: 16px;
    //          }
    //          .second-line{
    //              font-size: 20px;
    //          }
    //          .horizontal-line{
    //              border-bottom: 1.5px solid #00060f;
    //          }
    //          .third-line{
    //              font-size: 18px;
    //              font-weight: 300;
    //          }
    //          .order-details{
    //              display: flex;
    //              justify-content: space-between;
    //          }
    //          .order-head-1{
    //              margin: 0;
    //              font-weight: 300;
    //              padding-bottom: 10px;
    //          }
    //          .order-details-1{
    //              display: flex;
    //              justify-content: space-between;
    //              width: 40%;
    //          }
    //          .order-head-2{
    //              margin: 0;
    //              font-size: 16px;
    //          }
    //          .fa-phone{
    //              margin-right: 11px;
    //          }
    //          .fa-envelope{
    //              margin-right: 11px;
    //          }
    //          .fa-user{
    //              margin-right: 11px;
    //          }
    //          .pdf-file{
    //              text-decoration: none;
    //              color: #00060f;
    //              font-weight: 300;
    //              font-size: 18px;
    //          }
    //          .pdf-file img{
    //              width: 2%;
    //              margin-right: 12px;
    //          }
    //          .footer a{
    //              text-decoration: none;
    //              color: #f29b61de;
    //          }
    //          </style>
    //          <div class="container">
    //             <div class="inner-container">
    //              <div class="logo-img"><img src="./images/SLAYLEWKS.png" alt=""></div>
    //              <h3 class="first-line">Awesome,You have closed your order</h3>
    //              <h3 class="second-line">Hi ${vendor?.vendor_name || 'NA'}</h3>
    //              <h4 class="third-line">Branch name - ${branch_name || 'NA'}</h4>
    //              <hr class="horizontal-line">
    //              <div class="order-details">
    //                  <h4 class="order-head-1">Appoinement date & time</h4>
    //                  <h4 class="order-head-1">${new Date(appointment_start_date_time).toLocaleString('en-IN', {
    //   timeZone: 'Asia/Kolkata',
    // })}</h4>
    //              </div>
    //              <h3>Service</h3>
    //              ${servicesList}
    //              <div class="order-details-1">
    //                  <h3 class="order-head-2">Total</h3>
    //                  <h3 class="order-head-2">${sub_total}</h3>
    //              </div>
    //              <hr class="horizontal-line">
    //              <h3>customer Details</h3>
    //              <div class="inner-second-container">
    //              <h4 class="address"><i class="fa-solid fa-user" style="color: #161717;"></i></i>${customer_name}</h4>
    //              <h4 class="address"><i class="fa-sharp fa-solid fa-phone"></i>${customer_mobile}</h4>
    //              <h4 class="address"><i class="fa-solid fa-envelope" style="color: #020a17;"></i>${customer_email}</h4>
    //             ${this.footer()}
    //          </div>
    //      </div>
    //  </body>
    //  </html>`;

    let html1 = `<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Invoice</title>
            <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
            <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600&display=swap"
                rel="stylesheet">
            <script src="https://kit.fontawesome.com/241380efd2.js" crossorigin="anonymous"></script>
        </head>
        
        <body style="font-family: 'Inter', sans-serif;">
            <div>
                <div class="container"
                    style="max-width: 939px;background-color: white;border: 1px solid #e1dddd;margin: 0 auto;">
                    <div class="inner-container" style=" padding: 0px 50px;">
${this.logo()}
                        <h3 class="first-line" style=" font-size: 16px;">Awesome,You have closed your order</h3>
                        <h3 class="second-line" style=" font-size: 20px;">Hi ${vendor?.vendor_name || 'NA'}</h3>
                        <h4 class="third-line" style="font-size: 18px;font-weight: 300;">Branch name - ${branch_name || 'NA'}</h4>
                        <hr class="horizontal-line" style="border-bottom: 1.5px solid #00060f;">
                        <table style="width: 100%;">
                                <tr>
                                <td style="font-size: 16px;  text-decoration: none;color: #00060f;font-weight: 600;">Appointment date & time</td>
                                <td style="font-size: 16px;text-align: end;  text-decoration: none;color: #00060f;font-weight: 600;">${dateOnly} ${FreelancerBookingSlots[booking_slot] || timeOnly}</td>
                            </tr>
                        </table>
                        <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Service</h3>
                        <table style="width: 100%;">
                        ${servicesList}
                            <tr>
                                <td style="font-size: 16px;  text-decoration: none;color: #00060f;font-weight: 600;"colspan="2">Total</td>
                                <td style="font-size: 16px;text-align: start;  text-decoration: none;color: #00060f;font-weight: 600;text-align: end;">${total}</td>
                            </tr>
                        </table>
                        <hr class="horizontal-line" style="border-bottom: 1.5px solid #00060f;">
                    ${this.customerDetails(customer_name, customer_mobile, customer_email)}
${this.footer()}
                    </div>
                </div>
            </div>
        </body>
        
        </html>`;

    return html1;
  }

  static helpAndSupportTemplete(createHelpAndSupportDto: CreateHelpAndSupportDto) {
    let { from_email, message, phone_number, name } = createHelpAndSupportDto;
    let phone = phone_number ? `<p>phone number:${phone_number}</p>` : '';
    let html = `<p>name:${name}</p>
    <p>from:${from_email}</p>
    <p>message:${message}</p>
    ${phone}`;

    return html;
  }

  static EMAIL_DATA = {
    SUBJECT: 'Slaylewks',
    OTP_VERIFY: 'OTP verification',
    ORDER_CONFIRMED: 'Order Confirmed',
    ORDER_CANCELLED: 'Order Cancelled',
    ORDER_REJECTED: 'Order Rejected',
    ORDER_RESCHEDULED: 'Order Rescheduled',
    ORDER_REMINDER: 'Order Reminder',
    ORDER_AWAITING: 'Order Awiating',
    PASSWORD_SENT: 'Password Verification',
    RESET_PASSWORD_SENT: 'Reset Password',
    FORGOT_PASSWORD: 'Forgot password',
    TEXT_FOR_SLAYLEWKS: 'Thank you for contacting slaylewks',
    VENDOR_ENQUIRY_REQUEST: 'Vendor Enquiry Request',
  };

  static customerDetails(
    customer_name: string,
    customer_email: string,
    customer_mobile: string,
    address?: UserAddress,
  ) {
    return address
      ? ` <div class="mt-3 order-detail">
    <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Customer details</h3>
    <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
        <li>Name: ${customer_name || "NA"}</li>
        <li>Email: ${customer_email || "NA"}</li>
        <li>Address: ${address.flat_no || "NA"}, ${address.area || "NA"}, ${address.city || "NA"}, ${address.state || "NA"} ${address.country || "NA"} ${address.zipcode || "NA"}</li>
        <li>contact number: ${customer_mobile || "NA"}</li>
    </ul>
  </div>`
      : ` <div class="mt-3 order-detail">
         <h3 class="Confirmed mt-3" style=" font-size: 20px;font-weight: 600;letter-spacing: 1px; margin-top: 3rem;">Customer details</h3>
         <ul style="list-style: none; font-size: 16px;line-height: 1.5;letter-spacing: 1px;padding-left: 0;">
             <li>Name: ${customer_name || "NA"}</li>
             <li>Email: ${customer_email || "NA"}</li>
             <li>contact number: ${customer_mobile || "NA"}</li>
         </ul>
       </div>`;
  }

  static footer() {
    // return `
    // <div class="mt-5">
    // <p class="content-text">If you have any questions or concerns, please don’t hesitate to get in touch with us at
    //      <a href="mailto:${Slaylewks_details.email}">${Slaylewks_details.email}</a>
    //     </p>
    // <p class="content-text mt-1">For any order related queries please reach out to Slaylewks team</p>
    // <p class="content-text mt-1">Thanks!</p>
    // <p class="content-text mt-1">Slaylewks Team</p>
    // <p class="content-text mt-1 policy"><a href=${Slaylewks_details.web_url}>Slaylewks.com</a> | <a href=${Slaylewks_details.privacy_policy}>Privacy policy</a> | <a href=${Slaylewks_details.terms_and_conditions}>Terms and Conditions</a> | <br><a href=${Slaylewks_details.cancelation_and_reschedule}>Cancellation and Rescheduling policy</a></p>
    // </div>`;
    return `
    <div class="mt-5 fw-700" style=" margin-top: 3rem;  text-decoration: none;color: #00060f; font-weight: 800; font-size: 16px; line-height: 1.5;letter-spacing: 1px; font-weight: 700;">
    <p>If you have any questions or concerns, please don’t hesitate to get in touch with us at
    <a href="mailto:${Slaylewks_details.email}">${Slaylewks_details.email}</a></p>
  <p>Thank you for choosing Slaylewks. We hope you had a good experience!</p>
  <p>For any order related queries please reach out to Slaylewks team.</p>
  <p>Thanks!</p>
  <p>Slaylewks Team.</p>
  </div>
  <h3 class="footer"><a href="${Slaylewks_details.web_url}" style=" text-decoration: none;color: #f29b61de;">Slaylewks.com </a>| <a href="${Slaylewks_details.privacy_policy}" style=" text-decoration: none;color: #f29b61de;">Privacy policy</a> | <a href="${Slaylewks_details.terms_and_conditions}" style=" text-decoration: none;color: #f29b61de;">Terms and Conditions</a> | <a href=${Slaylewks_details.cancelation_and_reschedule} style=" text-decoration: none;color: #f29b61de;">Cancellation and Rescheduling policy</a></h3>`;
  }
}

//<h4 class="pdf-file"><a href="./images/invoice-email.pdf" download class="pdf-file"><img src="./images/pdf.svg" alt="">Invoice.pdf </a> <a href="./images/invoice-email.pdf" download><i class="fa-sharp fa-regular fa-circle-down" style="color: #1b1e22;"></i></a></h4>
