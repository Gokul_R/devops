import { logger } from './logger';
import OrderRemainder from '../database/models/OrderRemainder';
import { Op } from 'sequelize';
import UserNotification from '../database/models/UserNotification';
import { MailUtils } from './mailUtils';
import { CronJob } from 'cron';
import { SchedulerRegistry } from '@nestjs/schedule';
import User from '../database/models/User';
import { SendPushNotification } from './sendPushNotification';
import moment from 'moment';
import { CreateNotificationDto } from 'src/modules/notification/dto/create-notification.dto';
import { NotificationType, Slaylewks_details, notificationTypeConfig, SECRET_FILENAME, LOCAL } from '../constants';
import Vendor from '../database/models/Vendor';
import { SecretManager } from './secretDatas';
import { newLogger } from './loggerNew';
import { AdminService } from 'src/admin/admin.service';
export class CronUtils {
  private schedulerRegistry: SchedulerRegistry;
  constructor(schedulerRegistry: SchedulerRegistry) {
    this.schedulerRegistry = schedulerRegistry;
  }
  // cron job running on every half hour
  public async sendAlertEmailToUserForOrders() {
    try {
      logger.info('Cronjob_entry: ' + new Date());
      // newLogger.info({
      //   type: 'cron',
      //   body: 'Every 30 minutes',
      //   response: { Cronjob_entry: new Date() },
      // });
      const recordsOrderRemind = await OrderRemainder.findAll({
        where: {
          trigger_time: {
            [Op.gte]: new Date().getTime() - 3600000, //-1h // 7200000 - 2h,// 1800000 - 30 m, // Find records with timestamps after 2 hours from now
            [Op.lte]: new Date().getTime() + 3600000, //-1h  // 7200000  - 2h, // 1800000 - 30 m // Find records with timestamps before 2 hours ago
          },
          is_sent: false,
          is_active: true,
        },
      });
      if (recordsOrderRemind?.length <= 0) return;
      else {
        // console.log(recordsOrderRemind);
        for (let index = 0; index < recordsOrderRemind.length; index++) {
          let element: any = recordsOrderRemind[index];
          logger.info(
            'OrderRemainderExcutionOrderId: ' + JSON.stringify(element?.order_data?.order_id || element?.order_data),
          );
          newLogger.info({
            type: 'cron',
            body: 'Every 30 minutes',
            response: { OrderRemainderExcutionOrderId: element?.order_data?.order_id || element?.order_data },
          });
          let sendAlertRemaindMail = await this.sendAlertRemaindMail(element);
          await this.sendNotifyToUser(
            element?.order_data?.user_id,
            element?.order_data?.customer_name,
            element?.order_data?.order_id,
            element?.order_data?.appointment_start_date_time,
          );
        }
      }
    } catch (error) {
      logger.error('CronJob_Error: ' + JSON.stringify(error));
      newLogger.error({
        type: 'cron',
        body: 'Every 30 minutes',
        response: { CronJob_Error: error },
      });
    }
  }

  private async sendNotifyToUser(usersId: string, userName: string, orderId: string, appointment: any) {
    let local_date_time = moment(appointment).tz('Asia/Kolkata');
    // let dateOnly = local_date_time.format('DD-MMM-YYYY');
    let timeOnly = local_date_time.format('hh:mm A');
    let reqObj = {
      user_id: usersId,
      title: `Hi ${userName || 'Slaylewks users'} 💇 Your order is coming soon!`,
      body: `Hello ${
        userName || 'Slaylewks users'
      }! Great news - your order with ID ${orderId} is scheduled to arrive soon (by ${timeOnly}). Get ready to elevate your self-care routine! Thank you for choosing Slaylewks.com.`,
      data: { order_id: orderId, Time: appointment, user_id: usersId },
      dataModels: User,
    };
    await new SendPushNotification().sendNotifyToUserPhone(reqObj);
  }

  private async sendAlertRemaindMail(req_data: any) {
    try {
      // let req: any = {
      //   description: 'Today Your Service Available',
      //   title: req_data?.order_data?.branch?.branch_name,
      //   image: req_data?.order_data?.branch?.branch_image,
      //   user_id: [req_data?.order_data?.user_id],
      // };
      let semdMailUser = await new MailUtils().sendOrderReminderMail(
        req_data?.user_email,
        req_data?.appointment_data,
        req_data?.order_data,
      );
      let semdMailVendor = await new MailUtils().sendOrderReminderMailVendor(
        req_data?.branch_email,
        req_data?.appointment_data,
        req_data?.order_data,
      );
      // await UserNotification.create(req);
      this.deleteOrderRemainder(req_data?.order_data?.order_id);
      this.notifyVendor(req_data?.order_data?.branch?.vendor_id, NotificationType.REMAINDER);
    } catch (error) {
      OrderRemainder.update({ error_data: error }, { where: { order_id: req_data?.order_data?.order_id } });
      logger.error(`SendOrder_Remaind_Error: Order_id:${req_data?.order_data?.order_id}: ` + JSON.stringify(error));
      newLogger.error({
        type: 'cron',
        body: 'Every 30 minutes',
        response: { SendOrder_Remaind_Error: { Order_id: req_data?.order_data?.order_id, error: error } },
      });
    }
  }
  public deleteOrderRemainder(order_id: string) {
    OrderRemainder.destroy({ force: true, where: { order_id: order_id } });
  }

  async notifyVendor(vendor_id: string, type: NotificationType) {
    if (vendor_id) {
      const typeConfig = notificationTypeConfig[type];
      const vendor: Vendor = await Vendor.findByPk<Vendor>(vendor_id);
      let notificationData = {
        to:
          vendor.device_token ||
          'eQNzky7-SUmu1SnTM2-Ed6:APA91bHTcGmahV707KPx16jhKCzKlcyOXcjjy_zdmTX_vMgxnPKVXUrEVycS576quAXXKqi8tdklwdnlPuYDTjLF9VJfDnowOXJOqP4mHbFg8cHDtLHvhq6nmB8iqGQXGWUB7o-0ynlS',
        notification: {
          title: typeConfig.title,
          body: typeConfig.description,
          image: `${Slaylewks_details?.logo}`,
        },
      };
      new SendPushNotification().sendNotification(notificationData);
    }
  }
}

/**---------------------------------------------------------------------------------------------- */
// The purpose of a cron job to send reminder emails only to ordered users.
// This function extcution time is every half an hour 30 minutes
const job = new CronJob('0 */30 * * * *', function () {
  logger.info('Order_remainder_cron_job_executed_at', new Date());
  newLogger.info({
    type: 'cron',
    body: 'Every 30 minutes',
    response: { Order_remainder_cron_job_executed_at: new Date() },
  });
  const runCronFun = new CronUtils(null).sendAlertEmailToUserForOrders();
});
job.start();
/**---------------------------------------------------------------------------------------------- */
//The purpose of cron job to get the aws secrets from AWS secret manager.
//This function execute for every 5 hours.
const job1 = new CronJob('0 */5 * * *', () => {
  logger.info('Find_secret_data_cron_job_executed_at', new Date());
  new SecretManager().awsSecretManager();
});
newLogger.info({
  type: 'cron',
  body: 'Every 5 hours',
  response: { Find_secret_data_cron_job_executed_at: new Date() },
});
job1.start();
/**----------------------------------------------------------------------------------------------- */
//The purpose of cron job to delete logs older then 30 days.
//This function execute for every 24 hours.
const job2 = new CronJob('0 0 */24 * *', () => {
  console.log('test cro');
  logger.info('Deleting_old_logs_cron_job_executed_at', new Date());
  newLogger.info({
    type: 'cron',
    body: 'Every 24 hours',
    response: { Deleting_old_logs_cron_job_executed_at: new Date() },
  });
  const data = new AdminService().deleteOldLogs();
});
// job2.start()
job2.start();

/**----------------------------------------------------------------------------------------------- */
