import winston from 'winston';
import { format } from 'winston';
const { combine, timestamp, printf } = format;

import DailyRotateFile from 'winston-daily-rotate-file';
import { PRODUCTION } from '../constants';

const combinedFileRotateTransport = new DailyRotateFile({
  filename: './logs/%DATE%.log',
  datePattern: 'YYYY-MM-DD',
  maxFiles: '51d',
  zippedArchive: true,
  maxSize: '20m',
  dirname: './logs',
});

const combinedFileTransport = new winston.transports.File({
  filename: './logs/CombinedLogs.log',
  maxsize: 20 * 1024 * 1024,
  tailable: true, // Keep file open for further writing
});

export const newLogger = winston.createLogger({
  format: combine(
    timestamp({
      format: 'YYYY-MM-DD hh:mm:ss.SSS A',
    }),
    printf((info) => {
      const { level, type, code, body, url, response, message, response_time, response_size } = info;
      return JSON.stringify({
        level: level,
        date: new Date(info.timestamp).toISOString().split('T')[0], // Extracting date from timestamp
        type,
        url,
        code,
        body,
        response,
        message,
        response_time,
        response_size,
        timestamp: info.timestamp,
      });
    }),
  ),
  transports: [combinedFileRotateTransport, combinedFileTransport],
});

if (process.env.NODE_ENV !== PRODUCTION) {
  newLogger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    }),
  );
}

//------------------------------------------------------------------------->>
// require('dotenv').config();
// import winston from 'winston';
// const { combine, timestamp, printf, align } = winston.format;

// import DailyRotateFile from 'winston-daily-rotate-file';
// import { PRODUCTION } from '../constants';
// import { Req } from '@nestjs/common';

// const combinedFileRotateTransport = new DailyRotateFile({
//   filename: 'NewCombined-%DATE%.log',
//   datePattern: 'YYYY-MM-DD',
//   maxFiles: '20d', // Retain logs for the last 20 days
//   zippedArchive: true,
//   maxSize: '20m',
//   dirname: './logs',
// });

// // export const newLogger = winston.createLogger({
// //   format: combine(
// //     timestamp({
// //       format: 'YYYY-MM-DD hh:mm:ss.SSS A',
// //     }),
// //     align(),
// //     printf(
// //       (info) =>
// //         `timestamp:${info.timestamp},logType:{info.level},type:${info.type},body:${info.body},response:${info.response},message:${info.message}`,
// //     ),
// //   ),
// //   transports: [combinedFileRotateTransport],
// // });
// export const newLogger = winston.createLogger({
//   format: combine(
//     timestamp({
//       format: 'YYYY-MM-DD hh:mm:ss.SSS A',
//     }),
//     printf((info) => {
//       const { level, type, body, response, message } = info;
//       return JSON.stringify({
//         level: level,
//         date: new Date(info.timestamp).toISOString().split('T')[0], // Extracting date from timestamp
//         type,
//         body,
//         response,
//         // Request,
//         // URL,
//         // Device_OS,
//         // Status,
//         // Client_IP,
//         // Response_Time,
//         // Device_Type,
//         message,
//         timestamp: info.timestamp,
//       });
//     }),
//   ),
//   transports: [combinedFileRotateTransport],
// });

// if (process.env.NODE_ENV != PRODUCTION) {
//   newLogger.add(
//     new winston.transports.Console({
//       format: winston.format.simple(),
//     }),
//   );
// }
