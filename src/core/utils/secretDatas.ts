import AWS from "aws-sdk";
import { SECRET_FILENAME, PRODUCTION, STAGING } from "../constants";
import { logger } from "./logger";
import fs from 'fs';

export class SecretManager {
    constructor() { }


    async awsSecretManager() {
        await this.findSecretdatas();
    }
    protected async findSecretdatas() {
        if (process.env.NODE_ENV == STAGING || process.env.NODE_ENV == PRODUCTION) {
            AWS.config.update({
                accessKeyId: process.env.SM_ACCESS_KEY_ID,
                secretAccessKey: process.env.SM_SECRET_ACCESS_KEY_ID,
                region: process.env.SM_REGION,
            });
            const secret_name = [process.env.SM_PROD_SECRET_NAME, process.env.SM_PROD_DB_SECRET_NAME];
            for (const secret of secret_name) {
                const client = new AWS.SecretsManager();
                try {
                    const data = await client.getSecretValue({ SecretId: secret }).promise();
                    if (!data) return;
                    logger.info('ScretManager-Data--->>', JSON.stringify(data));
                    let SecretStringJson = JSON.parse(data.SecretString);
                    let existingContent = fs.readFileSync(SECRET_FILENAME, 'utf-8'); // Read the existing content of the file
                    let existingData: any = existingContent // Parse the existing content into an object
                        .split('\n') // Split content by newline
                        .map(line => line.trim()) // Trim whitespace from each line
                        .filter(line => line && !line.startsWith('#')) // Filter out empty lines and comments
                        .reduce((acc, line) => {
                            const [key, ...valueParts] = line.split('=');
                            const value = valueParts.join('=');
                            acc[key] = value;
                            return acc;
                        }, {});

                    for (const [key, value] of Object.entries(SecretStringJson)) {
                        if (existingData.hasOwnProperty(key)) {
                            existingData[key] = value; // Update existing value
                        } else {
                            existingData[key] = value; // Append new key-value pair
                        }
                    }

                    // Convert updated data to string
                    const updatedContent = Object.entries(existingData)
                        .map(([key, value]) => `${key}=${value}`)
                        .join('\n');

                    // Write updated data back to the environment file
                    fs.writeFileSync('.env', updatedContent);

                } catch (error) {
                    logger.info('find_aws_secret_conjob_error', error);
                    console.error('Error retrieving secret:', error);
                }

            }
        }
    }
}