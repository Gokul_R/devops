import { Injectable, NestInterceptor, ExecutionContext, CallHandler, Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { logger } from './logger';
import { newLogger } from './loggerNew';

@Injectable()
export class ResponseLoggingInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest();
    const host = request.headers.host;
    const protocol = context.getType();
    const { originalUrl } = context.switchToHttp().getRequest();
    const fullUrl = `${protocol}://${host}${originalUrl}`;
    const response = context.switchToHttp().getResponse();
    const now = Date.now();
    const type = this.extractType(originalUrl);
    const { body } = request;
    const originalResponse = response.send;
    const clientIP = request?.headers['x-forwarded-for'] || request?.connection?.remoteAddress;
    const deviceName = request?.device?.type;

    response.send = function (responseData: any) {
      const responseTime = Date.now() - now;
      // const contentLength = responseData ? JSON.stringify(responseData).length : 0; // Calculate content length
      const contentLength = Buffer.byteLength(JSON.stringify(responseData), 'utf8');
      const parsedResponseData = JSON.parse(responseData); // Assuming responseData is a JSON string
      const code = parsedResponseData.code;

      if (type !== 'logs') {
        if (code === 500) {
          newLogger.error({
            'Device.os': request.rawHeaders[13],
            Device_ID: clientIP,
            Device_Type: deviceName,
            type: type,
            url: fullUrl,
            code: code,
            body: body,
            response: parsedResponseData,
            response_time: responseTime.toString() + 'ms',
            response_size: contentLength.toString() + 'kb', // Adjust format
          });
        } else {
          newLogger.info({
            'Device.os': request.rawHeaders[13],
            Device_IP: clientIP,
            Device_Type: deviceName,
            type: type,
            url: fullUrl,
            code: code,
            body: body,
            response: parsedResponseData,
            response_time: responseTime.toString() + 'ms',
            response_size: contentLength.toString() + 'kb', // Adjust format
          });
        }
      }
      originalResponse.call(this, responseData); // Calling the original response.send method
    };
    return next.handle().pipe(
      tap(() => {
        const request = context?.switchToHttp().getRequest();
        const response = context?.switchToHttp().getResponse();
        const responseTime = Date.now() - now;
        const clientIP = request?.headers['x-forwarded-for'] || request?.connection?.remoteAddress;
        const deviceName = request?.device?.type;
        logger.info(
          `Entry of API-->Request: ${request?.method},  URL: ${request?.originalUrl},  Device_OS: ${request?.rawHeaders[13]},  Client_IP: ${clientIP},  Device_Type: ${deviceName}`,
        );
        logger.info(
          `Exit of API--> Response: ${request?.method},   URL: ${request?.url},  Status: ${response?.statusCode},  Response_Time: ${responseTime}ms`,
        );
      }),
    );
  }
  private extractType(url: string): string {
    const partsAfterV1 = url.split('/v1/')[1];
    const parts = partsAfterV1.split('/');
    // const part1 = parts[1] || '';
    // if (part1 === '') {
    //   return parts[0];
    // }
    // const part2 = parts[2] || '';
    // return `${parts[0]}/${part1}/${part2}`;
    return parts[0];
  }
}
