import {  Injectable } from "@nestjs/common";
import { DiscountStrategy } from "./discount-strategy";

@Injectable()
export class PercentageDiscountStrategy implements DiscountStrategy {
    calculateDiscount(originalPrice: number, discountPercentage: number): number {
      const discountAmount = (originalPrice * discountPercentage) / 100;
      const discountedPrice = originalPrice - discountAmount;
      return discountedPrice;
    }
  }