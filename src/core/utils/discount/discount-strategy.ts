export interface DiscountStrategy {
    calculateDiscount(originalPrice: number, discountValue: number): number;
  }