import { Inject, Injectable } from '@nestjs/common';
import { DiscountStrategy } from './discount-strategy';
import {
  FLAT_DISCOUNT_STRATEGY,
  PERCENTAGE_DISCOUNT_STRATEGY,
} from 'src/core/constants';

@Injectable()
export class DiscountService {
  constructor(
    @Inject(FLAT_DISCOUNT_STRATEGY)
    private readonly flatDiscountStrategy: DiscountStrategy,
    @Inject(PERCENTAGE_DISCOUNT_STRATEGY)
    private readonly percentageDiscountStrategy: DiscountStrategy,
  ) {}

  calculateDiscountedPrice(
    originalPrice: number,
    discountValue: number,
    is_flat: boolean,
  ): number {
    // Use the appropriate discount strategy based on the discount type
    const discountStrategy = is_flat
      ? this.flatDiscountStrategy
      : this.percentageDiscountStrategy;

    // Calculate the discounted price using the selected strategy
    const discountedPrice = discountStrategy.calculateDiscount(
      originalPrice,
      discountValue,
    );

    return discountedPrice;
  }
}
