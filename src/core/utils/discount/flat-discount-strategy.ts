import { Injectable } from "@nestjs/common";
import { DiscountStrategy } from "./discount-strategy";

@Injectable()
export class FlatDiscountStrategy implements DiscountStrategy {
    calculateDiscount(originalPrice: number, discountPercentage: number): number {
      const discountAmount = discountPercentage;
      const discountedPrice = originalPrice - discountAmount;
      return discountedPrice;
    }
  }