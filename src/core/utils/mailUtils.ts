import { Injectable, Logger } from '@nestjs/common';
const sgMail = require('@sendgrid/mail');
import puppeteer from 'puppeteer';
import EmailTemService from './emailTemplate';
import { CreateHelpAndSupportDto } from 'src/admin/help-and-support/dto/create-help-and-support.dto';
import SmtpSettings from '../database/models/SmtpSettings';
import { PRODUCTION, Slaylewks_details } from '../constants';
import { logger } from './logger';
import Branch from '../database/models/Branch';
import Location from '../database/models/Location';
import Appointment from '../database/models/Appointment';
import Order from '../database/models/Order';
import { CreateVendorDto } from 'src/admin/vendor/dto/create-vendor.dto';
import { VendorEnquiryDto } from 'src/admin/vendor/dto/enquiry-vendor.dto';
import { newLogger } from './loggerNew';

@Injectable()
export class MailUtils {
  async sendOrderConfirmationMail(to_mail: string, confirmationContent: string, inovoice_pdf?: Buffer) {
    logger.info(`Send_email_order_confirm_entry_email_id: ${to_mail} `);
    newLogger.info({
      type: 'mail',
      body: { Send_email_order_confirm_entry_email_id: to_mail },
    });
    let mailOptions: MailOptions = {
      to: to_mail,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_CONFIRMED,
      html: confirmationContent,
      // cc: ['slaylewksdata@gmail.com', 'slaylewks.official@gmail.com'],
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live : Slaylewks_details.cc_mail_dev,
      attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'invoice.pdf' }],
    };
    await this.sendEmail(mailOptions);
  }
  async sendOrderPlacedMailToVendor(to_mail: string, appointments: Appointment[], inovoice_pdf: Buffer, order: Order) {
    logger.info(`Send_email_order_confirm_entry_email_id: ${to_mail} `);
    newLogger.info({
      type: 'mail',
      body: { Send_email_order_confirm_entry_email_id: to_mail },
    });
    let mailOptions: MailOptions = {
      to: to_mail,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_CONFIRMED,
      html: EmailTemService.vendorOrderPlaced(appointments, order),
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live_1 : Slaylewks_details.cc_mail_dev,
      // attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'inovoice.pdf' }],
    };
    await this.sendEmail(mailOptions);
  }
  async sendOrderCancelationMailToUser(to_mail: string, mailData: Record<'order' | 'location', Order & Location>) {
    logger.info(`Send_email_order_cancel_entry_email_id: ${to_mail} `);
    newLogger.info({
      type: 'mail',
      body: { Send_email_order_cancel_entry_email_id: to_mail },
    });
    let mailOptions: MailOptions = {
      to: to_mail,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_CANCELLED,
      html: EmailTemService.cancelationTempUser(mailData),
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live : Slaylewks_details.cc_mail_dev,
      // attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'inovoice.pdf' }],
    };
    this.sendEmail(mailOptions);
  }
  async sendOrderCancelationMailToVendor(to_mail: string, mailData: Record<'order' | 'location', Order & Location>) {
    logger.info(`Send_email_order_cancel_entry_email_id: ${to_mail} `);
    newLogger.info({
      type: 'mail',
      body: { Send_email_order_cancel_entry_email_id: to_mail },
    });
    let mailOptions: MailOptions = {
      to: to_mail,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_CANCELLED,
      html: EmailTemService.cancelationTempVendor(mailData),
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live_1 : Slaylewks_details.cc_mail_dev,
      // attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'inovoice.pdf' }],
    };
    this.sendEmail(mailOptions);
  }
  async OrderRejectedMailVendorUser(contentData: Order[], remarks: string, Customer_email?: string) {
    let branchEmail = contentData[0]?.branch?.branch_email;
    let to = Customer_email ? Customer_email : branchEmail;
    logger.info(`Send_email_order_reject_VendorandUser_entry_email_id: ${branchEmail ? branchEmail : Customer_email} `);
    newLogger.info({
      type: 'mail',
      body: { Send_email_order_reject_VendorandUser_entry_email_id: branchEmail ? branchEmail : Customer_email },
    });
    let html = await EmailTemService.rejectedTemp(contentData, remarks, Customer_email);
    // let invoice: Buffer = await MailUtils.convertHtmlToPdf(html);

    let mailOptions: MailOptions = {
      to: to,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_REJECTED,
      html: html,
      //  attachments: [{ content: invoice.toString('base64'), filename: 'inovoice.pdf' }],
    };

    this.sendEmail(mailOptions);
  }
  async sendOrderRescheduleMail(to_mail: string, confirmationContent: string, inovoice_pdf?: Buffer) {
    logger.info(`Send_email_order_reschedule_entry_email_id: ${to_mail} `);
    newLogger.info({
      type: 'mail',
      body: { Send_email_order_reschedule_entry_email_id: to_mail },
    });
    let mailOptions: MailOptions = {
      to: to_mail,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_RESCHEDULED,
      html: confirmationContent,
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live : Slaylewks_details.cc_mail_dev,
      //  attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'inovoice.pdf' }],
    };
    this.sendEmail(mailOptions);
  }

  async sendOrderRescheduleVendor(email: string, confirmationContent: string, inovoice_pdf?: Buffer) {
    let mailOptions: MailOptions = {
      to: email,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_RESCHEDULED,
      html: confirmationContent,
      // attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'inovoice.pdf' }],
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live_1 : Slaylewks_details.cc_mail_dev,
    };
    this.sendEmail(mailOptions);
  }

  async awatingApprovalVendorMail(email: string, confirmationContent: string) {
    let mailOptions: MailOptions = {
      to: email,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_AWAITING,
      html: confirmationContent,
      // attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'inovoice.pdf' }],
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live_1 : Slaylewks_details.cc_mail_dev,
    };
    await this.sendEmail(mailOptions);
  }

  async awatingApprovalUserMail(email: string, confirmationContent: string) {
    let mailOptions: MailOptions = {
      to: email,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_AWAITING,
      html: confirmationContent,
      // attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'inovoice.pdf' }],
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live_1 : Slaylewks_details.cc_mail_dev,
    };
    await this.sendEmail(mailOptions);
  }

  async sendOrderReminderMail(to_mail: string, appointments: Appointment[], order?: Order) {
    logger.info(`Send_email_order_reminder_entry_email_id: ${to_mail} `);
    newLogger.info({
      type: 'mail',
      body: { Send_email_order_reminder_entry_email_id: to_mail },
    });
    let mailOptions: MailOptions = {
      to: to_mail,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_REMINDER,
      html: EmailTemService.reminderTempUser(appointments, order),
      cc: process.env.NODE_ENV == PRODUCTION ? [] : Slaylewks_details.cc_mail_dev,
      // attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'inovoice.pdf' }],
    };
    await this.sendEmail(mailOptions);
  }
  async sendOrderReminderMailVendor(to_mail: string, appointments: Appointment[], order?: Order) {
    logger.info(`Send_email_order_reminder_entry_email_id: ${to_mail} `);
    newLogger.info({
      type: 'mail',
      body: { Send_email_order_reminder_entry_email_id: to_mail },
    });
    let mailOptions: MailOptions = {
      to: to_mail,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.ORDER_REMINDER,
      html: EmailTemService.reminderTempVendor(appointments, order),
      // attachments: [{ content: inovoice_pdf.toString('base64'), filename: 'inovoice.pdf' }],
      cc: process.env.NODE_ENV == PRODUCTION ? [] : Slaylewks_details.cc_mail_dev,
    };
    this.sendEmail(mailOptions);
  }

  async sendConfirmationOtpToMail(otp: any, email: string) {
    logger.info(`Mail_Sent_Otp_Entry_emailId:${JSON.stringify(email)} ,OTP: ${otp}`);
    newLogger.info({
      type: 'mail',
      body: { Mail_Sent_Otp_Entry_emailId: email, OTP: otp },
    });
    let mailOptions: MailOptions = {
      to: email,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.OTP_VERIFY,
      html: EmailTemService.emailOtpVerification(otp),
    };
    this.sendEmail(mailOptions);
  }
  helpAndSupportContent(createHelpAndSupportDto: CreateHelpAndSupportDto) {
    let email =
      process.env.NODE_ENV === PRODUCTION
        ? Slaylewks_details.support_team_to_email_prod
        : Slaylewks_details.support_team_to_email_dev;
    logger.info(`Send_email_help_and_support_entry_email_id: ${email}`);
    newLogger.info({
      type: 'mail',
      body: { Send_email_order_confirm_entry_email_id: email },
    });
    let mailOptions: MailOptions = {
      to: email,
      subject: EmailTemService.EMAIL_DATA.SUBJECT,
      text: EmailTemService.EMAIL_DATA.TEXT_FOR_SLAYLEWKS,
      html: EmailTemService.helpAndSupportTemplete(createHelpAndSupportDto),
    };
    this.sendEmail(mailOptions);
  }

  async sendEmail(mailOptions: MailOptions) {
    try {
      let { to, subject, text, html, attachments, cc } = mailOptions;
      let mailcredentials: SmtpSettings = await SmtpSettings.findOne({
        attributes: ['mail_username', 'mail_from_address'],
      });
      logger.info('mailcredentials: ' + JSON.stringify(mailcredentials));
      newLogger.info({
        type: 'mail',
        body: { mailcredentials: mailcredentials },
      });
      if (!mailcredentials) throw new Error('mailcredentials is empty');
      let SENDGRID_API_KEY = mailcredentials?.mail_username;
      let SENDER_EMAIL_ADDRESS = mailcredentials?.mail_from_address;
      sgMail.setApiKey(SENDGRID_API_KEY);
      let ccMail = cc && cc?.length ? { cc: cc } : { cc: [] };
      // ? (process.env.NODE_ENV == PRODUCTION
      //   ? { cc: Slaylewks_details.cc_mail_live }
      //   : { cc: Slaylewks_details.cc_mail_dev })
      // : { cc: [] };
      const msg = {
        to: process.env.NODE_ENV == PRODUCTION ? to : Slaylewks_details.to_mail_dev,
        from: SENDER_EMAIL_ADDRESS, // Use the email address or domain you verified above
        subject: subject,
        ...ccMail,
        text: text,
        html: html,
        attachments: attachments,
      };
      let res = await sgMail.send(msg);
      // console.log('---->', res);
      logger.info(`Send_email_successfully_email_id: ${to} ` + JSON.stringify(res));
      newLogger.info({
        type: 'mail',
        body: { Send_email_successfully_email_id_to: res },
      });
    } catch (error) {
      // Logger.error('======>' + error);
      console.log('Send_Email_Error: ' + error);
      logger.error(
        `Send_email_error_email_id: ${mailOptions?.to} ` + JSON.stringify(error?.stack || error?.message || error),
      );
      newLogger.error({
        type: 'mail',
        body: { Send_email_error_email_id: mailOptions?.to, error: error?.stack || error?.message || error },
      });
    }
  }
  static async convertHtmlToPdf(htmlContent: string) {
    try {
      const browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox'],
      });
      const page = await browser.newPage();
      await page.setContent(htmlContent, { waitUntil: 'networkidle0' });
      const pdfBuffer = await page.pdf({ printBackground: true });
      await browser.close();
      return pdfBuffer;
    } catch (error) {
      console.error('Convert Pdf Error:', error);
      logger.error(`Convert_pdf_error: ` + JSON.stringify(error?.stack || error?.message || error));
      newLogger.error({
        type: 'mail',
        body: { Convert_pdf_error: error?.stack || error?.message || error },
      });
    }
  }

  public sendVendorRequestMail(data: VendorEnquiryDto) {
    let email =
      process.env.NODE_ENV === PRODUCTION
        ? Slaylewks_details.support_team_to_email_prod
        : Slaylewks_details.support_team_to_email_dev;
    let mailOptions: MailOptions = {
      to: email,
      subject: EmailTemService.EMAIL_DATA.VENDOR_ENQUIRY_REQUEST,
      text: EmailTemService.EMAIL_DATA.VENDOR_ENQUIRY_REQUEST,
      html: EmailTemService.vendorRequestTemplete(data),
      cc: process.env.NODE_ENV == PRODUCTION ? Slaylewks_details.cc_mail_live_1 : Slaylewks_details.cc_mail_dev,
    };
    this.sendEmail(mailOptions);
  }
}

export interface MailOptions {
  to: string | string[];
  subject: string;
  text: string;
  html: string;
  cc?: string[];
  attachments?: AttachmentOptions[];
}

export interface AttachmentOptions {
  filename: string;
  content: any;
  type?: string;
  disposition?: string;
}
