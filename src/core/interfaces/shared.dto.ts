import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsBoolean,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';
import { BRANCH_ID, VENDOR_ID } from '../constants';

export enum BlockingUserType {
  USER = 'USER'
}

export class SharedDto {

  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional()
  @IsArray()
  branch_ids?: string[];

  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional()
  @IsArray()
  vendor_ids?: string[];

  @ApiProperty({ example: [BRANCH_ID] })
  @IsOptional()
  @IsArray()
  service_ids?: string[];

  @ApiProperty({ example: BRANCH_ID })
  @IsNotEmpty()
  @IsString()
  branch_id?: string;



  @ApiProperty({ example: VENDOR_ID })
  @IsNotEmpty()
  @IsOptional()
  vendor_id?: string;

  @ApiProperty({ example: VENDOR_ID })
  @IsNotEmpty()
  @IsOptional()
  user_id?: string;

  @ApiProperty({ example: 'SLAY3521' })
  @IsNotEmpty()
  @IsOptional()
  reg_id?: string;

  @ApiProperty({ example: '' })
  @IsNotEmpty()
  @IsOptional()
  image?: string | null;

  @ApiProperty({ example: '' })
  @IsNotEmpty()
  @IsString({ each: true })
  service_id: string;

  @ApiProperty({ example: '' })
  @IsNotEmpty()
  @IsString()
  category_id: string;

  @ApiProperty({ example: '' })
  @IsNotEmpty()
  @IsString()
  category_code: string;

  @ApiProperty({ example: '' })
  @IsNotEmpty()
  @IsString()
  service_category_id: string;

  @ApiProperty({ example: ['cec5ca18-0a51-4e80-96a0-0ea1b1d9bd62'] })
  @IsOptional()
  @IsArray()
  marketing_ids?: string[];

  @ApiProperty({ example: '' })
  @IsNotEmpty()
  @IsString()
  service_category_code: string;

  @ApiProperty({ example: 'Best mega offer' })
  @IsString()
  description?: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  amount: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  lat: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  lng: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email_id?: string;

  @ApiProperty()
  // @IsNotEmpty()
  @IsOptional()
  @IsString()
  password?: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name?: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsBoolean()
  is_active: boolean;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumberString()
  mobile_no?: string;
}

export class PaginationDto {
  @ApiProperty({ example: 'Some text' })
  @IsString()
  @IsOptional()
  searchText?: string;

  @ApiProperty({ example: 1 })
  @IsNotEmpty()
  @IsNumber()
  pagNo?: number;

  @ApiProperty({ example: 10 })
  @IsNotEmpty()
  @IsNumber()
  limit?: number;
}
export class BlockingDto {

  @ApiProperty({ example: BRANCH_ID })
  @IsString()
  @IsNotEmpty()
  id?: string;

  @ApiProperty({ example: 'USER' })
  @IsEnum(BlockingUserType)
  @IsNotEmpty()
  type?: string;

  @ApiProperty({ example: true })
  @IsBoolean()
  @IsNotEmpty()
  block?: boolean;
}