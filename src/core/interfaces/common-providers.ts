import { Service } from 'aws-sdk';
import {
  PERMISSION_REPOSITORY,
  ADMIN_NOTIFICATION_REPOSITORY,
  ADMIN_USER_REPOSITORY,
  APP_INFORMATION_REPOSITORY,
  BANNER_REPOSITORY,
  BRANCH_REPOSITORY,
  BRANCH_SERVICE_CATEGORY_REPOSITORY,
  BRANCH_CATEGORY_REPOSITORY,
  BRANCH_SERVICE_TYPE_REPOSITORY,
  SLOT_DETAILS_REPOSITORY,
  VENDOR_REPOSITORY,
  BRANCH_GALLERY_REPOSITORY,
  BRANCH_GALLERY_FOLDER_REPOSITORY,
  COUPON_REPOSITORY,
  CURRENCY_REPOSITORY,
  FOOTER_REPOSITORY,
  LANGUAGE_REPOSITORY,
  LOGO_REPOSITORY,
  MARKETING_REPOSITORY,
  MASTER_SERVICE_CATEGORY_REPOSITORY,
  MASTER_CATEGORY_REPOSITORY,
  NOTIFICATION_CONFIG_REPOSITORY,
  PAYMENT_CONFIG_REPOSITORY,
  PAGES_REPOSITORY,
  ROLE_REPOSITORY,
  ROLE_PERMISSION_REPOSITORY,
  USER_ROLE_REPOSITORY,
  SERVICES_REPOSITORY,
  DISCOUNT_SERVICE_REPOSITORY,
  SITE_MAINTENANCE_REPOSITORY,
  SMS_SETTINGS_REPOSITORY,
  SMTP_SETTINGS_REPOSITORY,
  COUPON_BRANCH_REPOSITORY,
  COUPON_SERVICE_REPOSITORY,
  LOCATION_REPOSITORY,
  USER_REPOSITORY,
  OTP_REPOSTIORY,
  NOTIFICATION_MARKETING_MAPPING,
  REVIEW_REPOSITORY,
  CUSTOMER_DETAILS_REPOSITORY,
  CART_SERVICE_MAPPINGS,
  CHALLANGES_REPOSITORY,
  ORDERS_REPORT_REPOSITORY,
  ORDER_SERVICES_REPO,
  CATEGORY_SERIVICE_CAT_REPOSITORY,
  COUPON_SERVICE_CATEGORY_MAPPING_REPOSITORY,
  COUPON_CATEGORY_MAPPING_REPOSITORY,
  USER_SERVICE,
  SEQUELIZE,
  MASTER_SERVICE_TYPE_REPO,
  NOTIFICATION_SERVICES_REPO,
  REVIEWS_REPLY_REPOSITORY,
} from '../constants';
import AdminNotification from '../database/models/AdminNotification';
import AdminUser from '../database/models/AdminUser';
import AppInformation from '../database/models/AppInformation';
import Banner from '../database/models/Banner';
import Branch from '../database/models/Branch';
import BranchCategoryMapping from '../database/models/BranchCategoryMapping';
import BranchGallery from '../database/models/BranchGallery';
import BranchGalleryFolder from '../database/models/BranchGalleryFolder';
import BranchServiceCategoryMapping from '../database/models/BranchServiceCategoryMapping.ts';
import BranchServiceTypeMapping from '../database/models/BranchServiceTypeMapping';
import Review from '../database/models/Reviews';
import CouponBranchMapping from '../database/models/CouponBranchMapping';
import CouponServiceMapping from '../database/models/CouponServiceMapping';
import Coupon from '../database/models/Coupons';
import Currency from '../database/models/Currency';
import Footer from '../database/models/Footer';
import Language from '../database/models/Langauge';
import Logo from '../database/models/Logo';
import MarketingGroup from '../database/models/MarketingGroup';
import MasterCategory from '../database/models/MasterCategory';
import MasterServiceCategory from '../database/models/MasterServiceCategory';
import PaymentConfig from '../database/models/PaymentConfig';
import Permission from '../database/models/Permission';
import Role from '../database/models/Role';
import RolePermission from '../database/models/RolePermission';
import SiteMaintenance from '../database/models/SiteMaintenance';
import SlotDetail from '../database/models/SlotDetail';
import SmsSettings from '../database/models/SmsSettings';
import SmtpSettings from '../database/models/SmtpSettings';
import UserRole from '../database/models/UserRole';
import Vendor from '../database/models/Vendor';

import Screen from '../database/models/Screen';
import { DiscountService } from '../utils/discount/discount-service';
import User from '../database/models/User';
import Otp from '../database/models/Otp';
import Location from '../database/models/Location';
import NotificationConfig from '../database/models/NotificationConfig';
import NotificationMarketingMapping from '../database/models/NotificationMarketingMapping';
import CartServiceMapping from '../database/models/CartServiceMapping';
import Challenges from '../database/models/Challenges';
import OrderService from '../database/models/OrderServices';
import CategoryServiceCategoryMappings from '../database/models/CategoryServiceCatMapping';
import CouponServiceCategoryMapping from '../database/models/CouponServiceCategoryMapping';
import CouponCategoryMapping from '../database/models/CouponCategoryMapping';
import { UsersService } from 'src/modules/users/users.service';
import { Sequelize } from 'sequelize-typescript';
import MasterServiceType from '../database/models/MasterServiceType';
import { NotificationService } from 'src/modules/notification/notification.service';
import ReviewReply from '../database/models/ReviewReply';



export const commonProvider = {
  sequelize_repo: {
    provide: SEQUELIZE,
    useValue: Sequelize,
  },
  permission_repository: {
    provide: PERMISSION_REPOSITORY,
    useValue: Permission,
  },
  admin_notification_repository: {
    provide: ADMIN_NOTIFICATION_REPOSITORY,
    useValue: AdminNotification,
  },
  admin_user_repository: {
    provide: ADMIN_USER_REPOSITORY,
    useValue: AdminUser,
  },
  app_information_repository: {
    provide: APP_INFORMATION_REPOSITORY,
    useValue: AppInformation,
  },
  auth_repository: {
    provide: ADMIN_USER_REPOSITORY,
    useValue: AdminUser,
  },
  banner_repository: {
    provide: BANNER_REPOSITORY,
    useValue: Banner,
  },
  branch_repository: {
    provide: BRANCH_REPOSITORY,
    useValue: Branch,
  },
  users_service: {
    provide: USER_SERVICE,
    useValue: UsersService,
  },
  branch_service_category_repository: {
    provide: BRANCH_SERVICE_CATEGORY_REPOSITORY,
    useValue: BranchServiceCategoryMapping,
  },
  master_service_type_repository: {
    provide: MASTER_SERVICE_TYPE_REPO,
    useValue: MasterServiceType,
  },
  branch_category_repository: {
    provide: BRANCH_CATEGORY_REPOSITORY,
    useValue: BranchCategoryMapping,
  },
  branch_service_type_repository: {
    provide: BRANCH_SERVICE_TYPE_REPOSITORY,
    useValue: BranchServiceTypeMapping,
  },
  slot_details_repository: {
    provide: SLOT_DETAILS_REPOSITORY,
    useValue: SlotDetail,
  },
  vendor_repository: {
    provide: VENDOR_REPOSITORY,
    useValue: Vendor,
  },

  branch_gallary_repository: {
    provide: BRANCH_GALLERY_REPOSITORY,
    useValue: BranchGallery,
  },
  branch_gallery_folder_repository: {
    provide: BRANCH_GALLERY_FOLDER_REPOSITORY,
    useValue: BranchGalleryFolder,
  },

  branch_slot_details_repository: {
    provide: SLOT_DETAILS_REPOSITORY,
    useValue: SlotDetail,
  },
  review_repository: {
    provide: REVIEW_REPOSITORY,
    useValue: Review,
  },
  coupon_repository: {
    provide: COUPON_REPOSITORY,
    useValue: Coupon,
  },
  coupon_branch_repository: {
    provide: COUPON_BRANCH_REPOSITORY,
    useValue: CouponBranchMapping,
  },
  coupon_service_repository: {
    provide: COUPON_SERVICE_REPOSITORY,
    useValue: CouponServiceMapping,
  },
  coupon_category_mapping_repository: {
    provide: COUPON_CATEGORY_MAPPING_REPOSITORY,
    useValue: CouponCategoryMapping,
  },
  coupon_service_category_mapping_repository: {
    provide: COUPON_SERVICE_CATEGORY_MAPPING_REPOSITORY,
    useValue: CouponServiceCategoryMapping,
  },
  currency_repository: {
    provide: CURRENCY_REPOSITORY,
    useValue: Currency,
  },

  footer_repository: {
    provide: FOOTER_REPOSITORY,
    useValue: Footer,
  },
  language_repository: {
    provide: LANGUAGE_REPOSITORY,
    useValue: Language,
  },
  logo_repository: {
    provide: LOGO_REPOSITORY,
    useValue: Logo,
  },
  marketing_group_repository: {
    provide: MARKETING_REPOSITORY,
    useValue: MarketingGroup,
  },

  branch_catagory_repository: {
    provide: BRANCH_CATEGORY_REPOSITORY,
    useValue: BranchCategoryMapping,
  },

  master_service_category_repository: {
    provide: MASTER_SERVICE_CATEGORY_REPOSITORY,
    useValue: MasterServiceCategory,
  },
  master_category_repository: {
    provide: MASTER_CATEGORY_REPOSITORY,
    useValue: MasterCategory,
  },
  category_service_cat_repo: {
    provide: CATEGORY_SERIVICE_CAT_REPOSITORY,
    useValue: CategoryServiceCategoryMappings,
  },

  notification_config_repository: {
    provide: NOTIFICATION_CONFIG_REPOSITORY,
    useValue: NotificationConfig,
  },
  payment_config_method: {
    provide: PAYMENT_CONFIG_REPOSITORY,
    useValue: PaymentConfig,
  },
  page_repository: {
    provide: PAGES_REPOSITORY,
    useValue: Screen,
  },

  role_repository: {
    provide: ROLE_REPOSITORY,
    useValue: Role,
  },
  role_permission_repository: {
    provide: ROLE_PERMISSION_REPOSITORY,
    useValue: RolePermission,
  },
  user_role_repository: {
    provide: USER_ROLE_REPOSITORY,
    useValue: UserRole,
  },

  service_repository: {
    provide: SERVICES_REPOSITORY,
    useValue: Service,
  },
  discount_service_repository: {
    provide: DISCOUNT_SERVICE_REPOSITORY,
    useValue: DiscountService,
  },

  site_maintenance_repository: {
    provide: SITE_MAINTENANCE_REPOSITORY,
    useValue: SiteMaintenance,
  },
  sms_settings_repository: {
    provide: SMS_SETTINGS_REPOSITORY,
    useValue: SmsSettings,
  },
  smtp_settings_repository: {
    provide: SMTP_SETTINGS_REPOSITORY,
    useValue: SmtpSettings,
  },
  location_repo: {
    provide: LOCATION_REPOSITORY,
    useValue: Location,
  },
  user_repo: {
    provide: USER_REPOSITORY,
    useValue: User,
  },
  cart_service_repo: {
    provide: CART_SERVICE_MAPPINGS,
    useValue: CartServiceMapping,
  },
  otp_repo: {
    provide: OTP_REPOSTIORY,
    useValue: Otp,
  },
  notification_marketing_mapping: {
    provide: NOTIFICATION_MARKETING_MAPPING,
    useValue: NotificationMarketingMapping,
  },
  customer_details_repository: {
    provide: CUSTOMER_DETAILS_REPOSITORY,
    useValue: User,
  },
  challanges_repository: {
    provide: CHALLANGES_REPOSITORY,
    useValue: Challenges
  },
  order_services_repo: {
    provide: ORDER_SERVICES_REPO,
    useValue: OrderService,
  },
  notification_service_repo: {
    provide: NOTIFICATION_SERVICES_REPO,
    useValue: NotificationService,
  },

  review_reply_repository: {
    provide: REVIEWS_REPLY_REPOSITORY,
    useValue: ReviewReply,
  },

  // orders_report_repository:{
  //   provide:ORDERS_REPORT_REPOSITORY,
  //   useValue:OrdersReport
  // }
};
