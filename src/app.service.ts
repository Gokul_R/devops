import { Injectable } from '@nestjs/common';
import { DEVICE_TYPE, GetForceUpdate } from './modules/shared/dto/forceUpdate.dto';
import AppInformation from './core/database/models/AppInformation';

@Injectable()
export class AppService {
  [x: string]: any;
  getHello(): string {
    return 'Hello World!';
  }
  // AWS.config.update({ region: awsConfig.region });

  async getForceUpdateVersion(req_body: GetForceUpdate) {
    let data: any;
    data = await AppInformation.findOne({
      attributes: ['android_version', 'ios_version', 'vendor_android_version', 'vendor_ios_version'],
      order: [['updated_at', 'DESC']]
    });
    switch (req_body.device_type) {
      case 'Android':
        return { device_type: DEVICE_TYPE.ANDROID, version: data?.dataValues?.android_version };
        break;
      case 'iOS':
        return { device_type: DEVICE_TYPE.IOS, version: data?.dataValues?.ios_version };
        break;
      case 'Vendor_Android':
        return { device_type: DEVICE_TYPE.VendorAndroid, version: data?.dataValues?.vendor_android_version };
        break;
      case 'Vendor_iOS':
        return { device_type: DEVICE_TYPE.VendorIos, version: data?.dataValues?.vendor_ios_version };
        break;
      default:
        return null;
        break;
    }
  }

}
