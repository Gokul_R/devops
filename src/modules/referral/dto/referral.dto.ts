import { IsDefined, IsString } from "class-validator";

export class ReferralDto {
    @IsDefined()
    @IsString()
    referral_code: string;
    @IsDefined()
    @IsString()
    user_id: string;
}