import { Injectable } from '@nestjs/common';
import { Sequelize } from 'sequelize-typescript';
import {
  E0,
  EC200,
  EC422,
  EM106,
  EM122,
  EM422,
  Referral_Pionts,
} from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import Helpers from 'src/core/utils/helpers';
import User from 'src/core/database/models/User';
import ReferralHistroy from 'src/core/database/models/ReferralHistroy';
import { USER_ATTRIBUTES } from 'src/core/attributes/user';

@Injectable()
export class ReferralService {
  /** get referral list function written by sasi 2023-05-11 */
  async findAll(user_id: string) {
    let referralQry = {
      where: { user_id: user_id },
      include: [
        {
          attributes: [
            ['id', 'user_id'],
            USER_ATTRIBUTES.USER_NAME,
            USER_ATTRIBUTES.PROFILE_URL,
          ],
          model: User,
        },
      ],
    };
    let referralData = await Helpers.collectionFindAndCountAll(
      ReferralHistroy,
      referralQry,
    );
    return HandleResponse.buildSuccessObj(
      EC200,
      referralData?.message,
      referralData,
    );
  }

  /** check and update referral coins function written by sasi 2023-05-11 */
  async checkReferral(req_data: any) {
    let where_data = { where: { referral_code: req_data.referral_code } };
    const check_referral = await Helpers.findAll(User, where_data);

    if (
      (!check_referral &&
        !check_referral[0].dataValues &&
        !check_referral[0].dataValues.referral_code) ||
      check_referral[0].dataValues.referral_code != req_data.referral_code
    )
      return HandleResponse.buildErrObj(EC422, EM422, EM422);

    const updateReferralCoins = await User.update(
      {
        earn_coins: Sequelize.literal(
          `earn_coins + ${Referral_Pionts._referralPoint}`,
        ),
      },
      { where: { id: check_referral[0].dataValues.id } },
    );

    const insertReferralHistroy = await Helpers.create(ReferralHistroy, {
      user_id: check_referral[0].dataValues.id,
      referral_user_id: req_data.user_id,
      earn_coins: Referral_Pionts._referralPoint,
    });

    return HandleResponse.buildSuccessObj(EC200, EM122, []);
  }
}
