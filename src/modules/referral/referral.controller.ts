import { Controller, Get, Post, Param, Body, Patch, Query } from "@nestjs/common";
import { ReferralService } from "./referral.service";
import { ReferralDto } from "./dto/referral.dto";
import { ApiTags } from "@nestjs/swagger";
@Controller('referral')
@ApiTags("Referral")
export class ReferralController {

    constructor(private readonly referalService: ReferralService) { };

    /** get referral list function written by sasi 2023-05-11 */
    @Get(':user_id')
    findAll(@Param('user_id') user_id: string) {
        return this.referalService.findAll(user_id);
    }

    /** check and update referral coins function written by sasi 2023-05-11 */
    @Post()
    checkReferral(@Body() referal: ReferralDto) {
        return this.referalService.checkReferral(referal);
    }

}