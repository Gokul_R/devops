import { Inject, Injectable, NotFoundException, forwardRef } from '@nestjs/common';
import { Op, Sequelize, literal } from 'sequelize';
import { ModelCtor } from 'sequelize-typescript';
import { BaseService } from 'src/base.service';
import { UsersService } from '../users/users.service';
import User from 'src/core/database/models/User';
import { Errors } from 'src/core/constants/error_enums';
import { SendPushNotification } from 'src/core/utils/sendPushNotification';
import { CreateNotificationDto, UpdateNotification } from './dto/create-notification.dto';
import UserNotification from 'src/core/database/models/UserNotification';
import sequelize from 'sequelize';
import { PRODUCTION, Slaylewks_details } from 'src/core/constants';

@Injectable()
export class NotificationService extends BaseService<UserNotification> {
  protected model: ModelCtor<UserNotification>;
  constructor() {
    super();
    this.model = UserNotification;
  }
  async findAllNotifications(user_id: string, page_no: number) {
    let user: User = await User.findOne({ where: { id: user_id } });
    if (!user) throw new NotFoundException(Errors.INVALID_USER_DETAILS);
    let pageNo = page_no ? (page_no - 1) * 10 : 1;
    let offset = pageNo;
    let limit = 10;
    const data: any = await UserNotification.findAndCountAll({
      attributes: {
        include: [
          [
            literal(`
              COALESCE(viewers::text[], ARRAY[]::text[]) @> ARRAY['${user_id}'::text]
            `),
            'isViewed',
          ],
          [Sequelize.literal(` (SELECT COUNT(id)::integer FROM "user_notification"
            WHERE NOT COALESCE(viewers, ARRAY[]::text[]) @> ARRAY['${user_id}']::text[]
            AND  '${user_id}' = ANY(user_id) AND deleted_at is NULL
            )
          `), 'not_viewed'],
        ],
        exclude: ['user_id', 'viewers'],
      },
      where: {
        user_id: {
          [Op.contains]: [user_id],
        },
      },
      order: [['created_at', 'DESC']],
      offset: offset,
      limit: limit,
    });
    return { count: data?.count, not_viewed: data?.rows[0]?.dataValues?.not_viewed || 0, rows: data?.rows };
  }

  /** create notification function written by sasi 2023-05-10 */
  async create(req_data: CreateNotificationDto) {
    // let notificationData = await super.create(req_data);
    return await this.sendPushNotification(req_data);
    // return notificationData;
  }

  async sendPushNotification(createAdminNotificationDto: CreateNotificationDto) {
    let requestObj = {
      to: createAdminNotificationDto.device_token,
      // registration_ids: createAdminNotificationDto.device_token,
      notification: {
        title: createAdminNotificationDto.title,
        body: createAdminNotificationDto.description,
        // image: createAdminNotificationDto.image
        image: createAdminNotificationDto?.image
          ? `${Slaylewks_details?.s3base_url}${process.env.NODE_ENV != PRODUCTION ? 'dev/' : 'prod/'}${createAdminNotificationDto?.image
          }`
          : `${Slaylewks_details?.logo}`,
      },
    };
    const sendNotification = new SendPushNotification().sendNotification(requestObj);
  }

  /** update view status notification function written by sasi 2023-05-10 */
  async updateView(req_data: UpdateNotification) {
    const { id, user_id } = req_data;
    let notificationData = await UserNotification.update(
      { viewers: sequelize.fn('array_append', sequelize.col('viewers'), user_id) },
      {
        where: { id: id },
      },
    );
    return notificationData;
  }

  async removeAll(user_id: string): Promise<any> {
    const deleteNotify = await UserNotification.update(
      {
        user_id: sequelize.literal(`array_remove("user_id", '${user_id}')`),
      },
      {
        where: {
          user_id: {
            [Op.contains]: [user_id],
          },
        },
      },
    );
    return;
  }

  async unseenNotificationsCount(user_id: string): Promise<number> {
    const unseenCount: number = await UserNotification.count({
      where: {
        user_id: { [Op.contains]: [user_id] },
        viewers: {
          [Op.or]: [literal(`NOT '${user_id}' = ANY("UserNotification"."viewers"::text[])`), { [Op.is]: null }],
        },
      },
    });
    return unseenCount;
  }
}
