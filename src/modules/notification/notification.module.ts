import { Module, forwardRef } from '@nestjs/common';
import { NotificationController } from './notification.controller';
import { NotificationService } from './notification.service';
import { UsersService } from '../users/users.service';
import { UsersModule } from '../users/users.module';
import { usersProviders } from '../users/users.providers';

@Module({
  imports: [forwardRef(() => UsersModule)],
  controllers: [NotificationController],
  providers: [NotificationService,...usersProviders],
  exports: [NotificationService],
})
export class NotificationModule {}
