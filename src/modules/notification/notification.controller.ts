import { Controller, Get, Post, Param, Body, Patch, Query, Delete } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { CreateNotificationDto, UpdateNotification } from './dto/create-notification.dto';
import { ApiTags } from '@nestjs/swagger';
import { EC200, EM106, EM116, EM127 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import UserNotification from 'src/core/database/models/UserNotification';
@Controller('notification')
@ApiTags('Notification')
export class NotificationController {
  constructor(private readonly notificationService: NotificationService) {
    // super(notificationService);
  }

  @Post()
  async create(@Body() createAdminNotificationDto: CreateNotificationDto) {
    try {
      let notificationData = await this.notificationService.create(createAdminNotificationDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, notificationData);
    } catch (error) {
      return HandleResponse.buildErrObj(error.status || null, error.message, error);
    }
  }

  @Get(':user_id/:page_no')
  async findAllNotifications(@Param('user_id') user_id: string, @Param('page_no') page_no: number) {
    try {
      let notificationData = await this.notificationService.findAllNotifications(user_id, page_no);
      return HandleResponse.buildSuccessObj(EC200, EM106, notificationData);
    } catch (error) {
      return HandleResponse.buildErrObj(error.status || null, error.message, error);
    }
  }

  @Patch()
  async updateView(@Body() updateNotification: UpdateNotification) {
    //  return this.notificationService.updateView(updateNotification);
    try {
      let notificationData = await this.notificationService.updateView(updateNotification);
      return HandleResponse.buildSuccessObj(EC200, EM116, notificationData);
    } catch (error) {
      return HandleResponse.buildErrObj(error.status || null, error.message, error);
    }
  }
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<any> {
    try {
      let notificationData = await UserNotification.destroy({ where: { id: id } });
      return HandleResponse.buildSuccessObj(EC200, EM127, notificationData);
    } catch (error) {
      return HandleResponse.buildErrObj(error.status || null, error.message, error);
    }
  }

  @Delete('by-user/:user_id')
  async removeAllByUser(@Param('user_id') user_id: string): Promise<any> {
    try {
      let notificationData = await this.notificationService.removeAll(user_id);
      return HandleResponse.buildSuccessObj(EC200, EM127, notificationData);
    } catch (error) {
      return HandleResponse.buildErrObj(error.status || null, error.message, error);
    }
  }
}
