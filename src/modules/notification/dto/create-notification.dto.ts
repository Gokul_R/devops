import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsDefined, IsNotEmpty, IsString } from "class-validator";
import { IsOptional, Length } from "nestjs-class-validator";

export class CreateNotificationDto {
    @ApiProperty({ example: ' https//17.54.23:4000/235.img' })
    @IsString()
    @IsOptional()
    image?: string | null;

    @ApiProperty({ example: 'Lorem Ipsum' })
    @IsString()
    title?: string;

    @ApiProperty({ example: 'Lorem Ipsum' })
    @IsString()
    @IsOptional()
    description?: string;

    @IsDefined()
    @IsString()
    user_id: string;
    
    @IsString()
    device_token?: string;

}

export class UpdateNotification {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    id: string;
    
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    user_id: string;
}

