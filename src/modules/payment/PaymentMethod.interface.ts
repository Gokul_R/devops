import PaymentConfig from 'src/core/database/models/PaymentConfig';
import { CreateOrderDto } from '../orders/dto/create-order.dto';
import Payment_Txn from 'src/core/database/models/Payment-Tnx';

// payment-method.interface.ts
export interface PaymentMethod {
  init(payment_config: PaymentConfig): void;
  createOrder(req_data: CreateOrderDto, receipt_id?: string, freelancerDueOrderCreate?: boolean): Promise<any>;
  paymentStatus(paymentId: string): Promise<any>;
  initiateRefund(paymentId: string): Promise<any>;
  calculatePayout(req_data: CreateOrderDto,Payment_Txn?:Payment_Txn): Promise<any>;
  insertPayTnx(order: any, req_data: CreateOrderDto): Promise<any>;
}
