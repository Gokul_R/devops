import { Injectable } from '@nestjs/common';
import axios from 'axios';
import moment from 'moment';
import Razorpay from 'razorpay';
import { RefundAmountTryAgain } from 'src/admin/payout/dto/view-payout.dto';
import { InstantRefund } from 'src/admin/payout/payout.interfaces';
import { razorPayAPIs } from 'src/core/constants/appConfig';
import Order from 'src/core/database/models/Order';
import Payment_Txn from 'src/core/database/models/Payment-Tnx';
import PaymentConfig from 'src/core/database/models/PaymentConfig';
import PayoutRefundTnx, { PayOutRefund } from 'src/core/database/models/PayoutRefundTnx';
import { logger } from 'src/core/utils/logger';
import { newLogger } from 'src/core/utils/loggerNew';

@Injectable()
export class paymentRefundService {
  private payment_config: PaymentConfig;
  private razor_instance;
  constructor() {}

  init(config: PaymentConfig) {
    this.razor_instance = new Razorpay({
      key_id: config.razorpay_key,
      key_secret: config.razorpay_secret,
    });
  }

  async instantRefundAmountToUser(req_body: RefundAmountTryAgain) {
    try {
      await this.setupPayment();
      // logger.info('Cancelled_RefundAmount_entry: ' + JSON.stringify(req_body));
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'payment',
      //   body: { Cancelled_RefundAmount_entry: req_body },
      // });
      let req_obj: InstantRefund = {
        receipt: `ref#${req_body.order_id + '_' + moment().utc().unix()}`, // send order id for our reference
        speed: 'normal', // 'normal', // optimum
        notes: {
          order_id: req_body.order_id,
          user_id: req_body.user_id,
        },
      };
      // If the amount parameter is passed, send the incoming amount. Otherwise full amount will be refunded
      if (req_body?.amount && req_body?.amount >= 100 && !req_body?.full_refund) req_obj['amount'] = req_body.amount;

      const instantRefund = await this.apiCallFromAxios(
        `${razorPayAPIs.instantRefund}/${req_body.payment_id}/refund`,
        req_obj,
      );
      logger.info('Cancelled_RefundAmount_razorpay_exit: ' + JSON.stringify(instantRefund?.data));
      newLogger.info({
        url: '/api/v1/orders',
        type: 'razorpay',
        body: { req_obj, req_body },
        response: { Cancelled_RefundAmount_razorpay_exit: instantRefund?.data },
      });
      const updateRefundStatus = await this.updateRefundDataToTables(req_body, instantRefund);
      return instantRefund?.data;
    } catch (error) {
      logger.error('Cancelled_RefundAmount_error: ' + JSON.stringify(error));
      newLogger.error({
        url: '/api/v1/orders',
        type: 'payment',
        body: { req_body },
        response: { Cancelled_RefundAmount_error: error },
      });
      throw new Error(error?.response?.data?.error?.description || error);
    }
  }

  private async setupPayment() {
    this.payment_config = await PaymentConfig.findOne();
    logger.info('payment_config_entry: ' + JSON.stringify(this.payment_config));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'payment',
    //   body: { payment_config_entry: this.payment_config },
    // });
    if (!this.payment_config) throw Error('No payment method configured');
    if (this.payment_config.razorpay_key && this.payment_config.razorpay_secret) return this.init(this.payment_config);
    else throw Error('Razorpay credentials not configured!!');
  }

  // api call to razorpay
  private async apiCallFromAxios(url: string, body: any) {
    const result = await axios.post(url, body, {
      auth: {
        username: this.payment_config.razorpay_key,
        password: this.payment_config.razorpay_secret,
      },
      // maxContentLength: 100 * 1024 * 1024,
      headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
      maxContentLength: Infinity,
      maxBodyLength: Infinity,
    });
    return result;
  }

  // refund status update the tables
  private async updateRefundDataToTables(req_body: RefundAmountTryAgain, instantRefund: any) {
    logger.info('Cancelled_RefundAmount_updatedTable_entry: ' + JSON.stringify(req_body));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'payment',
    //   body: { Cancelled_RefundAmount_updatedTable_entry: req_body },
    // });
    let obj = {
      user_id: req_body.user_id,
      order_id: req_body.order_id,
      tnx_type: PayOutRefund.Refund,
      payout_id: instantRefund?.data?.id,
      payout_amount: instantRefund?.data?.amount,
      status: instantRefund?.data?.status,
      request_response: instantRefund?.data,
      merchant_id: instantRefund?.data?.merchant_id,
      utr: instantRefund?.data?.utr,
      reference_id: instantRefund?.data?.receipt,
    };
    let payoutTnxTableData: any;
    if (req_body?.payout_id)
      payoutTnxTableData = await PayoutRefundTnx.update(obj, {
        where: { payout_id: req_body.payout_id, reference_id: req_body.reference_id },
      });
    else payoutTnxTableData = await PayoutRefundTnx.create(obj);
    const updatePaymentTnx = await Payment_Txn.update(
      { refund_status: instantRefund?.data?.status, amount_refunded: req_body.amount },
      { where: { order_id: req_body.order_id, user_id: req_body.user_id } },
    );

    const updateOrderData = await Order.update(
      { refund_status: instantRefund?.data?.status, refund_amount: instantRefund?.data?.amount, refunded: true },
      { where: { order_id: req_body.order_id, user_id: req_body.user_id } },
    );
    logger.info('Cancelled_RefundAmount_updatedTable_exit: ' + JSON.stringify(req_body));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'payment',
    //   body:{req_body,instantRefund},
    //   response: { Cancelled_RefundAmount_updatedTable: req_body },
    // });
    return;
  }
}
