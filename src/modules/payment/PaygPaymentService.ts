// paypal-payment.service.ts
import { Injectable } from '@nestjs/common';
import { PaymentMethod } from './PaymentMethod.interface';
import { CreateOrderDto } from '../orders/dto/create-order.dto';
import { HttpUtils } from 'src/core/utils/http-utils';
import { CREATE, DETAIL, EM100, PAYG_URL, Provided_By, Slaylewks_details } from 'src/core/constants';
import Helpers from 'src/core/utils/helpers';
import Payment_Txn from 'src/core/database/models/Payment-Tnx';
import { PaygPaymentStatus } from 'src/core/utils/enum';
import { paygConfig } from 'src/core/constants/appConfig';
import encode from 'nodejs-base64-encode';
import PaymentConfig from 'src/core/database/models/PaymentConfig';
import { CouponService } from '../coupon/coupon.service';
import { BranchService } from '../shop/branch.service';
import { CommissionType } from 'src/admin/branch/dto/create-branch.dto';
import {
  CouponDiscountType,
} from 'src/admin/coupon/dto/create-coupon.dto';
import Coupon from 'src/core/database/models/Coupons';
import Branch from 'src/core/database/models/Branch';

@Injectable()
export class PayGPaymentService implements PaymentMethod {
  headersRequest: any;
  constructor(
    private readonly couponService: CouponService,
    private readonly branchService: BranchService,
    private readonly httpUtils: HttpUtils,
  ) { }

  init(payment_config: PaymentConfig) {
    let { authentication_key, authentication_token, merchant_key_id } =
      payment_config;
    let headerurl = `${authentication_key || paygConfig.payg.AuthenticationKey
      }:${authentication_token || paygConfig.payg.AuthenticationToken}:M:${merchant_key_id || paygConfig.payg.MerchantKeyId
      }`;
    let encode_header_credentail = encode.encode(headerurl, 'base64');

    this.headersRequest = {
      'Content-Type': 'application/json',
      // Authorization: `Basic ${encode_header_credentail}`,
      Authorization: `Basic YjYwYmU1NzQwMTg5NDY3OWIyNGQ0ZDgwZDljNGE1OWM6ODQ2MjhiNWRmYmU3NDdkYjk0MjFhMTFkZTg5MTYxM2E6TTo4Nzky`,
    };
  }
  async calculatePayout(req_data: CreateOrderDto): Promise<any> {
    let { applied_coupon_id, final_price, branch_id } = req_data;
    let coupon: Coupon | null;
    if (applied_coupon_id) {
      coupon = await this.couponService.findOne(applied_coupon_id);
    } else coupon = null;
    let branch: Branch = await this.branchService.findOne(branch_id);
    if (!branch) throw Error('Invalid Branch Details');
    let { commission_type, payment_commission } = branch;
    let payout_amount: number;
    let discount: number;

    let commission =
      commission_type === CommissionType.PERCENTAGE
        ? final_price * (payment_commission / 100)
        : payment_commission;

    let commission_gst = commission * Slaylewks_details.gst;

    let razor_commission = final_price * Slaylewks_details.transaction_fees;

    if (coupon && coupon.provided_by === Provided_By.SUPER_ADMIN) {
      let { discount_type, discount_value } = coupon;
      discount =
        discount_type == CouponDiscountType.PERCENTAGE
          ? final_price * (discount_value / 100)
          : discount_value;
    } else discount = 0;

    payout_amount =
      final_price + discount - (commission + commission_gst + razor_commission);
    return payout_amount;
  }

  async createOrder(req_data: CreateOrderDto): Promise<any> {
    const request = this.prepareRequest(req_data);
    let order = await this.httpUtils.post(
      PAYG_URL + CREATE,
      request,
      this.headersRequest,
    );
    const payment_tnx = await this.insertPayTnx(order.data, req_data);
    return { payment_order: order.data, payment_tnx: payment_tnx };
  }
  async paymentStatus(paymentId: string): Promise<any> {
    let request = {
      OrderKeyId: paymentId,
      MerchantKeyId: paygConfig.payg.MerchantKeyId,
      PaymentType: '',
    };
    let order = await this.httpUtils.post(
      PAYG_URL + DETAIL,
      request,
      this.headersRequest,
    );
    return order.data;
  }
  async insertPayTnx(order: any, req_data: CreateOrderDto): Promise<any> {
    //insert payment transaction details
    const insertPaymentTnx = await Helpers.create(Payment_Txn, {
      id: order.OrderKeyId,
      order_id: req_data.order_id,
      amount: order.OrderAmount,
      status: PaygPaymentStatus[order.OrderPaymentStatus],
      ...order,
      ...req_data,
    });
    return insertPaymentTnx;
  }

  async initiateRefund(paymentId: string): Promise<any> {
    try {
      let request: TransactionRequest = {
        OrderKeyId: paymentId,
        MerchantKeyId: paygConfig.payg.MerchantKeyId,
        PaymentType: '',
        PaymentTransactionId: '',
        RefundAmount: '',
      };
      let order = await this.httpUtils.post(
        PAYG_URL + DETAIL,
        request,
        this.headersRequest,
      );
      return order.data;
    } catch (error) {
      console.error('Error at refund initialization ', error.message);
      throw Error('Error at refund initialization ' + error?.message);
    }
  }

  prepareRequest(req_data: CreateOrderDto) {
    let { final_price, user_id, customer_mobile } = req_data;

    let ProductData: ProductData = {
      PaymentReason: 'OnlineOrder for OrderNo- 1234',
      ItemId: 'T-shirt',
      Size: 'medium',
      AppName: 'Slaylewks',
    };
    let CustomerData: CustomerData = {
      CustomerId: user_id,
      MobileNo: customer_mobile,
    };
    let IntegrationData: IntegrationData = {
      Source: 'MobileSDK',
      IntegrationType: '',
    };
    let request: TransactionRequest = {
      Merchantkeyid: paygConfig.payg.MerchantKeyId,
      OrderAmount: final_price + '',
      UniqueRequestId: Helpers.getRandomString(10, Helpers.lowercaseLetters),
      CustomerData: CustomerData,
      ProductData: JSON.stringify(ProductData),
      IntegrationData: IntegrationData,
    };
    return request;
  }
}
