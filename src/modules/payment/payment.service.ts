const Razorpay = require('razorpay');
import { PaymentMethod } from './PaymentMethod.interface';
import { CreateOrderDto } from '../orders/dto/create-order.dto';
import Payment_Txn from 'src/core/database/models/Payment-Tnx';
import Helpers from 'src/core/utils/helpers';
import { CouponService } from '../coupon/coupon.service';
import Coupon from 'src/core/database/models/Coupons';
import { CouponDiscountType } from 'src/admin/coupon/dto/create-coupon.dto';
import { BranchService } from '../shop/branch.service';
import Branch from 'src/core/database/models/Branch';
import { CommissionType } from 'src/admin/branch/dto/create-branch.dto';
import { Body, Injectable } from '@nestjs/common';
import PaymentConfig from 'src/core/database/models/PaymentConfig';
import { Provided_By, Slaylewks_details } from 'src/core/constants';
import { logger } from 'src/core/utils/logger';
import PartialPayment from 'src/core/database/models/PartialPayments';
import { newLogger } from 'src/core/utils/loggerNew';
@Injectable()
export class RazorPaymentService implements PaymentMethod {
  private razor_instance;
  constructor(private readonly couponService: CouponService, private readonly branchService: BranchService) {}

  init(payment_config: PaymentConfig) {
    let { razorpay_key, razorpay_secret } = payment_config;
    this.razor_instance = new Razorpay({
      key_id: razorpay_key,
      key_secret: razorpay_secret,
    });
  }

  async calculatePayout(req_data: CreateOrderDto, payment_tnx?: any): Promise<any> {
    logger.info(`Calculate_Payout_Entry: ` + JSON.stringify(req_data));
    // newLogger.info({
    //   url: '/api/v1/orders/create',
    //   type: 'payment',
    //   body: { Calculate_Payout_Entry: req_data },
    // });
    let { applied_coupon_id, final_price, branch_id, original_price, coupon_discount, discount, vendor_type } =
      req_data;
    let coupon: Coupon | null;
    if (applied_coupon_id) {
      coupon = await this.couponService.findOne(applied_coupon_id);
    } else coupon = null;
    let branch: Branch = await this.branchService.findOne(branch_id);
    if (!branch) throw Error('Invalid Branch Details');
    let { commission_type, payment_commission } = branch;
    let payout_amount: number;
    let slaylewks_revenue: number;
    let coupon_admin_discount: number;
    original_price -= discount;
    let commission =
      commission_type === CommissionType.PERCENTAGE ? original_price * (payment_commission / 100) : payment_commission;
    if (commission >= 1000) {
      commission = 1000;
    } else {
      commission = commission;
    }
    let commission_gst = commission * Slaylewks_details.gst;
    if (payment_tnx?.notes?.partialPayment) {
      commission = 0;
      commission_gst = 0;
      coupon = null;
    }

    let razor_commission = final_price * Slaylewks_details.transaction_fees;
    if (coupon && coupon.provided_by === Provided_By.SUPER_ADMIN) {
      coupon_admin_discount = coupon_discount;
    } else coupon_admin_discount = 0;

    payout_amount = final_price + coupon_admin_discount - (commission + commission_gst + razor_commission);
    slaylewks_revenue =
      coupon_admin_discount === 0
        ? commission + commission_gst + razor_commission
        : coupon_admin_discount - (commission + commission_gst + razor_commission);
    logger.info(`Calculate_Payout_Exit_Payout_Amount ` + JSON.stringify(payout_amount.toFixed(2)));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'payment',
    //   response: { Calculate_Payout_Exit_Payout_Amount: payout_amount.toFixed(2) },
    // });
    return { payout_amount: payout_amount.toFixed(2), slaylewks_revenue: slaylewks_revenue.toFixed(2) };
  }

  async createOrder(req_data: CreateOrderDto, receipt_id: string, freelancerDueOrderCreate: boolean): Promise<any> {
    logger.info(`Create_Order_Entry_ReceiptId ` + JSON.stringify(receipt_id + 'data' + JSON.stringify(req_data)));
    // newLogger.info({
    //   url: '/api/v1/orders/create',
    //   type: 'payment',
    //   body: { Create_Order_Entry_ReceiptId: receipt_id, data: req_data },
    // });
    let notes = freelancerDueOrderCreate
      ? { notes: { vendor_type: req_data.vendor_type, partialPayment: true, order_id: req_data.order_id } }
      : { notes: { vendor_type: req_data.vendor_type, partialPayment: false, order_id: req_data.order_id } };
    let payload = {
      amount: (req_data.final_price * 100).toFixed(),
      receipt: receipt_id,
      currency: 'INR',
      ...notes,
    };
    let order = await this.razor_instance.orders.create(payload);
    newLogger.info({
      url: '/api/v1/orders/create',
      type: 'razorPay',
      body: payload,
      response: order,
    });
    const payment_tnx = await this.insertPayTnx(order, req_data);
    logger.info(`Create_Order_Exit_payment_order` + JSON.stringify(payment_tnx));

    return { payment_order: order, insertPaymentTnx: payment_tnx };
  }
  async paymentStatus(order_id: string): Promise<any> {
    const paymentStatus = await this.razor_instance.orders.fetch(order_id);
    return paymentStatus;
  }
  async insertPayTnx(order: any, req_data: CreateOrderDto): Promise<any> {
    logger.info(`Create_Order_insertPayTnx_Entry_Order ` + JSON.stringify(order));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'payment',
    //   body: { Create_Order_insertPayTnx_Entry_Order: order },
    // });
    //insert payment transaction details
    const insertPaymentTnx = await Helpers.create(Payment_Txn, {
      ...order,
      ...req_data,
    });
    logger.info(`Create_Order_insertPayTnx_Exit ` + JSON.stringify(insertPaymentTnx));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'payment',
    //   response: { Create_Order_insertPayTnx_Exit: insertPaymentTnx },
    // });
    return insertPaymentTnx;
  }

  async insertPartialPayTxn(order: any, req_data: CreateOrderDto): Promise<any> {
    logger.info(`Create_Order_insertpartialPayTnx_Entry_Order ` + JSON.stringify(order));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'payment',
    //   body: { Create_Order_insertpartialPayTnx_Entry_Order: order },
    // });
    //insert  partial payment transaction details
    const insertPartialPayTxn = await Helpers.create(PartialPayment, {
      ...order,
      ...req_data,
    });
    logger.info(`Create_Order_insertpartialPayTnx_Exit ` + JSON.stringify(insertPartialPayTxn));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'payment',
    //   response: { Create_Order_insertpartialPayTnx_Exit: insertPartialPayTxn },
    // });
    return insertPartialPayTxn;
  }

  initiateRefund(): Promise<any> {
    throw new Error('Method not implemented.');
  }
}
