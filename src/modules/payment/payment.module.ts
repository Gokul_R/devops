import { Module } from '@nestjs/common';
import { RazorPaymentService } from './payment.service';
import { PayGPaymentService } from './PaygPaymentService';
import { UtilsModule } from 'src/core/utils/utils.module';
import { HttpModule } from '@nestjs/axios';
import { HttpUtils } from 'src/core/utils/http-utils';
import { CouponModule } from '../coupon/coupon.module';
import { BranchModule } from '../shop/branch.module';

@Module({
  imports:[HttpModule,CouponModule,BranchModule],
  providers: [RazorPaymentService,PayGPaymentService,HttpUtils],
  exports:[PayGPaymentService,RazorPaymentService]
})
export class PaymentModule {}
