interface TransactionRequest {
    Merchantkeyid?: string;
    UniqueRequestId?: string;
    UserDefinedData?: UserDefinedData;
    ProductData?: string; // You might want to parse this as JSON
    RequestDateTime?: string;
    RedirectUrl?: string;
    TransactionData?: TransactionData;
    OrderAmount?: string;
    OrderType?: string;
    OrderAmountData?: OrderAmountData;
    CustomerData?: CustomerData;
    IntegrationData?: IntegrationData;
    OrderKeyId?:string;
    PaymentTransactionId?:string;
    RefundAmount?:string
    PaymentType?: string,
    MerchantKeyId?: string,

  }


interface CustomerData {
    CustomerId: string;
    CustomerNotes?: string;
    FirstName?: string;
    LastName?: string;
    MobileNo: string;
    Email?: string;
    EmailReceipt?: string;
    BillingAddress?: string;
    BillingCity?: string;
    BillingState?: string;
    BillingCountry?: string;
    BillingZipCode?: string;
    ShippingFirstName?: string;
    ShippingLastName?: string;
    ShippingAddress?: string;
    ShippingCity?: string;
    ShippingState?: string;
    ShippingCountry?: string;
    ShippingZipCode?: string;
    ShippingMobileNo?: string;
  }
  
  interface UserDefinedData {
    UserDefined1: string;
  }
  
  interface TransactionData {
    AcceptedPaymentTypes: string;
    PaymentType: string;
    SurchargeType: string;
    SurchargeValue: string;
    RefTransactionId: string;
    IndustrySpecificationCode: string;
    PartialPaymentOption: string;
  }
  
  interface OrderAmountData {
    AmountTypeDesc: string;
    Amount: string;
  }
  
  interface IntegrationData {
    UserName?: string;
    Source: string;
    IntegrationType: string;
    HashData?: string;
    PlatformId?: string;
  }
  
  interface ProductData {
    PaymentReason: string;
    ItemId: string;
    Size: string;
    AppName: string;
  }
  

  
  