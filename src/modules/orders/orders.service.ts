import { Inject, Injectable, NotAcceptableException, NotFoundException } from '@nestjs/common';
import { ConfirmOrderDto, CreateFreelancerOrderDto, CreateOrderDto, DownloadPdfDto } from './dto/create-order.dto';
import { CancelOrderDto } from './dto/update-order.dto';
import { RazorPaymentService } from '../payment/payment.service';
import Helpers from 'src/core/utils/helpers';
import Payment_Txn from 'src/core/database/models/Payment-Tnx';
import Order from 'src/core/database/models/Order';
import {
  EC200,
  EC400,
  EC403,
  EM104,
  EM106,
  EM119,
  EM124,
  EM125,
  ORDER_SERVICES_REPO,
  Payment_Status,
  Provided_By,
  RAZORPAY,
  LOCAL,
  DEVELOPMENT,
  STAGING,
  PRODUCTION,
  TriggerType,
  NotificationType,
  notificationTypeConfig,
  EC204,
  EC404,
} from 'src/core/constants';
import Appointment, { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import { CartService } from '../cart/cart.service';
import Branch from 'src/core/database/models/Branch';
import Service from 'src/core/database/models/Services';
import Location from 'src/core/database/models/Location';
import { RescheduleOrderDto } from './dto/RescheduleOrderDto';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { BaseService } from 'src/base.service';
import { ModelCtor } from 'sequelize-typescript';
import { SERVICE_ATTRIBUTES } from 'src/core/attributes/services';
import { APPOINTMENT_ATTRIBUTES } from 'src/core/attributes/appointment';
import EmailTemService from 'src/core/utils/emailTemplate';
import { MailUtils } from 'src/core/utils/mailUtils';
import { UsersService } from '../users/users.service';
import User from 'src/core/database/models/User';
import { LOCATION_ATTRIBUTES } from 'src/core/attributes/location';
import { PayGPaymentService } from '../payment/PaygPaymentService';
import { PaymentMethod } from '../payment/PaymentMethod.interface';
import { CouponService } from '../coupon/coupon.service';
import PaymentConfig from 'src/core/database/models/PaymentConfig';
import { SharedService } from '../shared/shared.service';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { Op } from 'sequelize';
import { DateUtils } from 'src/core/utils/dateUtils';
import { Errors } from 'src/core/constants/error_enums';
import { Cron } from 'nestjs-schedule';
import { CronUtils } from 'src/core/utils/cronUtils';
import { SchedulerRegistry } from '@nestjs/schedule';
import FlashSale from 'src/core/database/models/FlashSale';
import { FlashSaleConstants } from '../flash-sale/flashsale-constants';
import { NotificationService } from '../notification/notification.service';
import { paymentRefundService } from '../payment/paymentRefundService';
import { logger } from 'src/core/utils/logger';
import { FileUtils } from 'src/core/utils/fileUtils';
import MasterCategory from 'src/core/database/models/MasterCategory';
import { MASTER_CATEGORIES_ATTRIBUTES } from 'src/core/attributes/master_category';
import OrderService from 'src/core/database/models/OrderServices';
import OrderRemainder from 'src/core/database/models/OrderRemainder';
import { BranchService } from '../shop/branch.service';
import SlotDetail from 'src/core/database/models/SlotDetail';
import SlotManagement from 'src/core/database/models/SlotManagement';
import { ORDER_ATTRIBUTES } from 'src/core/attributes/order';
import UserAddress from 'src/core/database/models/UserAddress';
import { RazorPayWebhookPayload } from './dto/webhook-payload.dto';
import { RazorpayPaymentStatus, VendorType } from 'src/core/utils/enum';
import Vendor from 'src/core/database/models/Vendor';
import { CreateNotificationDto } from '../notification/dto/create-notification.dto';
import sequelize from 'sequelize';
import PartialPayment from 'src/core/database/models/PartialPayments';
import { newLogger } from 'src/core/utils/loggerNew';

@Injectable()
export class OrdersService extends BaseService<Order> {
  protected model: ModelCtor<Order>;
  paymentService: PaymentMethod;
  payment_method: string;
  order: Order;
  cronUtils: CronUtils;
  constructor(
    @Inject(ORDER_SERVICES_REPO)
    private orderServiceRepo: typeof OrderService,
    private readonly cartService: CartService,
    private readonly userService: UsersService,
    private readonly branchService: BranchService,
    private readonly notificationService: NotificationService,
    private readonly MailUtilsService: MailUtils,
    private readonly paygPaymentService: PayGPaymentService,
    private readonly razorPaymentService: RazorPaymentService,
    private readonly couponService: CouponService,
    private readonly sharedService: SharedService,
    private schedulerRegistry: SchedulerRegistry,
  ) {
    super();
    this.model = Order;
    this.cronUtils = new CronUtils(this.schedulerRegistry);
    //this.setupPayment();
  }
  async createNewOrder(createOrderDto: CreateOrderDto, req: any) {
    logger.info(`Order_Create_Entry: ` + JSON.stringify(createOrderDto, req?.device?.type));
    // newLogger.info({
    //   url: '/api/v1/orders/create',
    //   type: 'order',
    //   body: { Order_Create_Entry: createOrderDto, req: req?.device?.type },
    // });
    logger.info(`Order_Create_Device_Name: ` + JSON.stringify(req?.device?.type));

    await this.setupPayment();
    logger.info(`Order_Create_check_vendor_type: ` + JSON.stringify(createOrderDto.vendor_type));
    // newLogger.info({
    //   url: '/api/v1/orders/create',
    //   type: 'order',
    //   response: { Order_Create_check_vendor_type: createOrderDto.vendor_type },
    // });
    if (createOrderDto.vendor_type == VendorType.FREELANCER) {
      createOrderDto.initial_amount = createOrderDto?.final_price;
      createOrderDto.due_amount = createOrderDto?.final_price;
    }
    createOrderDto.final_price = Number(createOrderDto.final_price.toFixed(2));
    let { payout_amount, slaylewks_revenue } = await this.paymentService.calculatePayout(createOrderDto);
    logger.info(`Order_Create_Payout_amount: ` + JSON.stringify(createOrderDto.payout_amount));
    // newLogger.info({
    //   url: '/api/v1/orders/create',
    //   type: 'order',
    //   response: { Order_Create_Payout_amount: createOrderDto.payout_amount },
    // });
    createOrderDto.payout_amount = payout_amount;
    createOrderDto.revenue_amount = Number(slaylewks_revenue);
    createOrderDto.invoice_no = Helpers.generateInvoiceNumber('INV');

    //insert order details
    createOrderDto.applied_coupon_id ? createOrderDto.applied_coupon_id : delete createOrderDto.applied_coupon_id;
    createOrderDto.device = req?.device?.type;
    const order: Order = await super.create(createOrderDto);
    await this.mappings(createOrderDto, order);
    createOrderDto.order_id = order.order_id;
    let receipt_id = Order.generateReceiptId(order.order_id);

    //create record in razorpay or payg
    const { payment_order, insertPaymentTnx } = await this.paymentService.createOrder(
      createOrderDto,
      receipt_id,
      false,
    );
    await Order.update(
      { payment_order_id: payment_order.id, receipt_id: receipt_id },
      { where: { order_id: order.order_id } },
    );

    let result = {
      user_id: createOrderDto?.user_id,
      payment_order_id: this.payment_method === RAZORPAY ? insertPaymentTnx?.id : payment_order?.OrderKeyId,
      receipt_id: insertPaymentTnx?.receipt,
      status: insertPaymentTnx?.status,
      order_id: order?.order_id,
      payment_process_url: payment_order?.PaymentProcessUrl,
      payment_method: this.payment_method,
    };
    logger.info(`Order_Create_Exit: ` + JSON.stringify(result));
    // newLogger.info({
    //   url: '/api/v1/orders/create',
    //   type: 'order',
    //   response: { Order_Create_Exit: result },
    // });

    return {
      code: EC200,
      msg: EM104,
      data: result,
    };
  }

  async createFreelancerOrder(createOrderDto: CreateFreelancerOrderDto, req: any) {
    logger.info(`Freelancer_Order_Create_Entry: ` + JSON.stringify(createOrderDto, req?.device?.type));
    // newLogger.info({
    //   url: '/api/v1/orders/',
    //   type: 'order',
    //   response: { Freelancer_Order_Create_Entry: createOrderDto, req: req?.device?.type },
    // });
    logger.info(`Freelancer_Order_Create_Device_Name: ` + JSON.stringify(req?.device?.type));

    await this.setupPayment();
    logger.info(`Freelancer_Order_Create_check_vendor_type: ` + JSON.stringify(createOrderDto.vendor_type));
    // newLogger.info({
    //   url: '/api/v1/orders/',
    //   type: 'order',
    //   response: { Freelancer_Order_Create_check_vendor_type: createOrderDto.vendor_type },
    // });
    let checkIfOrder = await Order.count({ where: { order_id: createOrderDto?.order_id } });
    if (checkIfOrder) {
      createOrderDto.final_price = Number(createOrderDto.final_price.toFixed(2));
      createOrderDto.device = req?.device?.type;
      let receipt_id = Order.generateReceiptId(createOrderDto?.order_id);
      const { payment_order, insertPaymentTnx } = await this.paymentService.createOrder(
        createOrderDto,
        receipt_id,
        true,
      );
      let { payout_amount, slaylewks_revenue } = await this.paymentService.calculatePayout(
        createOrderDto,
        insertPaymentTnx,
      );
      let payment_txn = await Payment_Txn.findOne({ where: { order_id: createOrderDto?.order_id } });
      //TODO Create partial payment table records
      let obj = {
        order_id: createOrderDto?.order_id,
        partial_payment_order_id: payment_order?.id,
        pay_tnx_id: payment_txn.id,
        partial_payment_receipt_id: receipt_id,
        amount: Number(payment_order?.amount).toFixed(2),
        status: insertPaymentTnx?.status,
      };
      let partialData = await PartialPayment.create(obj);
      let order = await Order.findOne({
        attributes: ['payout_amount', 'revenue_amount', 'final_price'],
        where: { order_id: createOrderDto?.order_id },
      });
      await Order.update(
        {
          partial_id: partialData?.id,
          payout_amount: order?.payout_amount + Number(payout_amount),
          final_price: Number(order?.final_price + createOrderDto.final_price).toFixed(2),
          revenue_amount: Number((order?.revenue_amount + Number(slaylewks_revenue)).toFixed(2)),
        },
        { where: { order_id: createOrderDto?.order_id } },
      );
      let result = {
        user_id: createOrderDto?.user_id,
        payment_order_id: this.payment_method === RAZORPAY ? insertPaymentTnx?.id : payment_order?.OrderKeyId,
        receipt_id: insertPaymentTnx?.receipt,
        status: insertPaymentTnx?.status,
        order_id: createOrderDto?.order_id,
        payment_process_url: payment_order?.PaymentProcessUrl,
        payment_method: this.payment_method,
      };
      logger.info(`Order_Create_Exit: ` + JSON.stringify(result));
      // newLogger.info({
      //   url: '/api/v1/orders/',
      //   type: 'order',
      //   response: { Order_Create_Exit: result },
      // });
      return {
        code: EC200,
        msg: EM104,
        data: result,
      };
    } else throw new NotFoundException('Initial payment not completed!!');
  }

  async checkIsSlotAvailable(createOrderDto: CreateOrderDto): Promise<boolean> {
    let { appointment_start_date_time, branch_id } = createOrderDto;
    let orders = await this.getOrdersByDate(appointment_start_date_time, branch_id);
    const ordersCount = orders.length;
    const d = new Date(appointment_start_date_time);
    const day = d.getUTCDay();

    let slot: SlotDetail | SlotManagement = await this.branchService.getSlotDetails(branch_id, d, day);
    return slot.no_of_employees > ordersCount;
  }
  //create Appointments and order services mappings while placing order
  async mappings(createOrderDto: CreateOrderDto, order: Order) {
    delete order?.id;
    for (const id of createOrderDto.service_ids) {
      let servicesObj = {
        ...createOrderDto,
        ...order,
        service_id: id,
        user_id: createOrderDto.user_id,
      };
      let apps = await Helpers.create(Appointment, servicesObj);
      await Helpers.create(this.orderServiceRepo, servicesObj);
    }
  }
  async confirmOrder(req_data: ConfirmOrderDto) {
    logger.info(`Confirm_Order_Entry: ` + JSON.stringify(req_data));
    // newLogger.info({
    //   url: '/api/v1/orders/',
    //   type: 'confirmOrder',
    //   body: { Confirm_Order_Entry: req_data },
    // });
    await this.setupPayment();
    let { order_id, payment_order_id, user_id, payment_id } = req_data;
    let payment_qry: SequelizeFilter<Payment_Txn> = {
      where: { id: payment_order_id, user_id: user_id },
    };
    let get_data = await Payment_Txn.count(payment_qry);
    if (!get_data) {
      logger.info(`Confirm_Order_No_Record_Found:`);
      // newLogger.info({
      //   url: '/api/v1/orders/',
      //   type: 'confirmOrder',
      //   response: 'Confirm_Order_No_Record_Found',
      // });
      return { code: EC403, msg: EM119, data: {} };
    }
    const payment_data: any = await this.paymentService.paymentStatus(payment_order_id);
    if (!payment_data) throw Error(Errors.INVALID_ORDER_DETAILS);

    //-------------------------------------------------------------------------------------
    //freelancer sencond payment flow order confirm
    if (payment_data?.notes?.partialPayment == true || payment_data?.notes?.partialPayment == 'true') {
      //-------------------------------------------------------------------------------------
      // logger.info(`Confirm_Order_Freelancer_second_payment:`, JSON.stringify(payment_data));
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'confirmOrder',
      //   body: { Confirm_Order_Freelancer_second_payment: payment_data },
      // });
      payment_data['partial_payment_id'] = payment_id;
      payment_data['partial_payment_completed'] = payment_data?.status == 'paid' ? true : false;
      payment_data['due_amount'] = 0;
      payment_data['payment_id'] = payment_id;
      delete payment_data.id;
      //-------------------------------------------------------------------------------------
      let orderPartial = await PartialPayment.findOne({ where: { partial_payment_order_id: payment_order_id } });
      if (payment_data?.status === 'paid') {
        let updatePartialPaymentTable = await PartialPayment.update(payment_data, {
          where: { partial_payment_order_id: payment_order_id, order_id: order_id },
        });
        await this.updatePaymentTnx(payment_qry, payment_data);
        const { user, confirmationContent, invoice_pdf, appointments } = await this.prepareContent(user_id, order_id);
        this.MailUtilsService.sendOrderConfirmationMail(
          this.order[0]?.customer_email || user?.email_id,
          confirmationContent,
          invoice_pdf,
        );
        await Helpers.update(
          Order,
          { where: { order_id: order_id } },
          { due_amount: payment_data?.due_amount, partial_payment_completed: payment_data?.partial_payment_completed },
        );
        return { code: EC200, msg: EM124, data: {} };
      } else if (orderPartial?.status === Payment_Status.PaymentFailed) {
        return { code: EC400, msg: EM125, data: {} };
      } else return { code: EC400, msg: EM125, data: {} };
      //-------------------------------------------------------------------------------------
    } else {
      //business and freelancer first payment and business order confirm only
      // logger.info(`Confirm_Order_Business_And_Freelancer_first_payment:`, JSON.stringify(payment_data));
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'confirmOrder',
      //   body: { Confirm_Order_Business_And_Freelancer_first_payment: payment_data },
      // });
      if (payment_data?.amount_paid && payment_data?.status == 'paid') {
        //-------------------------------------------------------------------------------------
        payment_data['payment_id'] = payment_id;
        let order = await Order.findOne({ where: { payment_id: payment_id } });
        payment_data['payment_order_id'] = payment_data.id;
        delete payment_data.id;
        payment_data['order_status'] =
          req_data?.vendor_type == VendorType.FREELANCER
            ? APPOINTMENT_STATUS.AWAITING_APPROVAL
            : APPOINTMENT_STATUS.CONFIRMED;
        //-------------------------------------------------------------------------------------
        if (order?.is_payment_completed && order?.order_status == APPOINTMENT_STATUS.CONFIRMED) {
          logger.info(`This order already confirmed & payed: ${JSON.stringify(payment_data)}`);
          // newLogger.info({
          //   url: '/api/v1/orders',
          //   type: 'confirmOrder',
          //   response: { This_order_already_confirmed_and_payed: payment_data },
          // });
        } else if (order?.is_payment_completed && order?.order_status == APPOINTMENT_STATUS.AWAITING_APPROVAL)
          logger.info(`This order already  payed: ${JSON.stringify(payment_data)}`);
        // newLogger.info({
        //   url: '/api/v1/orders',
        //   type: 'confirmOrder',
        //   response: { This_order_already_payed: payment_data },
        // });
        //-------------------------------------------------------------------------------------
        if (order?.payment_status === Payment_Status.PaymentSuccess) {
          return { code: EC200, msg: EM124, data: {} };
        } else if (order?.payment_status === Payment_Status.PaymentFailed) {
          return { code: EC400, msg: EM125, data: {} };
        }
        //-------------------------------------------------------------------------------------
        await this.updatePaymentTnx(payment_qry, payment_data);
        this.updateOrderInfo(payment_data, user_id, order_id, Payment_Status.PaymentSuccess);
        this.cartService.removeCart(null, user_id);
        req_data?.vendor_type == VendorType.BUSINESS && (await this.informUsers(user_id, order_id));
        req_data?.vendor_type == VendorType.FREELANCER && (await this.informFreelancerAndUser(user_id, order_id));
        //-------------------------------------------------------------------------------------
        logger.info(`Confirm_Order_Exit_with_Payment_Sucess:` + JSON.stringify(payment_data));
        // newLogger.info({
        //   url: '/api/v1/orders',
        //   type: 'confirmOrder',
        //   response: { Confirm_Order_Exit_with_Payment_Sucess: payment_data },
        // });
        return { code: EC200, msg: EM124, data: {} };
        //-------------------------------------------------------------------------------------
      } else {
        //-------------------------------------------------------------------------------------
        payment_data['order_status'] = APPOINTMENT_STATUS.PENDING;
        await this.updatePaymentTnx(payment_qry, payment_data);
        this.updateOrderInfo(payment_data, user_id, order_id, Payment_Status.PaymentFailed);
        logger.info(`Confirm_Order_Exit_with_Payment_Pending:` + JSON.stringify(payment_data));
        // newLogger.info({
        //   url: '/api/v1/orders',
        //   type: 'confirmOrder',
        //   response: { Confirm_Order_Exit_with_Payment_Pending: payment_data },
        // });
        return { code: EC400, msg: EM125, data: {} };
        //-------------------------------------------------------------------------------------
      }
    }
  }

  private async informUsers(user_id: string, order_id: string) {
    let { user, confirmationContent, invoice_pdf, appointments } = await this.prepareContent(user_id, order_id);
    let { customer_email, applied_coupon_id, branch } = this.order[0];
    let { branch_email } = branch;

    await this.couponService.increaseCouponUsedCount(user_id, applied_coupon_id);
    this.MailUtilsService.sendOrderConfirmationMail(customer_email || user.email_id, confirmationContent, invoice_pdf);
    this.notifyVendor(branch.vendor_id, NotificationType.CONFIRMED);
    this.MailUtilsService.sendOrderPlacedMailToVendor(branch_email, appointments, invoice_pdf, this.order);
    this.scheduleReminderMail(customer_email || user.email_id, branch_email, user, this.order[0], appointments);
  }

  async notifyVendor(vendor_id: string, type: NotificationType) {
    const typeConfig = notificationTypeConfig[type];
    const vendor: Vendor = await Vendor.findByPk<Vendor>(vendor_id);
    const notificationData = new CreateNotificationDto();

    notificationData.device_token =
      vendor.device_token ||
      'eQNzky7-SUmu1SnTM2-Ed6:APA91bHTcGmahV707KPx16jhKCzKlcyOXcjjy_zdmTX_vMgxnPKVXUrEVycS576quAXXKqi8tdklwdnlPuYDTjLF9VJfDnowOXJOqP4mHbFg8cHDtLHvhq6nmB8iqGQXGWUB7o-0ynlS';
    notificationData.description = typeConfig.description;
    notificationData.title = typeConfig.title;
    await this.notificationService.sendPushNotification(notificationData);
  }

  async informFreelancerAndUser(user_id: string, order_id: string) {
    let { user, vendortemp, confirmationContent } = await this.prepareFreelancerContent(user_id, order_id);
    let { customer_email, branch } = this.order[0];
    let { branch_email } = branch;
    this.MailUtilsService.awatingApprovalUserMail(customer_email || user?.email_id, confirmationContent);
    this.MailUtilsService.awatingApprovalVendorMail(branch_email, vendortemp);
    this.notifyVendor(branch?.vendor_id, NotificationType.AWATING_ORDER);
  }

  async prepareFreelancerContent(user_id: string, order_id: string) {
    let confirmationContent: any;
    let vendortemp: any;
    let user: User = await this.userService.findOneById(user_id);
    let { data: appointments } = await this.getAppointments(user_id, 1, order_id);
    this.order = await this.getOrderDetails(order_id);
    let useraddress = await UserAddress.findOne({ where: { user_id: user_id } });
    confirmationContent = EmailTemService.awaitingApprovalUserTemp(appointments, this.order);
    vendortemp = EmailTemService.awaitingApprovalVendorTemp(this.order[0].appointments, this.order, useraddress);

    // let invoiceContent: string = EmailTemService.orderInovoice(appointments, this.order);
    // let invoice: Buffer = await MailUtils.convertHtmlToPdf(invoiceContent);
    return {
      user: user,
      appointments: appointments,
      confirmationContent: confirmationContent,
      vendortemp: vendortemp,
    };
  }

  async scheduleReminderMail(
    user_mail: string,
    branch_email: string,
    user: User,
    order: Order,
    appointments: Appointment[],
  ) {
    logger.info(
      `Confirm_Order_Schedule_Reminder_Mail_Entry: ` +
        JSON.stringify(user_mail) +
        `order_id:${order.order_id}: ` +
        JSON.stringify(order),
    );
    // let { order_id, appointment_start_date_time, user_id, branch } = order || {};
    // if (!user) user = await this.userService.findOneById(user_id);
    // let reminding_time: Date = DateUtils.addHours(appointment_start_date_time, -1);
    // let diff = DateUtils.findDiffInHrs(reminding_time, new Date());
    // let sendRemindMail = async () => {
    //   let { branch_name, branch_image, branch_email } = branch || {};
    //   let req: CreateNotificationDto = {
    //     description: 'Today Your Service Available',
    //     title: branch_name,
    //     image: branch_image,
    //     device_token: user.device_token,
    //     user_id: user_id,
    //   };
    //   log(order);
    //   await this.MailUtilsService.sendOrderReminderMail(mail, appointments, order);
    //   await this.MailUtilsService.sendOrderReminderMailVendor(branch_email, appointments, order);
    //   await this.notificationService.create(req);
    // };
    // logger.info(`Confirm_Order_Schedule_Reminder_Mail_addCronJob_Entry: `);
    // if (diff > 0) this.cronUtils.addCronJob(order_id, reminding_time, sendRemindMail);
    const createRemainder = await this.createRemaiderRecords(user_mail, branch_email, appointments, order);
  }

  async createRemaiderRecords(user_email: string, branch_email: string, appointments: any, order: any) {
    const timestamp = new Date(order?.appointment_start_date_time).getTime();
    const threeHoursBehindTimestamp = timestamp - 3 * 60 * 60 * 1000; //half hour - 30 * 60 * 1000;
    let update_body = {
      order_id: order?.order_id,
      user_email: user_email,
      branch_email: branch_email,
      trigger_type: TriggerType.OrderRemain,
      trigger_time: threeHoursBehindTimestamp,
      appointment_data: appointments,
      order_data: order,
    };
    let where = { where: { order_id: order?.order_id }, defaults: update_body };
    const createRemainder = await Helpers.findOrCreate(OrderRemainder, where);
    if (!createRemainder[1]) {
      delete where?.defaults;
      let updateRemainder = await Helpers.update(OrderRemainder, where, update_body);
    }
  }

  async updateOrderInfo(get_Payment_data: any, user_id: string, order_id: string, payment_status: string) {
    let qry = { where: { user_id: user_id, order_id: order_id } };
    await Order.update(
      {
        is_payment_completed: payment_status === Payment_Status.PaymentSuccess,
        payment_status: payment_status,
        ...get_Payment_data,
      },
      qry,
    );

    await Helpers.update(Appointment, qry, {
      appointment_status: get_Payment_data.order_status,
    });
  }

  async updatePaymentTnx(qry: SequelizeFilter<Payment_Txn>, payment_data: any) {
    // payment_data['status'] = payment_data.payment_status;
    await Helpers.update(Payment_Txn, qry, payment_data);
  }

  async updatePartialPaymentTnx(qry: SequelizeFilter<Payment_Txn>, payment_data: any) {
    await Helpers.update(PartialPayment, qry, payment_data);
  }
  async getOrderDetails(order_id: string) {
    let order_qry = {
      attributes: [
        'order_otp',
        'original_price',
        'user_id',
        'createdAt',
        'coupon_discount',
        'applied_coupon_id',
        'gst',
        'service_type',
        'order_status',
        'is_payment_completed',
        'payment_status',
        'payment_method',
        'payment_id',
        'total_services',
        'customer_email',
        'customer_mobile',
        'customer_name',
        'final_price',
        'discount',
        'original_price',
        'order_id',
        'appointment_start_date_time',
        'created_at',
        'rescheduled',
        'rescheduled_at',
        'partial_id',
        'initial_amount',
        ORDER_ATTRIBUTES.due_amount,
        ORDER_ATTRIBUTES.booking_slot,
        ORDER_ATTRIBUTES.updated_at,
        ORDER_ATTRIBUTES.address_id,
        ORDER_ATTRIBUTES.vendor_type,
        ORDER_ATTRIBUTES.partial_payment_completed,
        ORDER_ATTRIBUTES.pay_by_cash,
      ],
      where: {
        order_id: order_id,
      },
      paranoid: false,
      include: [
        {
          model: Branch,
          attributes: [
            BRANCH_ATTRIBUTES.ID,
            BRANCH_ATTRIBUTES.BRANCH_IMAGE,
            BRANCH_ATTRIBUTES.BRANCH_NAME,
            BRANCH_ATTRIBUTES.BRANCH_EMAIL,
            BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO,
            BRANCH_ATTRIBUTES.IS_TAX_CHECKED,
            BRANCH_ATTRIBUTES.TAX_PERCENTAGE,
            BRANCH_ATTRIBUTES.GST_NUMBER,
            BRANCH_ATTRIBUTES.VENDOR_ID,
          ],
          paranoid: false,
          include: {
            attributes: [
              LOCATION_ATTRIBUTES.latitude,
              LOCATION_ATTRIBUTES.longitude,
              LOCATION_ATTRIBUTES.address,
              LOCATION_ATTRIBUTES.area,
              LOCATION_ATTRIBUTES.city,
              LOCATION_ATTRIBUTES.state,
              LOCATION_ATTRIBUTES.pincode,
            ],
            model: Location,
            paranoid: false,
          },
        },
        {
          attributes: ['id', 'receipt', 'status'],
          model: Payment_Txn,
          paranoid: false,
        },
        {
          model: UserAddress,
          attributes: ['flat_no', 'area', 'city', 'zipcode', 'state', 'country', 'customer_name', 'customer_mobile'],
          paranoid: false,
        },
        {
          attributes: ['appointment_start_date_time', 'appointment_status'],
          model: Appointment,
          paranoid: false,
          include: {
            model: Service,
            attributes: { exclude: ['is_active', 'createdAt', 'updatedAt'] },
            paranoid: false,
            include: [
              {
                attributes: { exclude: ['createdAt', 'updatedAt'] },
                model: FlashSale,
                where: FlashSaleConstants.getFlashSaleQry(),
                required: false,
                paranoid: false,
              },
              { attributes: [MASTER_CATEGORIES_ATTRIBUTES.category], model: MasterCategory, paranoid: false },
            ],
          },
        },
      ],
    };
    let order = await Helpers.findAll(Order, order_qry);
    if (order.length == 0) throw new NotFoundException(Errors.INVALID_ORDER_DETAILS);
    return order;
  }

  async getAppointments(id: string, status: number, order_id?: string) {
    let statusArr = {
      1: [APPOINTMENT_STATUS.PENDING, APPOINTMENT_STATUS.CONFIRMED, APPOINTMENT_STATUS.AWAITING_APPROVAL],
      2: APPOINTMENT_STATUS.COMPLETED,
      3: [APPOINTMENT_STATUS.CANCELLED, APPOINTMENT_STATUS.REJECTED, APPOINTMENT_STATUS.EXPIRED],
    };
    let appointment_qry = {
      attributes: [
        APPOINTMENT_ATTRIBUTES.IS_ACCEPTED_BY_BRANCH,
        APPOINTMENT_ATTRIBUTES.IS_CANCELLED,
        APPOINTMENT_ATTRIBUTES.APPOINTMENT_STATUS,
        APPOINTMENT_ATTRIBUTES.ORDER_ID,
        APPOINTMENT_ATTRIBUTES.APPOINTMENT_START_DATE_TIME,
        APPOINTMENT_ATTRIBUTES.BOOKING_SLOT,
      ],
      where: { user_id: id, appointment_status: statusArr[status] },
      include: [
        {
          model: Service,
          attributes: [
            SERVICE_ATTRIBUTES.SERVICE_NAME,
            SERVICE_ATTRIBUTES.SERVICE_DESCRIPTION,
            SERVICE_ATTRIBUTES.AMOUNT,
            SERVICE_ATTRIBUTES.DISCOUNT,
            SERVICE_ATTRIBUTES.DISCOUNTED_PRICE,
          ],
          include: [
            {
              attributes: { exclude: ['createdAt', 'updatedAt'] },
              model: FlashSale,
              where: FlashSaleConstants.getFlashSaleQry(),
              required: false,
            },
            {
              attributes: [MASTER_CATEGORIES_ATTRIBUTES.category],
              model: MasterCategory,
              required: false,
            },
          ],
        },
        {
          model: Order,
          attributes: [
            'order_otp',
            ORDER_ATTRIBUTES.service_type,
            ORDER_ATTRIBUTES.payment_status,
            ORDER_ATTRIBUTES.booking_slot,
            ORDER_ATTRIBUTES.vendor_type,
            ORDER_ATTRIBUTES.partial_payment_completed,
          ],
        },
        {
          model: Branch,
          attributes: [BRANCH_ATTRIBUTES.ID, BRANCH_ATTRIBUTES.BRANCH_IMAGE, BRANCH_ATTRIBUTES.BRANCH_NAME],
          include: {
            attributes: ['area', 'city'],
            model: Location,
          },
        },
      ],
      order: [[APPOINTMENT_ATTRIBUTES.APPOINTMENT_START_DATE_TIME, 'ASC']],
    };
    if (order_id) appointment_qry.where['order_id'] = order_id;
    let appointments = await Helpers.findAll(Appointment, appointment_qry);
    appointments = JSON.stringify(appointments);
    appointments = JSON.parse(appointments);
    appointments = appointments.map((appts) => {
      let apt = {
        ...appts.order,
        ...appts,
      };
      delete apt.order;
      return apt;
    });
    // console.log('======>', appointments);
    return { code: EC200, msg: EM106, data: appointments };
  }

  async getOrdersByDate(date: Date, branch_id: string): Promise<any> {
    let order_qry = {
      where: { appointment_start_date_time: date, order_status: APPOINTMENT_STATUS.CONFIRMED, branch_id: branch_id },
    };
    let orders: Order[] = await super.findAll(order_qry);
    return orders;
  }
  /**
   *
   * @param id -order id
   * @param cancelOrderDto -resaons
   * @param isExpired -is order expired
   * @returns
   */
  async cancelOrder(id: string, cancelOrderDto: CancelOrderDto, isExpired: boolean) {
    logger.info(
      `User_Cancel_Order_Entry_Id: ` +
        JSON.stringify(id) +
        'cancel_data: ' +
        JSON.stringify(cancelOrderDto) +
        'isExpired' +
        JSON.stringify(isExpired),
    );
    newLogger.info({
      url: '/api/v1/orders',
      type: 'cancelOrder',
      body: this.cancelOrder,
      response: { User_Cancel_Order_Entry_Id: id, cancel_data: cancelOrderDto, isExpired: isExpired },
    });
    let cancel_query = {
      where: {
        order_id: id,
      },
    };
    let checkIfOrder = await Order.findOne({ attributes: ['is_payment_completed'], where: { order_id: id } });
    if (!checkIfOrder || !checkIfOrder?.dataValues) throw Error(Errors.INVALID_ORDER_DETAILS);
    if (cancelOrderDto?.on_back && checkIfOrder?.is_payment_completed) {
      //TODO should be add partial payment true condition
      logger.info(`Order already paid can't cancel order` + JSON.stringify(cancelOrderDto));
      newLogger.info({
        url: '/api/v1/orders',
        type: 'cancelOrder',
        body: this.cancelOrder,
        response: { Order_already_paid_cant_cancel_order: cancelOrderDto },
      });
      return;
    }
    let status = isExpired ? APPOINTMENT_STATUS.EXPIRED : APPOINTMENT_STATUS.CANCELLED;
    // let payment_status: string = cancelOrderDto.on_back && Payment_Status.PaymentFailed;
    let order: Order[] = await Helpers.update(Order, cancel_query, {
      ...cancelOrderDto,
      order_status: status,
      // payment_status:payment_status,
      cancelled_by: Provided_By.USER,
      is_cancelled: true,
    });

    let appointments = await Helpers.update(Appointment, cancel_query, {
      ...cancelOrderDto,
      appointment_status: status,
      is_cancelled: true,
    });

    let { customer_email, branch_id, address_id } = order[0];
    const [services, branch, location] = await Promise.all([
      OrderService.findAll({
        include: [{ model: Service, paranoid: false }],
        where: { order_id: id },
        paranoid: false,
      }),
      Branch.findByPk(branch_id, { paranoid: false }),
      Location.findOne({ where: { branch_id: branch_id }, paranoid: false }),
    ]);

    let address: UserAddress;
    if (address_id) {
      address = await UserAddress.findByPk(address_id, { paranoid: false });
    }
    let mailData: any = {
      order: order[0],
      location: location,
      branch: branch,
      appointments: services,
      address: address,
    };
    await this.MailUtilsService.sendOrderCancelationMailToUser(customer_email, mailData);
    if (!cancelOrderDto.on_back) {
      await this.MailUtilsService.sendOrderCancelationMailToVendor(branch.branch_email, mailData);
      // this.cronUtils.deleteCron(id);
      await this.initiateRefund(order[0]);
      this.notifyVendor(branch.vendor_id, NotificationType.CANCELLED);
    }
    this.deleteOrderRemainder(id);
    return order;
  }
  public deleteOrderRemainder(order_id: string) {
    OrderRemainder.destroy({ force: true, where: { order_id: order_id } });
  }

  async initiateRefund(order: Order): Promise<void> {
    logger.info(`User_Cancel_Order_InitiateRefund_Entry_order: ` + JSON.stringify(order));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'refund',
    //   body: order,
    //   response: { User_Cancel_Order_InitiateRefund_Entry_order: order },
    // });
    let { appointment_start_date_time, final_price } = order;
    let full_refund: boolean = true;
    let diff: number = DateUtils.findDiffInHrs(new Date(appointment_start_date_time), new Date());
    if (diff < 24 && diff > 0) {
      logger.info('User_Cancel_Order_with_in_24h_entry: ');
      // info({
      //   url: '/api/v1/orders',
      //   type: 'refund',
      //   response: 'User_Cancel_Order_with_in_24h_entry',
      // });
      const getPaymentConfig = await PaymentConfig.findOne({
        attributes: ['within_24h_pct', 'before_24h_pct', 'rejectedby_admin_pct'],
      });
      if (!getPaymentConfig) throw new Error('The cancellation deduction percentage is not defined');
      let deductPct = Number((getPaymentConfig.within_24h_pct / 100).toFixed(2));
      final_price = deductPct > 0 ? final_price - final_price * deductPct : final_price;
      full_refund = false;
    } else if (diff < 0) {
      logger.info('User_Cancel_Order_without_24h_entry: ');
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'refund',
      //   response: 'User_Cancel_Order_without_24h_entry',
      // });
      final_price = 0;
    }
    final_price = Number(final_price) * 100;
    logger.info('User_Cancel_Order_final_price: ' + JSON.stringify(final_price));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'refund',
    //   body: order,
    //   response: { User_Cancel_Order_final_price: final_price },
    // });
    if (final_price <= 100) {
      // update refunded status
      const updatePaymentTnx = await Payment_Txn.update(
        { refund_status: 'processed', amount_refunded: final_price },
        { where: { order_id: order.order_id, user_id: order.user_id } },
      );

      const updateOrderData = await Order.update(
        { refund_status: 'processed', refund_amount: final_price, refunded: true },
        { where: { order_id: order.order_id, user_id: order.user_id } },
      );
      logger.info('User_Cancel_Order_success_low_amount_exit: ' + JSON.stringify(final_price));
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'refund',
      //   response: { User_Cancel_Order_success_low_amount_exit: final_price },
      // });
      return;
      // throw new Error(EM144);//
    } else {
      if (order?.payment_status == Payment_Status?.PaymentSuccess && order?.payment_id) {
        let req = {
          amount: final_price, //// TODO Check whether there is cancellation fee or not
          user_id: order.user_id,
          order_id: order.order_id,
          payment_id: order.payment_id, // TODO change the pay_id
          full_refund: full_refund, //full_refund  //// TODO Check whether there is cancellation fee or not
        };
        logger.info('User_Cancel_Order_instantRefundAmountToUser_entry: ' + JSON.stringify(req));
        // newLogger.info({
        //   url: '/api/v1/orders',
        //   type: 'refund',
        //   response: { User_Cancel_Order_instantRefundAmountToUser_entry: req },
        // });
        const sendRefundAmount = await new paymentRefundService().instantRefundAmountToUser(req);
        logger.info('User_Cancel_Order_instantRefundAmountToUser_exit: ' + JSON.stringify(sendRefundAmount));
        newLogger.info({
          url: '/api/v1/orders',
          type: 'refund',
          body: order,
          response: { User_Cancel_Order_instantRefundAmountToUser: sendRefundAmount },
        });
        return;
      } else {
        logger.info('User_Cancel_Order_user_not_paid_exit: ' + JSON.stringify(order));
        newLogger.info({
          url: '/api/v1/orders',
          type: 'refund',
          body: order,
          response: { User_Cancel_Order_user_not_paid: order },
        });
        // throw new Error(EM143);
      }
    }
    // await this.paymentService.initiateRefund('uiujk');
    // return diff;
  }
  async rescheduleOrder(id: string, rescheduleOrderDto: RescheduleOrderDto) {
    logger.info(
      `User_Reschedule_order_Entry_id: ` + JSON.stringify(id) + 'reschudule_data' + JSON.stringify(rescheduleOrderDto),
    );
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'reschedule',
    //   response: { User_Reschedule_order_Entry_id: id, reschudule_data: rescheduleOrderDto },
    // });
    if (await new DateUtils().isTimestampInThePast(rescheduleOrderDto.appointment_start_date_time))
      throw new Error(Errors.IFPASTTIME);
    let reschedule_query = {
      where: {
        order_id: id,
      },
    };
    let count = await Order.count(reschedule_query);
    if (count < 0) throw new NotFoundException(Errors.INVALID_ORDER_DETAILS);
    reschedule_query.where['rescheduled'] = false;
    let order = await Helpers.update(Order, reschedule_query, {
      ...rescheduleOrderDto,
      rescheduled: true,
      rescheduled_at: new Date(),
    });
    if (order.length == 0) throw new NotAcceptableException(Errors.MAXIMUM_RESCHEDULE_LIMIT_REACHED);
    if ('rescheduled' in reschedule_query.where) delete reschedule_query.where.rescheduled;
    let appointment = await Helpers.update(Appointment, reschedule_query, rescheduleOrderDto);
    let { customer_email, branch_id } = order[0];
    let branch = await Branch.findOne({ where: { id: branch_id } });
    let { confirmationContent, vendortemp } = await this.prepareContent(
      order[0].user_id,
      order[0].order_id,
      order[0].rescheduled,
    );
    await this.MailUtilsService.sendOrderRescheduleMail(customer_email, confirmationContent);
    await this.MailUtilsService.sendOrderRescheduleVendor(branch.branch_email, vendortemp);
    this.notifyVendor(branch.vendor_id, NotificationType.RESCHEDULED);
    // this.cronUtils.deleteCron(id);
    //-------------------------------------------------
    let orders = await this.getOrderDetails(id);
    // let { data: appointments } = await this.getAppointments(orders[0]?.user_id, 1, orders[0]?.order_id);
    this.scheduleReminderMail(customer_email, branch.branch_email, null, orders[0], orders[0].appointments);
    //---------------------------------------------------
    logger.info(`User_Reschedule_order_Exit: ` + JSON.stringify(order));
    newLogger.info({
      url: '/api/v1/orders',
      type: 'reschedule',
      body: { RescheduleOrderDto, id },
      response: { User_Reschedule_order: order },
    });
    return order;
  }

  async downloadPdf(download_pdf: DownloadPdfDto): Promise<string | null> {
    let { user_id, order_id } = download_pdf;
    let { inovoiceContent } = await this.prepareContent(user_id, order_id);
    const pdfFileName = `invoice_${order_id}.pdf`;
    try {
      await FileUtils.generatePDF(inovoiceContent, pdfFileName);
      let url = this.buildUrl(pdfFileName);
      logger.info('Generated PDF URL:', url);
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'order',
      //   response: { Generated_PDF_URL: url },
      // });
      return url;
    } catch (error) {
      logger.error(error.message);
      newLogger.error({
        url: '/api/v1/orders',
        type: 'order',
        body: DownloadPdfDto,
        response: { error },
      });
      throw Error(error.message);
    }
  }

  buildUrl(pdfFileName: string): string {
    const envAssetPaths = {
      [LOCAL]: process.env.ASSETS_PATH_DEV,
      [DEVELOPMENT]: process.env.ASSETS_PATH_DEV,
      [STAGING]: process.env.ASSETS_PATH_STAGING,
      [PRODUCTION]: process.env.ASSETS_PATH_PROD,
    };

    const selectedEnv = envAssetPaths[process.env.NODE_ENV] || envAssetPaths[STAGING];

    return `${selectedEnv}/${pdfFileName}`;
  }
  async prepareContent(user_id: string, order_id: string, rescheduled?: boolean) {
    let confirmationContent: any;
    let vendortemp: any;
    let user: User = await this.userService.findOneById(user_id);
    let { data: appointments } = await this.getAppointments(user_id, 1, order_id);
    this.order = await this.getOrderDetails(order_id);

    if (rescheduled) {
      let useraddress = await UserAddress.findOne({ where: { user_id: user_id } });
      confirmationContent = EmailTemService.rescheduleTemp(appointments, this.order, useraddress);
      vendortemp = EmailTemService.rescheduleVendorTemplete(appointments, this.order, useraddress);
    } else {
      confirmationContent = EmailTemService.confirmationTemp(appointments, this.order);
    }
    let invoiceContent: string = EmailTemService.orderInovoice(appointments, this.order);
    let invoice: Buffer = await MailUtils.convertHtmlToPdf(invoiceContent);
    return {
      user: user,
      confirmationContent: confirmationContent,
      inovoiceContent: invoiceContent,
      invoice_pdf: invoice,
      appointments: appointments,
      vendortemp: vendortemp,
    };
  }

  async setupPayment() {
    logger.info(`Payment_setUp_Entry: `);
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'order',
    //   response: 'Payment_setUp_Entry',
    // });
    let payment_config: PaymentConfig = await this.sharedService.fetchPaymentMethod();
    if (!payment_config) throw Error('No payment method configured');
    this.payment_method = payment_config?.active_payment_method;
    this.paymentService = this.payment_method === RAZORPAY ? this.razorPaymentService : this.paygPaymentService;
    this.paymentService.init(payment_config);
    logger.info(`Payment_setUp_Exit: ` + JSON.stringify(payment_config));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'order',
    //   response: { Payment_setUp_Exit: payment_config },
    // });
  }
  /**
   * this function fetch expired orders and change it status to cancelled
   */
  @Cron('* 59 23 * * 0-6')
  //  @Cron('30 * * * * *')
  async getExpiredOrders(): Promise<void> {
    try {
      let expiry_qry: SequelizeFilter<Order> = {
        where: {
          appointment_start_date_time: {
            [Op.lt]: new Date(),
          },
          order_status: [APPOINTMENT_STATUS.CONFIRMED, APPOINTMENT_STATUS.PENDING],
        },
      };
      let cancelOrderDto: CancelOrderDto = {
        cancellation_reason: 'Order Expired',
        comment: 'Order Expired',
      };
      //  await this.cancelOrder(null,null,null)
      let expired_orders: Order[] = await Helpers.findAll(Order, expiry_qry);
      expired_orders.forEach(async (order) => {
        let { appointment_start_date_time } = order;

        let diff: number = DateUtils.findDiffInWeek(new Date(appointment_start_date_time), new Date());
        if (diff <= -2) await this.cancelOrder(order.order_id, cancelOrderDto, true);
        logger.info('CronCancellOrderSuccessOrderId: ' + JSON.stringify(order?.order_id));
        newLogger.info({
          url: '/api/v1/orders',
          type: 'cronCancellOrder',
          response: { CronCancellOrderSuccessOrderId: order?.order_id },
        });
      });
    } catch (error) {
      console.log('Error at cancelling expired orders', error);
      logger.error('CronCancellOrderError: ' + JSON.stringify(error));
      newLogger.error({
        url: '/api/v1/orders',
        type: 'cronCancellOrder',
        response: { CronCancellOrderError: error },
      });
    }
  }

  async confirmOrderWebhook(razorPayWebHookPayload: RazorPayWebhookPayload) {
    let order;
    const { payload } = razorPayWebHookPayload;
    const { payment } = payload;
    const { entity } = payment;
    const { id, status, order_id: payment_order_id } = entity;
    const payment_data = { ...entity };
    delete payment_data.order_id;
    delete payment_data.id;
    logger.info('webhook Confirm order webhook entry' + JSON.stringify(razorPayWebHookPayload));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'confirmOrderWebhook',
    //   response: { webhook_Confirm_order_webhook_entry: razorPayWebHookPayload },
    // });

    if (entity?.notes?.partialPayment && entity?.notes?.vendor_type == VendorType.FREELANCER) {
      order = await PartialPayment.findOne({
        attributes: [],
        include: [
          {
            attributes: [
              ORDER_ATTRIBUTES.user_id,
              ORDER_ATTRIBUTES.order_id,
              ORDER_ATTRIBUTES.payment_status,
              ORDER_ATTRIBUTES.is_payment_completed,
            ], //line - changed sasi(07-02-2024)
            model: Order,
          },
        ],
        where: { partial_payment_order_id: payment_order_id },
      });
    } else {
      order = await Order.findOne({
        attributes: [
          ORDER_ATTRIBUTES.user_id,
          ORDER_ATTRIBUTES.order_id,
          ORDER_ATTRIBUTES.payment_status,
          ORDER_ATTRIBUTES.is_payment_completed,
        ], //line - changed sasi(07-02-2024)
        where: { payment_order_id },
      });
    }

    if (!order) {
      logger.info('webhook Confirm order->Order not found');
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'confirmOrderWebhook',
      //   body: razorPayWebHookPayload,
      //   response: 'webhook Confirm order->Order not found',
      // });
      throw new NotFoundException(Errors.INVALID_ORDER_DETAILS);
    }
    order?.order_id && this.updatePaymentAttempt(order?.order_id); // update attempt count in order table
    const { order_id, user_id, payment_status } = order;
    const payment_qry: SequelizeFilter<Payment_Txn> = { where: { id: entity?.order_id } };
    payment_data['payment_id'] = id;
    if (status === RazorpayPaymentStatus.CAPTURED && !order?.is_payment_completed) {
      //line - changed sasi(07-02-2024)
      logger.info('webhook Confirm order-> Updating payment status ' + JSON.stringify(order));
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'confirmOrderWebhook',
      //   response: { webhook_Confirm_order_Updating_payment_status: order },
      // });
      if (entity?.notes?.partialPayment && entity?.notes?.vendor_type == VendorType.FREELANCER) {
        //freelancer second payment
        payment_data['partial_payment_id'] = payment_data.id;
        payment_data['partial_payment_completed'] = payment_data?.status == 'paid' ? true : false;
        payment_data['due_amount'] = 0;
        // let orderPartial = await PartialPayment.findOne({ where: { partial_payment_order_id: order_id } });
        if (status === 'paid') {
          // let order = await Order.findOne({ where: { partial_id: orderPartial.id } });
          await Order.update(payment_data, { where: { order_id: order_id } });
          let updatePartialPaymentTable = await this.updatePartialPayment(payment_data, payment_order_id, order_id);
          const { user, confirmationContent, invoice_pdf, appointments } = await this.prepareContent(user_id, order_id);
          this.MailUtilsService.sendOrderConfirmationMail(
            this.order[0]?.customer_email || user?.email_id,
            confirmationContent,
            invoice_pdf,
          );
        }
      } else if (!entity?.notes?.partialPayment && entity?.notes?.vendor_type == VendorType.FREELANCER) {
        //freelancer first payment
        payment_data['order_status'] = APPOINTMENT_STATUS.AWAITING_APPROVAL;
        this.updateOrderInfo(payment_data, user_id, order_id, Payment_Status.PaymentSuccess);
        this.cartService.removeCart(null, user_id);
        this.informFreelancerAndUser(user_id, order_id);
      } else {
        //business order payment
        payment_data['order_status'] = APPOINTMENT_STATUS.CONFIRMED;
        this.updatePaymentAndOrder(payment_qry, payment_data, user_id, order_id, Payment_Status.PaymentSuccess);
        const { user, confirmationContent, invoice_pdf, appointments } = await this.prepareContent(user_id, order_id);
        const { customer_email, applied_coupon_id, branch } = this.order[0];
        const { branch_email } = branch;

        await this.couponService.increaseCouponUsedCount(user_id, applied_coupon_id);
        this.MailUtilsService.sendOrderConfirmationMail(
          customer_email || user.email_id,
          confirmationContent,
          invoice_pdf,
        );
        this.MailUtilsService.sendOrderPlacedMailToVendor(branch_email, appointments, invoice_pdf, this.order);
        this.scheduleReminderMail(customer_email || user.email_id, branch_email, user, this.order[0], appointments);
        logger.info('webhook Confirm order-> Status updated successfully ' + JSON.stringify(payment_data));
        newLogger.info({
          url: '/api/v1/orders',
          type: 'confirmOrderWebhook',
          body: razorPayWebHookPayload,
          response: { webhook_Confirm_order_Status_updated_successfully: payment_data },
        });
      }
    } else if (status === RazorpayPaymentStatus.FAILED && !order?.is_payment_completed) {
      //line - changed sasi(07-02-2024)
      this.orderFailedWebhook(razorPayWebHookPayload, order);
    } else {
      //line - changed sasi(07-02-2024)
      logger.info('Webhook different status coming:' + JSON.stringify(razorPayWebHookPayload));
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'confirmOrderWebhook',
      //   response: { Webhook_different_status_coming: razorPayWebHookPayload },
      // });
    }
    return payment_data;
  }

  private async updatePartialPayment(payment_data: any, payment_order_id: string, order_id: any) {
    return await PartialPayment.update(payment_data, {
      where: { partial_payment_order_id: payment_order_id, order_id: order_id },
    });
  }

  private async updatePaymentAndOrder(
    paymentQuery: SequelizeFilter<Payment_Txn>,
    paymentData: Record<string, any>,
    userId: string,
    orderId: string,
    paymentStatus: string,
  ) {
    this.updatePaymentTnx(paymentQuery, paymentData);
    this.updateOrderInfo(paymentData, userId, orderId, paymentStatus);
    this.cartService.removeCart(null, userId);
  }

  async orderFailedWebhook(razorPayWebHookPayload: RazorPayWebhookPayload, order?: Order) {
    const { payload } = razorPayWebHookPayload;
    const { payment } = payload;
    const { entity } = payment;
    const { id, status, order_id: payment_order_id } = entity;
    const payment_data = { ...entity };
    delete payment_data.order_id;
    delete payment_data.id;
    logger.info('webhook order-failed entry' + JSON.stringify(razorPayWebHookPayload));
    // newLogger.info({
    //   url: '/api/v1/orders',
    //   type: 'orderFailedWebhook',
    //   response: { webhook_order_failed_entry: razorPayWebHookPayload },
    // });
    if (!order) {
      order = await Order.findOne({
        attributes: [
          ORDER_ATTRIBUTES.user_id,
          ORDER_ATTRIBUTES.order_id,
          ORDER_ATTRIBUTES.payment_status,
          ORDER_ATTRIBUTES.is_payment_completed,
        ], //line - changed sasi(07-02-2024)
        where: { payment_order_id },
      });
    }
    if (!order) {
      logger.info('webhook order-failed invalid order details' + JSON.stringify(razorPayWebHookPayload));
      // newLogger.info({
      //   url: '/api/v1/orders',
      //   type: 'orderFailedWebhook',
      //   response: { webhook_order_failed_invalid_order_details: razorPayWebHookPayload },
      // });
      throw new NotFoundException(Errors.INVALID_ORDER_DETAILS);
    }
    payment_data['payment_id'] = id;
    const { order_id, user_id, payment_status } = order;
    const payment_qry: SequelizeFilter<Payment_Txn> = { where: { id: order_id } };
    //line - changed sasi(07-02-2024)
    if (order?.order_status == APPOINTMENT_STATUS.CONFIRMED || order?.is_payment_completed)
      newLogger.info({
        url: '/api/v1/orders',
        type: 'orderFailedWebhook',
        body: razorPayWebHookPayload,
        response: { This_order_already_confirmed_and_payed: payment_data },
      });
    return logger.info(`This order already confirmed & payed: ${JSON.stringify(payment_data)}`);
    if (status !== order?.payment_status) {
      payment_data['order_status'] = APPOINTMENT_STATUS.CANCELLED;
      await this.updatePaymentAndOrder(payment_qry, payment_data, user_id, order_id, Payment_Status.PaymentFailed);
      if (!entity?.notes?.partialPayment && entity?.notes?.vendor_type == VendorType.FREELANCER)
        this.updatePartialPayment(payment_data, payment_order_id, order_id);
      logger.info(`webhook Updated order failed status: ${JSON.stringify(payment_data)}`);
      newLogger.info({
        url: '/api/v1/orders',
        type: 'orderFailedWebhook',
        body: razorPayWebHookPayload,
        response: { webhook_Updated_order_failed_status: payment_data },
      });
    }
    return payment_data;
  }

  async updatePaymentAttempt(order_id: string) {
    Order.update({ payment_attempt: sequelize.literal('payment_attempt + 1') }, { where: { order_id: order_id } });
    logger.info(`payment_attempt updated: ${JSON.stringify(order_id)}`);
    newLogger.info({
      url: '/api/v1/orders',
      type: 'payment',
      body: order_id,
      response: { payment_attempt_updated: order_id },
    });
  }

  async PaybyCash(order_id: string) {
    let existOrder = await Order.findOne({
      attributes: ['order_id', 'final_price'],
      where: { order_id: order_id, is_payment_completed: true, order_status: APPOINTMENT_STATUS.CONFIRMED },
    });
    if (existOrder) {
      await Order.update(
        {
          pay_by_cash: true,
          due_amount: 0,
          final_price: Number(existOrder?.final_price * 2).toFixed(2),
          partial_payment_completed: true,
        },
        { where: { order_id: existOrder?.order_id } },
      );
      let order = await Order.findOne({ where: { order_id: order_id } });
      return { code: EC200, msg: 'order confirmed', data: order };
    }
    {
      return { code: EC404, msg: "Payment pending or order didn't confirmed.", data: null };
    }
  }
}
