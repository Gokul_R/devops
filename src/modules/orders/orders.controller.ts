import { Controller, Get, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, Headers, Req } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { ConfirmOrderDto, CreateFreelancerOrderDto, CreateOrderDto, DownloadPdfDto } from './dto/create-order.dto';
import { CancelOrderDto } from './dto/update-order.dto';
import { RescheduleOrderDto } from './dto/RescheduleOrderDto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM106, EM116, EM126 } from 'src/core/constants';
import { RazorPayWebhookPayload } from './dto/webhook-payload.dto';
@ApiTags('Orders')
@Controller('/orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post('/create')
  @ApiOperation({ summary: '{(Order create for business) and (freelancer 1st create order) }' })
  async create(@Body() createOrderDto: CreateOrderDto , @Req() req:any) {
    try {
      let { code, msg, data } = await this.ordersService.createNewOrder(createOrderDto, req);
      return HandleResponse.buildSuccessObj(code, msg, data);
    } catch (error) {
      let e = HandleResponse.buildErrObj(error.status, error.message, error);
      return e;
    }
  }

  @Post('/create-freelancer-order')
  @ApiOperation({ summary: '{Freelancer second create order}' })
  async createFreelancerOrder(@Body() createOrderDto: CreateFreelancerOrderDto , @Req() req:any) {
    try {
      let { code, msg, data } = await this.ordersService.createFreelancerOrder(createOrderDto, req);
      return HandleResponse.buildSuccessObj(code, msg, data);
    } catch (error) {
      let e = HandleResponse.buildErrObj(error.status, error.message, error);
      return e;
    }
  }
  
  @Post('/confirm')
  async confirmOrder(@Body() req: ConfirmOrderDto) {
    try {
      let { code, msg, data } = await this.ordersService.confirmOrder(req);
      return HandleResponse.buildSuccessObj(code, msg, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  // @Post('/refund')
  // async refund(@Body() req: InitateRefundDto) {
  //   try {
  //     let  data  = await this.ordersService.initiateRefund(req);
  //     return HandleResponse.buildSuccessObj(EC200, EM104, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Get('details/:order_id')
  async getOrderDetails(@Param('order_id') order_id: string) {
    try {
      let data = await this.ordersService.getOrderDetails(order_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':user_id/:status')
  async findByUserId(@Param('user_id') id: string, @Param('status') status: number) {
    try {
      let { code, msg, data } = await this.ordersService.getAppointments(id, status);
      return HandleResponse.buildSuccessObj(code, msg, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch('/pay-by-cash/:order_id')
  async PaybyCash(@Param('order_id') order_id: string) {
    try {
      let  { code, msg, data }  = await this.ordersService.PaybyCash(order_id);
      return HandleResponse.buildSuccessObj(code, msg, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch('cancel/:order_id')
  async cancelOrder(@Param('order_id') id: string, @Body() cancelOrderDto: CancelOrderDto) {
    try {
      let data = await this.ordersService.cancelOrder(id, cancelOrderDto, false);
      return HandleResponse.buildSuccessObj(EC200, EM126, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('download_pdf')
  async downloadPdf(@Body() downloadPdf: DownloadPdfDto, @Headers('origin') origin: string) {
    try {
      let data = await this.ordersService.downloadPdf(downloadPdf);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('confirm-order/webhook')
  async confirmOrderWebhook(@Body() payload: any) {
    try {
      let data = await this.ordersService.confirmOrderWebhook(payload);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('order-failed/webhook')
  async OrderFailedWebhook(@Body() payload: any) {
    try {
      let data = await this.ordersService.orderFailedWebhook(payload);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch('reschedule/:order_id')
  async reschedulelOrder(@Param('order_id') id: string, @Body() rescheduleOrderDto: RescheduleOrderDto) {
    try {
      let data = await this.ordersService.rescheduleOrder(id, rescheduleOrderDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      if (error.name === 'Error') return HandleResponse.buildErrObj(null, error.message, error);
      else return HandleResponse.buildErrObj(error?.status, error.message, error);
    }
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ordersService.destroy(id);
  }
}
