import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class Notes {
  @ApiProperty()
  participant_name: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  phone: string;

  @ApiProperty()
  vendor_type: string;

  @ApiProperty()
  partialPayment: boolean;

  @ApiProperty()
  order_id: string;
}
export class Entity {
  @ApiProperty()
  id: string;

  @ApiProperty()
  entity: string;

  @ApiProperty()
  amount: number;

  @ApiProperty()
  currency: string;

  @ApiProperty()
  status: string;

  @ApiProperty()
  order_id: string;

  @ApiProperty()
  invoice_id: any;

  @ApiProperty()
  international: boolean;

  @ApiProperty()
  method: string;

  @ApiProperty()
  amount_refunded: number;

  @ApiProperty()
  refund_status: any;

  @ApiProperty()
  captured: boolean;

  @ApiProperty()
  description: any;

  @ApiProperty()
  card_id: any;

  @ApiProperty()
  bank: any;

  @ApiProperty()
  wallet: any;

  @ApiProperty()
  vpa: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  contact: string;

  @ApiProperty()
  notes: Notes;

  @ApiProperty()
  fee: number;

  @ApiProperty()
  tax: number;

  @ApiProperty()
  error_code: any;

  @ApiProperty()
  error_description: any;

  @ApiProperty()
  created_at: number;
}
export class Payment {
  @ApiProperty()
  entity: Entity;
}
export class Payload {
  @ApiProperty()
  payment: Payment;
}





export class RazorPayWebhookPayload {
  @ApiProperty()
  entity: string;

  @ApiProperty()
  account_id: string;

  @ApiProperty()
  event: string;

  @ApiProperty()
  contains: string[];

  @ApiProperty()
  payload: Payload;

  @ApiProperty()
  created_at: number;
}
