import { PartialType } from '@nestjs/mapped-types';
import { CreateOrderDto } from './create-order.dto';
import { IsDate, IsEnum, IsInt, IsNotEmpty, IsOptional, IsString, ValidateIf } from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { VendorType } from 'src/core/utils/enum';

export class RescheduleOrderDto {
  @ApiProperty()
  @IsDate()
  @Transform(({ value }) => value ? new Date(value) : null)
  appointment_start_date_time: Date | null;

  @ApiProperty({ example: `${VendorType.FREELANCER} 'or' ${VendorType.BUSINESS}` })
  @IsNotEmpty()
  @IsEnum(VendorType)
  vendor_type: string;

  @ApiProperty()
  @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  @IsInt()
  booking_slot: number;
}
