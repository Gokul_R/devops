import { ApiProperty, OmitType, PickType } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsString,
  IsUUID,
  IsArray,
  IsEnum,
  IsNumberString,
  IsNumber,
  IsOptional,
  IsNotEmpty,
  IsDate,
  ValidatorConstraintInterface,
  ValidatorConstraint,
  Validate,
  ValidationOptions,
  ValidationArguments,
  IsInt,
  ValidateIf,
} from 'class-validator';
import { VendorType, FreelancerSlot } from 'src/core/utils/enum';

enum ServiceType {
  Home = 'Home',
  SHOP = 'Shop',
  // Add other service types here
}

@ValidatorConstraint({ name: 'isCouponIdValid', async: false })
export class IsCouponIdValidConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments): boolean {
    let dto: any = args.object;
    // Check if coupon_discount has a value (not null or undefined)
    if (value) {
      // If coupon_discount has a value, applied_coupon_id should not be null
      let t = dto?.applied_coupon_id !== null && dto?.applied_coupon_id !== '';
      return t;
    }
    return true; // Validation passes if coupon_discount is not provided
  }

  defaultMessage(args: ValidationArguments): string {
    // console.log('------->', args);
    return 'applied_coupon_id should not be null when coupon_discount has a value';
  }
}

export class CreateOrderDto {
  @ApiProperty({ example: '7756e9ce-be14-40fc-a7b9-aec16ff8fdaa' })
  // @IsUUID()
  @IsString()
  user_id: string;

  device: string;

  @ApiProperty({ example: 'fe8c8323-b471-4fbe-81bc-36a36563a016' })
  // @IsUUID()
  @IsString()
  branch_id: string;

  @ApiProperty()
  @IsOptional()
  applied_coupon_id: string;

  @ApiProperty()
  @IsOptional()
  gst: number;

  @ApiProperty()
  @IsOptional()
  service_type: string;

  @ApiProperty()
  @IsOptional()
  @Validate(IsCouponIdValidConstraint, {
    context: this,
    message: 'applied_coupon_id should not be null when coupon_discount has a value',
  })
  coupon_discount: number;

  @ApiProperty({ example: ['0e3e4d77-69ba-4b93-a445-b365897882ca'] })
  @IsArray()
  // @IsUUID(undefined, { each: true })
  service_ids: string[];

  @ApiProperty()
  @IsNumber()
  original_price: number;

  @ApiProperty()
  @IsNumber()
  discount: number;

  @ApiProperty()
  @IsNumber()
  final_price: number;

  @ApiProperty()
  @IsDate()
  @Transform(({ value }) => value ? new Date(value) : null)
  appointment_start_date_time?: Date | null;

  @ApiProperty()
  @IsNumber()
  total_services: number;

  @ApiProperty()
  @IsString()
  payment_method: string;

  @ApiProperty({ example: 'Testuser' })
  @IsString()
  customer_name: string;

  @ApiProperty({ example: '9856234569' })
  @IsString()
  customer_mobile: string;

  @ApiProperty({ example: 'rag@gmail.com' })
  @IsString()
  customer_email: string;

  @ApiProperty({ example: `${VendorType.FREELANCER} or ${VendorType.BUSINESS}` })
  @IsNotEmpty()
  @IsEnum(VendorType)
  vendor_type: string;

  @ApiProperty({ example: 'GBWT_170903212481' })
  @IsOptional()
  @IsString()
  order_id: string;

  @ApiProperty({ example: FreelancerSlot.MORNING })
  @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  @IsInt()
  @IsNotEmpty()
  booking_slot: number;

  due_amount: number;
  initial_amount: number;


  @ApiProperty()
  @IsOptional()
  address_id: string;
  payment_status: string;
  order_otp: string;
  payout_amount: string;
  revenue_amount: number
  invoice_no: string;
}

export class CreateFreelancerOrderDto extends OmitType(CreateOrderDto, ['order_id', 'address_id']) {
  @ApiProperty()
  @IsNotEmpty()
  address_id: string;

  @ApiProperty({ example: 'GBWT_170903212481' })
  @IsNotEmpty()
  @IsString()
  order_id: string;

  @ApiProperty({ example: FreelancerSlot.MORNING })
  @ValidateIf((o) => o.vendor_type == VendorType.FREELANCER)
  @IsInt()
  @IsNotEmpty()
  booking_slot: number;

}
export class ConfirmOrderDto extends PickType(CreateOrderDto, ['user_id']) {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  order_id: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  payment_id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  payment_order_id: string;

  @ApiProperty({ example: VendorType.FREELANCER })
  @IsOptional()
  @IsEnum(VendorType)
  vendor_type: string;
}
export class DownloadPdfDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  user_id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  order_id: string;
}
