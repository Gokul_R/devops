import { PartialType } from '@nestjs/mapped-types';
import { CreateOrderDto } from './create-order.dto';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { ApiProperty, PickType } from '@nestjs/swagger';

export class CancelOrderDto {
  @ApiProperty({ example: 'Test reason' })
  @IsString()
  cancellation_reason: string;

  @ApiProperty({ example: 'Test comment' })
  @IsString()
  comment: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  order_status?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  cancelled_user_id?: string;

  //if user cancelled order it will true
  @ApiProperty()
  @IsBoolean()
  @IsOptional()
  on_back?: boolean = false;
}

export class InitateRefundDto extends PickType(CreateOrderDto, [
  'appointment_start_date_time',
  'user_id',
  'order_id',
]) {}
