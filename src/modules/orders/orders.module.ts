import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { CartModule } from '../cart/cart.module';
import { orderProviders } from './orders.provider';
import { MailUtils } from 'src/core/utils/mailUtils';
import { UsersModule } from '../users/users.module';
import { PaymentModule } from '../payment/payment.module';
import { CouponModule } from '../coupon/coupon.module';
import { SharedModule } from '../shared/shared.module';
import { ScheduleModule } from 'nestjs-schedule';
import { SchedulerRegistry } from '@nestjs/schedule';
import { NotificationModule } from '../notification/notification.module';
import { NotificationService } from '../notification/notification.service';
import { BranchService } from '../shop/branch.service';
import { BranchModule } from '../shop/branch.module';
import { ServicesModule } from '../services/services.module';
import { SlotManagementModule } from '../slot-management/slot-management.module';
import { SlotManagementService } from '../slot-management/slot-management.service';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    CartModule,
    UsersModule,
    PaymentModule,
    CouponModule,
    SharedModule,
    NotificationModule,
    BranchModule,
    ServicesModule,
    SlotManagementModule,
  ],
  controllers: [OrdersController],
  providers: [
    OrdersService,
    ...orderProviders,
    SlotManagementService, 
    MailUtils,
    SchedulerRegistry,
    NotificationService,
    BranchService,
  ],
  exports: [OrdersService, NotificationService, BranchService],
})
export class OrdersModule {}
