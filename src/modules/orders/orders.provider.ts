import { Sequelize } from 'sequelize-typescript';
import { SEQUELIZE } from 'src/core/constants';
import { commonProvider } from 'src/core/interfaces/common-providers';

export const orderProviders = [
  // {
  //     provide: USER_REPOSITORY,
  //     useValue: User,
  // },
  {
    provide: SEQUELIZE,
    useValue: Sequelize,
  },
  commonProvider.branch_repository,
  commonProvider.order_services_repo,
  commonProvider.location_repo,
  commonProvider.coupon_repository,
  commonProvider.coupon_branch_repository,
  commonProvider.branch_gallary_repository,
  commonProvider.branch_gallery_folder_repository
];
