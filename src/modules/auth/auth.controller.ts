import { Controller, Body, Post, UseGuards, Request, HttpCode, UsePipes, ValidationPipe, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { AuthService } from './auth.service';
import { UpdateUserDto } from '../users/dto/user.dto';
import { DoesUserExist } from '../../core/guards/doesUserExist.guard';
import { ApiTags } from '@nestjs/swagger';
import { SendOtpDto } from './dto/send-otp.dto';
import { VerifyOtpDto } from './dto/verify-otp.dto';
import { EC200, EC204, EC401, EC500, EM100, EM106, EM107, EM127, EM134, EM141, EM149 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { logger } from 'src/core/utils/logger';
import { DeleteDto, LoginDto, userlogoutDto } from './dto/login-dto';
import { LogoutDto } from 'src/admin/dto/logOut.dto';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private authService: AuthService) { }

  // @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Body() req: LoginDto) {
    try {
      let data = await this.authService.login(req);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(error.status, EM100, error);
    }
  }

  @Post('verify-mobile-no')
  async verifyMobileNo(@Body() user_id: string) {
    try {
      let data = await this.authService.verifyMobileNo(user_id);
      return HandleResponse.buildSuccessObj(EC401, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(error.status, EM100, error);
    }
  }
  @Post('sendOtp')
  async sendOtp(@Body() body: SendOtpDto) {
    try {
      let data = await this.authService.sendOtp(body);
      if (data && data.code == EC401) {
        logger.info(`User_Account_Disabled:  ` + JSON.stringify(body));
        return HandleResponse.buildSuccessObj(EC401, EM134, data);
      }
      return HandleResponse.buildSuccessObj(EC200, EM107, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }
  @Post('verifyOtp')
  async verifyOtp(@Body() body: VerifyOtpDto) {
    try {
      let { code, msg, data } = await this.authService.verifyOtp(body);
      return HandleResponse.buildSuccessObj(code, msg, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }
  @Post('forgot-password/:email_id')
  async forgotPassword(@Param('email_id') email_id: string) {
    try {
      let data = await this.authService.forgotPassword(email_id);
      return HandleResponse.buildSuccessObj(EC200, EM141, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500 || error.status, error?.message || EM100, error);
    }
  }

  @Post('destroy')
  async deleteUser(@Body() deleteDto: DeleteDto) {
    try {
      let data: any = await this.authService.deleteUserData(deleteDto);
      if (data && data?.code === EC204) return data;
      return HandleResponse.buildSuccessObj(EC200, EM127, null);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500 || error.status, error?.message || EM100, error);
    }
  }

  @Post('logout')
  async logout(@Body() logoutDto: userlogoutDto) {
    try {
      let data = await this.authService.logout(logoutDto);
      return HandleResponse.buildSuccessObj(EC200, EM149, null);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500 || error.status, error?.message || EM100, error);
    }
  }
}
