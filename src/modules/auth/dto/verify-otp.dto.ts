import { PartialType } from '@nestjs/mapped-types';
import { SendOtpDto } from './send-otp.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsNumberString, IsOptional, IsUUID, Length } from 'class-validator';

export class VerifyOtpDto extends PartialType(SendOtpDto) {
    @ApiProperty({ example: "ff41c19f-df22-4052-8800-25dc4418099d" })
    @IsNotEmpty()
    // @IsUUID()
    user_id: string;

    @ApiProperty({ example: 1234 })
    @IsNotEmpty()
    @IsNumberString()
    @Length(4, 4)
    otp: string;

    @ApiProperty({example:true})
    // @IsNotEmpty()
    @IsBoolean()
    @IsOptional()
    is_mobile_verified:boolean

    @ApiProperty({ example: "" })
    @IsOptional()
    device_token: string;
}
