import { ApiProperty, PartialType } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString } from "class-validator";
import { BRANCH_ID } from "src/core/constants";
import { SharedDto } from "src/core/interfaces/shared.dto";

export class LoginDto {
  @ApiProperty({ example: "sample@gmail.com" })
  @IsNotEmpty()
  email_id: string;

  @ApiProperty()
  @IsNotEmpty()
  password: string;

  @ApiProperty({ example: "" })
  @IsOptional()
  @IsString()
  device_token: string;
}

export class DeleteDto {
  @ApiProperty({ example: 'Enter your email (or) mobile number include (+91)' })
  @IsNotEmpty()
  identity: string;
}

export class userlogoutDto {
  @ApiProperty({ example: BRANCH_ID })
  @IsNotEmpty()
  @IsString()
  user_id: string;
}