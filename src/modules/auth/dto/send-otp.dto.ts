import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class SendOtpDto {
  @ApiProperty({example:"+917010132357"})
  @IsNotEmpty()
  mobile_no: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  user_id:string

  @ApiProperty()
  @IsOptional()
  @IsString()
  otp_sha_key:string
}
