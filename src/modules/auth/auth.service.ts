import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { UsersService } from '../users/users.service';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EC204, EC401, EC410, EC500, EM101, EM103, EM104, EM105, EM109, EM132, EM134 } from 'src/core/constants';
import moment from 'moment';
import Helpers from 'src/core/utils/helpers';
import User from 'src/core/database/models/User';
import { failiure, success } from 'src/core/interfaces/response.interface';
import { SendOtpDto } from './dto/send-otp.dto';
import { VerifyOtpDto } from './dto/verify-otp.dto';
import { TextLocalSMS } from 'src/core/utils/textlocal';
import { logger } from 'src/core/utils/logger';
import { DeleteDto, LoginDto, userlogoutDto } from './dto/login-dto';
import { Errors } from 'src/core/constants/error_enums';
import Encryption from 'src/core/utils/encryption';
import { MailUtils } from 'src/core/utils/mailUtils';
import { Op } from 'sequelize';
import Favourites from 'src/core/database/models/Favourites';
import UserNotification from 'src/core/database/models/UserNotification';
import Review from 'src/core/database/models/Reviews';
import Cart from 'src/core/database/models/Cart';
import ReviewReply from 'src/core/database/models/ReviewReply';
import Otp from 'src/core/database/models/Otp';
import sequelize from 'sequelize';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
    private readonly textLocalService: TextLocalSMS,
    private readonly MailUtilsService: MailUtils,
  ) { }

  async validateUser(username: string, pass: string) {
    // find if user exist with this email
    const user = await this.userService.findOneByPhone(username);
    if (!user) {
      return null;
    }

    // tslint:disable-next-line: no-string-literal
    const { ...result } = user['dataValues'];
    return result;
  }

  async sendOtp(body: SendOtpDto) {
    logger.info(`User_Sent_OTP_Entry_MobileNo: ` + JSON.stringify(body));
    let { mobile_no, otp_sha_key, user_id } = body || {};
    if (user_id) await this.userService.checkMobileExists(mobile_no, user_id);
    let user: User = await this.userService.findOneByPhone(mobile_no);
    if (user && user?.is_blocked) {
      logger.info(`User_Account_Disabled: ` + JSON.stringify(body));
      return { code: EC401, message: EM134 };
    }
    let otp = await Helpers.generateOTP();
    let otprequest = {};
    if (user) {
      otprequest = {
        userId: user.id,
        otp: otp,
        created_at: moment().utc().format(),
        expires_at: moment().add(5, 'minutes').utc().format(),
      };
    } else {
      user = await this.userService.create({
        mobile_no: mobile_no,
      });
      // console.log('otpResultotpResult', otpResult);
      otprequest = {
        userId: user.id,
        otp: otp,
        created_at: moment().utc().format(),
        expires_at: moment().add(5, 'minutes').utc().format(),
      };
    }
    await Otp.destroy({ force: true, where: { user_id: user.id } });
    let otp_res = await this.userService.createOtp(otprequest);
    if (otp_res) {
      let r = await this.textLocalService.sendSMS(
        mobile_no,
        `Dear user, use this One Time Password ${otp} to verify your mobile number for Slaylewks account.`,
        // 'Slayelwks login otp is ' + otp,
      );
      // console.log(r);
      logger.info(`User_Sent_OTP_response:` + JSON.stringify(r));
      logger.info(`User_Sent_OTP_Exit_UserId:` + JSON.stringify(otp_res.userId) + `OTP: ` + JSON.stringify(otp));
      return {
        userId: otp_res.userId,
        // otp: otp,
      };
    }
  }
  async verifyOtp(body: VerifyOtpDto) {
    logger.info(`Verify_Otp_Entry: ` + JSON.stringify(body));
    let request = body;
    let result = await this.userService.findOtpById(request);
    if (result) {
      let now = moment().utc();
      let expires = result.expires_at;
      let difference = now.diff(moment.utc(expires), 'seconds');
      if (difference > 0) {
        logger.info(`Verify_OTP_Code_Expired: ` + JSON.stringify(body));
        return { code: EC410, msg: EM132 };
      } else {
        let user: any = await this.userService.findOneById(request.user_id);
        let token = null;
        // console.log(user);
        if (user) {
          token = await this.generateToken({ user_id: user.id });
        }
        await this.userService.deleteOtp(request.user_id);
        await this.userService.updateProfile(body);
        logger.info(`Verify_OTP_Login_Successfully: ` + JSON.stringify(user));
        return { code: EC200, msg: EM103, data: user };
      }
    } else {
      logger.info(`Verify_OTP_Invalid_Credentials: ` + JSON.stringify(body));
      return { code: EC204, msg: EM109 };
    }
    // const user = await models.User.findOne();
  }

  public async login(userData: LoginDto): Promise<User> {
    const user: User = await this.userService.findOneByEmail(userData.email_id);
    if (!user) throw new NotFoundException(Errors.USER_NOT_EXISTS);
    if (!user.password) throw new UnauthorizedException(Errors.INCORRECT_USER_PASSWORD);
    if (!Encryption.comparePassword(userData.password, user.password))
      throw new UnauthorizedException(Errors.INVALID_USER_DETAILS);
    const token = await this.generateToken(user);
    const updatedUser = await this.userService.update(user.id, { device_token: userData?.device_token });
    return updatedUser && updatedUser[0] || user;
  }
  public async verifyMobileNo(user_id: string) {
    let user = await this.userService.update(user_id, { is_mobile_verified: true });
    return user;
  }
  private async generateToken(user: any) {
    // console.log('secret----->', process.env.JWTKEY);
    const token = await this.jwtService.signAsync(user, {
      secret: process.env.JWTKEY,
      expiresIn: '1h',
    });
    return token;
  }
  async forgotPassword(email_id: string) {
    let user: User = await this.userService.findOneByEmail(email_id);
    if (!user) throw new NotFoundException(Errors.INVALID_USER_DETAILS);
    let otp = Helpers.generateOTP();
    let otpRequest = {
      userId: user.id,
      otp: otp,
      created_at: moment().utc().format(),
      expires_at: moment().add(5, 'minutes').utc().format(),
    };
    await Otp.destroy({ force: true, where: { user_id: user.id } });
    let result = await this.userService.createOtp(otpRequest);
    let res = await this.MailUtilsService.sendConfirmationOtpToMail(otp, email_id);
    return { user_id: user.id };
  }

  async deleteUserData(req_data: DeleteDto) {
    const checkIfUserNotExit = await Helpers.findAll(User, {
      where: {
        [Op.or]: [
          {
            email_id: req_data.identity,
          },
          {
            mobile_no: req_data.identity,
          },
        ],
      },
    });
    if (checkIfUserNotExit && checkIfUserNotExit?.length <= 0) return { code: EC204, status: false, message: EM101 };
    else {
      let userId = checkIfUserNotExit?.map((id) => {
        return id.id;
      });
      const deleteDeviceToken = await User.update({ device_token: null }, {
        where: {
          id: {
            [Op.in]: userId,
          },
        },
      });
      const deteUserAC = await Helpers.softDelete(User, {
        where: {
          id: {
            [Op.in]: userId,
          },
        },
      });
      const deleteFav = await Favourites.destroy({
        force: true,
        where: {
          user_id: {
            [Op.in]: userId,
          },
        },
      });
      const deleteUserNotify = await UserNotification.update(
        {
          user_id: sequelize.literal(`array_remove("user_id", '${userId[0]}')`)
        },
        {
          where: {
            user_id: {
              [Op.contains]: [userId[0]]
            }
          }
        }
      );
      // // Then, delete the records in the reviews table
      // const deleteUserReviews = await Review.destroy({
      //   force: true,
      //   where: {
      //     user_id: {
      //       [Op.in]: userId
      //     }
      //   }
      // });
      const deleteUserCart = await Cart.destroy({
        force: true,
        where: {
          user_id: {
            [Op.in]: userId,
          },
        },
      });
      return deteUserAC;
      // notification// orders// applied coupon// revies // revie reply// otp// user address// favroute// collected_coupons// cart// appointment
    }
  }

  async logout(req: userlogoutDto) {
    let userLogout = await User.update({ device_token: null }, { where: { id: req.user_id } });
    return userLogout;
  }
}
