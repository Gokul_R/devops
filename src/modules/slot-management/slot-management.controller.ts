import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { SlotManagementService } from './slot-management.service';
import { CreateSlotManagementDto, ReserveSlotDto } from './dto/create-slot-management.dto';
import { UpdateSlotManagementDto } from './dto/update-slot-management.dto';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106 } from 'src/core/constants';
import { ApiTags } from '@nestjs/swagger';

@Controller('slot-management')
@ApiTags('Slot Management')
export class SlotManagementController {
  constructor(private readonly slotManagementService: SlotManagementService) {}

  // @Post()
  // async create(@Body() createSlotManagementDto: CreateSlotManagementDto) {
  //   try {
  //     let data = await this.slotManagementService.create(createSlotManagementDto);
  //     return HandleResponse.buildSuccessObj(EC200, EM104, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  // @Get('all-slots/:id')
  // async findAll(@Param('id') id: string) {
  //   try {
  //     let data = await this.slotManagementService.findAllSlots(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.slotManagementService.findOneByBranch(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('reserve-slot')
  async reserveSlot(@Body() reserveSlotDto: ReserveSlotDto) {
    try {
      let data = await this.slotManagementService.reserveSlot(reserveSlotDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Get('get-reserved-slots/:branch_id/:slot_date_time')
  async getReserveSlots(@Param('branch_id') branch_id:string,@Param("slot_date_time") slot_date_time:any) {
    try {
      let data = await this.slotManagementService.getReserveSlot(branch_id,slot_date_time);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateSlotManagementDto: UpdateSlotManagementDto) {
  //   return this.slotManagementService.update(id, updateSlotManagementDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.slotManagementService.remove(id);
  // }
}
