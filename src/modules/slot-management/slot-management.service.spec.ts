import { Test, TestingModule } from '@nestjs/testing';
import { SlotManagementService } from './slot-management.service';

describe('SlotManagementService', () => {
  let service: SlotManagementService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SlotManagementService],
    }).compile();

    service = module.get<SlotManagementService>(SlotManagementService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
