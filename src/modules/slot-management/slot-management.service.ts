import { Injectable } from '@nestjs/common';
import { CreateSlotManagementDto, ReserveSlotDto } from './dto/create-slot-management.dto';
import { UpdateSlotManagementDto } from './dto/update-slot-management.dto';
import { ModelCtor } from 'sequelize-typescript';
import SlotManagement from 'src/core/database/models/SlotManagement';
import { BaseService } from 'src/base.service';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { Op, col } from 'sequelize';
import { logger } from 'src/core/utils/logger';
import ReservedSlots from 'src/core/database/models/ReservedSlots';

@Injectable()
export class SlotManagementService extends BaseService<SlotManagement> {
  slot_management_qry: SequelizeFilter<SlotManagement>;
  protected model: ModelCtor<SlotManagement>;
  constructor() {
    super();
    this.model = SlotManagement;
  }

  init() {
    this.slot_management_qry = { where: {}, order: [['created_at', 'DESC']] };
  }
  async create(createSlotManagementDto: CreateSlotManagementDto): Promise<SlotManagement> {
    return await super.create(createSlotManagementDto);
  }

  async findAllSlots(branch_id: string) {
    this.init();
    this.slot_management_qry.where.branch_id = branch_id;
    let data = await super.findAll(this.slot_management_qry);
    return data;
  }

  async findOneByBranch(branch_id: string, date?: any): Promise<SlotManagement> {
    logger.info(`Find_One_ByBranch_Entry_branchId:` + JSON.stringify(branch_id) + 'data:' + JSON.stringify(date));
    this.init();

    let filter = {
      ...this.slot_management_qry.where,
      branch_id: branch_id,
    };
    if (date) {
      const startOfDay = new Date(date);
      startOfDay.setHours(0, 0, 0, 0);

      const endOfDay = new Date(date);
      endOfDay.setHours(23, 59, 59, 999);
      filter['date'] = startOfDay;
      // {
      //   [Op.gte]: startOfDay, // Greater Than or Equal to the start of the day
      //   [Op.lt]: endOfDay, // Less Than the end of the day
      // };
    }
    this.slot_management_qry.where = filter;
    let data = await super.findOne(null, this.slot_management_qry);
    logger.info(`Find_One_ByBranch_Exit: ` + JSON.stringify(data));
    return data;
  }

  async update(id: string, updateSlotManagementDto: UpdateSlotManagementDto): Promise<SlotManagement> {
    return await super.update(id, updateSlotManagementDto);
  }

  async reserveSlot(reserveSlotDto: ReserveSlotDto) {
    let reserved_slots: ReservedSlots = await ReservedSlots.create({ ...reserveSlotDto });
    return reserved_slots;
  }
  async UnreserveSlot(id: string) {
    let removedSlots: number = await ReservedSlots.destroy({ where: { id: id } });
    return removedSlots;
  }
  async getReserveSlot(branch_id: string, date: Date) {
    const startOfDay = new Date(date);
    startOfDay.setHours(0, 0, 0, 0);

    const endOfDay = new Date(date);
    endOfDay.setHours(23, 59, 59, 999);
    let reserved_slots: ReservedSlots = await ReservedSlots.findOne({
      where: {
        branch_id: branch_id,
        appointment_start_date_time: {
          [Op.gte]: startOfDay,
          [Op.lt]: endOfDay,
        },
      },
    });
    return reserved_slots;
  }

  //  async remove(id: number) {
  //     return `This action removes a #${id} slotManagement`;
  //   }
}
