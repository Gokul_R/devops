import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsString, IsDate, IsInt, IsBoolean } from 'class-validator';

export class CreateSlotManagementDto {
  @ApiProperty()
  @IsString()
  branch_id: string;

  @ApiProperty()
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsDate()
  start_date: Date | null;

  @ApiProperty()
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsDate()
  end_date: Date | null;

  @ApiProperty()
  @IsInt()
  no_of_staff: number;

  @ApiProperty()
  @IsBoolean()
  is_leave: boolean;

  @ApiProperty()
  @IsString()
  slot_start_time: string;

  @ApiProperty()
  @IsString()
  slot_end_time: string;
}

export class ReserveSlotDto {
  @ApiProperty()
  @IsString()
  branch_id: string;

  @ApiProperty()
  @Transform(({ value }) => new Date(value))
  @IsDate()
  appointment_start_date_time: Date;

  @ApiProperty()
  @IsString()
  user_id: string;

  @ApiProperty()
  @IsBoolean()
  is_reserved: boolean;
}
