import { Controller, Get, Post, Body, Param, Delete, Query } from '@nestjs/common';
import { CartService } from './cart.service';
import { CreateCartDto, GetCartByIdDto } from './dto/create-cart.dto';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM127 } from 'src/core/constants';
import { ForeignKeyConstraintError } from 'sequelize';
import { DeleteCartDto } from './dto/update-cart.dto';
@ApiTags('Cart')
@Controller('cart')
export class CartController {
  constructor(private readonly cartService: CartService) {}

  @Post()
  // @UseGuards(DoesCartExist)
  async create(@Body() createCartDto: CreateCartDto) {
    try {
      let { data } = await this.cartService.create(createCartDto);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      if (error instanceof ForeignKeyConstraintError && error.message.includes('cart_user_id_fkey')) {
        // Customize the error message here
        error.message = 'Invalid User. Please Logout And Login Again.';
        return HandleResponse.buildErrObj(null, error.message, error);
      } else return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get()
  async findAll() {
    try {
      let data = await this.cartService.findAll();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('/user_id')
  async getCartItems(@Body()getCartByIdDto : GetCartByIdDto) {
    try {
      let data = await this.cartService.getCartItems(getCartByIdDto?.user_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Patch(':id')
  // async update(@Param('id') id: string, @Body() updateCartDto: UpdateCartDto) {
  //   try {
  //     console.log('inside cart controller update=======>')
  //     let data = await this.cartService.getCartItems(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM116, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @ApiQuery({
    name: 'cart_id',
    type: String,
    description: 'Cart Id',
    required: false,
  })
  @Delete(':user_id/')
  async removeCart(@Param('user_id') user_id: string, @Query('cart_id') cart_id: string) {
    try {
      let data = await this.cartService.removeCart(cart_id, user_id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Delete('/multiple-delete/:user_id')
  async multipleDelete(@Param('user_id') user_id: string, @Body() deleteCartDto: DeleteCartDto) {
    try {
      let data = await this.cartService.deleteMany(deleteCartDto.cart_ids, user_id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
