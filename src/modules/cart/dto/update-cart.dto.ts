import { PartialType } from '@nestjs/mapped-types';
import { CreateCartDto } from './create-cart.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsString } from 'class-validator';

export class UpdateCartDto extends PartialType(CreateCartDto) {}
export class DeleteCartDto  {
      @ApiProperty()
      @IsArray()
      @IsString({ each: true })
      cart_ids:string[]
}
