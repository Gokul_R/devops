import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCartDto {
  @ApiProperty()
  @IsNotEmpty()
  user_id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  branch_id: string;

  @ApiProperty()
  @IsNotEmpty()
  service_ids: Array<string>;
}

export class GetCartByIdDto{
@ApiProperty()
@IsNotEmpty()
@IsString()
user_id:string
}
