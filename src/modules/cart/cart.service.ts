import { Inject, Injectable } from '@nestjs/common';
import { CreateCartDto } from './dto/create-cart.dto';
import { UpdateCartDto } from './dto/update-cart.dto';
import Helpers from 'src/core/utils/helpers';
import Cart from 'src/core/database/models/Cart';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM104, CART_REPOSITORY, EM106, EC409, EM100 } from 'src/core/constants';
import Service from 'src/core/database/models/Services';
import Branch from 'src/core/database/models/Branch';
import Location from 'src/core/database/models/Location';
import { UsersService } from '../users/users.service';
import AppliedCoupon from 'src/core/database/models/AppliedCoupons';
import Coupon from 'src/core/database/models/Coupons';
import { CouponService } from '../coupon/coupon.service';
import User from 'src/core/database/models/User';
import { failiure, success } from 'src/core/interfaces/response.interface';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { BaseService } from 'src/base.service';
import { ModelCtor } from 'sequelize-typescript';
import { COUPON_ATTRIBUTES } from 'src/core/attributes/coupon';
import { LOCATION_ATTRIBUTES } from 'src/core/attributes/location';
import FlashSale from 'src/core/database/models/FlashSale';
import { FlashSaleConstants } from '../flash-sale/flashsale-constants';
import BranchServiceTypeMapping from 'src/core/database/models/BranchServiceTypeMapping';
import MasterServiceType from 'src/core/database/models/MasterServiceType';
import MasterCategory from 'src/core/database/models/MasterCategory';
import { MASTER_CATEGORIES_ATTRIBUTES } from 'src/core/attributes/master_category';
import { logger } from 'src/core/utils/logger';
import Vendor from 'src/core/database/models/Vendor';
import { VENDOR_ATTRIBUTES } from 'src/core/attributes/vendor';

@Injectable()
export class CartService extends BaseService<Cart> {
  protected model: ModelCtor<Cart>;
  constructor(
    @Inject(CART_REPOSITORY) private readonly cartRepository: typeof Cart,
    // private readonly userService: UsersService,
    private readonly couponService: CouponService,
  ) {
    super();
    this.model = cartRepository;
  }

  async create(createCartDto: CreateCartDto) {
    //check cart exists for user
    const cartExistForUser = await Helpers.findOne(this.cartRepository, {
      where: {
        user_id: createCartDto.user_id,
      },
    });
    //check cart exists for user and branch
    const cartExistForBranch = await Helpers.findOne(this.cartRepository, {
      where: {
        user_id: createCartDto.user_id,
        branch_id: createCartDto.branch_id,
      },
    });
    //if cart not exists allow them to create cart
    if (!cartExistForUser) {
      // let cart_id = Helpers.generateCartId();
      return await this.insertData(createCartDto);
    } else {
      //check same branch record is exists
      if (cartExistForBranch) {
        // let cart: Cart = await super.create(createCartDto);
        return await this.insertData(createCartDto);
      } else return { code: EC409, msg: 'Please Clear Your Cart' };
    }
  }
  async insertData(createCartDto: CreateCartDto) {
    let cartDto = createCartDto.service_ids.map((serviceId) => {
      return {
        user_id: createCartDto.user_id,
        branch_id: createCartDto.branch_id,
        service_id: serviceId,
        // cart_id: cart_id,
      };
    });
    // console.log('----->', cartDto);
    logger.info('InsercartDto: ' + JSON.stringify(cartDto));
    let cart = await super.bulkCreate(cartDto);
    return HandleResponse.buildSuccessObj(EC200, EM104, cart);
  }
  // async findAll() {
  //   return `This action returns all cart`;
  // }

  async getCartItems(id: string): Promise<any> {
    let cart_qry = {
      attributes: ['id', 'branch_id', 'createdAt'],
      where: { user_id: id },
      include: [
        {
          model: Service,
          attributes: { exclude: ['is_active', 'createdAt', 'updatedAt'] },
          include: [
            {
              attributes: {
                exclude: ['createdAt', 'updatedAt', 'is_active', 'is_deleted', 'deletedAt'],
              },
              model: FlashSale,
              where: FlashSaleConstants.getFlashSaleQry(),
              required: false,
            },
            { attributes: [MASTER_CATEGORIES_ATTRIBUTES.category], model: MasterCategory },
          ],
        },
        {
          model: Branch,
          attributes: [
            BRANCH_ATTRIBUTES.VENDOR_ID,
            BRANCH_ATTRIBUTES.BRANCH_IMAGE,
            BRANCH_ATTRIBUTES.BRANCH_NAME,
            BRANCH_ATTRIBUTES.IS_TAX_CHECKED,
            BRANCH_ATTRIBUTES.TAX_PERCENTAGE,
          ],
          include: [
            {
              attributes: [
                LOCATION_ATTRIBUTES.address,
                LOCATION_ATTRIBUTES.state,
                LOCATION_ATTRIBUTES.area,
                LOCATION_ATTRIBUTES.city,
                LOCATION_ATTRIBUTES.pincode,
              ],
              model: Location,
            },
            {
              attributes: [VENDOR_ATTRIBUTES.VENDOR_NAME,VENDOR_ATTRIBUTES.VENDOR_TYPE],
              model: Vendor,
            },
            {
              attributes: ['id'],
              model: BranchServiceTypeMapping,
              include: {
                attributes: ['service_type_name'],
                model: MasterServiceType,
              },
            },
          ],
        },
      ],
    };

    let cart_items = await Helpers.findAll(this.cartRepository, cart_qry);
    // console.log('cart_items', JSON.stringify(cart_items));

    let applied_coupon_qry = {
      attributes: ['is_applied'],
      where: {
        user_id: id,
        is_applied: true,
      },
      include: {
        attributes: [
          COUPON_ATTRIBUTES.ID,
          COUPON_ATTRIBUTES.COUPON_CODE,
          COUPON_ATTRIBUTES.DESCRIPTION,
          COUPON_ATTRIBUTES.DISCOUNT_TYPE,
          COUPON_ATTRIBUTES.DISCOUNT_VALUE,
        ],
        where: {
          deleted_at: null,
          is_active: true,
        },
        required: true,
        model: Coupon,
      },
    };
    let applied_coupons = await Helpers.findAll(AppliedCoupon, applied_coupon_qry);
    if (applied_coupons && applied_coupons.length > 0) {
      let formatted = JSON.stringify(applied_coupons);
      applied_coupons = JSON.parse(formatted);
      applied_coupons = await applied_coupons.map((data) => {
        data = { ...data, ...data.coupon };
        delete data.coupon;
        return data;
      });
    }
    //remove from cart if service deleted
    const updatedCartItems = await Promise.all(
      cart_items.map(async (cart) => {
        if (!cart?.services) {
          await this.removeCart(cart?.id);
          return null; // Remove this cart item from the array
        }
        return cart;
      }),
    );
    const filteredCartItems = updatedCartItems.filter((cart) => cart !== null);

    let data = { cart_items: filteredCartItems, applied_coupons };
    return data;
  }

  // update(id: number, updateCartDto: UpdateCartDto) {
  //   return `This action updates a #${id} cart`;
  // }

  async removeCart(cart_id?: string, user_id?: string) {
    let cart_dlt_qry = {
      where: {
        id: cart_id,
        user_id: user_id,
      },
    };
    cart_id != null ? delete cart_dlt_qry.where.user_id : delete cart_dlt_qry.where.id;
    let cart = await super.destroy(cart_id, cart_dlt_qry);
    let remove_coupon = await Helpers.update(AppliedCoupon, { where: { user_id: user_id } }, { is_applied: false });
    return cart;
  }
  async deleteMany(cart_id: Array<string>, user_id: string) {
    let cart_dlt_qry = {
      where: {
        id: cart_id,
      },
    };
    let cart = await this.cartRepository.destroy(cart_dlt_qry);
    let remove_coupon = await Helpers.update(AppliedCoupon, { where: { user_id: user_id } }, { is_applied: false });
    return cart; 
  }

  async getCartCount(user_id: string): Promise<number> {
    const count: number = await this.cartRepository.count({ where: { user_id: user_id } });
    return count;
  }
}
