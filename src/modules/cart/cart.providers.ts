import { CART_REPOSITORY, USER_REPOSITORY } from '../../core/constants';
import Cart from 'src/core/database/models/Cart';
import { UsersService } from '../users/users.service';
import { commonProvider } from 'src/core/interfaces/common-providers';

export const cartProviders = [
  {
    provide: CART_REPOSITORY,
    useValue: Cart,
  },
// commonProvider.user_repo,
commonProvider.cart_service_repo,
];
