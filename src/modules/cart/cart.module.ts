import { Module } from '@nestjs/common';
import { CartService } from './cart.service';
import { CartController } from './cart.controller';
import Cart from 'src/core/database/models/Cart';
import { cartProviders } from './cart.providers';
import { UsersModule } from '../users/users.module';
import { CouponModule } from '../coupon/coupon.module';

@Module({
  imports: [CouponModule],
  controllers: [CartController],
  providers: [CartService, Cart, ...cartProviders],
  exports: [CartService],
})
export class CartModule {}
