import { Module } from '@nestjs/common';

import { sharedProviders } from './shared.providers';
import { ServicesModule } from '../services/services.module';
import { BranchModule } from '../shop/branch.module';
import { SharedService } from './shared.service';


@Module({
  imports:[ServicesModule,BranchModule],
  providers: [SharedService, ...sharedProviders],
  exports: [SharedService],
  // controllers: [ UsersController],
})
export class SharedModule {}
