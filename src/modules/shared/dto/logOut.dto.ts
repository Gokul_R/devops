import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsString } from "class-validator";
import { BRANCH_ID, Provided_By } from "src/core/constants";

export class Logout {
    @ApiProperty({ example: BRANCH_ID })
    @IsString()
    @IsNotEmpty()
    user_id: string;

    @ApiProperty({ example: `${Provided_By.SUPER_ADMIN} (or) ${Provided_By.VENDOR} (or) ${Provided_By.BRANCH}` })
    @IsString()
    @IsNotEmpty()
    @IsEnum(Provided_By)
    user_type: string;
}