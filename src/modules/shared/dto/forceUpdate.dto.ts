import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsString } from "class-validator";

export enum DEVICE_TYPE {
    ANDROID = 'Android',
    IOS = 'iOS',
    VendorAndroid = 'Vendor_Android',
    VendorIos = 'Vendor_iOS'
}
export class GetForceUpdate {

    @ApiProperty({ example: 'Android (or) iOS' })
    @IsEnum(DEVICE_TYPE)
    @IsString()
    device_type: string;
}
