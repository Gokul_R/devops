import { Inject, Injectable } from '@nestjs/common';
import moment from 'moment';
import { Op } from 'sequelize';
import { ModelCtor } from 'sequelize-typescript';
import { BaseService } from 'src/base.service';
import { APPOINTMENT_ATTRIBUTES } from 'src/core/attributes/appointment';
import {
  BANNER_REPOSITORY,
  MASTER_CATEGORY_REPOSITORY,
  MASTER_SERVICE_CATEGORY_REPOSITORY,
} from 'src/core/constants';
import Appointment, {
  APPOINTMENT_STATUS,
} from 'src/core/database/models/Appointment';
import Banner from 'src/core/database/models/Banner';
import Location from 'src/core/database/models/Location';
import MasterCategory from 'src/core/database/models/MasterCategory';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import PaymentConfig from 'src/core/database/models/PaymentConfig';
import Service from 'src/core/database/models/Services';
import User from 'src/core/database/models/User';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import Helpers from 'src/core/utils/helpers';

@Injectable()
export class SharedService extends BaseService<MasterCategory> {
  protected model: ModelCtor<MasterCategory>;
  public date_filter: any;
  constructor(
    @Inject(MASTER_CATEGORY_REPOSITORY)
    masterCategoryRepo: typeof MasterCategory,
    @Inject(BANNER_REPOSITORY)
    private bannerRepo: typeof Banner,
    @Inject(MASTER_SERVICE_CATEGORY_REPOSITORY)
    private masterServiceCategoryRepo: typeof MasterServiceCategory,
  ) {
    super();
    this.model = masterCategoryRepo;
  }
  init() {
    this.date_filter = {
      where: {
        [Op.and]: [
          {
            start_date: {
              [Op.lte]:moment( new Date()).format("YYYY/MM/DD"),
            },
          },
          {
            end_date: {
              [Op.gte]:moment( new Date()).format("YYYY/MM/DD"),
            },
          },
        ],
      },
    };
  }
  async getAllMasterCats(): Promise<any> {
    return await super.findAll();
  }

  async getAllBanners(): Promise<any> {
    this.init();
    return await Helpers.findAll(this.bannerRepo, this.date_filter);
  }

  async getAppointments(user_id: string) {
    //Get appointment details
    let appointment_query:SequelizeFilter<Appointment> = {
      attributes: [
        APPOINTMENT_ATTRIBUTES.APPOINTMENT_START_DATE_TIME,
        APPOINTMENT_ATTRIBUTES.IS_ACCEPTED_BY_BRANCH, 
        APPOINTMENT_ATTRIBUTES.APPOINTMENT_STATUS,
        APPOINTMENT_ATTRIBUTES.IS_CANCELLED,
        APPOINTMENT_ATTRIBUTES.ORDER_ID,
      ],
      where: {
        user_id: user_id,
        appointment_status: [
          APPOINTMENT_STATUS.CONFIRMED,
        ],
        is_cancelled: false,
        is_active: true,
      },
      include: [
        {
          attributes: ['service_name', 'duration'],
          model: Service,
          paranoid:false,
          include: {
            attributes: ['area'],
            model: Location,
            paranoid:false,
          },
        },
      ],
      limit: 1,
      order: [
        [APPOINTMENT_ATTRIBUTES.APPOINTMENT_START_DATE_TIME, 'ASC'],
    ]
    };
    let appointmentDetails = await Helpers.findAll(
      Appointment,
      appointment_query,
    );
    return appointmentDetails;
  }

  async findOneUserById(id: any): Promise<any> {
    let user = await Helpers.findOne(User, { where: { id: id } });
    return user;
  }

  async fetchPaymentMethod() {
    let payment_config: PaymentConfig = await Helpers.findOne(
      PaymentConfig,
      {},
    );
    return payment_config;
  }
}
