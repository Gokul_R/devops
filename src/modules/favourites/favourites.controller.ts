import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FavouritesService } from './favourites.service';
import { CreateFavouriteDto } from './dto/create-favourite.dto';
import { UpdateFavouriteDto } from './dto/update-favourite.dto';
import { ApiTags } from '@nestjs/swagger';
import { EC200, EM100, EM106, EM127 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
@ApiTags('Favouries')
@Controller('favourites')
export class FavouritesController {
  constructor(private readonly favouritesService: FavouritesService) {}

  @Post()
  create(@Body() createFavouriteDto: CreateFavouriteDto) {
    return this.favouritesService.create(createFavouriteDto);
  }

  @Get()
  findAll() {
    return this.favouritesService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.favouritesService.findOne(+id);
  // }

  @Get(':user_id/:vendor_type')
  async userFavourites(
    @Param('user_id') user_id: string,
    @Param('vendor_type') vendor_type: number,
  ) {
    try {
      let data = await this.favouritesService.userFavouritesList(
        user_id,
        vendor_type,
      );
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateFavouriteDto: UpdateFavouriteDto,
  ) {
    return this.favouritesService.update(id, updateFavouriteDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    try {
      let data = this.favouritesService.destroy(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
