import { Inject, Injectable } from '@nestjs/common';
import { CreateFavouriteDto as CreateFavoriteDto } from './dto/create-favourite.dto';
import Helpers from 'src/core/utils/helpers';
import {
  EC200,
  EM104,
  EM116, FAVOURITES_REPOSITORY
} from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { Sequelize, literal } from 'sequelize';
import Branch from 'src/core/database/models/Branch';
import { VendorType } from 'src/core/utils/enum';
import Location from 'src/core/database/models/Location';
import Review from 'src/core/database/models/Reviews';
import Favourites from 'src/core/database/models/Favourites';
import { BaseService } from 'src/base.service';
import { ModelCtor } from 'sequelize-typescript';
import { LOCATION_ATTRIBUTES } from 'src/core/attributes/location';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';

@Injectable()
export class FavouritesService extends BaseService<Favourites> {
  protected model: ModelCtor<Favourites>;
  constructor(
    @Inject(FAVOURITES_REPOSITORY)
    private readonly favoritesRepository: typeof Favourites,
  ) {
    super();
    this.model = favoritesRepository;
  }
  async create(createFavoriteDto: CreateFavoriteDto) {
    let { user_id, branch_id } = createFavoriteDto;
    try {
      let favoriteQry = {
        where: {
          user_id: user_id,
          branch_id: branch_id,
        },
        defaults: createFavoriteDto,
      };
      let favorite = await Helpers.findOrCreate(
        this.favoritesRepository,
        favoriteQry,
      );
      if (!favorite[1]) {
        let update_body = favorite[0].is_active
          ? { is_active: false }
          : { is_active: true };
        let remove_coupon = await Helpers.update(
          Favourites,
          {
            where: favoriteQry.where,
          },
          update_body,
        );
        return HandleResponse.buildSuccessObj(EC200, EM116, remove_coupon[0]);
      }
      // let result = await Helpers.collectionInsert(
      //   Favourites,
      //   createFavouriteDto,
      // );
      return HandleResponse.buildSuccessObj(EC200, EM104, favorite[0]);
    } catch (error) {
      return HandleResponse.buildErrObj(null, error.message, error);
    }
  }

  async userFavouritesList(user_id: string, vendor_type: number) {
    let vendors = {
      1: VendorType.BUSINESS,
      2: VendorType.FREELANCER,
    };

    let query = {
      where: { user_id: user_id, is_deleted: false,   is_active: true },
      include: [
        {
          model: Branch,
          attributes: [
            BRANCH_ATTRIBUTES.BRANCH_NAME,
            BRANCH_ATTRIBUTES.BRANCH_EMAIL,
            BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO,
            BRANCH_ATTRIBUTES.BRANCH_LANDLINE,
            BRANCH_ATTRIBUTES.VENDOR_TYPE,
            BRANCH_ATTRIBUTES.BRANCH_IMAGE,
            BRANCH_ATTRIBUTES.SECONDARY_MOBILE,
            BRANCH_ATTRIBUTES.STD_CODE,
            [literal('ROUND(COALESCE(AVG("branch->reviews"."rating"), 0)::numeric, 1)'), 'average_rating'],
          ],
          where: {
            vendor_type: vendors[vendor_type],
          },
          required: true,
          include: [
            {
              model: Location,
              attributes: [
                LOCATION_ATTRIBUTES.area,
                LOCATION_ATTRIBUTES.address,
                LOCATION_ATTRIBUTES.city,
                LOCATION_ATTRIBUTES.state,
              ],
            },
            {
              model: Review,
              attributes: [],
              required:false,
              where: { is_deleted: false },
            },
          ],        
        },
      ],
      group: [
        'Favourites.id',
        'Favourites.is_active',
        'Favourites.is_deleted',
        'Favourites.user_id',
        'Favourites.branch_id',
        'Favourites.created_by',
        'Favourites.updated_by',
        'Favourites.created_at',
        'Favourites.updated_at',
        'Favourites.deleted_at',
        'branch.id',
        'branch.branch_name',
        'branch.branch_email',
        'branch.branch_contact_no',
        'branch.branch_landline',
        'branch.vendor_type',
        'branch.branch_image',
        'branch.secondary_mobile',
        'branch.std_code',
        'branch->location.id',
        'branch->location.area',
        'branch->location.address',
        'branch->location.city',
        'branch->location.state',
      ],
    };
    return await super.findAll(query);
  }
}
