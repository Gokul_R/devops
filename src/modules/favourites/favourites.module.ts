import { Module } from '@nestjs/common';
import { FavouritesService } from './favourites.service';
import { FavouritesController } from './favourites.controller';
import { favouriteProviders } from './favourites.provider';

@Module({
  controllers: [FavouritesController],
  providers: [FavouritesService,...favouriteProviders]
})
export class FavouritesModule {}
