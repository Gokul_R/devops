import { Sequelize } from "sequelize-typescript";
import { FAVOURITES_REPOSITORY, SEQUELIZE } from "src/core/constants";
import Favourites from "src/core/database/models/Favourites";

export const favouriteProviders = [
    {
        provide: FAVOURITES_REPOSITORY,
        useValue: Favourites,
    },
    {
        provide: SEQUELIZE,
        useValue: Sequelize,
    },
];
