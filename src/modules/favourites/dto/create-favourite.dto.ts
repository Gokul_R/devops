
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateFavouriteDto {
  @ApiProperty({example:"94ff15f8-4bc3-4697-9a26-5349d6f27706"})
  @IsNotEmpty()
  @IsString()
  user_id: string;

  @ApiProperty({example:"62a8332aecb4a12ed2bd2f44"})
  @IsNotEmpty()
  @IsString()
  branch_id: string;
  
  // @IsNotEmpty()
  // @IsString()
  // type: string;
}

