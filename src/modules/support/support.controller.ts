import { Body, Controller, Post, Get } from '@nestjs/common';
import { SupportService } from './support.service';
import { ApiTags } from '@nestjs/swagger';
import { CreateSupportDto } from './dto/create-support.dto';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EC500, EM100, EM104, EM106 } from 'src/core/constants';

@Controller('/support')
@ApiTags('Support')
export class SupportController {
  constructor(private readonly supportService: SupportService) { }

  @Post()
  async createSupport(@Body() createSupport: CreateSupportDto) {
    try {
      let data = await this.supportService.createSupport(createSupport);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Get()
  async getSupport() {
    try {
      let data = await this.supportService.getSupportList();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }
}
