import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsNotEmpty, IsOptional, IsString, ValidateIf } from 'class-validator';
import { complaintType } from 'src/core/constants';

export class CreateSupportDto {
  @ApiProperty({ example: '9315b480-f207-41be-bec6-a875f1759901' })
  @IsString()
  @IsNotEmpty()
  user_id!: string;

  @ApiProperty({ example: `${complaintType.SERVICE} (OR) ${complaintType.GENERAL}` })
  @IsString()
  @IsNotEmpty()
  complaint_type!: string;

  @ApiProperty({ example: 'ZVVL_1691481363410' })
  @ValidateIf((o) => o.complaint_type === complaintType.SERVICE)
  @IsString()
  // @IsOptional()
  @IsNotEmpty()
  order_id!: string;

  @ApiProperty({ example: 'Yoge' })
  @IsString()
  @IsNotEmpty()
  user_name!: string;

  @ApiProperty({ example: '+919111111111' })
  @IsString()
  @IsNotEmpty()
  user_ph!: string;

  @ApiProperty({ example: 'mano@mailinator.com' })
  @IsString()
  @IsNotEmpty()
  user_email!: string;

  @ApiProperty({ example: 'hello' })
  @IsString()
  @IsNotEmpty()
  message!: string;
}
