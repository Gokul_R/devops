import { Injectable } from '@nestjs/common';
import SupportCenter from 'src/core/database/models/SupportCenter';
import Helpers from 'src/core/utils/helpers';

@Injectable()
export class SupportService {
  async createSupport(req_data: any) {
    const insertSupport = await Helpers.create(SupportCenter, req_data);
    return insertSupport;
  }

  async getSupportList() {
    const getSupport = await Helpers.findAll(SupportCenter, {});
    return getSupport;
  }
}
