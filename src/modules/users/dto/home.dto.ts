import { ApiExtraModels, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsUUID } from 'class-validator';
import { IsString, IsNumber, IsArray, IsBoolean } from 'class-validator';

export class HomeDto {
  @ApiProperty({ example: '62a5af6ec5ca5e39d17cfe04' })
  // @IsUUID()
  @IsOptional()
  user_id: string;

  // @ApiProperty({ example: 1 })
  // @IsNotEmpty()
  // page: number;

  // @ApiProperty({ example: '' })
  // location_search: string;

  // @ApiProperty({ example: 'KFC' })
  // shopNameSearch: string;

  // @ApiProperty({
  //   example:
  //     'Chennai Bypass Road, Om Shakti Nagar, Menambedu, Korattur, Chennai, Tamil Nadu, India',
  // })
  // @IsOptional()
  // homelocation: string;

  // @ApiProperty({ example: '' })
  // serviceNameSearch: string;

  // @ApiProperty({ example: '' })
  // services_filter: string;

  // @ApiProperty({ example: '' })
  // servicetypes_filter: string;

  // @ApiProperty({ example: '' })
  // gender_filter: string;

  // @ApiProperty({ example: '' })
  // service_shop_search: string;
  @ApiProperty({ example: 13.045218 })
  @IsOptional()
  @IsNumber()
  lat: number;
  @ApiProperty({ example: 80.135437 })
  @IsOptional()
  @IsNumber()
  lng: number;
}

export class ExploreDto {
  @ApiProperty({ example: '94ff15f8-4bc3-4697-9a26-5349d6f27706' })
  @IsString()
  user_id: string;

  @ApiProperty({ example: '1' })
  @IsNumber()
  page: number;

  // @ApiProperty({example:'Thiruverkadu'})
  // @IsString()
  // location_search: string;

  @ApiProperty({ example: '' })
  @IsString()
  @IsOptional()
  search_text?: string;

  @ApiProperty({ example: ['625911a9bdfbbd99f6eed957'] })
  @IsArray()
  @IsOptional()
  service_category_filter?: string[];

  @ApiProperty({ example: ['6256734b8a46f576c864166d'] })
  @IsArray()
  @IsOptional()
  service_type_filter?: string[];

  @ApiProperty({ example: ['625902fc918130fdfc052d3f'] })
  @IsArray()
  @IsOptional()
  category_filter?: string[];

  @ApiProperty({ example: [] })
  @IsArray()
  @IsOptional()
  area_filter?: string[];

  @ApiProperty({ example: [] })
  @IsArray()
  @IsOptional()
  ratings_filter?: string[];

  @ApiProperty({ example: 'business' })
  @IsString()
  vendor_type: string;

  @ApiProperty({ example: 0, description: '0 for all,1 for business and freelancers, 2 for services' })
  @IsNumber()
  @IsOptional()
  type: number;

  @ApiProperty({ example: '13.0452745' })
  @IsOptional()
  @IsNumber()
  lat: number;

  @ApiProperty({ example: '80.1351977' })
  @IsOptional()
  @IsNumber()
  lng: number;
}
