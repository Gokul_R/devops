import { Body, Controller, Get, Param, Post, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { UsersService } from './users.service';
import { GetBranchAndServiceSearch, UpdateUserDto } from './dto/user.dto';
import { DoesUserExist } from 'src/core/guards/doesUserExist.guard';
import { ApiTags } from '@nestjs/swagger';
import { ExploreDto, HomeDto } from './dto/home.dto';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM106, EM116, EM127 } from 'src/core/constants';

@ApiTags('User')
@Controller('user')
export class UsersController {
  constructor(private readonly userService: UsersService) { }

  @Post('home')
  async home(@Body() body: HomeDto) {
    try {
      let data = await this.userService.home(body);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('explore')
  async explore(@Body() body: ExploreDto) {
    try {
      let data = await this.userService.explore(body);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Get(':user_id')
  async profile(@Param('user_id') id: string) {
    try {
      let data = await this.userService.findOneById(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  // @UseGuards(DoesUserExist)
  @Post('updateProfile')
  async updateUser(@Body() user: UpdateUserDto) {
    try {
      let data = await this.userService.updateProfile(user);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(error?.status || null, error.message, error);
    }
  }

  @Get('locations/:city')
  async getLocatons(@Param('city') city: string) {
    try {
      let locations = await this.userService.getLocatons(city);
      // let data = locations.map((location: any) => location.area);
      return await HandleResponse.buildSuccessObj(EC200, EM106, locations);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error.message);
    }
  }


  @Post('organic-search')
  async getBranchAndServiceSearch(@Body() req_data: GetBranchAndServiceSearch) {
    try {
      const data = await this.userService.getBranchAndServiceSearch(req_data);
      return await HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, error?.message || EM100, error?.message || error);
    }
  }
}
