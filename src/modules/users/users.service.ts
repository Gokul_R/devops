import { Injectable, Inject, CACHE_MANAGER, ConflictException, forwardRef } from '@nestjs/common';

import {
  LOCATION_REPOSITORY,
  MASTER_SERVICE_CATEGORY_REPOSITORY,
  OTP_REPOSTIORY,
  SEQUELIZE,
  USER_REPOSITORY,
} from '../../core/constants';
import Otp from 'src/core/database/models/Otp';
import User from 'src/core/database/models/User';
import { Sequelize, ModelCtor } from 'sequelize-typescript';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import Helpers from 'src/core/utils/helpers';
import Location from 'src/core/database/models/Location';
import { ExploreDto, HomeDto } from './dto/home.dto';
import { BaseService } from 'src/base.service';
import { SharedService } from '../shared/shared.service';
import { FlashSaleService } from '../flash-sale/flash-sale.service';
import { CouponService } from '../coupon/coupon.service';
import { VendorType } from 'src/core/utils/enum';
import { Cache } from 'cache-manager';
import sequelize, { Op, QueryTypes } from 'sequelize';
import { VerifyOtpDto } from '../auth/dto/verify-otp.dto';
import { GetBranchAndServiceSearch, UpdateUserDto } from './dto/user.dto';
import { Errors } from 'src/core/constants/error_enums';
import { logger } from 'src/core/utils/logger';
import Branch from 'src/core/database/models/Branch';
import { SERVICE_ATTRIBUTES } from 'src/core/attributes/services';
import { CartService } from '../cart/cart.service';
import { BranchService } from '../shop/branch.service';
import { ServicesService } from '../services/services.service';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { NotificationService } from '../notification/notification.service';

@Injectable()
export class UsersService extends BaseService<User> {
  protected model: ModelCtor<User>;
  area_filter: Array<string>;
  constructor(
    @Inject(USER_REPOSITORY) private readonly userRepository: typeof User,
    @Inject(OTP_REPOSTIORY) private readonly otp_repo: typeof Otp,
    private readonly branch_service: BranchService,
    @Inject(SEQUELIZE) private readonly sequelize: Sequelize,
    private readonly servicesService: ServicesService,
    private readonly sharedService: SharedService,
    private readonly flashSaleService: FlashSaleService,
    private readonly couponService: CouponService,
    private readonly cartService: CartService,
    @Inject(forwardRef(() => NotificationService)) //for circular dependency
    private readonly notificationService: NotificationService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {
    super();
    this.model = this.userRepository;
  }
  init() {}

  //Insert new user record
  async create(user: any): Promise<User> {
    return await super.create(user);
  }

  //TODO need to pass id directly instead of passing object
  async findOneById(id: any): Promise<any> {
    let user = await super.findOne(id);
    return user;
  }
  //Insert otp
  async createOtp(user: any): Promise<Otp> {
    return await Otp.create<Otp>(user);
  }
  //Remove otp
  async deleteOtp(userId: string) {
    return Otp.destroy<Otp>({
      where: {
        userId: userId,
      },
    });
  }
  //Find user using phone
  async findOneByPhone(mobile: string): Promise<User> {
    return await super.findOne(null, {
      where: { mobile_no: mobile },
    });
  }
  //Find user using email
  async findOneByEmail(email_id: string): Promise<User> {
    return await super.findOne(null, {
      where: { email_id: email_id },
    });
  }
  //Get otp using id
  async findOtpById(request: any): Promise<Otp> {
    return await Helpers.findOne(this.otp_repo, {
      where: { userId: request.user_id, otp: request.otp },
    });
  }
  //Home api
  async home(body: HomeDto) {
    logger.info(`User_Home_Entry: ` + JSON.stringify(body));
    const { user_id } = body;
    //let data = await this.cacheManager.get('home_data');
    //if (data) return data;
    const [banners, service_categories, appointments, coupons, flash_sales, branches, freelancers, cartCount,unseenCount] =
      await Promise.all([
        this.sharedService.getAllBanners(),
        this.servicesService.getServiceCategories(),
        user_id ? this.sharedService.getAppointments(user_id) : null,
        user_id ? this.couponService.findCouponsForUser(user_id) : null,
        this.flashSaleService.findFlashSales(body),
        this.branch_service.findBranches(body, VendorType.BUSINESS),
        this.branch_service.findBranches(body, VendorType.FREELANCER),
        user_id ? this.cartService.getCartCount(user_id) : 0,
        user_id ? this.notificationService.unseenNotificationsCount(user_id) : 0,
      ]);
    let home_data = {
      banners: banners,
      service_categories: service_categories,
      appointments: appointments,
      flash_sales: flash_sales,
      coupons: coupons,
      branches: branches,
      freelancers: freelancers,
      cartCount: cartCount,
      unseenNotificationsCount:unseenCount
    };
    // await this.cacheManager.set('home_data', home_data, 200);
    logger.info(`User_Home_Exit: 200`);
    return home_data;
  }
  //Explore api
  async explore(body: ExploreDto) {
    logger.info(`User_Explore_Entry: ` + JSON.stringify(body));
    let { type } = body;
    type = type ?? 0;
    let freelancers: Branch[];
    let businessPromise: Promise<any> | null = null;
    let servicesPromise: Promise<any> | null = null;
    if (type == 3 || type == 0) freelancers = await this.branch_service.findBranches(body, VendorType.FREELANCER);
    if (type == 1 || type == 0) businessPromise = this.branch_service.findBranches(body, VendorType.BUSINESS);
    if (type == 2 || type == 0) servicesPromise = this.servicesService.getServices(body);
    const [businessResult, servicesResult] = await Promise.all([businessPromise, servicesPromise]);
    let explore_data = {
      branches: businessResult || [],
      freelancers: freelancers || [],
      services: servicesResult || [],
      // branchesAndFreelancersCount: count + freelancersCount,
    };
    logger.info(`User_Explore_Exit: 200`);
    return explore_data;
  }
  //Update already registered user
  public async updateProfile(body: UpdateUserDto | VerifyOtpDto) {
    logger.info(`User_UpdateProfile_Entry: ` + JSON.stringify(body));
    let user_id: string = 'id' in body ? body.id : body.user_id;
    let user: User;
    if ('email_id' in body) {
      user = await Helpers.findOne(this.userRepository, {
        where: { email_id: body.email_id, id: { [Op.ne]: user_id } },
      });
      if (user) throw new ConflictException(Errors.EMAIL_ID_ALREADY_EXISTS);
    }
    // if ('mobile_no' in body) {
    //   user = await this.checkMobileExists(body.mobile_no, user_id);
    // }
    // create the user
    let newUser: any = await Helpers.update(this.userRepository, { where: { id: user_id } }, { ...body });
    // console.log('----<>', newUser);
    // if (newUser.length == 0) newUser =await User.findByPk(user_id);
    logger.info(`User_UpdateProfile_Exit: ` + JSON.stringify(newUser));
    return newUser;
  }

  //Get All locations
  async getLocatons(city: string) {
    // await Location.updateArea();
    logger.info(`User_GetLocation_Entry: ` + JSON.stringify(city));
    const cleanedAreas = await Location.findAll({
      attributes: [
        // LOCATION_ATTRIBUTES.area
        // [sequelize.fn('DISTINCT', sequelize.fn('UPPER', sequelize.fn('REGEXP_REPLACE', sequelize.fn('TRIM', sequelize.col('area')), '[^a-zA-Z0-9 ]', '', 'g'))), 'area'],
        [sequelize.fn('DISTINCT', sequelize.fn('UPPER', sequelize.col('area'))), 'area']],
      // where: {
      //   city: {
      //     [sequelize.Op.iLike]: `%${city}%`,
      //   },
      // },
      order: [
        ['area', 'ASC'],
        // [sequelize.fn('UPPER', sequelize.fn('REGEXP_REPLACE', sequelize.fn('TRIM', sequelize.col('area')), '[^a-zA-Z0-9 ]', '', 'g')), 'ASC'],
      ],
    });
    // Access the cleaned, case-insensitive, and non-empty areas
    let data = cleanedAreas.map((result: any) => result?.area);
    logger.info(`User_GetLocation_Exit_200`);
    return data;
  }

  //Helper function for locationfilter
  getLocationFilters(search_text: string): any {
    if (this.area_filter?.length > 0) {
      return {
        where: {
          // [Op.or]: [
          //   { city: { [Op.like]: `%${search_text}%` } },
          //   { area: { [Op.like]: `%${search_text}%` } },
          //   { zipcode: { [Op.like]: `%${search_text}%` } },

          // ],
          area: this.area_filter,
        },
      };
    } else return {};
  }

  async checkMobileExists(mobile_no: string, user_id: string) {
    let user = await Helpers.findOne(this.userRepository, {
      where: { mobile_no: mobile_no, id: { [Op.ne]: user_id } },
    });
    if (user) throw new ConflictException(Errors.MOBILE_NO_ALREADY_EXISTS);
    return user;
  }

  async getBranchAndServiceSearch(req_data: GetBranchAndServiceSearch) {
    const data = await Branch.sequelize.query(
      `
    SELECT ${BRANCH_ATTRIBUTES.ID}, ${BRANCH_ATTRIBUTES.BRANCH_NAME} as name, ${BRANCH_ATTRIBUTES.VENDOR_TYPE} AS type, ${BRANCH_ATTRIBUTES.BRANCH_IMAGE} as image
    FROM ${BRANCH_ATTRIBUTES.TABLE_NAME}
    WHERE ${BRANCH_ATTRIBUTES.APPROVAL_ATATUS} = 1 
    AND ${BRANCH_ATTRIBUTES.IS_ACTIVE} = true 
    AND ${BRANCH_ATTRIBUTES.IS_DELETED} = false
    AND ${BRANCH_ATTRIBUTES.DELETED_AT} is null
    AND branch_name ILIKE '%${req_data.searchText}%'
    UNION
    SELECT ${SERVICE_ATTRIBUTES.ID}, ${SERVICE_ATTRIBUTES.SERVICE_NAME} as name, 'services' AS type, ${SERVICE_ATTRIBUTES.SERVICE_IMAGE}
    FROM ${SERVICE_ATTRIBUTES.TABLE_NAME} 
    WHERE ${BRANCH_ATTRIBUTES.IS_ACTIVE} = true
    AND ${BRANCH_ATTRIBUTES.IS_DELETED} = false 
    AND ${BRANCH_ATTRIBUTES.DELETED_AT} is null
    AND ${SERVICE_ATTRIBUTES.SERVICE_NAME} ILIKE '%${req_data.searchText}%'
    LIMIT 10;
  `,
      {
        type: QueryTypes.SELECT,
      },
    );
    return data;
  }
}
