import { CacheModule, Module, forwardRef } from '@nestjs/common';

import { UsersService } from './users.service';
import { usersProviders } from './users.providers';
import { ServicesModule } from '../services/services.module';
import { BranchModule } from '../shop/branch.module';
import { SharedModule } from '../shared/shared.module';
import { FlashSaleModule } from '../flash-sale/flash-sale.module';
import { CouponModule } from '../coupon/coupon.module';
import { CartModule } from '../cart/cart.module';
import { NotificationModule } from '../notification/notification.module';
import { NotificationService } from '../notification/notification.service';

@Module({
  imports: [
    ServicesModule,
    BranchModule,
    SharedModule,
    FlashSaleModule,
    CouponModule,
    CartModule,
    CacheModule.register(),
    forwardRef(()=>NotificationModule)
  ],
  providers: [UsersService, ...usersProviders],
  exports: [UsersService],
  // controllers: [ UsersController],
})
export class UsersModule {}
