import {
  SEQUELIZE
} from '../../core/constants';
import { Sequelize } from 'sequelize-typescript';
import { commonProvider } from 'src/core/interfaces/common-providers';

export const usersProviders = [

  {
    provide: SEQUELIZE,
    useValue: Sequelize,
  },
  commonProvider.user_repo,
  commonProvider.master_service_category_repository,
  commonProvider.location_repo,
  commonProvider.service_repository,
  commonProvider.otp_repo,
  commonProvider.slot_details_repository,
  commonProvider.branch_repository,
  commonProvider.coupon_repository,
  commonProvider.cart_service_repo,
  commonProvider.notification_service_repo
];
