import { Sequelize } from 'sequelize-typescript';
import {
  LOCATION_REPOSITORY,
  SEQUELIZE,
  BRANCH_REPOSITORY,
} from 'src/core/constants';
import Location from 'src/core/database/models/Location';
import Branch from 'src/core/database/models/Branch';
import { commonProvider } from 'src/core/interfaces/common-providers';

export const servicesProviders = [
  // {
  //     provide: USER_REPOSITORY,
  //     useValue: User,
  // },
  {
    provide: SEQUELIZE,
    useValue: Sequelize,
  },
  // {
  //     provide: MASTER_SERVICE_CATEGORY_REPOSITORY,
  //     useValue: MasterServiceCategory,
  // },
  commonProvider.branch_repository,
  commonProvider.location_repo,
  commonProvider.master_category_repository,
  commonProvider.master_service_category_repository,
  commonProvider.master_service_category_repository,
  commonProvider.branch_catagory_repository,
  commonProvider.branch_service_category_repository,
  commonProvider.master_service_type_repository,
];
