import { Module } from '@nestjs/common';
import { ServicesService } from './services.service';
import { ServicesController } from './services.controller';
import { servicesProviders } from './services.provider';
import { SharedModule } from '../shared/shared.module';

@Module({
  controllers: [ServicesController],
  providers: [ServicesService,...servicesProviders],
  exports:[ServicesService]
})
export class ServicesModule {}
