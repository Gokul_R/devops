import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { BRANCH_ID } from 'src/core/constants';
import { PaginationDto, SharedDto } from 'src/core/interfaces/shared.dto';

export class CreateServiceDto {}
export class GetServicesByServiceCategoryDto extends PickType(SharedDto, [
  'service_category_id',
  'lat',
  'lng',
]) {
  @ApiProperty({ example: 'Some text' })
  @IsString()
  @IsOptional()
  searchText?: string;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  pagNo?: number;

  @ApiProperty({ example: 10 })
  @IsOptional()
  @IsNumber()
  limit?: number;
}
