import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ServicesService } from './services.service';
import { EC200, EM106, EM100 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { GetServicesByServiceCategoryDto } from './dto/create-service.dto';
@ApiTags('Services')
@Controller('services')
export class ServicesController {
  constructor(private readonly servicesService: ServicesService) {}

  @Post('by-service-category')
  async getCategoryServices(@Body() getServicesByServiceCategoryDto: GetServicesByServiceCategoryDto) {
    try {
      let data = await this.servicesService.getServicesByServiceCategory(getServicesByServiceCategoryDto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Get('categories-by-branch/:branch_id')
  // @ApiOperation({ summary: 'Get category services' })
  @ApiParam({ name: 'branch_id', example: '', description: 'Branch Id' })
  async getCategoriesByBranch(@Param('branch_id') id: string) {
    try {
      let data = await this.servicesService.getCategoriesByBranch(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Get('service-categories-by-branch/:branch_id')
  @ApiParam({ name: 'branch_id', example: '', description: 'Branch Id' })
  async getServiceCatByBranch(@Param('branch_id') id: string) {
    try {
      let data = await this.servicesService.getServiceCatByBranch(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get('service-categories')
  async getServiceCategories() {
    try {
      let data = await this.servicesService.getServiceCategories();
      return await HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error.message);
    }
  }
  @Get('categories')
  async getCategories() {
    try {
      let data = await this.servicesService.getCategories();
      return await HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error.message);
    }
  }
  @Get('services-by-coupon/:coupon_id')
  async servicesByCoupon(@Param('coupon_id') coupon_id:string) {
    try {
      let data = await this.servicesService.servicesByCoupon(coupon_id);
      return await HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error.message);
    }
  }
  @Get('service-types')
  async serviceTypes() {
    try {
      let data = await this.servicesService.getServiceTypes();
      return await HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error.message);
    }
  }
}
