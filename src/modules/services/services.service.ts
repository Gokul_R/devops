import { Inject, Injectable } from '@nestjs/common';
import FlashSale from 'src/core/database/models/FlashSale';
import Branch from 'src/core/database/models/Branch';
import Helpers from 'src/core/utils/helpers';
import { FlashSaleConstants } from '../flash-sale/flashsale-constants';
import Service from 'src/core/database/models/Services';
import { BaseService } from 'src/base.service';
import { ModelCtor, Sequelize } from 'sequelize-typescript';
import { Op } from 'sequelize';
import {
  LOCATION_REPOSITORY,
  BRANCH_REPOSITORY,
  MASTER_CATEGORY_REPOSITORY,
  MASTER_SERVICE_CATEGORY_REPOSITORY,
  BRANCH_CATEGORY_REPOSITORY,
  BRANCH_SERVICE_CATEGORY_REPOSITORY,
  SEQUELIZE,
  RADIUS,
  MASTER_SERVICE_TYPE_REPO,
  Approval,
} from 'src/core/constants';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import Location from 'src/core/database/models/Location';
import { LOCATION_ATTRIBUTES } from 'src/core/attributes/location';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { ExploreDto } from '../users/dto/home.dto';
import { GetServicesDto } from '../shop/dto/get-services-dto';
import MasterCategory from 'src/core/database/models/MasterCategory';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import { MASTER_CATEGORIES_ATTRIBUTES } from 'src/core/attributes/master_category';
import { MASTER_SERVICE_CATEGORY_ATTRIBUTES } from 'src/core/attributes/master_service_category';
import BranchCategoryMapping from 'src/core/database/models/BranchCategoryMapping';
import BranchServiceCategoryMapping from 'src/core/database/models/BranchServiceCategoryMapping.ts';
import CouponServiceMapping from 'src/core/database/models/CouponServiceMapping';
import { GetServicesByServiceCategoryDto } from './dto/create-service.dto';
import MasterServiceType from 'src/core/database/models/MasterServiceType';
import { logger } from 'src/core/utils/logger';

@Injectable()
export class ServicesService extends BaseService<Service> {
  protected model: ModelCtor<Service> = Service;
  services_qry: SequelizeFilter<Service>;
  search_text: string;
  area_filter: string[];
  distance_formale: any;
  distance_qry: string;
  limit = 10;
  page: number;
  constructor(
    @Inject(SEQUELIZE) private readonly sequelize: typeof Sequelize,
    @Inject(BRANCH_REPOSITORY)
    private readonly branchRepository: typeof Branch,
    @Inject(LOCATION_REPOSITORY)
    private readonly locationRepository: typeof Location,
    @Inject(MASTER_CATEGORY_REPOSITORY)
    private readonly masterCategoryRepo: typeof MasterCategory,
    @Inject(MASTER_SERVICE_CATEGORY_REPOSITORY)
    private readonly masterServiceCategoryRepo: typeof MasterServiceCategory,
    @Inject(BRANCH_CATEGORY_REPOSITORY)
    private readonly branchCategoryRepo: typeof BranchCategoryMapping,
    @Inject(BRANCH_SERVICE_CATEGORY_REPOSITORY)
    private readonly branchServiceCategoryRepo: typeof BranchServiceCategoryMapping,
    @Inject(MASTER_SERVICE_TYPE_REPO)
    private readonly masterServiceTypeRepo: typeof MasterServiceType,
  ) {
    super();
  }
  init() {
    this.services_qry = {
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'is_deleted', 'is_active'],
      },
      where: { is_active: true },
      include: [
        {
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          model: FlashSale,
          where: FlashSaleConstants.getFlashSaleQry(),
          required: false,
        },
        {
          attributes: [MASTER_CATEGORIES_ATTRIBUTES.category, MASTER_CATEGORIES_ATTRIBUTES.image],
          model: this.masterCategoryRepo,
        },
        {
          attributes: [MASTER_SERVICE_CATEGORY_ATTRIBUTES.service_category_name],
          model: this.masterServiceCategoryRepo,
        },
        {
          attributes: [BRANCH_ATTRIBUTES.BRANCH_NAME],
          model: this.branchRepository,
          where: { approval_status: Approval.ACCEPTED, is_deleted: false, is_active: true, account_verified: true },
          include: {
            attributes: [
              LOCATION_ATTRIBUTES.address,
              LOCATION_ATTRIBUTES.area,
              LOCATION_ATTRIBUTES.city,
              LOCATION_ATTRIBUTES.state,
              LOCATION_ATTRIBUTES.latitude,
              LOCATION_ATTRIBUTES.longitude,
            ],
            model: this.locationRepository,
            where:
              this.area_filter?.length > 0
                ? {
                    area: this.area_filter,
                  }
                : { [Op.and]: [this.distance_formale] },
          },
          required: true,
        },
      ],
      // order:[[["Service.branch.distance", 'ASC']]],
      // order: [[Sequelize.literal('"branch"."distance"'), 'ASC']],
      offset: (this.page - 1) * this.limit,
      limit: this.limit,
    };
  }

  async serviceCount(service_count_qry: SequelizeFilter<Service>) {
    let count = await Service.count({
      attributes: [],
      where: {
        deleted_at: null,
        is_active: true,
        ...service_count_qry,
      },
    });
    return count;
  }
  async getServicesByServiceCategory(req_data: GetServicesByServiceCategoryDto) {
    let search: {};
    this.addDistanceQry(req_data);
    let { service_category_id, limit, pagNo, searchText } = req_data;
    this.page = pagNo;
    this.limit = limit ? limit : this.limit;
    this.init();

    if (searchText) {
      search = {
        [Op.or]: [
          { service_name: { [Op.iLike]: `%${searchText}%` } },
          { '$branch->location.area$': { [Op.iLike]: `%${searchText}%` } },
          { '$branch->location.state$': { [Op.iLike]: `%${searchText}%` } },
          { '$branch->location.city$': { [Op.iLike]: `%${searchText}%` } },
          { '$branch->location.address$': { [Op.iLike]: `%${searchText}%` } },
          { '$branch.branch_name$': { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }

    if (searchText) {
      this.services_qry.where = { ...this.services_qry.where, ...search };
    }
    this.services_qry.where.service_category_id = service_category_id;

    this.services_qry.include[3].attributes.push(
      this.distance_qry && [this.sequelize.literal(this.distance_qry), 'distance'],
    );
    this.services_qry.order = [
      [Sequelize.literal('"branch.distance"'), 'ASC'],
      ['id', 'ASC'],
    ];

    // this.services_qry.limit = limit ? limit : 10;
    // this.services_qry.offset = pagNo ? (pagNo - 1) * this.limit : 0;

    let services = await super.findAndCountAll(this.services_qry);
    return services;
  }
  async getServices(body: ExploreDto) {
    let { service_category_filter, category_filter } = body;

    if ('page' in body) {
      this.page = body.page;
      if ('area_filter' in body && body.area_filter.length > 0) {
        this.area_filter = body.area_filter;
      } else {
        this.addDistanceQry(body);
      }
    }
    if ('page' in body) this.page = body.page;

    this.init();
    this.services_qry.include[3].attributes.push(
      this.distance_qry && [this.sequelize.literal(this.distance_qry), 'distance'],
    );
    this.services_qry.order = [[Sequelize.literal('"branch.distance"'), 'ASC']];
    if ('search_text' in body) {
      let { search_text } = body;

      this.search_text = search_text;

      if (search_text)
        this.services_qry.where.service_name = {
          [Op.iLike]: `%${this.search_text.toLowerCase()}%`,
        };
    }

    if ('branch_id' in body) {
      this.services_qry.where = {
        ...this.services_qry.where,
        branch_id: body.branch_id,
      };
    }
    // this.services_qry.order =[[Sequelize.literal('"branch.distance"'), 'ASC']]

    if (service_category_filter.length > 0) this.services_qry.where.service_category_id = service_category_filter;

    if (category_filter.length > 0) this.services_qry.where.category_id = category_filter;

    let services = await super.findAll(this.services_qry);
    return services;
  }

  async getBranchServices(body: GetServicesDto) {
    logger.info(`Get_Shop_Service_Entry: ` + JSON.stringify(body));
    let { service_category_filter, category_filter, searchText, pagNo, limit } = body;
    this.page = (pagNo && pagNo) || 1;
    this.limit = limit ? limit : this.limit;
    let search = {};
    if (searchText) {
      search = {
        [Op.or]: [{ '$Service.service_name$': { [Op.iLike]: `%${body.searchText}%` } }],
      };
    }
    this.distance_formale = null;
    this.init();
    if (searchText) {
      this.services_qry.where = { ...this.services_qry.where, ...search };
    }
    if ('branch_id' in body) {
      this.services_qry.where = {
        ...this.services_qry.where,
        branch_id: body.branch_id,
      };
    }
    if (service_category_filter.length > 0) this.services_qry.where.service_category_id = service_category_filter;
    if (category_filter.length > 0) this.services_qry.where.category_id = category_filter;

    let services = await super.findAll(this.services_qry);
    let servicesCount = await this.serviceCount({ ...this.services_qry.where });
    logger.info(`Get_Shop_Service_Exit: ` + JSON.stringify(services));
    return { count: servicesCount, rows: services };
  }
  addDistanceQry(body: ExploreDto | GetServicesDto | GetServicesByServiceCategoryDto) {
    this.area_filter = [];
    // this.distance_qry
    if ('lat' in body) {
      let { lat, lng } = body;
      this.distance_qry = `(6371 * acos(cos(radians(${lat})) * cos(radians("branch->location"."latitude")) * cos(radians("branch->location"."longitude") - radians(${lng})) + sin(radians(${lat})) * sin(radians("branch->location"."latitude"))))`;
      this.distance_formale = this.sequelize.literal(`${this.distance_qry} <= ${RADIUS}`);
    }
  }
  async servicesByCoupon(coupon_id: string) {
    let services: CouponServiceMapping = await Helpers.findAll(CouponServiceMapping, {
      attributes: ['id'],
      where: { coupon_id: coupon_id },
      include: [
        {
          model: this.model,
        },
      ],
    });
    return services;
  }
  async getCategoriesByBranch(branch_id: string) {
    let categories_qry: SequelizeFilter<BranchCategoryMapping> = {
      where: { branch_id: branch_id, is_active: true },
      attributes: ['id'],
      include: [
        {
          model: this.masterCategoryRepo,
          require: true,
          where: { is_active: true },
        },
      ],
    };
    let categories: BranchCategoryMapping = await Helpers.findAll(this.branchCategoryRepo, categories_qry);
    return categories;
  }
  async getServiceCatByBranch(branch_id: string) {
    let categories_qry: SequelizeFilter<BranchServiceCategoryMapping> = {
      where: { branch_id: branch_id, is_active: true },
      attributes: ['id'],
      include: [
        {
          model: this.masterServiceCategoryRepo,
          required: true,
          where: { is_active: true },
        },
      ],
    };
    let categories: BranchCategoryMapping = await Helpers.findAll(this.branchServiceCategoryRepo, categories_qry);
    return categories;
  }

  //Get all master categories
  async getServiceCategories() {
    return await Helpers.findAll(this.masterServiceCategoryRepo, {
      where: { is_active: true, is_deleted: false },
      order: [['service_category_name', 'ASC']],
    });
  }
  //Get all master categories
  async getCategories() {
    let categories = await Helpers.findAll(this.masterCategoryRepo, { where: { is_active: true } });
    return categories;
  }

  async getServiceTypes() {
    let service_type = await Helpers.findAll(this.masterServiceTypeRepo, {
      where: { is_deleted: false, is_active: true },
    });
    return service_type;
  }

  includeServices(attributes: string[], query?: any, required?: boolean) {
    return {
      // attributes: attributes ? attributes : {},
      model: this.model,
      where: query || {},
      required: required || false,
    };
  }
}
