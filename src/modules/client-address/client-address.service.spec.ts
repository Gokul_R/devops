import { Test, TestingModule } from '@nestjs/testing';
import { ClientAddressService } from './client-address.service';

describe('ClientAddressService', () => {
  let service: ClientAddressService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClientAddressService],
    }).compile();

    service = module.get<ClientAddressService>(ClientAddressService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
