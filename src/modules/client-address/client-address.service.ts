import { Injectable } from '@nestjs/common';
import CreateClientAddressDto from './dto/create-client-address.dto';
import { UpdateClientAddressDto } from './dto/update-client-address.dto';
import Helpers from 'src/core/utils/helpers';
import UserAddress from 'src/core/database/models/UserAddress';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM104, EM106, EM116, EM127 } from 'src/core/constants';
import { error } from 'console';

@Injectable()
export class ClientAddressService {
  async create(createClientAddressDto: CreateClientAddressDto) {
    let client_address = await Helpers.create(
      UserAddress,
      createClientAddressDto,
    );
    return HandleResponse.buildSuccessObj(EC200, EM104, client_address);
  }

  findAll() {
    return `This action returns all clientAddress`;
  }

  async findOne(id: string) {
    try {
      let client_adrs_qry = {
        where: {
          user_id: id,
        },
      };
      let addresses = await Helpers.findAll(
        UserAddress,
        client_adrs_qry,
      );
      return HandleResponse.buildSuccessObj(EC200, EM106, addresses);
    } catch (error) {
      return HandleResponse.buildErrObj(null, error.message, error);
    }
  }

  async update(id: string, updateClientAddressDto: UpdateClientAddressDto) {
    try {
      let address = await Helpers.update(
        UserAddress,
        { where: { id: id } },
        updateClientAddressDto,
      );
      return HandleResponse.buildSuccessObj(EC200, EM116, address);
    } catch (error) {
      return HandleResponse.buildErrObj(null, error.message, error);
    }
  }

  async remove(id: string) {
    try {
      let address = await Helpers.collectionDelete(
        UserAddress,
        { where: { id: id } },
      );
      return HandleResponse.buildSuccessObj(EC200, EM127, address);
    } catch (error) {
      return HandleResponse.buildErrObj(null, error.message, error);
    }
  }
}
