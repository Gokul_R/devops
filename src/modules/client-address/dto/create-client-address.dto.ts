import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsBoolean, IsUUID, IsOptional, IsNumberString, IsEmail } from 'class-validator';

export default class CreateClientAddressDto {
  @ApiProperty()
  @IsString()
  flat_no: string;

  @ApiProperty()
  @IsString()
  area: string;
  
  @ApiProperty()
  @IsString()
  address_type: string;

  @ApiProperty()
  @IsString()
  customer_name: string;

  @ApiProperty()
  @IsString()
  customer_mobile: string;

  @ApiProperty()
  @IsString()
  message: string;

  @ApiProperty()
  @IsString()
  @IsEmail()
  customer_email: string;

  @ApiProperty()
  @IsString()
  city: string;

  @ApiProperty()
  @IsNumberString()
  zipcode: string;

  @ApiProperty()
  @IsString()
  state: string;

  @ApiProperty()
  @IsString()
  country: string;

  @ApiProperty()
  // @IsUUID()
  @IsString()
  user_id: string;
}
