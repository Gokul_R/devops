import { Module } from '@nestjs/common';
import { ClientAddressService } from './client-address.service';
import { ClientAddressController } from './client-address.controller';

@Module({
  controllers: [ClientAddressController],
  providers: [ClientAddressService]
})
export class ClientAddressModule {}
