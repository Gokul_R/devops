import { Test, TestingModule } from '@nestjs/testing';
import { ClientAddressController } from './client-address.controller';
import { ClientAddressService } from './client-address.service';

describe('ClientAddressController', () => {
  let controller: ClientAddressController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClientAddressController],
      providers: [ClientAddressService],
    }).compile();

    controller = module.get<ClientAddressController>(ClientAddressController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
