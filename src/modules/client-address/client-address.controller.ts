import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ClientAddressService } from './client-address.service';
import CreateClientAddressDto from './dto/create-client-address.dto';
import { UpdateClientAddressDto } from './dto/update-client-address.dto';
import { ApiTags } from '@nestjs/swagger';
@ApiTags('Client Address')
@Controller('client-address')
export class ClientAddressController {
  constructor(private readonly clientAddressService: ClientAddressService) {}

  
  @Post()
  create(@Body() createClientAddressDto: CreateClientAddressDto) {
    return this.clientAddressService.create(createClientAddressDto);
  }

  @Get()
  findAll() {
    return this.clientAddressService.findAll();
  }

  @Get(':user_id')
  findOne(@Param('user_id') id: string) {
    return this.clientAddressService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateClientAddressDto: UpdateClientAddressDto,
  ) {
    return this.clientAddressService.update(id, updateClientAddressDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.clientAddressService.remove(id);
  }
}
