import {
  IsNumber,
  IsDate,
  Min,
  Max,
  IsNotEmpty,
  IsString
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export class CreateFlashSaleDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  service_id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  branch_id: string;

  @ApiProperty()
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsDate()
  @IsNotEmpty()
  start_time: Date | null;

  @ApiProperty()
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsDate()
  @IsNotEmpty()
  end_time: Date | null;

  @ApiProperty()
  @IsNumber()
  @Min(0)
  @Max(100)
  @IsNotEmpty()
  discount_percentage: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  price_before_sale: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  price_after_sale: number;

  @ApiProperty()
  @IsNumber()
  @Min(0)
  @IsNotEmpty()
  quantity_available: number;

  @ApiProperty()
  @IsNumber()
  @Min(0)
  sold_quantity: number;
}
