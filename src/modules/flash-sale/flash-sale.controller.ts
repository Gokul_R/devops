import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { FlashSaleService } from './flash-sale.service';
import { CreateFlashSaleDto } from './dto/create-flash-sale.dto';
import { UpdateFlashSaleDto } from './dto/update-flash-sale.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM106, EM116, EM127 } from 'src/core/constants';
@ApiTags('Flash Sale')
@Controller('flash-sale')
export class FlashSaleController {
  constructor(private readonly flashSaleService: FlashSaleService) { }

  // @Post()
  // create(@Body() createFlashSaleDto: CreateFlashSaleDto) {
  //   return this.flashSaleService.create(createFlashSaleDto);
  // }

  @Get()
  async findAll() {
    try {
      let data = await this.flashSaleService.findFlashSales();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  // @Get(':id')
  // async findOne(@Param('id') id: string) {
  //   try {
  //     let data = await this.flashSaleService.findOne(id);
  //     return HandleResponse.buildSuccessObj(EC200, EM106, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  // @Patch(':id')
  // async update(
  //   @Param('id') id: string,
  //   @Body() updateFlashSaleDto: UpdateFlashSaleDto,
  // ) {
  //   try {
  //     let data = await this.flashSaleService.update(id, updateFlashSaleDto);
  //     return HandleResponse.buildSuccessObj(EC200, EM116, data);
  //   } catch (error) {
  //     return HandleResponse.buildErrObj(null, EM100, error);
  //   }
  // }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      let data = await this.flashSaleService.remove(id);
      return HandleResponse.buildSuccessObj(EC200, EM127, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
