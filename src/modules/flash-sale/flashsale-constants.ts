import moment from 'moment';
import { Op } from 'sequelize';
import Helpers from 'src/core/utils/helpers';

export class FlashSaleConstants {
  static getFlashSaleQry(): any {
    let flashSaleQry = {
      [Op.and]: [
        {
          start_date: {
            [Op.lte]:moment( new Date()).format("YYYY/MM/DD"),
          },
        },
        {
          end_date: {
            [Op.gte]:moment( new Date()).format("YYYY/MM/DD"),
          },
        },
      ],
    };
    return flashSaleQry;
  }
}
