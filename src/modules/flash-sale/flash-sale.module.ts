import { Module } from '@nestjs/common';
import { FlashSaleService } from './flash-sale.service';
import { FlashSaleController } from './flash-sale.controller';
import { SharedModule } from '../shared/shared.module';
import { flashSaleProviders } from './flash-sale.providers';

@Module({
  imports: [SharedModule],
  controllers: [FlashSaleController],
  providers: [FlashSaleService,...flashSaleProviders],
  exports:[FlashSaleService]
})
export class FlashSaleModule {}
