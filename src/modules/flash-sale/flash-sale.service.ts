import { Inject, Injectable } from '@nestjs/common';
import { CreateFlashSaleDto } from './dto/create-flash-sale.dto';
import { UpdateFlashSaleDto } from './dto/update-flash-sale.dto';
import { BaseService } from '../../base.service';
import FlashSale from 'src/core/database/models/FlashSale';
import MasterServiceCategory from 'src/core/database/models/MasterServiceCategory';
import { ModelCtor, Sequelize } from 'sequelize-typescript';
import Service from 'src/core/database/models/Services';
import { SERVICE_ATTRIBUTES } from '../../core/attributes/services';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { SharedService } from '../shared/shared.service';
import { Op } from 'sequelize';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { LOCATION_ATTRIBUTES } from 'src/core/attributes/location';
import { BRANCH_REPOSITORY, LOCATION_REPOSITORY, RADIUS, SEQUELIZE, SERVICES_REPOSITORY } from 'src/core/constants';
import Location from 'src/core/database/models/Location';
import Branch from 'src/core/database/models/Branch';
import { HomeDto } from '../users/dto/home.dto';
import { FlashSaleConstants } from './flashsale-constants';

@Injectable()
export class FlashSaleService extends BaseService<FlashSale> {
  protected model: ModelCtor<FlashSale>;
  flash_sale_qry: SequelizeFilter<FlashSale>;
  distance_formale: any;
  distance_qry: any;

  constructor(
    @Inject(SEQUELIZE) private sequelize: Sequelize,
    @Inject(LOCATION_REPOSITORY) private locationRepo: typeof Location,
    @Inject(BRANCH_REPOSITORY) private readonly branchRepo: typeof Branch,
    private sharedService: SharedService,
  ) {
    super();
    this.model = FlashSale as typeof FlashSale;
  }
  init() {
    this.flash_sale_qry = {
      where: FlashSaleConstants.getFlashSaleQry(),

      include: [
        {
          attributes: [
            SERVICE_ATTRIBUTES.SERVICE_CATEGORY_ID,
            SERVICE_ATTRIBUTES.SERVICE_NAME,
            SERVICE_ATTRIBUTES.AMOUNT,
            SERVICE_ATTRIBUTES.branch_id,
          ],
          model: Service,
          include: {
            attributes: [
              BRANCH_ATTRIBUTES.BRANCH_NAME,
              this.distance_qry && [this.sequelize.literal(this.distance_qry), 'distance'],
            ].filter((item) => item != null),
            model: this.branchRepo,
            include: {
              attributes: [
                LOCATION_ATTRIBUTES.address,
                LOCATION_ATTRIBUTES.area,
                LOCATION_ATTRIBUTES.city,
                LOCATION_ATTRIBUTES.state,
                LOCATION_ATTRIBUTES.latitude,
                LOCATION_ATTRIBUTES.longitude,
              ],
              model: this.locationRepo,
              where: this.distance_formale ? { [Op.and]: [this.distance_formale] } : {},
            },
            required: true,
          },
          where: { is_active: true },
          required: true,
        },
      ],
    };
  }
  async findFlashSales(body?: HomeDto) {
    this.distance_formale = null;
    this.distance_qry = null;
    if (body) this.addDistanceQry(body);
    this.init();
    if (body) this.flash_sale_qry.limit = 4;
    this.flash_sale_qry.where.is_active = true;
    return await super.findAll(this.flash_sale_qry);
  }
  addDistanceQry(body: HomeDto) {
    // if ('lat' in body) {
    let { lat, lng } = body;
    this.distance_qry = `(6371 * acos(cos(radians(${
      lat || 0
    })) * cos(radians("service->branch->location"."latitude")) * cos(radians("service->branch->location"."longitude") - radians(${
      lng || 0
    })) + sin(radians(${lat || 0})) * sin(radians("service->branch->location"."latitude"))))`;

    this.distance_formale = this.sequelize.literal(`${this.distance_qry} <= ${RADIUS}`);
    // }
  }
}
