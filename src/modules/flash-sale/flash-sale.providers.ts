import { commonProvider } from "src/core/interfaces/common-providers";

export const flashSaleProviders = [
commonProvider.location_repo,
commonProvider.branch_repository,
commonProvider.sequelize_repo,
commonProvider.service_repository
];
