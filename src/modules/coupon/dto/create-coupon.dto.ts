import { ApiProperty, PickType } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsString,
  IsArray,
  IsEnum,
  IsNumber,
  IsDate,
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsInt,
  isArray,
} from 'class-validator';
import { SharedDto } from 'src/core/interfaces/shared.dto';

enum CouponType {
  AMOUNT = 'amount',
  PERCENTAGE = 'percentage',
}
enum LimitType {
  LIMITED = 'limited',
  UNLIMITED = 'unlimited',
}

export class CreateCouponDto {
  @ApiProperty({ example: 'SAVE100' })
  @IsNotEmpty()
  @IsString()
  coupon_code: string;

  @ApiProperty({ example: '62a8332aecb4a12ed2bd2f44' })
  @IsNotEmpty()
  //   @IsArray()
  @IsOptional() // Make the property optional
  @IsString({ each: true })
  branch_id: string;

  //   @IsNotEmpty()
  //   @IsArray()
  //   @IsString({ each: true })
  //   service_id: string;
  @ApiProperty({ example: 'amount' })
  @IsNotEmpty()
  @IsEnum(CouponType)
  discount_type: CouponType;

  @ApiProperty({ example: 10 })
  @IsNotEmpty()
  @IsNumber()
  discount_value: number;

  // @ApiProperty()
  // @IsNotEmpty()
  // @IsString()
  // discount_percentage: string;

  @ApiProperty({ example: 'limited' })
  @IsEnum(LimitType)
  @IsString()
  quantity_type: LimitType;

  @ApiProperty({ example: 100 })
  @IsNotEmpty()
  @IsNumber()
  quantity_value: number;

  @ApiProperty({ example: 3 })
  @IsInt()
  per_user_limit: number;

  @ApiProperty({ example: 100 })
  @IsNotEmpty()
  @IsNumber()
  minimum_spend: number;

  @ApiProperty({ example: 500 })
  @IsNotEmpty()
  @IsNumber()
  max_discount_amount: number;

  @ApiProperty({ example: new Date() })
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsNotEmpty()
  @IsDate()
  start_date: string | null;

  @ApiProperty({ example: new Date() })
  @Transform(({ value }) => value ? new Date(value) : null)
  @IsNotEmpty()
  @IsDate()
  end_date: string | null;

  // @ApiProperty()
  // @IsString()

  // grouped_coupon_status: string;

  // @ApiProperty({ example: true })
  // @IsBoolean()
  // grouped_coupon_type: boolean;

  @ApiProperty({ example: 'Best mega offer' })
  @IsString()
  description: string;

  @ApiProperty({ example: false })
  @IsNotEmpty()
  @IsBoolean()
  is_active: boolean;
}

export class ApplyCouponDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  user_id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  coupon_id: string;
}
export class FindCouponByBranchDto extends PickType(SharedDto, [
  'branch_id',
  'user_id',
]) {
  @ApiProperty()
  @IsArray()
  @IsOptional()
  category_ids: string[];
  @ApiProperty()
  @IsArray()
  @IsOptional()
  service_ids: string[];
  @ApiProperty()
  @IsArray()
  @IsOptional()
  service_category_ids: string[];
  @ApiProperty()
  @IsString()
  @IsOptional()
  vendor_id: string;
}
