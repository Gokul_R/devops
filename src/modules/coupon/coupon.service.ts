import { Inject, Injectable } from '@nestjs/common';
import { ApplyCouponDto, CreateCouponDto, FindCouponByBranchDto } from './dto/create-coupon.dto';
import Helpers from 'src/core/utils/helpers';
import HandleResponse from 'src/core/utils/handle_response';
import {
  COUPON_BRANCH_REPOSITORY,
  COUPON_CATEGORY_MAPPING_REPOSITORY,
  COUPON_SERVICE_CATEGORY_MAPPING_REPOSITORY,
  COUPON_SERVICE_REPOSITORY,
  EC200,
  EC201,
  EM104,
  EM116,
  SEQUELIZE,
  USER_SERVICE,
} from 'src/core/constants';
import Coupon from 'src/core/database/models/Coupons';
import AppliedCoupon from 'src/core/database/models/AppliedCoupons';
import { BaseService } from 'src/base.service';
import { ModelCtor, Sequelize } from 'sequelize-typescript';
import { COUPON_ATTRIBUTES } from 'src/core/attributes/coupon';
import CouponBranchMapping from 'src/core/database/models/CouponBranchMapping';
import User from 'src/core/database/models/User';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import ReferalManage from 'src/core/database/models/ReferalManage';
import { Op } from 'sequelize';
import MarketingGroup from 'src/core/database/models/MarketingGroup';
import { UsersService } from '../users/users.service';
import { SharedService } from '../shared/shared.service';
import CouponCategoryMapping from 'src/core/database/models/CouponCategoryMapping';
import CouponServiceCategoryMapping from 'src/core/database/models/CouponServiceCategoryMapping';
import CouponServiceMapping from 'src/core/database/models/CouponServiceMapping';
import { ServicesService } from '../services/services.service';
import CollectedCoupons from 'src/core/database/models/CollectedCoupons';
import { Errors } from 'src/core/constants/error_enums';
import { logger } from 'src/core/utils/logger';
import moment from 'moment-timezone';

@Injectable()
export class CouponService extends BaseService<Coupon> {
  protected model: ModelCtor<Coupon>;
  coupon_qry: SequelizeFilter<Coupon>;
  constructor(
    @Inject(SEQUELIZE) private sequelize: Sequelize,
    @Inject(COUPON_BRANCH_REPOSITORY)
    private coupon_branch_repository: CouponBranchMapping,
    @Inject(COUPON_CATEGORY_MAPPING_REPOSITORY)
    private coupon_category_repository: CouponBranchMapping,
    @Inject(COUPON_SERVICE_CATEGORY_MAPPING_REPOSITORY)
    private coupon_service_category_repository: CouponServiceCategoryMapping,
    @Inject(COUPON_SERVICE_REPOSITORY)
    private coupon_service_repository: CouponServiceMapping,
    private sharedService: SharedService,
    private servicesService: ServicesService,
  ) {
    super();
    this.model = Coupon;
  }
  init() {
    this.coupon_qry = {
      attributes: [
        COUPON_ATTRIBUTES.COUPON_CODE,
        COUPON_ATTRIBUTES.DESCRIPTION,
        COUPON_ATTRIBUTES.COUPON_NAME,
        COUPON_ATTRIBUTES.ID,
        COUPON_ATTRIBUTES.DISCOUNT_TYPE,
        COUPON_ATTRIBUTES.DISCOUNT_VALUE,
        COUPON_ATTRIBUTES.PER_USER_LIMIT,
        COUPON_ATTRIBUTES.MINIMUM_SPEND,
        COUPON_ATTRIBUTES.MAX_DISCOUNT_AMOUNT,
        [this.sequelize.col('applied_coupons.is_applied'), 'is_applied'],
        [this.sequelize.fn('COALESCE', this.sequelize.col('collected_coupons.is_collected'), false), 'is_collected'],
        COUPON_ATTRIBUTES.COUPON_NAME,
        COUPON_ATTRIBUTES.START_DATE,
        COUPON_ATTRIBUTES.END_DATE,
        COUPON_ATTRIBUTES.IMAGE,
      ],

      where: {
        is_active: true,
        per_user_limit: {
          [Op.or]: [
            this.sequelize.where(this.sequelize.fn('COALESCE', this.sequelize.col('applied_coupons.used_count'), 0), {
              [Op.lt]: this.sequelize.col('Coupon.per_user_limit'),
            }),
            { [Op.eq]: null },
          ],
        },
        [Op.or]: [
          { quantity_type: 'unlimited' },
          {
            quantity_value: {
              [Op.gt]: this.sequelize.literal(`(
            SELECT (COALESCE(SUM("applied_coupons"."used_count"),0))
            FROM applied_coupons
            WHERE "applied_coupons"."coupon_id" = "Coupon"."id"
          )`),
            },
          },
          { lifetime_coupon: true },
          {
            end_date: {
              [Op.gte]: moment(new Date().toISOString()).tz('Asia/Kolkata').format('YYYY-MM-DD'),
            },
            start_date: {
              [Op.lte]: moment(new Date().toISOString()).tz('Asia/Kolkata').format('YYYY-MM-DD'),
            },
          },
        ],
      },
    };
  }
  async findCouponsForUser(user_id: string) {
    logger.info(`Find_Coupon_for_User_Entry_UserId: ` + JSON.stringify(user_id));
    this.init();
    let user: User = await this.sharedService.findOneUserById(user_id);
    let { invited_ref_code, mobile_no } = user || {};
    this.coupon_qry.include = [
      {
        attributes: [],
        model: AppliedCoupon,
        where: { user_id: user_id },
        required: false,
      },
      {
        attributes: [],
        model: CollectedCoupons,
        where: { user_id: user_id },
        required: false,
      },
      // this.servicesService.includeServices(null),
      {
        attributes: [],
        model: MarketingGroup,
        where: {
          is_active: true,
          [Op.or]: [
            invited_ref_code && { referal_code: invited_ref_code },
            { users_mobile: { [Op.contains]: [mobile_no] } },
          ],
        },
        required: true,
      },
    ];
    let coupons = await super.findAll(this.coupon_qry);

    logger.info(`Find_Coupon_for_User_Exit:` + JSON.stringify(coupons));
    return coupons;
  }
  async findAllCoupons(user_id: string) {
    let coupon_qry = {
      attributes: [
        COUPON_ATTRIBUTES.COUPON_CODE,
        COUPON_ATTRIBUTES.COUPON_NAME,
        COUPON_ATTRIBUTES.DESCRIPTION,
        COUPON_ATTRIBUTES.ID,
        COUPON_ATTRIBUTES.DISCOUNT_TYPE,
        COUPON_ATTRIBUTES.DISCOUNT_VALUE,
        COUPON_ATTRIBUTES.START_DATE,
        COUPON_ATTRIBUTES.END_DATE,
        [this.sequelize.fn('COALESCE', this.sequelize.col('collected_coupons.is_collected'), false), 'is_collected'],
        COUPON_ATTRIBUTES.IMAGE,
      ],
      include: [
        {
          attributes: ['is_applied'],
          model: AppliedCoupon,
          where: { user_id: user_id },
          required: false,
        },
        {
          attributes: [],
          model: CollectedCoupons,
          where: { user_id: user_id },
          required: false,
        },
      ],
      where: { is_deleted: false, is_active: true },
    };
    let coupons = await super.findAll(coupon_qry);
    let formatted = JSON.stringify(coupons);
    coupons = JSON.parse(formatted);
    let formatted_coupons = await coupons?.map((data) => {
      let formatted = {
        ...data,
        is_applied: data?.applied_coupons[0]?.is_applied,
      };
      delete formatted?.applied_coupons;
      return formatted;
    });
    return formatted_coupons;
  }
  /**
   * this function apply or remove coupons
   * @param applyCoupon \userid & coupon id
   * @returns applied or removed response
   */
  async applyCoupon(applyCoupon: ApplyCouponDto) {
    logger.info(`Apply_Coupon_Entry: ` + JSON.stringify(applyCoupon));
    let couponQry = {
      where: {
        user_id: applyCoupon.user_id,
        coupon_id: applyCoupon.coupon_id,
      },
      defaults: applyCoupon,
    };
    let applied_coupon = await Helpers.findOrCreate(AppliedCoupon, couponQry);
    if (!applied_coupon[1]) {
      let update_body = applied_coupon[0].is_applied ? { is_applied: false } : { is_applied: true };
      let remove_coupon = await Helpers.update(
        AppliedCoupon,
        {
          where: {
            user_id: applyCoupon.user_id,
            coupon_id: applyCoupon.coupon_id,
          },
        },
        update_body,
      );
      logger.info(`Apply_Coupon_Exit: ` + JSON.stringify(remove_coupon));
      return { code: EC200, msg: EM116, data: remove_coupon };
    }
    logger.info(`Apply_Coupon_Exit: ` + JSON.stringify(applied_coupon));
    return { code: EC201, msg: EM104, data: applied_coupon };
  }

  async collectCoupon(applyCoupon: ApplyCouponDto) {
    logger.info(`User_CollectCoupon_Entry: ` + JSON.stringify(applyCoupon));
    try {
      let couponQry = {
        where: {
          user_id: applyCoupon.user_id,
          coupon_id: applyCoupon.coupon_id,
        },
        defaults: applyCoupon,
      };
      let collected_coupon: CollectedCoupons[] = await Helpers.findOrCreate(CollectedCoupons, couponQry);
      // if (!collected_coupon[1]) {
      //   let update_body = collected_coupon[0].is_collected ? { is_collected: false } : { is_collected: true };
      //   let remove_coupon = await Helpers.update(
      //     CollectedCoupons,
      //     {
      //       where: {
      //         user_id: applyCoupon.user_id,
      //         coupon_id: applyCoupon.coupon_id,
      //       },
      //     },
      //     update_body,
      //   );
      //   logger.info(`User_CollectCoupon_Exit: ` + JSON.stringify(remove_coupon));
      //   return { code: EC200, msg: EM116, data: remove_coupon };
      // }
      collected_coupon.pop();
      return collected_coupon;
    } catch (error) {
      return error;
    }
  }

  async findByShop(body: FindCouponByBranchDto) {
    let { branch_id, user_id, category_ids, service_ids, service_category_ids, vendor_id } = body;
    let coupon_qry = {
      attributes: [
        COUPON_ATTRIBUTES.COUPON_CODE,
        COUPON_ATTRIBUTES.COUPON_NAME,
        COUPON_ATTRIBUTES.DESCRIPTION,
        COUPON_ATTRIBUTES.ID,
        COUPON_ATTRIBUTES.DISCOUNT_TYPE,
        COUPON_ATTRIBUTES.DISCOUNT_VALUE,
        COUPON_ATTRIBUTES.PER_USER_LIMIT,
        COUPON_ATTRIBUTES.MINIMUM_SPEND,
        COUPON_ATTRIBUTES.MAX_DISCOUNT_AMOUNT,
        COUPON_ATTRIBUTES.START_DATE,
        COUPON_ATTRIBUTES.END_DATE,
        [this.sequelize.col('coupon_branch_mapping"."branch_id'), 'branch_id'],
        [this.sequelize.col('coupon_category_mapping"."category_id'), 'category_id'],
        [this.sequelize.col('coupon_service_category_mapping"."service_category_id'), 'service_category_id'],
        [this.sequelize.col('coupon_service_mapping"."service_id'), 'service_id'],
        [this.sequelize.col('applied_coupons.is_applied'), 'is_applied'],
        COUPON_ATTRIBUTES.IMAGE,
      ],
      include: [
        {
          attributes: [],
          model: this.coupon_branch_repository,
          // where: {
          //   [Op.or]: [{ branch_id: branch_id }],
          // },
          // required: false,
        },
        {
          attributes: [],
          model: this.coupon_category_repository,
          // where: service_category_ids
          //   ? { is_active: true, [Op.or]: [{ category_id: category_ids }] }
          //   : { is_active: true },
          // required: false,
        },
        {
          attributes: [],
          model: this.coupon_service_category_repository,
          // where: service_category_ids
          //   ? {
          //       [Op.or]: [{ service_category_id: service_category_ids }],
          //     }
          //   : {},
          // required: false,
        },
        {
          attributes: [],
          model: this.coupon_service_repository,
          // where: service_ids
          //   ? {
          //       [Op.or]: [{ service_id: service_ids }],
          //     }
          //   : {},
          // required: false,
        },
        {
          attributes: [],
          model: AppliedCoupon,
          where: { user_id: user_id },
          required: false,
        },
      ],
      where: {
        is_active: true,
        [Op.or]: [
            { lifetime_coupon: true },
            {
              end_date: {
                [Op.gte]: new Date().setHours(0, 0, 0, 0),
              },
              start_date: {
                [Op.lte]: new Date().setHours(0, 0, 0, 0),
              },
            },
          ],
        
        [Op.and]: [

          Sequelize.literal(`
            (CASE WHEN 
            "coupon_branch_mapping"."branch_id" ='${branch_id}' 
            AND  "coupon_category_mapping"."category_id" IN ('${category_ids}')
            AND  "coupon_service_category_mapping"."service_category_id" IN ('${service_category_ids}')
            AND  "coupon_service_mapping"."service_id" IN ('${service_ids}') THEN true ELSE false END) OR
            
            (CASE WHEN 
            "coupon_branch_mapping"."branch_id" ='${branch_id}' 
            AND "coupon_category_mapping"."category_id" IN ('${category_ids}')
            AND "coupon_service_category_mapping"."service_category_id" IN ('${service_category_ids}')
            THEN true ELSE false END) OR 

            (CASE WHEN 
              "coupon_branch_mapping"."branch_id" ='${branch_id}' 
              AND "coupon_category_mapping"."category_id" IN ('${category_ids}')
              THEN true ELSE false END) OR 

            (CASE WHEN 
                "coupon_branch_mapping"."branch_id" ='${branch_id}' 
                AND "vendor_id" = '${vendor_id}'
                THEN true ELSE false END) OR 

                (CASE WHEN 
                  "coupon_branch_mapping"."branch_id" IS null 
                  AND  "coupon_category_mapping"."category_id" IS null
                  AND  "coupon_service_category_mapping"."service_category_id" IS null
                  AND  "coupon_service_mapping"."service_id" IS null THEN true ELSE false END)

            `),

          // {
          //   [Op.or]: [
          //     { '$coupon_branch_mapping.branch_id$': { [Op.ne]: null, [Op.eq]: branch_id } },
          //     { '$coupon_category_mapping.category_id$': { [Op.ne]: null, [Op.in]: category_ids } },
          //     {
          //       '$coupon_service_category_mapping.service_category_id$': {
          //         [Op.ne]: null,
          //         [Op.in]: service_category_ids,
          //       },
          //     },
          //     { '$coupon_service_mapping.service_id$': { [Op.ne]: null, [Op.in]: service_ids } },
          //     {
          //       [Op.and]: [
          //         { '$coupon_branch_mapping.branch_id$': { [Op.is]: null } },
          //         { '$coupon_category_mapping.category_id$': { [Op.is]: null} },
          //         {
          //           '$coupon_service_category_mapping.service_category_id$': {
          //             [Op.is]: null                },
          //         },
          //         { '$coupon_service_mapping.service_id$': { [Op.is]: null} },
          //         { marketing_ids: { [Op.is]: null} },
          //       ],
          //     },
          //   ],

          // },

          {
            per_user_limit: {
              [Op.or]: [
                this.sequelize.where(
                  this.sequelize.fn('COALESCE', this.sequelize.col('applied_coupons.used_count'), 0),
                  {
                    [Op.lt]: this.sequelize.col('Coupon.per_user_limit'),
                  },
                ),
                // { [Op.eq]: null },
              ],
            },
          },
          {
            [Op.or]: [
              { quantity_type: 'unlimited' },
              {
                quantity_value: {
                  [Op.gt]: this.sequelize.literal(`(
            SELECT (COALESCE(SUM("applied_coupons"."used_count"),0))
            FROM applied_coupons
            WHERE "applied_coupons"."coupon_id" = "Coupon"."id"
          )`),
                },
              },
            ],
          },
          
        ],
      },
    };

    let coupons = await super.findAll(coupon_qry);

    const filteredCoupons = coupons.filter(coupon => this.matchesCriteria(coupon, body));
    // let user_coupons = await this.findCouponsForUser(user_id);
    let collected_coupons = await this.findCollectedCoupons(user_id);
    const collectedCouponCodes = collected_coupons.map((coupon) => coupon.coupon_code);
    const filteredSalonCoupons = filteredCoupons.filter((coupon) => !collectedCouponCodes.includes(coupon.coupon_code));
    return { salon_coupons: filteredSalonCoupons, collected_coupons: collected_coupons };
  }

  matchesCriteria(coupon: any, criteria: any) {
    return ((coupon.dataValues.branch_id == criteria.branch_id || null) &&
      (criteria.category_ids.includes(coupon?.dataValues?.category_id) || coupon?.dataValues?.category_id == null) &&
      (criteria.service_category_ids.includes(coupon?.dataValues?.service_category_id) || coupon?.dataValues?.service_category_id == null) &&
      (criteria.service_ids.includes(coupon?.dataValues?.service_id) || coupon?.dataValues?.service_id == null)) ||
      (coupon.dataValues.branch_id == null && coupon.dataValues.category_id == null && coupon.dataValues.service_category_id == null && coupon.dataValues.service_id == null)
  }

  async increaseCouponUsedCount(user_id: string, coupon_id: string) {
    let where_qry = { where: { user_id: user_id, coupon_id: coupon_id } };
    let updated_coupon = await Helpers.update(AppliedCoupon, where_qry, {
      used_count: this.sequelize.literal('used_count + 1'),
      is_applied: false,
    });
  }

  async findCollectedCoupons(user_id: string) {
    this.init();
    this.coupon_qry.include = [
      {
        attributes: [],
        model: AppliedCoupon,
        where: { user_id: user_id },
        required: false,
      },
      {
        attributes: [],
        model: CollectedCoupons,
        where: { user_id: user_id },
        required: true,
      },
    ];
    let coupons = await super.findAll(this.coupon_qry);
    return coupons;
  }
}

// async findByShop(body: FindCouponByBranchDto) {
//   let { branch_id, user_id } = body;
//   let coupon_qry = {
//     where: {
//       branch_id: branch_id,
//     },
//     include: [
//       {
//         attributes: [
//           COUPON_ATTRIBUTES.ID,
//           COUPON_ATTRIBUTES.COUPON_CODE,
//           COUPON_ATTRIBUTES.DESCRIPTION,
//           COUPON_ATTRIBUTES.DISCOUNT_TYPE,
//           COUPON_ATTRIBUTES.DISCOUNT_VALUE,
//           COUPON_ATTRIBUTES.START_DATE,
//           COUPON_ATTRIBUTES.END_DATE,
//           COUPON_ATTRIBUTES.MINIMUM_SPEND,
//         ],
//         model: Coupon,
//         include: [
//           {
//             attributes: ['is_applied'],
//             model: AppliedCoupon,
//             where: { user_id: user_id },
//             required: false,
//           },
//         ],
//       },
//     ],
//   };
//   let coupons: any = await Helpers.findAll(CouponBranchMapping, coupon_qry);
//   let formatted = JSON.stringify(coupons);
//   coupons = JSON.parse(formatted);
//   let branch_coupons = await coupons?.map((data) => {
//     let formatted = {
//       ...data.coupons,
//       is_applied: data?.coupons?.applied_coupons[0]?.is_applied,
//     };
//     delete formatted?.applied_coupons;
//     console.log(formatted);
//     return formatted;
//   });
//   // let user_coupons = await this.findCouponsForUser(user_id);

//   return branch_coupons;
// }
