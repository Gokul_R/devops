import { Controller, Get, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe } from '@nestjs/common';
import { CouponService } from './coupon.service';
import { ApplyCouponDto, CreateCouponDto, FindCouponByBranchDto } from './dto/create-coupon.dto';
import { UpdateCouponDto } from './dto/update-coupon.dto';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import {
  EC200,
  EM127,
  EM100,
  EM106,
  EM116,
  EM104,
  EM133,
} from 'src/core/constants';
@ApiTags('Coupons')
@Controller('coupon')
export class CouponController {
  constructor(private readonly couponService: CouponService) {}

  @Get('by-user/:user_id')
  async findAll(@Param('user_id') user_id: string) {
    try {
      let data = await this.couponService.findCouponsForUser(user_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('by-branch')
  async findOne(@Body() req: FindCouponByBranchDto) {
    try {
      let data = await this.couponService.findByShop(req);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Post('apply')
  async applyCoupon(@Body() applyCoupon: ApplyCouponDto) {
    try {
      let data = await this.couponService.applyCoupon(applyCoupon);
      return HandleResponse.buildSuccessObj(data.code, data.msg, data.data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('collect')
  async collectCoupon(@Body() applyCoupon: ApplyCouponDto) {
    try {
      let data = await this.couponService.collectCoupon(applyCoupon);
      return HandleResponse.buildSuccessObj(EC200, EM133, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
