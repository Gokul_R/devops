import { Sequelize } from "sequelize-typescript";
import { SEQUELIZE } from "src/core/constants";
import { commonProvider } from "src/core/interfaces/common-providers";

export const couponProviders = [
 
    {
        provide: SEQUELIZE,
        useValue: Sequelize,
      },
    commonProvider.users_service,
    commonProvider.marketing_group_repository,
    commonProvider.coupon_branch_repository,
    commonProvider.coupon_category_mapping_repository,
    commonProvider.coupon_service_category_mapping_repository,
    commonProvider.coupon_service_repository,

];