import { Module } from '@nestjs/common';
import { CouponService } from './coupon.service';
import { CouponController } from './coupon.controller';
import { couponProviders } from './coupon.providers';
import { UsersService } from '../users/users.service';
import { UsersModule } from '../users/users.module';
import { SharedModule } from '../shared/shared.module';
import { ServicesService } from '../services/services.service';
import { ServicesModule } from '../services/services.module';

@Module({
  imports:[SharedModule,ServicesModule],
  controllers: [CouponController],
  providers: [CouponService,...couponProviders],
  exports: [CouponService],
})
export class CouponModule {}
