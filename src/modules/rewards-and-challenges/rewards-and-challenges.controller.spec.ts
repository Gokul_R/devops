import { Test, TestingModule } from '@nestjs/testing';
import { RewardsAndChallengesController } from './rewards-and-challenges.controller';
import { RewardsAndChallengesService } from './rewards-and-challenges.service';

describe('RewardsAndChallengesController', () => {
  let controller: RewardsAndChallengesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RewardsAndChallengesController],
      providers: [RewardsAndChallengesService],
    }).compile();

    controller = module.get<RewardsAndChallengesController>(RewardsAndChallengesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
