import {
  Body,
  Controller,
  Get,
  Post,
  Patch,
  Delete,
  Param,
} from '@nestjs/common';
import { RewardsAndChallengesService } from './rewards-and-challenges.service';
import { CreateChallenge } from './dto/createChallenge.dto';
import { CreateRewards } from './dto/createRewards';
import { ApiTags } from '@nestjs/swagger';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EC500, EM100, EM106 } from 'src/core/constants';

@Controller()
@ApiTags('Rewards And Challenges')
export class RewardsAndChallengesController {
  constructor(
    private readonly rewardsAndChallengesService: RewardsAndChallengesService,
  ) {}

  @Get('/get-challenges')
 async getChallenges() {
    try {
      let data =await this.rewardsAndChallengesService.getChalleges();
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Patch('/update-challenges')
  updateChallenges(@Body() updateChallenge: CreateChallenge) {
    return this.rewardsAndChallengesService.updateChalleges(updateChallenge);
  }

  /*----------------------------------------------------------------------------------------------*/

  @Get('/get-rewards')
  getRewards() {
    return this.rewardsAndChallengesService.getRewards();
  }

  @Patch('/update-rewards')
  updateRewards(@Body() updateRewards: CreateRewards) {
    return this.rewardsAndChallengesService.updateRewards(updateRewards);
  }

  /**------------------------------------------------------------------------------------------- */

  @Get('/get-rewards-challenges/:user_id')
  getRewardsChallenges(@Param() user_id: any) {
    return this.rewardsAndChallengesService.getRewardsChallenges(user_id);
  }

  @Patch('/update-coins')
  updateRewardsChallengesCoins(@Body() req_data: any) {
    return this.rewardsAndChallengesService.updateRewardsChallengesCoins(
      req_data,
    );
  }
}
