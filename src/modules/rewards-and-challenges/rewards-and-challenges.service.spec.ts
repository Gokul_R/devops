import { Test, TestingModule } from '@nestjs/testing';
import { RewardsAndChallengesService } from './rewards-and-challenges.service';

describe('RewardsAndChallengesService', () => {
  let service: RewardsAndChallengesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RewardsAndChallengesService],
    }).compile();

    service = module.get<RewardsAndChallengesService>(RewardsAndChallengesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
