import { Injectable } from '@nestjs/common';
import { Op, Sequelize } from 'sequelize';
import { ModelCtor } from 'sequelize-typescript';
import { BaseService } from 'src/base.service';
import { EM104, EC200, EM106, EM127 } from 'src/core/constants';
import Challenges from 'src/core/database/models/Challenges';
import RewardChallengeHistroy from 'src/core/database/models/RewardChallengeHistroy';
import Rewards from 'src/core/database/models/Rewards';
import User from 'src/core/database/models/User';
import HandleResponse from 'src/core/utils/handle_response';
import Helpers from 'src/core/utils/helpers';

@Injectable()
export class RewardsAndChallengesService extends BaseService<Challenges> {
    protected model: ModelCtor<Challenges>;
    constructor() {
        super();
        this.model = Challenges;
    }
    async getChalleges() {
        const data = await super.findAll();
        return data;
    }

    async updateChalleges(req_data: any) {
        let where = {
            where: { id: req_data.id }
        };
        const getData = await Helpers.update(Challenges, where, req_data);
        return getData;
    }

    async deleteChalleges(req_data: any) {
        const getData = await Helpers.collectionDelete(Challenges, { where: { id: req_data.id } });
        return getData;
    }

    /**------------------------------------------------------------------------------------------- */

    async createRewards(req_data: any) {
        const ceratedData = await Helpers.create(Rewards, req_data);
        return HandleResponse.buildSuccessObj(EC200, EM104, ceratedData);
    }

    async getRewards() {
        const getData = await Helpers.findAll(Rewards, {});
        return HandleResponse.buildSuccessObj(EC200, EM106, getData);
    }

    async updateRewards(req_data: any) {
        let where = {
            where: { id: req_data.id }
        };
        const getData = await Helpers.update(Rewards, where, req_data);
        return getData;
    }

    async deleteRewards(req_data: any) {
        const getData = await Helpers.collectionDelete(Rewards, { where: { id: req_data.id } });
        return getData;
    }

    /**------------------------------------------------------------------------------------------- */

    async getRewardsChallenges(req_data: any) {
        let where = {
            where: {
                end_date: {
                    [Op.gte]:
                        new Date().toISOString().slice(0, 10),
                },
                is_active: true
            }
        };
        const getUserCoinsCount = await Helpers.findAll(User,
            {
                where: { id: req_data.user_id }, attributes: ["earn_coins"]
            });
        const getChallengesData = await Helpers.findAll(Challenges,
            {
                ...where,
                include: [{
                    attributes: ['id', 'attempt_count'],
                    model: RewardChallengeHistroy,
                    required: false,
                    where: { user_id: req_data.user_id }
                }]
            });
        const getRewardsData = await Helpers.findAll(Rewards, {
            ...where,
            include: [{
                attributes: ['id', 'attempt_count'],
                model: RewardChallengeHistroy,
                required: false,
                where: { user_id: req_data.user_id }
            }]
        });
        // console.log()
        let res_data = {
            challenges: getChallengesData,
            rewards: getRewardsData,
            total_coins: getUserCoinsCount[0] ? getUserCoinsCount[0].earn_coins : 0
        };
        return HandleResponse.buildSuccessObj(EC200, EM106, res_data);
    }

    async updateRewardsChallengesCoins(req_data: any) {
        let where_table = req_data.is_rewards ? { rewards_id: req_data.rewards_id } : { challenge_id: req_data.challenge_id };
        const checkData = await RewardChallengeHistroy.count({ where: { ...where_table, user_id: req_data.user_id, is_rewards: req_data.is_rewards } });
        if (checkData) {
            let where = {
                where: {
                    ...where_table,
                    user_id: req_data.user_id,
                    is_rewards: req_data.is_rewards
                },
            };
            const update = await RewardChallengeHistroy.update({ attempt_count: Sequelize.literal(`attempt_count + 1`) }, where);
        } else {
            req_data.attempt_count = 1;
            const Insert = await Helpers.create(RewardChallengeHistroy, req_data);
        }
        const updateCoins = User.update(
            { earn_coins: Sequelize.literal(`earn_coins + ${req_data.coins}`) },
            { where: { id: req_data.user_id } });
        return HandleResponse.buildSuccessObj(EC200, EM127, []);
    }
}
