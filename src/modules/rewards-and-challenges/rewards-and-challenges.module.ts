import { Module } from "@nestjs/common";
import { RewardsAndChallengesController } from "./rewards-and-challenges.controller";
import { RewardsAndChallengesService } from "./rewards-and-challenges.service";

@Module({
    controllers: [RewardsAndChallengesController],
    providers: [RewardsAndChallengesService]
})

export class RewardsAndChallengesModule { }