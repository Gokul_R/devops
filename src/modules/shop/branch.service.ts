import { CACHE_MANAGER, Inject, Injectable, ForbiddenException, NotFoundException } from '@nestjs/common';
import Helpers from 'src/core/utils/helpers';
import Service from 'src/core/database/models/Services';
import {
  Approval,
  BRANCH_GALLERY_FOLDER_REPOSITORY,
  BRANCH_GALLERY_REPOSITORY,
  BRANCH_REPOSITORY,
  COUPON_BRANCH_REPOSITORY,
  COUPON_REPOSITORY,
  ChennaiLocation,
  LOCATION_REPOSITORY,
  RADIUS,
  SEQUELIZE,
} from 'src/core/constants';
import SlotDetail from 'src/core/database/models/SlotDetail';
import Branch from 'src/core/database/models/Branch';
import Location from 'src/core/database/models/Location';
import { GetServicesDto } from './dto/get-services-dto';
import BranchServiceCategoryMapping from 'src/core/database/models/BranchServiceCategoryMapping.ts';
import BranchServiceTypeMapping from 'src/core/database/models/BranchServiceTypeMapping';
import { BaseService } from 'src/base.service';
import { ModelCtor } from 'sequelize-typescript';
import { Double } from 'aws-sdk/clients/apigateway';
import SequelizeFilter from 'src/core/interfaces/sequelize.interface';
import { GroupedCountResultItem, Op, Sequelize, literal } from 'sequelize';
import { BRANCH_ATTRIBUTES } from 'src/core/attributes/branch';
import { FAVOURITES_ATTRIBUTES as FAVORITES_ATTRIBUTES } from 'src/core/attributes/favourite';
import { LOCATION_ATTRIBUTES } from 'src/core/attributes/location';
import Favourites from 'src/core/database/models/Favourites';
import Review from 'src/core/database/models/Reviews';
import Coupon from 'src/core/database/models/Coupons';
import { FreelancerSlot, VendorType } from 'src/core/utils/enum';
import { ExploreDto, HomeDto } from '../users/dto/home.dto';
import CouponBranchMapping from 'src/core/database/models/CouponBranchMapping';
import BranchCategoryMapping from 'src/core/database/models/BranchCategoryMapping';
import { ServicesService } from '../services/services.service';
import BranchGalleryFolder from 'src/core/database/models/BranchGalleryFolder';
import BranchGallery from 'src/core/database/models/BranchGallery';
import { BRANCH_GALLERY_FOLDER_ATTRIBUTES } from 'src/core/attributes/branch_gallery_folder';
import { BRANCH_GALLERY_ATTRIBUTES } from 'src/core/attributes/branch_gallery';
import Order from 'src/core/database/models/Order';
import { ORDER_ATTRIBUTES } from 'src/core/attributes/order';
import { APPOINTMENT_STATUS } from 'src/core/database/models/Appointment';
import moment from 'moment';
import { Cache } from 'cache-manager';
import { DateUtils } from 'src/core/utils/dateUtils';
import { SlotManagementService } from '../slot-management/slot-management.service';
import SlotManagement from 'src/core/database/models/SlotManagement';
import { Errors } from 'src/core/constants/error_enums';
import { logger } from 'src/core/utils/logger';
import { GetGalleryList } from './dto/getGalleryList.dto';

@Injectable()
export class BranchService extends BaseService<Branch> {
  protected model: ModelCtor<Branch>;
  branchQry: SequelizeFilter<Branch>;
  area_filter: string[];
  folder_qry: SequelizeFilter<BranchGalleryFolder>;

  user_id: string | null;
  lat: Double;
  lng: Double;
  constructor(
    @Inject(SEQUELIZE) private readonly sequelize: typeof Sequelize,
    @Inject(BRANCH_REPOSITORY) private readonly branchRepo: typeof Branch,
    @Inject(LOCATION_REPOSITORY)
    private readonly locationRepo: typeof Location,
    @Inject(BRANCH_GALLERY_REPOSITORY)
    private readonly branchGalleryRepo: typeof BranchGallery,
    @Inject(BRANCH_GALLERY_FOLDER_REPOSITORY)
    private readonly branchGalleryFolderRepo: typeof BranchGalleryFolder,
    private servicesService: ServicesService,
    private slotManagementService: SlotManagementService, // @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {
    super();
    this.model = branchRepo;
  }
  init() {
    this.branchQry = {
      attributes: [
        BRANCH_ATTRIBUTES.ID,
        BRANCH_ATTRIBUTES.BRANCH_NAME,
        BRANCH_ATTRIBUTES.BRANCH_EMAIL,
        BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO,
        BRANCH_ATTRIBUTES.VENDOR_TYPE,
        BRANCH_ATTRIBUTES.BRANCH_IMAGE,
        BRANCH_ATTRIBUTES.LEAVE_DAYS,
        [literal('ROUND(COALESCE(AVG("reviews"."rating"), 0)::numeric, 1)'), 'ratings'],
        [literal('COALESCE(COUNT(DISTINCT("reviews"."id")), 0)'), 'reviews_count'],
        [literal('COALESCE("favorites"."is_active", false)'), 'is_favourite'],
      ],
      include: [
        {
          attributes: [
            LOCATION_ATTRIBUTES.address,
            LOCATION_ATTRIBUTES.area,
            LOCATION_ATTRIBUTES.city,
            LOCATION_ATTRIBUTES.state,
            LOCATION_ATTRIBUTES.latitude,
            LOCATION_ATTRIBUTES.longitude,
          ],
          model: this.locationRepo,
        },
        {
          attributes: [[FAVORITES_ATTRIBUTES.IS_ACTIVE, 'is_favourite']],
          model: Favourites,
          where: this.user_id ? { user_id: this.user_id } : { user_id: `IS NULL` },
          required: false,
        },
        {
          attributes: [],
          model: Review,
          where: { is_deleted: false },
          required: false,
        },
      ],
      where: { approval_status: Approval.ACCEPTED, is_deleted: false, is_active: true, account_verified: true },
      subQuery: false,
      group: [
        'Branch.' + BRANCH_ATTRIBUTES.ID,
        BRANCH_ATTRIBUTES.BRANCH_NAME,
        BRANCH_ATTRIBUTES.BRANCH_EMAIL,
        BRANCH_ATTRIBUTES.BRANCH_CONTACT_NO,
        'Branch.' + BRANCH_ATTRIBUTES.VENDOR_TYPE,
        'location.id',
        'location.' + LOCATION_ATTRIBUTES.address,
        'location.' + LOCATION_ATTRIBUTES.latitude,
        'location.' + LOCATION_ATTRIBUTES.longitude,
        'location.' + LOCATION_ATTRIBUTES.area,
        'favorites.id',
        'favorites.is_active',
      ],
    };

    this.folder_qry = {
      attributes: [BRANCH_GALLERY_FOLDER_ATTRIBUTES.FOLDER_NAME],
      include: [
        {
          attributes: [BRANCH_GALLERY_ATTRIBUTES.GALLERY_LIST],
          model: this.branchGalleryRepo,
        },
      ],
      where: { is_deleted: false },
    };
  }

  async findBranches(body: HomeDto | ExploreDto, vendor_type?: string) {
    const { user_id, lat, lng } = body;
    this.user_id = user_id || null;
    this.lat = lat || ChennaiLocation.lat;
    this.lng = lng || ChennaiLocation.lng;
    let count: number;
    this.init();
    let distance_qry = `(6371 * acos(cos(radians(${this.lat})) * cos(radians("location"."latitude")) * cos(radians("location"."longitude") - radians(${this.lng})) + sin(radians(${this.lat})) * sin(radians("location"."latitude"))))`;

    //remove vendor type filter if selected all
    if (vendor_type !== VendorType.ALL) {
      this.branchQry.where = {
        ...this.branchQry.where,
        vendor_type: vendor_type,
      };
    }
    //add lat lng search only value exists
    if (lat && lng) {
      (this.branchQry.attributes as any[]).push([this.sequelize.literal(distance_qry), 'distance']);
      this.branchQry.order = [
        ['distance', 'ASC'],
        [BRANCH_ATTRIBUTES.ID, 'ASC'],
      ];
      this.branchQry.having = this.sequelize.literal(`${distance_qry} <= ${RADIUS}`);
    }
    //for explore api. checking is explore api using search text field
    if ('search_text' in body) {
      this.branchQry.offset = (body.page - 1) * 10;
      this.filterBranches(body);
      if (body.area_filter.length > 0 || body.search_text) {
        delete this.branchQry.having;
      }
      // count = await super.count(this.branchQry);
    }
    this.branchQry.limit = 'search_text' in body ? 10 : 5;
    let data = await super.findAll(this.branchQry);
    return data;
  }
  filterBranches(body: ExploreDto) {
    let { search_text, service_type_filter, service_category_filter, category_filter, ratings_filter, area_filter } =
      body;

    //search branches
    // this.branchQry.where.branch_name = { [Op.iLike]: `%${search_text}%` };
    if (search_text && search_text != '' && search_text != null) {
      this.branchQry.where = {
        ...this.branchQry.where,
        // [Op.or]: {
        branch_name: { [Op.iLike]: `%${search_text.trim()}%` },
        // '$services.service_name$': { [Op.iLike]: `%${search_text}%` },
        // },
      };
    }
    //filter branches based on category, service category and service type
    let branch_filter_qry = [
      {
        attributes: [],
        model: BranchServiceTypeMapping,
        where: {
          service_type_id: service_type_filter,
        },
      },
      {
        attributes: [],
        model: BranchServiceCategoryMapping,
        where: {
          service_category_id: service_category_filter,
        },
      },
      {
        attributes: [],
        model: BranchCategoryMapping,
        where: {
          category_id: category_filter,
        },
      },
    ];

    //Remove empty filters
    branch_filter_qry = branch_filter_qry.filter((filter) => {
      const filterValue = Object.values(filter.where)[0];
      return filterValue?.length > 0;
    });
    let service = {
      attributes: [],
      model: Service,
      // where: {
      //   service_name: { [Op.iLike]: `%${search_text}%` },
      // },
      required: false,
    };
    this.branchQry.include = this.branchQry.include.concat([service]);
    this.branchQry.include = this.branchQry.include.concat(branch_filter_qry);

    //rating filters
    const ratingsConditions = ratings_filter?.map((rating) =>
      Sequelize.literal(`COALESCE(AVG("reviews"."rating"), 0) >= ${rating}`),
    );
    if (ratings_filter.length > 0) {
      this.branchQry.having = {
        [Op.or]: ratingsConditions,
      };
    }

    //area filters
    this.area_filter = area_filter.map((area)=>{ return area.toLowerCase()});
    if (this.area_filter.length > 0) {
      this.branchQry.include[0].where = {
        area: this.area_filter,
      };
    }
  }

  async getServices(getServicesDto: GetServicesDto) {
    logger.info(`Get_Service_Entry: ` + JSON.stringify(getServicesDto));
    //this.init();
    //TODO get data from database if request body has any change
    // let data = await this.cacheManager.get('shop_details');
    // if (data) return data;
    let services: any = await this.servicesService.getBranchServices(getServicesDto);
    // this.branchQry.where.id = getServices.branch_id;
    // let is_shop_open: boolean = await this.getShopStatus(getServices.branch_id);
    // let shop = await super.findOne(null, this.branchQry);
    // shop['is_shop_open'] = is_shop_open;
    //let data = { count: services.count,services: services.rows };
    // await this.cacheManager.set('shop_details', data, 2000);
    logger.info(`Get_Service_Exit_data: ` + JSON.stringify(services));
    return services;
  }

  async getShopStatus(branch_id: string): Promise<boolean> {
    logger.info(`Get_Shop_Status_Entry_branchId: ` + JSON.stringify(branch_id));
    let date = new Date();
    let day = date.getDay();
    let slot = await this.getSlots(branch_id, day);
    let isBetweenSlotTimes: boolean;
    if (slot[0]) {
      isBetweenSlotTimes = DateUtils.isBetweenSlotTimes(slot[0].slot_start_time, slot[0].slot_end_time, date);
    }
    if (slot[0] && isBetweenSlotTimes) {
      logger.info(`Get_Shop_Status_Exit: ` + JSON.stringify(true));
      return true;
    } else {
      logger.info(`Get_Shop_Status_Exit: ` + JSON.stringify(false));
      return false;
    }
  }

  async getSlots(branch_id: string, day?: number) {
    logger.info(`Get_Shop_Slots_Entry_branchId: ` + JSON.stringify(branch_id) + 'day:' + JSON.stringify(day));
    if (day != undefined && day != null) {
      if (day == 0) day = 7;
      day--;
    }
    let slotQry: SequelizeFilter<SlotDetail> = {
      // attributes: ['open_at', 'close_at', 'day'],
      where: {
        branch_id: branch_id,
      },
    };
    if (day != null || day != undefined) slotQry.where.day = day;
    let slots = await Helpers.findAll(SlotDetail, slotQry);
    logger.info(`Get_Shop_Slots_Exit: ` + JSON.stringify(slots));
    return slots;
  }

  async findGalleries(branch_id: string): Promise<any> {
    logger.info(`Get_Shop_Gallaries_Entry_BranchId: ` + JSON.stringify(branch_id));
    this.init();
    this.folder_qry.where = {
      ...this.folder_qry.where,
      branch_id: branch_id,
    };
    let data = Helpers.findAll(this.branchGalleryFolderRepo, this.folder_qry);
    logger.info(`Get_Shop_Gallaries_Exit: ` + JSON.stringify(data));
    return data;
  }
  async findAllGalleries(req_data: GetGalleryList): Promise<any> {
    logger.info(`Get_Shop_Gallaries_Entry_BranchId: ` + JSON.stringify(req_data.branch_id));
    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    return await this.branchGalleryFolderRepo.findAndCountAll({
      attributes: ['folder_name', 'id'],
      include: [
        {
          attributes: ['gallery_list'],
          model: BranchGallery,
        },
      ],
      where: {
        is_deleted: false,
        is_active: true,
        branch_id: req_data.branch_id,
      },
      offset: offset,
      limit: limit,
      order: [['created_at', 'DESC']],
    });
  }

  async getSlotAvailability(date: string, branch_id: string, vendor_type: string) {
    // date = moment(date).format("YYYY/MM/DD")
    logger.info(
      `Shop_GetSlotAvailability_Entry_BranchId: ` + JSON.stringify(branch_id) + 'data:' + JSON.stringify(date),
    );
    // let gmt=DateUtils.convertToGMT(date);
    let orders = await this.getOrdersByDate(date, branch_id, vendor_type);
    //TODO Waiting for integrate with front end
    //  let reserved_slots = await this.slotManagementService.getReserveSlot(branch_id, new Date(date));
    //orders= orders.concat(reserved_slots)
    const d = new Date(date);
    let day = d.getDay();
    let slotsDetails: SlotDetail | SlotManagement = await this.getSlotDetails(branch_id, d, day);
    if ('is_leave' in slotsDetails && slotsDetails.is_leave) {
      logger.info(`Shop_GetSlotAvailability_Leave_Exit: `);
      return [];
    }
    //  else if ('is_leave' in slotsDetails && !slotsDetails.is_leave && vendor_type === VendorType.FREELANCER) { // sasi added this else if condition
    //   let slots = await this.getSlots(branch_id, day);
    //   if (slots && slots.length > 0) {
    //     slotsDetails = slots[0];
    //   } else {
    //     throw new NotFoundException(Errors.NO_SLOTS_AVAILABLE);
    //   }
    // }

    //TODO send freelancer flow slot timing - // sasi added this freelancer function
    if (vendor_type === VendorType.FREELANCER) {
      return this.freelancerSlotDetails(orders, slotsDetails);
    }
    //--------------
    let slots = await this.splitSlotDetails(slotsDetails);
    orders = JSON.stringify(orders);
    orders = JSON.parse(orders);
    return this.decreaseStaffCount(orders, slots);
  }

  freelancerSlotDetails(orders: any, slot_detail: any) {
    let data: any = { morning: slot_detail?.morning, afternoon: slot_detail?.afternoon, evening: slot_detail?.evening };
    if (orders && orders.length > 0) {
      orders?.map((order: any) => {
        if (order?.booking_slot == FreelancerSlot.MORNING) data.morning = false
        else if (order?.booking_slot == FreelancerSlot.AFTERNOON) data.afternoon = false
        else if (order?.booking_slot == FreelancerSlot.EVENING) data.evening = false
      })
    } else {
      data.morning = slot_detail?.morning ? true : false;
      data.afternoon = slot_detail?.afternoon ? true : false;
      data.evening = slot_detail?.evening ? true : false;
    }
    return [data];
  }
  async getSlotDetails(branch_id: string, date: Date, day: number): Promise<SlotDetail | SlotManagement> {
    let slotsDetails: SlotDetail | SlotManagement;
    slotsDetails = await this.slotManagementService.findOneByBranch(branch_id, date);
    if (!slotsDetails) {
      let slots = await this.getSlots(branch_id, day);

      if (slots && slots.length > 0) {
        slotsDetails = slots[0];
      } else {
        throw new NotFoundException(Errors.NO_SLOTS_AVAILABLE);
      }
    }
    return slotsDetails;
  }
  private decreaseStaffCount(orders: any, slots: any[]) {
    logger.info(`Decrease_Staff_Count_Entry_order: ` + JSON.stringify(orders) + 'slots' + JSON.stringify(slots));
    for (const appointment of orders) {
      for (const slot of slots) {
        let start_time = moment(slot.slot_start_time, 'HH:mm a');
        slot.slot_end_time = start_time.add('60', 'minutes').format('HH:mm a');
        const { slot_start_time, slot_end_time } = slot;
        const { appointment_start_date_time } = appointment;
        const appointmentDateTime = new Date(appointment_start_date_time);
        const isBetweenSlotTimes = DateUtils.isBetweenSlotTimes(slot_start_time, slot_end_time, appointmentDateTime);
        if (isBetweenSlotTimes) {
          slot.no_of_employees = slot.no_of_employees - 1;
        }
        slot.slot_start_time = moment(slot.slot_start_time, 'HH:mm a').format('hh:mm A');
        slot.slot_end_time = moment(slot.slot_end_time, 'HH:mm a').format('hh:mm A');
      }
    }
    logger.info(`Decrease_Staff_Count_Exit: ` + JSON.stringify(slots));
    return slots;
  }

  async splitSlotDetails(slots: SlotDetail | SlotManagement) {
    logger.info(`splitSlotDetails_Entry: ` + JSON.stringify(slots));
    let availableSlots = [];
    let start_time = moment(slots.slot_start_time, 'h:mm a');
    let end_time = moment(slots.slot_end_time, 'h:mm a');
    availableSlots.push({
      slot_start_time: start_time.format('h:mm a'),
      no_of_employees: slots.no_of_employees,
    });
    let diff = moment.duration(end_time.diff(start_time));
    let hours = Math.abs(diff.asHours());
    for (let index = 0; index < Math.floor(hours); index++) {
      start_time = start_time.add('60', 'minutes');
      availableSlots.push({
        slot_start_time: start_time.format('h:mm a'),
        no_of_employees: slots.no_of_employees,
      });
    }
    //insert slot end time at last position
    if (availableSlots.length > 0) {
      console.log('test' + end_time);
      // availableSlots[availableSlots.length - 1].slot_start_time = end_time.format('hh:mm a');
      // end_time.isSame(moment('11:59 PM', 'HH:mm a')) && availableSlots.pop();
    }
    logger.info(`splitSlotDetails_Exit: ` + JSON.stringify(availableSlots));
    return availableSlots;
  }

  async getOrdersByDate(date: string, branch_id: string, vendor_type: string) {
    let appointmentStatus = vendor_type == VendorType.FREELANCER ? [APPOINTMENT_STATUS.AWAITING_APPROVAL, APPOINTMENT_STATUS.CONFIRMED] : [APPOINTMENT_STATUS.CONFIRMED]
    const startOfDay = new Date(date);
    startOfDay.setHours(0, 0, 0, 0);

    const endOfDay = new Date(date);
    endOfDay.setHours(23, 59, 59, 999);
    // const startOfDay = moment(date).startOf('day').tz('Asia/kolkata');
    // const endOfDay = moment(date).endOf('day').tz('Asia/kolkata');

    let order_qry: SequelizeFilter<Order> = {
      attributes: [ORDER_ATTRIBUTES.order_id, ORDER_ATTRIBUTES.appointment_start_date_time, ORDER_ATTRIBUTES.booking_slot],
      where: {
        branch_id: branch_id,
        appointment_start_date_time: {
          [Op.gte]: startOfDay, // Greater Than or Equal to the start of the day
          [Op.lt]: endOfDay, // Less Than the end of the day
        },
        order_status: appointmentStatus,
      },
    };
    let orders = await Helpers.findAll(Order, order_qry);
    return orders;
  }

  async findAll(): Promise<any> {
    logger.info(`Shop_FindAll_Entry: `);
    let branches;
    //  = await this.cacheManager.get('branches');
    if (!branches) {
      branches = await super.findAll();
      // await this.cacheManager.set('branches', branches, 2000);
    }
    logger.info(`Shop_FindAll_Entry ` + JSON.stringify(branches));
    return branches;
  }

  async includeBranches(attributes: string[], query: any, required?: boolean) {
    return {
      attributes: attributes ? attributes : [],
      model: this.branchRepo,
      where: query,
      required: required || false,
    };
  }

  async findBranch(id: string, user_id: string) {
    this.user_id = user_id;
    this.init();
    this.branchQry.where.id = id;
    let is_shop_open: boolean = await this.getShopStatus(id);
    let shop = await super.findOne(null, this.branchQry);
    if (!shop) throw new NotFoundException('Invalid Branch Details');
    shop['is_shop_open'] = is_shop_open;
    return shop;
  }
}
