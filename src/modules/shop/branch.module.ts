import { CacheModule, Module } from '@nestjs/common';
import { BranchService } from './branch.service';
import { BranchController } from './branch.controller';
import { branchProviders } from './branch.providers';
import { ServicesModule } from '../services/services.module';
import { SlotManagementModule } from '../slot-management/slot-management.module';
import { SlotManagementService } from '../slot-management/slot-management.service';

@Module({
  imports:[ServicesModule,CacheModule.register(),SlotManagementModule],
  controllers: [BranchController],
  providers: [BranchService,...branchProviders,SlotManagementService],
  exports:[BranchService]
})
export class BranchModule {} 
