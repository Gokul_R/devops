import { Controller, Get, Post, Body, Param, CacheKey, CacheTTL, Query } from '@nestjs/common';
import { BranchService } from './branch.service';
import { GetServicesDto } from './dto/get-services-dto';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { EC100, EC200, EC204, EC500, EM100, EM106 } from 'src/core/constants';
import HandleResponse from 'src/core/utils/handle_response';
import { GetGalleryList } from './dto/getGalleryList.dto';

@Controller('branch')
@ApiTags('Branch User')
export class BranchController {
  constructor(private readonly shopService: BranchService) { }

  @Get()
  findAll() {
    try {
      return this.shopService.findAll();
    } catch (error) {
      return HandleResponse.buildErrObj(EC500, EM100, error);
    }
  }

  @Post('services')
  async services(@Body() getServices: GetServicesDto) {
    try {
      let data = await this.shopService.getServices(getServices);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get('slots/:branch_id')
  async slots(@Param('branch_id') branch_id: string) {
    try {
      let data = await this.shopService.getSlots(branch_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get('gallery/:branch_id')
  async findAllByBranch(@Param('branch_id') branch_id: string) {
    try {
      let data = await this.shopService.findGalleries(branch_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
  @Post('gallery')
  async findAllByBranchGallery(@Body() getGalleryList: GetGalleryList) {
    try {
      let data = await this.shopService.findAllGalleries(getGalleryList);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, error.message || EM100, error);
    }
  }

  @ApiQuery({
    name: 'vendor_type',
    type: String,
    description: 'Type of vendor freelancer or business',
    required: true,
  })
  @Get('slot-availability/:date/:branch_id')
  async getSlotAvailability(@Param('date') date: string, @Param('branch_id') branch_id: string, @Query('vendor_type') vendor_type: string) {
    try {
      let data = await this.shopService.getSlotAvailability(date, branch_id, vendor_type);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      // if (error?.name === 'NotFoundException')
      //   return HandleResponse.buildErrObj(EC204, error.message, error.message);
      // else 
      return HandleResponse.buildErrObj(error.status, error.message, error);
    }
  }

  @Get(':id')
  async findBranch(@Param('id') id: string, @Query('user_id') user_id: string) {
    try {
      let data = await this.shopService.findBranch(id, user_id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }


  // @Delete(':id')
  remove(@Param('id') id: string) {
    return this.shopService.remove(id);
  }
}
