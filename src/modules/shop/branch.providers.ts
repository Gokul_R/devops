import User from 'src/core/database/models/User';
import { SEQUELIZE, USER_REPOSITORY } from '../../core/constants';
import { Sequelize } from 'sequelize-typescript';
import { SequelizeModule } from '@nestjs/sequelize';
import { commonProvider } from 'src/core/interfaces/common-providers';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';

export const branchProviders = [
 
    {
        provide: SEQUELIZE,
        useValue: Sequelize,
      },
    commonProvider.branch_repository,
    commonProvider.location_repo,
    commonProvider.coupon_repository,
    commonProvider.coupon_branch_repository,
    commonProvider.branch_gallary_repository,
    commonProvider.branch_gallery_folder_repository
];
