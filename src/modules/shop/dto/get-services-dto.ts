import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class GetServicesDto{
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  public branch_id: string;

  // @ApiProperty()
  // @IsString()
  // @IsOptional()
  // public search_text: string;

  @ApiProperty()
  @IsArray()
  public category_filter: string[];

  @ApiProperty()
  @IsArray()
  public service_type_filter: string[];

  @ApiProperty()
  @IsArray()
  public service_category_filter: string[];

  @ApiProperty({ example: 'Some text' })
  @IsOptional()
  @IsString()
  searchText?: string;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  pagNo?: number;

  @ApiProperty({ example: 10 })
  @IsOptional()
  @IsNumber()
  limit?: number;
}
