import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { BRANCH_ID } from "src/core/constants";
import { PaginationDto } from "src/core/interfaces/shared.dto";

export class GetGalleryList {
    @ApiProperty({ example: BRANCH_ID })
    @IsString()
    @IsNotEmpty()
    branch_id: string;

    @ApiProperty({ example: 1 })
    @IsNotEmpty()
    @IsNumber()
    pagNo?: number;

    @ApiProperty({ example: 10 })
    @IsOptional()
    @IsNumber()
    limit?: number;
}