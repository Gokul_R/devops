import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";
import { IsNotEmpty } from "nestjs-class-validator";

export class CreateAwsS3Dto {}
export class FileUploadDto {
      @ApiProperty({ type: 'string', format: 'binary' }) 
      image: any;
      @ApiProperty({ type: 'string',required:false, format: 'string' }) 
      field_name: any;
    }