import { Injectable } from '@nestjs/common';
import { FileUploadDto } from './dto/create-aws-s3.dto';
import { S3 } from 'aws-sdk';
import { ConfigService } from '@nestjs/config';
import { s3Credentials } from 'src/core/constants/appConfig';
import { logger } from 'src/core/utils/logger';

@Injectable()
export class AwsS3Service {
  private readonly s3: S3;

  constructor(private readonly configService: ConfigService) {
    this.s3 = new S3(s3Credentials);
  }

  // async uploadFile(buffer: Buffer, fileName: string, path: string): Promise<string> {
  async uploadFile(file: Express.Multer.File, fileUploadDto: FileUploadDto): Promise<string> {
    const { buffer, originalname, mimetype } = file;
    let path = originalname.split('_');
    let full_path = path[0];
    if (path.length > 2) full_path = path[0] + '/' + path[1];
    const uploadParams: S3.Types.PutObjectRequest = {
      Bucket: s3Credentials.bucket_name + '/' + full_path,
      Key: originalname,
      ContentType: mimetype,
      Body: buffer,
      ACL: 'public-read', // or 'private' based on your requirements
    };

    const uploadResponse = await this.s3
      .upload(uploadParams)
      .promise()
      .catch((err) => {
        logger.error(err);
        console.log('Error At uploading image===>', err);
        logger.error('ErrorAtUploadingSingleImage: ' + JSON.stringify(err));
        throw new Error(err);
      });
    let uploaded_image_path = full_path + '/' + uploadParams.Key;
    let res;
    if (fileUploadDto?.field_name) {
      res = { [fileUploadDto.field_name]: uploaded_image_path };
    } else {
      res = uploaded_image_path;
    }

    return res;
  }

  async uploadMultipleImages(files: Array<Express.Multer.File>): Promise<string[]> {
    const uploadPromises = [];

    for (const file of files) {
      const { originalname } = file;
      let path = originalname.split('_');
      let full_path = path[0];
      if (path.length > 2) full_path = path[0] + '/' + path[1];
      const uploadParams: AWS.S3.Types.PutObjectRequest = {
        Bucket: s3Credentials.bucket_name + '/' + full_path,
        Key: file.originalname,
        ContentType: file.mimetype,
        Body: file.buffer,
        ACL: 'public-read', // or 'private' based on your requirements
      };
      const uploadResponse = await this.s3
        .upload(uploadParams)
        .promise()
        .catch((err) => {
          console.log('Error At uploading image===>', err);
          logger.error('ErrorAtUploadingMultiImage: ' + JSON.stringify(err));
          throw new Error(err);
        });
      uploadPromises.push(full_path + '/' + uploadParams.Key);
    }

    const uploadLocations = await Promise.all(uploadPromises);
    return uploadLocations;
  }

  // create(createAwsS3Dto: CreateAwsS3Dto) {
  //   return 'This action adds a new awsS3';
  // }
  // findAll() {
  //   return `This action returns all awsS3`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} awsS3`;
  // }

  // update(arg0: number, updateAwsS3Dto: UpdateAwsS3Dto) {
  //   throw new Error('Method not implemented.');
  // }
  // findOne(arg0: number) {
  //   throw new Error('Method not implemented.');
  // }
}
