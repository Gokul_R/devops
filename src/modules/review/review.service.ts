import { Injectable } from '@nestjs/common';
import { BaseService } from 'src/base.service';
import Review from 'src/core/database/models/Reviews';
import { ModelCtor } from 'sequelize-typescript';
import { QueryTypes } from 'sequelize';
import { EC200, EM106 } from 'src/core/constants';
import User from 'src/core/database/models/User';
import HandleResponse from 'src/core/utils/handle_response';
import Helpers from 'src/core/utils/helpers';
import { USER_ATTRIBUTES } from 'src/core/attributes/user';
import ReviewReply from 'src/core/database/models/ReviewReply';
import Branch from 'src/core/database/models/Branch';
import { Op } from 'sequelize';

@Injectable()
export class ReviewService extends BaseService<Review> { 
  protected model: ModelCtor<Review> = Review;

  async getReviews(id: string) {
    let reviewqry = { 
      attributes: {
        exclude: ['is_active','updatedAt'],
      },
      where: {
        branch_id: id,
        is_active: true,
        is_deleted: false,
      },
      include: [
        {
          attributes: [USER_ATTRIBUTES.USER_NAME, USER_ATTRIBUTES.PROFILE_URL],
          model: User,
          paranoid:false
        },
        {
          attributes: ['reply_review'],
          model: ReviewReply,
          required: false,
        },
      ],
      group: [
        'Review.id',
        'user.id',
        'Review.rating',
        'Review.review',
        'Review.review_images',
        'Review.user_id',
        'Review.branch_id',
        'Review.is_deleted',
        "reply.id"
      ],
      order:[['created_at','DESC']]
    };

    let avg_star_qry = Review.sequelize.literal(`SELECT AVG(rating) FROM reviews WHERE reviews.branch_id ='${id}'`);
    let total_star_qry = Review.sequelize.literal(
      `(SELECT COUNT(rating) FROM reviews WHERE reviews.branch_id ='${id}' AND is_deleted=false AND deleted_at is null)`,
    );
    let five_star_qry = Review.sequelize.literal(
      `(SELECT COUNT(rating) FROM reviews WHERE reviews.branch_id ='${id}' AND rating = 5 AND is_deleted=false AND deleted_at is null)`,
    );
    let four_star_qry = Review.sequelize.literal(
      `(SELECT COUNT(rating) FROM reviews WHERE reviews.branch_id ='${id}' AND rating = 4 AND is_deleted=false AND deleted_at is null)`,
    );
    let three_star_qry = Review.sequelize.literal(
      `(SELECT COUNT(rating) FROM reviews WHERE reviews.branch_id ='${id}' AND rating = 3 AND is_deleted=false AND deleted_at is null)`,
    );
    let two_star_qry = Review.sequelize.literal(
      `(SELECT COUNT(rating) FROM reviews WHERE reviews.branch_id ='${id}' AND rating = 2 AND is_deleted=false AND deleted_at is null)`,
    );
    let one_star_qry = Review.sequelize.literal(
      `(SELECT COUNT(rating) FROM reviews WHERE reviews.branch_id ='${id}' AND rating = 1 AND is_deleted=false)`,
    );
 
    let avg_stars = await Review.sequelize.query(`${avg_star_qry.val} AND deleted_at is null`, {
      type: QueryTypes.SELECT,
    });
    let total_stars = await Review.sequelize.query(`SELECT ${total_star_qry.val}`, { type: QueryTypes.SELECT });
    let five_stars = await Review.sequelize.query(`SELECT ${five_star_qry.val}`, { type: QueryTypes.SELECT });
    let four_stars = await Review.sequelize.query(`SELECT ${four_star_qry.val}`, { type: QueryTypes.SELECT });
    let three_stars = await Review.sequelize.query(`SELECT ${three_star_qry.val}`, { type: QueryTypes.SELECT });
    let two_stars = await Review.sequelize.query(`SELECT ${two_star_qry.val}`, {
      type: QueryTypes.SELECT,
    });
    let one_stars = await Review.sequelize.query(`SELECT ${one_star_qry.val}`, {
      type: QueryTypes.SELECT,
    });

    let reviews = await Helpers.findAll(Review, reviewqry);

    let reviewsWithRatings = {
      reviews,
      avg_star: parseFloat(avg_stars[0]['avg']).toFixed(1),
      total_reviews: parseInt(total_stars[0]['count']),
      five_star: parseInt(five_stars[0]['count']),
      four_star: parseInt(four_stars[0]['count']),
      three_star: parseInt(three_stars[0]['count']),
      two_star: parseInt(two_stars[0]['count']),
      one_star: parseInt(one_stars[0]['count']),
    };
    let { one_star, two_star, three_star, four_star, five_star } = reviewsWithRatings;
    reviewsWithRatings['ratings'] = [one_star, two_star, three_star, four_star, five_star];
    return reviewsWithRatings;
  }

  async getFilterList(req_data: any) {
    let search: any;
    if (req_data.searchText) {
      const searchText = req_data.searchText.toLowerCase();
      search = {
        [Op.or]: [
          { '$user.user_name$': { [Op.iLike]: `%${searchText}%` } },
          { '$branch.branch_name$': { [Op.iLike]: `%${searchText}%` } },
          { review: { [Op.iLike]: `%${searchText}%` } },
        ],
      };
    }

    let pagNo = req_data.pagNo || 1;
    let limit = req_data.limit || 10;
    let offset = (pagNo - 1) * req_data.limit;
    let branch_id = req_data.branch_id ? { branch_id: req_data.branch_id } : {};
    let query: any = {
      where: {
        ...branch_id,
        is_deleted: false,
        ...search,
      },
      include: [
        {
          attributes: ['user_name'],
          model: User,
          as: 'user', // Specify the alias for the User model if it's defined in the association
          required: true,
        },
        {
          attributes: ['branch_name'],
          model: Branch,
          as: 'branch', // Specify the alias for the Branch model if it's defined in the association
          required: true,
        },
      ],
    };
    let count = await Review.count(query);
    query.include.push({
      attributes: ['reply_review_id', 'reply_review', 'user_type', 'reply_user_id', 'created_at'],
      model: ReviewReply,
      as: 'reply',
      // required: true,
    });
    let data = await Review.findAll({
      ...query,
      offset: offset,
      limit: limit,
      order: [['created_at', 'DESC']],
    });
    return { count: count ? count : 0, rows: data ? data : [] };
  }
}
