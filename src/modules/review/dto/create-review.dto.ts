import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayMinSize,
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { BRANCH_ID } from 'src/core/constants';
import { PaginationDto } from 'src/core/interfaces/shared.dto';
import { UserTypes } from 'src/core/utils/enum';

export class CreateReviewDto {
  @ApiProperty()
  @IsNotEmpty()
  rating: number;
  @ApiProperty()
  @IsNotEmpty()
  review!: string;
  @ApiProperty()
  @IsNotEmpty()
  user_id!: string;

  @ApiProperty()
  @IsNotEmpty()
  branch_id!: string;

  @ApiProperty()
  @IsArray()
  // @ArrayMinSize(1)
  @IsString({ each: true })
  @IsOptional()
  review_images?: string[];

  @ApiProperty({ example: 'User Name' })
  @IsOptional()
  user_name?: string;

  @ApiProperty({ example: `${UserTypes.USER}` })
  @IsOptional()
  @IsEnum(UserTypes)
  user_type?: string;
  // @ApiProperty()
  // @IsNotEmpty()
  // service_id!: string;
}


export class GetBranchReviewsDto extends PaginationDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: BRANCH_ID })
  branch_id?: string;
}