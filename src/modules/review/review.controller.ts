import { Controller, Get, Post, Body, Patch, Param } from '@nestjs/common';
import { ReviewService } from './review.service';
import { CreateReviewDto, GetBranchReviewsDto } from './dto/create-review.dto';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { BaseController } from 'src/base.controller';
import HandleResponse from 'src/core/utils/handle_response';
import { EC200, EM100, EM104, EM106, EM116 } from 'src/core/constants';
import { UpdateReviewDto } from 'src/admin/review/dto/update-review.dto';

@Controller('review')
@ApiTags('Reviews')
export class ReviewController extends BaseController<CreateReviewDto> {
  constructor(private readonly reviewService: ReviewService) {
    super(reviewService);
  }

  @Post()
  async create(@Body() reivew: CreateReviewDto) {
    try {
      let data = await this.reviewService.create(reivew);
      return HandleResponse.buildSuccessObj(EC200, EM104, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }


  @Post('search')
  async getFilterList(@Body() getReviewListedto: GetBranchReviewsDto) {
    try {
      let data = await this.reviewService.getFilterList(getReviewListedto);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    example: '62a8332aecb4a12ed2bd2f44',
    description: 'Branch id',
  })
  async findOne(@Param('id') id: string) {
    try {
      let data = await this.reviewService.getReviews(id);
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateReviewDto: UpdateReviewDto) {
    try {
      let data = await this.reviewService.update(id, updateReviewDto);
      return HandleResponse.buildSuccessObj(EC200, EM116, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }
}
