import { Body, Controller, Get, Patch, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { GetForceUpdate } from './modules/shared/dto/forceUpdate.dto';
import HandleResponse from './core/utils/handle_response';
import { EC200, EM100, EM106 } from './core/constants';
import { ApiTags } from '@nestjs/swagger';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @ApiTags('Force Update')
  @Post('force-update')
  async forceUpdate(@Body() getForceUpdate: GetForceUpdate) {
    try {
      let data = await this.appService.getForceUpdateVersion(getForceUpdate);;
      return HandleResponse.buildSuccessObj(EC200, EM106, data);
    } catch (error) {
      return HandleResponse.buildErrObj(null, EM100, error);
    }
  }

}
