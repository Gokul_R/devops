# -----------------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------------
# ---------------------------for-local-----------------------------------

# # Stage 1: Build stage
# FROM node:18-alpine as build

# # Install NestJS CLI globally
# RUN npm install -g @nestjs/cli

# WORKDIR /slaylewks_devops_test

# # Copy dependencies files
# COPY package*.json ./
# COPY tsconfig.build.json ./

# # Install dependencies (including dev dependencies for build)
# RUN npm install --force

# # Copy source files
# COPY . .

# # Build the application
# RUN npm run build

# # Stage 2: Production stage
# FROM node:18-alpine as production

# # Install PM2 globally
# RUN npm install -g pm2

# WORKDIR /slaylewks

# # Copy only necessary files from build stage
# COPY --from=build /slaylewks_devops_test/package*.json ./
# COPY --from=build /slaylewks_devops_test/.env ./
# COPY --from=build /slaylewks_devops_test/dist ./dist

# # Install only production dependencies
# RUN npm install --production --force

# # Expose application ports
# EXPOSE 7000
# EXPOSE 8000

# # Set NODE_ENV to development
# ENV NODE_ENV=development

# # Start application with PM2
# CMD ["pm2-runtime", "dist/main.js", "--no-daemon"]

# # Optional: Health check (adjust URL/path as needed)
# HEALTHCHECK --interval=30s --timeout=30s --start-period=10s --retries=3 CMD curl -f http://localhost:7000/health || exit 1

# -----------------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------------

# Stage 1: Build stage
FROM node:18-alpine as build

# Install NestJS CLI globally
RUN npm install -g @nestjs/cli

WORKDIR /slaylewks_devops_test

# Copy dependencies files
COPY package*.json ./
COPY tsconfig.build.json ./

# Install dependencies (including dev dependencies for build)
RUN npm install --force

# Copy source files
COPY . .

# Build the application
RUN npm run build

# Stage 2: Development stage
FROM node:18-alpine 

# Install PM2 globally
RUN npm install -g pm2

WORKDIR /slaylewks

# Copy only necessary files from build stage
COPY --from=build /slaylewks_devops_test/package*.json ./
COPY --from=build /slaylewks_devops_test/dist ./dist

# Install only development dependencies
RUN npm install --development --force

# Expose application ports
EXPOSE 7000
EXPOSE 8000

# Set NODE_ENV to development
ENV NODE_ENV=development

# Set environment variables
ARG POSTGRES_DB
ARG POSTGRES_USER
ARG POSTGRES_PASSWORD
ARG POSTGRES_HOST
ARG POSTGRES_PORT
ARG NODE_ENV
ARG USER_PORT
ARG ADMIN_PORT
ARG TZ
ARG ECY_ALGO
ARG ECY_KEY
ARG ECY_IV
ARG JWTKEY
ARG RAZORPAY_WH_SECRET
ARG S3_ACCESS_KEY_ID
ARG S3_SECRET_ACCESSKEY_ID
ARG S3_BUCKET_NAME_DEV
ARG S3_BUCKET_NAME_PROD
ARG S3_CDN_BUCKET_NAME_DEV
ARG S3_CDN_BUCKET_NAME_PROD
ARG TOKEN_EXPIRATION
ARG DB_HOST_DEV
ARG DB_PORT_DEV
ARG DB_USER_DEV
ARG DB_PASSWORD_DEV
ARG DB_NAME_DEV
ARG DB_DIALECT_DEV
ARG FRONTEND_FORGET_URL_DEV
ARG ASSETS_PATH_DEV

# Set environment variables
ENV POSTGRES_DB=$POSTGRES_DB
ENV POSTGRES_USER=$POSTGRES_USER
ENV POSTGRES_PASSWORD=$POSTGRES_PASSWORD
ENV POSTGRES_HOST=$POSTGRES_HOST
ENV POSTGRES_PORT=$POSTGRES_PORT
ENV NODE_ENV=$NODE_ENV
ENV USER_PORT=$USER_PORT
ENV ADMIN_PORT=$ADMIN_PORT
ENV TZ=$TZ
ENV ECY_ALGO=$ECY_ALGO
ENV ECY_KEY=$ECY_KEY
ENV ECY_IV=$ECY_IV
ENV JWTKEY=$JWTKEY
ENV RAZORPAY_WH_SECRET=$RAZORPAY_WH_SECRET
ENV S3_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
ENV S3_SECRET_ACCESSKEY_ID=$S3_SECRET_ACCESSKEY_ID
ENV S3_BUCKET_NAME_DEV=$S3_BUCKET_NAME_DEV
ENV S3_BUCKET_NAME_PROD=$S3_BUCKET_NAME_PROD
ENV S3_CDN_BUCKET_NAME_DEV=$S3_CDN_BUCKET_NAME_DEV
ENV S3_CDN_BUCKET_NAME_PROD=$S3_CDN_BUCKET_NAME_PROD
ENV TOKEN_EXPIRATION=$TOKEN_EXPIRATION
ENV DB_HOST_DEV=$DB_HOST_DEV
ENV DB_PORT_DEV=$DB_PORT_DEV
ENV DB_USER_DEV=$DB_USER_DEV
ENV DB_PASSWORD_DEV=$DB_PASSWORD_DEV
ENV DB_NAME_DEV=$DB_NAME_DEV
ENV DB_DIALECT_DEV=$DB_DIALECT_DEV
ENV FRONTEND_FORGET_URL_DEV=$FRONTEND_FORGET_URL_DEV
ENV ASSETS_PATH_DEV=$ASSETS_PATH_DEV


# Start application with PM2
CMD ["pm2-runtime", "dist/main.js", "--no-daemon"]

# Optional: Health check (adjust URL/path as needed)
HEALTHCHECK --interval=30s --timeout=30s --start-period=10s --retries=3 CMD curl -f http://localhost:7000/health || exit 1
