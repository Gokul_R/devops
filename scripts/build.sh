echo "Reading build.sh file"

# Login to Docker Hub
echo "$DOCKER_HUB_TOKEN" | docker login -u "$DOCKER_HUB_USER" --password-stdin
if [ $? -eq 0 ]; then
  echo "Docker Hub login success"
else
  echo "Docker Hub login failed"
  exit 1
fi

# Build Docker image
# docker-compose build "-t gokulravichandran/slaylewks:${CI_COMMIT_SHORT_SHA}"
# docker-compose build --build-arg IMAGE_TAG="$DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}".
docker build -t $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA} .

if [ $? -eq 0 ]; then
  echo "Docker build success"
else
  echo "Docker build failed"
  exit 1
fi

# Push Docker image
docker push $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}
if [ $? -eq 0 ]; then
  echo "Docker image pushed to hub"
else
  echo "Docker push failed"
  exit 1
fi
# -----------------------------to test deploy deeply---------------------------------------------------------------
# echo "Reading build.sh file"
# # Login to Docker Hub
# echo "$DOCKER_HUB_TOKEN" | docker login -u "$DOCKER_HUB_USER" --password-stdin
# if [ $? -eq 0 ]; then
#   echo "Docker Hub login success"
# else
#   echo "Docker Hub login failed"
#   exit 1
# fi