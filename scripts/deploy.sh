# # echo "Reading deploy.sh file"

# # # Start the SSH agent
# # eval $(ssh-agent -s)

# # # Add the SSH private key to the agent
# # echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
# # if [ $? -eq 0 ]; then
# #   echo "SSH private key added successfully"
# # else
# #   echo "Failed to add SSH private key"
# #   exit 1
# # fi

# # # Check if REMOTE_HOST and REMOTE_USER are set
# # if [ -z "$REMOTE_HOST" ] || [ -z "$REMOTE_USER" ]; then
# #   echo "REMOTE_HOST or REMOTE_USER is not set"
# #   exit 1
# # fi

# # echo "REMOTE_HOST is set to $REMOTE_HOST"
# # echo "REMOTE_USER is set to $REMOTE_USER"

# # # Create .ssh directory and set permissions
# # mkdir -p ~/.ssh
# # chmod 700 ~/.ssh

# # # Add the remote host to known_hosts
# # ssh-keyscan $REMOTE_HOST >> ~/.ssh/known_hosts
# # chmod 644 ~/.ssh/known_hosts

# # # Execute commands on the remote host
# # ssh $REMOTE_USER@$REMOTE_HOST 
# #   echo "$DOCKER_HUB_TOKEN" | docker login -u "$DOCKER_HUB_USER" --password-stdin
# #   if [ $? -eq 0 ]; then
# #     echo "Docker Hub login success"
# #   else
# #     echo "Docker Hub login failed"
# #     exit 1
# #   fi

# #   docker pull $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}
# #   if [ $? -eq 0 ]; then
# #     echo "Docker image pulled successfully"
# #   else
# #     echo "Docker image pull failed"
# #     exit 1
# #   fi

# #   docker stop test-slaylewks-app || true
# #   docker rm test-slaylewks-app || true
# #   docker run -d --name test-slaylewks-app -p 7000:7000 -p 8000:8000 $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}
# #   if [ $? -eq 0 ]; then
# #     echo "Docker container started successfully"
# #   else
# #     echo "Failed to start Docker container"
# #     exit 1
# #   fi
# # echo "Reading deploy .sh file"
# # 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
# # eval $(ssh-agent -s)
# # echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
# # mkdir -p ~/.ssh
# # chmod 700 ~/.ssh
# # ssh-keyscan $REMOTE_HOST >> ~/.ssh/known_hosts
# # chmod 644 ~/.ssh/known_hosts
# # ssh $REMOTE_USER@$REMOTE_HOST "docker login -u $DOCKER_HUB_USER --password-stdin <<< $DOCKER_HUB_TOKEN && docker pull $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA} && docker stop slaylewks-devops-test || true && docker rm slaylewks-devops-test || true && docker run -d --name slaylewks-devops-test -p 7000:7000 -p 8000:8000 $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}"
# # #ssh $REMOTE_USER@$REMOTE_HOST "docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD && docker pull $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA} && docker stop test-cicd-app || true && docker rm test-cicd-app || true && docker run -d --name test-cicd-app -p 3500:3000 $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}"

# #!/bin/bash

# echo "Reading deploy.sh file"

# # Ensure the SSH agent is running and add the private key
# which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
# eval $(ssh-agent -s)
# echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

# # Prepare the SSH configuration
# mkdir -p ~/.ssh
# chmod 700 ~/.ssh
# ssh-keyscan $REMOTE_HOST >> ~/.ssh/known_hosts
# chmod 644 ~/.ssh/known_hosts

# # Debugging information
# echo "REMOTE_HOST: $REMOTE_HOST"
# echo "REMOTE_USER: $REMOTE_USER"

# # SSH into the remote server and perform the deployment steps
# ssh -o StrictHostKeyChecking=no $REMOTE_USER@$REMOTE_HOST << EOF
#   echo "Logging in to Docker Hub..."
#   echo "$DOCKER_HUB_TOKEN" | docker login -u "$DOCKER_HUB_USER" --password-stdin
#   docker pull $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}
#   docker stop slaylewks-devops-test || true
#   docker rm slaylewks-devops-test || true
#   docker run -d --name slaylewks-devops-test -p 7000:7000 -p 8000:8000 $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}
# EOF

# echo "Deployment completed successfully"
#!/bin/bash
#!/bin/bash

echo "Reading deploy.sh file"

# Ensure the SSH agent is running
# which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
eval $(ssh-agent -s)

# Add the private key to the SSH agent
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

# Ensure ~/.ssh directory exists and set correct permissions
mkdir -p ~/.ssh
chmod 700 ~/.ssh

# Add remote host key to known_hosts and set correct permissions
ssh-keyscan $REMOTE_HOST >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# SSH to remote host and run docker commands
echo "root login"
ssh root@74.50.86.238<< EOF
docker login -u $DOCKER_HUB_USER --password-stdin <<< $DOCKER_HUB_TOKEN
docker pull $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}
# docker pull gokulravichandran/slaylewks:08bd4d9f
# docker stop slaylewks-devops-test || true
# docker rm slaylewks-devops-test || true
docker run -d --name slaylewks-devops-test -p 7000:7000 -p 8000:8000 $DOCKER_HUB_USER/$DOCKER_HUB_APPNAME:${CI_COMMIT_SHORT_SHA}
EOF
